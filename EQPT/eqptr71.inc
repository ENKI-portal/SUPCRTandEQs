C                                                                               
C                                                                               
c-------------------------------------------------------------------            
c implicit.h                                                                    
c   standard implicit statement for 32 bit machines                             
c   for eq3/6 codes generally requiring 64 bit arithmetic                       
      implicit logical(q),integer(i,j,k,n),real*8(a-h,l,m,o,p,r-t,v-z),         
     $ character*8(u)                                                           
c----------------------------------------------------------------               
c parset.h  last revised 12sep86 by tjw                                         
c                                                                               
c   symbolic dimensioning                                                       
c                                                                               
c   nctpar = max no. of elements                                                
c   nsqpar = nsqmax = max no. of expanded basis species                         
c   iktpar = max no. of solid solution end members                              
c   npxpar = max dimension for pitzer parameter arrays                          
c   nstpar = maximum number of aqeuous species on                               
c                       file data0                                              
c                                                                               
      parameter(nctpar=100,nsqpar=200,iktpar=20)                                
      parameter(npxpar=600,nstpar=1000)                                         
c                                                                               
      parameter(nsqpa1=nsqpar+1,nsqpa2=nsqpar+2)                                
      parameter(nctpa1=nctpar+1)                                                
C--------------------------------------------------------------                 
c ab.h                                                                          
      common /ab/ ars(10,nsqpa1)                                                
c--------------------------------------------------------------                 
c av.h                                                                          
      common /av/ avs(10,nsqpa1)                                                
c--------------------------------------------------------------                 
c blank.h                                                                       
      common cdrs(nsqpa2,nsqpa1),cess(nctpar,nsqpa1),z(nsqpa1)                  
c                                                                               
      character*24 uspec                                                        
      common /uspec/ uspec(nsqpa1)                                              
c----------------------------------------------------------------               
c dat.h                                                                         
      common /dat/ nublk1,ixred(nsqpar),nublk3,nubas                            
c                                                                               
      character*80 ublk1,uel,ublk3                                              
      CHARACTER*80  UBASIS                                                      
C  *************************************************************                
C                                                                               
C                                                                               
C                     CHANGES NEEDED FOR SEGMENTING                             
C                     ON PRIME MACHINES                                         
C                                                                               
C                                                                               
C     common /datc/ ublk1(12),uel(nctpar),ublk3(52),ubasis(3375)                
C                                                                               
C                                                                               
      common /datc/ ublk1(12),uel(nctpar),ublk3(52)                             
      COMMON /DATC2/ ubasis(3375)                                               
C                                                                               
C                                                                               
C  *************************************************************                
c      ublk1   lines from 1 to 'elements'                                       
c      uel     nct lines, element data                                          
c      ublk3   lines from 'temperatures' to 'aqueous species'                   
c      ubasis  lines from nsq basis species blocks                              
c--------------------------------------------------------------                 
c dbug.h                                                                        
      common /dbug/ ndbfl3,qdbug1,qdbug2                                        
c--------------------------------------------------------------                 
c dv.h                                                                          
      common /dv/ delvr(8,nctpa1)                                               
c--------------------------------------------------------------                 
c ee.h                                                                          
      common /ee/ cco2(8,4),ch2o(8,4)                                           
c-------------------------------------------------------------------            
c eqtaa.h                                                                       
      common /eqtaa/ atwt(nctpar),oxfac                                         
c                                                                               
      common /eqtaac/ uelem(nctpar),uoxide                                      
c---------------------------------------------------------------                
c eqtti.h                                                                       
      character*80 utitld                                                       
      common /eqtti/ utitld(4)                                                  
c----------------------------------------------------------------               
c eqttt.h                                                                       
      common /eqttt/nct,nsb,nsb1,nsb2,nsq,nsq1,nsq2,nsqmax                      
c----------------------------------------------------------------               
c eqtver.h                                                                      
      common /eqtver/ ucode,urelno,ustage,ueqlrn,ueqlst                         
c----------------------------------------------------------------               
c fa.h                                                                          
      common /fa/ nslt                                                          
c                                                                               
      character*16 ussrs                                                        
      common /fac/ ussrs(2,npxpar)                                              
c----------------------------------------------------------------               
c fsp.h                                                                         
      common /fsp/ nssp                                                         
c                                                                               
      character*16 ussp                                                         
      common /fspc/ ussp(npxpar)                                                
c----------------------------------------------------------------               
c hh.h                                                                          
      common /hh/ tempc(8),press(8),adh(8),bdh(8),bdot(8)                       
c-------------------------------------------------------------------            
c hhn.h                                                                         
      common /hhn/ tempcs(8), tmax                                              
c-----------------------------------------------------------------------        
c jj.h                                                                          
      common /jj/ jrflag(2,nsqpa1)                                              
c--------------------------------------------------------------                 
c jv.h                                                                          
      common /jv/ jvflag(2,nsqpa1)                                              
c--------------------------------------------------------------                 
c key.h                                                                         
      common /key/ ukey                                                         
c-------------------------------------------------------------------            
c mu.h                                                                          
      common /mu/ cph(npxpar),dc(npxpar),d2c(npxpar),zm(npxpar),                
     $ zx(npxpar),l0(npxpar),l1(npxpar),l2(npxpar),dl0(npxpar),                 
     $ dl1(npxpar),dl2(npxpar),d2l0(npxpar),d2l1(npxpar),d2l2(npxpar),          
     $ alph1(npxpar),alph2(npxpar),npxp                                         
c                                                                               
      character*24 umsp,uxsp                                                    
      common /muc/ umsp(npxpar),uxsp(npxpar)                                    
c----------------------------------------------------------------               
c nfm.h                                                                         
      common /nfm/ nmn                                                          
c                                                                               
      character*24 umspn                                                        
      common /nfmc/ umspn(npxpar)                                               
c----------------------------------------------------------------               
c nfsp.h                                                                        
      common /nfsp/ nspn,nspt                                                   
c                                                                               
      character*24 usspn                                                        
      common /nfspc/ usspn(npxpar)                                              
c----------------------------------------------------------------               
c nrp.h                                                                         
      common /nrp/ nrsp                                                         
c                                                                               
      character*16 ursp                                                         
      common /nrpc/ ursp(npxpar)                                                
c     ursp                                                                      
c      species names where species found on data0 and dpitz0,                   
c      but at least one reaction species not found in                           
c      the restricted basis                                                     
c----------------------------------------------------------------               
c nsp.h                                                                         
      common /nsp/ nstmax,nnsp                                                  
c                                                                               
      character*16 unsp                                                         
      common /nspc/ unsp(nstpar)                                                
c     unsp                                                                      
c      species names on data0, not found on dpitz0                              
c-------------------------------------------------------------------            
c re.h                                                                          
      common /re/ cdum1(nsqpa1)                                                 
c                                                                               
      character*24 unam24                                                       
      common /rec/ unam24(nsqpa1),unam8(nctpar)                                 
c-------------------------------------------------------------------            
c rsb.h                                                                         
      common /rsb/ nrsb                                                         
c                                                                               
      character*16 ursb                                                         
      common /rsbc/ ursb(nsqpar)                                                
c----------------------------------------------------------------               
c snm.h                                                                         
      common /snm/ nst,nst0,z0(nstpar)                                          
c                                                                               
      character*24 uspec0                                                       
      common /snmc/ uspec0(nstpar)                                              
c     uspec0                                                                    
c      names of aqeuous species on data0                                        
c----------------------------------------------------------------               
c un.h                                                                          
      common /un/ ndat0s,nda0,ndpt0s,ndpt1,ndpt2,ndhkf,                         
     $    nout,noutpt,nslist,ndata1,ndata2,ndata3,nttyi,nttyo                   
c----------------------------------------------------------------               
c uu.h                                                                          
      common /uu/ xbarlm(iktpar),apx(12)                                        
c----------------------------------------------------------------               
c xx.h                                                                          
      common /xx/ xlks(8,nsqpa1),xlkeh(8),xlkh2(8),xlkn2(8),xlko2(8)            
c-------------------------------------------------------------------            
