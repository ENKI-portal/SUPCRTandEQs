C D 
C  Last Compiled: '04 Jun 89' '1:58 PM'  PRD002>BHL>EQ3/6>EQPT.DIR>EQPT.F77     
c adnam    last revised 08/02/87 by tjw                                         
c*adnam f77 rewrite 01/28/87 by tjw                                             
c                                                                               
c     This routine may appear first in the concatenated source file             
c     of the EQPT computer code, part of the EQ3/6 software package.  The       
c     copyright notice for this code and other information are contained        
c     in the main program, eqpt.                                                
c                                                                               
      subroutine adnam(unm,unam,nmax,n,iflag)                                   
c                                                                               
c     input  unm     ascii name                                                 
c            unam    array of ascii names                                       
c            nmax    array bound of unam                                        
c            n       number of entries in unam                                  
c                                                                               
c     add unm to array unam.                                                    
c     increment n                                                               
c     iflag = 0 if array does not overflow                                      
c     if array is full, do not add, set iflag = -1                              
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C
                                                                               
      include "eqptr71.inc"
                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/un.h'                                                     
c                                                                               
      character*(*) unm,unam(*)                                                 
c                                                                               
c----------------------------------------------------------------------         
      iflag = -1                                                                
      if (n .lt. nmax) then                                                     
c                                                                               
c       unam is not full                                                        
c                                                                               
        iflag = 0                                                               
        n = n+1                                                                 
        unam(n) = unm                                                           
      endif                                                                     
      end                                                                       
c adspn    last revised 02/03/87 by tjw                                         
c*adspn f77 rewrite 01/28/87 by tjw                                             
      subroutine adspn(usp)                                                     
c                                                                               
c     add a species name to array usspn                                         
c                                                                               
c     input   usp   ascii species name a12                                      
c                                                                               
c     test usspn - if species name usp is found, return.                        
c     if not found, tally nspn and add species to usspn                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C
                                                                               
      include "eqptr71.inc"
                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/nfsp.h'                                                   
C     include '../inc/un.h'                                                     
c                                                                               
      character*24 usp                                                          
c                                                                               
c     nspn = number of species in array usspn                                   
c     nspt = maximum number of species names allowed in usspn                   
c     usspn = ascii species names not found in uspec                            
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     if tally = 0, put first entry in                                          
c                                                                               
      if (nspn .gt. 0) then                                                     
c                                                                               
c       search for species in usspn                                             
c                                                                               
        do 10 ns=1,nspn                                                         
        if (usp(1:24) .eq. usspn(ns)(1:24)) go to 999                           
   10   continue                                                                
      endif                                                                     
c                                                                               
c     species not found, put in                                                 
c                                                                               
      nspn = nspn+1                                                             
      if (nspn .gt. nspt) then                                                  
        write (nttyo,1000)                                                      
        write (nout,1000)                                                       
 1000   format(' * usspn array index overflow, routine adspn')                  
        stop                                                                    
      else                                                                      
        usspn(nspn) = usp                                                       
      endif                                                                     
  999 continue                                                                  
      end                                                                       
c bda0p    last revised 05/01/87 by rmm                                         
c*bda0p f77 rewrite 01/28/87 by tjw                                             
      subroutine bda0p(ndatp0)                                                  
c                                                                               
c     build file data0p                                                         
c      parallel to file data0                                                   
c                                                                               
c     data0p will include the 'restricted basis', and only                      
c     element data corresponding to it                                          
c                                                                               
c     the restricted basis is defined below -                                   
c     data0p includes                                                           
c      1)  element data (a reduced set), and other parameters                   
c      2)  strict basis species only if they also appear                        
c           on dpt0 (this set is the initial restricted basis)                  
c      3)  auxiliary basis species if                                           
c          a) they appear on dpt0  *and*                                        
c          b) all reaction species appear in the restricted basis               
c          auxiliary basis species which satisfy these criteria                 
c          are added to the restricted basis set                                
c          c) they are h2(aq) or o2(aq)                                         
c      4)  all other aqueous species if they satisfy 3) a) and )b               
c      5)  minerals, gases, if they satisfy 3)  b)                              
c      6)  all solid solution data                                              
c                                                                               
c     ndatp0 is unit number of file data0p                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/dat.h'                                                    
C     include '../inc/fsp.h'                                                    
C     include '../inc/nfm.h'                                                    
C     include '../inc/nrp.h'                                                    
C     include '../inc/nsp.h'                                                    
C     include '../inc/rsb.h'                                                    
C     include '../inc/un.h'                                                     
C     include '../inc/eqttt.h'                                                  
c                                                                               
      character*80 ubbuf(50),uline                                              
      character*24 udf                                                          
      character*16 usnam(21),unam16                                             
c                                                                               
      data usp /'species '/                                                     
      data ufnam /'data0p  '/                                                   
      data uendit /'endit.  '/                                                  
      data udf /'*   data derived from   '/                                     
      data uterm /'+-------'/                                                   
      data uele /'elements'/, uaqu /'aqueous '/                                 
      data uh2o,uo2g /'H2O     ','O2(G)   '/                                    
      data uh2aq,uo2aq /'H2(AQ)  ','O2(AQ)  '/                                  
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      rewind ndat0s                                                             
c                                                                               
c     read first part of data0 - ublk1 data                                     
c                                                                               
      nublk1 = 0                                                                
c                                                                               
  200 nublk1=nublk1+1                                                           
      read (ndat0s,1030) ublk1(nublk1)                                          
 1030 format(a80)                                                               
      if (ublk1(nublk1)(1:8) .ne. uele(1:8)) go to 200                          
c                                                                               
c     read element data                                                         
c                                                                               
      do 205 k=1,nct                                                            
      read (ndat0s,1030) uel(k)                                                 
  205 continue                                                                  
c                                                                               
c     read third part of data - ublk3                                           
c                                                                               
      nublk3 = 0                                                                
c                                                                               
  210 nublk3 = nublk3+1                                                         
      read (ndat0s,1030) ublk3(nublk3)                                          
      if (ublk3(nublk3)(1:8) .ne. uaqu(1:8)) go to 210                          
c                                                                               
c     read basis species blocks - ubasis                                        
c                                                                               
      nubas = 0                                                                 
      do 215 k=1,nsq                                                            
  212 nubas = nubas+1                                                           
      read (ndat0s,1030) ubasis(nubas)                                          
      if (ubasis(nubas)(1:8) .ne. uterm(1:8)) go to 212                         
  215 continue                                                                  
c                                                                               
c     now have data0 parameters from line 1 to end of auxiliary basis           
c                                                                               
      do 301 k=1,nsq                                                            
      ixred(k) = 0                                                              
  301 continue                                                                  
c                                                                               
      nrsb = 0                                                                  
      nnsp = 0                                                                  
      nrsp = 0                                                                  
      ifl = 1                                                                   
c                             *----------------------------------------         
c                             * strict basis species loop                       
c                             *----------------------------------------         
      do 320 ns=1,nsb                                                           
c                                                                               
c     find terminator                                                           
c                                                                               
      call fblb(ubasis,ifl,itl)                                                 
c                                                                               
c     read name from species block                                              
c                                                                               
      read (ubasis(ifl),1120) unam16                                            
 1120 format(a12)                                                               
      if (unam16(1:8) .ne. uh2o(1:8)) go to 310                                 
c                                                                               
c     put name into restricted basis set                                        
c                                                                               
  303 call adnam(unam16,ursb,nsqmax,nrsb,iflag)                                 
      ixred(ns) = 1                                                             
      if (iflag .ne. 0) then                                                    
        write (nttyo,1122)                                                      
        write (nout,1122)                                                       
        stop                                                                    
      else                                                                      
        go to 319                                                               
      endif                                                                     
c                                                                               
  310 if (unam16(1:8) .eq. uo2g(1:8)) go to 303                                 
c                                                                               
c     search for species name in dpt0 list                                      
c                                                                               
      call srchn2(unam16,ussp,nssp,inx)                                         
      if (inx .gt. 0) go to 303                                                 
c                                                                               
c     not found                                                                 
c                                                                               
      call adnam(unam16,unsp,nstmax,nnsp,iflag)                                 
      if (iflag .lt. 0) then                                                    
        write (nout,1124)                                                       
        write (nttyo,1124)                                                      
        stop                                                                    
      endif                                                                     
c                                                                               
c     up for next block                                                         
c                                                                               
  319 ifl = itl+1                                                               
c                                                                               
  320 continue                                                                  
c                                                                               
c - - - - - - - - - - end strict basis species loop - - - - - - - - - -         
c     write(nout,1310)                                                          
c1310 format(/,2x,'Initial restricted basis')                                   
c     write(nout,1312) nrsb                                                     
c1312 format(5x,i4,' species')                                                  
c     write(nout,1314) (ursb(i),i=1,nrsb)                                       
c1314 format((5x,3(2x,a16)))                                                    
c                                                                               
c     set new nct                                                               
c                                                                               
      nctnew = nrsb-1                                                           
c                                                                               
c                             *----------------------------------------         
c                             * auxiliary basis species loop                    
c                             *----------------------------------------         
c                                                                               
      do 370 ns=nsb1,nsq                                                        
c                                                                               
c     find terminator                                                           
c                                                                               
      call fblb(ubasis,ifl,itl)                                                 
c                                                                               
c     read species name                                                         
c                                                                               
      read (ubasis(ifl),1120) unam16                                            
      if ((unam16(1:8) .eq. uh2aq(1:8)) .or. (unam16(1:8) .eq.                  
     $     uo2aq(1:8))) go to 337                                               
c                                                                               
c     search for name in dpt0 set                                               
c                                                                               
      call srchn2(unam16,ussp,nssp,inx)                                         
      if (inx .le. 0) then                                                      
c                                                                               
c       not found                                                               
c                                                                               
        call adnam(unam16,unsp,nstmax,nnsp,iflag)                               
c                                                                               
        if (iflag .lt. 0) then                                                  
          write (nout,1124)                                                     
          write (nttyo,1124)                                                    
 1124     format(' * error- unsp species array overflow')                       
          stop                                                                  
        else                                                                    
          go to 369                                                             
        endif                                                                   
      endif                                                                     
c                                                                               
c     search for 'n species ... ' line                                          
c                                                                               
  337 continue                                                                  
      ilx = ifl+1                                                               
  338 ilx = ilx+1                                                               
      if (ilx .le. itl) go to 350                                               
c                                                                               
c     reaction species not found                                                
c                                                                               
  344 call adnam(unam16,ursp,npxpar,nrsp,iflag)                                 
      if (iflag .lt. 0) then                                                    
        write (nout,1132)                                                       
        write (nttyo,1132)                                                      
        stop                                                                    
      else                                                                      
        go to 369                                                               
      endif                                                                     
c                                                                               
  350 read (ubasis(ilx),1134) udum                                              
 1134 format(7x,a8)                                                             
      if (udum(1:8) .ne. usp(1:8)) go to 338                                    
c                                                                               
      read (ubasis(ilx),1135) ndrs                                              
 1135 format(4x,i2)                                                             
c                                                                               
c     found 'n species ... ' line                                               
c     read species names                                                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             PROGRAM CHANGES GIVEN ON                                          
C             VAX CRIB SHEET DATED 6/7/88                                       
C                                                                               
c      read (ubasis(ilx+1),1136) (usnam(n),n=1,ndrs)                            
C                                                                               
C                                                                               
      IND=ILX+1                                                                 
      ISX=0                                                                     
      DO 352 K=1,7                                                              
      read (ubasis(IND),1136) (usnam(ISX+N),N=1,3)                              
      IND=IND+1                                                                 
      ISX=ISX+3                                                                 
      IF(ISX.GE.NDRS) GO TO 353                                                 
  352 CONTINUE                                                                  
  353 CONTINUE                                                                  
 1136 format((5x,3(8x,a12,2x)))                                                 
C  ***********************************************                              
c                                                                               
c     search for reaction species names in restricted basis                     
c                                                                               
      do 355 k=2,ndrs                                                           
      call srchn2(usnam(k),ursb,nrsb,inx)                                       
      if (inx .lt. 0) go to 344                                                 
  355 continue                                                                  
c                                                                               
c     all reaction species found - add species to restricted basis              
c                                                                               
      call adnam(unam16,ursb,nsqmax,nrsb,iflag)                                 
      ixred(ns) = 1                                                             
      if (iflag .lt. 0) then                                                    
        write (nout,1122)                                                       
        write (nttyo,1122)                                                      
 1122   format(' * error- restricted basis species array overflow')             
        stop                                                                    
      endif                                                                     
c                                                                               
  369 ifl = itl+1                                                               
c                                                                               
  370 continue                                                                  
c                                                                               
c     write (nout,1524)                                                         
c1524 format(//,5x,'restricted basis',/)                                        
c     write (nout,1522) (ursb(k),k=1,nrsb)                                      
c                                                                               
c     write (nout,1526)                                                         
c     write (nout,1528) (ixred(i),i=1,nsq)                                      
c1526 format(//,5x,'ixred array',/)                                             
c1528 format((2x,10i5))                                                         
c                                                                               
c - - - - - - - - - - end of auxiliary basis species loop - - - - - - -         
c     write(nout,1316)                                                          
c1316 format(/,2x,'Final restricted basis')                                     
c     write(nout,1312) nrsb                                                     
c     write(nout,1314) (ursb(i),i=1,nrsb)                                       
c                                                                               
c     set new nsq                                                               
c                                                                               
      nsqnew = nrsb                                                             
c                                                                               
c     now have restricted basis data - write front end of data0p                
c                                                                               
      rewind ndatp0                                                             
c                                                                               
c     'data0' and 'data0p' lines                                                
c                                                                               
      ublk1(1)(20:27) = ufnam                                                   
      write (ndatp0,1030) ublk1(1)                                              
c                                                                               
c     nct, nsq line                                                             
c                                                                               
      write (ndatp0,1220) nctnew,nsqnew                                         
 1220 format(2i5)                                                               
c                                                                               
c     title lines                                                               
c                                                                               
      do 510 k=3,nublk1                                                         
      write (ndatp0,1030) ublk1(k)                                              
  510 continue                                                                  
c                                                                               
c     element data - second block                                               
c                                                                               
      do 515 k=1,nct                                                            
      if (ixred(k) .ne. 0) write (ndatp0,1030) uel(k)                           
  515 continue                                                                  
c                                                                               
c     third block                                                               
c                                                                               
      do 520 k=1,nublk3                                                         
      write (ndatp0,1030) ublk3(k)                                              
  520 continue                                                                  
c                                                                               
c     basis species blocks                                                      
c                                                                               
      ifl = 1                                                                   
c                                                                               
      do 525 k=1,nsq                                                            
c                                                                               
c     find block terminator                                                     
c                                                                               
      call fblb(ubasis,ifl,itl)                                                 
      if (ixred(k) .ne. 0) then                                                 
c                                                                               
c       write this block                                                        
c                                                                               
        do 522 j=ifl,itl                                                        
        write (ndatp0,1030) ubasis(j)                                           
  522   continue                                                                
      endif                                                                     
      ifl = itl+1                                                               
c                                                                               
  525 continue                                                                  
c                                                                               
c                            *-----------------------------------------         
c                            * non-basis aqueous species                        
c                            *-----------------------------------------         
c                                                                               
  400 iln = 0                                                                   
c                                                                               
c     read a block                                                              
c                                                                               
  402 iln = iln+1                                                               
      if (iln .gt. 50) then                                                     
        write (nout,2006)                                                       
        write(nttyo,2006)                                                       
 2006     format(' * ubbuf overflow - no terminator found')                     
        stop                                                                    
      endif                                                                     
c                                                                               
      read (ndat0s,1030) ubbuf(iln)                                             
      ufwd = ubbuf(iln)(1:8)                                                    
      if (ufwd(1:8) .eq. uendit(1:8)) go to 450                                 
      if (ufwd(1:8) .ne. uterm(1:8)) go to 402                                  
c                                                                               
c     get species name                                                          
c                                                                               
      read (ubbuf(1),1052) unam16                                               
 1052 format(a12)                                                               
c                                                                               
c     search for name in dpt0 set                                               
c                                                                               
      call srchn2(unam16,ussp,nssp,inx)                                         
      if (inx .le. 0) then                                                      
c                                                                               
c       not found                                                               
c                                                                               
        call adnam(unam16,unsp,nstmax,nnsp,iflag)                               
        if (iflag .lt. 0) then                                                  
          write (nout,1124)                                                     
          write (nttyo,1124)                                                    
          stop                                                                  
        else                                                                    
c                                                                               
c         read next block                                                       
c                                                                               
          go to 400                                                             
        endif                                                                   
      endif                                                                     
c                                                                               
c     search for 'n species ... ' line                                          
c                                                                               
      ilx = 1                                                                   
  410 ilx = ilx+1                                                               
      if (ilx .gt. iln) then                                                    
c                                                                               
c       add to reaction species not found                                       
c                                                                               
        call adnam(unam16,ursp,npxpar,nrsp,iflag)                               
        if (iflag .lt. 0) then                                                  
          write (nout,1132)                                                     
          write (nttyo,1132)                                                    
 1132     format(' * error- ursp reaction species array overflow')              
          stop                                                                  
        else                                                                    
          go to 400                                                             
        endif                                                                   
      endif                                                                     
c                                                                               
      read (ubbuf(ilx),1140) udum                                               
 1140 format(7x,a8)                                                             
      if (udum(1:8) .ne. usp(1:8)) go to 410                                    
c                                                                               
      read (ubbuf(ilx),1142) ndrs                                               
 1142 format(4x,i2)                                                             
c                                                                               
c     read species names                                                        
c                                                                               
      ind = ilx+1                                                               
      isx = o                                                                   
c                                                                               
      do 415 k=1,7                                                              
      read (ubbuf(ind),1136) (usnam(isx+n),n=1,3)                               
      ind = ind+1                                                               
      isx = isx+3                                                               
      if (isx .ge. ndrs) go to 417                                              
  415 continue                                                                  
  417 continue                                                                  
c                                                                               
c     search for reaction species names in restricted basis                     
c                                                                               
      do 420 k=2,ndrs                                                           
      call srchn2(usnam(k),ursb,nrsb,inx)                                       
      if (inx .le. 0) then                                                      
c                                                                               
c       not found - skip this block                                             
c                                                                               
        call adnam(unam16,ursp,npxpar,nrsp,iflag)                               
        if (iflag .lt. 0) then                                                  
          write (nout,1132)                                                     
          write (nttyo,1132)                                                    
          stop                                                                  
        else                                                                    
          go to 400                                                             
        endif                                                                   
      endif                                                                     
  420 continue                                                                  
c                                                                               
c     found all reaction species - write block                                  
c                                                                               
      do 425 k=1,iln                                                            
      write (ndatp0,1030) ubbuf(k)                                              
  425 continue                                                                  
      go to 400                                                                 
c                                                                               
  450 write (ndatp0,1060) uendit,uendit                                         
 1060 format(a8,50x,a8)                                                         
c                                                                               
c - - - - - - - - - -end non-basis aq species loop - - - - - - - - - - -        
c                                                                               
c                             *----------------------------------------         
c                             * mineral and gas species                         
c                             *----------------------------------------         
c                                                                               
      nmn = 0                                                                   
c                                                                               
      do 140 ii=1,2                                                             
      read (ndat0s,1022) uline                                                  
      write (ndatp0,1022) uline                                                 
 1022 format(a80)                                                               
c                                                                               
c     read a block                                                              
c                                                                               
   95 iln = 0                                                                   
c                                                                               
  100 iln = iln+1                                                               
      if (iln .gt. 50) then                                                     
        write (nout,2006)                                                       
        write(nttyo,2006)                                                       
        stop                                                                    
      endif                                                                     
      read (ndat0s,1030) ubbuf(iln)                                             
      ufwd = ubbuf(iln)(1:8)                                                    
      if (ufwd(1:8) .eq. uendit(1:8)) go to 135                                 
      if (ufwd(1:8) .ne. uterm(1:8)) go to 100                                  
c                                                                               
c     search for line with ' n species in reaction'                             
c                                                                               
      ilx = 1                                                                   
  110 ilx = ilx+1                                                               
c                                                                               
c     note pseudo block if with external entry                                  
      if (ilx .le. iln) go to 112                                               
c                                                                               
c       add to not found minerals                                               
c                                                                               
  102   nmn = nmn+1                                                             
        read (ubbuf(1),1040) umspn(nmn)                                         
 1040   format(a18)                                                             
        go to 95                                                                
  112 continue                                                                  
c                                                                               
      read (ubbuf(ilx),1032) udum                                               
 1032 format(7x,a8)                                                             
      if (udum(1:8) .ne. usp(1:8)) go to 110                                    
c                                                                               
c     read number of species in reaction                                        
c                                                                               
      read (ubbuf(ilx),1034) ndrs                                               
 1034 format(4x,i2)                                                             
c                                                                               
c     read species names                                                        
c                                                                               
C  **********************************************                               
C                                                                               
C                                                                               
C            CHANGES GIVEN IN VAX CRIB SHEET                                    
C            DATED 6/7/88                                                       
C                                                                               
C                                                                               
C      read (ubbuf(ilx+1),1036) (usnam(n),n=1,ndrs)                             
C                                                                               
C                                                                               
      IND=ILX+1                                                                 
      ISX=0                                                                     
      DO 115 K=1,7                                                              
      read (UBBUF(IND),1036) (usnam(ISX+N),N=1,3)                               
      IND=IND+1                                                                 
      ISX=ISX+3                                                                 
      IF(ISX.GE.NDRS) GO TO 116                                                 
  115 CONTINUE                                                                  
  116 CONTINUE                                                                  
 1036 format((5x,3(8x,a12,2x)))                                                 
C  ***********************************************                              
c **                                                                            
c     write (nout,1110) ndrs,(ubbuf(i,1),i=1,2)                                 
c1110 format(/,2x,i3,2x,2a10)                                                   
c     write (nout,1112) ((usnam(i,n),i=1,2),n=1,ndrs)                           
c1112 format((8x,3(2x,2a6)))                                                    
c **                                                                            
c                                                                               
c     search for species names - (omit first name)                              
c                                                                               
      do 120 k=2,ndrs                                                           
      call srchn2(usnam(k),ursb,nrsb,inx)                                       
c                                                                               
c     note this goes back to a pseudo block if                                  
c                                                                               
      if (inx .le. 0) go to 102                                                 
  120 continue                                                                  
c                                                                               
c     all species in reaction found                                             
c                                                                               
      do 130 k=1,iln                                                            
      write (ndatp0,1030) ubbuf(k)                                              
  130 continue                                                                  
c                                                                               
      go to 95                                                                  
c                                                                               
c     ref line 445                                                              
c                                                                               
  135 continue                                                                  
      do 137 k=1,iln                                                            
      write (ndatp0,1030) ubbuf(k)                                              
  137 continue                                                                  
c                                                                               
  140 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * read and write the rest of data0                
c                             *----------------------------------------         
c                                                                               
   75 read (ndat0s,1022) uline                                                  
      write (ndatp0,1022) uline                                                 
      if (uline(1:8) .ne. uendit(1:8)) go to 75                                 
c                                                                               
      end                                                                       
c bldsp    last revised 01/28/87 by tjw                                         
c*bldsp f77 rewrite 01/28/87 by tjw                                             
      subroutine bldsp                                                          
c                                                                               
c     make a pass through the array ussrs (nslt entries)                        
c      (derived from dpt0, observable components)                               
c                                                                               
c     build array ussp (nssp entries)                                           
c       containing species names corresponding to the found arrays              
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/fa.h'                                                     
C     include '../inc/fsp.h'                                                    
C     include '../inc/un.h'                                                     
c                                                                               
      character*24 un1,un2                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     initialize ussp - put first two species names in                          
c                                                                               
      ussp(1) = ussrs(1,1)                                                      
      ussp(2) = ussrs(2,1)                                                      
      ns = 2                                                                    
c                                                                               
      do 30 k=2,nslt                                                            
c                                                                               
c     first and second species names                                            
c                                                                               
      un1 = ussrs(1,k)                                                          
      un2 = ussrs(2,k)                                                          
c                                                                               
c     search for names                                                          
c                                                                               
      jns = ns                                                                  
      do 10 j=1,jns                                                             
      if (un1(1:12) .eq. ussp(j)(1:12)) go to 14                                
   10 continue                                                                  
c                                                                               
c     first name not in ussp - put it in                                        
c                                                                               
      ns = ns+1                                                                 
      ussp(ns) = un1                                                            
c                                                                               
c     search for second name                                                    
c                                                                               
   14 do 20 j=1,jns                                                             
      if (un2(1:12) .eq. ussp(j)(1:12)) go to 30                                
   20 continue                                                                  
c                                                                               
c     second name not in ussp - put it in                                       
c                                                                               
      ns = ns+1                                                                 
      ussp(ns) = un2                                                            
c                                                                               
   30 continue                                                                  
c                                                                               
      nssp = ns                                                                 
      end                                                                       
c elimy    last revised 01/28/87 by tjw                                         
c*elimy f77 rewrite 01/28/87 by tjw                                             
      subroutine elimy(ns)                                                      
c                                                                               
c     subroutine elimy eliminates auxiliary basis variables                     
c     from the basis set of aqueous species used to write the                   
c     ns-th reaction.  this puts the reaction in the short format.              
c     the log k grid and interpolating polynomials are correspondingly          
c     changed.                                                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ab.h'                                                     
C     include '../inc/ee.h'                                                     
C     include '../inc/hh.h'                                                     
C     include '../inc/jj.h'                                                     
C     include '../inc/re.h'                                                     
C     include '../inc/eqtti.h'                                                  
C     include '../inc/eqttt.h'                                                  
C     include '../inc/uu.h'                                                     
C     include '../inc/un.h'                                                     
C     include '../inc/xx.h'                                                     
c                                                                               
      dimension xdum(8)                                                         
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (ns.le.nsq) then                                                       
        qstop=.false.                                                           
        do 110 nse=nsb1,nsq                                                     
        if (cdrs(nse,ns).ne.0.) then                                            
          write (noutpt,105) uspec(nse)(1:12),uspec(ns)(1:12)                   
          write (nslist,105) uspec(nse)(1:12),uspec(ns)(1:12)                   
  105     format(/,1x,'----- aux. basis species ',a12,                          
     $    ' illegally appears ',/10x,'in the reaction for ',                    
     $    a12,' -----')                                                         
          qstop=.true.                                                          
        endif                                                                   
  110   continue                                                                
        if (qstop) stop                                                         
        cdrs(nsb1,ns)=cdrs(nsq1,ns)                                             
        cdrs(nsq1,ns)=0.                                                        
        go to 999                                                               
      endif                                                                     
c                                                                               
      do 120 i=1,8                                                              
      xdum(i)=xlks(i,ns)                                                        
  120 continue                                                                  
      adum1=ars(1,ns)                                                           
      adum2=ars(6,ns)                                                           
c                                                                               
      do 40 nse=nsb1,nsq                                                        
      if (cdrs(nse,ns).eq.0.) go to 40                                          
      if (nse.ne.nsb1) then                                                     
        cdrs(nse,nse)=cdrs(nsb1,nse)                                            
        cdrs(nsb1,nse)=0.                                                       
      endif                                                                     
      f=cdrs(nse,ns)/cdrs(nse,nse)                                              
c                                                                               
      do 20 nss=1,nsb                                                           
      cdrs(nss,ns)=cdrs(nss,ns)-(f*cdrs(nss,nse))                               
   20 continue                                                                  
c                                                                               
      do 30 i=1,8                                                               
      if (xdum(i).lt.500.) then                                                 
        xdum1=xlks(i,nse)                                                       
        if (xdum1.ge.500.) then                                                 
          xlks(i,ns)=500.                                                       
          xdum(i)=500.                                                          
        else                                                                    
          xlks(i,ns)=xlks(i,ns)-f*xdum1                                         
        endif                                                                   
      endif                                                                     
   30 continue                                                                  
c                                                                               
      if (adum1.lt.500.) then                                                   
        if (ars(1,nse).ge.500.) then                                            
          ars(1,ns)=500.                                                        
          adum1=500.                                                            
          do 32 i=2,5                                                           
          ars(i,ns)=0.                                                          
   32     continue                                                              
        else                                                                    
          do 37 i=1,5                                                           
          ars(i,ns)=ars(i,ns)-f*ars(i,nse)                                      
   37     continue                                                              
        endif                                                                   
      endif                                                                     
c                                                                               
      if (adum2.lt.500.) then                                                   
        if (ars(6,nse).ge.500.) then                                            
          ars(6,ns)=500.                                                        
          adum2=500.                                                            
          do 42 i=7,10                                                          
          ars(i,ns)=0.                                                          
   42     continue                                                              
        else                                                                    
          do 47 i=6,10                                                          
          ars(i,ns)=ars(i,ns)-f*ars(i,nse)                                      
   47     continue                                                              
        endif                                                                   
      endif                                                                     
c                                                                               
      if (nse.ne.nsb1) then                                                     
        cdrs(nsb1,nse)=cdrs(nse,nse)                                            
        cdrs(nse,nse)=0.                                                        
      endif                                                                     
c                                                                               
   40 continue                                                                  
c                                                                               
      cdrs(nsb1,ns)=cdrs(nsq1,ns)                                               
      cdrs(nsq1,ns)=0.                                                          
c                                                                               
  999 continue                                                                  
      end                                                                       
c eqpt     3245R71, last revised 08/02/87 by rlh                                
c*eqpt f77 rewrite 01/29/87 by tjw                                              
c                                                                               
c   Be sure to put the correct release and stage numbers in parameters          
c   urelno and ustage.                                                          
c                                                                               
c     This is the main program of the EQPT computer code, part of the           
c     EQ3/6 software package.  This computer code is copyrighted as             
c     follows-                                                                  
c                                                                               
c     Copyright (c) 1987 The Regents of the University of California,           
c     Lawrence Livermore National Laboratory. All rights reserved.              
c                                                                               
c          The person responsible for this code is                              
c                                                                               
c               Thomas J. Wolery                                                
c               L-219                                                           
c               Lawrence Livermore National Laboratory                          
c               P.O. Box 808                                                    
c               Livermore, CA  94550                                            
c               (415) 422-5789                                                  
c               FTS 532-5789                                                    
c                                                                               
      program eqpt                                                              
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     modification history has been moved to eqpt.his                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     the eqpt code reads primary data files data0 and dpt0,                    
c     and generates data file dpt1 which supports                               
c     the eq3nr and eq6 modeling codes.                                         
c     in addition, eqpt constructs data file data0p, which                      
c     parallels file data0, but contains only aqueous species                   
c     appearing on data file dpt0.  see routine bld0p                           
c     for details.                                                              
c                                                                               
c     the eqtld routine reads the file data0p and generates                     
c     from it parts of the three secondary data files which directly            
c     support the eq3nr and eq6 geochemical modeling codes (data1               
c     supports eq3nr, data2 and data3 support eq6).  eqtl checks                
c     the consistency of composition, charge, and reaction data                 
c     by calculating mass and charge balances for the reactions                 
c     written on the data file.  it also fits interpolating                     
c     polynomials to temperature-dependent data, and writes                     
c     the reactions in the short format used by eq6.                            
c                                                                               
c     eqpt performs the following sequence of steps -                           
c                                                                               
c     reads primary input file data0                                            
c      gets information on elements, and aqueous species.                       
c                                                                               
c     reads input file dpt0                                                     
c      part 1 - single salt parameters                                          
c             constructs lambda and mu arrays, scales beta and                  
c             c phi values, writes one block to output file dpt1.               
c      part 2 - builds file data0p containing only parameters from              
c             file data0 for species which appear on file dpt0                  
c             reads data0p to construct file data3, and parts                   
c             of data1, data2.                                                  
c      part 3 - mixture term parameters                                         
c             constructs the theta and psi sets, writes two data                
c             blocks to output file dpt1, writes four data                      
c             blocks to output files data1, data2, and dpt2.                    
c                                                                               
c     eqpt structure and function -                                             
c                                                                               
c     eqpt consists of                                                          
c                                                                               
c      a driver program (main)                                                  
c        reads data0 for element data, calls routines to                        
c        read and process, and output.                                          
c                                                                               
c      primary routines -                                                       
c                                                                               
c        1) pcraq0                                                              
c            reads data0, gets aqueous species data                             
c                                                                               
c        2) pdpz2                                                               
c            reads dpt0, processes part 1 - single salt parameters              
c            scales values using multipliers, builds lambda and mu              
c            arrays for later output, writes single salt parameter              
c            block to output file dpt1.                                         
c                                                                               
c        3) bld0p                                                               
c            reads data0, writes data0p                                         
c                                                                               
c        4) eqtld  (formerly program eqtl)                                      
c            reads data0p, builds file data3, and first parts                   
c            of files data1, data2                                              
c                                                                               
c            supporting routines -                                              
c             a) pcraq  reads data0p, gets aqueous species data, checks         
c                       for many types of errors                                
c             b) pcrsg  reads data0p, gets mineral and gas species              
c                       data, checks for many types of errors                   
c             c) intrp  finds interpolating polynomial coefficients             
c                       with temperature as independent variable                
c             d) funcy  computes 4th degree polynomial                          
c             e) gridpy writes temperatures, grid values, computes              
c                       errors                                                  
c             f) rxnchk checks reaction for mass and charge balance             
c             g) elimy  eliminates auxiliary basis variables                    
c             h) preacy outputs a reaction                                      
c                                                                               
c        5) pdpz3                                                               
c            reads dpt0, processes part 2 - mixture term parameters             
c            builds psi and theta array sets, writes block 2                    
c            (theta set) and block 3 (psi set) to output file dpt1.             
c            writes two lambda blocks and two mu blocks to output               
c            files data1, data2, and dpt2.                                      
c                                                                               
c       supporting routines -                                                   
c                                                                               
c        6) srch22  searches for species name pairs                             
c        7) bldsp   builds species array from ussrs array                       
c        8) adspn   adds species name to usspn array                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ab.h'                                                     
C     include '../inc/av.h'                                                     
C     include '../inc/dat.h'                                                    
C     include '../inc/dbug.h'                                                   
C     include '../inc/dv.h'                                                     
C     include '../inc/ee.h'                                                     
C     include '../inc/fa.h'                                                     
C     include '../inc/fsp.h'                                                    
C     include '../inc/hh.h'                                                     
C     include '../inc/hhn.h'                                                    
C     include '../inc/key.h'                                                    
C     include '../inc/jj.h'                                                     
C     include '../inc/jv.h'                                                     
C     include '../inc/mu.h'                                                     
C     include '../inc/nfm.h'                                                    
C     include '../inc/nfsp.h'                                                   
C     include '../inc/nrp.h'                                                    
C     include '../inc/nsp.h'                                                    
C     include '../inc/re.h'                                                     
C     include '../inc/rsb.h'                                                    
C     include '../inc/snm.h'                                                    
C     include '../inc/eqtti.h'                                                  
C     include '../inc/eqttt.h'                                                  
C     include '../inc/un.h'                                                     
C     include '../inc/uu.h'                                                     
C     include '../inc/eqtver.h'                                                 
C     include '../inc/xx.h'                                                     
c                                                                               
      character*80 uttl(5)                                                      
      character*1 uyn                                                           
cDAS      real*4 timear(2)
c                                                                                                                                         
      data nttyo /6/
      data nttyi /5/                                                            
c                                                                               
      data uknp,ukp,ukh /'stfipc  ','stpitz  ','sthkf   '/                      
      data udata0 /'data0   '/                                                  
      data uaq /'aqueous '/                                                     
      data ublank/'        '/                                                   
      data udpt2 /'dpt2    '/                                                   
c                                                                               
c     version identification                                                    
c       ucode = code name                                                       
c       urelno = release number                                                 
c       ustage = stage number                                                   
c                                                                               
c     data ucode/'eqpt    '/,urelno/'3245    '/,ustage/'R71     '/              
c                                                                               
c     qpflag = true/false  pitzer data/none                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     get time and date                                                         
c                                                                               
cDAS      call timdat(udate0,utime0)                                                
c                                                                               
c     open the output file                                                      
c                                                                               
      call openou('output','formatted',noutpt)                                  
c                                                                               
c     print version numbers and copyright notice                                
c                                                                               
      write (noutpt,1000) urelno,ustage                                         
      write (nttyo,1000) urelno,ustage                                          
c                                                                               
 1000 format(/' EQPT, version ',a4,a4,//                                        
     $ '   Copyright (c) 1987 The Regents of the University of',                
     $ ' California,',/'   Lawrence Livermore National Laboratory. All',        
     $ ' rights reserved.')                                                     
c                                                                               
c     get eqlib release and stage number identification                         
c                                                                               
      call eqlib(ueqlrn,ueqlst,noutpt,nttyo,qtrue,qfalse)                       
c                                                                               
      write (noutpt,1003) utime0,udate0                                         
      write (nttyo,1003) utime0,udate0                                          
 1003 format(' Run',2(2x,a8),//)                                                
c                                                                               
      write (nttyo,1011)                                                        
 1011 format(' Do you wish to include Pitzer data? (y/n)')                      
      read (nttyi,1013) uyn                                                     
 1013 format(a1)                                                                
      write (nttyo,1015) uyn                                                    
 1015 format(' answer = ',a1)                                                   
c                                                                               
c              pitzer file                                                      
      if (uyn .eq. 'y'.or.uyn. eq. 'Y') then                                    
        qpflag = .true.                                                         
        ukey = ukp                                                              
      else                                                                      
        write(nttyo,1050)                                                       
 1050   format(' Do you wish to include hkf (part iv) data? (y/n)')             
        read(nttyi,1013) uyn                                                    
        write(nttyo,1015) uyn                                                   
c                                                                               
c              hkf file                                                         
        if (uyn .eq. 'y'.or. uyn.eq.'Y') then                                   
          ukey = ukh                                                            
          qpflag = .false.                                                      
          inquire(file='hkfdat',exist=qex)                                      
          if (.not. qex) then                                                   
            write(nttyo,1051)                                                   
            write(noutpt,1051)                                                  
 1051       format(' no hkf data file -- must be called hkfdat')                
            stop                                                                
          else                                                                  
            call openin('hkfdat','formatted',ndhkf)                             
          endif                                                                 
c                                                                               
c              standard file only                                               
        else                                                                    
          ukey = uknp                                                           
          qpflag = .false.                                                      
        endif                                                                   
      endif                                                                     
c                                                                               
c     set line length for stripped files                                        
c                                                                               
      iline = 80                                                                
c                                                                               
c     make stripped data files if necessary or desired.                         
c                                                                               
      qex = .false.                                                             
      inquire(file='data0s',exist=qex)                                          
      if (qex) then                                                             
        write (nttyo,2100)                                                      
 2100   format(' The stripped data file (data0s) exists-',                      
     $  /'   Do you wish to use it?  (y/n)')                                    
        read (nttyi,1013) uyn                                                   
        write (nttyo,1015) uyn                                                  
        if (uyn .eq. 'y'.or.uyn.eq.'Y') then                                    
          call openin('data0s','formatted',ndat0s)                              
          go to 2102                                                            
        endif                                                                   
      endif                                                                     
      call openin('data0','formatted',ndata0)                                   
      call openou('data0s','formatted',ndat0s)                                  
      call stripl(ndata0,ndat0s,iline)                                          
      close(ndata0)                                                             
 2102 continue                                                                  
c                                                                               
      if (qpflag) then                                                          
        inquire(file='dpt0s',exist=qex)                                         
        if (qex) then                                                           
          write (nttyo,2104)                                                    
 2104     format(' The stripped pitzer coefficient data file (dpt0s) ',         
     $    'exists-',/'   Do you wish to use it?  (y/n)')                        
          read (nttyi,1013) uyn                                                 
          write (nttyo,1015) uyn                                                
          if (uyn .eq. 'y'.or.uyn.eq.'Y') then                                  
            call openin('dpt0s','formatted',ndpt0s)                             
            go to 2106                                                          
          endif                                                                 
        endif                                                                   
        call openin('dpt0','formatted',ndpt0)                                   
        call openou('dpt0s','formatted',ndpt0s)                                 
        call stripl(ndpt0,ndpt0s,iline)                                         
        close(ndpt0)                                                            
 2106 continue                                                                  
      endif                                                                     
c                                                                               
c     nda0 is the unit number for the effective 'data0' file.                   
c     under the non-pitzer option, this is data0s.  under the                   
c     pitzer option, this is data0p, which must be constructed                  
c     from data0s by eliminating all species which are not                      
c     connected to the pitzer model defined on dpt0s.                           
c                                                                               
      if (qpflag) then                                                          
        call openou('data0p','formatted',nda0)                                  
      else                                                                      
        nda0 = ndat0s                                                           
      endif                                                                     
c                                                                               
      call openou('data1','unformatted',ndata1)                                 
      call openou('data2','unformatted',ndata2)                                 
      call openou('data3','unformatted',ndata3)                                 
      if (qpflag) then                                                          
        call openou('dpt1','formatted',ndpt1)                                   
        call openou('dpt2','formatted',ndpt2)                                   
        call openou('out','formatted',nout)                                     
      endif                                                                     
      call openou('slist','formatted',nslist)                                   
      ndbfl3 = noutpt                                                           
c                                                                               
c     qdbug2 = .true./.false.  no debug output/output to dbfil3                 
c                                                                               
      qdbug2 = .false.                                                          
c                                                                               
c     initialize some array dimension values.  see include file                 
c     'parset.h' for values                                                     
c                                                                               
      nsqmax=nsqpar                                                             
      nstmax=nstpar                                                             
      nspt = npxpar                                                             
      nspn = 0                                                                  
c                                                                               
      if(qpflag) write(nout,1029)                                               
 1029 format(/,5x,'Pitzer option',/)                                            
      if(ukey .eq. ukh) write(nout,1031)                                        
 1031 format(/,5x,'HKF part iv option',/)                                       
c                                                                               
      rewind ndat0s                                                             
      if (qpflag) rewind ndpt0s                                                 
      read (ndat0s,1016) udum                                                   
 1016 format(a8)                                                                
      if (udum(1:8) .ne. udata0(1:8)) then                                      
        write (noutpt,1018)                                                     
 1018   format(/1x,'bad file header = ',a8,' on file data0')                    
        write (nttyo,2000)                                                      
 2000   format(' * bad file header on file data0')                              
        stop                                                                    
      endif                                                                     
c                                                                               
      if (qpflag) then                                                          
c                                                                               
c       read and save first 5 lines from dpt0                                   
c                                                                               
        do 10 k=1,5                                                             
        read (ndpt0s,1052) uttl(k)                                              
 1052   format(a80)                                                             
   10   continue                                                                
      else                                                                      
        call eqtld                                                              
        go to 160                                                               
      endif                                                                     
c                                                                               
      read (ndat0s,1022,end=109) nct,nsq                                        
 1022 format(2i5)                                                               
      go to 110                                                                 
c                                                                               
  109 write (nout,1024)                                                         
 1024 format(/1x,'----- primary data file data0 is missing -----')              
      write (nttyo,1024)                                                        
      stop                                                                      
c                                                                               
  110 nsb=nct+1                                                                 
      nsb1=nsb+1                                                                
      nsb2=nsb+2                                                                
      nsq1=nsq+1                                                                
      nsq2=nsq+2                                                                
      write (nout,1026) nct,nctpar,nsq,nsqpar                                   
 1026 format(11x,'no. of elements on the data file = ',i5,/14x,'the',           
     $ ' dimensioned limit = ',i5,/11x,'no. of aqueous species',                
     $ ' in the master set = ',i5,/14x,'the dimensioned limit = ',              
     $ i5,/)                                                                    
      if (nct.gt.nctpar) then                                                   
        write (nttyo,2002)                                                      
 2002   format(' * nct .gt. nctpar')                                            
        stop                                                                    
      endif                                                                     
c                                                                               
      if (nsq.gt.nsqpar) then                                                   
        write (nttyo,2004)                                                      
 2004   format(' * nsq .gt. nsqpar')                                            
        stop                                                                    
      endif                                                                     
c                                                                               
      do 112 i=1,4                                                              
      read (ndat0s,1017) utitld(i)                                              
 1017 format(a80)                                                               
  112 continue                                                                  
c                                                                               
      write (nout,1030) (utitld(i),i=1,4)                                       
 1030 format((a80))                                                             
      write(nout,1030) (uttl(i),i=1,2)                                          
c                                                                               
      read (ndat0s,1016) udum                                                   
c                                                                               
c                             *----------------------------------------         
c                             * get element data                                
c                             *----------------------------------------         
c                                                                               
      write(nout,1059)                                                          
 1059 format(/,'elements on data0')                                             
      write (nout,1060)                                                         
 1060 format(' element',2x,'atomic wt.',/)                                      
c                                                                               
      do  120 nc=1,nct                                                          
      read (ndat0s,1032) uelem(nc),atwt(nc),uoxide,oxfac                        
 1032 format(a8,f10.5,5x,a8,5x,f10.5)                                           
      write (nout,1038) nc,uelem(nc),atwt(nc)                                   
 1038 format(1x,i2,1x,a3,2x,f10.5)                                              
  120 continue                                                                  
c                                                                               
c     skip to 'aqueous species'                                                 
c                                                                               
  130 read (ndat0s,1040) udum                                                   
 1040 format(a8)                                                                
      if (udum .ne. uaq) go to 130                                              
c                                                                               
c                             *----------------------------------------         
c                             * process aqueous species                         
c                             *----------------------------------------         
c                                                                               
      nerr=0                                                                    
c                                                                               
      write(nout,1033)                                                          
 1033 format(//,'data0')                                                        
      call pcraq0(nerr)                                                         
c                                                                               
      if (nerr .gt. 0) then                                                     
        write (nout,1036) nerr                                                  
        write (nttyo,1036) nerr                                                 
 1036   format(/1x,'-----',i4,' fatal errors were found on data0 -----')        
        write (nttyo,1012)                                                      
 1012   format(1x,10('-------'),/,' ***')                                       
        stop                                                                    
      endif                                                                     
c                                                                               
      write (nttyo,1037)                                                        
 1037 format(' completed reading data0')                                        
      write (nout,1012)                                                         
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      write (ndpt1,1020)                                                        
 1020 format('dpt1')                                                            
c                                                                               
c     write header lines from dpt0                                              
c                                                                               
      write (ndpt1,1005)                                                        
 1005 format('*',/,'* data derived from')                                       
c                                                                               
      do 140 k=1,4                                                              
      write (ndpt1,1044) uttl(k)                                                
 1044 format(a80)                                                               
  140 continue                                                                  
c                                                                               
c     e lambda flag                                                             
c                                                                               
      write (ndpt1,1046) uttl(5)                                                
 1046 format('*',/,a80,/,'*')                                                   
c                                                                               
c                             *----------------------------------------         
c                             * part 1 - single salt parameters                 
c                             *----------------------------------------         
c                                                                               
      call pdpz2                                                                
c                                                                               
      write (nttyo,1130)                                                        
 1130 format(' completed dpt0 - single salt parameters')                        
c                                                                               
c     number of entries in *found* arrays is in nslt.                           
c     build ussp species array.                                                 
c                                                                               
      call bldsp                                                                
c                                                                               
      write (nout,1070) nssp                                                    
 1070 format(/,1x,i3,' aqueous species found on both dpt0 and data0',/)         
      nl = 1+(nssp-1)/5                                                         
      do 141 k=1,nl                                                             
      j = 1+5*(k-1)                                                             
      if(j+4 .gt. nssp) then                                                    
        write(nout,1072) j, (ussp(i),i=j,nssp)                                  
      else                                                                      
        write(nout,1072) j,(ussp(i),i=j,j+4)                                    
      endif                                                                     
 1072 format(1x,i4,5(2x,a12))                                                   
  141 continue                                                                  
c                                                                               
c     build data0p file                                                         
c                                                                               
      call bda0p(nda0)                                                          
c                                                                               
      write (nout,1140) nrsb                                                    
 1140 format(//,2x,i3,' aqueous species in the restricted basis',/)             
      nl = 1+(nrsb-1)/5                                                         
      do 142 k=1,nl                                                             
      j = 1+5*(k-1)                                                             
      if(j+4 .gt. nrsb) then                                                    
        write(nout,1142) j,(ursb(i),i=j,nrsb)                                   
      else                                                                      
        write(nout,1142) j,(ursb(i),i=j,j+4)                                    
      endif                                                                     
 1142 format(1x,i4,5(2x,a12))                                                   
  142 continue                                                                  
c                                                                               
      if (nnsp .gt. 0) then                                                     
        write (nout,1144) nnsp                                                  
 1144   format(//,2x,i3,' species on data0, not on dpt0',                       
     $  ' (filtered out)',/)                                                    
        nl = 1+(nnsp-1)/5                                                       
        do 143 k=1,nl                                                           
        j = 1+5*(k-1)                                                           
        if(j+4 .gt. nnsp) then                                                  
          write(nout,1142) j,(unsp(i),i=j,nnsp)                                 
        else                                                                    
          write(nout,1142) j,(unsp(i),i=j,j+4)                                  
        endif                                                                   
  143   continue                                                                
      endif                                                                     
c                                                                               
      if (nrsp .gt. 0) then                                                     
        write (nout,1146) nrsp                                                  
 1146   format(//,2x,i3,' species found on both data0 and dpt0,'                
     $  /10x,'with at least one reaction species not found',                    
     $  ' in the restricted basis',/,15x,'(filtered out)')                      
        nl = 1+(nrsp-1)/5                                                       
        do 146 k=1,nl                                                           
        j = 1+5*(k-1)                                                           
        if(j+4 .gt. nrsp) then                                                  
          write(nout,1142) j,(ursp(i),i=j,nrsp)                                 
        else                                                                    
          write(nout,1142) j,(ursp(i),i=j,j+4)                                  
        endif                                                                   
  146   continue                                                                
      endif                                                                     
c                                                                               
      if (nmn .gt. 0) then                                                      
        write (nout,1080) nmn                                                   
 1080   format(/,1x,i3,' minerals and gases with reaction species',             
     $  ' not found in the restricted basis',/,                                 
     $  10x,'(filtered out)',/)                                                 
        nl = 1+(nmn-1)/3                                                        
        do 145 k=1,nl                                                           
        j = 1+3*(k-1)                                                           
        if(j+2 .gt. nmn) then                                                   
          write(nout,1082) j,(umspn(i),i=j,nmn)                                 
        else                                                                    
          write(nout,1082) j,(umspn(i),i=j,j+2)                                 
        endif                                                                   
 1082   format(1x,i4,3(2x,a18))                                                 
  145   continue                                                                
      endif                                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c                             *----------------------------------------         
c                             * build front ends for files data1, data2         
c                             *  build file data3                               
c                             *----------------------------------------         
c                                                                               
      rewind nda0                                                               
      rewind ndat0s                                                             
c                                                                               
      call eqtld                                                                
c                                                                               
      write (nttyo,1120)                                                        
 1120 format(' completed eqtld - data3, first parts of data1, data2')           
c                                                                               
      write (ndpt2,1021) udpt2                                                  
      write (ndata1) udpt2                                                      
      write (ndata2) udpt2                                                      
 1021 format(a8)                                                                
c                                                                               
      write (ndpt2,1005)                                                        
c                                                                               
      do 144 k=1,4                                                              
      write (ndpt2,1044) uttl(k)                                                
      write (ndata1) uttl(k)                                                    
      write (ndata2) uttl(k)                                                    
  144 continue                                                                  
c                                                                               
c     e lambda flag                                                             
c                                                                               
      write (ndpt2,1046) uttl(5)                                                
      write (ndata1) uttl(5)                                                    
      write (ndata2) uttl(5)                                                    
 1047 format(a80)                                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c                             *----------------------------------------         
c                             * part 2 - mixture term parameters                
c                             *  complete files data1 and data2                 
c                             *----------------------------------------         
c                                                                               
      call pdpz3                                                                
c                                                                               
      write (nttyo,1134)                                                        
 1134 format(' completed dpt0 - mixture term parameters',                       
     $ /'   completed data1 and data2')                                         
c                                                                               
      if (nspn .gt. 0) then                                                     
        write (nout,1074) nspn                                                  
 1074   format(/,1x,i3,' species on dpt0 were not found on data0',/)            
        write (nout,1076) (usspn(k),k=1,nspn)                                   
 1076   format((5(2x,a12)))                                                     
      endif                                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
  160 continue                                                                  
c                                                                               
c              if hkf equations process hkfdat                                  
      if (ukey .eq. ukh) call pchkf                                             
      if (qpflag) then                                                          
        write (noutpt,1132)                                                     
        write (nttyo,1132)                                                      
 1132   format(' Completed EQPT preprocessor using Pitzer data')                
      else                                                                      
        write (noutpt,1136)                                                     
        write (nttyo,1136)                                                      
 1136   format(' Completed EQPT preprocessor, no Pitzer data')                  
c                                                                               
c       get end time and date                                                   
c                                                                               
cDAS        call timdat(udate1,utime1)                                              
c                                                                               
        write (noutpt,1208) utime0,udate0,utime1,udate1                         
        write (nttyo,1208) utime0,udate0,utime1,udate1                          
 1208   format(/,10x,'start time = ',a8,2x,a8,/12x,                             
     $  'end time = ',a8,2x,a8)                                                 
c                                                                               
c       timdum = etime(timear)                                                  
c       write (noutpt,1211) timear                                              
c       write (nttyo,1211) timear                                               
c1211   format(/10x,'user time = ',f10.3,/10x,' cpu time = ',f10.3)             
c                                                                               
      endif                                                                     
c                                                                               
      write (noutpt,2006)                                                       
      write (nttyo,2006)                                                        
 2006 format(/' * normal exit')                                                 
      stop                                                                      
      end                                                                       
c eqtld    last revised 03/03/87 by tjw                                         
c*eqtld f77 rewrite 01/29/87 by tjw                                             
      subroutine eqtld                                                          
c                                                                               
c     The eqtld routine reads the file data0p and generates                     
c     from it parts of the three secondary data files which directly            
c     support the eq3nr and eq6 geochemical modeling codes (data1               
c     supports eq3nr, data2 and data3 support eq6).  Eqtld checks               
c     the consistency of composition, charge, and reaction data                 
c     by calculating mass and charge balances for the reactions                 
c     written on the data file.  It also fits interpolating                     
c     polynomials to temperature-dependent data, and writes                     
c     the reactions in the short format used by eq6.                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ee.h'                                                     
C     include '../inc/hh.h'                                                     
C     include '../inc/hhn.h'                                                    
C     include '../inc/key.h'                                                    
C     include '../inc/re.h'                                                     
C     include '../inc/eqtti.h'                                                  
C     include '../inc/uu.h'                                                     
C     include '../inc/un.h'                                                     
C     include '../inc/xx.h'                                                     
C     include '../inc/eqtver.h'                                                 
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c                  g l o s s a r y                                              
c                                                                               
c          utitld = title description of supporting data file                   
c          uelem = name of element                                              
c          atwt = atomic weight                                                 
c          uspec = name of species                                              
c          cess = moles element per mole species                                
c          mwtss = molecular weight of species                                  
c          ncomp = no. of end-members in a solid solution                       
c          jsol = index specifying solid solution treatment                     
c          xbarlm = limit on mole-fraction of an end-member                     
c          z = charge on an aqueous species                                     
c          titr = titration factor of aqueous species (used to define           
c             the titration alkalinity)                                         
c          azero = debye-huckel parameter                                       
c          cdrs = coefficient of dissociation/dissolution reaction              
c          press = total pressure, in bars                                      
c          adh = debye-huckel a                                                 
c          bdh = debye-huckel b                                                 
c          bdot = extended debye-huckel parameter                               
c          cco2 = power series coefficients for computing g co2 (aq)            
c          ch2o = coefficients for computing act h2o (l)                        
c          xlks = log k for reaction                                            
c          xlkeh = special log k for computing eh and p e-                      
c          xlko2 = log k for o2 dissolution in water                            
c          xlkh2 = log k for h2 dissolution in water                            
c          xlkn2 = log k for n2 dissolution in water                            
c          vol = volume of a species at 298 k and 1.013 bar                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      write (noutpt,1010)                                                       
      write (nslist,1010)                                                       
 1010 format(1x,//////1x,                                                       
     $ 'eeee   qqq   pppp   ttttt',/1x,                                         
     $ 'e     q   q  p   p    t  ',/1x,                                         
     $ 'eeee  q   q  pppp     t  ',/1x,                                         
     $ 'e     q   q  p        t  ',/1x,                                         
     $ 'eeee   qqq   p        t  ',/1x,                                         
     $ '          q              ',///)                                         
c                                                                               
c                             *----------------------------------------         
c                             * process element data, etc.                      
c                             *----------------------------------------         
c                                                                               
      call rdpar                                                                
c                                                                               
      write(nttyo,1030)                                                         
 1030 format(' begin processing data0p')                                        
c                                                                               
c                             *----------------------------------------         
c                             * process compositions and reactions for          
c                             *   aqueous species, minerals, and gases          
c                             *----------------------------------------         
c                                                                               
      nerr=0                                                                    
c                                                                               
c     ilim = 1  aqueous species                                                 
c            2  minerals                                                        
c            3  gases                                                           
c                                                                               
      do  100 ilim=1,3                                                          
      read (nda0,1012) udum,udum2,udum3                                         
      write (ndata1) udum,udum2,udum3                                           
      write (ndata2) udum,udum2,udum3                                           
 1012 format(10a8)                                                              
      write (nslist,1014) udum,udum2,udum3                                      
 1014 format(//,10a8)                                                           
c                                                                               
      if (ilim .eq. 1) then                                                     
c                                                                               
c       aqueous species                                                         
c                                                                               
        call pcraq(nerr)                                                        
        write(nttyo,1032)                                                       
 1032   format(' ')                                                             
      else                                                                      
c                                                                               
c       minerals, gases                                                         
c                                                                               
        call pcrsg(nerr)                                                        
        write(nttyo,1032)                                                       
      endif                                                                     
  100 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * process solid solutions                         
c                             *----------------------------------------         
c                                                                               
      call pcrss(nerr)                                                          
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c      normal exit here                                                         
c                                                                               
      if (nerr.gt.0) then                                                       
        write (noutpt,1016) nerr                                                
        write (nslist,1016) nerr                                                
 1016   format(/,1x,'-----',i4,                                                 
     $  ' fatal errors were found on data0p -----')                             
        write(nttyo,2008)                                                       
 2008   format(' * fatal errors on data0p')                                     
        stop                                                                    
      endif                                                                     
c                                                                               
      end                                                                       
c fblb     last revised 02/03/87 by tjw                                         
c*fblb f77 rewrite 01/29/87 by tjw                                              
      subroutine fblb(ubuf,ibf,itx)                                             
c                                                                               
c     input  ubuf   ascii line buffer, a80                                      
c            ibf    beginning index                                             
c                                                                               
c     output itx    index of terminator line                                    
c                   if not found in 500 lines, itx = -1                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/un.h'                                                     
c                                                                               
      character*80 ubuf(*)                                                      
c                                                                               
      data jlim /500/                                                           
      data uterm /'+-------'/                                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      il = jlim+ibf                                                             
      do 10 itx=ibf,il                                                          
      if (ubuf(itx)(1:8) .eq. uterm(1:8)) go to 999                             
   10 continue                                                                  
c                                                                               
      itx = -1                                                                  
  999 continue                                                                  
      end                                                                       
c funcy    last revised 01/29/87 by tjw                                         
c*funcy f77 rewrite 01/29/87 by tjw                                             
      real*8 function funcy(x,b)                                                
c                                                                               
c     compute a 4th degree polynomial                                           
c                                                                               
c     input                                                                     
c       x   the independent variable                                            
c       b   the array of 10 coefficients                                        
c                                                                               
c     output                                                                    
c       funcy                                                                   
c                                                                               
c     algorithm -                                                               
c       if x .le. 450.                                                          
c          move b(1),...,b(5) to d(1),...,d(5)                                  
c       if x .gt. 450.                                                          
c          move b(6),...,b(10) to d(1),...d(5)                                  
c                                                                               
c       funcy = d(1) + d(2)x + d(3)x**2 + d(4)x**3 + d(5)x**4                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/un.h'                                                     
c                                                                               
      dimension b(10),d(5)                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (x .le. 450.) then                                                     
        do 10 n=1,5                                                             
        d(n) = b(n)                                                             
   10   continue                                                                
      else                                                                      
        do 20 n = 1,5                                                           
        k = n+5                                                                 
        d(n) = b(k)                                                             
   20   continue                                                                
      endif                                                                     
c                                                                               
      funcy = d(5)                                                              
      do 40 n=2,5                                                               
      k = 6-n                                                                   
      funcy = d(k) + x*funcy                                                    
   40 continue                                                                  
c                                                                               
      end                                                                       
c gridpy   last revised 08/02/87 by tjw                                         
c*gridpy f77 rewrite 01/29/87 by tjw                                            
      subroutine gridpy(grida,gridb,rms1,rms2,nfile)                            
c                                                                               
c     input                                                                     
c       grida  real array of 8 values                                           
c       gridb  real array of 9 values, fitted to grid a                         
c       tempc  real array (from common)                                         
c       nfile  unit number to write to                                          
c                                                                               
c       grida and gridb are handled as two sets -                               
c       the first 4 values (temperature 0 - 100)                                
c       and the last 4 values (temperature 150 - 300)                           
c       for each set -                                                          
c         write the temperature, grida value, gridb value, difference           
c           to unit nfile                                                       
c         compute rms                                                           
c           if rms .gt. 0.0001, write rms                                       
c           rms1 is rms for the set 0 - 100                                     
c           rms2 is rms for the set 150 - 300                                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/hh.h'                                                     
C     include '../inc/un.h'                                                     
c                                                                               
      dimension grida(*),gridb(*)                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      write (nfile,100)                                                         
  100 format(/11x,'temp        grid value     fitted value',                    
     $ '     delta',/)                                                          
c                                                                               
c     do first 4 values                                                         
c                                                                               
      rms1=0.                                                                   
      nj=0                                                                      
c                                                                               
      do 5 i=1,4                                                                
      if (grida(i).lt.500. .and. gridb(i).lt.500.) then                         
        diff=gridb(i)-grida(i)                                                  
        rms1=rms1+diff*diff                                                     
        nj=nj+1                                                                 
        write (nfile,105) tempc(i),grida(i),gridb(i),diff                       
  105   format(11x,f5.0,6x,f9.4,6x,f9.4,6x,f9.4)                                
      else                                                                      
        write (nfile,110) tempc(i),gridb(i)                                     
  110   format(11x,f5.0,21x,f9.4)                                               
      endif                                                                     
    5 continue                                                                  
c                                                                               
      if (nj.ge.1) rms1=sqrt(rms1/nj)                                           
      write (nfile,112)                                                         
  112 format(1x)                                                                
c                                                                               
c     do last 4 values                                                          
c                                                                               
      rms2=0.                                                                   
      nj=0                                                                      
c                                                                               
      if (grida(4).lt.500. .and. gridb(9).lt.500.) then                         
        diff=gridb(9)-grida(4)                                                  
        rms2=diff*diff                                                          
        nj=1                                                                    
        write (nfile,105) tempc(4),grida(4),gridb(9),diff                       
      else                                                                      
        write (nfile,110) tempc(4),gridb(9)                                     
      endif                                                                     
c                                                                               
      do 25 i=5,8                                                               
      if (grida(i).lt.500. .and. gridb(i).lt.500.) then                         
        diff=gridb(i)-grida(i)                                                  
        rms2=rms2+diff*diff                                                     
        nj=nj+1                                                                 
        write (nfile,105) tempc(i),grida(i),gridb(i),diff                       
      else                                                                      
        write (nfile,110) tempc(i),gridb(i)                                     
      endif                                                                     
   25 continue                                                                  
c                                                                               
      if (nj.ge.1) rms2=sqrt(rms2/nj)                                           
c                                                                               
      if (rms1.gt.0.0001) then                                                  
        write (nfile,115) rms1                                                  
  115   format(/13x,'rms delta (  0-100 deg c) = ',f9.4)                        
      endif                                                                     
c                                                                               
      if (rms2.gt.0.0001) then                                                  
        write (nfile,117) rms2                                                  
  117   format(13x,'rms delta (100-300 deg c) = ',f9.4)                         
      endif                                                                     
c                                                                               
      write (nfile,112)                                                         
c                                                                               
      end                                                                       
c intrp    last revised 01/29/87 by tjw                                         
c*intrp f77 rewrite 01/29/87 by tjw                                             
      subroutine intrp (qall,a,c,b,j1,j2)                                       
c                                                                               
c     find interpolating polynomial coefficients.  independent                  
c     variable array is scaled temperature tempcs.                              
c                                                                               
c     input                                                                     
c       qall   logical flag to control degree of fit                            
c              true/false  max degree/max degree - 1                            
c                          max for aqueous basis species                        
c       a      real array, dependent variable                                   
c                                                                               
c     output                                                                    
c       c      real array, values of interpolating polynomial at                
c               tempc values                                                    
c       b      real array,  polynomial coefficients                             
c              note -  b(1),...,b(5) are coefficients over the                  
c                      range 0 to 100 degrees c                                 
c                      b(6),...,b(10) are coefficients over the                 
c                      range 100 to 300 degrees c                               
c              where a = b(1) + b(2)t + b(3)t**2 + b(4)t**3 + ...               
c       j1     0/1  normal case/all values of a = 500. over                     
c                               the temperature range 0 - 100                   
c       j2     0/1  normal case/all values of a = 500. over                     
c                               the temperature range 100 - 300                 
c              note - if j1 = 1, b(1) = 500., b(2),... = 0.                     
c                     if j2 = 1, b(6) = 500., b(7),... = 0.                     
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/hh.h'                                                     
C     include '../inc/hhn.h'                                                    
C     include '../inc/un.h'                                                     
c                                                                               
      dimension a(8), c(9), b(10)                                               
      dimension x(20),y(20),cof(21),w(20)                                       
c                                                                               
      data tx100/450.00001/                                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      j1 = 0                                                                    
      j2 = 0                                                                    
c                                                                               
c     initialize b to 0.                                                        
c                                                                               
      do 10 k=1,10                                                              
      b(k) = 0.                                                                 
   10 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * do lower temperature range  (0 - 100)           
c                             *----------------------------------------         
c                                                                               
c     get all non-default values of a, place into array y                       
c     corresponding scaled temperature values in array x                        
c     corresponding weights in array w                                          
c                                                                               
      npx = 0                                                                   
c                                                                               
      do 20 n=1,4                                                               
      if (a(n) .ge. 500.) go to 20                                              
      npx = npx+1                                                               
      x(npx) = tempcs(n)                                                        
      y(npx) = a(n)                                                             
      w(npx) = 1.                                                               
c                                                                               
c     weight last temperature                                                   
c                                                                               
      if (n .eq. 4) w(npx) = 10.                                                
   20 continue                                                                  
c                                                                               
c     npx = number of "good" values                                             
c                                                                               
      if (npx .le. 0) then                                                      
c                                                                               
c       no good values                                                          
        b(1) = 500.                                                             
        j1 = 1                                                                  
        go to 100                                                               
      endif                                                                     
c                                                                               
      if (npx .le. 1) then                                                      
        b(1) = y(1)                                                             
        go to 100                                                               
      endif                                                                     
c                                                                               
c     set degree of polynomial                                                  
c                                                                               
      nord = npx-1                                                              
      if (.not.qall) then                                                       
        if (nord .ge. 3) nord = nord-1                                          
      endif                                                                     
c                                                                               
c     find polynomial coeff  (0 - 100)                                          
c                                                                               
      call lsqp(x,y,w,npx,nord,cof,ier)                                         
      if (ier .gt. 0) then                                                      
        write(noutpt,1010)                                                      
 1010   format(/,' ----- lsqp4 error, temperature range 0 - 100')               
        stop                                                                    
      endif                                                                     
      npc = nord+1                                                              
c                                                                               
c     rescale coefficients (0 - 100)                                            
c                                                                               
      call rscal(cof,npc,tmax,cof)                                              
c                                                                               
      do 45 k=1,npc                                                             
      b(k) = cof(k)                                                             
   45 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * do upper temperature range  (100 - 300)         
c                             *----------------------------------------         
c                                                                               
  100 npx = 0                                                                   
c                                                                               
      do 120 n=4,8                                                              
      if (a(n) .eq. 500.) go to 120                                             
      npx = npx+1                                                               
      x(npx) = tempcs(n)                                                        
      y(npx) = a(n)                                                             
      w(npx) = 1.                                                               
c                                                                               
c     weight first value                                                        
c                                                                               
      if (n .eq. 4) w(npx) = 10.                                                
  120 continue                                                                  
c                                                                               
      if (npx .le. 0) then                                                      
        b(6) = 500.                                                             
        j2 = 1                                                                  
        go to 190                                                               
      endif                                                                     
      if (npx .le. 1) then                                                      
        b(6) = y(1)                                                             
        go to 190                                                               
      endif                                                                     
c                                                                               
c     set degree of polynomial                                                  
c                                                                               
      nord = npx-1                                                              
      if (.not.qall) then                                                       
        if (nord .ge. 3) nord = nord-1                                          
      endif                                                                     
c                                                                               
c     find polynomial coefficients  (100 - 300)                                 
c                                                                               
      call lsqp(x,y,w,npx,nord,cof,ier)                                         
      if (ier .gt. 0) then                                                      
        write(noutpt,1020)                                                      
 1020   format(/, ' ----- lsqp4 error, temperature range 100 - 300')            
        stop                                                                    
      endif                                                                     
      npc = nord+1                                                              
c                                                                               
c     rescale coefficients  (100 - 300)                                         
c                                                                               
      call rscal(cof,npc,tmax,cof)                                              
c                                                                               
      do 145 k=1,npc                                                            
      n = k+5                                                                   
      b(n) = cof(k)                                                             
  145 continue                                                                  
c                                                                               
c      compute values of c                                                      
c       interpolating polynomial at tempc values                                
c                                                                               
  190 do 195 k=1,8                                                              
      tx = tempc(k)                                                             
      c(k) = funcy(tx,b)                                                        
  195 continue                                                                  
c                                                                               
c     c(9) in upper set (100 - 300)                                             
c                                                                               
      c(9) = funcy(tx100,b)                                                     
c                                                                               
      end                                                                       
c pchkf    last revised 07/29/87 by rlh                                         
      subroutine pchkf                                                          
c                                                                               
c     created 3/6/87 by rmm                                                     
c                                                                               
c     process hkf data (part iv) which has been                                 
c     appended to data0                                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/un.h'                                                     
c                                                                               
      character*24 unam1,unam2,ublnk,uend24                                     
      character*80 utlhkf(4)                                                    
c                                                                               
      data uend24 /'endit.                  '/                                  
      data ublnk  /'                        '/                                  
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      write(noutpt,1000)                                                        
 1000 format(//,' processing hkf data')                                         
c                                                                               
c              header lines                                                     
c                                                                               
      read(ndhkf,1100) uhead                                                    
 1100 format(a8)                                                                
      write(ndata1) uhead                                                       
      write(ndata2) uhead                                                       
      do 25 i=1,4                                                               
      read(ndhkf,1200) utlhkf(i)                                                
 1200 format(a80)                                                               
      write(ndata1) utlhkf(i)                                                   
      write(ndata2) utlhkf(i)                                                   
      write(noutpt,1200) utlhkf(i)                                              
   25 continue                                                                  
c                                                                               
c              single ion parameters--name and omega                            
c                                                                               
  100 continue                                                                  
      read(ndhkf,1300) unam1,omega                                              
 1300 format(a24,2x,f15.0)                                                      
      if (unam1(1:8) .eq. uend24(1:8)) then                                     
        write(ndata1) uend24,ublnk                                              
        write(ndata2) uend24,ublnk                                              
      else                                                                      
        write(ndata1) unam1,omega                                               
        write(ndata2) unam1,omega                                               
        go to 100                                                               
      endif                                                                     
c                                                                               
c              ion-ion parameters--names and bji                                
c                                                                               
  200 continue                                                                  
      read(ndhkf,1400) unam1,unam2,bji                                          
 1400 format(a24,2x,a24,2x,f15.0)                                               
      if (unam1(1:8) .eq. uend24(1:8)) then                                     
        write(ndata1) uend24,ublnk,ublnk                                        
        write(ndata2) uend24,ublnk,ublnk                                        
      else                                                                      
        write(ndata1) unam1,unam2,bji                                           
        write(ndata2) unam1,unam2,bji                                           
        go to 200                                                               
      endif                                                                     
c                                                                               
      write(noutpt,1500)                                                        
 1500 format(//,' done with processing hkf data')                               
c                                                                               
      end                                                                       
c pcraq    last revised 03/03/87 by tjw                                         
c*pcraq f77 rewrite 01/29/87 by tjw                                             
      subroutine pcraq(nerr)                                                    
c                                                                               
c     read data0, write data1, data2, data3                                     
c     process compositions, reactions for aqueous species                       
c       test data for many types of errors,                                     
c       output diagnostic messages if errors found                              
c                                                                               
c     nerr is tallied one for each fatal error found                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ab.h'                                                     
C     include '../inc/av.h'                                                     
C     include '../inc/dv.h'                                                     
C     include '../inc/hh.h'                                                     
C     include '../inc/hhn.h'                                                    
C     include '../inc/jj.h'                                                     
C     include '../inc/jv.h'                                                     
C     include '../inc/re.h'                                                     
C     include '../inc/snm.h'                                                    
C     include '../inc/eqttt.h'                                                  
C     include '../inc/un.h'                                                     
C     include '../inc/xx.h'                                                     
c                                                                               
      character*24 uspn(3),unm,uend24,ublk24                                    
c                                                                               
      dimension gdum(8),gridb(9),apr(10)                                        
c                                                                               
      data ublank,uendit /'        ','endit.  '/                                
      data ublk24 /'                        '/                                  
      data uo2g /'O2(G)   '/                                                    
      data tol /1.0/                                                            
      data ndum1,ndum2,ndum3 /3*0/                                              
      data tx100/450.00001/                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      uend24(1:8)=uendit(1:8)                                                   
      nspnx = 1                                                                 
      nnx = -2                                                                  
      nn = 1                                                                    
c                                                                               
c     main loop returns here                                                    
c                                                                               
   20 ns = min(nn,nsq1)                                                         
      qall = ns.le.nsq                                                          
      mwtss = 0.                                                                
      ncts = 0                                                                  
      ndrs = 0                                                                  
c                                                                               
c     initialize cess, cdrs                                                     
c                                                                               
      do 25 nc=1,nct                                                            
      cess(nc,ns) = 0.                                                          
   25 continue                                                                  
c                                                                               
      do 30 nse=1,nsq2                                                          
      cdrs(nse,ns) = 0.                                                         
   30 continue                                                                  
c                                                                               
c     read name                                                                 
c                                                                               
      read (nda0,1010) uspec(ns)                                                
 1010 format(a18)                                                               
c                                                                               
c     test for exit                                                             
c                                                                               
      udum = uspec(ns)(1:8)                                                     
      if (udum(1:8) .eq. uendit(1:8)) then                                      
c                                                                               
c       clean up and exit here                                                  
c                                                                               
        nxm = nspnx-1                                                           
        nnx = nnx+3                                                             
        if (nspnx .gt. 1) then                                                  
c         write (nttyo,1012) nnx,(uspn(jj),jj=1,nxm)                            
          write (nslist,1012) nnx,(uspn(jj),jj=1,nxm)                           
 1012     format(1x,i5,3(2x,a18))                                               
        endif                                                                   
        write (ndata1) uend24,ublk24,ublk24                                     
        write (ndata2) uend24,ublk24,ublk24                                     
        write (ndata3) uend24,ublk24,ublk24                                     
        nst = nn-1                                                              
        go to 999                                                               
      endif                                                                     
c                                                                               
c     write name                                                                
c                                                                               
      write (noutpt,1016) nn,uspec(ns)                                          
 1016 format(1x,i5,1x,a18)                                                      
      if (mod(nn,20) .eq. 1) write (nttyo,1200) uspec(ns)                       
 1200 format(2x,a18)                                                            
c                                                                               
c     save name for tty, slist output                                           
c                                                                               
      uspn(nspnx) = uspec(ns)                                                   
c                                                                               
      nspnx = nspnx+1                                                           
      if (nspnx .gt. 3) then                                                    
        nspnx = 1                                                               
        nnx = nnx+3                                                             
c       write (ntty,1012) nnx,(uspn(jj),jj=1,3)                                 
        write (nslist,1012) nnx,(uspn(jj),jj=1,3)                               
      endif                                                                     
c                                                                               
c     read 'entered by' and date                                                
c                                                                               
      read (nda0,1034) udum1,udum2,udate                                        
 1034 format(16x,2a6,17x,a8)                                                    
c                                                                               
c     read source and quality                                                   
c                                                                               
      read (nda0,1036) udum3,udum4,udum5,udum6                                  
 1036 format(16x,2a6,17x,2a6)                                                   
c                                                                               
c     read charge, titr factor                                                  
c                                                                               
      read (nda0,1022) zdum,titr                                                
 1022 format(16x,f5.1,24x,f5.1)                                                 
      z0(nn) = zdum                                                             
      z(ns) = zdum                                                              
c                                                                               
c     read ion size, hydr. number                                               
c                                                                               
      read (nda0,1024) azero,hydr                                               
 1024 format(16x,f5.1,24x,f5.1)                                                 
c                                                                               
c     skip if o2(g)                                                             
c                                                                               
      if (udum .eq. uo2g) go to 60                                              
c                                                                               
c     read number of elements                                                   
c                                                                               
      read (nda0,1028) ncts                                                     
 1028 format(4x,i2)                                                             
c                                                                               
      if (ncts.le.0 .and. udum(1:8).ne.ublank(1:8)) then                        
        write (noutpt,1030) uspec(ns)                                           
        write (nslist,1030) uspec(ns)                                           
c       write (nttyo,1030) uspec(ns)                                            
 1030   format(/,' ----- species ',a18,' has no elements')                      
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
      if (ncts .gt. nct) then                                                   
        write (noutpt,1032) ncts,nct,uspec(ns)                                  
        write (nslist,1032) ncts,nct,uspec(ns)                                  
c       write (nttyo,1032) ncts,nct,uspec(ns)                                   
 1032   format(/,' ----- ncts = ',i5,' exceeds nct = ',i5,                      
     $  ' for ',a18)                                                            
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
c     read st. coeff, element names (3/line)                                    
c                                                                               
      read (nda0,1040) (cdum1(i),unam8(i),i=1,ncts)                             
 1040 format((5x,3(f7.3,1x,a8,6x)))                                             
c                                                                               
      do 58 n=1,ncts                                                            
c                                                                               
c     search for name in uelem                                                  
c                                                                               
      unm = unam8(n)(1:8)                                                       
      do 55 nc=1,nct                                                            
      if (unm(1:8) .eq. uelem(nc)(1:8)) go to 56                                
   55 continue                                                                  
c                                                                               
c     error, not found                                                          
c                                                                               
      write (noutpt,1042) uspec(ns),unm                                         
      write (nslist,1042) uspec(ns),unm                                         
c     write (nttyo,1042) uspec(ns),unm                                          
 1042 format(/,' ----- ',a18,' has bad element name = ',a6)                     
      nerr = nerr+1                                                             
      go to 58                                                                  
c                                                                               
   56 cess(nc,ns) = cdum1(n)                                                    
      mwtss = mwtss + atwt(nc) * cdum1(n)                                       
   58 continue                                                                  
c                                                                               
c     skip if basis species                                                     
c                                                                               
      if (ns .le. nsb) go to 60                                                 
c                                                                               
c     read number of species in reaction                                        
c                                                                               
      read (nda0,1052) ndrs                                                     
 1052 format(4x,i2)                                                             
c                                                                               
   60 write (ndata1) uspec(ns),ncts,ndrs,ndum1,ndum2,ndum3                      
c1050 format(a18,2x,5i5)                                                        
c                                                                               
      write (ndata1) mwtss,z(ns),titr,azero,hydr                                
c1051 format(5x,f10.3,f5.0,3f5.1)                                               
c                                                                               
c     skip if o2(g)                                                             
c                                                                               
      if (udum(1:8) .eq. uo2g(1:8)) go to 110                                   
c                                                                               
      write (ndata1) (cdum1(i),unam8(i), i=1,ncts)                              
c                                                                               
c     skip if basis species                                                     
c                                                                               
      if (ns .le. nsb) go to 110                                                
c                                                                               
c     read associated cross-link, dissociation, or dissolution reaction         
c                                                                               
      if (ndrs .gt. nsq1) then                                                  
        write (noutpt,1054) ndrs,nsq1,uspec(ns)                                 
        write (nslist,1054) ndrs,nsq1,uspec(ns)                                 
c       write (nttyo,1054) ndrs,nsq1,uspec(ns)                                  
 1054   format(/,' ----- ndrs = ',i5,' exceeds nsq1 = ',i5,                     
     $  ' for reaction of ',a18)                                                
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
      if (ndrs .le. 0) then                                                     
        write (noutpt,1056) uspec(ns)                                           
        write (nslist,1056) uspec(ns)                                           
c       write (nttyo,1056) uspec(ns)                                            
 1056   format(/,' ----- there are no components in the reaction for ',         
     $  a18)                                                                    
        nerr = nerr+1                                                           
        go to 150                                                               
      endif                                                                     
c                                                                               
c     read reaction coeff, species in reaction (3/line)                         
c                                                                               
      do 63 n=1,nsqpa1                                                          
      unam24(n)=ublk24                                                          
   63 continue                                                                  
      read (nda0,1060) (cdum1(n),unam24(n)(1:12),n=1,ndrs)                      
 1060 format((5x,3(f7.3,1x,a12,2x)))                                            
      write (ndata1) (cdum1(n),unam24(n),n=1,ndrs)                              
c                                                                               
      if (ndrs .le. 1) go to 75                                                 
c                                                                               
      do 72 n=2,ndrs                                                            
c                                                                               
c     search for species name in uspec                                          
c                                                                               
      unm = unam24(n)(1:12)                                                     
c                                                                               
      do 68 nse = 1,nsq                                                         
      if (unm(1:12) .eq. uspec(nse)(1:12)) then                                 
        cdrs(nse,ns) = cdum1(n)                                                 
        go to 72                                                                
      endif                                                                     
   68 continue                                                                  
c                                                                               
c     error, not found                                                          
c                                                                               
      write (noutpt,1062) uspec(ns), unam24(n)                                  
      write (nslist,1062) uspec(ns), unam24(n)                                  
c     write (nttyo,1062) uspec(ns), unam24(n)                                   
 1062 format(/,' ----- the reaction for ',a18,/,                                
     $   10x,' has bad aqueous species name ',a12)                              
      nerr = nerr+1                                                             
c                                                                               
   72 continue                                                                  
c                                                                               
   75 cdrs(nsq1,ns) = cdum1(1)                                                  
c                                                                               
c     test the reaction for mass and charge balance                             
c                                                                               
      call rxnchk(ns)                                                           
c                                                                               
c     read the log k grid and fit interpolating polynomials                     
c                                                                               
      read (nda0,1070) (xlks(i,ns),i=1,8)                                       
 1070 format(5x,4f10.4,/,5x,4f10.4)                                             
c                                                                               
      do 80 i=1,8                                                               
      gdum(i) = xlks(i,ns)                                                      
   80 continue                                                                  
c                                                                               
      call intrp(qall,gdum,gridb,apr,j1,j2)                                     
      jrflag(1,ns) = j1                                                         
      jrflag(2,ns) = j2                                                         
c                                                                               
c     put results away                                                          
c                                                                               
      do 85 i=1,10                                                              
      ars(i,ns) = apr(i)                                                        
   85 continue                                                                  
c                                                                               
      write (ndata1) (apr(i),i=1,10)                                            
c1072 format(5(1pe16.9),/,5(1pe16.9))                                           
c                                                                               
c     skip if ok                                                                
c                                                                               
      if (rms1 .gt. tol .or. rms2 .gt. tol) then                                
c                                                                               
c       output to noutpt and tty if gross error                                 
c                                                                               
        call preacy(ns,0,noutpt)                                                
        call gridpy(gdum,gridb,rms1,rms2,noutpt)                                
        call preacy(ns,0,nttyo)                                                 
        call gridpy(gdum,gridb,rms1,rms2,nttyo)                                 
      endif                                                                     
c                                                                               
c     read the delvr grid                                                       
c                                                                               
      read (nda0,1074) (delvr(i,ns),i=1,8)                                      
 1074 format(5x,4f10.4,/,5x,4f10.4)                                             
c                                                                               
      qzflag = .true.                                                           
      j1 = 0                                                                    
      j2 = 0                                                                    
c                                                                               
      do 90 i=1,8                                                               
      gdum(i) = delvr(i,ns)                                                     
      if (gdum(i) .ne. 0.) qzflag = .false.                                     
   90 continue                                                                  
c                                                                               
      do 91 i=1,10                                                              
      apr(i) = 0.                                                               
   91 continue                                                                  
c                                                                               
c     skip if all delvr = 0.                                                    
c                                                                               
      if (.not.qzflag) call intrp(qall,gdum,gridb,apr,j1,j2)                    
c                                                                               
      jvflag(1,ns) = j1                                                         
      jvflag(2,ns) = j2                                                         
c                                                                               
      do 93 i=1,10                                                              
      avs(i,ns) = apr(i)                                                        
   93 continue                                                                  
c                                                                               
c ***  do not write volume coefficients yet                                     
c     write (ndata1) apr(i),i=1,10)                                             
c                                                                               
c     call gridpy(gdum,gridb,rms1,rms2,noutpt)                                  
c ***                                                                           
c                                                                               
c     write reactions in short format                                           
c                                                                               
      call elimy(ns)                                                            
      cdum3 = cdrs(nsb1,ns)                                                     
c                                                                               
      do 95 nse = 2,nct                                                         
      cdum3 = cdum3+cdrs(nse,ns)                                                
   95 continue                                                                  
c                                                                               
      cdrs(nsb2,ns) = cdum3                                                     
c                                                                               
c     tally number of species in reaction                                       
c                                                                               
      ndrs = 0                                                                  
      do 100 nse = 1,nsb1                                                       
      if (cdrs(nse,ns) .ne. 0.) ndrs = ndrs+1                                   
  100 continue                                                                  
c                                                                               
  110 continue                                                                  
c                                                                               
c     write data on files data2 and data3                                       
c                                                                               
      write (ndata2) uspec(ns),ncts,ndrs,ndum1,ndum2,ndum3                      
c1080 format(a18,2x,5i5)                                                        
      write (ndata2) mwtss,z(ns),titr,azero,hydr                                
c1082 format(5x,f10.3,f5.0,2f5.1,f10.3)                                         
c                                                                               
c     skip if o2(g)                                                             
c                                                                               
      if (udum(1:8) .eq. uo2g(1:8)) go to 150                                   
c                                                                               
c     get element coeff, names, write to data2                                  
c                                                                               
      nnn = 0                                                                   
      do 115 nc = 1,nct                                                         
      cdum3 = cess(nc,ns)                                                       
      if (cdum3 .ne. 0.) then                                                   
        nnn = nnn+1                                                             
        cdum1(nnn) = cdum3                                                      
        unam8(nnn) = uelem(nc)                                                  
        if (nnn .ge. ncts) go to 120                                            
      endif                                                                     
  115 continue                                                                  
c                                                                               
  120 write (ndata2) (cdum1(n),unam8(n),n=1,ncts)                               
c1084 format((5x,3(f7.3,1x,a6,8x)))                                             
c                                                                               
c     skip if basis species                                                     
c                                                                               
      if (ns .le. nsb) go to 150                                                
c                                                                               
c     skip if no species                                                        
c                                                                               
      if (ndrs .le. 0) go to 150                                                
c                                                                               
c     get cdrs, names, write to data2                                           
c                                                                               
      cdum1(1) = cdrs(nsb1,ns)                                                  
      unam24(1)(1:12) = uspec(ns)(1:12)                                         
c                                                                               
      ndrs1 = ndrs+1                                                            
c                                                                               
      cdum1(ndrs1) = cdrs(nsb2,ns)                                              
      unam24(ndrs1)(1:12) = ublk24(1:12)                                        
c                                                                               
      nnn = 1                                                                   
      do 130 nse = 1,nsb                                                        
      cdum3 = cdrs(nse,ns)                                                      
      if (cdum3 .ne. 0.) then                                                   
        nnn = nnn+1                                                             
        cdum1(nnn) = cdrs(nse,ns)                                               
        unam24(nnn)(1:12) = uspec(nse)(1:12)                                    
      endif                                                                     
  130 continue                                                                  
c                                                                               
      write (ndata2) (cdum1(n),unam24(n),n=1,ndrs1)                             
c1086 format((5x,3(f7.3,1x,a12,2x)))                                            
c                                                                               
c     get ars, write to data3                                                   
c                                                                               
      do 135 i=1,10                                                             
      apr(i) = ars(i,ns)                                                        
  135 continue                                                                  
c                                                                               
c *** ??                                                                        
c  note that this block is never written out --                                 
c     get xlks, temp grid                                                       
c                                                                               
      do 140 i=1,8                                                              
      gdum(i) = xlks(i,ns)                                                      
      tx = tempc(i)                                                             
      gridb(i) = funcy(tx,apr)                                                  
  140 continue                                                                  
c                                                                               
      gridb(9) = funcy(tx100,apr)                                               
c *** ??                                                                        
c                                                                               
      write (ndata3) (apr(i),i=1,10)                                            
c1088 format(5(1pe16.9),/,5(1pe16.9))                                           
c                                                                               
c     back for next reaction                                                    
c                                                                               
c     skip the block delimiter line                                             
c                                                                               
  150 read (nda0,1010) udm                                                      
c                                                                               
      nn = nn+1                                                                 
      go to 20                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c pcraq0   last revised 03/24/87 by mlc                                         
c*pcraq0 f77 rewrite 01/29/87 by tjw                                            
      subroutine pcraq0(nerr)                                                   
c                                                                               
c     read file data0                                                           
c                                                                               
c     read input file data0 for aqueous species data                            
c     get species names (array uspec0) and charges (z0)                         
c                                                                               
c     nerr is tallied one for each fatal error found                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/snm.h'                                                    
C     include '../inc/eqttt.h'                                                  
C     include '../inc/un.h'                                                     
c                                                                               
      character*24 uspn(3)                                                      
c                                                                               
      data ublank /'        '/,uendit /'endit.  '/                              
      data uo2g /'O2(G)   '/                                                    
      data uterm /'+-------'/                                                   
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      nspnx = 1                                                                 
      nnx = -2                                                                  
      nn = 1                                                                    
      write(nout,1014)                                                          
 1014 format(/,' aqueous species',/)                                            
c                                                                               
c     main loop returns here                                                    
c                                                                               
   20 ns = min(nn,nsq1)                                                         
      mwtss = 0.                                                                
      ncts = 0                                                                  
      ndrs = 0                                                                  
c                                                                               
c     read name                                                                 
c                                                                               
      read(ndat0s,1010) uspec(ns)                                               
 1010 format(a18)                                                               
c                                                                               
c     test for exit                                                             
c                                                                               
      udum(1:8) = uspec(ns)(1:8)                                                
      if (udum(1:8) .eq. uendit(1:8)) then                                      
c                                                                               
c       clean up and exit here                                                  
c                                                                               
        nxm = nspnx-1                                                           
        nnx = nnx+3                                                             
        if (nspnx .gt. 1) then                                                  
          write(nout,1012) nnx,(uspn(jj),jj=1,nxm)                              
 1012     format(1x,i5,3(2x,a18))                                               
        endif                                                                   
c                                                                               
c       normal exit here                                                        
c       set total number of aqueous species                                     
c                                                                               
        nst0 = nn-1                                                             
        go to 999                                                               
      endif                                                                     
c                                                                               
c     save name for tty, output                                                 
c                                                                               
      uspn(nspnx) = uspec(ns)                                                   
c                                                                               
c     build uspec0 array (all aqueous species names)                            
c                                                                               
      na = nn                                                                   
      if (na .gt. nstpar) then                                                  
        write(nout,1040) uspec(ns)                                              
 1040   format(' ----- index exceeds uspec0 array limit, ',1x,a18)              
        nerr = nerr+1                                                           
      else                                                                      
        uspec0(na) = uspec(ns)                                                  
      endif                                                                     
c                                                                               
      nspnx = nspnx+1                                                           
      if (nspnx .gt. 3) then                                                    
        nspnx = 1                                                               
        nnx = nnx+3                                                             
        write(nout,1012) nnx,(uspn(jj),jj=1,3)                                  
      endif                                                                     
c                                                                               
c     read 'entered by' and date                                                
c                                                                               
      read(ndat0s,1034) udum1,udum2,udate                                       
 1034 format(16x,2a6,17x,a8)                                                    
c                                                                               
c     read source and quality                                                   
c                                                                               
      read(ndat0s,1036) udum3,udum4,udum5,udum6                                 
 1036 format(16x,2a6,17x,2a6)                                                   
c                                                                               
c     read charge, titr factor                                                  
c                                                                               
      read(ndat0s,1022) zdum,titr                                               
 1022 format(16x,f5.1,24x,f5.1)                                                 
      z0(nn) = zdum                                                             
c                                                                               
c     read ion size, hydr. number                                               
c                                                                               
      read(ndat0s,1024) azero,hydr                                              
 1024 format(16x,f5.1,24x,f5.1)                                                 
c                                                                               
c     skip if o2(g)                                                             
c                                                                               
      if (udum(1:8) .eq. uo2g(1:8)) go to 80                                    
c                                                                               
c     read number of elements                                                   
c                                                                               
      read(ndat0s,1028) ncts                                                    
 1028 format(4x,i2)                                                             
c                                                                               
      if (ncts .le. 0 .and. udum(1:8) .ne. ublank(1:8)) then                    
        write(nout,1030) uspec(ns)                                              
 1030   format(/,' ----- species ',a18,' has no elements')                      
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
      if (ncts .gt. nct) then                                                   
        write(nout,1032) ncts,nct,uspec(ns)                                     
 1032   format(/,' ----- ncts = ',i5,' exceeds nct = ',i5,                      
     $  ' for ',a18)                                                            
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
c     skip to end of block                                                      
c                                                                               
   80 read(ndat0s,1064) udum                                                    
 1064 format(a8)                                                                
      if (udum(1:8) .ne. uterm(1:8)) go to 80                                   
c                                                                               
c     back for next reaction                                                    
c                                                                               
      nn = nn+1                                                                 
      go to 20                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c pcrsg    last revised 03/03/87 by rlh                                         
c*pcrsg f77 rewrite 01/29/87 by tjw                                             
      subroutine pcrsg(nerr)                                                    
c                                                                               
c     process compositions, reactions for solids and gases                      
c       test data for many types of errors,                                     
c       output diagnostic messages if errors found                              
c                                                                               
c     nerr is tallied one for each fatal error found                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ab.h'                                                     
C     include '../inc/av.h'                                                     
C     include '../inc/dv.h'                                                     
C     include '../inc/hh.h'                                                     
C     include '../inc/hhn.h'                                                    
C     include '../inc/jj.h'                                                     
C     include '../inc/jv.h'                                                     
C     include '../inc/re.h'                                                     
C     include '../inc/eqttt.h'                                                  
C     include '../inc/un.h'                                                     
C     include '../inc/xx.h'                                                     
c                                                                               
      character*24 uspn(3),unm1,uend24,ublk24                                   
c                                                                               
      dimension gdum(8),gridb(9),apr(10)                                        
c                                                                               
      data ublank,uendit /'        ','endit.  '/                                
      data ublk24 /'                        '/                                  
      data tol /1.0/                                                            
      data titr,azero /2*0./                                                    
      data qall /.true./                                                        
      data ndum1,ndum2,ndum3 /3*0/                                              
      data tx100/450.00001/                                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      uend24(1:8)=uendit(1:8)                                                   
      nspnx = 1                                                                 
      nnx = -2                                                                  
      nn = 1                                                                    
      ns = nsq1                                                                 
      z(ns) = 0.                                                                
c                                                                               
c     main loop returns here                                                    
c                                                                               
   20 mwtss = 0.                                                                
      ncts = 0                                                                  
      ndrs = 0                                                                  
c                                                                               
c     initialize cess, cdrs                                                     
c                                                                               
      do 25 nc=1,nct                                                            
      cess(nc,ns) = 0.                                                          
   25 continue                                                                  
c                                                                               
      do 30 nse=1,nsq2                                                          
      cdrs(nse,ns) = 0.                                                         
   30 continue                                                                  
c                                                                               
c     read name                                                                 
c                                                                               
      read (nda0,1010) uspec(ns)                                                
 1010 format(a18)                                                               
c                                                                               
c     test for exit                                                             
c                                                                               
      udum = uspec(ns)(1:8)                                                     
      if (udum(1:8) .eq. uendit(1:8)) then                                      
c                                                                               
c      clean up and exit here                                                   
c                                                                               
        nxm = nspnx-1                                                           
        nnx = nnx+3                                                             
        if (nspnx .gt. 1) then                                                  
          write (nslist,1012) nnx,(uspn(jj),jj=1,nxm)                           
 1012     format(1x,i5,3(2x,a18))                                               
        endif                                                                   
        write (ndata1) uend24,ublk24,ublk24                                     
        write (ndata2) uend24,ublk24,ublk24                                     
        write (ndata3) uend24,ublk24,ublk24                                     
        go to 999                                                               
      endif                                                                     
c                                                                               
c     write name                                                                
c                                                                               
      write (noutpt,1016) nn,uspec(ns)                                          
 1016 format(1x,i5,1x,a18)                                                      
      if (mod(nn,20) .eq. 1) write (nttyo,1200) uspec(ns)                       
 1200 format(2x,a18)                                                            
c                                                                               
c     save name for tty, slist output                                           
c                                                                               
      uspn(nspnx) = uspec(ns)                                                   
c                                                                               
      nspnx = nspnx+1                                                           
      if (nspnx .gt. 3) then                                                    
        nspnx = 1                                                               
        nnx = nnx+3                                                             
        write (nslist,1012) nnx,(uspn(jj),jj=1,3)                               
      endif                                                                     
c                                                                               
c     read 'entered by' and date                                                
c                                                                               
      read (nda0,1034) udum1,udum2,udate                                        
 1034 format(16x,2a6,17x,a8)                                                    
c                                                                               
c     read source and quality                                                   
c                                                                               
      read (nda0,1036) udum3,udum4,udum5,udum6                                  
 1036 format(16x,2a6,17x,2a6)                                                   
c                                                                               
c     read volume                                                               
c                                                                               
      read (nda0,1025) vol                                                      
 1025 format(16x,f9.3)                                                          
c                                                                               
c     read number of elements                                                   
c                                                                               
      read (nda0,1028) ncts                                                     
 1028 format(4x,i2)                                                             
c                                                                               
      if (ncts.le.0 .and. udum(1:8).ne.ublank(1:8)) then                        
        write (noutpt,1030) uspec(ns)                                           
        write (nslist,1030) uspec(ns)                                           
 1030   format(/,' ----- species ',a18,' has no elements')                      
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
      if (ncts .gt. nct) then                                                   
        write (noutpt,1032) ncts,nct,uspec(ns)                                  
        write (nslist,1032) ncts,nct,uspec(ns)                                  
 1032   format(/,' ----- ncts = ',i5,' exceeds nct = ',i5,                      
     $  ' for ',a18)                                                            
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
c     read st. coeff, element names (3/line)                                    
c                                                                               
      read (nda0,1040) (cdum1(i),unam8(i),i=1,ncts)                             
 1040 format((5x,3(f7.3,1x,a8,6x)))                                             
c                                                                               
      mwtss = 0.                                                                
      do 58 n=1,ncts                                                            
c                                                                               
c     search for name in uelem                                                  
c                                                                               
      unm = unam8(n)(1:8)                                                       
      do 55 nc=1,nct                                                            
      if (unm(1:8) .eq. uelem(nc)(1:8)) then                                    
        cess(nc,ns) = cdum1(n)                                                  
        mwtss = mwtss + atwt(nc) * cdum1(n)                                     
        go to 58                                                                
      endif                                                                     
   55 continue                                                                  
c                                                                               
c     error, not found                                                          
c                                                                               
      write (noutpt,1042) uspec(ns),unm                                         
      write (nslist,1042) uspec(ns),unm                                         
 1042 format(/,' ----- ',a18,' has bad element name = ',a8)                     
      nerr = nerr+1                                                             
c                                                                               
   58 continue                                                                  
c                                                                               
c     read number of species in reaction                                        
c                                                                               
      read (nda0,1052) ndrs                                                     
 1052 format(4x,i2)                                                             
c                                                                               
      write (ndata1) uspec(ns),ncts,ndrs,ndum1,ndum2,ndum3                      
c1050 format(a18,2x,5i5)                                                        
c                                                                               
      write (ndata1) mwtss,z(ns),titr,azero,vol                                 
c1051 format(5x,f10.3,f5.0,2f5.1,f10.3)                                         
c                                                                               
      write (ndata1) (cdum1(i),unam8(i), i=1,ncts)                              
c                                                                               
c     read associated cross-link, dissociation, or dissolution reaction         
c                                                                               
      if (ndrs .gt. nsq1) then                                                  
        write (noutpt,1054) ndrs,nsq1,uspec(ns)                                 
        write (nslist,1054) ndrs,nsq1,uspec(ns)                                 
 1054   format(/,' ----- ndrs = ',i5,' exceeds nsq1 = ',i5,                     
     $  ' for reaction of ',a18)                                                
        nerr = nerr+1                                                           
      endif                                                                     
c                                                                               
      if (ndrs .le. 0) then                                                     
        write (noutpt,1056) uspec(ns)                                           
        write (nslist,1056) uspec(ns)                                           
 1056   format(/,' ----- there are no components in the reaction for ',         
     $  a18)                                                                    
        nerr = nerr+1                                                           
        go to 150                                                               
      endif                                                                     
c                                                                               
c     read reaction coeff, species in reaction (3/line)                         
c                                                                               
      do 34 i=1,nsqpa1                                                          
      unam24(i)=ublk24                                                          
   34 continue                                                                  
      read (nda0,1060) (cdum1(n),unam24(n),n=1,ndrs)                            
 1060 format((5x,3(f7.3,1x,a12,2x)))                                            
      write (ndata1) (cdum1(n),unam24(n),n=1,ndrs)                              
c                                                                               
      if (ndrs .le. 1) go to 75                                                 
c                                                                               
      do 72 n=2,ndrs                                                            
c                                                                               
c     search for species name in uspec                                          
c                                                                               
      unm1(1:12) = unam24(n)(1:12)                                              
c                                                                               
      do 68 nse = 1,nsq                                                         
      if (unm1(1:12) .eq. uspec(nse)(1:12)) then                                
        cdrs(nse,ns) = cdum1(n)                                                 
        go to 72                                                                
      endif                                                                     
   68 continue                                                                  
c                                                                               
c     error, not found                                                          
c                                                                               
      write (noutpt,1062) uspec(ns), unam24(n)                                  
      write (nslist,1062) uspec(ns), unam24(n)                                  
 1062 format(/,' ----- the reaction for ',a18,/,                                
     $   10x,' has bad aqueous species name ',a12)                              
      nerr = nerr+1                                                             
c                                                                               
   72 continue                                                                  
c                                                                               
   75 continue                                                                  
c                                                                               
      cdrs(nsq1,ns) = cdum1(1)                                                  
c                                                                               
c     test the reaction for mass and charge balance                             
c                                                                               
      call rxnchk(ns)                                                           
c                                                                               
c     read the log k grid and fit interpolating polynomial                      
c                                                                               
      read (nda0,1070) (xlks(i,ns),i=1,8)                                       
 1070 format(5x,4f10.4,/,5x,4f10.4)                                             
c                                                                               
      do 80 i=1,8                                                               
      gdum(i) = xlks(i,ns)                                                      
   80 continue                                                                  
c                                                                               
      call intrp(qall,gdum,gridb,apr,j1,j2)                                     
      jrflag(1,ns) = j1                                                         
      jrflag(2,ns) = j2                                                         
c                                                                               
c     put results away                                                          
c                                                                               
      do 85 i=1,10                                                              
      ars(i,ns) = apr(i)                                                        
   85 continue                                                                  
c                                                                               
      write (ndata1) (apr(i),i=1,10)                                            
c1072 format(5(1pe16.9),/,5(1pe16.9))                                           
c                                                                               
c     skip if ok                                                                
c                                                                               
      if (rms1 .gt. tol .or. rms2 .gt. tol) then                                
c                                                                               
c       output to noutpt and tty if gross error                                 
c                                                                               
        call preacy(ns,0,noutpt)                                                
        call gridpy(gdum,gridb,rms1,rms2,noutpt)                                
        call preacy(ns,0,nttyo)                                                 
        call gridpy(gdum,gridb,rms1,rms2,nttyo)                                 
      endif                                                                     
c                                                                               
c     read the delvr grid                                                       
c                                                                               
      read (nda0,1074) (delvr(i,ns),i=1,8)                                      
 1074 format(5x,4f10.4,/,5x,4f10.4)                                             
c                                                                               
      qzflag = .true.                                                           
      j1 = 0                                                                    
      j2 = 0                                                                    
c                                                                               
      do 90 i=1,8                                                               
      gdum(i) = delvr(i,ns)                                                     
      if (gdum(i) .ne. 0.) qzflag = .false.                                     
   90 continue                                                                  
c                                                                               
      do 91 i=1,10                                                              
      apr(i) = 0.                                                               
   91 continue                                                                  
c                                                                               
c     skip if all delvr = 0.                                                    
c                                                                               
      if (.not.qzflag) then                                                     
        call intrp(qall,gdum,gridb,apr,j1,j2)                                   
      endif                                                                     
      jvflag(1,ns) = j1                                                         
      jvflag(2,ns) = j2                                                         
c                                                                               
      do 93 i=1,10                                                              
      avs(i,ns) = apr(i)                                                        
   93 continue                                                                  
c                                                                               
c ***  do not write at this time                                                
c     write (ndata1) apr(i),i=1,10)                                             
c                                                                               
c     call gridpy(gdum,gridb,rms1,rms2,noutpt)                                  
c ***                                                                           
c                                                                               
c     write reactions in short format                                           
c                                                                               
      call elimy(ns)                                                            
      cdum3 = 0.                                                                
c                                                                               
      do 95 nse = 2,nct                                                         
      cdum3 = cdum3+cdrs(nse,ns)                                                
   95 continue                                                                  
c                                                                               
      cdrs(nsb2,ns) = cdum3                                                     
c                                                                               
c     tally number of species in reaction                                       
c                                                                               
      ndrs = 0                                                                  
      do 100 nse = 1,nsb1                                                       
      if (cdrs(nse,ns) .ne. 0.) ndrs = ndrs+1                                   
  100 continue                                                                  
c                                                                               
c     write data on files data2 and data3                                       
c                                                                               
      write (ndata2) uspec(ns),ncts,ndrs,ndum1,ndum2,ndum3                      
c1080 format(a18,2x,5i5)                                                        
      write (ndata2) mwtss,z(ns),titr,azero,vol                                 
c1082 format(5x,f10.3,f5.0,2f5.1,f10.3)                                         
c                                                                               
c     get element coeff, names, write to data2                                  
c                                                                               
      nnn = 0                                                                   
      do 115 nc = 1,nct                                                         
      cdum3 = cess(nc,ns)                                                       
      if (cdum3 .ne. 0.) then                                                   
        nnn = nnn+1                                                             
        cdum1(nnn) = cdum3                                                      
        unam8(nnn)(1:8) = uelem(nc)(1:8)                                        
        if (nnn .ge. ncts) go to 120                                            
      endif                                                                     
  115 continue                                                                  
c                                                                               
  120 write (ndata2) (cdum1(n),unam8(n),n=1,ncts)                               
c1084 format((5x,3(f7.3,1x,a6,8x)))                                             
c                                                                               
c     skip if no species                                                        
c                                                                               
      if (ndrs .le. 0) go to 150                                                
c                                                                               
c     get cdrs, names, write to data2                                           
c                                                                               
      cdum1(1) = cdrs(nsb1,ns)                                                  
      unam24(1) = uspec(ns)(1:12)                                               
c                                                                               
      ndrs1 = ndrs+1                                                            
c                                                                               
      cdum1(ndrs1) = cdrs(nsb2,ns)                                              
c   note- trouble here???  this should not be used                              
      unam24(ndrs1) = ublk24                                                    
c                                                                               
      nnn = 1                                                                   
      do 130 nse = 1,nsb                                                        
      cdum3 = cdrs(nse,ns)                                                      
      if (cdum3 .ne. 0.) then                                                   
        nnn = nnn+1                                                             
        cdum1(nnn) = cdrs(nse,ns)                                               
        unam24(nnn) = uspec(nse)(1:12)                                          
      endif                                                                     
  130 continue                                                                  
c                                                                               
c   note- think the following line should be                                    
c     write (ndata2) (cdum1(n),unam24(n),n=1,ndrs),cdum1(ndrst)                 
c     instead of                                                                
      write (ndata2) (cdum1(n),unam24(n),n=1,ndrs1)                             
c1086 format((5x,3(f7.3,1x,a12,2x)))                                            
c                                                                               
c     get ars, write to data3                                                   
c                                                                               
      do 135 i=1,10                                                             
      apr(i) = ars(i,ns)                                                        
  135 continue                                                                  
c *** ??                                                                        
c  note that this block is never written out --                                 
c      get xlks, temp grid                                                      
c                                                                               
      do 140 i=1,8                                                              
      gdum(i) = xlks(i,ns)                                                      
      tx = tempc(i)                                                             
      gridb(i) = funcy(tx,apr)                                                  
  140 continue                                                                  
c                                                                               
      gridb(9) = funcy(tx100,apr)                                               
c *** ??                                                                        
c                                                                               
      write (ndata3) (apr(i),i=1,10)                                            
c1088 format(5(1pe16.9),/,5(1pe16.9))                                           
c                                                                               
c     back for next reaction                                                    
c                                                                               
  150 continue                                                                  
c                                                                               
c     skip the block delimiter line                                             
c                                                                               
      read (nda0,1010) udm                                                      
c                                                                               
      nn = nn+1                                                                 
      go to 20                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c pcrss    last revised 03/03/87 by rlh                                         
c*pcrss f77 rewrite 01/29/87 by tjw                                             
      subroutine pcrss(nerr)                                                    
c                                                                               
c     process solid solution data from file data0                               
c     nerr is tallied one for each error found                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/re.h'                                                     
C     include '../inc/eqttt.h'                                                  
C     include '../inc/un.h'                                                     
C     include '../inc/uu.h'                                                     
c                                                                               
      character*24 uspn(3),utmp                                                 
      character*24 uend24,ublk24                                                
c                                                                               
      data uendit /'endit.  '/                                                  
      data ublk24 /'                        '/                                  
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      uend24(1:8)=uendit(1:8)                                                   
      read (nda0,1010) udum,udum2,udum3                                         
      write (ndata1) udum,udum2,udum3                                           
      write (ndata2) udum,udum2,udum3                                           
 1010 format(3a8)                                                               
      write (nslist,1012) udum,udum2,udum3                                      
 1012 format(/,/,3a8)                                                           
      nspnx = 1                                                                 
      nnx = -2                                                                  
      nx = 1                                                                    
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
   10 read (nda0,1014) utmp,ncomp,jsol,ndum1,ndum2,ndum3                        
 1014 format(a18,2x,5i5)                                                        
      udum=utmp(1:8)                                                            
c                                                                               
c     loop exit test                                                            
c                                                                               
      if (udum(1:8) .eq. uendit(1:8)) then                                      
c                                                                               
c       clean up and exit here                                                  
c                                                                               
        nxm = nspnx-1                                                           
        nnx = nnx+3                                                             
        if (nspnx .le. 1) go to   15                                            
 1016   format(1x,i5,3(2x,a18))                                                 
        write (nslist,1016) nnx, (uspn(jj),jj=1,nxm)                            
   15   continue                                                                
        write (ndata1) uend24,ublk24,ublk24                                     
        write (ndata2) uend24,ublk24,ublk24                                     
        write (ndata3) uend24,ublk24,ublk24                                     
        go to 999                                                               
      endif                                                                     
c                                                                               
      write (ndata1) utmp,ncomp,jsol,ndum1,ndum2,ndum3                          
      write (ndata2) utmp,ncomp,jsol,ndum1,ndum2,ndum3                          
      write (noutpt,1020) utmp,ncomp,jsol,ndum1,ndum2,ndum3                     
 1020 format(1x,a18,2x,5i5)                                                     
c                                                                               
      uspn(nspnx) = utmp                                                        
c                                                                               
      nspnx = nspnx+1                                                           
      if (nspnx .gt. 3) then                                                    
        nspnx = 1                                                               
        nnx = nnx+3                                                             
        write (nslist,1016) nnx, (uspn(jj),jj=1,3)                              
      endif                                                                     
c                                                                               
      if (ncomp.le.0) go to 60                                                  
      if (ncomp.gt.iktpar) then                                                 
        write (noutpt,1022) ncomp,iktpar                                        
        write (nslist,1022) ncomp,iktpar                                        
 1022   format(1x,'----- ncomp = ',i5,' exceeds iktpar = ',i5,' -----')         
        nerr=nerr+1                                                             
      endif                                                                     
c                                                                               
      read (nda0,1024) (xbarlm(n),unam24(n), n=1,ncomp)                         
 1024 format ((5x,2(f7.3,1x,a18,2x)))                                           
      write (ndata1) (xbarlm(n),unam24(n), n=1,ncomp)                           
      write (ndata2) (xbarlm(n),unam24(n), n=1,ncomp)                           
      read (nda0,1026) (apx(i),i=1,12)                                          
 1026 format(/,6f10.4,/,6f10.4)                                                 
      write (ndata1) (apx(i), i=1,12)                                           
c1028 format(6f10.4,/,6f10.4)                                                   
      write (ndata3) (apx(i), i=1,12)                                           
c                                                                               
c     skip block delimiter line                                                 
c                                                                               
      read (nda0,1030) udm                                                      
 1030 format(a8)                                                                
c                                                                               
   60 nx = nx+1                                                                 
      go to 10                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c pdpz2    last revised 7/21/87 by rmm                                          
c*pdpz2 f77 rewrite 02/02/87 by tjw                                             
      subroutine pdpz2                                                          
c                                                                               
c     process dpt0, part 1 - single salt parameters                             
c                                                                               
c     read and scale (multipliers) beta and c phi data,                         
c     build and write first block to output file dpt1                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/fa.h'                                                     
C     include '../inc/mu.h'                                                     
C     include '../inc/snm.h'                                                    
C     include '../inc/un.h'                                                     
c                                                                               
      character*80 uttl                                                         
      character*24 usrc1,usrc2                                                  
      character*24 usp1,usp2                                                    
c                                                                               
      dimension zt(2)                                                           
c                                                                               
      data umult  /'multipli'/                                                  
      data uendit /'endit.  '/                                                  
      data uterm  /'+-------'/                                                  
      data usingl /'single-s'/                                                  
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     the *found* arrays                                                        
c      data from species whose names were found in uspec0                       
c                                                                               
c      the index used with these arrays is nslt                                 
c      two a6 ascii species names                                               
c         ussrs(2,2,npxpar)                                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c      lambda and mu arrays                                                     
c                                                                               
c      cphi: cphi(npxpar)                                                       
c      dcphi/dt: dc(npxpar)                                                     
c      names of cations, anions: umsp(2,npxpar), uxsp(2,npxpar)                 
c      corresponding charges: zm(npxpar), zc(npxpar)                            
c      lambda(0),(1),(2): l0(npxpar),l1(npxpar),l2(npxpar)                      
c      dl0/dt,dl1/dt,dl2/dt: dl0(npxpar),dl1(npxpar),dl2(npxpar)                
c       alpha(1), (2): alph1(npxpar),alph2(npxpar)                              
c                                                                               
c     array index is npx, maximum npx is npxpar                                 
c     number of entries is npxp                                                 
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      nn = 1                                                                    
      npx = 0                                                                   
      nslt = 0                                                                  
c                                                                               
      read (ndpt0s,1010) uttl                                                   
 1010 format(a80)                                                               
      if (uttl(1:8) .ne. usingl(1:8)) then                                      
        write (nout,1012)                                                       
 1012   format(/,' ----- dpt0 file format error')                               
        write (nttyo,2000)                                                      
 2000   format(' * dpt0 file format error')                                     
        stop                                                                    
      endif                                                                     
c                                                                               
      write (ndpt1,1010) uttl                                                   
      write (ndpt1,1016)                                                        
 1016 format('*')                                                               
c                                                                               
c----------main loop----------------------------------------------------        
c                                                                               
c      read a block from dpt0                                                   
c                                                                               
c      get species names   (or multiplier line)                                 
c                                                                               
   20 read (ndpt0s,1040) usp1,usp2,bdm0,bdm1,bdm2,cpmd                          
 1040 format(a12,2x,a12,3(f7.4,2x),f8.5)                                        
      if (usp1(1:8) .eq. uendit(1:8)) then                                      
c                                                                               
c       found end of single salt params                                         
c                                                                               
        npxp = npx                                                              
        write (ndpt1,1024) uendit,uendit                                        
 1024   format(a8,50x,a8,/,'*',10('------'))                                    
        go to 999                                                               
      endif                                                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      if (usp1(1:8) .eq. umult(1:8)) then                                       
        write (ndpt1,1030) usp2(1:6)                                            
 1030   format('* - - ',a6,10(' - - -'))                                        
c                                                                               
c       save multipliers                                                        
c                                                                               
        bm0 = bdm0                                                              
        bm1 = bdm1                                                              
        bm2 = bdm2                                                              
        cpdm = cpmd                                                             
c                                                                               
c       skip terminator line                                                    
c                                                                               
        read (ndpt0s,1013) udum                                                 
 1013   format(a6)                                                              
        go to 20                                                                
      endif                                                                     
c                                                                               
c     betas                                                                     
c                                                                               
      read (ndpt0s,1042) bet0,bet1,bet2                                         
 1042 format(3(13x,f8.4))                                                       
c                                                                               
c     apply beta multipliers                                                    
c                                                                               
      b0 = bm0*bet0                                                             
      b1 = bm1*bet1                                                             
      b2 = bm2*bet2                                                             
c                                                                               
c     alphas                                                                    
c                                                                               
      read (ndpt0s,1044) alf1,alf2                                              
 1044 format(18x,2(16x,f5.1))                                                   
c                                                                               
c     cphi, maxm                                                                
c                                                                               
      read (ndpt0s,1046) cpdum,maxm                                             
 1046 format(13x,f9.5,12x,f5.1)                                                 
c                                                                               
c     apply cphi multiplier                                                     
c                                                                               
      cphi = cpdm*cpdum                                                         
c                                                                               
c     source 1                                                                  
c                                                                               
      read (ndpt0s,1048) usrc1                                                  
 1048 format(13x,a18)                                                           
c                                                                               
c     itgflag                                                                   
c                                                                               
      read (ndpt0s,1050) itgflag                                                
 1050 format(13x,i2)                                                            
c                                                                               
      if (itgflag .gt. 0) then                                                  
        write (nout,1060)                                                       
 1060   format(' ----- itgflag .gt. zero is not implemented')                   
        stop                                                                    
      endif                                                                     
c                                                                               
c     beta derivatives                                                          
c                                                                               
      read (ndpt0s,1062) dbet0,d2bet0                                           
 1062 format(13x,e10.3,13x,e10.3)                                               
      read (ndpt0s,1062) dbet1,d2bet1                                           
      read (ndpt0s,1062) dbet2,d2bet2                                           
c                                                                               
c     apply multipliers                                                         
c                                                                               
      db0 = bm0*dbet0                                                           
      db1 = bm1*dbet1                                                           
      db2 = bm2*dbet2                                                           
      d2b0 = bm0*d2bet0                                                         
      d2b1 = bm1*d2bet1                                                         
      d2b2 = bm2*d2bet2                                                         
c                                                                               
c     cphi derivatives                                                          
c                                                                               
      read (ndpt0s,1062) dcph,d2cph                                             
c                                                                               
c     apply cphi multiplier                                                     
c                                                                               
      dcp = cpdm*dcph                                                           
      d2cp = cpdm*d2cph                                                         
c                                                                               
c     maxm, maxt                                                                
c                                                                               
      read (ndpt0s,1051) maxm2,maxt                                             
 1051 format(13x,f5.1,18x,f5.1)                                                 
c                                                                               
c     source 2                                                                  
c                                                                               
      read (ndpt0s,1048) usrc2                                                  
c                                                                               
c     skip to block delimiter line                                              
c                                                                               
   36 read (ndpt0s,1052) udum                                                   
 1052 format(a8)                                                                
      if (udum(1:8) .ne. uterm(1:8)) go to 36                                   
c                                                                               
c     search for species names in uspec0                                        
c                                                                               
      qnf = .false.                                                             
c                                                                               
      do 38 ns=2,nst0                                                           
      if (usp1(1:12) .eq. uspec0(ns)(1:12)) then                                
c                                                                               
c       found first species, save charge                                        
c                                                                               
        zt(1) = z0(ns)                                                          
        go to 40                                                                
      endif                                                                     
   38 continue                                                                  
c                                                                               
c     first species not found, add to not found list                            
c                                                                               
      call adspn(usp1)                                                          
      qnf = .true.                                                              
c                                                                               
   40 continue                                                                  
      do 42 ns=2,nst0                                                           
      if (usp2(1:12) .eq. uspec0(ns)(1:12)) then                                
c                                                                               
c       found second species, save charge                                       
c                                                                               
        zt(2) = z0(ns)                                                          
        go to 46                                                                
      endif                                                                     
   42 continue                                                                  
c                                                                               
c     second species not found, add to not found list                           
c                                                                               
      call adspn(usp2)                                                          
      qnf = .true.                                                              
c                                                                               
c      skip if either species not found                                         
c                                                                               
   46 if (qnf) go to 70                                                         
c                                                                               
c      put data into *found* arays                                              
c                                                                               
      nslt = nslt + 1                                                           
      if (nslt .gt. npxpar) then                                                
        write (nout,1072)                                                       
        write (nttyo,1072)                                                      
 1072   format(' * overflow of *found* array index, npxpar too small')          
        stop                                                                    
      endif                                                                     
c                                                                               
c     species names                                                             
c                                                                               
      ussrs(1,nslt) = usp1                                                      
      ussrs(2,nslt) = usp2                                                      
c                                                                               
c     write a block to file dpt1                                                
c                                                                               
      write (ndpt1,1112) usp1,usp2                                              
 1112 format(a12,2x,a12)                                                        
      write (ndpt1,1110)                                                        
 1110 format('*')                                                               
c                                                                               
      write (ndpt1,1114) b0,b1,b2                                               
 1114 format(5x,'beta0 = ',f8.4,5x,'beta1 = ',f8.4,                             
     $  5x,'beta2 = ',f8.4)                                                     
      write (ndpt1,1116) alf1,alf2                                              
 1116 format(25x,'alpha1 = ',f5.1,7x,'alpha2 = ',f5.1)                          
c                                                                               
      write (ndpt1,1118) cphi                                                   
 1118 format(6x,'cphi = ',f9.5)                                                 
      write (ndpt1,1110)                                                        
c                                                                               
      write (ndpt1,1132) db0,d2b0                                               
 1132 format(4x,'db0/dt = ',1pe10.3,2x,'d2b0/dt2 = ',1pe10.3)                   
      write (ndpt1,1134) db1,d2b1                                               
 1134 format(4x,'db1/dt = ',1pe10.3,2x,'d2b1/dt2 = ',1pe10.3)                   
      write (ndpt1,1136) db2,d2b2                                               
 1136 format(4x,'db2/dt = ',1pe10.3,2x,'d2b2/dt2 = ',1pe10.3)                   
c                                                                               
      write (ndpt1,1138) dcp,d2cp                                               
 1138 format(5x,'dc/dt = ',1pe10.3,3x,'d2c/dt2 = ',1pe10.3)                     
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt1,1130)                                                        
 1130 format('+',12('------'))                                                  
c                                                                               
      npx = npx+1                                                               
      if (npx .gt. npxpar) then                                                 
        write (nttyo,1026)                                                      
        write (nout,1026)                                                       
 1026   format(' * error- mu array index overflow, npxpar too small')           
        stop                                                                    
      endif                                                                     
c                                                                               
c     save lambda's                                                             
c                                                                               
      l0(npx) = b0                                                              
      l1(npx) = b1                                                              
      l2(npx) = b2                                                              
c                                                                               
c     save lambda temperature derivatives                                       
c                                                                               
      dl0(npx) = db0                                                            
      dl1(npx) = db1                                                            
      dl2(npx) = db2                                                            
      d2l0(npx) = d2b0                                                          
      d2l1(npx) = d2b1                                                          
      d2l2(npx) = d2b2                                                          
c                                                                               
c     save cphi                                                                 
c                                                                               
      cph(npx) = cphi                                                           
c                                                                               
c     save cphi temperature derivatives                                         
c                                                                               
      dc(npx) = dcp                                                             
      d2c(npx) = d2cp                                                           
c                                                                               
c     save alpha parameters                                                     
c                                                                               
      alph1(npx) = alf1                                                         
      alph2(npx) = alf2                                                         
c                                                                               
c     get corresponding charges                                                 
c                                                                               
      z1 = zt(1)                                                                
      z2 = zt(2)                                                                
c                                                                               
c     test signs                                                                
c                                                                               
      if (z1 .lt. 0.) then                                                      
        zx(npx) = z1                                                            
        uxsp(npx) = usp1                                                        
        zm(npx) = z2                                                            
        umsp(npx) = usp2                                                        
        go to 70                                                                
      elseif (z1 .gt. 0.) then                                                  
        zm(npx) = z1                                                            
        umsp(npx) = usp1                                                        
        zx(npx) = z2                                                            
        uxsp(npx) = usp2                                                        
      else                                                                      
        write (nttyo,2002) usp1                                                 
        write (nout,2002) usp1                                                  
 2002   format(' * error- ',a24,' has zero charge (pdpz2)')                     
      endif                                                                     
c                                                                               
      if (z2 .eq. 0.) then                                                      
        write (nttyo,2002) usp2                                                 
        write (nout,2002) usp2                                                  
      endif                                                                     
c                                                                               
   70 nn = nn+1                                                                 
      go to 20                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c pdpz3    last revised 07/29/87 by rlh                                         
c*pdpz3 f77 rewrite 02/02/87 by tjw                                             
      subroutine pdpz3                                                          
c                                                                               
c     process dpt0, part 2 - mixture term parameters                            
c                                                                               
c     read a line, search for three species names in uspec0                     
c     order the species names                                                   
c       name1, name2, common name                                               
c       with name1 .lt. name2                                                   
c       (name1,name2) define a pair                                             
c     store pairs into arrays usp1,usp2, common into array uspcm                
c                                                                               
c     note - the theta values are derived by weighting each theta               
c            with the corresponding max m value, summing these                  
c            products, summing the max m's, and averaging.                      
c                                                                               
c     the theta set (functions of two ions) and the psi set (functions          
c     of three ions) are constructed.                                           
c     block 2 (theta set) and block 3 (psi set) are written to                  
c     output file dpt1.                                                         
c                                                                               
c     blocks 1 and 2 (theta and psi sets of lambdas), and                       
c     block 3 (c phi set of mu's) are written to output file dpt2.              
c                                                                               
c     block 4 (psi set of mu's) is constructed and written to                   
c     output file dpt2.                                                         
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/fa.h'                                                     
C     include '../inc/dbug.h'                                                   
C     include '../inc/mu.h'                                                     
C     include '../inc/snm.h'                                                    
C     include '../inc/un.h'                                                     
c                                                                               
      character*80 uttl                                                         
      character*24 usrc,u1,u2,uc                                                
      character*24 unam1,unam2,unam3,us1,us2,usc                                
      character*24 udm,uend24,ublk24,ustp24                                     
      character*8 usdum(8),utrm(9)                                              
c                                                                               
C*********************************************************************          
C                                                                               
C                                                                               
C      dimension mummx(npxpar), mumxx(npxpar)                                   
C      dimension dpsi(npxpar),d2psi(npxpar)                                     
C      dimension dmxx(npxpar),dmmx(npxpar),d2mxx(npxpar),d2mmx(npxpar)          
      COMMON /ZDUM1/ mummx(npxpar), mumxx(npxpar),                              
     1       dpsi(npxpar),d2psi(npxpar),                                        
     2       dmxx(npxpar),dmmx(npxpar),d2mxx(npxpar),d2mmx(npxpar)              
C                                                                               
C                                                                               
C*********************************************************************          
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     theta array set                                                           
c                                                                               
c     species names                                                             
c                                                                               
C*********************************************************************          
C                                                                               
C                                                                               
C     character*24 uspc1(8,npxpar), uspc2(8,npxpar)                             
      character*24 uspc1, uspc2                                                 
      COMMON/ZDUM2/ uspc1(8,npxpar)                                             
      COMMON/ZDUM2A/ uspc2(8,npxpar)                                            
C                                                                               
C                                                                               
C*********************************************************************          
c                                                                               
c     species names,  usp1 .lt. usp2                                            
c                                                                               
C*********************************************************************          
C                                                                               
C                                                                               
C     character*24 usp1(npxpar), usp2(npxpar)                                   
      character*24 usp1, usp2                                                   
      COMMON/ZDUM3/ usp1(npxpar), usp2(npxpar)                                  
c                                                                               
c     common species names   first several characters only                      
c                                                                               
C************************************************************                   
C                                                                               
C                                                                               
C     character*8 uspcm(8,npxpar)                                               
      character*8 uspcm                                                         
      COMMON/ZDUM4/ uspcm(8,npxpar)                                             
c                                                                               
c     maxm, theta                                                               
c                                                                               
C********************************************************************           
C                                                                               
C                                                                               
C      dimension maxm(8,npxpar),theta(8,npxpar)                                 
      COMMON/ZDUM5/ maxm(8,npxpar),theta(8,npxpar)                              
c                                                                               
c     sum of maxm*theta, sum of maxm*dtheta/dt, sum of maxm*d2th/dt2,           
c     and sum of maxm                                                           
c                                                                               
C      dimension sumt(npxpar), sumdt(npxpar),sumd2t(npxpar),summ(npxpar)        
      COMMON/ZDUM6/ sumt(npxpar), sumdt(npxpar),sumd2t(npxpar),                 
     1              summ(npxpar)                                                
c                                                                               
c     average theta, average dtheta/dt, average d2th/dt2                        
c                                                                               
C      dimension atheta(npxpar),dth(npxpar),d2th(npxpar)                        
      COMMON/ZDUM7/ atheta(npxpar),dth(npxpar),d2th(npxpar)                     
c                                                                               
c     index of number of terms in sums  integer                                 
c                                                                               
C      dimension ixs(npxpar)                                                    
      COMMON/ZDUM8/ixs(npxpar)                                                  
c                                                                               
C      character*24 uref(npxpar)                                                
      character*24 uref                                                         
      COMMON/ZDUM9/ uref(npxpar)                                                
c                                                                               
c     array index is nmt, maximum nmt = npxpar                                  
c     number of entries is nmtp                                                 
c     maximum for any single ixs is ixsmax                                      
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     psi array set                                                             
c                                                                               
c     species names  uspp1  .lt.  uspp2   a16 each                              
c                                                                               
C      character*24 uspp1(npxpar), uspp2(npxpar)                                
      character*24 uspp1, uspp2                                                 
      COMMON/ZDUM10/ uspp1(npxpar), uspp2(npxpar)                               
c                                                                               
c     common species names                                                      
c                                                                               
C      character*24 usppc(npxpar)                                               
      character*24 usppc                                                        
      COMMON/ZDUM11/ usppc(npxpar)                                              
c                                                                               
c     psi   real                                                                
c                                                                               
C      dimension psi(npxpar)                                                    
      COMMON/ZDUM12/ psi(npxpar)                                                
c                                                                               
c     comments  a24                                                             
c                                                                               
C      character*24 urefp(npxpar)                                               
      character*24 urefp                                                        
      COMMON/ZDUM13/ urefp(npxpar)                                              
c                                                                               
c     array index is nmp,  maximum nmp is npxpar,                               
c     number of entries is nmpp                                                 
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      data ixsmax /8/                                                           
      data ustop,uendit /'stop.   ','endit.  '/                                 
      data ublank /'        '/                                                  
      data ublk24 /'                        '/                                  
      data ustp24 /'                        '/                                  
      data utrm /'+-------',8*'--------'/                                       
      data zro /0./                                                             
      data uterm /'+-------'/                                                   
      data adum /99./                                                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      uend24(1:8)=uendit(1:8)                                                   
      ustp24(1:8)=ustop(1:8)                                                    
c                                                                               
c     read title line                                                           
c                                                                               
      read (ndpt0s,1010) uttl                                                   
      write (ndpt1,1010) uttl                                                   
 1010 format(a80)                                                               
      write (nout,1012) uttl                                                    
 1012 format('*',10('------'),/,a80)                                            
      write(nout,1013)                                                          
 1013 format(5x,'exist for the following combinations')                         
c                                                                               
      nmt = 0                                                                   
      nmp = 0                                                                   
c                                                                               
c     initialize sum arrays to zeroes                                           
c                                                                               
      do 10 k=1,npxpar                                                          
      sumt(k) = 0.                                                              
      sumdt(k) = 0.                                                             
      sumd2t(k) = 0.                                                            
      summ(k) = 0.                                                              
      ixs(k) = 0                                                                
   10 continue                                                                  
c                                                                               
      nn = 1                                                                    
c                                                                               
c----------begin main loop----------------------------------------------        
c                                                                               
c      read a block from file dpt0                                              
c      species names                                                            
c                                                                               
   20 read (ndpt0s,1020) unam1,unam2,unam3                                      
 1020 format(a12,2(2x,a12))                                                     
c                                                                               
      write (nout,1022) nn,unam1,unam2,unam3                                    
 1022 format(i3,3(2x,a12))                                                      
c                                                                               
c     test for end of data                                                      
c                                                                               
      if (unam1(1:8) .eq. ustop(1:8)) go to 100                                 
c                                                                               
c     read theta, psi, maxm from dpt0                                           
c                                                                               
      read (ndpt0s,1170) thdum,psdum,maxdum                                     
 1170 format(2(13x,f8.4),13x,f5.2)                                              
c                                                                               
c     source                                                                    
c                                                                               
      read (ndpt0s,1172) usrc                                                   
 1172 format(13x,a18)                                                           
c                                                                               
c     tgflg1                                                                    
c                                                                               
      read (ndpt0s,1174) ngflg1                                                 
 1174 format(13x,i2)                                                            
c                                                                               
      if (ngflg1 .ne. 0) then                                                   
        write (nout,1016)                                                       
 1016   format(' ----- tgflg1 .ne. zero is not implemented')                    
        write (nttyo,1016)                                                      
        stop                                                                    
      endif                                                                     
c                                                                               
c     derivatives                                                               
c                                                                               
      read (ndpt0s,1180) dthd,d2thd                                             
 1180 format(13x,e10.3,13x,e10.3)                                               
      read (ndpt0s,1180) dpsid,d2psid                                           
c                                                                               
c     maxm, maxt                                                                
c                                                                               
      read (ndpt0s,1182) maxm2,maxt                                             
 1182 format(13x,f5.1,18x,f5.1)                                                 
c                                                                               
c     source2                                                                   
c                                                                               
      read (ndpt0s,1184) usrc                                                   
 1184 format(13x,a18)                                                           
c                                                                               
c     skip to block terminator line                                             
c                                                                               
   58 read (ndpt0s,1176) udum                                                   
 1176 format(a8)                                                                
      if (udum(1:8) .ne. uterm(1:8)) go to 58                                   
c                                                                               
c     search for species names in uspec0                                        
c                                                                               
      qnf = .false.                                                             
c                                                                               
      do 24 ns=2,nst0                                                           
      if (unam1(1:12) .eq. uspec0(ns)(1:12)) go to 26                           
   24 continue                                                                  
c                                                                               
c     not found, add to not found list                                          
c                                                                               
      call adspn(unam1)                                                         
      qnf = .true.                                                              
   26 continue                                                                  
c                                                                               
      do 28 ns=2,nst0                                                           
      if (unam2(1:12) .eq. uspec0(ns)(1:12)) go to 30                           
   28 continue                                                                  
c                                                                               
c     not found, add to not found list                                          
c                                                                               
      call adspn(unam2)                                                         
      qnf = .true.                                                              
   30 continue                                                                  
c                                                                               
      do 32 ns=2,nst0                                                           
      if (unam3(1:12) .eq. uspec0(ns)(1:12)) go to 36                           
   32 continue                                                                  
c                                                                               
c     not found, add to not found list                                          
c                                                                               
      call adspn(unam3)                                                         
      qnf = .true.                                                              
   36 continue                                                                  
c                                                                               
c     skip if any species not found                                             
c                                                                               
      if (qnf) go to 80                                                         
c                                                                               
      us1 = unam1                                                               
      us2 = unam2                                                               
c                                                                               
      usc = unam3                                                               
c                                                                               
c     put us1,us2 in order                                                      
c                                                                               
      if (us1 .eq. us2) then                                                    
c                                                                               
c       us1 = us2 here - error                                                  
c                                                                               
        write (nout,1033) unam1,unam2,unam3                                     
 1033   format(' +++++ ',3(1x,a12), ' have two common species')                 
        go to 80                                                                
c                                                                               
      elseif (us1 .gt. us2) then                                                
c                                                                               
c        us1 .gt. us2 here  -  exchange                                         
c                                                                               
         udm = us1                                                              
         us1 = us2                                                              
        us2 = udm                                                               
      endif                                                                     
c                                                                               
c      now have us1 .lt. us2, and common species usc                            
c                                                                               
c                             *----------------------------------------         
c                             * save the psi set                                
c                             *----------------------------------------         
c                                                                               
      nmp = nmp+1                                                               
      if (nmp .gt. npxpar) then                                                 
        write (nttyo,1036)                                                      
        write (nout,1036)                                                       
 1036   format(' * error- psi array index overflow, npxpar too small')          
        stop                                                                    
      endif                                                                     
c                                                                               
c     add new values to arrays                                                  
c                                                                               
      uspp1(nmp) = us1                                                          
      uspp2(nmp) = us2                                                          
      usppc(nmp) = usc                                                          
      psi(nmp) = psdum                                                          
      dpsi(nmp) = dpsid                                                         
      d2psi(nmp) = d2psid                                                       
      urefp(nmp) = usrc(1:18)                                                   
c                                                                               
c                             *----------------------------------------         
c                             * save the theta set                              
c                             *----------------------------------------         
c                                                                               
      if (nmt .ge. 1) then                                                      
c                                                                               
c       search usp1,usp2 for us1,us2                                            
c                                                                               
        do 56 k=1,nmt                                                           
        if (us1 .eq. usp1(k)) then                                              
          if (us2 .eq. usp2(k)) then                                            
c                                                                               
c           us2 = usp2 here - found same pair                                   
c                                                                               
            nmx = k                                                             
            go to 70                                                            
          endif                                                                 
        endif                                                                   
   56   continue                                                                
c                                                                               
c       not found                                                               
c                                                                               
      endif                                                                     
c                                                                               
      nmt = nmt+1                                                               
      if (nmt .gt. npxpar) then                                                 
        write (nttyo,1038)                                                      
        write (nout,1038)                                                       
 1038   format(' * error- theta array index overflow, npxpar too small')        
        stop                                                                    
      endif                                                                     
c                                                                               
c     add new values to arrays                                                  
c                                                                               
      usp1(nmt) = us1                                                           
      usp2(nmt) = us2                                                           
      uref(nmt) = usrc                                                          
      nmx = nmt                                                                 
c                                                                               
c      save species names, theta, maxm for output file                          
c                                                                               
   70 ixs(nmx) = ixs(nmx) + 1                                                   
      ix = ixs(nmx)                                                             
      if (ix .gt. ixsmax) then                                                  
        ix = ixsmax                                                             
        write (nout,1041)                                                       
        write (nttyo,1041)                                                      
 1041   format(' * error- ixs array index overflow')                            
      endif                                                                     
c                                                                               
c     save only first 6 characters of common species names                      
c                                                                               
      uspcm(ix,nmx) = usc(1:6)                                                  
      maxm(ix,nmx) = maxdum                                                     
      theta(ix,nmx) = thdum                                                     
      uspc1(ix,nmx) = unam1                                                     
      uspc2(ix,nmx) = unam2                                                     
c                                                                               
c     sum into theta*maxm, dtheta/dt*maxm,  d2th/dt2, and maxm                  
c                                                                               
      mmax = maxm2                                                              
      if (mmax .lt. 1.) mmax = 1.                                               
      sumt(nmx) = sumt(nmx) + thdum*mmax                                        
      sumdt(nmx) = sumdt(nmx) + dthd*mmax                                       
      sumd2t(nmx) = sumd2t(nmx) + d2thd*mmax                                    
      summ(nmx) = summ(nmx) + mmax                                              
c                                                                               
   80 nn = nn+1                                                                 
      go to 20                                                                  
c                                                                               
c----------end of main loop---------------------------------------------        
c                                                                               
  100 nmtp = nmt                                                                
      nmpp = nmp                                                                
c                                                                               
c     have data, write two sets to dpt1                                         
c                                                                               
      if (.not.qdbug2) then                                                     
        write (ndbfl3,1110)                                                     
 1110   format('dbfil3 from eqpitz',/,                                          
     $  '  mixture term parameter arrays',/)                                    
        write (ndbfl3,1112) nmtp                                                
 1112   format(2x,i4,' values in theta arrays',/)                               
        write (ndbfl3,1114)                                                     
 1114   format(5x,'sumt',5x,'summ',3x,'ixs')                                    
        write (ndbfl3,1116) (sumt(i),summ(i),ixs(i),i=1,nmtp)                   
 1116   format((2x,f8.4,2x,f6.1,2x,i2))                                         
      endif                                                                     
c                                                                               
c                             *----------------------------------------         
c                             * write theta set to dpt1                         
c                             *----------------------------------------         
c                                                                               
      write (ndpt1,1060)                                                        
 1060 format('*',5x,'theta set')                                                
      write (ndpt1,1062)                                                        
 1062 format('*  note that alphas=99. since lambda1=lambda2=zero')              
c                                                                               
      do 110 k=1,nmtp                                                           
c                                                                               
      do 116 j=1,ixsmax                                                         
      usdum(j) = ublank                                                         
  116 continue                                                                  
c                                                                               
c     get common species names                                                  
c                                                                               
      ixn = ixs(k)                                                              
      do 109 ii=1,ixn                                                           
      usdum(ii) = uspcm(ii,k)                                                   
  109 continue                                                                  
c                                                                               
      sm = summ(k)                                                              
      ath = sumt(k) / sm                                                        
      dtheta = sumdt(k) / sm                                                    
      d2thet = sumd2t(k) / sm                                                   
c                                                                               
      atheta(k) = ath                                                           
      dth(k) = dtheta                                                           
      d2th(k) = d2thet                                                          
c                                                                               
c     write a block                                                             
c                                                                               
      write (ndpt1,1210) usp1(k),usp2(k)                                        
 1210 format(a12,2x,a12)                                                        
      write (ndpt1,1214)                                                        
 1214 format('* common species')                                                
      write (ndpt1,1216) (usdum(i),i=1,ixn)                                     
 1216 format(('* ',a6,2(2x,a6)))                                                
c                                                                               
      write (ndpt1,1218)                                                        
 1218 format('*')                                                               
c                                                                               
      write (ndpt1,1220) ath,dtheta,d2thet                                      
 1220 format(5x,'theta = ',f8.4,5x,'dth/dt = ',1pe10.3,                         
     $       2x,'d2th/dt2 = ',1pe10.3)                                          
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt1,1222)                                                        
 1222 format('+',12('------'))                                                  
c                                                                               
  110 continue                                                                  
c                                                                               
      write (ndpt1,1052) uendit,uendit                                          
 1052 format(a8,50x,a8,/,'*',10('------'))                                      
c                                                                               
c     write theta information to output                                         
c                                                                               
c     write (nout,1080)                                                         
c1080 format(/,'*',10('-------'))                                               
c     write (nout,1081)                                                         
c1081 format(10x,'theta information',/)                                         
c                                                                               
c     do 114 k=1,nmtp                                                           
c     write (nout,1082) usp1(k),usp2(k),                                        
c    $  atheta(k)                                                               
c1082 format(' theta (',a12,',',a12,') = ',f7.4)                                
c     write (nout,1084)                                                         
c1084 format(8x,'species 1',5x,'species 2',5x,'species 3',                      
c    $   4x,'maxm',4x,'theta')                                                  
c     ixt = ixs(k)                                                              
c                                                                               
c     do 112 j=1,ixt                                                            
c     write (nout,1086) uspc1(j,k),uspc2(j,k),                                  
c    $  uspcm(j,k),                                                             
c    $  maxm(j,k),theta(j,k)                                                    
c1086 format(8x,a12,2x,a12,2x,a6,8x,f4.1,2x,f7.4)                               
c 112 continue                                                                  
c                                                                               
c     write (nout,1088)                                                         
c1088 format(' ')                                                               
c                                                                               
c 114 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * write psi set to dpt1                           
c                             *----------------------------------------         
c                                                                               
      write (ndpt1,1064)                                                        
 1064 format('*',5x,'psi set')                                                  
c                                                                               
      do 120 k=1,nmpp                                                           
c                                                                               
c     write a block                                                             
c                                                                               
      write (ndpt1,1230) uspp1(k),uspp2(k),usppc(k)                             
 1230 format(a12,2(2x,a12))                                                     
      write (ndpt1,1218)                                                        
c                                                                               
      write (ndpt1,1232) psi(k),dpsi(k),d2psi(k)                                
 1232 format(7x,'psi = ',f8.4,3x,'dpsi/dt = ',1pe10.3,                          
     $       1x,'d2psi/dt2 = ',1pe10.3)                                         
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt1,1222)                                                        
  120 continue                                                                  
c                                                                               
      write (ndpt1,1052) uendit,uendit                                          
      write (ndpt1,1070) ustop                                                  
 1070 format(a8)                                                                
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      if (.not.qdbug2) then                                                     
        write (ndbfl3,1150)                                                     
 1150   format(/,'umsp',10x,'uxsp',10x,'zm',3x,'zx')                            
        write (ndbfl3,1152) (umsp(i),uxsp(i),                                   
     $  zm(i),zx(i),i=1,npxp)                                                   
 1152   format((2(2x,a12),2(2x,f3.0)))                                          
      endif                                                                     
c                                                                               
c                             *----------------------------------------         
c                             * write two lambda sets to files                  
c                             *  data1, data2, dpt2                             
c                             *----------------------------------------         
c                                                                               
c     beta set                                                                  
c                                                                               
      write (ndpt2,1050)                                                        
 1050 format('*',10('-------'))                                                 
      write (ndpt2,1140)                                                        
 1140 format('*',10x,'beta set of lambda-s')                                    
c                                                                               
      do 150 k=1,npxp                                                           
c                                                                               
c     write a block                                                             
c                                                                               
      write (ndpt2,1240) umsp(k),uxsp(k)                                        
      write (ndata1) umsp(k),uxsp(k)                                            
      write (ndata2) umsp(k),uxsp(k)                                            
 1240 format(a12,2x,a12)                                                        
      write (ndpt2,1242)                                                        
 1242 format('*')                                                               
c                                                                               
      write (ndpt2,1244) l0(k),l1(k),l2(k)                                      
      write (ndata1) l0(k),l1(k),l2(k)                                          
      write (ndata2) l0(k),l1(k),l2(k)                                          
 1244 format(3x,'lambda0 = ',f8.4,                                              
     $       3x,'lambda1 = ',f8.4,                                              
     $       3x,'lambda2 = ',f8.4)                                              
      write (ndpt2,1246) alph1(k),alph2(k)                                      
      write (ndata1) alph1(k),alph2(k)                                          
      write (ndata2) alph1(k),alph2(k)                                          
 1246 format(25x,'alpha1 = ',f5.1,7x,'alpha2 = ',f5.1)                          
      write (ndpt2,1242)                                                        
c                                                                               
      write (ndpt2,1248) dl0(k),d2l0(k)                                         
      write (ndata1) dl0(k),d2l0(k)                                             
      write (ndata2) dl0(k),d2l0(k)                                             
 1248 format(4x,'dl0/dt = ',1pe10.3,2x,'d2l0/dt2 = ',1pe10.3)                   
      write (ndpt2,1250) dl1(k),d2l1(k)                                         
      write (ndata1) dl1(k),d2l1(k)                                             
      write (ndata2) dl1(k),d2l1(k)                                             
 1250 format(4x,'dl1/dt = ',1pe10.3,2x,'d2l1/dt2 = ',1pe10.3)                   
      write (ndpt2,1252) dl2(k),d2l2(k)                                         
      write (ndata1) dl2(k),d2l2(k)                                             
      write (ndata2) dl2(k),d2l2(k)                                             
 1252 format(4x,'dl2/dt = ',1pe10.3,2x,'d2l2/dt2 = ',1pe10.3)                   
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt2,1222)                                                        
      write (ndata1) (utrm(i),i=1,9)                                            
      write (ndata2) (utrm(i),i=1,9)                                            
  150 continue                                                                  
c                                                                               
      write (ndpt2,1052) uendit,uendit                                          
      write (ndata1) uend24,ublk24,ublk24                                       
      write (ndata2) uend24,ublk24,ublk24                                       
 1053 format(a8,50x,a8)                                                         
c                                                                               
c     theta set                                                                 
c                                                                               
      write (ndpt2,1146)                                                        
 1146 format('*',10x,'theta set of lambda-s')                                   
c                                                                               
      do 156 k=1,nmtp                                                           
c                                                                               
c     write a block                                                             
c                                                                               
      write (ndpt2,1240) usp1(k),usp2(k)                                        
      write (ndata1) usp1(k),usp2(k)                                            
      write (ndata2) usp1(k),usp2(k)                                            
      write (ndpt2,1242)                                                        
c                                                                               
c     lambda0,1,2                                                               
c                                                                               
      write (ndpt2,1244) atheta(k),zro,zro                                      
      write (ndata1) atheta(k),zro,zro                                          
      write (ndata2) atheta(k),zro,zro                                          
c                                                                               
      write (ndpt2,1246) adum,adum                                              
      write (ndata1) adum,adum                                                  
      write (ndata2) adum,adum                                                  
c                                                                               
      write (ndpt2,1242)                                                        
c                                                                               
c     derivatives, l0,l1,l2                                                     
c                                                                               
      write (ndpt2,1248) dth(k),d2th(k)                                         
      write (ndpt2,1250) zro,zro                                                
      write (ndpt2,1252) zro,zro                                                
c                                                                               
      write (ndata1) dth(k),d2th(k)                                             
      write (ndata1) zro,zro                                                    
      write (ndata1) zro,zro                                                    
c                                                                               
      write (ndata2) dth(k),d2th(k)                                             
      write (ndata2) zro,zro                                                    
      write (ndata2) zro,zro                                                    
c                                                                               
c      block terminator                                                         
c                                                                               
      write (ndpt2,1222)                                                        
      write (ndata1) (utrm(i),i=1,9)                                            
      write (ndata2) (utrm(i),i=1,9)                                            
  156 continue                                                                  
c                                                                               
      write (ndpt2,1052) uendit,uendit                                          
      write (ndata1) uend24,ublk24,ublk24                                       
      write (ndata2) uend24,ublk24,ublk24                                       
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c                             *----------------------------------------         
c                             * compute mu(mmx) and mu(mxx)                     
c                             * write cphi set to dpt2                          
c                             *----------------------------------------         
c                                                                               
      write (ndpt2,1030)                                                        
 1030 format('*',10x,'cphi set of mu-s')                                        
c                                                                               
      do 160 k=1,npxp                                                           
      if (zm(k) .eq. 0. .or. zx(k) .eq. 0.) go to 160                           
c                                                                               
c     compute mu(mmx), mu(mxx), and derivatives                                 
c                                                                               
      cmmx = sqrt(-zm(k)/zx(k)) / 6.                                            
      cmxx = sqrt(-zx(k)/zm(k)) / 6.                                            
      mummx(k) = cph(k) * cmmx                                                  
      mumxx(k) = cph(k) * cmxx                                                  
      dmmx(k) = dc(k) * cmmx                                                    
      dmxx(k) = dc(k) * cmxx                                                    
      d2mmx(k) = d2c(k)*cmmx                                                    
      d2mxx(k) = d2c(k)*cmxx                                                    
c                                                                               
c     write first block                                                         
c                                                                               
      write (ndpt2,1260) umsp(k),uxsp(k),umsp(k)                                
      write (ndata1) umsp(k),uxsp(k),umsp(k)                                    
      write (ndata2) umsp(k),uxsp(k),umsp(k)                                    
 1260 format(a12,2(2x,a12))                                                     
      write (ndpt2,1262)                                                        
 1262 format('*')                                                               
c                                                                               
      write (ndpt2,1264) mummx(k),dmmx(k),d2mmx(k)                              
      write (ndata1) mummx(k),dmmx(k),d2mmx(k)                                  
      write (ndata2) mummx(k),dmmx(k),d2mmx(k)                                  
 1264 format(8x,'mu = ',f9.5,2x,'dmmx/dt = ',1pe10.3,                           
     $       2x,'d2mmx/dt2 = ',1pe10.3)                                         
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt2,1222)                                                        
      write (ndata1) (utrm(i),i=1,9)                                            
      write (ndata2) (utrm(i),i=1,9)                                            
c                                                                               
c     write second block                                                        
c                                                                               
      write (ndpt2,1260) umsp(k),uxsp(k),uxsp(k)                                
      write (ndpt2,1262)                                                        
      write (ndata1) umsp(k),uxsp(k),uxsp(k)                                    
      write (ndata2) umsp(k),uxsp(k),uxsp(k)                                    
c                                                                               
      write (ndpt2,1266) mumxx(k),dmxx(k),d2mxx(k)                              
      write (ndata1) mumxx(k),dmxx(k),d2mxx(k)                                  
      write (ndata2) mumxx(k),dmxx(k),d2mxx(k)                                  
 1266 format(8x,'mu = ',f9.5,2x,'dmxx/dt = ',1pe10.3,                           
     $       2x,'d2mxx/dt2 = ',1pe10.3)                                         
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt2,1222)                                                        
      write (ndata1) (utrm(i),i=1,9)                                            
      write (ndata2) (utrm(i),i=1,9)                                            
  160 continue                                                                  
c                                                                               
      write (ndpt2,1052) uendit,uendit                                          
      write (ndata1) uend24,ublk24,ublk24                                       
      write (ndata2) uend24,ublk24,ublk24                                       
c                                                                               
c                            *-----------------------------------------         
c                             * compute mu(3 ions),                             
c                            *  write psi set to file dpt2                      
c                            *-----------------------------------------         
c                                                                               
      write (ndpt2,1120)                                                        
 1120 format('*',10x,'psi set of mu-s')                                         
c                                                                               
      if (.not.qdbug2) then                                                     
        write (ndbfl3,1154)                                                     
 1154   format('u1',11x,'uc',14x,'j',3x,'mu 1',6x,'z1')                         
        write (ndbfl3,1156)                                                     
 1156   format(38x,'u2',11x,'uc',14x,'j',3x,'mu 2',6x,'z2')                     
      endif                                                                     
c                                                                               
      do 140 k=1,nmpp                                                           
      u1 = uspp1(k)                                                             
      u2 = uspp2(k)                                                             
      uc = usppc(k)                                                             
c                                                                               
c     search for (uspp1, usppc) pair in arrays (umsp, uxsp)                     
c                                                                               
      call srch22(u1,uc,umsp,uxsp,npxp,j,jj)                                    
c                                                                               
      if (j .le. 0) then                                                        
c                                                                               
c       error, not found                                                        
c                                                                               
        write (nttyo,1122) u1,uc                                                
        write (nout,1122) u1,uc                                                 
 1122   format(' *error- ',2(a12,2x),' not found in psi set ',                  
     $  'construction')                                                         
        stop                                                                    
      endif                                                                     
c                                                                               
c     found first pair - get mu, derivative, charge z1                          
c                                                                               
      mu1 = mummx(j)                                                            
      dm1 = dmmx(j)                                                             
      d2m1 = d2mmx(j)                                                           
      z1 = zm(j)                                                                
      if (jj .le. 0) then                                                       
c                                                                               
c       mxx here                                                                
c                                                                               
        mu1 = mumxx(j)                                                          
        dm1 = dmxx(j)                                                           
        d2m1 = d2mxx(j)                                                         
        z1 = zx(j)                                                              
      endif                                                                     
c                                                                               
      if (.not.qdbug2) write (ndbfl3,1158) u1,uc,j,mu1,z1                       
 1158 format(2(a12,2x),i2,2x,f8.5,2x,f3.0)                                      
c                                                                               
c     search for (uspp2,usppc) pair in arrays (umsp,uxsp)                       
c                                                                               
      call srch22(u2,uc,umsp,uxsp,npxp,j,jj)                                    
c                                                                               
      if (j .le. 0) then                                                        
c                                                                               
c       error - not found                                                       
c                                                                               
        write (nttyo,1122) u2,uc                                                
        write (nout,1122) u2,uc                                                 
        stop                                                                    
      endif                                                                     
c                                                                               
c     found second pair - get mu, derivatives, charge z2,                       
c     and dpsi derivatives                                                      
c                                                                               
      mu2 = mummx(j)                                                            
      dm2 = dmmx(j)                                                             
      d2m2 = d2mmx(j)                                                           
      z2 = zm(j)                                                                
      if (jj .le. 0) then                                                       
c                                                                               
c       mxx here                                                                
c                                                                               
        mu2 = mumxx(j)                                                          
        dm2 = dmxx(j)                                                           
        d2m2 = d2mxx(j)                                                         
        z2 = zx(j)                                                              
      endif                                                                     
c                                                                               
      if (z1 .eq. 0. .or. z2 .eq. 0.) go to 140                                 
      dps = dpsi(k)                                                             
      d2ps = d2psi(k)                                                           
c                                                                               
      if (.not.qdbug2) write (ndbfl3,1160) u2,uc,j,mu2,z2                       
 1160 format(38x,2(2x,a12),i2,2x,f8.5,2x,f3.0,/)                                
c                                                                               
c     compute mu(3 ions), derivatives                                           
c                                                                               
      mumnx = (psi(k) + 3.0*((z2/z1)*mu1 + (z1/z2)*mu2)) / 6.                   
      dmu =   (dps +    3.0*((z2/z1)*dm1 + (z1/z2)*dm2)) / 6.                   
      d2mu =  (d2ps +   3.0*((z2/z1)*d2m1+ (z1/z2)*d2m2))/ 6.                   
c                                                                               
c     write a block                                                             
c                                                                               
      write (ndpt2,1260) u1,u2,uc                                               
      write (ndpt2,1262)                                                        
      write (ndpt2,1270) mumnx,dmu,d2mu                                         
      write (ndata1) u1,u2,uc                                                   
      write (ndata1) mumnx,dmu,d2mu                                             
      write (ndata2) u1,u2,uc                                                   
      write (ndata2) mumnx,dmu,d2mu                                             
 1270 format(8x,'mu = ',f9.5,3x,'dmu/dt = ',1pe10.3,                            
     $       3x,'d2mu/dt2 = ',1pe10.3)                                          
c                                                                               
c     block terminator                                                          
c                                                                               
      write (ndpt2,1222)                                                        
      write (ndata1) (utrm(i),i=1,9)                                            
      write (ndata2) (utrm(i),i=1,9)                                            
  140 continue                                                                  
c                                                                               
      write (ndpt2,1052) uendit,uendit                                          
      write (ndpt2,1070) ustop                                                  
      write (ndata1) uend24,ublk24,ublk24                                       
      write (ndata1) ustp24,ublk24,ublk24                                       
      write (ndata2) uend24,ublk24,ublk24                                       
      write (ndata2) ustp24,ublk24,ublk24                                       
c                                                                               
  999 continue                                                                  
      end                                                                       
c preacy   last revised 01/29/87 by tjw                                         
c*preacy f77 rewrite 01/29/87 by tjw                                            
      subroutine preacy(ns,iarg,nfile)                                          
c                                                                               
c     input                                                                     
c                                                                               
c       ns = species whose reaction is to be printed                            
c       iarg = 0   long format                                                  
c            = 1   short format                                                 
c       nfile = unit number of the file to write on                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqttt.h'                                                  
C     include '../inc/un.h'                                                     
C     include '../inc/xx.h'                                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      nsl=nsq                                                                   
      if (iarg.ge.1) nsl=nsb                                                    
      nsl1=nsl+1                                                                
      cdum3=-cdrs(nsl1,ns)                                                      
      write (nfile,105) cdum3,uspec(ns)                                         
  105 format(/,21x,f10.5,2x,a18)                                                
c                                                                               
      do 5 nse=1,nsl                                                            
      cdum3=-cdrs(nse,ns)                                                       
      if (cdum3.gt.0.) then                                                     
        write (nfile,110) cdum3,uspec(nse)                                      
  110   format(18x,' + ',f10.5,2x,a18)                                          
      endif                                                                     
    5 continue                                                                  
c                                                                               
      write (nfile,115)                                                         
  115 format(/31x,'='/)                                                         
c                                                                               
      nj=0                                                                      
      do 10 nse=1,nsl                                                           
      cdum3=cdrs(nse,ns)                                                        
      if (cdum3.gt.0.) then                                                     
        if (nj.le.0) then                                                       
          write (nfile,107) cdum3,uspec(nse)                                    
  107     format(21x,f10.5,2x,a18)                                              
          nj=1                                                                  
        else                                                                    
          write (nfile,110) cdum3,uspec(nse)                                    
        endif                                                                   
      endif                                                                     
   10 continue                                                                  
c                                                                               
      end                                                                       
c rdpar    last revised 03/23/87 by mlc                                         
c*rdpar f77 rewrite 01/30/87 by tjw                                             
      subroutine rdpar                                                          
c                                                                               
c     read parameters from first part of data0 -                                
c       element data                                                            
c       temperatures, pressures, ...                                            
c     write to files data1, data2, data3                                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ee.h'                                                     
C     include '../inc/hh.h'                                                     
C     include '../inc/hhn.h'                                                    
C     include '../inc/key.h'                                                    
C     include '../inc/xx.h'                                                     
C     include '../inc/un.h'                                                     
C     include '../inc/eqtti.h'                                                  
C     include '../inc/eqttt.h'                                                  
C     include '../inc/eqtver.h'                                                 
c                                                                               
      character*80 udum                                                         
      character*24 uend24,ublk24                                                
      character*8 uname(17)                                                     
c                                                                               
      dimension gdum(8),gridb(9),apr(10)                                        
c                                                                               
      data uendit/'endit.  '/,ublank/'        '/                                
      data ublk24 /'                        '/                                  
      data udat1,udat2,udat3 /'data1   ','data2   ','data3    '/                
      data udata0/'data0   '/                                                   
      data (uname(i), i=1,17)/'temp    ','press   ','adh     ',                 
     $  'bdh     ','bdot    ',                                                  
     $  'cco2 1  ','cco2 2  ','cco2 3  ','cco2 4  ','ch2o 1  ',                 
     $  'ch2o 2  ','ch2o 3  ','ch2o 4  ','xlkeh   ','xlko2   ',                 
     $  'xlkh2   ','xlkn2   '/                                                  
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      uend24(1:8)=uendit(1:8)                                                   
      write (noutpt,1010) urelno,ustage,ueqlrn,ueqlst                           
      write (nslist,1010) urelno,ustage,ueqlrn,ueqlst                           
c     write (nttyo,1010) urelno,ustage,ueqlrn,ueqlst                            
 1010 format(1x,'eq6.',a4,a6,/2x,'supported by eqlib.',a4,a6,//)                
c                                                                               
      rewind nda0                                                               
      rewind ndata1                                                             
      rewind ndata2                                                             
      rewind ndata3                                                             
      read (nda0,1012) udum                                                     
 1012 format(a80)                                                               
      if (udum(1:6) .ne. udata0(1:6)) then                                      
        write (noutpt,1014) udum(1:6)                                           
        write (nslist,1014) udum(1:6)                                           
 1014   format(/1x,'bad file header = ',a6,' on file DATA0')                    
        write (nttyo,2000)                                                      
 2000   format(' * bad file header on DATA0')                                   
        stop                                                                    
      endif                                                                     
c                                                                               
      write (ndata1) udat1                                                      
      write(ndata1) ukey                                                        
c                                                                               
      write (ndata2) udat2                                                      
      write(ndata2) ukey                                                        
c                                                                               
      write (ndata3) udat3                                                      
      write(ndata3) ukey                                                        
c                                                                               
      read (nda0,1022) nct,nsq                                                  
      write (ndata1) nct,nsq                                                    
      write (ndata2) nct,nsq                                                    
 1022 format(2i5)                                                               
c                                                                               
      nsb=nct+1                                                                 
      nsb1=nsb+1                                                                
      nsb2=nsb+2                                                                
      nsq1=nsq+1                                                                
      nsq2=nsq+2                                                                
      write (noutpt,1026) nct,nctpar,nsq,nsqpar                                 
      write (nslist,1026) nct,nctpar,nsq,nsqpar                                 
 1026 format(11x,'no. of elements on the data file = ',i5,/14x,'the',           
     $ ' dimensioned limit = ',i5,/11x,'no. of aqueous species',                
     $ ' in the master set = ',i5,/14x,'the dimensioned limit = ',              
     $ i5,/)                                                                    
      if (nct.gt.nctpar) then                                                   
        write (nttyo,2002)                                                      
 2002   format(' * error  nct .gt. nctpar')                                     
        stop                                                                    
      endif                                                                     
c                                                                               
      if (nsq.gt.nsqpar) then                                                   
        write (nttyo,2004)                                                      
 2004   format(' * error  nsq .gt. nsqpar')                                     
        stop                                                                    
      endif                                                                     
c                                                                               
      read(nda0,1012) utitld                                                    
      do 25 k=1,4                                                               
      write(ndata1) utitld(k)                                                   
      write(ndata2) utitld(k)                                                   
   25 continue                                                                  
c                                                                               
      write(noutpt,1028) utitld                                                 
      write(nslist,1028) utitld                                                 
 1028 format(//,4(1x,a80,/),///)                                                
c                                                                               
      read (nda0,1012) udum                                                     
      write (ndata1) udum                                                       
      write (ndata2) udum                                                       
c                                                                               
      do 27 nc=1,nctpar                                                         
      uelem(nc)=ublank                                                          
   27 continue                                                                  
c                                                                               
      do 30 nc=1,nct                                                            
      read (nda0,1030) uelem(nc),atwt(nc),uoxide,oxfac                          
      write (ndata1) uelem(nc),atwt(nc),uoxide,oxfac                            
      write (ndata2) uelem(nc),atwt(nc),uoxide,oxfac                            
 1030 format(a8,f10.5,5x,a8,5x,f10.5)                                           
      write (noutpt,1032) uelem(nc),atwt(nc)                                    
      write (nslist,1032) uelem(nc),atwt(nc)                                    
 1032 format(1x,'element = ',a8,', atwt = ',f10.5)                              
   30 continue                                                                  
c                                                                               
c     read in temperatures                                                      
c                                                                               
      read (nda0,1034) (tempc(i), i=1,8)                                        
 1034 format(/,5x,4f10.4,/,5x,4f10.4)                                           
c                                                                               
c     scale for interpolation                                                   
c                                                                               
      call scal(tempc,8,tempcs,tmax,ier)                                        
c                                                                               
      read (nda0,1034) (press(i), i=1,8)                                        
      qall=.true.                                                               
      call intrp(qall,press,gridb,apr,j1,j2)                                    
      write (ndata1) uname(2),(apr(i), i=1,10)                                  
c1036 format(a6,/,5(1pe16.9),/,5(1pe16.9))                                      
      write (ndata3) (apr(i), i=1,10)                                           
c1038 format(5(1pe16.9),/,5(1pe16.9))                                           
      write (noutpt,1040) uname(2)                                              
 1040 format(7x,a6)                                                             
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(press,gridb,rms1,rms2,noutpt)                                
c                                                                               
      read (nda0,1034) (adh(i), i=1,8)                                          
      call intrp(qall,adh,gridb,apr,j1,j2)                                      
      write (ndata1) uname(3),(apr(i), i=1,10)                                  
      write (ndata3) (apr(i), i=1,10)                                           
      write (noutpt,1040) uname(3)                                              
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(adh,gridb,rms1,rms2,noutpt)                                  
c                                                                               
      read (nda0,1034) (bdh(i), i=1,8)                                          
      call intrp(qall,bdh,gridb,apr,j1,j2)                                      
      write (ndata1) uname(4),(apr(i), i=1,10)                                  
      write (ndata3) (apr(i), i=1,10)                                           
      write (noutpt,1040) uname(4)                                              
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(bdh,gridb,rms1,rms2,noutpt)                                  
c                                                                               
      read (nda0,1034) (bdot(i), i=1,8)                                         
      call intrp(qall,bdot,gridb,apr,j1,j2)                                     
      write (ndata1) uname(5),(apr(i), i=1,10)                                  
      write (ndata3) (apr(i), i=1,10)                                           
      write (noutpt,1040) uname(5)                                              
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy (bdot,gridb,rms1,rms2,noutpt)                                
c                                                                               
      do   50 j=1,4                                                             
      read (nda0,1042) (cco2(i,j), i=1,8)                                       
 1042 format(/,5x,4(e10.3),/,5x,4(e10.3))                                       
c                                                                               
      do  60 i=1,8                                                              
      gdum(i)=cco2(i,j)                                                         
   60 continue                                                                  
c                                                                               
      call intrp(qall,gdum,gridb,apr,j1,j2)                                     
      k = j+5                                                                   
      write (ndata1) uname(k),(apr(i), i=1,10)                                  
      write (ndata3) (apr(i), i=1,10)                                           
      write (noutpt,1040) uname(k)                                              
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(gdum,gridb,rms1,rms2,noutpt)                                 
   50 continue                                                                  
c                                                                               
      do   70 j=1,4                                                             
      read (nda0,1042) (ch2o(i,j), i=1,8)                                       
c                                                                               
      do  65 i=1,8                                                              
      gdum(i)=ch2o(i,j)                                                         
   65 continue                                                                  
c                                                                               
      call intrp(qall,gdum,gridb,apr,j1,j2)                                     
      k=j+9                                                                     
      write (ndata1) uname(k),(apr(i), i=1,10)                                  
      write (ndata3) (apr(i), i=1,10)                                           
      write (noutpt,1040) uname(k)                                              
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(gdum,gridb,rms1,rms2,noutpt)                                 
   70 continue                                                                  
c                                                                               
      read (nda0,1034) (xlkeh(i), i=1,8)                                        
      call intrp(qall,xlkeh,gridb,apr,j1,j2)                                    
      write (ndata1) uname(14),(apr(i), i=1,10)                                 
      write (ndata3) (apr(i), i=1,10)                                           
      write (noutpt,1040) uname(14)                                             
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(xlkeh,gridb,rms1,rms2,noutpt)                                
      write (ndata3) uend24,ublk24,ublk24                                       
c                                                                               
      read (nda0,1034) (xlko2(i), i=1,8)                                        
      call intrp(qall,xlko2,gridb,apr,j1,j2)                                    
      write (ndata1) uname(15),(apr(i), i=1,10)                                 
      write (noutpt,1040) uname(15)                                             
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(xlko2,gridb,rms1,rms2,noutpt)                                
c                                                                               
      read (nda0,1034) (xlkh2(i), i=1,8)                                        
      call intrp(qall,xlkh2,gridb,apr,j1,j2)                                    
      write (ndata1) uname(16),(apr(i), i=1,10)                                 
      write (noutpt,1040) uname(16)                                             
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(xlkh2,gridb,rms1,rms2,noutpt)                                
c                                                                               
      read (nda0,1034) (xlkn2(i), i=1,8)                                        
      call intrp(qall,xlkn2,gridb,apr,j1,j2)                                    
      write (ndata1) uname(17),(apr(i), i=1,10)                                 
      write (noutpt,1040) uname(17)                                             
      if (rms1.gt.1.e-4 .or. rms2.gt.1.e-4)                                     
     $ call gridpy(xlkn2,gridb,rms1,rms2,noutpt)                                
c                                                                               
      end                                                                       
c rxnchk   last revised 07/29/87 by rlh                                         
c*rxnchk f77 rewrite 01/30/87 by tjw                                            
      subroutine rxnchk(ns)                                                     
c                                                                               
c     this routine checks the ns-th reaction for mass and charge                
c     balance.  if an error is found, a warning message is printed.             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/blank.h'                                                  
C     include '../inc/eqtaa.h'                                                  
C     include '../inc/ab.h'                                                     
C     include '../inc/ee.h'                                                     
C     include '../inc/hh.h'                                                     
C     include '../inc/re.h'                                                     
C     include '../inc/eqttt.h'                                                  
C     include '../inc/uu.h'                                                     
C     include '../inc/un.h'                                                     
C     include '../inc/xx.h'                                                     
c                                                                               
      data rxntol /1.e-6/                                                       
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      ner=0                                                                     
      cess(1,nsb)=2.                                                            
      nlim=ns-1                                                                 
      nlim=min(nlim,nsq)                                                        
c                                                                               
      zdum=cdrs(nsq1,ns)*z(ns)                                                  
      do 21 nse=1,nlim                                                          
      zdum=zdum+cdrs(nse,ns)*z(nse)                                             
   21 continue                                                                  
      adum=abs(zdum)                                                            
      if (adum.gt.rxntol) then                                                  
        write (noutpt,200) zdum                                                 
  200   format(/1x,'----- electrical imbalance = ',1pe13.6,' for the ',         
     $  'reaction below -----')                                                 
        write (nslist,200) zdum                                                 
c       write (nttyo,200) zdum                                                  
        ner=ner+1                                                               
      endif                                                                     
c                                                                               
      do 23 nc=1,nct                                                            
      edum=cdrs(nsq1,ns)*cess(nc,ns)                                            
      do 22 nse=1,nlim                                                          
      edum=edum+cdrs(nse,ns)*cess(nc,nse)                                       
   22 continue                                                                  
      adum=abs(edum)                                                            
      if (adum.gt.rxntol) then                                                  
        write (noutpt,210) uelem(nc),edum                                       
  210   format(/1x,'----- ',a3,' mass imbalance = ',1pe13.6,' for the ',        
     $  'reaction below -----')                                                 
        write (nslist,210) uelem(nc),edum                                       
c       write (nttyo,210) uelem(nc),edum                                        
        ner=ner+1                                                               
      endif                                                                     
   23 continue                                                                  
c                                                                               
      cess(1,nsb)=0.                                                            
      if (ner.gt.0) then                                                        
        call preacy(ns,0,noutpt)                                                
        call preacy(ns,0,nslist)                                                
c       call preacy(ns,0,nttyo)                                                 
      endif                                                                     
c                                                                               
      end                                                                       
c srch22   last revised 03/03/87 by tjw                                         
c*srch22 f77 rewrite 01/30/87 by tjw                                            
      subroutine srch22(ua,ub,uc,ud,n,k,i)                                      
c                                                                               
c     input two 24-character strings ua and ub                                  
c     and two 24-character parallel arrays uc and ud, n pairs each.             
c                                                                               
c     search arrays uc,ud for pairs ua,ub                                       
c                                                                               
c     notes - 1)  ua may occur in uc or ud, and ub may                          
c                 occur in ud or uc, but never both ua and ub                   
c                 in only one array.                                            
c             2)  (ua,ub) must be found at the same index                       
c                 position in (uc,ud)                                           
c                                                                               
c     return  k = -1 if not found                                               
c               =  index position in arrays if found                            
c             i = +1  (ua,ub) found in (uc,ud)                                  
c               = -1    "       "    " (ud,uc)                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/un.h'                                                     
c                                                                               
      character*24 ua,ub                                                        
      character*24 uc(*),ud(*)                                                  
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     set not found                                                             
c                                                                               
      k = -1                                                                    
c                                                                               
c     search for ua in array uc                                                 
c                                                                               
      do 10 j=1,n                                                               
      if (ua(1:24) .eq. uc(j)(1:24)) then                                       
c                                                                               
c       ua found in uc - test for ub at same index in ud                        
c                                                                               
        if (ub(1:24) .ne. ud(j)(1:24)) go to 10                                 
c                                                                               
c       found in (uc,ud)                                                        
c                                                                               
        i = 1                                                                   
        k = j                                                                   
        go to 999                                                               
c                                                                               
      endif                                                                     
   10 continue                                                                  
c                                                                               
c     ua not in uc - search for ua in array ud                                  
c                                                                               
      do 50 j=1,n                                                               
      if (ua(1:24) .eq. ud(j)(1:24)) then                                       
c                                                                               
c       ua found in ud - test for ub at same index in nc                        
c                                                                               
        if (ub(1:24) .ne. uc(j)(1:24)) go to 50                                 
c                                                                               
c       found in (ud,uc)                                                        
c                                                                               
        i = -1                                                                  
        k = j                                                                   
        go to 999                                                               
c                                                                               
      endif                                                                     
   50 continue                                                                  
c                                                                               
c     ua not found in uc or in ud arrays                                        
c                                                                               
  999 continue                                                                  
      end                                                                       
c srchn2   last revised 01/30/87 by tjw                                         
c*srchn2 f77 rewrite 01/30/87 by tjw                                            
      subroutine srchn2(unm,unam,n,inx)                                         
c                                                                               
c     input   unm   ascii species name a16                                      
c             unam  array of ascii species names  a16 each                      
c             n     number of entries in unam                                   
c     output  inx   index positon of unm in unam                                
c                   inx = -1 if not found                                       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
      include "eqptr71.inc"                                                     
C                                                                               
C                                                                               
C  ************************************************                             
C     include '../inc/implicit.h'                                               
c                                                                               
C     include '../inc/parset.h'                                                 
c                                                                               
C     include '../inc/un.h'                                                     
c                                                                               
      character*16 unm,unam(*)                                                  
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      do 10 k=1,n                                                               
      if (unm(1:16) .eq. unam(k)(1:16)) then                                    
        inx = k                                                                 
        go to 999                                                               
      endif                                                                     
   10 continue                                                                  
c                                                                               
      inx = -1                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
      block data const
      include "eqptr71.inc"
c     data nttyo /0/                                                            
c     data nttyi /0/                                                            
c                                                                               
      data ucode/'eqpt    '/,urelno/'3245    '/,ustage/'R71     '/              
      end
      