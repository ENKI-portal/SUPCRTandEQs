c eqlsza.h                                                                      
c      selm   =  array of sums (e lambda * concentration)                       
c                 for each charge                                               
c      selmp   =  array of sums (e-lambda prime * concentration)                
c                 for each charge                                               
c                                                                               
      common /eqlsza/  selm(21),selmp(21)                                       
c--------------------------------------------------------------------           