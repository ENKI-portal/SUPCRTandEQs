c--------------------------------------------------------------------           
c implicit.h                                                                    
c   standard implicit statement for 32 bit machines                             
c   for eq3/6 codes generally requiring 64 bit arithmetic                       
      implicit logical(q),integer(i,j,k,n),real*8(a-h,l,m,o,p,r-t,v-z),         
     $ character*8(u)                                                           
