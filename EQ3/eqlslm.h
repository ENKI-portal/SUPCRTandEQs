c eqlslm.h                                                                      
c      pslam  triples                                                           
c       s-lambda                                                                
c       s-lambda first derivative                                               
c       s-lambda second derivative                                              
c       indexed same as pslm array                                              
      common /eqlslm/ pslam(3,nslpar)                                           
c--------------------------------------------------------------------           