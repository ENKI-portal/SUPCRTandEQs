UNAME := $(shell uname)

ifeq ($(UNAME), Darwin)
all: libeq.dylib EQPT/eqpt EQ3/eq3 EQ6/eq6

libeq.dylib: eqlibr136.f
	f2c eqlibr136.f
	clang -dynamiclib -o libeq.dylib -L/usr/local/lib -lf2c eqlibr136.c
	rm eqlibr136.c

EQPT/eqpt: EQPT/eqpt.f libeq.dylib
	f2c EQPT/eqpt.f
	clang -o EQPT/eqpt libeq.dylib -L/usr/local/lib -lf2c eqpt.c -lm -lc
	rm eqpt.c

EQ3/eq3: EQ3/eq3nr110.f libeq.dylib
	f2c EQ3/eq3nr110.f
	clang -o EQ3/eq3 libeq.dylib -L/usr/local/lib -lf2c eq3nr110.c -lm -lc
	rm eq3nr110.c

EQ6/eq6: EQ6/eq6r100.f libeq.dylib
	f2c EQ6/eq6r100.f
	clang -o EQ6/eq6 libeq.dylib -L/usr/local/lib -lf2c eq6r100.c -lm -lc
	rm eq6r100.c

clean:
	rm -rf output
	rm -rf eqlibr136.c
	rm -rf EQPT/eqpt
	rm -rf EQPT/eqpt.c
	rm -rf EQ3/eq3
	rm -rf EQ3/eq3nr110.c
	rm -rf EQ6/eq6
	rm -rf EQ6/eq6r100.c
	rm -rf libeq.dylib

endif

ifeq ($(UNAME), Linux)
all: libeq.so EQPT/eqpt EQ3/eq3 EQ6/eq6

libeq.so: eqlibr136.f
	f2c eqlibr136.f
	clang -shared -fPIC -o libeq.so -L/usr/local/lib -lf2c eqlibr136.c
	cp libeq.so /usr/local/lib
	rm eqlibr136.c

EQPT/eqpt: EQPT/eqpt.f libeq.so
	f2c EQPT/eqpt.f
	clang -o EQPT/eqpt eqpt.c /usr/local/lib/libeq.so -L/usr/local/lib -lf2c -lm -lc
	cp EQPT/eqpt /usr/local/bin
	rm eqpt.c

EQ3/eq3: EQ3/eq3nr110.f libeq.so
	f2c EQ3/eq3nr110.f
	clang -o EQ3/eq3 eq3nr110.c /usr/local/lib/libeq.so -L/usr/local/lib -lf2c -lm -lc
	cp EQ3/eq3 /usr/local/bin
	rm eq3nr110.c

EQ6/eq6: EQ6/eq6r100.f libeq.so
	f2c EQ6/eq6r100.f
	clang -o EQ6/eq6 eq6r100.c /usr/local/lib/libeq.so -L/usr/local/lib -lf2c -lm -lc
	cp EQ6/eq6 /usr/local/bin
	rm eq6r100.c

clean:
	rm -rf output
	rm -rf eqlibr136.c
	rm -rf EQPT/eqpt
	rm -rf EQPT/eqpt.c
	rm -rf EQ3/eq3
	rm -rf EQ3/eq3nr110.c
	rm -rf EQ6/eq6
	rm -rf EQ6/eq6r100.c
	rm -rf libeq.so
	rm -rf /usr/local/lib/libeq.so
	rm -rf /usr/local/bin/eqpt
	rm -rf /usr/local/bin/eq3
	rm -rf /usr/local/bin/eq6

endif
