
****  ATTACHMENT: d:\for\standard.for *****

C ---------------------------------------------------------------
      SUBROUTINE GCALC(GMIN, I)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
C
C      THIS ROUTINE CALCULATES THE G OF A PHASE/GAS FOR A GIVEN P AND T

      PARAMETER (MAXPHS=100,MAXCMP=15,MAXVOL=18)
      COMMON /THERMM/ THERMO(11,MAXPHS), COMP(MAXCMP,MAXPHS),
     1       REAC(MAXPHS), ACT(MAXPHS), THERLM(9,20), VOLPAR(MAXVOL,
     2       MAXPHS), THERDI(10,20), IWEIRD(MAXPHS), NDX(MAXPHS),
     3       IDIS(MAXPHS), LMDA(MAXPHS,3), NPHASE, NPR, NCTZ
	COMMON/MPHASE/MPHASE
      COMMON /CHOOSE/ ICHH2O, ICHCO2, ICHMIX
      COMMON /INTER/ A, B, C, XD, XE, XF, XG, G, H, XMF(10), XNMOL(10),
     1       P, S, TC, V, XOTHER, XNACL
      COMMON /DIAGRA/ XVAL, YVAL, ZVAL, IXY(0:5,2), IXYCOE(5,2), IPTX,
     1       ICRITC
      CHARACTER*8 NAMEA(MAXPHS)
      CHARACTER*120 TITLE
      CHARACTER*20 NAME(MAXPHS)
      COMMON /CHNAME/ TITLE, NAME, NAMEA
      COMMON /COMPS/ ICMP1(MAXPHS), ICMP2(MAXPHS), ACTPRINT
      CHARACTER*4 ACTPRINT
      
c from LOOK      
       COMMON /PASS/ GTOT,HTOT,STOT,CPTOT,VTOT
       COMMON /LAMB/ GLAM,HLAM,SLAM,CPLAM,VLAM
       COMMON /DISOR/GDIS,HDIS,SDIS,CPDIS,VDIS
       COMMON /VOL/ VPT
       
       
C  Changes made by Vera

	COMMON /PRESS/ PBER,TCBER

 
C
C
C      IN CP EQUATION, CP = A  +  B * T     +  C * T**-2  +  D * T**-.5
C                              +  E * T**2  +  F * T**-1  +  G * T**-3
C
      PARAMETER (R=8.3143D00,TR=298.15)
      TK = TC + 273.15
      AH = THERMO(1,I)
      AS = THERMO(2,I)
      AA = THERMO(3,I)
      AB = THERMO(4,I)
      AC = THERMO(5,I)
      AV = THERMO(6,I)
      AD = THERMO(7,I)
      AE = THERMO(9,I)
      AF = THERMO(8,I)
      AG = THERMO(10,I)
      
C  Changes made by Vera

      AGG = THERMO(11,I)
      
      P=PBER
      TC=TCBER
      TK=TC+273.15
            
C

      TFUG = 0.0
      IF (NDX(I) .GT. 0 .AND. NDX(I) .LT. 99) THEN
        IF (IPTX .EQ. 3 .AND. I .EQ. IXY(1,1)) THEN
          POLD = P
          P = 1.0
        END IF
        INDX = NDX(I)
        IF (XMF(INDX) .LT. 1.0D-10) THEN
          GMIN = 1000000.0
          RETURN
        ELSE
          CALL CHEWS(INDX, FC, VOL, GH2O, ACTIV, 0)
        END IF
C
        IF (ICHH2O .EQ. 1 .AND. INDX .EQ. 1) THEN
          TFUG = GH2O + (R*TK*DLOG(ACTIV))
        ELSE
          TFUG = R * TK * DLOG((FC*P*ACTIV))
        END IF
        IF (IPTX .EQ. 3 .AND. I .EQ. IXY(1,1)) THEN
          P = POLD
        END IF
      END IF
C
      TFUG = TFUG + (ACT(I)*DLOG(10.0D00)*R*TK)
C
C      ADD G FOR PHASES WITH DISORDER
C

c from LOOK
              HDIS=0.
              GDIS=0.
              SDIS=0.
              CPDIS=0.
              VDIS=0.
c              
              
              
      IF (IDIS(I) .NE. 0) THEN
        J = IDIS(I)
        CALL DISORD(P, TK, THERDI(1,J), THERDI(2,J), THERDI(3,J),
     1       THERDI(5,J), THERDI(6,J), THERDI(7,J), THERDI(10,J),
     2       THERDI(8,J), THERDI(9,J), GSS)
        TFUG = TFUG + GSS
      END IF
C
C      ADD G FOR SPECIAL SOLN PHASES
C
      IF (NAME(I) .EQ. 'CALCITESS' .OR. NAME(I) .EQ. 'Ccss')
     1       THEN
        CALL SOLN(P, TK, GSS, 'CALCITE')
        TFUG = TFUG + GSS
      ELSE IF (NAME(I) .EQ. 'ALENSTATITE' .OR. NAME(I) .EQ. 'aEn')
     1       THEN
        CALL SOLN(P, TK, GSS, 'ALENSTATITE')
        TFUG = TFUG + GSS
      ELSE IF (NAME(I) .EQ. 'CORDIERITE' .OR. NAME(I) .EQ. 'Cd' .OR.
     1       NAME(I) .EQ. 'FE_CORDIERITE' .OR. NAME(I) .EQ. 'fCd')
     2       THEN
        CALL SOLN(P, TK, GSS, 'CORDIERITE')
        TFUG = TFUG + GSS
      ELSE IF (NAME(I) .EQ. 'HYD-CORDIERITE' .OR. NAME(I) .EQ. 'hCd')
     1       THEN
        CALL SOLN(P, TK, GSS, 'HYD-CORD')
        TFUG = TFUG + GSS
      END IF
C
C    add G contribution from general solution model
C
      IF (IWEIRD(I) .NE. 0) THEN
C       CALL SSCALC(P, TK, GSS, ICMP1(I), ICMP2(I), 0)
        TFUG = TFUG + GSS
      END IF
C
C      ADD G FOR PHASES WITH LAMBDA TRANSITIONS
C

c from LOOK
              HLAM=0.
              GSPK=0.
              SLAM=0.
              CPLAM=0.
              VLAM=0.
c             
              
      IF (LMDA(I,1) .NE. 0 .OR. LMDA(I,2) .NE. 0 .OR. LMDA(I,3) .NE. 0)
     1       THEN
        GSPK = 0.0D00
        CALL LAMBDA(P, TK, GSPK, I)
        TFUG = TFUG + GSPK
      END IF
C
C      Include extra Gibbs free energy as a function of volume
C
      IF (IPTX .EQ. 3 .AND. I .EQ. IXY(1,1)) THEN
        POLD = P
        P = 1.0
      END IF
      
      CALL VOLMIN(GVOL, P, TK, VOLPAR(1,I), VOLPAR(2,I), VOLPAR(3,I),
     1     VOLPAR(4,I))
      IF (IPTX .EQ. 3 .AND. I .EQ. IXY(1,1)) THEN
        P = POLD
      END IF
      
C  from LOOK

      CPTOT = AA + AB*TK + AC/TK/TK + AD/DSQRT(TK) + AE*TK*TK
     .  + AF/TK + AG/TK/TK/TK +CPLAM + CPDIS
      H1 = (AA * (TK - TR)) 
      S1 = (AA *  (DLOG(TK) - DLOG(TR)))
      H2 = (AB * (TK**2 - TR**2) / 2.0) 
      S2 = (AB * (TK - TR))
      S3 = (-AC * ((1./TK**2) - (1./TR**2)) / 2.0) 
      H3 = (-AC * ((1./TK) - (1./TR)))
      H4 = (AD * 2.0 *      (TK**0.5    - TR**0.5   )) 
      S4 = (-AD * 2.0 * (TK**(-0.5) - TR**(-0.5)))
      H5 = (AE *      (TK**3 - TR**3) / 3.0) 
      S5 = (AE * (TK**2 - TR**2) / 2.0)
      H6 = (AF * DLOG(TK/TR)) 
      S6 = (-AF * (1.0/TK - (1.0/TR)))
      H7 =  AG * (1.0/TK**2 - 1.0/TR**2)      / (-2.0)
      S7 =  -AG * (1.0/TK**3 - 1.0/TR**3)  /   3.0
      HCPSUM  = H1 + H2 + H3 + H4 + H5 + H6 + H7
      SCPSUM  = S1 + S2 + S3 + S4 + S5 + S6 + S7
      GCPSUM = HCPSUM - TK * SCPSUM
      HTOT = AH + HCPSUM + HLAM + HDIS
      STOT = AS + SCPSUM + SLAM + SDIS
      VTOT = AV + VLAM + VDIS + VPT
C  changes made by Vera
      GMIN = AGG - AS * (TK-TR) + GCPSUM + GVOL + TFUG
      IF (I .NE. IXY(1,1).OR. IPTX .NE. 3) GMIN = GMIN + ((P - 1.0)*AV)
      GTOT = GMIN
      
       
      RETURN
      END
      
c LAMBDA from LOOK


      SUBROUTINE LAMBDA (P, T, GLAMBD, I)

       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C

C     CALCULATE THE EXTRA ENERGY OF A LAMDBA TRANSITION USING MODEL

C     OF BERMAN AND BROWN (1985, CONTRIBS. MINER. PETRO.)

C

C     INPUT VARIABLES

C                        P = PRESSURE IN BARS

C                        T = TEMPERATURE IN K

C                        I = POINTER TO PHASE IN THERMO, COMP, ETC

C

C     RETURNED - GSPK - ENERGY BECAUSE OF LAMBDA SPIKE (JOULES)

C

      PARAMETER (MAXPHS=100, MAXCMP=15, MAXVOL=18)

      COMMON /THERMM/ THERMO(11,MAXPHS), COMP(MAXCMP,MAXPHS),

     .                REAC(MAXPHS), ACT(MAXPHS), THERLM(9, 20),

     .                VOLPAR(MAXVOL,MAXPHS), THERDI(10, 20),

     .                IWEIRD(MAXPHS), NDX(MAXPHS), IDIS(MAXPHS),

     .                LMDA(MAXPHS,3), NPHASE, NPR, NCTZ

      DIMENSION GSAV(3)

       COMMON /LAMB/ GSPK,DHSPK,DSSPK,CPSPK,VSPK

C

      DO 5 IK = 1,3

      IF ( IK.EQ.1 ) P = P + .001

      IF ( IK.EQ.2 ) THEN

        P = P - .001

        T = T + .001

      ELSEIF ( IK.EQ.3 ) THEN

        T = T - .001

      ENDIF

      GSPK   = 0.0

      DHSPK=0.0

      DSSPK=0.0

      CPSPK=0.0

      VSPK=0.0

        GLAMBD = 0.0

      DO 10 J = 1, 3

          KK     = LMDA(I,J)

          IF (KK .NE. 0) THEN

              DSTR   = 0.0

              ASPK   = THERLM( 1,KK)

              BSPK   = THERLM( 2,KK)

              TQ1BAR = THERLM( 3,KK)

              TEQ    = (THERLM( 4,KK) * (P - 1.0)) + THERLM( 3,KK)

              DVDTR  = THERLM( 5,KK)

              DVDP   = THERLM( 6,KK)

              TR     = THERLM( 7,KK)

              DHTR   = THERLM( 8,KK)

C             VTRANS = THERLM( 9,KK)

              CTRANS = TQ1BAR -TEQ

              TR9    = TR - CTRANS

              IF (T .GT. TEQ) THEN

                  T9 = TEQ

              ELSE

                  T9 = T

              ENDIF

              IF ( T.LT.TR9 ) GO TO 10

C

              D1 = BSPK**2

              A1 = (ASPK**2 * CTRANS) + (2.0 * ASPK * BSPK * CTRANS**2)

     .           + (D1 * CTRANS**3)

              B1 =  ASPK**2 + (4.0 * ASPK * BSPK * CTRANS)

     .           + (3.0 * D1 * CTRANS**2)

              C1 = (2.0 * ASPK * BSPK) + (3.0 * CTRANS * D1)

              CPSPK = (T9+CTRANS) * (ASPK + BSPK * (T9+CTRANS))**2

              DHSPK = (A1 * (T9 - TR9)) + (B1 *(T9**2 - TR9**2) / 2.0)

     .              + (C1 * (T9**3 - TR9**3) / 3.0)

     .              + (D1 * (T9**4 - TR9**4) / 4.0)

              DSSPK = (A1 * (DLOG(T9) - DLOG(TR9))) + (B1 * (T9 - TR9))

     .              + (C1 * (T9**2 - TR9**2) / 2.0)

     .              + (D1 * (T9**3 - TR9**3) / 3.0)

              IF ( (T9+CTRANS) .LT. 298.15) DHSPK = 0.

              IF ( (T9+CTRANS) .LT. 298.15) DSSPK = 0.

              GSPK = GSPK - (T9 * DSSPK) + DHSPK

              DSTR = DHTR / TQ1BAR

              IF (T.GT.TEQ) GSPK = GSPK - ((DSTR + DSSPK) * (T-TEQ))

C

              GSPK = GSPK + DVDTR * (P-1.0D0) * (T9-298.15)

     .                    + DVDP/2.0D0 * (P*P-1.0D0) - DVDP * (P-1.0D0)

              IF (T.GT.TEQ) DHSPK = DHSPK + DHTR

              IF (T.GT.TEQ) DSSPK = DSSPK + DHTR/TQ1BAR

              GLAMBD = GSPK

          ENDIF

   10 CONTINUE

          GSAV(IK) = GSPK

    5 CONTINUE

C

C    CALCULATE LAMBDA S,V FROM FINITE DIFF DERIVS SINCE dCp/dP =/ 0

C

      VSPK = ( GSAV(1)-GSAV(3) )/.001D0

      DSSPK = - ( GSAV(2)-GSAV(3) )/.001D0

      RETURN

      END

C  SUBROUTINE VOLMIN IS FROM LOOK PACKAGE ( CHANGED SLIGHTLY )


      SUBROUTINE VOLMIN( GVOL, P, TK, VTA, VTB, VPA, VPB)

C

C      calculates G contribution from mineral volumes as f(P,T)

C            dvdt and dvdp assumed constant over P,T

C

C     Modified and replaced in Library-December 1, 1986

C

      IMPLICIT DOUBLE PRECISION(A-H, O -Z)

      PARAMETER (T0=298.15D00, P0=1.D00)

       COMMON /VOL/VPT

C

C

      GVOL = (VTA*(TK-T0) + VTB*(TK-T0)**2) * (P-P0)

     .               + VPA * ( P**2 / 2.0D0 - P + 0.5D0 )

     .               +  VPB * ( P**3 / 3.0D0 - P**2 + P - 1.0D0/3.0D0 )

C

      VPT = VTA*(TK-T0) + VTB*(TK-T0)**2 + VPA*(P-P0) + VPB*(P-P0)**2

C

      RETURN

      END

      SUBROUTINE ALBITE(P, TT, G, H, S, CP, V)
C    routine to compute thermodynamic properties of albite
C     ordering relative to low albite standard state
C    *** function of Salje (1985) uses monalbite std state ***
C
C
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
C     READ (5,98) T
      T = TT
      G = 0.0D0
      H = 0.0D0
      S = 0.0D0
      CP = 0.0D0
      V = 0.0D0
      IF (T .GE. 1290.0D0) RETURN
C
C   **** Q parameters for solving eqn. (19) *****
C
      A0 = 5.479D0
      B = 6854.0D0
      BOD = -9301.0D0
      COD = 43600.0D0
      DD0 = -2.171D0
      DD1 = -3.043D0
      DD2 = -0.001569D0
      DD3 = 0.000002109D0
C
      AOD0 = 41.620D0
      D = DD0 + DD1 * T + DD2 * T ** 2 + DD3 * T ** 3
C
      T1 = T - 1251.0D0
      T2 = T - 824.1D0
      D3 = D ** 3
      D5 = D ** 5
      T12 = T1 ** 2
      T13 = T1 ** 3
      T14 = T1 ** 4
      T15 = T1 ** 5
      B2 = B ** 2
      B3 = B ** 3
      B4 = B ** 4
      B5 = B ** 5
      A02 = A0 ** 2
      A03 = A0 ** 3
      A04 = A0 ** 4
      A05 = A0 ** 5
C
C   *******************************
C
C
C
C    Now iteratively find the solution (Q) to the following equation
C
      E1 = A0 * AOD0 * T1 * T2 / D - D
      E2 = AOD0 * B * T2 / D + BOD * A03 * T13 / D3
      E3 = 3.0D0 * BOD * A02 * B * T12 / D3 + COD * A05 * T15 / D5
      E4 = 3.0D0 * BOD * A0 * B2 * T1 / D3 + 5.0D0 * A04 * COD * B *
     1T14 / D5
      E5 = BOD * B3 / D3 + 10.0D0 * COD * A03 * B2 * T13 / D5
      E6 = 10.0D0 * COD * A02 * B3 * T12 / D5
      E7 = 5.0D0 * COD * A0 * B4 * T1 / D5
      E8 = COD * B5 / D5
C
      ICOUNT = 0
      QINT = 0.10D0
      FLAST = -1.0
      IF (T .LT. 1100.0D0) THEN
        QLAST = 1.10D0
        Q = 1.1D0
      ELSE
        QLAST = 0.60D0
        Q = 0.60D0
      END IF
   10 Q = Q - QINT
      IF (Q .LT. 0.00001) Q = 0.0001D0
   20 F = E1 * Q + E2 * Q ** 3 + E3 * Q ** 5 + E4 * Q ** 7 + E5 * Q **
     19 + E6 * Q ** 11 + E7 * Q ** 13 + E8 * Q ** 15
      ICOUNT = ICOUNT + 1
      IF (DABS(Q - QLAST) .LT. 0.0001) GO TO 30
      IF (ICOUNT .GT. 30) GO TO 30
      IF (F*FLAST .LT. 0.001) THEN
        QINT = QINT / 2.0D0
        Q = Q + QINT
        GO TO 20
      ELSE
        QLAST = Q
        FLAST = F
        GO TO 10
      END IF
C
C
C   *****  NOW  calculate Qod value from eqn. (4)
C
   30 A = A0 * (T - 1251.0D0)
      TC = 1251.0D0
      TOD = 824.1D0
C
      QOD = (-A*Q - B*Q**3) / D
C
C
C   *** NOW Calculate dS and dH from eqns. (21) and (22) ***
C
      S = 0.5D0 * A0 * Q ** 2 + 0.5D0 * AOD0 * QOD ** 2 + (DD1 + 2.0D0*
     1DD2*T + 3.0D0*DD3*T**2) * Q * QOD
      S = -S
      H = -0.5D0 * A0 * TC * Q ** 2 + 0.25D0 * B * Q ** 4 - 0.5D0 *
     1AOD0 * TOD * QOD ** 2 + 0.25D0 * BOD * QOD ** 4 + COD / 6.0D0 *
     2QOD ** 6 + (DD0 - DD2*T**2 - 2.0D0*DD3*T**3) * Q * QOD
      CP = A02 * T / (2.0D0*B)
C
      V = H / 335282.925D0
C
C   above factor gives .04 J/bar as diff between high and low albite
C      with shape of vol(disorder) following shape of H(disorder)
C
      G = H - T * S + (P - 1.0D0) * V
C
C     WRITE (6,99) ICOUNT, T, Q, F, QOD, G, H ,S
   40 FORMAT (I5, 4F15.5, /, 3F15.2)
      RETURN
      END


      SUBROUTINE DISORD(P, TK, D0, D1, D2, D4, D5, D6, VDIS, TMIN, TMAX,
     1           GSS)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
       COMMON /DISOR/ GDIS,DH,DS,DCP,DV 
C
C     THIS ROUTINES APPROXIMATES A DISORDER ENERGY USING A CP LIKE
C          EQUATION - IT PRODUCES A H-H AND DS INCREMENTS
C
C      EQUATION IS:
C      DIS = D0  +  D1 * T**-.5  +  D2 * T**-2
C                +  D4 * T**-1   +  D5 * T      +  D6 * T**2
C
C      VOLUME TAKEN FROM SHAPE OF ENTHALPY FUNCTION, SCALED TO DATA
C               BY THE VARIABLE VDIS
C
C     INTEGRATE TO GET H AND S TERMS
C
C     ONLY USE ABOVE TMIN - ABOVE TMAX, ONLY INTEGRATE TO TMAX
C     RETURN EXTRA ENERGY IN GSS
C
C     SEE BERMAN AND BROWN FOR DETAILS
C
      TR = TMIN
      GSS = 0.0
      
C FROM LOOK
      GDIS=0.0
C
      IF (DABS(D0) .LT. 0.001 .AND. DABS(D1) .LT. 0.001) THEN
C
C       *** ALBITE DISORDERING WITH SALJE FUNCTION ***
C
        CALL ALBITE(P, TK, GSS, DH, DS, DCP, DV)
        
C FROM LOOK
        GDIS=GSS
C
        RETURN
      END IF
C
      IF (TK .LT. TR) RETURN
      TT = TK
      IF (TK .GT. TMAX) TT = TMAX
      XS0 = D0 * (DLOG(TT) - DLOG(TR))
      XH0 = D0 * (TT - TR)
      XH1 = 2.0D00 * D1 * ((TT**0.5) - (TR**0.5))
      XS1 = -2.0D00 * D1 * (TT**(-0.5) - TR**(-0.5))
      XH2 = -(D2*((1.0/TT) - (1.0/TR)))
      XS2 = -D2 * ((1.0D0/TT**2) - (1.0D0/TR**2)) / 2.0D00
      XH3 = 0.0
      XS3 = 0.0
      XH4 = D4 * DLOG(TT/TR)
      XS4 = -D4 * (1.0D00/TT - 1.0/TR)
      XH5 = (D5*(TT**2 - (TR**2))) / 2.0
      XS5 = (D5*(TT - TR))
      XH6 = D6 * (TT**3 - TR**3) / 3.0D00
      XS6 = D6 * (TT**2 - TR**2) / 2.0D00
      
C FROM LOOK
      DCP = D0 + D1/DSQRT(TT) + D2/TT/TT + D4/TT + D5*TT + D6*TT*TT
C
      DH = XH0 + XH1 + XH2 + XH3 + XH4 + XH5 + XH6
      DS = XS0 + XS1 + XS2 + XS3 + XS4 + XS5 + XS6
      DV = 0.0
      IF (DABS(VDIS) .NE. 0.0) DV = DH / VDIS
C
      GSS = DH - (TK*DS) + (DV*(P - 1.0D0))
C  FROM LOOK
      GDIS=GSS
C
      RETURN
      END

      SUBROUTINE CHEWS(IDX, FP, VOLUME, GH2O, ACTCOF, ISWT)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
C
      COMMON /CHOOSE/ ICHH2O, ICHCO2, ICHMIX
      COMMON /INTER/ A, B, C, XD, XE, XF, XG, G, H, XMF(10), XNMOL(10),
     1       P, S, TC, V, XOTHER, XNACL
      COMMON /WATPRO/ WATTC, WATP, WATVOL, WATG
C
C     ICHH2O SELECTS WHICH WATER ROUTINE TO USE:
C                        1 - HAAR
C                        2 - KERRICK AND JACOBS, 1981
C                        3 - HOLLAND AND POWELL, 1990
C
C     ICHCO2 SELECTS WHICH CO2 ROUTINE TO USE:
C                        1 - MADER & BERMAN - 1991
C                        2 - KERRICK AND JACOBS, 1981
C                        3 - HOLLAND AND POWELL, 1990
C
C     ICHMIX SELECTS WHICH MIXING PROPERTIES TO USE:
C                        1 - KERRICK AND JACOBS, 1981
C                        2 - IDEAL
C                        3 - HOLLAND AND POWELL, 1990
C
C      ALL VARIBLES WITH "SAV" AS THE LAST THREE CHARACTERS
C      SAVE THE VALUES OF PRESSURE, TEMPERATURE, MOLE FRACTION
C      CO2, FUGACITY AND ACTIVITY OF WATER AND CO2 .
C      THE VALUES OF P, T, AND X ARE COMPARED ON THE NEXT CALL,
C      AND IF THE SAME, THE SAVED VALUES ARE USED.  THIS IS
C      BECAUSE THE K&J MIXING ROUTINES MUST EVALUATE BOTH
C      CO2 AND H2O PROPERTIES ON EACH CALL.  NO NEED TO CALL IT
C      TWICE.
C
C      ISWT CONTROLS IF THE TEST IS MADE - THUS AVOIDING COMPARING
C           UNDEFINED VARIABLES
C
      SAVE PSAV, TSAV, XSAV, FWSAV, FCSAV, AWSAV, ACSAV, GSAV
C
      FP = 1.0
      ACO2 = 1.0
      AH2O = 1.0
      TK = TC + 273.15
C
      IF (ICHMIX .EQ. 1 .AND. (IDX .EQ. 1 .OR. IDX .EQ. 2) .AND. XMF(2)
     1       .LT. 1.0 .AND. XMF(2) .GT. 1.0E-15) THEN
        IF (ISWT .GE. 1 .AND. ICHH2O.NE.3 .AND. ICHCO2.EQ.2) THEN
C   only uses saved values from KJ mix routine if CO2 also is KJ and
C          H2o routine is either KJ or Haar
          IF (P .EQ. PSAV .AND. TC .EQ. TSAV .AND. XMF(2) .EQ. XSAV)
     1           THEN
            IF (IDX .EQ. 1) THEN
              FP = FWSAV
              ACTCOF = AWSAV
              GH2O = GSAV
              RETURN
            ELSE IF (IDX .EQ. 2) THEN
              FP = FCSAV
              ACTCOF = ACSAV
              RETURN
            END IF
          END IF
        END IF
        CALL MIXKJ(TK, P, XMF(2), FGCO2, FGH2O, ACO2, AH2O)
        FWSAV = FGH2O
        FCSAV = FGCO2
        ACSAV = ACO2
        AWSAV = AH2O
        PSAV = P
        TSAV = TC
        XSAV = XMF(2)
      END IF
c
C       IF ( ICHMIX.EQ.3 )
C    .        CALL MIXHP(TK, P, XMF(2), FGCO2, FGH2O, ACO2, AH2O)
c
      IF (IDX .EQ. 1) THEN
        GSAV = 0.0
        IF (ICHH2O .EQ. 1) THEN
          IF (P .GT. 10000.0) THEN
            PP = 10000.0
          ELSE
            PP = P
          END IF
C
          CALL WHAAR(PP, TK, GH2O, VOLUME)
C
          GH2O = GH2O - 285829.96 - (298.15*69.9146)
          IF (P .GT. 10000) THEN
            GDIF = 0.0
            CALL WDH78(TK, P, GDIF, VOLUME)
            GH2O = GH2O + GDIF
          END IF
          WATTC = TC
          WATP = P
          WATVOL = VOLUME
          WATG = GH2O
          GSAV = GH2O
        ELSE IF (ICHH2O.EQ.3) THEN
C             CALL WTHB  (TK,P,IDX,FP,VOLUME)
C             CALL HOPO  (TK,P,IDX,FP)
        ELSE IF (ICHH2O .EQ. 2) THEN
          IF (ICHMIX.EQ.1 .AND. AH2O .NE. 1.0) THEN
            FP = FGH2O
          ELSE
            CALL WCKJ(TK, P, IDX, FP, VOLUME)
          END IF
        END IF
        IF (ICHMIX .EQ. 2) THEN
          ACTCOF = XMF(1)
        ELSE
          ACTCOF = AH2O
        END IF
C
      ELSE IF (IDX .EQ. 2) THEN
        IF (ICHCO2.EQ.3) THEN
C             CALL WTHB  (TK,P,IDX,FP,VOLUME)
c             CALL HOPO  (TK,P,IDX,FP)
        ELSEIF (ICHCO2.EQ.1) THEN
              CALL MABE90(P,TK,VP,FP)
        ELSEIF (ICHCO2 .EQ. 2) THEN
          IF (ICHMIX.EQ.1 .AND. ACO2 .NE. 1.0) THEN
            FP = FGCO2
          ELSE
            CALL WCKJ(TK, P, IDX, FP, VOLUME)
          END IF
        END IF
        IF (ICHMIX .EQ. 2) THEN
          ACTCOF = XMF(2)
        ELSE
          ACTCOF = ACO2
        END IF
C
      ELSE
C       CALL WTHB(TK, P, IDX, FP, VOLUME)
        ACTCOF = XMF(IDX)
      END IF
C
      RETURN
      END

C ---------------------------------------------------------------
      SUBROUTINE WHAAR(P, T, GH2O, VH2O)
C
C     Calculates and returns the Gibbs free energy of Water and
C            its volume using the Haar equation of State.  Written
C            by Christian De Capitan, Dept. Geological Sciences,
C            UBC, Vancouver, BC, Canada
C
C     This routine uses a Redlich-Kwong to obtain a first guess
C            for the density of water, thus speeding things up
C
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      DIMENSION TAUI(0:6), ERMI(0:9), GI(40), KI(40), LI(40)
      DIMENSION CI(18), RHOI(37:40), TTTI(37:40), ALPI(37:40)
      DIMENSION BETI(37:40)
C
C -----GI ARE IN (bar cc / g)  =  10 * (J / g)
C
      DATA GI /-.53062968529023D4, .22744901424408D5, .78779333020687D4,
     1     -.69830527374994D3, .17863832875422D6, -.39514731563338D6,
     2     .33803884280753D6, -.13855050202703D6, -.25637436613260D7,
     3     .48212575981415D7, -.34183016969660D7, .12223156417448D7,
     4     .11797433655832D8, -.21734810110373D8, .10829952168620D8,
     5     -.25441998064049D7, -.31377774947767D8, .52911910757704D8,
     6     -.13802577177877D8, -.25109914369001D7, .46561826115608D8,
     7     -.72752773275387D8, .41774246148294D7, .14016358244614D8,
     8     -.31555231392127D8, .47929666384584D8, .40912664781209D7,
     9     -.13626369388386D8, .69625220862664D7, -.10834900096447D8,
     *     -.22722827401688D7, .38365486000660D7, .68833257944332D5,
     1     .21757245522644D6, -.26627944829770D5, -.70730418082074D6,
     2     -.225D1, -1.68D1, .055D1, -93.0D1/
      DATA KI /4*1, 4*2, 4*3, 4*4, 4*5, 4*6, 4*7, 4*9, 2*3, 1, 5, 3*2,
     1     4/
      DATA LI /1, 2, 4, 6, 1, 2, 4, 6, 1, 2, 4, 6, 1, 2, 4, 6, 1, 2, 4,
     1     6, 1, 2, 4, 6, 1, 2, 4, 6, 1, 2, 4, 6, 0, 3*3, 0, 2, 0, 0/
      DATA CI /.19730271018D2, .209662681977D2, -.483429455355D0,
     1     .605743189245D1, 22.56023885D0, -9.87532442D0,
     2     -.43135538513D1, .458155781D0, -.47754901883D-1,
     3     .41238460633D-2, -.27929052852D-3, .14481695261D-4,
     4     -.56473658748D-6, .16200446D-7, -.3303822796D-9,
     5     .451916067368D-11, -.370734122708D-13, .137546068238D-15/
      DATA RHOI /0.319D0, 0.310D0, 0.310D0, 1.55D0/
      DATA TTTI /640.0D0, 640.0D0, 641.6D0, 270.0D0/
      DATA ALPI /34.0D0, 40.0D0, 30.0D0, 1050.0D0/
      DATA BETI /2.0D4, 2.0D4, 4.0D4, 25.0D0/
C
      R = 4.6152D0
      RT = R * T
      NLOW = 40
      NHIGH = 20
      IF (T .LT. 449.35D0) NHIGH = 40
C
C ----GREF CALCULATED WITH THIS ROUTINE AT 25 C AND 1 BAR
C
      GREF = -54955.2356146121147D0
      T0 = 647.073D0
C
C -----The values (T/T0)**i are stored in the array TAUI(i)
C
      TAUI(0) = 1.D0
      TAUI(1) = T / T0
      DO 10 I = 2, 6
        TAUI(I) = TAUI(I - 1) * TAUI(1)
   10 CONTINUE
C
      B = -0.3540782D0 * DLOG(TAUI(1)) + 0.7478629D0+0.007159876D0 /
     1TAUI(3) - 0.003528426D0 / TAUI(5)
      BB = 1.1278334D0-0.5944001D0 / TAUI(1) - 5.010996D0 / TAUI(2) + 0.
     163684256D0 / TAUI(4)
C
      PS = 220.55D0
      IF (T .LE. 647.25) CALL PSAT2(T, PS)
C
C -----SET INITIAL GUESS FOR RHO USING THB-FIT TO REDLICH-KWONG
C
      ARK = 1.279186D8-2.241415D4 * T
      BRK = 1.428062D1+6.092237D-4 * T
      RR = 8.31441D0
      OFT = ARK / (P*DSQRT(T))
      BUK = -10.0D0 * RR * T / P
      CUK = OFT - BRK * BRK + BRK * BUK
      DUK = -BRK * OFT
      CALL KUBIK(BUK, CUK, DUK, X1, X2, X2I, X3)
      IF (X2I .NE. 0.0D0) THEN
        VOL = X1
      ELSE
        IF (P .LT. PS) THEN
          VOL = DMAX1(X1,X2,X3)
        ELSE
          VOL = DMIN1(X1,X2,X3)
        END IF
      END IF
      IF (VOL .LE. 0.0D0) THEN
        RHN = 1.9D0
      ELSE
        RHN = 1.0D0 / VOL * 18.0152D0
      END IF
C
C -----FIND THE TRUE(?) RH(T,P)
C -----NOTE: PR = PRESSURE CORRESPONDING TO GUESSED RH
C            DPR = (dP / dRH)
C            the values (1-EXP(-RH))**i are stored in the array ERMI(i)
C
      DO 50 LOO = 1, 100
        RH = RHN
        IF (RH .LE. 0.0D0) RH = 1.D-8
        IF (RH .GT. 1.9D0) RH = 1.9D0
        RH2 = RH ** 2
        Y = RH * B / 4.D0
        ER = DEXP(-RH)
        Y3 = (1.0D0-Y) ** 3
        ALY = 11.D0 * Y
        BETY = 44.33333333333333D0 * Y * Y
        F1 = (1.D0+ALY + BETY) / Y3
        F2 = 4.D0 * Y * (BB/B - 3.5D0)
        ERMI(0) = 1.0D0
        ERMI(1) = 1.0D0-ER
        DO 20 I = 2, 9
          ERMI(I) = ERMI(I - 1) * ERMI(1)
   20   CONTINUE
        PR = 0.0D0
        DPR = 0.0D0
        DO 30 I = 1, 36
          S = GI(I) / TAUI(LI(I)) * ERMI(KI(I) - 1)
          PR = PR + S
          DPR = DPR + (2D0+RH*(KI(I)*ER - 1D0)/ERMI(1)) * S
   30   CONTINUE
        DO 40 I = NLOW, NHIGH
          DEL = RH / RHOI(I) - 1.0D0
          RHOI2 = RHOI(I) * RHOI(I)
          TAU = T / TTTI(I) - 1.0D0
          ABC = -ALPI(I) * DEL ** KI(I) - BETI(I) * TAU ** 2
          IF (ABC .GT. - 100.0D00) THEN
            Q10 = GI(I) * DEL ** LI(I) * DEXP(ABC)
          ELSE
            Q10 = 0.0D00
          END IF
          QM = LI(I) / DEL - KI(I) * ALPI(I) * DEL ** (KI(I) - 1)
          S = Q10 * QM * RH2 / RHOI(I)
          PR = PR + S
          DPR = DPR + S * (2.0D0/RH + QM/RHOI(I)) - RH2 / RHOI2 * Q10 *
     1    (LI(I)/DEL/DEL + KI(I)*(KI(I) - 1)*ALPI(I)*DEL**(KI(I) - 2))
   40   CONTINUE
        PR = RH * (RH*ER*PR + RT*(F1 + F2))
        DPR = RH * ER * DPR + RT * ((1.0D0+2D0*ALY + 3D0*BETY)/Y3 + 3D0*
     1  Y*F1/(1.0D0-Y) + 2D0*F2)
C
C -----
C
        IF (DPR .LE. 0.0D0) THEN
          IF (P .LE. PS) THEN
            RHN = RHN * 0.95D0
          ELSE
            RHN = RHN * 1.05D0
          END IF
        ELSE
          IF (DPR .LT. 0.01D0) DPR = 0.01D0
          X = (P - PR) / DPR
          IF (DABS(X) .GT. 0.1D0) X = 0.1D0 * X / DABS(X)
          RHN = RH + X
        END IF
        DP = DABS(1.0D0-PR/P)
        DR = DABS(1.0D0-RHN/RH)
        IF (DP .LT. 5.D-2 .AND. DR .LT. 5.D-2) GO TO 60
   50 CONTINUE
   60 RH = RHN
C
C -----
C
      Y = RH * B / 4.D0
      X = 1.0D0-Y
      ER = DEXP(-RH)
      ERMI(0) = 1.0D0
      ERMI(1) = 1.0D0-ER
      DO 70 I = 2, 9
        ERMI(I) = ERMI(I - 1) * ERMI(1)
   70 CONTINUE
C
C -----CALCULATE BASE FUNCTION
C
      AA = RT * (-DLOG(X) - 43.33333333333333D0/X + 28.16666666666667D0/
     1X/X + 4D0*Y*(BB/B - 3.5D0) + 15.16666666666667D0+DLOG(RH*RT/1.
     201325D0))
C
C -----CALCULATE RESIDUAL FUNCTION
C
      DO 80 I = 1, 36
        AA = AA + GI(I) / KI(I) / TAUI(LI(I)) * ERMI(KI(I))
   80 CONTINUE
      DO 90 I = NLOW, NHIGH
        DEL = RH / RHOI(I) - 1.0D0
        TAU = T / TTTI(I) - 1.0D0
        ABC = -ALPI(I) * DEL ** KI(I) - BETI(I) * TAU ** 2
        IF (ABC .GT. - 100.0D00) THEN
          AA = AA + GI(I) * DEL ** LI(I) * DEXP(ABC)
        ELSE
          AA = AA
        END IF
   90 CONTINUE
C
C -----CALCULATE IDEAL GAS FUNCTION
C
      TR = T / 1.0D2
      W = TR ** (-3)
      AID = 1.D0+(CI(1)/TR + CI(2)) * DLOG(TR)
      DO 100 I = 3, 18
        AID = AID + CI(I) * W
        W = W * TR
  100 CONTINUE
      AA = AA - RT * AID
C
C -----CALCULATE G = AA + P/RH  AND  V = 1/RH
C
      GH2O = ((AA + P/RH)*1.80152D0-GREF)
      VH2O = (1.0D0/RH) * 18.0152D0
C     WRITE (91,911) P,T,GH2O,VH2O
C911  FORMAT (4F15.3)
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE WCKJ(TDEGK, PB, JJ, FUGCF, VOLUM)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
C
C             PROGRAM WRITTEN BY KERRICK AND JACOBS (1981)
C             FOR CALCULATION OF FREE ENERGY OF CO2, H2O
C             AND FOR THEIR MIXTURES
C
C             MIXTURES ARE RESTRICTED TO 325 - 1050 C BECAUSE VALUES OF
C             C, D, OR E BECOME NEGATIVE AND THUS CAN'T TAKE THE
C             SQUARE ROOT
C
      COMMON /MVAR/ BC, CC, DC, EC, BW, CW, DW, EW, J, L, M
      COMMON /MNRVAR/ PK, P, T, R, T12
C
C       VARIABLES AND RULES ARE AS FOLLOWS:
C
C        ACO2,AH2O....ACTIVITY OF CO2 AND H2O, RESPECTIVELY
C        BC,BW,BM.....COVOLUME OF CO2,H2O AND MIXTURE; CC/MOLE
C        CC,CW,CM.....ATTRACTIVE TERM FOR CO2, H2O, AND MIXTURE IN
C                     MRK EQUATION ; BAR*(CC**2)**2SQRT(T)/MOLE**2
C        DC, DW,,DM...ATTRACTIVE TERM FOR CO2, H2O, AND MIXTURE IN
C                     MRK EQUATION; BAR*(CC**3)*SQRT(T)/MOLE**3
C        EC,EW,EM.....ATTRACTIVE TERM FOR CO2, H2O, AND MIXTURE IN
C                     MRK EQUATION; BAR*(CC**4)*SQRT(T)/MOLE**4
C        CIJ,DIJ,EIJ..CROSS COEFFICIENTS OF C,D,E
C        FKCM,FKWM....FUGACITY COEFFICIENT OF CO2 AND H2O IN
C                     THE FLUID MIXTURE
C        FKCP,FKWP....FUGACITY COEFFICIENTS OF PURE CO2 AND PURE
C                     H2O, RESPECTIVELY
C        PK,P.........PRESSURE; KBARS,BARS, RESPECTIVELY
C        R............GAS CONSTANT; 83.14 CC*BARS/MOLE*K
C        TC,T.........TEMPERATURE; CELSIUS,KELVIN, RESPECTIVELY
C        VC,VW,VM.....MOLAR VOLUME OF CO2, H2O, AND MIXTURE; CC/MOLE
C        XC,XCO2......MOLE FRACTION OF CO2 IN THE FLUID MIXTURE
C        XW,XH2O......MOLE FRACTION OF H2O IN THE FLUID MIXTURE
C        Y............B/V4; VARIABLE IN HARD SPHERE-EQUATION
C        ZC,ZW,ZM.....COMPRESSIBILITY OF CO2, H2O, AND MIXTURE
C
C
C        DEFINITION OF CONSTANRS AND INPUT FOR THE CALCULATION OF
C    THE P, T, XCO2 VALUES WHICH ARE USED THROUGHOUT THE PROGRAM.
C
C
      R = 83.14D0
      BW = 29.00D0
      BC = 58.00D0
C
C     *** INDEXING SO THAT KERRICK AND JACOBS IS COMPATIBLE
C         WITH UBC --
C             H2O = 1
C             CO2 = 2
C
      IF (JJ .EQ. 1) J = 2
      IF (JJ .EQ. 2) J = 1
      L = J
      M = J
C
C        CALCULATION OF PARAMETERS USED IN THE PROGRAM.
C
      P = PB
      PK = P / 1000.0D0
      T = TDEGK
      TC = T - 273.15D0
      T15 = DSQRT(T**3)
      T12 = DSQRT(T)
      RT = R * T15
      CC = (28.31D0+0.10721D0*T - 0.00000881D0*T*T) * 1000000.0D0
      DC = (9380.0D0-8.53D0*T + 0.001189D0*T*T) * 1000000.0D0
      EC = (-368654.0D0+715.9D0*T + 0.1534D0*T*T) * 1000000.0D0
      CW = (290.78D0-0.30276D0*T + 0.00014774D0*T*T) * 1000000.0D0
      DW = (-8374.0D0+19.437D0*T - 0.008148D0*T*T) * 1000000.0D0
      EW = (76600.0D0-133.9D0*T + 0.1071D0*T*T) * 1000000.0D0
C
C         ROUTINES ZPURE, FPURE ARE CALLED TO CALCULATE
C
C        1) Z OF PURE CO2 AND H2O RESPECTIVELY
C        2) FUGACITY COEFFICIENTS OF CO2 AND H2O RESPECTIVELY
C
C    AT EACH P, T, XCO2 CONDITION.
C
      CALL ZPURE(PK, ZC, VC, ZW, VW, TC)
      CALL FPURE(RT, FKWP, FKCP, ZC, VC, ZW, VW)
C
      FUGCF = FKWP
      VOLUM = VW
      IF (JJ .EQ. 2) FUGCF = FKCP
      IF (JJ .EQ. 2) VOLUM = VC
C
C   REFERENCES:
C
C       KERRICK, D. M. AND JACOBS, G. K. IN PRESS; A MODIFIED
C   REDLICH-KWONG EQUATION FOR H2O, CO2, AND H2O-CO2 MIXTURES AT
C   ELEVATED PRESSURES AND TEMPURATURES, AM. JOUR. SCI.,IN PRESS.
C
C
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE ZPURE(PK, ZC, VC, ZW, VW, TC)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      COMMON /MVAR/ BC, CC, DC, EC, BW, CW, DW, EW, J, L, M
C
C        A SUBPROGRAM TO CALCULATE THE VOLUME OF CO2 AND H2O
C    FOR EACH PRESSURE AND TEMPERATURE OF THE SERIES. AN INITIAL GUESS
C    OF THE VOLUME(VI) IS CHOSEN FOR A GIVEN PRESSURE RANGE. THIS VALUE
C    IS THEN USED IN THE ROUTINE NEWRAP, WHICH SOLVES FOR THE EXACT
C    VOLUME BY MEANS OF AN ITERATIVE NEWTON - RAPHSON TECHNIQUE.
C
C
      IF (J .EQ. 1) THEN
        B = BC
        C = CC
        D = DC
        E = EC
        IF (PK .GE. 1.0D0) VI = 35.00
        IF (PK .GE. 0.10D0 .AND. PK .LT. 1.0D0) VI = 100.00
        IF (PK .GE. 0.005D0 .AND. PK .LT. 0.10D0) VI = 500.00
        IF (PK .LT. 0.005D0) VI = 5000.0
      ELSE
        B = BW
        C = CW
        D = DW
        E = EW
        IF (PK .GE. 1.0D0) VI = 22.0
        IF (PK .GE. 0.90D0 .AND. PK .LT. 1.0D0) VI = 24.2
        IF (PK .GE. 0.60D0 .AND. PK .LT. 0.9D0) VI = 31.2
        IF (PK .GE. 0.21D0 .AND. PK .LT. 0.60D0 .AND. TC .GE. 550.0D0)
     1  VI = 75.00
        IF (PK .GE. 0.21D0 .AND. PK .LT. 0.60D0 .AND. TC .LT. 550.0D0)
     1  VI = 35.00
        IF (PK .GE. 0.10D0 .AND. PK .LT. 0.21D0 .AND. TC .LT. 400.0D0)
     1  VI = 15.00
        IF (PK .GE. 0.10D0 .AND. PK .LT. 0.21D0 .AND. TC .GE. 400.0D0)
     1  VI = 100.00
        IF (PK .GE. 0.005D0 .AND. PK .LT. 0.10D0) VI = 500.00
        IF (PK .LT. 0.005D0) VI = 1000.00
      END IF
C
      CALL NEWRAP(B, C, D, E, Z, V, VI)
C
      IF (J .EQ. 1) THEN
        ZC = Z
        VC = V
      ELSE
        ZW = Z
        VW = V
      END IF
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE ZMIX(XC, XW, VM, ZM, VC, VW, BM, CM, DM, EM)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      COMMON /MVAR/ BC, CC, DC, EC, BW, CW, DW, EW, J, L, M
C
C        A PROGRAM TO CALCULATE THE VOLUME OF A MIXTURE
C    OF CO2 AND H2O AT EACH PRESSURE, TEMPERATURE, AND XCO2.
C    THE MOLAR VOLUMES OF CO2 AND H2O AS CALCULATED IN ZPURE ARE
C    USED TO DEFINE THE INITIAL ESTIMATE. ROUTINE NEWRAP
C    IS THEN USED TO CALCULATE THE VOLUME OF THE MIXTURE.
C
C
      VI = (VC*XC) + (VW*XW)
      B = BM
      C = CM
      D = DM
      E = EM
      CALL NEWRAP(B, C, D, E, Z, V, VI)
      VM = V
      ZM = Z
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE NEWRAP(B, C, D, E, Z, V, VI)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      COMMON /MNRVAR/ PK, P, T, R, T12
C
C             A SUBPROGRAM CALLED BY ZPURE AND ZMIX TO CALCULATE THE
C     VOLUME OF CO2, H2O, AND A MIXTURE OF CO2-H2O FOR EACH PRESSURE,
C     TEMPERATURE, AND XCO2. THE METHOD OF NEWTON-RAPHSON IS
C     EMPLOYED HERE, SUMMARIZED AS FOLLOWS:
C
C
C             F(X) = 0;  X(K+1) = X(K) - F(X)/DF(X)
C     WHERE DF(X) IS THE PARTIAL DIFFERENTIAL OF F(X) WITH
C     RESPECT TO X. X(K+1) IS CALCULATED FOR "K" ITERATIONS,
C     UNTIL NO CHANGE IN X(K+1) OCCURS.
C
C
C        INITIALIZATION OF PARAMETERS:
C
C
      DO 10 K = 1, 50
        Y = B / (4.0D0*VI)
        X = (1.0D0-Y)
        BI = (VI + B)
        BI2 = (VI + B) ** 2
C
C
C        DEFINITION OF THE F(X) FOR NEWRAP:
C
C        F(X) = 0 = P(REPULSIVE) - P(ATTRACTIVE) - P
C
C        WHERE F(X) IS A REARRANGEMENT OF KERRICK AND JACOBS'(1980)
C        EQUATION (14).
C
C
        PN = 1.0D0+Y + (Y**2) - (Y**3)
        PR = (PN/(VI*(X**3))) * R * T
        PA1 = C + (D/VI) + (E/(VI*VI))
        PA2 = PA1 / (T12*VI*BI)
        F = PR - PA2 - P
C
C        DEFINITION OF THE DIFFERENTIAL OF F(X) FOR NEWARP:
C
C        DF(X) = DP(REPULSIVE) - DP(ATTRACTIVE)
C
        D1 = (-3.0D0*B) / (4.0D0*(VI**3)*X**4)
        D2 = -1.0D0 / ((VI**2)*(X**3))
        D3 = 1.0D0 / (VI*(X**3))
        D4 = -B / (4.0D0*VI**2)
        D5 = -2.0D0 * (B**2) / (16.0D0*(VI**3))
        D6 = 3.0D0 * (B**3) / (64.0D0*(VI**4))
        DPR = ((PN*(D1 + D2)) + (D3*(D4 + D5 + D6))) * R * T
        D7 = (-1.0D0/(VI*BI2)) + (-1.0D0/(VI**2*BI))
        D8 = 1.0D0 / (VI*BI)
        D9 = (-D/VI**2) + ((-2.0D0*E)/(VI**3))
        DPA = (PA1*D7 + D8*D9) / T12
        DF = DPR - DPA
C
C        CALCULATION OF V(K+1) AN CONTINUATION OR END OF ITERATIONS
C      FOR NEWARP
C
        V = VI - (F/DF)
        DIFF = DABS(V - VI)
        IF (DIFF .LT. 0.01D0) THEN
          Z = (V*P) / (R*T)
          RETURN
        END IF
        IF (V .GT. 1000000.0) V = 1000000.0
        IF (V .LT. 9.9) V = 10.0
        VI = V
   10 CONTINUE
      Z = (V*P) / (R*T)
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE FPURE(RT, FKWP, FKCP, ZC, VC, ZW, VW)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      COMMON /MVAR/ BC, CC, DC, EC, BW, CW, DW, EW, J, L, M
C
C        A SUBPROGRAM TO CALCULATE FUGACITY COEFFICIENTS
C    OF PURE CO2 AND PURE H2O AT EACH PRESSURE AND TEMPERATURE.
C    SEE KERRICK AND JACOBS (IN PRESS) FOR A DERIVATION OF THE PURE
C    FUGACITY COEFFICIENT EXPRESSION (FCP)
C
C
      IF (L .EQ. 1) THEN
        B = BC
        C = CC
        D = DC
        E = EC
        V = VC
        Z = ZC
      ELSE
        B = BW
        C = CW
        D = DW
        E = EW
        V = VW
        Z = ZW
      END IF
      Y = B / (4.0D0*V)
      FCP = 8.0D0 * Y - 9.0D0 * Y * Y + 3.0D0 * Y ** 3
      FCP = FCP / ((1.0D0-Y)**3)
      FCP = FCP - DLOG(Z)
      FCP = FCP - (C/(RT*(V + B))) - (D/(RT*V*(V + B)))
      FCP = FCP - (E/(RT*V*V*(V + B))) + ((C/(RT*B))*(DLOG(V/(V + B))))
      FCP = FCP - (D/(RT*B*V)) + ((D/(RT*B*B))*(DLOG((V + B)/V)))
      FCP = FCP - (E/(RT*2.0D0*B*V*V)) + (E/(RT*B*B*V))
      FCP = FCP - ((E/(RT*B**3))*(DLOG((V + B)/V)))
      IF (FCP .LT. - 1.0D2) FCP = -1.0D2
      IF (FCP .GT. 1.0D2) FCP = 1.0D2
      FCP = DEXP(FCP)
      IF (L .EQ. 1) THEN
        FKCP = FCP
      ELSE
        FKWP = FCP
      END IF
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE FMIX(RT, CIJ, DIJ, EIJ, XC, XW, BM, FCCM, FCWM, FCM,
     1           VM, ZM, CM, DM, EM)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      COMMON /MVAR/ BC, CC, DC, EC, BW, CW, DW, EW, J, L, M
C
C        A SUBPROGRAM TO CACLULATE THE FUGACITY COEFFICIENTS
C     OF CO2 AND H2O IN A H2O-CO2 MIXTURE.  THESE
C     TOGETHER WITH THE FUGACITY COEFFICIENTS OF PURE CO2 AND PURE
C     H2O CAN BE USED TO CALCULATE ACTIVITIES OF CO2 AND H2O IN THE
C     MIXTURE FOR EACH PRESSURE, TEMPERATURE, AND XCO2.
C     SEE KERRICK AND JACOBS (IN PRESS) FOR A DERIVATION OF FUGACITY
C     COEFFICIENT EXPRESSION (FCM)
C
C
      B = BM
      V = VM
      Z = ZM
      C = CM
      D = DM
      E = EM
      Y = B / (4.0D0*V)
      IF (M .EQ. 1) THEN
        B1 = BC
        C1 = CC
        D1 = DC
        E1 = EC
        X1 = XC
        X2 = XW
      ELSE
        B1 = BW
        C1 = CW
        D1 = DW
        E1 = EW
        X1 = XW
        X2 = XC
      END IF
      FCM = (4.0D0*Y - 3.0D0*Y*Y) / ((1.0D0-Y)**2)
      FCM = FCM + ((B1/B)*((4.0D0*Y - 2.0D0*Y*Y)/((1.0D0-Y)**3)))
      FCM = FCM - (((2.0D0*C1*X1 + 2.0D0*CIJ*X2)/(RT*B))*(DLOG((V + B)/
     1V)))
      FCM = FCM - ((C*B1)/(RT*B*(V + B)))
      FCM = FCM + (((C*B1)/(RT*B*B))*(DLOG((V + B)/V)))
      FCM = FCM - ((2.0D0*D1*X1 + 2.0D0*DIJ*X2 + D)/(RT*B*V))
      FCM = FCM + (((2.0D0*X1*D1 + 2.0D0*DIJ*X2 + D)/(RT*B*B))*(DLOG((V
     1+ B)/V)))
      FCM = FCM + ((D*B1)/(RT*V*B*(V + B)))
      FCM = FCM + ((2.0D0*B1*D)/(RT*B*B*(V + B)))
      FCM = FCM - (((2.0D0*B1*D)/(RT*(B**3)))*(DLOG((V + B)/V)))
      FCM = FCM - ((2.0D0*E1*X1 + 2.0D0*EIJ*X2 + 2.0D0*E)/(RT*2.0D0*B*V*
     1V))
      FCM = FCM + ((2.0D0*E1*X1 + 2.0D0*EIJ*X2 + 2.0D0*E)/(RT*B*B*V))
      FCM = FCM - (((2.0D0*E1*X1 + 2.0D0*EIJ*X2 + 2.0D0*E)/(RT*(B**3)))*
     1(DLOG((V + B)/V)))
      FCM = FCM + ((E*B1)/(RT*2.0D0*B*V*V*(V + B)))
      FCM = FCM - ((3.0D0*E*B1)/(RT*2.0D0*B*B*V*(V + B)))
      FCM = FCM + (((3.0D0*E*B1)/(RT*(B**4)))*(DLOG((V + B)/V)))
      FCM = FCM - ((3.0D0*E*B1)/(RT*(B**3)*(V + B)))
      FCM = FCM - (DLOG(Z))
      FCM = DEXP(FCM)
      IF (M .EQ. 1) THEN
        FCCM = FCM
      ELSE
        FCWM = FCM
      END IF
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE MIXKJ(TEMP, PBAR, XCO2, FGCO, FGH2O, ACO, AH2O)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
      COMMON /MVAR/ BC, CC, DC, EC, BW, CW, DW, EW, J, L, M
      COMMON /MNRVAR/ PK, P, T, R, T12
      P = PBAR
      T = TEMP
C
C       VARIABLES AND RULES ARE AS FOLLOWS:
C
C        ACO2,AH2O....ACTIVITY OF CO2 AND H2O, RESPECTIVELY
C        BC,BW,BM.....COVOLUME OF CO2,H2O AND MIXTURE; CC/MOLE
C        CC,CW,CM.....ATTRACTIVE TERM FOR CO2, H2O, AND MIXTURE IN
C                     MRK EQUATION ; BAR*(CC**2)**2SQRT(T)/MOLE**2
C        DC, DW,,DM...ATTRACTIVE TERM FOR CO2, H2O, AND MIXTURE IN
C                     MRK EQUATION; BAR*(CC**3)*SQRT(T)/MOLE**3
C        EC,EW,EM.....ATTRACTIVE TERM FOR CO2, H2O, AND MIXTURE IN
C                     MRK EQUATION; BAR*(CC**4)*SQRT(T)/MOLE**4
C        CIJ,DIJ,EIJ..CROSS COEFFICIENTS OF C,D,E
C        FKCM,FKWM....FUGACITY COEFFICIENT OF CO2 AND H2O IN
C                     THE FLUID MIXTURE
C        FKCP,FKWP....FUGACITY COEFFICIENTS OF PURE CO2 AND PURE
C                     H2O, RESPECTIVELY
C        PK,P.........PRESSURE; KBARS,BARS, RESPECTIVELY
C        R............GAS CONSTANT; 83.14 CC*BARS/MOLE*K
C        TC,T.........TEMPERATURE; CELSIUS,KELVIN, RESPECTIVELY
C        VC,VW,VM.....MOLAR VOLUME OF CO2, H2O, AND MIXTURE; CC/MOLE
C        XC,XCO2......MOLE FRACTION OF CO2 IN THE FLUID MIXTURE
C        XW,XH2O......MOLE FRACTION OF H2O IN THE FLUID MIXTURE
C        Y............B/V4; VARIABLE IN HARD SPHERE-EQUATION
C        ZC,ZW,ZM.....COMPRESSIBILITY OF CO2, H2O, AND MIXTURE
C
C        DEFINITION OF CONSTANRS AND INPUT FOR THE CALCULATION OF
C    THE P, T, XCO2 VALUES WHICH ARE USED THROUGHOUT THE PROGRAM.
C
      R = 83.14D0
      BW = 29.00D0
      BC = 58.00D0
C
C        CALCULATION OF PARAMETERS USED IN THE PROGRAM.
C
      PK = P / 1000.0D0
      TC = T - 273.15D0
      XC = XCO2
      XW = 1.0D0-XC
      ACO = XC
      AH2O = XW
      T15 = DSQRT(T**3)
      T12 = DSQRT(T)
      RT = R * T15
      CC = (28.31D0+0.10721D0*T - 0.00000881D0*T*T) * 1000000.0D0
      DC = (9380.0D0-8.53D0*T + 0.001189D0*T*T) * 1000000.0D0
      EC = (-368654.0D0+715.9D0*T + 0.1534D0*T*T) * 1000000.0D0
      CW = (290.78D0-0.30276D0*T + 0.00014774D0*T*T) * 1000000.0D0
      DW = (-8374.0D0+19.437D0*T - 0.008148D0*T*T) * 1000000.0D0
      EW = (76600.0D0-133.9D0*T + 0.1071D0*T*T) * 1000000.0D0
      IF (TC .GE. 325.0D0 .AND. TC .LE. 1050.0D0) THEN
        BM = (BC*XC) + (BW*XW)
        CIJ = DSQRT(CC*CW)
        DIJ = DSQRT(DC*DW)
        EIJ = DSQRT(EC*EW)
        CM = (CC*XC*XC) + (CW*XW*XW) + (2.0D0*XC*XW*CIJ)
        DM = (DC*XC*XC) + (DW*XW*XW) + (2.0D0*XC*XW*DIJ)
        EM = (EC*XC*XC) + (EW*XW*XW) + (2.0D0*XC*XW*EIJ)
      END IF
C
C    ROUTINES ZPURE, ZMIX, FPURE, AND FMIX ARE CALLED TO
C    CALCULATE:
C
C        1) Z OF PURE CO2 AND H2O RESPECTIVELY
C        2) Z OF CO2-H2O MIXTURES
C        3) FUGACITY COEFFICIENTS OF CO2 AND H2O RESPECTIVELY
C        4) FUGACITY COEFFICIENTS OF CO2 AN H2O IN THE MIXTURE
C
C    AT EACH P, T, XCO2 CONDITION.
C
      DO 10 J = 1, 2
        CALL ZPURE(PK, ZC, VC, ZW, VW, TC)
   10 CONTINUE
      IF (TC .GE. 325.0D0 .AND. TC .LE. 1050.0D0) THEN
        CALL ZMIX(XC, XW, VM, ZM, VC, VW, BM, CM, DM, EM)
      END IF
      DO 20 L = 1, 2
        CALL FPURE(RT, FKWP, FKCP, ZC, VC, ZW, VW)
   20 CONTINUE
      FGCO = FKCP
      FGH2O = FKWP
C
C     CALCULATION AND DEFINITION OF ARRAYS FOR ACTIVITIES,
C     FUGACITIES IN THE MIXTURE, AND MOLE FRACTION.
C
      IF (TC .GE. 325.0D0 .AND. TC .LE. 1050.0D0) THEN
        DO 30 M = 1, 2
          CALL FMIX(RT, CIJ, DIJ, EIJ, XC, XW, BM, FCCM, FCWM, FCM, VM,
     1         ZM, CM, DM, EM)
   30   CONTINUE
        ACO = FCCM * XC / FKCP
        AH2O = FCWM * XW / FKWP
      ELSE
        ACO = XCO2
        AH2O = XW
      END IF
C
C   REFERENCES:
C
C       KERRICK, D. M. AND JACOBS, G. K. IN PRESS; A MODIFIED
C   REDLICH-KWONG EQUATION FOR H2O, CO2, AND H20-CO2 MIXTURES AT
C     ELEVATED PRESSURES AND TEMPURATURES, AM. JOUR. SCI.,IN PRESS.
C
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE WDH78(TK, P, GDIF, VDH)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
C
C      RETURNS DIFFERENCE IN FREE ENERGY OF WATER
C      BETWEEN P,T AND 10KB,T
C
      DOUBLE PRECISION A(5,5)
C
      A(1,1) = -5.6130073D+04
      A(1,2) = 3.8101798D-01
      A(1,3) = -2.1167697D-06
      A(1,4) = 2.0266445D-11
      A(1,5) = -8.3225572D-17
      A(2,1) = -1.5285559D+01
      A(2,2) = 1.3752390D-04
      A(2,3) = -1.5586868D-09
      A(2,4) = 6.6329577D-15
      A(3,1) = -2.6092451D-02
      A(3,2) = 3.5988857D-08
      A(3,3) = -2.7916588D-14
      A(4,1) = 1.7140501D-05
      A(4,2) = -1.6860893D-11
      A(5,1) = -6.0126987D-09
C
      T = TK - 273.15
      II = 0
      P1 = 10000.0D0
C
   10 GH2O = 0.0D0
      VH2O = 0.0D0
      DO 30 J = 1, 5
        DO 20 L = 1, (6 - J)
          JJ = J - 1
          LL = L - 1
          IF (JJ .EQ. 0 .AND. LL .EQ. 0) GH2O = GH2O + A(J,L)
          IF (JJ .GT. 0 .AND. LL .GT. 0) GH2O = GH2O + (A(J,L)*T**(JJ)*
     1    P1**(LL))
          IF (JJ .EQ. 0 .AND. LL .GT. 0) GH2O = GH2O + (A(J,L)*P1**(LL))
          IF (JJ .GT. 0 .AND. LL .EQ. 0) GH2O = GH2O + (A(J,L)*T**(JJ))
C              IF (LL.NE.0) VH2O = VH2O + (A(J,L) * LL * T**JJ
C    .                                                     * P**(LL-1))
   20   CONTINUE
   30 CONTINUE
C
      VDH = VH2O / .0239
C
      IF (II .EQ. 0) THEN
        G10KB = GH2O
        P1 = P
        II = 1
        GO TO 10
      END IF
C
   40 GDIF = (GH2O - G10KB) * 4.184
C
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE PSAT2(T, PS)
C
C      Written by Christian De Capitan, Dept. Geological Sciences,
C            UBC, Vancouver, BC, Canada
C
      DOUBLE PRECISION T, PS, A(8), W, WSQ, V, FF
C
      DATA A /-7.8889166D0, 2.5514255D0, -6.716169D0, 33.239495D0,
     1     -105.38479D0, 174.35319D0, -148.39348D0, 48.631602D0/
C
      IF (T .LE. 314.00D0) THEN
        PS = DEXP(6.3573118D0-8858.843D0/T + 607.56335D0/(T**0.6D0))
      ELSE
        V = T / 647.25D0
        W = DABS(1.0D0-V)
        WSQ = DSQRT(W)
        FF = 0.0D0
        DO 10 I = 1, 8
          FF = FF + A(I) * W
          W = W * WSQ
   10   CONTINUE
        PS = 220.93D0 * DEXP(FF/V)
      END IF
      RETURN
      END

C  --------------------------------------------------------------
      SUBROUTINE KUBIK(B, C, D, X1, X2, X2I, X3)
C
C      Written by Christian De Capitan, Dept. Geological Sciences,
C            UBC, Vancouver, BC, Canada
C
      DOUBLE PRECISION B, C, D, Q, P, R, PI, PHI3, FF
      DOUBLE PRECISION X1, X2, X2I, X3
C
      PI = 3.14159263538979D0
      X2 = 0.0D0
      X2I = 0.0D0
      X3 = 0.0D0
      IF (C .EQ. 0.0D0 .AND. D .EQ. 0.0D0) THEN
        X1 = -B
        RETURN
      END IF
      Q = ((2.D0*B**3)/(27.D0) - (B*C)/(3.D0) + D) / 2.D0
      P = (3.D0*C - B**2) / (9.D0)
      FF = DABS(P)
      R = DSQRT(FF)
      FF = R * Q
      IF (FF .LT. 0.0D0) R = -R
      FF = Q / (R**3)
C
      IF (P .GT. 0.0D0) THEN
        PHI3 = DLOG(FF + DSQRT(FF**2 + 1.D0)) / 3.D0
        X1 = -R * (DEXP(PHI3) - DEXP(-PHI3)) - B / 3.D0
        X2I = 1.0D0
      ELSE
        IF (Q**2 + P**3 .GT. 0.0D0) THEN
          PHI3 = DLOG(FF + DSQRT(FF**2 - 1.D0)) / 3.D0
          X1 = -R * (DEXP(PHI3) + DEXP(-PHI3)) - B / 3.D0
          X2I = 1.0D0
        ELSE
          PHI3 = DATAN(DSQRT(1.D0-FF**2)/FF) / 3.D0
          X1 = -2.D0 * R * DCOS(PHI3) - B / 3.D0
          X2 = 2.D0 * R * DCOS(PI/3.D0-PHI3) - B / 3.D0
          X2I = 0.0D0
          X3 = 2.D0 * R * DCOS(PI/3.D0+PHI3) - B / 3.D0
        END IF
      END IF
      RETURN
      END
C  *********************************************************
C  # MADER & BERMAN EQUATION OF STATE FOR CO2              #
C  #                                                       #
C  #    Ref:  - Mader et al., 1988,                        #
C  #            Geol. Soc. Amer., Abstr. w. Progr., p.A190 #
C  #          - Mader, U.K., 1990, Ph.D., UBC              #
C  #          - Mader & Berman, 1991, Am. Min. ... ...     #
C  *********************************************************
C These subroutines compute the volume and fugacity of CO2
C at specified pressure and temperature:
C
C P = RT / (V-B) - A1 / T*V**2 + A2 / V**4
C   B = B1 + B2*T - B3/(V**3+C);  C = B3/(B1+B2*T)
C
C Program written by Urs Mader, June 1987
C   modified: Sept. 1988, by Urs (better volume iteration)
C   modified: July 1989, by Urs (clean up integration code)
C   modified: March 1990, by Urs (fool proof!?! V iterator)
C   added to ptax by RGB, Aug, 1991
C
C Program is written in standard FORTRAN 77
C
C Note: Rootfinder is not designed to handle subcritical P
C and T simultaneously (i.e. 88.754 bar, 332.74 K). To do so,
C the rootfinder must be capable of recognizing subcritical
C conditions and find the saturated volumes by equating
C f(vap) = f(liq).
C
C List of subroutines:
C MABE90: main subroutine, returns V, fugacity, etc. at P,T
C CO2VOL: computes V at P,T using CO2BIS,CO2ROO,CO2FUN
C CO2INT: computes int(V)dP with CO2ROO,CO2IN3,CO2IN1,CO2TIR
C  CO2BIS: converges to V at P,T by interval halfing
C   CO2FUN: returns the function value of the equation
C  CO2ROO: finds root(s) of 3rd order polynomial
C  CO2IN3: integrates attract. term (1st case, 3 real roots)
C  CO2IN1: integrates attract. term (2nd case, 1 real root)
C   CO2TIR: auxiliary routine for CO2IN1
C
C List of variables:
C   PBAR,P   : pressure [bar]
C   TK,T     : temperature [K]
C   V0       : volume at 1 bar and T [cm**3/mole]
C   VP,V     : volume at P and T [cm**3/mole]
C   VJ       : volume at P and T [Joule/bar]
C   R        : 10*gas constant (10*8.3147 [Joule/mole/K])
C   RTLNF    : R/10*T*ln(f)CO2 [Joule]
C   FUG      : fugacity [bar]
C   FUGLN    : ln(FUG)
C   FUGCOE   : fugacity coefficient
C   Z        : compressibility  {=R*T/(P*V)}
C   RHO      : density [kg/m**3]
C   RHOCGS   : density [g/cm**3]
C   A1,A2,B1,B2,B3 : constants for equation of state
C   B        : = B1 + B2 * TK
C   C        : = B3 / B
C   MAXIT    : iteration limit for routine CO2BIS
C   V1,V2    : volumes used during iteration
C   NROOT    : no. of roots of 3rd order polynomial
C   X1,X2,X3 : roots of 3rd order polynomial
C ==========================================================
      SUBROUTINE MABE90(PBAR,TK,VP,FUGCOE)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /PAR/ R,B,B3,C,A1,A2
      COMMON /LIMIT/ MAXIT
C     LOGICAL FIRST/T/        mainframe version
      LOGICAL FIRST/.TRUE./
C
      IF(.NOT.FIRST) GOTO 11
      FIRST = .FALSE.
C fit parameters: March 26, 1990
C                 121 phase equil. constraints, 440 PVT
      B1 = 28.064740D00
      B2 = 1.7287123D-4
      B3 = 8.3653408D04
      A1 = 1.0948021D09
      A2 = 3.3747488D09
      R  = 83.147D00
      MAXIT = 50
 11   B  = B1+B2*TK
      C  = B3/B
C test for subcritical P and T
      IF((PBAR.LT.88.745D0).AND.(TK.LT.332.74D0)) WRITE(6,*)
     & 'subcritical P or T in CO2 routine'
C compute VP,VJ,Z,RHO at PBAR,TK
      CALL CO2VOL(VP,V0,PBAR,TK)
      VJ = VP/10.0
      Z      = PBAR*VP/R/TK
      RHOCGS = 44.010/VP
      RHO    = RHOCGS*1000.0
C compute RTLNF,FUGLN,FUGCOE,FUG at PBAR,TK
      CALL CO2INT(RTLNF,PBAR,TK,VP,V0)
      FUGLN  = RTLNF/R/TK*10.0
      FUG    = DEXP(FUGLN)
      FUGCOE = FUG/PBAR
      RETURN
      END
C ----------------------------------------------------------
C subroutine to compute VP of CO2 at P and T
      SUBROUTINE CO2VOL(VP,V0,P,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /PAR/ R,B,B3,C,A1,A2
      COMMON /LIMIT/ MAXIT
C approx. V0 at 1 bar and set init. guess (V1,V2) to find VP
      V0 = R*T+B
      V2 = (R*T/P+B)*1.2
C compute minimum volume VMIN at T for P = inf.
C (solve V**3 - V**2*B + C = 0; from V = B - B3/(V**3 + C))
      WP = -B
      WR = C
      CALL CO2ROO(WP,WR,X1,X2,X3,NROOT)
      V1 = X1
      IF(NROOT.EQ.3) THEN
         IF(X2.GT.V1) V1 = X2
         IF(X3.GT.V1) V1 = X3
      ENDIF
      CALL CO2BIS(V1,V2,VP,P,T)
      RETURN
      END
C ----------------------------------------------------------
C subroutine to compute int(V)dP of CO2
      SUBROUTINE CO2INT(RTLNF,P,T,VP,V0)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /PAR/ R,B,B3,C,A1,A2
C integrate equation of state from 1 bar to P;
C note: int(V)dP = int(P)dV + V*(P-1) - (V-V0)*1
      WP=-B
      WR=C
C   compute roots X1,X2,X3 of 3rd order polynominal in V
C   (denominator of repulsive term of equation of state)
C   and integrate repulsive term (TERM1,TERM2)
      CALL CO2ROO(WP,WR,X1,X2,X3,NROOT)
      IF(NROOT.EQ.3) CALL CO2IN3(X1,X2,X3,V0,VP,T1,T2)
      IF(NROOT.EQ.1) CALL CO2IN1(X1,WP,V0,VP,T1,T2)
      TERM1 = R*T*T1
      TERM2 = R*T*C*T2
C   integrate attractive terms of equation of state
      TERM3 = A1/T*(1.0/V0-1.0/VP)
      TERM4 = -A2/3.0*(1.0/V0/V0/V0-1.0/VP/VP/VP)
      TERM5 = V0-VP
      TERM6 = (P-1.0)*VP
      RTLNF = (TERM1+TERM2+TERM3+TERM4-TERM5+TERM6)/10.0
      RETURN
      END
C ----------------------------------------------------------
C subroutine for rootfinding for CO2 equation of state
      SUBROUTINE CO2BIS(X1,X2,XMID,P,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /PAR/ R,B,B3,C,A1,A2
      COMMON /LIMIT/ MAXIT
      ACC = X1*1.0D-6
      X  = X2
      DX = X1-X2
      DO 11 J=1,MAXIT
         DX = DX*0.5
         XMID = X+DX
         CALL CO2FUN(XMID,FMID,P,T)
         IF(FMID.LT.0.0D00) X=XMID
         IF((DABS(DX).LT.ACC).OR.(DABS(FMID).LT.1.0D-08))
     &       RETURN
  11  CONTINUE
      WRITE(6,*) 'MAXIT (CO2BIS) EXEEDED'
      RETURN
      END
C subroutine to evaluate equation of state
      SUBROUTINE CO2FUN(V,FV,P,T)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON /PAR/ R,B,B3,C,A1,A2
      VSQ = V*V
      FV  = R*T/(V-B+B3/(VSQ*V+C))-A1/T/VSQ+A2/VSQ/VSQ-P
      RETURN
      END
C ----------------------------------------------------------
C subroutines for integration of repulsive term
      SUBROUTINE CO2ROO(WP,WR,X1,X2,X3,NROOT)
      IMPLICIT REAL*8(A-H,O-Z)
C find root(s) of polynominal X**3 + WP*X**2 + WR*X
      QA = (-WP*WP)/3.0
      QB = (2.0*WP*WP*WP+27.0*WR)/27.0
      DET = QB*QB/4.0+QA*QA*QA/27.0
      IF(DET.GT.0.0) THEN
         ARGF = DSQRT(DET)-QB/2.0
         ARGE = -DSQRT(DET)-QB/2.0
         SIGF = DABS(ARGF)/ARGF
         SIGE = DABS(ARGE)/ARGE
         QF = DEXP(DLOG(DABS(ARGF))/3.0)*SIGF
         QE = DEXP(DLOG(DABS(ARGE))/3.0)*SIGE
         X1 = QF+QE-WP/3.0
         NROOT = 1
      ELSE
         PI = 3.1415926536D00
C        PHI = DARCOS(-QB/2.0/DSQRT(-QA*QA*QA/27.0))   mainframe version
         PHI = DACOS(-QB/2.0/DSQRT(-QA*QA*QA/27.0))
         RM = 2.0*DSQRT(-QA/3.0)
         X1 = RM*DCOS(PHI/3.0)-WP/3.0
         X2 = RM*DCOS(PHI/3.0+2.0*PI/3.0)-WP/3.0
         X3 = RM*DCOS(PHI/3.0+4.0*PI/3.0)-WP/3.0
         NROOT = 3
      ENDIF
      RETURN
      END
C integration for case with 3 real roots X1,X2,X3
      SUBROUTINE CO2IN3(X1,X2,X3,V0,V,T1,T2)
      IMPLICIT REAL*8(A-H,O-Z)
C term 1 :  V**2 / (V**3 - V**2 * B + C)
      DIV = X1*X1*(X2-X3)+X2*X2*(X3-X1)+X3*X3*(X1-X2)
      SA = X1*X1/(X1*(X1-X2-X3)+X2*X3)
      SB = X2*X2*(X3-X1)/DIV
      SC = X3*X3*(X1-X2)/DIV
      T1 = SA*(DLOG(V0-X1)-DLOG(V-X1))+
     &     SB*(DLOG(V0-X2)-DLOG(V-X2))+
     &     SC*(DLOG(V0-X3)-DLOG(V-X3))
C term 2 :  1 / V / (V**3 - V**2 * B + C)
      SA = -1.0/X1/X2/X3
      SB = 1.0/X1/(X1*(X1-X2-X3)+X2*X3)
      SC = -1.0/X2/(X2*(X1+X3-X2)-X1*X3)
      SD = 1.0/X3/(X3*(X3-X2-X1)+X1*X2)
      T2 = SA*(DLOG(V0)-DLOG(V))+SB*(DLOG(V0-X1)-DLOG(V-X1))
     &     +SC*(DLOG(V0-X2)-DLOG(V-X2))
     &     +SD*(DLOG(V0-X3)-DLOG(V-X3))
      RETURN
      END
C integration for case with 1 real root X1
      SUBROUTINE CO2IN1(X1,WP,V0,V,T1,T2)
      IMPLICIT REAL*8(A-H,O-Z)
      ALP = X1+WP
      BET = X1*(X1+WP)
C term 1
      DIV = X1*(X1+ALP)+BET
      SA = X1*X1/DIV
      SB = (ALP*X1+BET)/DIV
      SC = X1*BET/DIV
      CALL CO2TIR(SB,SC,ALP,BET,V0,V,TR)
      T1 = SA*(DLOG(V0-X1)-DLOG(V-X1))+TR
C term 2
      SA = -1.0/BET/X1
      SB = 1.0/X1/DIV
      SC = (ALP+X1)/BET/DIV
      SD = (ALP*(ALP+X1)-BET)/BET/DIV
      CALL CO2TIR(SC,SD,ALP,BET,V0,V,TR)
      T2 = SA*(DLOG(V0)-DLOG(V))+SB*(DLOG(V0-X1)
     &     -DLOG(V-X1))+TR
      RETURN
      END
C auxiliar routine for CO2IN1
      SUBROUTINE CO2TIR(S1,S2,ALP,BET,V0,V,TR)
      IMPLICIT REAL*8(A-H,O-Z)
      RDET = DSQRT(4.0*BET-ALP*ALP)
      IF(RDET.LT.1.0D-15) RDET = 1.0D-15
      TR = S1/2.0*(DLOG(V0*V0+ALP*V0+BET)
     &     -DLOG(V*V+ALP*V+BET))
     &     +(2.0*S2-ALP*S1)/RDET*(DATAN((2.0*V0+ALP)/RDET)
     &     -DATAN((2.0*V+ALP)/RDET))
      RETURN
      END
      SUBROUTINE SOLN(P, TK, GSS, CODE)
      IMPLICIT DOUBLEPRECISION(A - H,O - Z)
C
C     THIS ROUTINE CALCULATES THE EXTRA ENERGY FOR "WEIRD" PHASES -
C     PHASES THAT DISPLAY UNUSUAL ACTIVITY PROPERTIES
C
C     PASSED VARIABLES - P     PRESSURE IN BARS
C                        TK    TEMPERATURE IN DEG K
C                        CODE  IDENTIFIES THE PHASE
C
      PARAMETER (MAXPHS=100,MAXCMP=15,MAXVOL=18)
      COMMON /THERMM/ THERMO(11,MAXPHS), COMP(MAXCMP,MAXPHS),
     1       REAC(MAXPHS), ACT(MAXPHS), THERLM(9,20), VOLPAR(MAXVOL,
     2       MAXPHS), THERDI(10,20), IWEIRD(MAXPHS), NDX(MAXPHS),
     3       IDIS(MAXPHS), LMDA(MAXPHS,3), NPHASE, NPR, NCTZ
     	COMMON/MPHASE/MPHASE
      CHARACTER*8 NAMEA(MAXPHS)
      CHARACTER*120 TITLE
      CHARACTER*20 NAME(MAXPHS)
      COMMON /CHNAME/ TITLE, NAME, NAMEA
C
      CHARACTER*20 CODE
C     CHARACTER*1 ANSWER
C
      TR = 298.15
      R = 1.98726
      RR = 8.31469
      GSS = 0.0D00
C     -------------------------------------------------------------
      IF (CODE(1:10) .EQ. 'CORDIERITE' .OR. CODE(1:8) .EQ. 'HYD-CORD')
     1       THEN
C
C  ** CORDIERITE ** BEAR'S JULY 26, 1984 FIT TO DATA OF MIRWALD (1979)
C     AND 2 KB DATA OF DUNCAN'S THESIS (1982), USING ONLY ONE POLYMORPH
C        SINCE THE DIFFERENCE IN POSITIONS OF THE ISOPLETHS USING
C        TWO POLYMORPHS IS ALMOST INVISIBLE
C          ** MIXING ON 2 SITES IN CORDIERITE **
C
C    ** FIT OF DATA WITH HAAR WATER ROUTINE **
C
        H = -563493.8
        S = 156.665
        V = 23.06
        A = 270.7386
        B = -.009046
        C = -8152144.
        D = -9633.768
        E = 0.0
        F = 133752.
C
        X1 = -A * TK * (DLOG(TK) - DLOG(TR))
        X2 = A * (TK - TR)
        X3 = (B/2.0D00*(TK**2 - (TR**2))) - (B*TK*(TK - TR))
        X4 = (C/2.0D00*((1/TK) - (TK/TR**2))) - (C*(1/TK - 1/TR))
        X5 = 2.0D00 * D * ((TK**0.5) - (TR**0.5)) + 2.0D00 * D * TK * (
     1  TK**(-0.5) - TR**(-0.5))
        X6 = E * (TK**3 - TR**3) / 3.0D00-E * TK * (TK**2 - TR**2) / 2.
     1  0D00
        X7 = F * DLOG(TK/TR) + F * (1.0D00-TK/TR)
        Z = X1 + X2 + X3 + X4 + X5 + X6 + X7
        G = (H - (TK*S) + Z) / 4.184
        G = G + ((P - 1.0)*V*0.02390064D00)
C
        PP = P
        IF (P .GT. 10000.) PP = 10000.
        CALL WHAAR(PP, TK, GWAT, VH2O)
        GWATER = GWAT - 285829.96 - (298.15*69.9146)
        IF (P .GT. 10000.) THEN
          CALL WDH78(TK, P, GDIF, VH2O)
          GWATER = GWATER + GDIF
        END IF
C
        G = G + (-2.0D0) * (GWATER/4.184)
        XK = DEXP(-G/R/TK/2.)
        XXX = XK / (1. + XK)
        IF (CODE(1:10) .EQ. 'CORDIERITE') THEN
          ACTIV = 1.0D0-XXX
        ELSE
          ACTIV = XXX
        END IF
        ACTIV = ACTIV * ACTIV
        GSS = RR * TK * DLOG(ACTIV)
        RETURN
C     -------------------------------------------------------------
      ELSE IF (CODE(1:9) .EQ. 'CALCITESS') THEN
C
C     CALCITE SOLID SOLUTION WITH DOLOMITE FROM EQN. OF SKIPPEN (1974)
C             DEFAULT IS TO USE THIS EQUATION
C
        DO 10 K = 1, NPHASE
          IF (DABS(REAC(K)) .GE. 0.01 .AND. NDX(K) .EQ. 0)
     1           THEN
            XLNA = (157.45/TK) - .2533
            GSS = RR * TK * XLNA
            RETURN
          END IF
   10   CONTINUE
        RETURN
C     -------------------------------------------------------------
      ELSE IF (CODE(1:11) .EQ. 'ALENSTATITE') THEN
C
C             *****     AL-ENSTATITE SOLUTION     *****
C
C     GASPARIK & NEWTON 1984 MODELS FOR ENSTATITE IN EQUILIBRIUM
C     WITH PYROPE AND WITH SPINEL AND FORSTERITE - G(SOLUTION) IS
C     CALCULATED BY USING G & N TO FIND X(MG(1)AL(2)SI(1)O(6) AS A
C     FUNCT. OF P AND T AND THEN GSS=RT*LN( X(MG(2)SI(2)O(6) ).
C
        PBARS = P
        ITH = 0
        DO 20 K = 1, NPHASE
          IF (DABS(REAC(K)) .GT. 0.01 .AND. NDX(K) .EQ. 0)
     1           THEN
            IF (NAME(K)(1:3) .EQ. 'Py ' .OR. NAME(K)(1:7) .EQ.
     1         'PYROPE ') GO TO 30
            IF (NAME(K)(1:3) .EQ. 'Sp ' .OR. NAME(K)(1:7) .EQ.
     1      'SPINEL ') ITH = ITH + 1
            IF (NAME(K)(1:3) .EQ. 'Fo ' .OR. NAME(K)(1:11) .EQ.
     1      'FORSTERITE ') ITH = ITH + 1
          END IF
   20   CONTINUE
        IF (ITH .GT. 1) GO TO 60
        GSS = 0.0
        RETURN
C
C             ***** OPYX STABLE WITH PYROPE *****
C
   30   VPP = (-0.832D0-(8.78D-05)*(TK - 298.0D0) + (16.6D-07)*PBARS) *
     1  PBARS
        RTLNXP = 5510.0D0-88.91D0 * TK + 19.0D0 * (TK**1.2D0) + VPP
        XMGTSP = DEXP(RTLNXP/(RR*TK))
        IF (XMGTSP .GT. 0.250D0) RETURN
        ROOTP = SQRT(1.0D0-(4.0*XMGTSP))
        XMGTSP = (1.0D0-ROOTP) / 2.0D0
        XORENP = 1.0D0-XMGTSP
        IF (XORENP .LT. 0.00D0) RETURN
        GSS = RR * TK * DLOG(XORENP) / 2.0
C      WRITE (99,999) P,TK,gss,NAME(II)
   40   FORMAT (3F15.3, 5X, A20)
   50   FORMAT (3F15.3, 5X, A20, A20)
        RETURN
C
C       ***** OPYX STABLE WITH SPINEL AND FORSTERITE *****
C
   60   VP = (0.013D0+3.34D-05*(TK - 298.0D0) - 6.6D-07*PBARS) * PBARS
        RTLNX = -29190.0D0+13.42D0 * TK - 0.18D0 * (TK**1.5) - VP
        XMGTS = DEXP(RTLNX/(RR*TK))
        XMGTS = XMGTS / (1.0D0+XMGTS)
        XOREN = 1.0D0-XMGTS
        IF (XOREN .LT. 0.0D0) RETURN
        GSS = RR * TK * DLOG(XOREN) / 2.0
C      WRITE (99,999) P,TK,gss,NAME(II)
        RETURN
C     -------------------------------------------------------------
      END IF
C
      END
