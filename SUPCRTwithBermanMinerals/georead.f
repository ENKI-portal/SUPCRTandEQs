	SUBROUTINE GEOREAD
	IMPLICIT DOUBLE PRECISION(A-H,O-Z)

       PARAMETER (MAXPHS=100, MAXCMP=15, MAXVOL=18)

       COMMON /THERMM/ THERMO(11,MAXPHS), COMP(MAXCMP,MAXPHS),

     .                 REAC(MAXPHS), ACT(MAXPHS), THERLM(9, 20),

     .                 VOLPAR(MAXVOL,MAXPHS), THERDI(10, 20),

     .                 IWEIRD(MAXPHS), NDX(MAXPHS), IDIS(MAXPHS),

     .                 LMDA(MAXPHS,3), NPHASE, NPR, NCTZ

      COMMON /INTER/ A, B, C, XD, XE, XF, XG, G, H, XMF(10),

     .                XNMOL(10),P, S, TC, V, XOTHER, XNACL

       COMMON /INFO  / AXX(3), AXY(3), YSIZE, XSIZE, NPNUM, ILABEL

       COMMON /PASS/ GTOT,HTOT,STOT,CPTOT,VTOT

       COMMON /LAMB/ GLAM,HLAM,SLAM,CPLAM,VLAM

       COMMON /DISOR/GDIS,HDIS,SDIS,CPDIS,VDIS 

       CHARACTER*8 NAMEA(MAXPHS),VERSN

       CHARACTER*20 NAME(MAXPHS)

       CHARACTER*120 TITLE

       CHARACTER*120 FILE
        DIMENSION INFP(MAXPHS+1)

       COMMON /CHNAME/ TITLE, NAME, NAMEA

       COMMON /EXCL  / KNCR(MAXCMP), IEXCL(50,MAXCMP+1), MEX, ICL,

     .                 NRC, NRR, IAF, INF

       DATA VERSN/'NOV 1987'/

	OPEN(3,FILE='berman.dat',STATUS='OLD')
C	OPEN(10,FILE='RESTART.DAT',STATUS='UNKNOWN')
C	OPEN(13,FILE='TABLE.DAT',STATUS='UNKNOWN')
	CALL PREAD1
C	WRITE(10,*)'after pread1'
C	CALL ELREAD
	CALL PREAD2(1)
C	WRITE(10,*)'AFTER PREAD2'
	CLOSE(3)
	

	END	

       SUBROUTINE ELREAD

       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C

C      THIS ROUTINE ASKS FOR THE SYSTEM ELEMENTS

C      ONLY ELEMENTS GIVEN HERE WILL BE CONSIDERED

C

       PARAMETER (MAXPHS=100, MAXCMP=15, MAXVOL=18)

       COMMON /THERMM/ THERMO(11,MAXPHS), COMP(MAXCMP,MAXPHS),

     .                REAC(MAXPHS), ACT(MAXPHS), THERLM(9, 20),

     .                VOLPAR(MAXVOL,MAXPHS), THERDI(10, 20),

     .                IWEIRD(MAXPHS), NDX(MAXPHS), IDIS(MAXPHS),

     .                LMDA(MAXPHS,3), NPHASE, NPR, NCTZ

       COMMON /INFO  / AXX(3), AXY(3), YSIZE, XSIZE, NPNUM, ILABEL

       COMMON /DIAGRA/ XVAL, YVAL, ZVAL, IX(0:5), IY(0:5), IXCOEF(5),

     .                 IYCOEF(5), IPTX, ICRITC

       COMMON /ELDATA/ PHCMP(120), WEIGHT(120), PSYCO(120), NCCC

       CHARACTER*8 NCOMP(120)

       COMMON /NAMELE/ NCOMP

C

       CHARACTER*120 BUFFER, DUMMY

       CHARACTER*20  NDUM

       CHARACTER*8   NABV

C

C      ENTER SYSTEM DATA - DECODE IT - HOW MANY ELEMENTS

C

   10  WRITE( *,1001)

       IF (IPTX .EQ. 1 .OR. IPTX .EQ. 2) THEN

           DUMMY(1:6) = 'H O C '

           ISTART   = 7

       ELSE IF (IPTX .EQ. 3) THEN

           DUMMY(1:2) = 'O '

           ISTART   = 3

       ELSE

           ISTART = 1

       ENDIF

C

C      READ - IGNORE BLANK RESPONSES

C

       READ ( *,1002) DUMMY(ISTART:120)

       CALL IGNORE (DUMMY, ' ', 1, 120, IFOUND)

       IF (IFOUND .EQ. 0) GO TO 10

C      WRITE(10,1002) DUMMY(ISTART:120)

       ISTART = 1

C

C      REPLACE COMMAS WITH BLANKS

C

   30  CALL FINDC(DUMMY, ',', ISTART, 120, IFOUND)

       IF (IFOUND .NE. 0) THEN

           ISTART = IFOUND

           DUMMY(IFOUND:IFOUND) = ' '

           GO TO 30

       ENDIF

C

C      BUILD STRING INTO A FORMULA

C

       BUFFER(1:) = 'SYSTEM         '

       ISTART = 1

       J = 12

   40  CALL IGNORE (DUMMY, ' ', ISTART, 120, IFOUND)

       IF (IFOUND .EQ. 0) GO TO 70

       ISTART = IFOUND

       CALL FINDC  (DUMMY, ' ', ISTART, 120, IFOUND)

       IF (IFOUND .EQ. 0) IFOUND = 121

       DO 50 I = ISTART, (IFOUND - 1)

           IF (I .LE. 120) THEN

               BUFFER(J:J) = DUMMY(I:I)

               J = J + 1

           ENDIF

   50  CONTINUE

       BUFFER(J:) = '(1)'

       J = J + 3

       ISTART = IFOUND

       IF (ISTART .LT. 100) GO TO 40

C

C      GET ELEMENTS IN SYSTEM - HOW MANY

C

   70  CALL DECODE( PSYCO, NDUM, NCCC, NCOMP, BUFFER, NABV, 120, 0, IER)

C

       IF (IER .NE. 0) THEN

            WRITE( 9, 1004)

       ENDIF

C

       NCTZ = 0

       DO 200 I = 1, NCCC

           IF (PSYCO(I).NE.0.0) NCTZ = NCTZ + 1

  200  CONTINUE

C

       IF (NCTZ .GT. MAXCMP) THEN

           WRITE (9, 1003) NCTZ, MAXCMP

           STOP

       ENDIF

C

       RETURN

 1001  FORMAT(/,'  ENTER ELEMENTS IN SYSTEM - SEPARATE BY BLANKS OR',

     .          ' COMMAS ',/,'     NO DEFAULT ')

 1002  FORMAT(120A)

 1003  FORMAT(//,' *******************************************',/,

     .           ' YOU CHOSE',I4,' ELEMENTS BUT THE PROGRAM IS',/,

     .           ' DIMENSIONED FOR A MAXIMUM OF',I4,'.  EITHER',/,

     .           ' CHOSE A SIMPLIER SYSTEM OR RECOMPILE THE   ',/,

     .           ' PROGRAM AFTER CHANGING THE MAXCMP PARAMETER',/,

     .           ' *******************************************',//)

 1004  FORMAT(//,' ************ERROR return from DECODE in PREAD1 ',

     .         /,'             Call E.H. Perkins ')

       END

       SUBROUTINE PREAD2(ISWIT1) 

       IMPLICIT DOUBLE PRECISION(A-H,O-Z) 

C

C      READ PART TWO OF THE DATA BASE - THE THERMODYNAMIC PROPERTIES 

C      OF MINERALS AND GASES 

C

C      ISWIT1 = 0 -- DATA WITH H GREATER THAN 99999 IS EXCLUDED. 

C                    GASES WITHOUT CRITICAL INFO ARE EXCLUDED. 

C             = 1 -- ALL ACCEPTED 

C

C     THERMO(I,J) CONTAINS THE FOLLOWING (I = PROPERTY J=PHASE) 

C            1 = H                     2 = S 

C            3 = K0 (CONSTANT TERM)    4 = K5 (T TERM) 

C            5 = K2 (T**-2 TERM)       6 = VOLUME 

C            7 = K1 (T**-.5 TERM)      8 = K4 (T**-1 TERM) 

C            9 = K6 (T**2 TERM)       10 = K3 (T**-3 TERM) 

C           11 = G (GENERALLY NOT USED) 

C

C     K0 TO K6 ARE HEAT CAPACITY TERMS 

C

      PARAMETER (MAXPHS=100, MAXCMP=15, MAXVOL=18) 

      COMMON /THERMM/ THERMO(11,MAXPHS), COMP(MAXCMP,MAXPHS), 

     .                REAC(MAXPHS), ACT(MAXPHS), THERLM(9, 20), 

     .                VOLPAR(MAXVOL,MAXPHS), THERDI(10, 20), 

     .                IWEIRD(MAXPHS), NDX(MAXPHS), IDIS(MAXPHS), 

     .                LMDA(MAXPHS,3), NPHASE, NPR, NCTZ 

       COMMON /CHOOSE/ ICHH2O, ICHCO2, ICHMIX 

       COMMON /DIAGRA/ XVAL, YVAL, ZVAL, IX(0:5), IY(0:5), IXCOEF(5), 

     .                 IYCOEF(5), IPTX, ICRITC 

       COMMON /INFO  / AXX(3), AXY(3), YSIZE, XSIZE, NPNUM, ILABEL 

       COMMON /EXCL  / KNCR(MAXCMP), IEXCL(50,MAXCMP+1), MEX, ICL, 

     .                 NRC, NRR, IAF, INF 

       COMMON /INTER/ A, B, C, XD, XE, XF, XG, G, H, XMF(10), 

     .                 XNMOL(10), P, S, TC, V, XOTHER, XNACL 

       COMMON /BASES / BASIS(MAXCMP,MAXCMP), BASE(MAXCMP,MAXPHS), 

     .                 NCE(MAXCMP), INFC(MAXCMP+1), NFC, NFLD, NFCHL 

       CHARACTER*8 NAMEA(MAXPHS) 

       CHARACTER*20 NAME(MAXPHS) 

       CHARACTER*120 TITLE 

       COMMON /CHNAME/ TITLE, NAME, NAMEA 

       CHARACTER*100  XNAME, YNAME 

       COMMON /AXNAME/ XNAME, YNAME 

       COMMON /ELDATA/ PHCMP(120), WEIGHT(120), PSYCO(120), NCCC 

       CHARACTER*8 NCOMP(120) 

       COMMON /NAMELE/ NCOMP 

       CHARACTER*4 CODE(MAXPHS) 

       COMMON /NAMCOD/ CODE 

C

       DIMENSION DATA(5) 

       CHARACTER*255 BUFFER 

       CHARACTER*130 DUMMY 

       CHARACTER*20 NDUM, CRITN(10), NDUM1 

       CHARACTER*8 NABV 

       DATA CRITN/ 'WATER               ', 'CARBON DIOXIDE      ', 

     .             'OXYGEN GAS          ', 'HYDROGEN GAS        ', 

     .             'SULFUR GAS          ', '                    ', 

     .             '                    ', '                    ', 

     .             '                    ', '                    '/ 

       DATA NCRIT/ 5/ 

C

       LONG = LEN(DUMMY) 

C

	DUMMY1=0
	
	

       NPHASE = 0 

       ILMDA  = 0 

       IDISO  = 0 

       DO 10 I = 1, 20 

           DO 5 J = 1, 9 

               THERLM(J,I) = 0.0 

    5      CONTINUE 

           DO 8 J = 1, 10 

               THERDI(J,I) = 0.0 

    8      CONTINUE 

   10  CONTINUE 

       DO 25 I = 1, MAXPHS 

            NAME(I)   = '                    ' 

            NAMEA(I)  = '        ' 

            CODE(I)   = '    ' 

            IWEIRD(I) = 0 

            NDX(I)    = 0 

            IDIS(I)   = 0 

            ACT(I)    = 0.0 

            DO 15 J = 1, 11 

                THERMO(J,I) = 0.0 

   15       CONTINUE 

            DO 18 J = 1, MAXVOL 

                VOLPAR(J,I) = 0.0 

   18       CONTINUE 

            DO 20 J = 1, 3 

                LMDA(I,J)   = 0 

   20       CONTINUE 

            DO 22 J = 1, MAXCMP 

                COMP(J,I) = 0.0 

   22       CONTINUE 

   25  CONTINUE 

C

C      READ LINES UNTIL A SECTION HEADING IS FOUND 

C

   30  READ ( 3, 1001, END=750) DUMMY

C	DUMMY1=DUMMY1+1
C	WRITE(10,*)'RECORD NUMBER=',DUMMY1
C	WRITE(10,*)DUMMY
	


       CALL IGNORE(DUMMY, ' ', 1, LONG, IFOUND)
C	WRITE(10,*)'IFOUND=',IFOUND 

       IF (IFOUND .EQ. 0)          GO TO 30 

       IF (DUMMY(IFOUND:IFOUND) .NE. '*') GO TO 30 

C

C      WHAT SECTION HEADING HAS BEEN FOUND - IF IT CAN NOT BE IDENTIFIED 

C                                            IGNORE TIL THE NEXT SECTION 

C

   40  CALL FINDC(DUMMY, '*MINERAL', 1, LONG, IFOUND) 

       IF (IFOUND .NE. 0) THEN 

           NPWS = 1 

           GO TO 100 

       ENDIF 

       CALL FINDC(DUMMY, '*GAS', 1, LONG, IFOUND) 

       IF (IFOUND .NE. 0) THEN 

           NPWS = 4 

           GO TO 100 

       ENDIF 

       CALL FINDC(DUMMY, '*AQU', 1, LONG, IFOUND) 

       IF (IFOUND .NE. 0) THEN 

C           IF (IPTX .LE. 3) GO TO 30 

           CALL FINDC(DUMMY, 'HELGESON', 1, LONG, IFOUND) 

           IF (IFOUND .NE. 0) THEN 

               NPWS = 5 

               GO TO 100 

           ENDIF 

           CALL FINDC(DUMMY, 'TANGER', 1, LONG, IFOUND) 

           IF (IFOUND .NE. 0) THEN 

               NPWS = 6 

               GO TO 100 

           ENDIF 

           NPWS = 2 

           GO TO 100 

       ENDIF 

       CALL FINDC(DUMMY, '*STOP', 1, LONG, IFOUND) 

       IF (IFOUND .NE. 0) THEN 

           GO TO 750 

       ENDIF 

       CALL FINDC(DUMMY, '*END',  1, LONG, IFOUND) 

       IF (IFOUND .NE. 0) THEN 

           GO TO 750 

       ENDIF 

C

C      IGNORE UNTIL NEXT SECTION HEADING IS READ 

C

       GO TO 30 

C

C      READ IN PHASE (MINERAL OR GAS) PROPERTIES 

C      IF A BLANK LINE, READ NEXT LINE 

C      IF FIRST NON-BLANK CHARACTER IS A *, GO TO SECTION DECODE AREA 

C      DECIDE IF IT IS IN COMPOSITIONAL SPACE OF REACTION 

C

C      READ NAME, FORMULA, ABBREVIATION (SKIP BLANK LINES) 

C                CHECK LINE FOR A '(' - IF NOT PRESENT, KEEP READING 

C                LINES TILL ONE IS FOUND (THIS IS THE WAY THAT WE KNOW 

C                THAT WE ARE AT THE START OF A NEW SET OF PHASE DATA. 

C                THIS IS ALSO USED TO SKIP PHASES NOT IN THE RIGHT 

C                COMPOSITONAL SPACE) 

C                IF A GAS, CHECK THAT WE KNOW ABOUT IT 

C

  100  READ( 3, 1001,END=750) DUMMY

	 

  110  CALL IGNORE(DUMMY, ' ', 1, LONG, IFOUND) 

       IF (IFOUND .EQ. 0) GO TO 100 

       IF (DUMMY(IFOUND:IFOUND) .EQ. '!') GO TO 100 

       IF (DUMMY(IFOUND:IFOUND) .EQ. '*') GO TO 40 

       CALL FINDC(DUMMY, '(', 1, LONG, IFOUND) 

       IF (IFOUND .EQ. 0) GO TO 100 

       IF (NPWS .EQ. 1 .OR. NPWS .EQ. 4) THEN 

           I = 1 

       ELSE IF (NPWS .EQ. 2 .OR. NPWS .EQ. 5 .OR. NPWS .EQ. 6) THEN 

           I = 0 

       ENDIF

C	WRITE(10,*)'NPWS=',NPWS
	
	 

       CALL DECODE( PHCMP, NDUM, NCCC, NCOMP, DUMMY, NABV, 120, I, IER) 

       IF (I .EQ. 0) NABV = NDUM 

       DO 130 K = 1, NCCC 

C           IF (PSYCO(K).EQ.0.0 .AND. PHCMP(K).NE.0.0) GO TO 100 

  130  CONTINUE 

       KK = 0 

       IF (NPWS .EQ. 2) KK = -1 

       IF (NPWS .EQ. 5) KK = -2 

       IF (NPWS .EQ. 6) KK = -3 

       IF (NPWS .EQ. 4) THEN 

           DO 140 I = 1, NCRIT 

               NDUM1 = NDUM 

               CALL CAP( NDUM1 ) 

               IF (NDUM1 .EQ. CRITN(I)) THEN 

                   KK = I 

                   GO TO 190 

               ENDIF 

  140      CONTINUE 

           IF (IPTX .EQ. 3) THEN 

               NDUM = XNAME 

               CALL CAP (NDUM) 

               IF (NDUM .EQ. NDUM1) THEN 

                   KK = 99 

                   GO TO 190 

               END IF 

           END IF 

           WRITE(*, 1002)  NDUM 

           GO TO 100 

       ENDIF 

C

  190  IF (IER .EQ. 1 .OR. IER .EQ. 3) WRITE( *, 1009) NDUM, DUMMY 

       IF (IER .EQ. 2 .OR. IER .EQ. 3) WRITE( *, 1010) NABV, DUMMY 

C

C	WRITE(10,*)'NPHASE=',NPHASE
	

       NPHASE = NPHASE + 1 

       IF (NPHASE .GT. MAXPHS) THEN 

           WRITE( *, 1000) NPHASE, MAXPHS 

           STOP 

       ENDIF 

       NDX(NPHASE)   = KK 

       NAME(NPHASE)  = NDUM 

       IF      (NPWS .EQ. 1 .OR. NPWS .EQ. 4 .OR. NPWS .EQ. 3) THEN 

           NAMEA(NPHASE) = NABV 

       ELSE IF (NPWS .EQ. 2 .OR. NPWS .EQ. 5 .OR. NPWS .EQ. 6) THEN 

           NAMEA(NPHASE) = NDUM 

       ENDIF 

       J = 0 

       DO 200 I = 1, NCCC 

           IF (PSYCO(I).NE.0.0) THEN 

               J = J + 1 

               COMP(J,NPHASE) = PHCMP(I) 

           ENDIF 

  200  CONTINUE 

C

C     READ NEXT LINES - IF THEY ARE BLANK OR HAVE A "(" ON THE LINE, 

C                       THEY BELONG TO THE NEXT PHASE.  OTHERWISE, 

C                       CHECK THE FIRST CHARACTER AND IGNORE OR STORE 

C                       IN APPROPIATE ARRAY 

C

  210 READ( 3, 1001,END=750) DUMMY 

      CALL IGNORE(DUMMY, ' ', 1, LONG, IFOUND) 

      IF (IFOUND .EQ. 0) GO TO 100 

      IF (DUMMY(IFOUND:IFOUND) .EQ. '!') GO TO 210 

      J = IFOUND 

      CALL FINDC(DUMMY, '(', 1, LONG, IFOUND) 

      IF (IFOUND .NE. 0) GO TO 110 

      IFOUND = J 

      J = IFOUND + 1 

      IF (DUMMY(IFOUND:IFOUND) .EQ. '*') THEN 

          GO TO 40 

       ELSE IF ((DUMMY(IFOUND:IFOUND) .EQ. 'S' .OR. 

     .           DUMMY(IFOUND:IFOUND) .EQ. 's')      .AND. 

     .          (DUMMY(J:J) .EQ. 'T' .OR. DUMMY(J:J) .EQ. 't') ) THEN

          WRITE (BUFFER, '(A)' ) DUMMY 

          READ ( BUFFER, 1003) (DATA(I),I=1,4) 

          IF (DATA(2) .GT. 999999. .AND. ISWIT1 .EQ. 0) THEN 

              IF (NPHASE .GT. 0) THEN 

                  IF (LMDA(NPHASE,1) .NE. 0) ILMDA = ILMDA - 1 

                  IF (LMDA(NPHASE,2) .NE. 0) ILMDA = ILMDA - 1 

                  IF (LMDA(NPHASE,3) .NE. 0) ILMDA = ILMDA - 1 

                  IF (IDIS(NPHASE)   .NE. 0) IDISO = IDISO - 1 

                  LMDA(NPHASE,1) = 0 

                  LMDA(NPHASE,2) = 0 

                  LMDA(NPHASE,3) = 0 

                  IDIS(NPHASE)   = 0 

                  DO 250 I = 1, 11 

                      THERMO(I,NPHASE) = 0.0 

  250             CONTINUE 

                  NPHASE = NPHASE - 1 

              ENDIF 

              GO TO 100 

          ENDIF 

          IF (DABS(DATA(2)) .LT. 0.00001 .AND. ISWIT1 .EQ. 0) 

     .                                        WRITE (*, 1004) NDUM 

          THERMO(11,NPHASE) = DATA(1) 

          THERMO( 1,NPHASE) = DATA(2) 

          THERMO( 2,NPHASE) = DATA(3) 

          THERMO( 6,NPHASE) = DATA(4) 

      ELSE IF (DUMMY(IFOUND:IFOUND) .EQ. 'C' .OR. 

     .         DUMMY(IFOUND:IFOUND) .EQ. 'c' ) THEN 

          WRITE(BUFFER,'(A)') DUMMY 

          READ (BUFFER, 1003)    (DATA(I),I=1,5) 

          IF (DUMMY(J:J) .EQ. '1' .AND. NDX(NPHASE) .GE. 0) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 3, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 7, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO( 5, NPHASE) = DATA(3) 

              IF (DATA(4) .NE. 0.0) THERMO(10, NPHASE) = DATA(4) 

          ELSE IF (DUMMY(J:J) .EQ. '2' .AND. NDX(NPHASE) .GE. 0) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 8, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 4, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO( 9, NPHASE) = DATA(3) 

          ELSE IF (DUMMY(J:J) .EQ. '3' .AND. NDX(NPHASE) .GE. 0) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 3, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 4, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO( 5, NPHASE) = DATA(3) 

          ELSE IF (DUMMY(J:J) .EQ. '6' .AND. NDX(NPHASE) .EQ. -2) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 4, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 5, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO( 3, NPHASE) = DATA(3) 

              IF (DATA(4) .NE. 0.0) THERMO( 7, NPHASE) = DATA(4) 

     .                                                   * 1.0D05 

          ELSE IF (DUMMY(J:J) .EQ. '7' .AND. NDX(NPHASE) .EQ. -3) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 4, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 5, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO( 3, NPHASE) = DATA(3) 

              IF (DATA(3) .EQ. 0.0) THERMO( 3, NPHASE) = 228.0 

              IF (DATA(4) .NE. 0.0) THERMO( 7, NPHASE) = DATA(4) 

     .                                                   * 1.0D05 

              IF (DATA(5) .NE. 0.0) THERMO( 8, NPHASE) = DATA(5) 

              IF (DATA(5) .EQ. 0.0) THERMO( 8, NPHASE) = 2600.0 

          ELSE IF (DUMMY(J:J) .EQ. '8' .AND. NDX(NPHASE) .EQ. -1) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 3, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 4, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO( 7, NPHASE) = DATA(3) 

              IF (DATA(4) .NE. 0.0) THERMO( 8, NPHASE) = DATA(4) 

          ELSE IF (DUMMY(J:J) .EQ. '9' .AND. NDX(NPHASE) .EQ. -1) THEN 

              IF (DATA(1) .NE. 0.0) THERMO( 5, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) THERMO( 9, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) THERMO(10, NPHASE) = DATA(3) 

          ENDIF 

      ELSE IF (DUMMY(IFOUND:IFOUND) .EQ. 'V'  .OR. 

     .         DUMMY(IFOUND:IFOUND) .EQ. 'v') THEN 

          WRITE(BUFFER,'(A)') DUMMY 

          READ (BUFFER, 1003)    (DATA(I),I=1,5) 

          IF (DUMMY(J:J) .EQ. '1') THEN 

              VOLPAR( 1, NPHASE) = DATA(1) 

              VOLPAR( 2, NPHASE) = DATA(2) 

              VOLPAR( 3, NPHASE) = DATA(3) 

              VOLPAR( 4, NPHASE) = DATA(4) 

              VOLPAR( 5, NPHASE) = DATA(5) 

          ELSE IF (DUMMY(J:J) .EQ. '6' .AND. NDX(NPHASE) .EQ. -2) THEN 

               VOLPAR( 1, NPHASE) = DATA(1) 

               VOLPAR( 2, NPHASE) = DATA(2) * 1.0D-05 

               VOLPAR( 3, NPHASE) = DATA(3) * 1.0D-02 

               VOLPAR( 4, NPHASE) = DATA(4) * 1.0D-05 

          ELSE IF (DUMMY(J:J) .EQ. '7' .AND. NDX(NPHASE) .EQ. -3) THEN 

              VOLPAR( 1, NPHASE) = DATA(1) 

              VOLPAR( 2, NPHASE) = DATA(2) * 1.0D-05 

              VOLPAR( 3, NPHASE) = DATA(3) * 1.0D-02 

              VOLPAR( 4, NPHASE) = DATA(4) * 1.0D-05 

           ELSE IF (DUMMY(J:J) .EQ. '8' .AND. NDX(NPHASE) .EQ. -1) THEN 

              IF (DATA(1) .NE. 0.0) VOLPAR( 1, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) VOLPAR( 2, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) VOLPAR( 4, NPHASE) = DATA(3) 

              IF (DATA(4) .NE. 0.0) VOLPAR( 5, NPHASE) = DATA(4) 

          ELSE IF (DUMMY(J:J) .EQ. '9' .AND. NDX(NPHASE) .EQ. -1) THEN 

              IF (DATA(1) .NE. 0.0) VOLPAR( 3, NPHASE) = DATA(1) 

              IF (DATA(2) .NE. 0.0) VOLPAR( 6, NPHASE) = DATA(2) 

              IF (DATA(3) .NE. 0.0) VOLPAR( 7, NPHASE) = DATA(3) 

          END IF


       ELSE IF ((DUMMY(IFOUND:IFOUND) .EQ. 'T' .OR. 

     .          DUMMY(IFOUND:IFOUND) .EQ. 't') .AND. 

     .          NDX(NPHASE)          .EQ.  0 ) THEN 

          WRITE(BUFFER,'(A)') DUMMY 

          READ (BUFFER, 1003)    (DATA(I),I=1,5) 

          IF (DUMMY(J:J) .EQ. '1' .OR. DUMMY(J:J) .EQ. '3' .OR. 

     .        DUMMY(J:J) .EQ. '5' ) THEN 

              IF (DUMMY(J:J) .EQ. '1') THEN 

                  I = LMDA(NPHASE,1) 

                  IF (I .EQ. 0) THEN 

                      ILMDA          = ILMDA + 1 

                      I              = ILMDA 

                      LMDA(NPHASE,1) = I 

                  ENDIF 

              ELSE IF (DUMMY(J:J) .EQ. '3') THEN 

                  I = LMDA(NPHASE,2) 

                  IF (I .EQ. 0) THEN 

                      ILMDA          = ILMDA + 1 

                      I              = ILMDA 

                      LMDA(NPHASE,2) = I 

                  ENDIF 

              ELSE IF (DUMMY(J:J) .EQ. '5') THEN 

                  I = LMDA(NPHASE,3) 

                  IF (I .EQ. 0) THEN 

                      ILMDA          = ILMDA + 1 

                      I              = ILMDA 

                      LMDA(NPHASE,3) = I 

                  ENDIF 

              ENDIF 

              THERLM( 3, I) = DATA(1) 

              THERLM( 7, I) = DATA(2) 

              THERLM( 1, I) = DATA(3) 

              THERLM( 2, I) = DATA(4) 

              THERLM( 8, I) = DATA(5) 

          ELSE IF (DUMMY(J:J) .EQ. '2' .OR. DUMMY(J:J) .EQ. '4' .OR. 

     .             DUMMY(J:J) .EQ. '6' ) THEN 

              IF (DUMMY(J:J) .EQ. '2') THEN 

                  I = LMDA(NPHASE,1) 

                  IF (I .EQ. 0) THEN 

                      ILMDA          = ILMDA + 1 

                      I              = ILMDA 

                      LMDA(NPHASE,1) = I 

                  ENDIF 

              ELSE IF (DUMMY(J:J) .EQ. '4') THEN 

                  I = LMDA(NPHASE,2) 

                  IF (I .EQ. 0) THEN 

                      ILMDA          = ILMDA + 1 

                      I              = ILMDA 

                      LMDA(NPHASE,2) = I 

                  ENDIF 

              ELSE IF (DUMMY(J:J) .EQ. '6') THEN 

                  I = LMDA(NPHASE,3) 

                  IF (I .EQ. 0) THEN 

                      ILMDA          = ILMDA + 1 

                      I              = ILMDA 

                      LMDA(NPHASE,3) = I 

                  ENDIF 

              ENDIF 

              THERLM( 4, I) = DATA(1) 

              THERLM( 9, I) = DATA(2) 

              THERLM( 5, I) = DATA(3) 

              THERLM( 6, I) = DATA(4) 

          ENDIF 

       ELSE IF ((DUMMY(IFOUND:IFOUND) .EQ. 'D' .OR. 

     .           DUMMY(IFOUND:IFOUND) .EQ. 'd') .AND. 

     .           NDX(NPHASE)          .EQ.  0 ) THEN 

          WRITE(BUFFER,'(A)') DUMMY 

          READ (BUFFER, 1003)    (DATA(I),I=1,5) 

          I = IDIS(NPHASE) 

          IF (I .EQ. 0) THEN 

              IDISO        = IDISO + 1 

              I            = IDISO 

              IDIS(NPHASE) = I 

          ENDIF 

          IF (DUMMY(J:J) .EQ. '1') THEN 

              THERDI( 1, I) = DATA(1) 

              THERDI( 2, I) = DATA(2) 

              THERDI( 3, I) = DATA(3) 

              THERDI( 5, I) = DATA(5) 

          ELSE IF (DUMMY(J:J) .EQ. '2') THEN 

              THERDI( 6, I) = DATA(1) 

              THERDI( 7, I) = DATA(2) 

              THERDI( 8, I) = DATA(3) 

              THERDI( 9, I) = DATA(4) 

              THERDI(10, I) = DATA(5) 

          ENDIF 

      ELSE IF ((DUMMY(IFOUND:IFOUND) .EQ. 'A'   .OR. 

     .          DUMMY(IFOUND:IFOUND) .EQ. 'a')  .AND. 

     .           NDX(NPHASE)          .GE.  0 ) THEN 

          ISTART = IFOUND 

          CALL FINDC(DUMMY, ' ',  ISTART, LONG, IFOUND) 

          IF (IFOUND .NE. 0) THEN 

              ISTART = IFOUND 

              CALL IGNORE(DUMMY, ' ', ISTART, LONG, IFOUND) 

          ENDIF 

          IF (IFOUND .NE. 0) THEN 

              CODE(NPHASE) = DUMMY(IFOUND:IFOUND+3) 

              IWEIRD(NPHASE) = 1 

          ENDIF 

      ENDIF 

      GO TO 210 

C

C     DATA BASE HAS BEEN READ 

C

  750  IF (NPHASE .LT. 1) THEN
C            WRITE(10,  1005) 

           WRITE (*, 1005) 

           STOP 

       ENDIF 

C

C      Modify Volume terms for solids 

C

       DO 760 I = 1, NPHASE 

           IF (NDX(I) .EQ. 0) THEN 

               VOLPAR( 1, I) = VOLPAR( 1, I) * THERMO(6, I) / 1.0D05 

               VOLPAR( 2, I) = VOLPAR( 2, I) * THERMO(6, I) / 1.0D05 

               VOLPAR( 3, I) = VOLPAR( 3, I) * THERMO(6, I) / 1.0D05 

               VOLPAR( 4, I) = VOLPAR( 4, I) * THERMO(6, I) / 1.0D08
 

           ENDIF 

  760  CONTINUE 

C

C      EXCLUDE ELEMENTS NOT USED 

C

       J = 0 

       DO 770 I = 1, NCCC 

           IF (PSYCO(I) .NE. 0.0) THEN 

               J = J + 1 

               IF (J .NE. I) THEN 

                   WEIGHT(J) = WEIGHT(I) 

                   NCOMP(J)  = NCOMP(I) 

               ENDIF 

           ENDIF 

  770  CONTINUE 

       NCCC = NCTZ 

C

C      IF WATER IS PRESENT AND HAAR EQUATION OF STATE IS USED, SET 

C         ALL WATER PROPERTIES TO ZERO 

C

       IF (ICHH2O .EQ. 1) THEN 

           DO 820 I = 1, NPHASE 

               IF (NAME(I)(1:6) .EQ. 'WATER ') THEN 

                   DO 810 J = 1, 11 

                       THERMO(J,I) = 0.0 

  810              CONTINUE 

                   GO TO 900 

               ENDIF 

  820      CONTINUE 

       ENDIF 


  900  RETURN 

C

 1000  FORMAT(//,' ********************************************',/, 

     .           ' As a result of your input,',I4,' phases have',/, 

     .           ' been extracted from the data base BUT the   ',/, 

     .           ' program is dimensioned for a MAXIMUM of', I4 ,/, 

     .           ' Choose a simplier system or recompile the   ',/, 

     .           ' program after changing the MAXPHS parameter',/, 

     .           ' *******************************************',//) 

 1001  FORMAT(A) 

 1002  FORMAT(//,' ',A20,' is a gas and its critical properties ', 

     .         /,' could not be found - It will be ignored.') 

 1003  FORMAT(5X,5F15.5) 

 1004  FORMAT(' Phase ',A20,' has an delta H of zero in the data bank.') 

 1005  FORMAT(//,' You have eliminated too many phases - There ',/, 

     .          ' are none left.  Try again.') 

 1009  FORMAT(' NAME truncated to: ',A,'.  Line in question', 

     .        ' is:', /, ' ',A) 

 1010  FORMAT(' ABREVIATION truncated to: ',A,'.  Line in question', 

     .        ' is:', /, ' ',A) 

       END

        SUBROUTINE NUMB1(ARRAY, IDIM, NUM, IBEGIN, IEND, IERR, INPUT)

       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C

C       GIVEN:  Character array INPUT

C

C       Starting at position IBEGIN, ending at position IEND inclusive,

C                    find up to NUM real numbers in INPUT

C

C       RESULTS:  NUM real numbers returned in ARRAY.

C

C       Notes:  If there is not NUM real numbers available, return

C                    0.0 for any missing.

C               If two comma's in are in a row, or two commas are

C                    seperated only by blanks, return 0.0

C               If IEND is greater than the dimension of INPUT, the

C                    dimension of INPUT is used

C               Numbers seperated by blank(s), comma, blank(s) are

C                    treated the same as those seperated by only

C                    blank(s)

C

C       ************This routine is similar to using full format free I/O

C                   but has the advantage that a blank line is read

C                   as zeros.  It does NOT keep expecting more numbers

C

C       Programmed 6/May/86        E.H. Perkins

C

        CHARACTER*(*) INPUT

        CHARACTER*1  LAST

        CHARACTER*20 BLANK

        CHARACTER*80 BUFFER

        DIMENSION ARRAY(IDIM)

C

        BLANK  = '                    '

        LAST   = ','

        ISTART = IBEGIN

        IPOINT = 0

        IERR   = 0

        DO 10 I = 1, IDIM

            ARRAY(I) = 0.0

   10   CONTINUE

C

        IFIN = MIN0(IEND, (LEN(INPUT)) )

C

  100   IF (ISTART .GE. IFIN) RETURN

        CALL IGNORE(INPUT, ' ', ISTART, IFIN, IFOUND)

        IF (IFOUND .EQ. 0) RETURN

        IF (INPUT(IFOUND:IFOUND) .EQ. ',') THEN

            IF (LAST .EQ. ' ') THEN

                ISTART = IFOUND + 1

                LAST   = ','

            ELSE

                IPOINT = IPOINT + 1

                ARRAY(IPOINT) = 0.0

                ISTART = IFOUND + 1

                LAST   = ','

                IF (IPOINT .GE. NUM) RETURN

            ENDIF

        ELSE

            IPOINT = IPOINT + 1

            ISTART = IFOUND

            CALL FINDC(INPUT, ',', ISTART, IFIN, IFOUND)

            J = IFOUND

            IF (J .EQ. 0 .OR. J .GT. IFIN) J = IFIN + 1

            CALL FINDC(INPUT, ' ', ISTART, IFIN, IFOUND)

            IF (IFOUND .EQ. 0 .OR. IFOUND .GT. IFIN) IFOUND = IFIN + 1

            IF (J .LT. IFOUND) THEN

                IFOUND = J

                LAST = ','

            ELSE

                LAST = ' '

            ENDIF

            IF ((IFOUND - ISTART) .GE. 14) THEN

                WRITE(BUFFER, '(A)', ERR=999) INPUT(ISTART:IFOUND)

            ELSE

                J = 14 - (IFOUND - ISTART)

                CALL FINDC(INPUT, '.', ISTART, IFOUND, K)

                IF (K .LT. ISTART .OR. K .GT. IFOUND) J = J - 1

                K = IFOUND - 1

                WRITE(BUFFER, '(A)', ERR=999)

     .                              BLANK(1:J)//INPUT(ISTART:K)//'.'

            ENDIF

            READ (BUFFER, '(F15.0)', ERR=999, END=999) ARRAY(IPOINT)

            ISTART = IFOUND + 1

            IF (IPOINT .GE. NUM) RETURN

        ENDIF

        GO TO 100

C

  999   IERR = 1

        RETURN

        END

        SUBROUTINE IGNORE( L1, L2, ISTART, IFIN, IFOUND)

C

C       This routine searchs for the first occurance (starting

C       at ISTART and ending at IFIN) of the character string

C       in the array L1 that does not match the character string L2.

C       If a string that does not match is found, then IFOUND equals

C            the position of that string.

C       If the string is a perfect match, IFOUND equals 0

C

        CHARACTER*(*) L1

        CHARACTER*(*) L2

C

        L2DIM = LEN(L2)

        IEND = MIN0((LEN(L1)), IFIN) - L2DIM + 1

C

        DO 20 I = ISTART, IEND, L2DIM

            DO 10 J = 1, L2DIM

                II = I + J - 1

                IF (L1(II:II) .NE. L2(J:J)) THEN

                    IFOUND = I

                    RETURN

                ENDIF

   10       CONTINUE

   20   CONTINUE

        IFOUND = 0

        RETURN

        END

        SUBROUTINE FINDC(L1, L2, ISTART, IFIN, IFOUND)

C

C       This routine searchs the character array L1 (starting

C       at location ISTART, ending at location IFIN) for the string

C       stored in the character array L2.  The string in L2 is assumed

C       to have the same length as the dimension of L2.

C       If found, IFOUND is set the starting position of the string,

C       if not, IFOUND is set to 0

C

        CHARACTER*(*) L1

        CHARACTER*(*) L2

C

C       L2DIM = LEN(L2)

        IEND = MIN0((LEN(L1)), IFIN)

C

        IFOUND = INDEX1(L1(ISTART:IEND), L2)

        IF (IFOUND .GT. 0 .AND. IFOUND .LE. IEND) IFOUND = IFOUND

     .                                                   + ISTART - 1

        RETURN

        END

        SUBROUTINE CAP( STRING )

C

C       This routine capitalizes all characters in string.

C       String must be a character variable

C

        CHARACTER*(*) STRING

C

        CHARACTER*26 UPPER, LOWER

C

        DATA UPPER /'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/

        DATA LOWER /'abcdefghijklmnopqrstuvwxyz'/

C

C       Get and check string lengths

C

        LEN1 = LEN(STRING)

C

C       Convert lower to upper case

C

        DO 50 I = 1, LEN1

            J = INDEX(LOWER, STRING(I:I))

            IF (J .GT. 0) STRING(I:I) = UPPER(J:J)

   50   CONTINUE

        RETURN

        END


       SUBROUTINE PREAD1

       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C

C      THIS ROUTINE READS PART ONE OF THE DATA BASE - THE ELEMENT DATA

C

       COMMON /ELDATA/ PHCMP(120), WEIGHT(120), PSYCO(120), NCCC

       CHARACTER*8 NCOMP(120)

       COMMON /NAMELE/ NCOMP

C

       CHARACTER*100 DUMMY

C

C      READ THE NUMBER OF ELEMENTS, THEIR SYMBOLS AND WEIGHTS

C

       READ (3,1001) NCCC

       I = 0

       DO 30 J = 1, (1 + ((NCCC - 1)/7))

           READ (3,1002) DUMMY

           ISTART = 1

           KK     = NCCC - (7 * (J - 1))

           IF (KK .GT. 7) KK = 7

           DO 20 K = 1, KK

               CALL IGNORE(DUMMY, ' ', ISTART, 100, IFOUND)

               IF (IFOUND .GT. 0) THEN

                   ISTART = IFOUND

                   CALL FINDC (DUMMY, ' ', ISTART, 100, IFOUND)

                   IF (IFOUND .GT. 0) THEN

                       I = I + 1

                       NCOMP(I) = DUMMY(ISTART:IFOUND)//'        '

                       ISTART = IFOUND

                   ENDIF

               ENDIF

   20      CONTINUE

   30  CONTINUE

C

       READ (3,1003) (WEIGHT(I), I = 1, NCCC)

C
        RETURN

 1001  FORMAT(I4)

 1002  FORMAT(A100)

 1003  FORMAT(7F10.5)

       END

       SUBROUTINE DECODE(PHCMP, NAME, NCTZ, NCOMP, LDUMMY, NABV,

     .                   NDIM, ISWT, IER)

       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C

C     THE ELEMENTAL SYMBOLS ARE PASSED IN NCOMP (CHARACTER*4 FORMAT)

C     THE NUMBER OF ELEMENTS IS NCTZ

C     LDUMMY OPTIONALLY CONTAINING ONE OR MORE OF THE FOLLOWING:

C                (A)     A MINERAL NAME,     3 BLANKS (OR MORE),

C                (B)     A CHEMICAL FORMULA, 3 BLANKS (OR MORE),

C                (C)     AN ABREVIATION,     3 BLANKS (OR MORE),

C                (D)     MISS. INFORMATION

C             (D) IS ALWAYS IGNORED.

C

C     ISWT CONTAINS A NUMBER .GE. -2 .AND. .LE. 4

C               IF ISWT = -2, LDUMMY CONTAINS  (A)

C               "    "     -1,   "       "     (A), (C)

C               "    "      0,   "       "     (A), (B)

C               "    "      1,   "       "     (A), (B), (C)

C               "    "      2,   "       "     (B), (C)

C               "    "      3,   "       "     (B)

C               "    "      4,   "       "     (C)

C

C     THE MINERAL NAME IS RETURNED IN NAME (IN A20 FORMAT)

C     THE FORMULA IS RETURNED IN PHCMP, IN ORDER SPECIFIED BY SYMBOLS

C         IN NCOMP.  DIMENSIONED NDIM

C     THE ABREVIATION IS RETURNED IN NABV (IN A8 FORMAT)

C

C     THREE BLANKS (   ) ARE USED AS DELIMINATORS BETWEEN FIELDS

C

C     SINGLE BLANKS ARE IGNORED

C

        DIMENSION PHCMP(NDIM)

        CHARACTER*(*) LDUMMY

        CHARACTER*20 NAME

        CHARACTER*8 NABV, NCOMP(NDIM)

C

        IER = 0

C

C       ERROR OUTPUT UNIT

C

        IOUT = 45
C  Changes made by Vera

	OPEN(UNIT=IOUT,FILE='decode.error',STATUS='UNKNOWN')
C	

C

        NABV = '        '

        NAME = '                    '

        DO 5 I = 1, NDIM

             PHCMP(I) = 0.0E00

    5   CONTINUE

        ISTART = 1

        IFOUND = 1

        IFIN   = LEN(LDUMMY)

C

C    CHECK IF NAME IS PRESENT

C

        IF (ISWT .GE. 2) GO TO 100

C

C    FIND FIRST NON BLANK CHARACTER, AND THEN 3 BLANKS -- NAME IS

C                  BETWEEN THESE TWO STRINGS

C

        CALL IGNORE(LDUMMY, ' ', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1001) LDUMMY

            WRITE(IOUT,1002)

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        END IF

        ISTART = IFOUND

        CALL FINDC(LDUMMY, '   ', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1001) LDUMMY

            WRITE(IOUT,1003)

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        END IF

        INUM = IFOUND - ISTART

        IF (INUM .GT. 20) THEN

C           WRITE(IOUT, 1008) LDUMMY

            IER  = IER + 1

            INUM = 20

        ENDIF

        NAME = LDUMMY(ISTART:(ISTART + INUM - 1))//'          '

     .                                           //'          '

C

C    TEST IF FORMULA COMES NEXT

C

  100   IF (ISWT .EQ. -2) RETURN

        IF (ISWT .EQ. -1) GO TO 200

        IF (ISWT .EQ.  4) GO TO 200

C

C    FIND START OF FORMULA, THEN 3 BLANKS.  IN BETWEEN IS FORMULA,

C                  DECODE WITH ELSORT

C

        ISTART = IFOUND

        CALL IGNORE(LDUMMY, ' ', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1001) LDUMMY

            WRITE(IOUT,1004)

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'


            STOP

        END IF

        ISTART = IFOUND

        CALL FINDC(LDUMMY, '   ', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1001) LDUMMY

            WRITE(IOUT,1005)

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        ELSE IF (INUM .GT. 80) THEN

            WRITE(IOUT, 1009) LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        ELSE

            CALL ELSORT(PHCMP, LDUMMY(ISTART:IFOUND), NCOMP,

     .                  NDIM, NCTZ, IOUT, -1)

        ENDIF

C

C    TEST IF ABREVIATION COMES NEXT

C

  200   IF (ISWT .EQ.  0) RETURN

        IF (ISWT .EQ.  3) RETURN

C

C    FIND START OF ABREVIATION, THEN 3 BLANKS.  ABV IS BETWEEN

C

        ISTART = IFOUND

        CALL IGNORE(LDUMMY, ' ', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1001) LDUMMY

            WRITE(IOUT,1006)

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        END IF

        ISTART = IFOUND

        CALL FINDC(LDUMMY, '   ', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1001) LDUMMY

            WRITE(IOUT,1007)

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        END IF

        IF ((IFOUND - ISTART) .GT. 8) THEN

            IFOUND = ISTART + 7

C           WRITE(IOUT, 1010) LDUMMY(ISTART:IFOUND), LDUMMY

            IER = IER + 2

        ENDIF

        IFOUND = IFOUND - 1

        NABV = LDUMMY(ISTART:IFOUND)//'        '

        RETURN

C

 1001   FORMAT(' ERROR in thermodynamic data bank.  Line in ERROR is',/,

     .             ' ', A)

 1002   FORMAT(' In particular, the entire line is blank thus there',

     .         ' is no phase/species NAME.')

 1003   FORMAT(' In particular, the NAME is too long or 3 blanks',

     .         ' were not found as a terminator.')

 1004   FORMAT(' In particular, no FORMULA was found.')

 1005   FORMAT(' In particular, the FORMULA is too long or 3 blanks',

     .         ' were not found as a terminator.')

 1006   FORMAT(' In particular, no ABREVIATION was found.')

 1007   FORMAT(' In particular, the ABREVIATION is too long or 3',

     .         ' blanks were not found as a terminator.')

 1008   FORMAT(' Truncation of NAME to 20 characters. ',

     .         ' Line in question is: ',/,

     .          ' ',A)

 1009   FORMAT(' FORMULA truncated - please correct data base ',/,

     .         ' and rerun.  Line in question is: ',/,

     .         ' ',A)

 1010   FORMAT(' ABREVIATION truncated to: ',A,'.  Line in question',

     .         ' is:', /, ' ',A)

        END

        FUNCTION INDEX1( STR1, STR2)

C

C       This routine looks for STR2 in STR1.  It checks first

C       exactly as passed - then converts everything to uppercase

C       and checks again.  The passed string lengths should be

C       less than 81 characters long.

C

C       It returns the first postion of STR2 or 0 if not found

C

        CHARACTER*(*) STR1

        CHARACTER*(*) STR2

C

        CHARACTER*26 UPPER, LOWER

        CHARACTER*201 STRI1, STRI2

C

        DATA UPPER /'ABCDEFGHIJKLMNOPQRSTUVWXYZ'/

        DATA LOWER /'abcdefghijklmnopqrstuvwxyz'/

C

C       Check as is first for speed

C

        INDEX1 = INDEX(STR1, STR2)

        IF (INDEX1 .GT. 0) RETURN

C

C       Get and check string lengths

C

        LEN1 = LEN(STR1)

        LEN2 = LEN(STR2)

        IF (LEN1 .GT. 201 .OR. LEN2 .GT. 201) THEN

            WRITE(*, 1001)

            STOP

        ENDIF

C

C       Convert lower to upper case

C

        DO 50 I = 1, LEN1

            J = INDEX(LOWER, STR1(I:I))

            IF (J .EQ. 0) THEN

                STRI1(I:I) = STR1(I:I)

            ELSE

                STRI1(I:I) = UPPER(J:J)

            ENDIF

   50   CONTINUE

        DO 60 I = 1, LEN2

            J = INDEX(LOWER, STR2(I:I))

            IF (J .EQ. 0) THEN

                STRI2(I:I) = STR2(I:I)

            ELSE

                STRI2(I:I) = UPPER(J:J)

            ENDIF

   60   CONTINUE

C

C       Now compare again

C

        INDEX1 = INDEX(STRI1(1:LEN1), STRI2(1:LEN2))

        RETURN

 1001   FORMAT(/,' ******ERROR in INDEX1.  Passed arrays must be less',

     .         /,'       81 characters long.  These are: ',I2,' and ',

     .         /,'       ',I2,' characters long.')

        END

        SUBROUTINE ELSORT( COMP, LDUMMY, ELNAME, NCHEM, NUMEL,

     .                     IOUT, ISWT)

       IMPLICIT DOUBLE PRECISION(A-H,O-Z)

C

C       THIS ROUTINE DECODES A FORMULA WHICH HAS THE GENERAL FORM

C

C                         CA(3)AL(2)SI(3)O(12)

C

C    SEE NOTES ON FORMAT BELOW

C

C    INPUT:  LDUMMY - CONTAINS THE FORMULA (SEE NOTES ON FORMAT BELOW)

C            LEN    - DIMENSION OF LDUMMY

C            ELNAME - ELEMENTAL SYMBOLS

C            NCHEM  - DIMENSION OF COMP AND ELNAME

C            NUMEL  - NUMBER OF ELEMENTAL SYMBOLS IN ELNAME

C            ISWT   - IF ISWT .GE. 0 AND AN UNKNOWN ELEMENT IF FOUND

C                                    (SYMBOL NOT IN ELNAME), ADDS SYMBOL

C                                    TO ELNAME AND ADDS 1 TO NUMEL

C                   - IF ISWT .LT. 0 AND AN UNKNOWN ELEMENT IF FOUND

C                                    (SYMBOL NOT IN ELNAME), AN ERROR

C                                    MESSAGE IS PRINTED AND PROGRAM

C                                    STOPS

C            IOUT   - UNIT TO WHICH ERROR MESSAGES ARE WRITTEN

C

C    OUTPUT: COMP   - CONTAINS THE COEFFICIENTS FOR EACH OF THE ELEMENTS

C                   - ZERO IF AN ELEMENT IS NOT PRESENT

C                   - ORDERED BY THE ORDER OF THE ATOMIC SYMBOLS IN

C                     ELNAME

C

C

C    NOTES ON FORMAT OF THE FORMULA:

C

C       ELEMENTAL SYMBOLS MUST BE SEPARATED BY THE SEQUENCE: LEFT

C                         DELIMINATOR, COEFFICIENT, RIGHT DELIMINATOR

C       THE LEFT PARENTHESIS '(' IS THE LEFT DELIMINATOR OF A

C                         COEFFICIENT

C       THE RIGHT PARENTHESIS ')' IS THE RIGHT DELIMINATOR OF A

C                         COEFFICIENT

C       DELIMINATORS MUST BE PAIRED

C       THE COEFFICIENTS CAN BE EITHER INTEGERS OR REAL NUMBERS

C       ELEMENTAL SYMBOLS/COEFFICIENTS CAN BE REPEATED AS MANY TIMES AS

C                 NEED BE.  CA(1)CA(3)CA(5.0)..... IS PREFECTLY LEGAL

C

C       REPEAT GROUPS CAN ALSO BE USED.  THE DELIMINATORS ARE [ AND ],

C              RESPECTIVELY LEFT AND RIGHT, AND MUST BE PAIRED.  THEY

C              MUST BE FOLLOWED BY ( COEFF ) SEQUENCE.

C

        DIMENSION COMP(NCHEM)

        CHARACTER*(*) LDUMMY

        CHARACTER*20 BUFFER, BLANK

        CHARACTER*8 ELNAME(NCHEM), NAME1

C

        DATA BLANK/'                    '/

C

        DO 1 I = 1, NCHEM

            COMP(I) = 0.0

    1   CONTINUE

        IFIN   = 0

        IREPL  = 0

        IREPR  = 0

        IREPN  = 0

        IREPN2 = 0

        REPEAT = 0.0

        LENGTH = LEN(LDUMMY)

C

   10  ISTART = IFIN + 1

        IFIN = LENGTH

        IF (ISTART .EQ. IREPL) ISTART = ISTART + 1

        IF (ISTART .EQ. IREPR) ISTART = IREPN  + 1

        IF (ISTART .GE. LENGTH) RETURN

C

C    LOOK FOR [  IF PRESENT, REPEAT GROUP EXISTS.

C       ONLY LOOK IF A REPEAT GROUP IS NOT CURRENTLY ACTIVE

C

        IF (ISTART .GE. IREPL .AND. ISTART .GE. IREPR) THEN

            CALL FINDC(LDUMMY, '[', ISTART, IFIN, IFOUND)

            IF (IFOUND .LE. 0) THEN

               IREPL  = 0

               IREPR  = LENGTH + 1

               IREPN  = LENGTH + 1

               REPEAT = 1.0

            ELSE

               IREPL = IFOUND

               CALL FINDC(LDUMMY, ']', IREPL, IFIN, IFOUND)

               IF (IFOUND .GT. 0) THEN

                  IREPR = IFOUND

                  IREPN = IFOUND + 1

               ELSE

                   WRITE(IOUT,1400) LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


                   STOP

               ENDIF

C

C              NOW DECODE REPEAT COEFFICIENT

C

               IF (LDUMMY(IREPN:IREPN) .NE. '(' ) THEN

                   WRITE(IOUT,1410) LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


                   STOP

               ENDIF

               CALL FINDC(LDUMMY, ')', IREPN, IFIN, IFOUND)

               IF (IFOUND.EQ.0) THEN

                   WRITE(IOUT,1100) LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


                   STOP

               END IF

C

               IREPN2 = IFOUND - 1

               IREPN  = IREPN + 1

               CALL FINDC(LDUMMY, '.', IREPN, IREPN2, IFOUND)

               IF (IFOUND.NE.0) THEN

                  WRITE(BUFFER,'(A20)') LDUMMY(IREPN:IREPN2)

                  READ(BUFFER,'(7X,F12.3)',ERR=100) REPEAT

               ELSE

                  I = 6 - (IREPN2 - IREPN)

                  IF (I .NE. 0) WRITE(BUFFER,'(A20)')

     .                              BLANK(1:I)//LDUMMY(IREPN:IREPN2)

                  IF(I .EQ. 0) WRITE(BUFFER,'(A20)')

     .                                          LDUMMY(IREPN:IREPN2)

                  READ(BUFFER,'(I20)',ERR=100) I

                  REPEAT = FLOAT(I)

               ENDIF

               IREPN = IREPN2 + 1

            ENDIF

        ENDIF

        IFIN = LENGTH

C

C

C    FIND A "(".  VALUE BETWEEN THE 0'TH POSITION OR THE LAST ")" AND

C      THIS "(" IS THE SYMBOL OF AN ELEMENT.

C

        CALL FINDC(LDUMMY, '(', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) RETURN

C

        I = IFOUND - 1

        NAME1 = LDUMMY(ISTART:I)//BLANK

        CALL CAP( NAME1 )

C

C    IF IT IS A NEW ELEMENT, STORE NAME AND OBTAIN INDEX.  IF IT

C      HAS ALREAD BEEN INPUT, GET INDEX.

C

        IF (NUMEL.GT.0) THEN

            DO 20 I=1, NUMEL

                INDX = I

                IF (ELNAME(I) .EQ. NAME1) GO TO 30

   20       CONTINUE

        END IF

C

        IF (ISWT .LT. 0) THEN

              WRITE(IOUT,1200) NAME1, LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


              STOP

        END IF

        NUMEL = NUMEL + 1

        INDX  = NUMEL

        ELNAME(NUMEL) = NAME1

C

C    FIND THE NEXT ")".  CHARACTERS BETWEEN LAST "(" AND THIS ")"

C      CORRESPOND TO THE NUMBER OF MOLES THE THE ELEMENT JUST READ

C      IN.  THIS ELEMENT HAS LOCATION INDEX IN THE COMPOSITION ARRAY

C      COMP.  STORE NUMBER OF MOLES HERE.

C

   30  ISTART = IFOUND + 1

        CALL FINDC(LDUMMY, ')', ISTART, IFIN, IFOUND)

        IF (IFOUND.EQ.0) THEN

            WRITE(IOUT,1100) LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	


            STOP

        END IF

C

        IFIN = IFOUND - 1

        CALL FINDC(LDUMMY, '.', ISTART, IFIN, IFOUND)

        IF (IFOUND.NE.0) THEN

             WRITE(BUFFER,'(A20)') LDUMMY(ISTART:IFIN)

             READ(BUFFER,'(F20.3)',ERR=100) X

        ELSE

             INUM = 20 - (IFIN - ISTART) - 1

             IF (INUM .NE. 0) THEN

                 WRITE(BUFFER,'(A20)') BLANK(1:INUM)//

     .                                 LDUMMY(ISTART:IFIN)

             ELSE

                 WRITE(BUFFER,'(A20)') LDUMMY(ISTART:IFIN)

             END IF

             READ(BUFFER,'(I20)',ERR=100) INUM

             X = FLOAT(INUM)

        END IF

        IF (ISTART .GE. IREPL .AND. ISTART .LE. IREPR) X  = X * REPEAT

        COMP(INDX) = COMP(INDX) + X

C

        IFIN = IFIN + 1

        GO TO 10

C

  100   WRITE(IOUT,1300) LDUMMY

C Changes made by Vera
      WRITE(*,*)'LOOK AT CONTAINS OF ''decode.error'' FILE TO FIND OUT'
      WRITE(*,*)' WHAT IS WRONG WITH ''berman.dat'' FILE'
C	

        STOP

C

 1100   FORMAT(' ONE OF THE COEFFICIENTS IN A FORMULA DOES NOT',

     .         ' HAVE A CLOSING',/,

     .        ' BRACKET.  LINE IN QUESTION IS:',/,

     .        ' ',A)

 1200   FORMAT(' ONE OF THE ELEMENTAL SYMBOLS COULD NOT BE RECOGNIZED.',

     .         /,'  SYMBOL IN QUESTION IS: ',A8,/,

     .         ' AND IS CONTAINED IN THE LINE: ',/,' ',A)

 1300   FORMAT(' ONE OF THE COEFFICIENTS',

     .         ' IN A FORMULA HAS NON-NUMERIC SYMBOLS ',

     .       /,' EMBEDED IN IT.  LINE IN QUESTION IS: ',/,

     .         ' ',A)

 1400   FORMAT(' A REPEAT GROUP DOES',

     .         ' NOT HAVE A CLOSING ] ',/,' LINE IN QUESTIONS IS:',/,

     .         ' ',A)

 1410   FORMAT(' COEFFICIENT DOES',

     .         ' NOT IMMEDIATELY FOLLOW A REPEAT GROUP.',/,

     .         ' LINE IN QUESTION IS: ',/,' ',A)

        END
