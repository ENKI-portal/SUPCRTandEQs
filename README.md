# SUPCRTandEQs README
---

## Overview
**SUPCRT** is a software package to calculate thermodynamic properties of aqueous ions and neutral species, minerals, and chemical reactions between those phases.

**EQ3** calculates homogeneous equilibrium (speciation) in aqueous solutions and compositions of solutions in equilibrium with specified minerals and gasses

**EQ6** calculates chemical mass transfer associated with minerals reacting with aqueous solutions.

The repository contains the following:
- The original FORTRAN 77 source code
- An Xcode project file that helps build executables for Macintosh
- Example thermodynamic data files
- Example input files for testing

The project has the following branches: 
- **master**: Original code branch, used for low pressure.  
This branch is the default branch compatible with example data and input files in the project. 
- **HighT**: Modified code branch for higher temperature calculations.  
- **Psat**: Recompiled EQ3/6 files for calculations at Psat (0-300º) conditions.  

:point_right: [View a flowchart of the build process.](https://gitlab.com/ENKI-portal/SUPCRTandEQs/wikis/flowcharts)

## Build SUPCRT and EQs on a Macintosh



### 1. Make sure that Xcode is installed 

Make sure that Xcode is installed (v 7.3 or later on Mac OS 10.11 or later) . You can [download Xcode from the App Store](https://itunes.apple.com/us/app/xcode/id497799835?mt=12).


### 2. Install the FORTRAN to C converter
Before building the software on the Macintosh, you must install f2c, a free FORTRAN 77 to C converter.  This software is available from [NIST](http://www.nist.gov).  

1. Obtain the tar ball for f2c. (A compressed tar ball [is available on the Wiki](https://gitlab.com/ENKI-portal/SUPCRTandEQs/wikis/f2c:-fortran-77-to-c-converter).)

2. Decompress the tar ball.  

    ```
    tar -zvxf f2c.tar.gzip
    ```

    This command creates a folder named **f2c**.  

3. Follow the README instructions in the **f2c** folder to install the converter.  Install into the root /usr/local directory for compatibility with the Xcode project.

### 3. Build executables
1. [Clone the repository.](https://gitlab.com/ENKI-portal/SUPCRTandEQs/blob/Documentation/README.md#cloning-the-repository) 
2. In Xcode, open the project SUPCRTandEQs or double-click that file in the project folder.
2. Execute the build targets as follows, in the order listed. Each build target has its own schema.

    NOTE: The first two targets are independent of the last four targets.  
    
    ---
    **CPRONS**   
    CPRONS takes a “sequential access data file” designed for SUPCRT and builds a “direct access data file” that is readable by SUPCRT.  You can rename this file. (By default, the file is named dprons*x*. For example, **dprons93** is generated from sprons93, the example file included in the project.)  
   
    :arrow_right: Run the CPRONS target schema to translate the code to C, to compile the code, and to run the executable. Then answer the questions. A file suitable for SUPCRT is copied into the project directory. Note that this directory is the Xcode project build directory, not the git project folder.
    
    ---
    **SUPCRT**  
    SUPCRT takes a data file produced by CPRONS and an input file (example provided in Calcite.in) and generates numerous output files.
      
    :arrow_right: Run the SUPCRT target schema to translate the code to C, to compile the code, and to run the executable. The executable appears in the Xcode project build directory along with any output files.
    
    ---  
    **EQLIB**  
    The EQLIB target builds the common software library functions and subroutines required for the last three targets.

    :arrow_right: Run the EQLIB target schema to translate the code to C, to compile the code, and to copy the library to the Xcode project build directory.  (The file containing the software library is named libEQLIB.a.)

    ---
    **EQPT**  
    The EQPT target processes a file named DATA0, which contains thermodynamic data required to run EQ3 and EQ6, into file formats suitable for those programs. Files named data0s, data1, data2, and data3 are produced on execution.

    :arrow_right: Run the EQPT target schema to translate the code to C, to compile the code, and to run the executable. The executable is placed into the Xcode project build directory along with all input and output files.

    ---
    **EQ3**  
    The EQ3 target requires the file **data1** (generated from EQPT) and an input file named **input** (example provided). It produces a plethora of output files, as documented.

    :arrow_right: Run the EQ3 target schema to translate the code to C, to compile the code, and to run the executable. The executable is placed in the Xcode project build directory along with all input and output files.

    ---
    **EQ6**  
    The EQ6 target requires the file **data1** (generated from EQPT) and an input file named **input** (example provided). It produces a plethora of output files, as documented.

    :arrow_right: Run the EQ6 target schema to translate the code to C, to compile the code, and to run the executable. The executable is placed into the Xcode project build directory along with all input and output files.

### Next steps
You can now move or copy the contents of the Xcode project build directory to any location. You can run the executables from a terminal window by moving to the directory containing the executable and typing the command

 
```
./[target]
```


where *[target]* is CPRONS, EQPT, EQ3, or EQ6.  Note that the data files and libEQLIB.a must reside in the same folder as executables.  

One additional target is available in the Xcode project, an experimental user interface for running all of these executables in a Macintosh-like window.  This code is not yet completed. If you want to try it, please do.  Remember to branch first, so that when you push your changes the new code is clearly identified!

#### Cloning the repository

1. In a terminal window, run the following command to clone the repository:  

    ```
    git clone git@gitlab.com:ENKI-portal/SUPCRTandEQs.git
    ```

    This command creates a folder named **SUPCRTandEQs**, which contains the files.

2. If you plan to modify the code (or for good practice), branch the project:  

    ```
    git checkout -b [name_of_your_new_branch]
    ```


For more instructions on using git, see the [overview on Wikipedia](https://en.wikipedia.org/wiki/Git_(software)) or purchase [Tower](https://www.git-tower.com).



## User documentation



## Tutorials and examples



## Additional resources

## Related software



