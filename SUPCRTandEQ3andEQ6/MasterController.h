//
//  MasterController.h
//  SUPCRTandEQs
//
//  Created by Mark Ghiorso on 10/10/15.
//  Copyright © 2015 Mark Ghiorso. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "SHCommand.h"

@interface MasterController : NSObject <SHCommandDelegate>

- (IBAction)executeCPRONS:(id)sender;
- (IBAction)executeEQPT:(id)sender;

- (IBAction)executeSUPCRT:(id)sender;

- (IBAction)executeEQ3:(id)sender;
- (IBAction)executeEQ6:(id)sender;

@property SHCommand*  m_command;

@property (weak) IBOutlet NSTextField *inputText;
@property (weak) IBOutlet NSTextField *errorText;
@property (unsafe_unretained) IBOutlet NSTextView *outputTextView;

- (IBAction)entryMadeInInputTextField:(id)sender;

@end
