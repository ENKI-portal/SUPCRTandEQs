//
//  AppDelegate.h
//  SUPCRTandEQ3andEQ6
//
//  Created by Mark Ghiorso on 10/10/15.
//  Copyright © 2015 Mark Ghiorso. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

