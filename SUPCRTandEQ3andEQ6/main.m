//
//  main.m
//  SUPCRTandEQ3andEQ6
//
//  Created by Mark Ghiorso on 10/10/15.
//  Copyright © 2015 Mark Ghiorso. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
