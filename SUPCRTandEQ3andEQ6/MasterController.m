//
//  MasterController.m
//  SUPCRTandEQs
//
//  Created by Mark Ghiorso on 10/10/15.
//  Copyright © 2015 Mark Ghiorso. All rights reserved.
//

#import "MasterController.h"

@implementation MasterController

- (void)commandDidFinish:(SHCommand *)command withExitCode:(int)iExitCode {
    [self.errorText setStringValue:[NSString stringWithFormat:@"FINISHED: Exit Code %d", iExitCode]];
}

- (void)outputData:(NSData *)data providedByCommand:(SHCommand *)command {
    NSString* szOutput = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self.outputTextView.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:szOutput attributes:NULL]];
    [self.outputTextView scrollRangeToVisible:NSMakeRange([self.outputTextView.string length], 0)];
}

- (void)errorData:(NSData *)data providedByCommand:(SHCommand *)command {
    NSString* szOutput = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    [self.outputTextView.textStorage appendAttributedString:[[NSAttributedString alloc] initWithString:szOutput attributes:NULL]];
    [self.outputTextView scrollRangeToVisible:NSMakeRange([self.outputTextView.string length], 0)];
}

- (IBAction)executeCPRONS:(id)sender {
    self.m_command = [SHCommand commandWithExecutablePath:@"/usr/bin/script"
                                            withArguments:[NSArray arrayWithObjects:@"-q", @"/dev/null", @"./CPRONS", nil]
                                             withDelegate:self];
    [self.m_command execute];
}

- (IBAction)executeEQPT:(id)sender {
    self.m_command = [SHCommand commandWithExecutablePath:@"/usr/bin/script"
                                            withArguments:[NSArray arrayWithObjects:@"-q", @"/dev/null", @"./EQPT", nil]
                                             withDelegate:self];
}

- (IBAction)executeSUPCRT:(id)sender {
    self.m_command = [SHCommand commandWithExecutablePath:@"/usr/bin/script"
                                            withArguments:[NSArray arrayWithObjects:@"-q", @"/dev/null", @"./SUPCRT", nil]
                                             withDelegate:self];
    [self.m_command execute];
}

- (IBAction)executeEQ3:(id)sender {
    self.m_command = [SHCommand commandWithExecutablePath:@"/usr/bin/script"
                                            withArguments:[NSArray arrayWithObjects:@"-q", @"/dev/null", @"./EQ3", nil]
                                             withDelegate:self];
    [self.m_command execute];
}

- (IBAction)executeEQ6:(id)sender {
    self.m_command = [SHCommand commandWithExecutablePath:@"/usr/bin/script"
                                            withArguments:[NSArray arrayWithObjects:@"-q", @"/dev/null", @"./EQ6", nil]
                                             withDelegate:self];
    [self.m_command execute];
}

- (IBAction)entryMadeInInputTextField:(id)sender {
    if (self.m_command) {
        if ([self.m_command isExecuting]) {
            [self.m_command provideInputData:[[self.inputText.stringValue stringByAppendingString:@"\n"]
                                              dataUsingEncoding:NSUTF8StringEncoding]];
        }
    }
}

@end
