//
//  AppDelegate.m
//  SUPCRTandEQ3andEQ6
//
//  Created by Mark Ghiorso on 10/10/15.
//  Copyright © 2015 Mark Ghiorso. All rights reserved.
//

#import "AppDelegate.h"
#import "MasterController.h"

@interface AppDelegate ()
@property (weak) IBOutlet NSWindow *window;
@property (weak) IBOutlet MasterController *controller;
@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    if (self.controller.m_command) [self.controller.m_command stopExecuting];
}

@end
