/* combine.f -- translated by f2c (version 20100827).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "f2c.h"

/* Common Block Declarations */

struct io_1_ {
    integer rterm, wterm, iconf, reacf, pronf, tabf, plotf[8];
};
struct io_2_ {
    integer rterm, wterm, iconf, reacf, pronf, tabf, plotf[6];
};

#define io_1 (*(struct io_1_ *) &io_)
#define io_2 (*(struct io_2_ *) &io_)

struct io2_1_ {
    integer tempf;
};

#define io2_1 (*(struct io2_1_ *) &io2_)

struct stvars_1_ {
    char isosat[20], isovar[72]	/* was [2][3] */, incvar[24]	/* was [2][3] 
	    */;
};

#define stvars_1 (*(struct stvars_1_ *) &stvars_)

struct tpdmap_1_ {
    integer mapiso[6]	/* was [2][3] */, mapinc[6]	/* was [2][3] */, 
	    mapv3[6]	/* was [2][3] */;
};

#define tpdmap_1 (*(struct tpdmap_1_ *) &tpdmap_)

struct headmp_1_ {
    char isov[60]	/* was [2][3] */, incv[60]	/* was [2][3] */, 
	    var3[60]	/* was [2][3] */;
};

#define headmp_1 (*(struct headmp_1_ *) &headmp_)

struct refval_1_ {
    doublereal mwh2o, r__, pref, tref, zprtr, yprtr;
};
struct refval_2_ {
    doublereal mwh2o, rr, pref, tref, zprtr, yprtr;
};
struct refval_3_ {
    doublereal mwh2o, r__, pr, tr, zprtr, yprtr;
};

#define refval_1 (*(struct refval_1_ *) &refval_)
#define refval_2 (*(struct refval_2_ *) &refval_)
#define refval_3 (*(struct refval_3_ *) &refval_)

struct {
    integer nmin1, nmin2, nmin3, nmin4, ngas, naqs, rec1m1, rec1m2, rec1m3, 
	    rec1m4, rec1gg, rec1aa;
} rlimit_;

#define rlimit_1 rlimit_

struct {
    integer ntrm2;
} tranm2_;

#define tranm2_1 tranm2_

struct aqscon_1_ {
    doublereal eta, theta, psi, anion, cation, gref;
};

#define aqscon_1 (*(struct aqscon_1_ *) &aqscon_)

struct qtzcon_1_ {
    doublereal aa, ba, ca, vpttta, vprttb, stran;
};

#define qtzcon_1 (*(struct qtzcon_1_ *) &qtzcon_)

struct satend_1_ {
    doublereal satmin[2];
};

#define satend_1 (*(struct satend_1_ *) &satend_)

struct defval_1_ {
    doublereal dpmin, dpmax, dpinc, dtmin, dtmax, dtinc, dtsmin, dtsmax, 
	    dtsinc;
};

#define defval_1 (*(struct defval_1_ *) &defval_)

struct null_1_ {
    doublereal xnullm, xnulla;
};

#define null_1 (*(struct null_1_ *) &null_)

struct badtd_1_ {
    logical lvdome[231]	/* was [21][11] */, h2oerr[231]	/* was [21][11] */;
};

#define badtd_1 (*(struct badtd_1_ *) &badtd_)

struct fnames_1_ {
    char namecf[20], namerf[20], nametf[20], namepf[160];
};

#define fnames_1 (*(struct fnames_1_ *) &fnames_)

struct eq36_1_ {
    logical eq3run;
};

#define eq36_1 (*(struct eq36_1_ *) &eq36_)

struct lv1b_1_ {
    logical lv1bar;
};

#define lv1b_1 (*(struct lv1b_1_ *) &lv1b_)

struct h2ogrd_1_ {
    doublereal dsvar[231]	/* was [21][11] */, vw[231]	/* was [21][
	    11] */, bew[231]	/* was [21][11] */, alw[231]	/* was [21][
	    11] */, dalw[231]	/* was [21][11] */, sw[231]	/* was [21][
	    11] */, cpw[231]	/* was [21][11] */, hw[231]	/* was [21][
	    11] */, gw[231]	/* was [21][11] */, zw[231]	/* was [21][
	    11] */, qw[231]	/* was [21][11] */, yw[231]	/* was [21][
	    11] */, xw[231]	/* was [21][11] */;
};

#define h2ogrd_1 (*(struct h2ogrd_1_ *) &h2ogrd_)

struct h2oss_1_ {
    doublereal dwss, vwss, bewss, alwss, dalwss, swss, cpwss, hwss, gwss, 
	    zwss, qwss, ywss, xwss;
};

#define h2oss_1 (*(struct h2oss_1_ *) &h2oss_)

struct {
    char pfname[20];
} dapron_;

#define dapron_1 dapron_

struct {
    integer isat, iopt, iplot, univar, noninc, uselvs, epseqn, geqn;
} icon_;

#define icon_1 icon_

struct {
    doublereal isomin, isomax, isoinc, v2min, v2max, v2inc, oddv1[21], oddv2[
	    21], kmin, kmax, kinc;
    integer niso, nv2, nlogk;
} grid_;

#define grid_1 grid_

struct {
    logical savecf, saverf;
} saveif_;

#define saveif_1 saveif_

struct {
    integer xyplot, end, nplots;
} plottr_;

#define plottr_1 plottr_

struct {
    char rtitle[800];
} reac1_;

#define reac1_1 reac1_

struct {
    doublereal coefm[100]	/* was [10][10] */, coefa[100]	/* was [10][
	    10] */, coefg[100]	/* was [10][10] */, coefw[10];
    integer nm[10], na[10], ng[10], nw[10], rec1m[100]	/* was [10][10] */, 
	    rec1a[100]	/* was [10][10] */, rec1g[100]	/* was [10][10] */;
    logical m2reac[10];
} reac2_;

#define reac2_1 reac2_

struct {
    char mname[200], mform[300];
} mnames_;

#define mnames_1 mnames_

struct {
    doublereal gfmin[10], hfmin[10], sprtrm[10], vprtrm[10], mk1[30]	/* 
	    was [3][10] */, mk2[30]	/* was [3][10] */, mk3[30]	/* 
	    was [3][10] */, mk4[30]	/* was [3][10] */, ttran[30]	/* 
	    was [3][10] */, htran[30]	/* was [3][10] */, vtran[30]	/* 
	    was [3][10] */, dpdttr[30]	/* was [3][10] */, tmaxm[10];
    integer ntran[10];
} minref_;

#define minref_1 minref_

struct {
    char gname[200], gform[300];
} gnames_;

#define gnames_1 gnames_

struct {
    doublereal gfgas[10], hfgas[10], sprtrg[10], vprtrg[10], mkg[30]	/* 
	    was [3][10] */, tmaxg[10];
} gasref_;

#define gasref_1 gasref_

struct {
    char aname[200], aform[300];
} anames_;

#define anames_1 anames_

struct {
    doublereal gfaqs[10], hfaqs[10], sprtra[10], c__[20]	/* was [2][10]
	     */, a[40]	/* was [4][10] */, wref[10], chg[10];
} aqsref_;

#define aqsref_1 aqsref_

struct {
    doublereal dvr, dsr, dcpr, dhr, dgr, logkr, dlogkt, dlogkp;
} fmeq_;

#define fmeq_1 fmeq_

struct {
    doublereal vmin[10], smin[10], cpmin[10], hmin[10], gmin[10];
    integer phaser[10];
} minsp_;

#define minsp_1 minsp_

struct {
    doublereal ttranp[30]	/* was [3][10] */, ptrant[30]	/* was [3][10]
	     */;
} pttran_;

#define pttran_1 pttran_

struct {
    doublereal ft, fd, fvd, fvk, fs, fp, fh, fst, fc;
} units_;

#define units_1 units_

struct {
    doublereal wprops[23], wpliq[23];
} wpvals_;

#define wpvals_1 wpvals_

struct tolers_1_ {
    doublereal ttol, ptol, dtol, xtol, exptol, fptol;
};

#define tolers_1 (*(struct tolers_1_ *) &tolers_)

struct crits_1_ {
    doublereal tc, rhoc, pc, pcon, ucon, scon, dpcon;
};
struct crits_2_ {
    doublereal tcrit, rhoc, pc, pcon, ucon, scon, dpcon;
};
struct crits_3_ {
    doublereal tc, dc, pc, pcon, ucon, scon, dpcon;
};

#define crits_1 (*(struct crits_1_ *) &crits_)
#define crits_2 (*(struct crits_2_ *) &crits_)
#define crits_3 (*(struct crits_3_ *) &crits_)

struct tpoint_1_ {
    doublereal utr, str, htr, atr, gtr, ttr, ptripl, dltrip, dvtrip;
};
struct tpoint_2_ {
    doublereal utripl, stripl, htripl, atripl, gtripl, ttripl, ptripl, dltrip,
	     dvtrip;
};
struct tpoint_3_ {
    doublereal utr, str, htr, atr, gtr, ttripl, ptripl, dltrip, dvtrip;
};

#define tpoint_1 (*(struct tpoint_1_ *) &tpoint_)
#define tpoint_2 (*(struct tpoint_2_ *) &tpoint_)
#define tpoint_3 (*(struct tpoint_3_ *) &tpoint_)

struct hgkbnd_1_ {
    doublereal ttop, tbtm, ptop, pbtm, dtop, dbtm;
};

#define hgkbnd_1 (*(struct hgkbnd_1_ *) &hgkbnd_)

struct liqice_1_ {
    doublereal sdli1, spli1, sdli37, spli37, sdib30, tli13, pli13, dli13, 
	    tnib30, dnib30;
};

#define liqice_1 (*(struct liqice_1_ *) &liqice_)

struct aconst_1_ {
    doublereal wm, gascon, tz, aa, zb, dzb, yb, uref, sref;
};
struct aconst_2_ {
    doublereal wm, gascon, tz, aa, z__, dz, y, uref, sref;
};
struct aconst_3_ {
    doublereal wm, gascon, tz, aa, zb, dzb, y, uref, sref;
};
struct aconst_4_ {
    doublereal wm, gascon, tz, a, z__, dz, y, uref, sref;
};

#define aconst_1 (*(struct aconst_1_ *) &aconst_)
#define aconst_2 (*(struct aconst_2_ *) &aconst_)
#define aconst_3 (*(struct aconst_3_ *) &aconst_)
#define aconst_4 (*(struct aconst_4_ *) &aconst_)

struct {
    doublereal rt;
} rtcurr_;

#define rtcurr_1 rtcurr_

struct coefs_1_ {
    doublereal a[20], q[20], x[11];
};
struct coefs_2_ {
    doublereal aa[20], qq[20], xx[11];
};

#define coefs_1 (*(struct coefs_1_ *) &coefs_)
#define coefs_2 (*(struct coefs_2_ *) &coefs_)

struct {
    doublereal dliq, dvap, dh2o;
    integer iphase;
} satur_;

#define satur_1 satur_

struct nconst_1_ {
    doublereal g[40];
    integer ii[40], jj[40], nc;
};
struct nconst_2_ {
    doublereal g[40];
    integer ii[40], jj[40], n;
};
struct nconst_3_ {
    doublereal g[40];
    integer kk[40], ll[40], n;
};

#define nconst_1 (*(struct nconst_1_ *) &nconst_)
#define nconst_2 (*(struct nconst_2_ *) &nconst_)
#define nconst_3 (*(struct nconst_3_ *) &nconst_)

struct ellcon_1_ {
    doublereal g1, g2, gf, b1, b2, b1t, b2t, b1tt, b2tt;
};

#define ellcon_1 (*(struct ellcon_1_ *) &ellcon_)

struct bconst_1_ {
    doublereal bp[10], bq[10];
};

#define bconst_1 (*(struct bconst_1_ *) &bconst_)

struct addcon_1_ {
    doublereal atz[4], adz[4], aat[4], aad[4];
};
struct addcon_2_ {
    doublereal tempi[4], densi[4], betai[4], alphai[4];
};

#define addcon_1 (*(struct addcon_1_ *) &addcon_)
#define addcon_2 (*(struct addcon_2_ *) &addcon_)

struct hgkcrt_1_ {
    doublereal tchgk, dchgk, pchgk;
};

#define hgkcrt_1 (*(struct hgkcrt_1_ *) &hgkcrt_)

union {
    struct {
	doublereal q0, q5;
    } _1;
    struct {
	doublereal qp, qdp;
    } _2;
    struct {
	doublereal q, q5;
    } _3;
    struct {
	doublereal q00, q11;
    } _4;
} qqqq_;

#define qqqq_1 (qqqq_._1)
#define qqqq_2 (qqqq_._2)
#define qqqq_3 (qqqq_._3)
#define qqqq_4 (qqqq_._4)

struct {
    doublereal ad, gd, sd, ud, hd, cvd, cpd, dpdt, dvdt, dpdd, cjtt, cjth;
} fcts_;

#define fcts_1 fcts_

struct {
    doublereal ab, gb, sb, ub, hb, cvb, pb, dpdtb;
} basef_;

#define basef_1 basef_

struct {
    doublereal ar, gr, sr, ur, hr, cvr, dpdtr;
} resf_;

#define resf_1 resf_

struct {
    doublereal ai, gi, si, ui, hi, cvi, cpi;
} idf_;

#define idf_1 idf_

struct {
    doublereal ae, ge, u, h__, entrop, cp, cv, betaw, alphw, heat, speed;
} therm_;

#define therm_1 therm_

struct {
    doublereal r1, th1;
} param_;

#define param_1 param_

struct {
    doublereal dpdd, dpdt;
} deri2_;

#define deri2_1 deri2_

union {
    struct {
	doublereal amu, s[2], pw, tw, dtw, dm0dt, dp0dt, d2pdm2, d2pdmt, 
		d2pdt2, p0th, p1th, xk[2];
    } _1;
    struct {
	doublereal amu, sss[2], pw, tw, dtw, dm0dt, dp0dt, d2pdm2, d2pdmt, 
		d2pdt2, p0th, p1th, xk[2];
    } _2;
} deriv_;

#define deriv_1 (deriv_._1)
#define deriv_2 (deriv_._2)

struct {
    doublereal r__, th;
} abc2_;

#define abc2_1 abc2_

struct {
    doublereal dpdm;
} abc1_;

#define abc1_1 abc1_

struct {
    doublereal dpdtcd;
} abc3_;

#define abc3_1 abc3_

struct {
    doublereal sav2, sav3, sav4, sav5, sav6, sav7, sav8, sav9, sav10, sav11, 
	    sav12, sav13, sav14, sav15, sav16, sav17, sav18, sav19;
    integer isav1;
} store_;

#define store_1 store_

struct {
    doublereal vgas[10], sgas[10], cpgas[10], hgas[10], ggas[10];
} gassp_;

#define gassp_1 gassp_

struct {
    doublereal vaqs[10], saqs[10], cpaqs[10], haqs[10], gaqs[10];
} aqsp_;

#define aqsp_1 aqsp_

struct {
    doublereal vqterm[10], syterm[10], cpxtrm[10], hyterm[10], gzterm[10];
} solvn_;

#define solvn_1 solvn_

struct {
    doublereal pbuff[1848]	/* was [21][11][8] */;
} pbuffr_;

#define pbuffr_1 pbuffr_

/* Initialized data */

struct {
    doublereal e_1[6];
    } tolers_ = { 1e-6, 1e-6, 1e-9, 1e-5, -673.5, 1e-7 };

struct {
    doublereal e_1[7];
    } crits_ = { 647.067, 322.778, 22.046, .034070660379837018423130834983, 
	    22046., 34.070660379837018423130834983, 
	    3.27018783663660700780197e-7 };

struct {
    doublereal fill_1[5];
    doublereal e_2[4];
    } tpoint_ = { {0}, 273.16, .00611731677193563186622762580414, 
	    .999778211030936587977889295063, 
	    4.85467583448287303988319166423e-6 };

struct {
    doublereal e_1[6];
    } hgkbnd_ = { 2250., -20., 3e4, .001, 1.38074666423686955066817336896, 
	    8.58745555396173972667420987465e-8 };

struct {
    doublereal e_1[10];
    } liqice_ = { -.00584797401732178547634910059828, 
	    -138.180804975562958027981345769, 
	    .00183244000000000000000000000007, 
	    174.536874999999999999999999995, 
	    -1.68375439429928741092636579574e-4, -15., 2074.1, 
	    1.08755631570602617113573577945, 145., 
	    1.02631640581853166397515716306 };

struct {
    doublereal e_1[4];
    doublereal fill_2[3];
    doublereal e_3[2];
    } aconst_ = { 18.0152, .461522, 647.073, 1., {0}, -4328.455039, 7.6180802 
	    };

struct {
    doublereal e_1[51];
    } coefs_ = { -.017762, 5.238, 0., -25.4915, 6.8445, .325, 1.4403, 0., 
	    1.3757, 23.6666, 4.82, .2942, -11.2326, -22.6547, -17.8876, 
	    -4.9332, 1.109430391161019373812391218008, 
	    -1.981395981400671095301629432211, 
	    .246912528778663959151808173743, -.843411332867484343974055795059,
	     -.006, -.003, 0., 647.067, 322.778, 22.046, .267, -1.6, 
	    .491775937675717720291497417773, .1085, 
	    .586534703230779473334597524774, 
	    -1.026243389120214352553706598564, 
	    .612903225806451612903225804745, .5, -.3915, .825, .7415, 
	    .103245882826119154987166286332, .16032243415919199139485749536, 
	    -.169859514687100893997445721324, 643., 645.3, 695., 199.775, 
	    420.04, 20.994569113594071907529394596, 
	    21.5814057875264119875397458907, 30.135, 40.484, 
	    .175777517046267847932127026995, .380293646126229135059562456934 }
	    ;

struct {
    doublereal e_1[40];
    integer e_2[81];
    } nconst_ = { -530.62968529023, 2274.4901424408, 787.79333020687, 
	    -69.830527374994, 17863.832875422, -39514.731563338, 
	    33803.884280753, -13855.050202703, -256374.3661326, 
	    482125.75981415, -341830.1696966, 122231.56417448, 
	    1179743.3655832, -2173481.0110373, 1082995.216862, 
	    -254419.98064049, -3137777.4947767, 5291191.0757704, 
	    -1380257.7177877, -251099.14369001, 4656182.6115608, 
	    -7275277.3275387, 417742.46148294, 1401635.8244614, 
	    -3155523.1392127, 4792966.6384584, 409126.64781209, 
	    -1362636.9388386, 696252.20862664, -1083490.0096447, 
	    -227228.27401688, 383654.8600066, 6883.3257944332, 
	    21757.245522644, -2662.794482977, -70730.418082074, -.225, -1.68, 
	    .055, -93., 0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 
	    4, 4, 5, 5, 5, 5, 6, 6, 6, 6, 8, 8, 8, 8, 2, 2, 0, 4, 2, 2, 2, 4, 
	    2, 3, 5, 7, 2, 3, 5, 7, 2, 3, 5, 7, 2, 3, 5, 7, 2, 3, 5, 7, 2, 3, 
	    5, 7, 2, 3, 5, 7, 2, 3, 5, 7, 1, 4, 4, 4, 0, 2, 0, 0, 36 };

struct {
    doublereal e_1[3];
    doublereal fill_2[6];
    } ellcon_ = { 11., 44.333333333333, 3.5 };

struct {
    doublereal e_1[20];
    } bconst_ = { .7478629, -.3540782, 0., 0., .007159876, 0., -.003528426, 
	    0., 0., 0., 1.1278334, 0., -.5944001, -5.010996, 0., .63684256, 
	    0., 0., 0., 0. };

struct {
    doublereal e_1[16];
    } addcon_ = { 640., 640., 641.6, 270., .319, .319, .319, 1.55, 2e4, 2e4, 
	    4e4, 25., 34., 40., 30., 1050. };

struct {
    doublereal e_1[3];
    } hgkcrt_ = { 647.126, 322., 22.055 };

struct {
    integer e_1;
    } io2_ = { 44 };

struct {
    char e_1[116];
    } stvars_ = { "TEMP(degC)PRES(bars)CHORES(g/cc)BARS(bars)  THERMS(degC)T"
	    "HERMS(degC)THERMS(degC)BARS(bars)  TEMPTEMPDENSPRESPRESTEMP" };

struct {
    integer e_1[18];
    } tpdmap_ = { 3, 2, 1, 1, 1, 2, 1, 1, 3, 2, 2, 1, 2, 3, 2, 3, 4, 4 };

struct {
    char e_1[180];
    } headmp_ = { "DH2O(g/cc)PRES(bars)TEMP(degC)TEMP(degC)TEMP(degC)PRES(ba"
	    "rs)TEMP(degC)TEMP(degC)DH2O(g/cc)PRES(bars)PRES(bars)TEMP(degC)P"
	    "RES(bars)DH2O(g/cc)PRES(bars)DH2O(g/cc)DH2O(g/cc)DH2O(g/cc)" };

struct {
    doublereal e_1[4];
    doublereal fill_2[2];
    } refval_ = { 18.0152, 1.9872, 1., 298.15 };

struct {
    integer e_1[14];
    } io_ = { 5, 6, 40, 41, 42, 43, 61, 62, 63, 64, 65, 66, 67, 68 };

struct {
    doublereal e_1[6];
    } aqscon_ = { 166027., 228., 2600., 0., .94, 0. };

struct {
    doublereal e_1[6];
    } qtzcon_ = { 549.824, .65995, -4.973e-5, 23.348, 23.72, .342 };

struct {
    doublereal e_1[2];
    } satend_ = { .01, .006117316772 };

struct {
    doublereal e_1[9];
    } defval_ = { 500., 5e3, 500., 0., 1e3, 100., 0., 350., 25. };

struct {
    doublereal e_1[2];
    } null_ = { 999999., 999. };

struct {
    logical e_1[462];
    } badtd_ = { FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, 
	    FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_, FALSE_ };

struct {
    char fill_1[60];
    char e_2[160];
    } fnames_ = { {0}, "                                                    "
	    "                                                                "
	    "                                            " };

struct {
    logical e_1;
    } eq36_ = { FALSE_ };

struct {
    logical e_1;
    } lv1b_ = { FALSE_ };

struct {
    doublereal e_1[3003];
    } h2ogrd_ = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 
	    0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };

struct {
    doublereal e_1[13];
    } h2oss_ = { 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0. };


/* Table of constant values */

static integer c__1 = 1;
static integer c__2 = 2;
static integer c__90 = 90;
static integer c__3 = 3;
static integer c__132 = 132;
static integer c__21 = 21;
static integer c__5 = 5;
static integer c__11 = 11;
static integer c__9 = 9;
static logical c_false = FALSE_;
static integer c__0 = 0;
static doublereal c_b915 = 10.;
static doublereal c_b943 = -.6;
static doublereal c_b968 = 1.;
static doublereal c_b997 = 2.;
static doublereal c_b1018 = .0263;
static doublereal c_b1024 = .4678;
static doublereal c_b1026 = 1.256;
static doublereal c_b1070 = 4.8;
static doublereal c_b1071 = 3.8;
static doublereal c_b1072 = 2.8;

/* ** supcrt92 - Calculates the standard molal thermodynamic properties */
/* **            of reactions among minerals, gases, and aqueous species */
/* **            using equations and data given by Helgeson et al. (1978), */
/* **            Tanger and Helgeson (1988), Shock and Helgeson */
/* **            (1988, 1990), Shock et al. (1989, 1991), Johnson and */
/* **            Norton (1991), Johnson et al. (1991), and Sverjensky */
/* **            et al. (1991). */
/* ** */
/* *********************************************************************** */
/* ** */
/* ** Author:     James W. Johnson */
/* **             Earth Sciences Department, L-219 */
/* **             Lawrence Livermore National Laboratory */
/* **             Livermore, CA 94550 */
/* **             johnson@s05.es.llnl.gov */
/* ** */
/* ** Abandoned:  13 November 1991 */
/* ** */
/* *********************************************************************** */
/* Main program */ int MAIN__(void)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 execution in progress ... \002,/)";
    static char fmt_20[] = "(\002 calculating H2O properties ...\002,/)";
    static char fmt_40[] = "(\002 calculating properties for reaction \002,i"
	    "2,\002 of \002,i2,\002 ...\002)";
    static char fmt_50[] = "(/,\002 ... execution completed.\002,/)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Local variables */
    static integer reac, nreac;
    extern /* Subroutine */ int geth2o_(logical *), readin_(integer *, 
	    logical *, logical *), banner_(void), getmgi_(integer *), tabtop_(
	    void);
    static logical unirun, wetrun;
    extern /* Subroutine */ int runrxn_(integer *, logical *), wrtrxn_(
	    integer *);

    /* Fortran I/O blocks */
    static cilist io___4 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___5 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___7 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___8 = { 0, 0, 0, fmt_50, 0 };


    banner_();
    readin_(&nreac, &wetrun, &unirun);
    io___4.ciunit = io_1.wterm;
    s_wsfe(&io___4);
    e_wsfe();
    if (wetrun) {
	io___5.ciunit = io_1.wterm;
	s_wsfe(&io___5);
	e_wsfe();
	geth2o_(&unirun);
    }
    tabtop_();
    i__1 = nreac;
    for (reac = 1; reac <= i__1; ++reac) {
	io___7.ciunit = io_1.wterm;
	s_wsfe(&io___7);
	do_fio(&c__1, (char *)&reac, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&nreac, (ftnlen)sizeof(integer));
	e_wsfe();
	getmgi_(&reac);
	wrtrxn_(&reac);
/* L30: */
	runrxn_(&reac, &wetrun);
    }
    io___8.ciunit = io_1.wterm;
    s_wsfe(&io___8);
    e_wsfe();
    return 0;
} /* MAIN__ */

/* ******************************************************************* */
/* ** banner - Write program banner to the terminal screen. */
/* Subroutine */ int banner_(void)
{
    /* Format strings */
    static char fmt_10[] = "(/,5x,\002 Welcome to SUPCRT92 Version 1.1\002,/"
	    ",5x,\002 Author:    James W. Johnson\002,/,5x,\002 Abandoned: 13"
	    " November 1991\002,/)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void);

    /* Fortran I/O blocks */
    static cilist io___9 = { 0, 0, 0, fmt_10, 0 };


    io___9.ciunit = io_1.wterm;
    s_wsfe(&io___9);
    e_wsfe();
    return 0;
} /* banner_ */

/* ******************************************************************* */
/* ** consts - Constants */
/* Subroutine */ int consts_(void)
{
    return 0;
} /* consts_ */

/* **   8 = NPLOTF */
/* **   13*MAXISO*MAXINC = 3003 */
/* **   2*MAXISO*MAXINC = 462 */
/* **   ZPrTr, YPrTr calculated in SUBR getH2O as f(epseqn) */
/* **   8 = NPLOTF */

/* *********************************************************************** */
/* ** readin - Open user-specified, direct-access data file (STOP if none */
/* **          can be located); open/read or create[/store] an input file */
/* **          containing i/o specifications and state conditions; */
/* **          open/read line 1 of an existing file containing reaction */
/* **          titles and stoichiometries or create[/store] such a file */
/* **          in its entirety. */
/* Subroutine */ int readin_(integer *nreac, logical *wetrun, logical *unirun)
{
    /* Builtin functions */
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    extern logical getdf_(void);
    extern /* Subroutine */ int getcon_(logical *, logical *);
    static logical wetcon;
    extern /* Subroutine */ int getrxn_(integer *, logical *), getout_(void);
    static logical wetrxn;

    if (! getdf_()) {
	s_stop("", (ftnlen)0);
    }
    getcon_(&wetcon, unirun);
    getrxn_(nreac, &wetrxn);
    *wetrun = wetcon || wetrxn;
    getout_();
    return 0;
} /* readin_ */

/* *********************************************************************** */
/* ** getdf - Returns .TRUE. if an appropriate direct-access */
/* **         data file can be opened; otherwise returns .FALSE. */
logical getdf_(void)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 would you like to use the default thermo"
	    "dynamic\002,\002 database? (y/n)\002,/)";
    static char fmt_20[] = "(a1)";
    static char fmt_30[] = "(/,\002 specify filename for thermodynamic datab"
	    "ase: \002,/)";
    static char fmt_40[] = "(a20)";
    static char fmt_50[] = "(6(1x,i4))";
    static char fmt_60[] = "(/,\002 Cannot find \002,a20,/,\002 enter correc"
	    "t filename: \002,/)";
    static char fmt_70[] = "(/,\002 I am tired of looking for this file;\002"
	    ",/,\002 please do the legwork yourself!\002,//,\002 Bye for now "
	    "...\002,/)";

    /* System generated locals */
    logical ret_val;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsfe(cilist *), do_fio(integer *
	    , char *, ftnlen), e_rsfe(void);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_rdfe(cilist *), e_rdfe(void);

    /* Local variables */
    static char ans[1];
    static integer try__;
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___12 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___13 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___15 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___16 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___18 = { 0, 0, 0, fmt_50, 1 };
    static cilist io___19 = { 0, 0, 0, fmt_50, 2 };
    static cilist io___20 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___21 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___22 = { 0, 0, 0, fmt_70, 0 };


L1:
    io___12.ciunit = io_1.wterm;
    s_wsfe(&io___12);
    e_wsfe();
    io___13.ciunit = io_1.rterm;
    s_rsfe(&io___13);
    do_fio(&c__1, ans, (ftnlen)1);
    e_rsfe();
    if (*(unsigned char *)ans != 'y' && *(unsigned char *)ans != 'Y' && *(
	    unsigned char *)ans != 'n' && *(unsigned char *)ans != 'N') {
	goto L1;
    }
    if (*(unsigned char *)ans == 'y' || *(unsigned char *)ans == 'Y') {
	s_copy(dapron_1.pfname, "dprons92.dat", (ftnlen)20, (ftnlen)12);
    } else {
	io___15.ciunit = io_1.wterm;
	s_wsfe(&io___15);
	e_wsfe();
	io___16.ciunit = io_1.rterm;
	s_rsfe(&io___16);
	do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
	e_rsfe();
    }
    try__ = 0;
L2:
    if (openf_(&io_1.wterm, &io_1.pronf, dapron_1.pfname, &c__1, &c__2, &c__1,
	     &c__90, (ftnlen)20)) {
	io___18.ciunit = io_1.pronf;
	s_rdfe(&io___18);
	do_fio(&c__1, (char *)&rlimit_1.nmin1, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.nmin2, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.nmin3, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.nmin4, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.ngas, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.naqs, (ftnlen)sizeof(integer));
	e_rdfe();
	io___19.ciunit = io_1.pronf;
	s_rdfe(&io___19);
	do_fio(&c__1, (char *)&rlimit_1.rec1m1, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.rec1m2, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.rec1m3, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.rec1m4, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.rec1gg, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&rlimit_1.rec1aa, (ftnlen)sizeof(integer));
	e_rdfe();
	ret_val = TRUE_;
	return ret_val;
    } else {
	++try__;
	if (try__ < 5) {
/* **             prompt for alternative file *** */
	    io___20.ciunit = io_1.wterm;
	    s_wsfe(&io___20);
	    do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
	    e_wsfe();
	    io___21.ciunit = io_1.rterm;
	    s_rsfe(&io___21);
	    do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
	    e_rsfe();
	    goto L2;
	} else {
/* **             give up *** */
	    io___22.ciunit = io_1.wterm;
	    s_wsfe(&io___22);
	    e_wsfe();
	    ret_val = FALSE_;
	    return ret_val;
	}
    }
    return ret_val;
} /* getdf_ */

/* *********************************************************************** */
/* ** getcon - Open/read or create[/store] an input (CON) file that */
/* **          contains i/o specifications and state conditions. */
/* Subroutine */ int getcon_(logical *wetcon, logical *unirun)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 choose file option for specifying\002"
	    ",\002 reaction-independent parameters: \002,/,\002      1 = sele"
	    "ct one of three default files\002,/,\002      2 = select an exis"
	    "ting non-default file\002,/,\002      3 = build a new file:\002,"
	    "/)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsle(cilist *), do_lio(integer *
	    , integer *, char *, ftnlen), e_rsle(void);

    /* Local variables */
    static integer ifopt;
    extern /* Subroutine */ int readcf_(logical *, logical *), makecf_(
	    logical *, logical *), defaul_(logical *);

    /* Fortran I/O blocks */
    static cilist io___23 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___24 = { 0, 0, 0, 0, 0 };


L1:
    io___23.ciunit = io_1.wterm;
    s_wsfe(&io___23);
    e_wsfe();
    io___24.ciunit = io_1.rterm;
    s_rsle(&io___24);
    do_lio(&c__3, &c__1, (char *)&ifopt, (ftnlen)sizeof(integer));
    e_rsle();
    if (ifopt < 1 || ifopt > 3) {
	goto L1;
    }
    if (ifopt == 1) {
	*unirun = FALSE_;
	defaul_(wetcon);
	return 0;
    }
    if (ifopt == 2) {
	readcf_(wetcon, unirun);
    } else {
	makecf_(wetcon, unirun);
    }
    return 0;
} /* getcon_ */

/* *********************************************************************** */
/* ** defaul - Set default options / state conditions. */
/* Subroutine */ int defaul_(logical *wetcon)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 input solvent phase region\002,/,\002   "
	    "   1 = one-phase region \002,/,\002      2 = liq-vap saturation "
	    "curve:\002,/,\002      3 = EQ3/6 one-phase/sat grid:\002,/)";

    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsle(cilist *), do_lio(integer *
	    , integer *, char *, ftnlen), e_rsle(void), i_dnnt(doublereal *);

    /* Fortran I/O blocks */
    static cilist io___26 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___27 = { 0, 0, 0, 0, 0 };


    icon_1.univar = 0;
    icon_1.noninc = 0;
    icon_1.uselvs = 1;
    icon_1.epseqn = 4;
    icon_1.geqn = 3;
    eq36_1.eq3run = FALSE_;
    saveif_1.savecf = FALSE_;
/* **** prompt for / read isat ***** */
L1:
    io___26.ciunit = io_1.wterm;
    s_wsfe(&io___26);
    e_wsfe();
    io___27.ciunit = io_1.rterm;
    s_rsle(&io___27);
    do_lio(&c__3, &c__1, (char *)&icon_1.isat, (ftnlen)sizeof(integer));
    e_rsle();
    if (icon_1.isat < 1 || icon_1.isat > 3) {
	goto L1;
    } else {
	--icon_1.isat;
	*wetcon = icon_1.isat == 1;
    }
    if (icon_1.isat == 0) {
	icon_1.iopt = 2;
	icon_1.iplot = 1;
	grid_1.isomin = defval_1.dpmin;
	grid_1.isomax = defval_1.dpmax;
	grid_1.isoinc = defval_1.dpinc;
	d__1 = (grid_1.isomax - grid_1.isomin) / grid_1.isoinc;
	grid_1.niso = i_dnnt(&d__1) + 1;
	grid_1.v2min = defval_1.dtmin;
	grid_1.v2max = defval_1.dtmax;
	grid_1.v2inc = defval_1.dtinc;
	d__1 = (grid_1.v2max - grid_1.v2min) / grid_1.v2inc;
	grid_1.nv2 = i_dnnt(&d__1) + 1;
	return 0;
    }
    if (icon_1.isat == 1) {
	icon_1.iopt = 1;
	icon_1.iplot = 3;
	grid_1.v2min = defval_1.dtsmin;
	grid_1.v2max = defval_1.dtsmax;
	grid_1.v2inc = defval_1.dtsinc;
	d__1 = (grid_1.v2max - grid_1.v2min) / grid_1.v2inc;
	grid_1.nv2 = i_dnnt(&d__1) + 1;
	grid_1.isomin = 0.;
	grid_1.isomax = 0.;
	grid_1.isoinc = 0.;
	grid_1.niso = 1;
	return 0;
    }
    if (icon_1.isat == 2) {
	icon_1.isat = 0;
	icon_1.iopt = 2;
	icon_1.iplot = 2;
	grid_1.niso = 0;
	grid_1.nv2 = 0;
	icon_1.noninc = 8;
	eq36_1.eq3run = TRUE_;
	grid_1.oddv1[0] = .01;
	grid_1.oddv1[1] = 25.;
	grid_1.oddv1[2] = 60.;
	grid_1.oddv1[3] = 100.;
	grid_1.oddv1[4] = 150.;
	grid_1.oddv1[5] = 200.;
	grid_1.oddv1[6] = 250.;
	grid_1.oddv1[7] = 300.;
	grid_1.oddv2[0] = 1.01322;
	grid_1.oddv2[1] = 1.01322;
	grid_1.oddv2[2] = 1.01322;
	grid_1.oddv2[3] = 1.01322;
	grid_1.oddv2[4] = 4.75717;
	grid_1.oddv2[5] = 15.5365;
	grid_1.oddv2[6] = 39.73649;
	grid_1.oddv2[7] = 85.83784;
	return 0;
    }
    return 0;
} /* defaul_ */

/* *********************************************************************** */
/* ** readcf - Read options / state conditions (CON) file. */
/* Subroutine */ int readcf_(logical *wetcon, logical *unirun)
{
    /* Initialized data */

    static char tp[1*2] = "T" "P";

    /* Format strings */
    static char fmt_10[] = "(/,\002 specify file name:\002,/)";
    static char fmt_20[] = "(a20)";
    static char fmt_21[] = "(///)";
    static char fmt_131[] = "(/,\002 Number of specified odd-increment pai"
	    "rs \002,\002(\002,i3,\002) exceeds MAXODD (\002,i3,\002).\002,/"
	    ",\002 Revise specifications.\002)";
    static char fmt_935[] = "(/,\002 Ill-defined \002,\002 min,max,increment"
	    "  trio\002,/,\002 Revise specifications.\002)";
    static char fmt_31[] = "(/,\002 Number of specified isopleths (\002,i4"
	    ",\002)\002,\002 exceeds MAXISO (\002,i3,\002).\002,/,\002 Revise"
	    " specifications.\002)";
    static char fmt_32[] = "(/,\002 Number of specified increments \002"
	    ",\002(\002,i3,\002) exceeds MAXINC (\002,i3,\002).\002,/,\002 Re"
	    "vise specifications.\002)";
    static char fmt_152[] = "(/,1x,a1,\002min >= \002,a1,\002max \002,/,1x"
	    ",\002 revise specifications\002)";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsfe(cilist *), do_fio(integer *
	    , char *, ftnlen), e_rsfe(void), s_rsle(cilist *), do_lio(integer 
	    *, integer *, char *, ftnlen), e_rsle(void);
    /* Subroutine */ int s_stop(char *, ftnlen);
    integer i_dnnt(doublereal *);

    /* Local variables */
    static integer i__;
    static doublereal fplk, fpnv2;
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);
    static doublereal fpniso;

    /* Fortran I/O blocks */
    static cilist io___29 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___30 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___31 = { 0, 0, 0, fmt_21, 0 };
    static cilist io___32 = { 0, 0, 0, 0, 0 };
    static cilist io___33 = { 0, 0, 0, fmt_131, 0 };
    static cilist io___34 = { 0, 0, 0, fmt_131, 0 };
    static cilist io___36 = { 0, 0, 0, 0, 0 };
    static cilist io___37 = { 0, 0, 0, 0, 0 };
    static cilist io___38 = { 0, 0, 0, 0, 0 };
    static cilist io___39 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___40 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___42 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___43 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___44 = { 0, 0, 0, fmt_31, 0 };
    static cilist io___45 = { 0, 0, 0, fmt_31, 0 };
    static cilist io___46 = { 0, 0, 0, 0, 0 };
    static cilist io___47 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___48 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___50 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___51 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___52 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___53 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___54 = { 0, 0, 0, 0, 0 };
    static cilist io___55 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___56 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___58 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___59 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___60 = { 0, 0, 0, 0, 0 };
    static cilist io___61 = { 0, 0, 0, fmt_152, 0 };
    static cilist io___62 = { 0, 0, 0, 0, 0 };
    static cilist io___63 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___64 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___65 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___66 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___67 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___68 = { 0, 0, 0, fmt_32, 0 };


L1:
    io___29.ciunit = io_1.wterm;
    s_wsfe(&io___29);
    e_wsfe();
    io___30.ciunit = io_1.rterm;
    s_rsfe(&io___30);
    do_fio(&c__1, fnames_1.namecf, (ftnlen)20);
    e_rsfe();
    if (! openf_(&io_1.wterm, &io_1.iconf, fnames_1.namecf, &c__1, &c__1, &
	    c__1, &c__132, (ftnlen)20)) {
	goto L1;
    }
    saveif_1.savecf = TRUE_;
/* **** skip first 4 comment lines */
    io___31.ciunit = io_1.iconf;
    s_rsfe(&io___31);
    e_rsfe();
/* ********************************************************* */
/* ** READ and hardwire statements for distribution version */
    io___32.ciunit = io_1.iconf;
    s_rsle(&io___32);
    do_lio(&c__3, &c__1, (char *)&icon_1.isat, (ftnlen)sizeof(integer));
    do_lio(&c__3, &c__1, (char *)&icon_1.iopt, (ftnlen)sizeof(integer));
    do_lio(&c__3, &c__1, (char *)&icon_1.iplot, (ftnlen)sizeof(integer));
    do_lio(&c__3, &c__1, (char *)&icon_1.univar, (ftnlen)sizeof(integer));
    do_lio(&c__3, &c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
    e_rsle();
    icon_1.uselvs = 1;
    icon_1.epseqn = 4;
    icon_1.geqn = 3;
/* ** READ statement for development version */
/*     READ(iconf,*) isat, iopt, iplot, univar, noninc, */
/*    1              useLVS, epseqn, geqn */
/* ********************************************************* */
/* ** insert validity checker for /icon/ */
/* ** variables here if desired later */
    *wetcon = icon_1.isat == 1 || icon_1.iopt == 1;
    *unirun = icon_1.univar == 1;
    if (icon_1.noninc != 0) {
/* **        univar = 0 */
/* **        read non-incremental state conditions */
	if (icon_1.noninc > 21) {
	    io___33.ciunit = io_1.wterm;
	    s_wsfe(&io___33);
	    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
	    e_wsfe();
	    io___34.ciunit = io_1.tabf;
	    s_wsfe(&io___34);
	    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
	i__1 = icon_1.noninc;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (icon_1.isat == 1) {
		io___36.ciunit = io_1.iconf;
		s_rsle(&io___36);
		do_lio(&c__5, &c__1, (char *)&grid_1.oddv1[i__ - 1], (ftnlen)
			sizeof(doublereal));
		e_rsle();
	    } else {
		io___37.ciunit = io_1.iconf;
		s_rsle(&io___37);
		do_lio(&c__5, &c__1, (char *)&grid_1.oddv1[i__ - 1], (ftnlen)
			sizeof(doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.oddv2[i__ - 1], (ftnlen)
			sizeof(doublereal));
		e_rsle();
	    }
/* L30: */
	}
	return 0;
    }
    if (icon_1.isat == 0) {
	io___38.ciunit = io_1.iconf;
	s_rsle(&io___38);
	do_lio(&c__5, &c__1, (char *)&grid_1.isomin, (ftnlen)sizeof(
		doublereal));
	do_lio(&c__5, &c__1, (char *)&grid_1.isomax, (ftnlen)sizeof(
		doublereal));
	do_lio(&c__5, &c__1, (char *)&grid_1.isoinc, (ftnlen)sizeof(
		doublereal));
	e_rsle();
	if (grid_1.isomin == grid_1.isomax) {
	    grid_1.niso = 1;
	} else {
	    if (grid_1.isoinc == 0.) {
		io___39.ciunit = io_1.wterm;
		s_wsfe(&io___39);
		e_wsfe();
		io___40.ciunit = io_1.tabf;
		s_wsfe(&io___40);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	    fpniso = (grid_1.isomax - grid_1.isomin) / grid_1.isoinc + 1.;
	    grid_1.niso = i_dnnt(&fpniso);
	    if ((d__1 = fpniso - (doublereal) grid_1.niso, abs(d__1)) > 1e-6) 
		    {
		io___42.ciunit = io_1.wterm;
		s_wsfe(&io___42);
		e_wsfe();
		io___43.ciunit = io_1.tabf;
		s_wsfe(&io___43);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	if (grid_1.niso > 11) {
	    io___44.ciunit = io_1.wterm;
	    s_wsfe(&io___44);
	    do_fio(&c__1, (char *)&grid_1.niso, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__11, (ftnlen)sizeof(integer));
	    e_wsfe();
	    io___45.ciunit = io_1.tabf;
	    s_wsfe(&io___45);
	    do_fio(&c__1, (char *)&grid_1.niso, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__11, (ftnlen)sizeof(integer));
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
    } else {
	io___46.ciunit = io_1.iconf;
	s_rsle(&io___46);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal)
		);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal)
		);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(doublereal)
		);
	e_rsle();
	if (grid_1.v2min == grid_1.v2max) {
	    grid_1.nv2 = 1;
	} else {
	    if (grid_1.v2inc == 0.) {
		io___47.ciunit = io_1.wterm;
		s_wsfe(&io___47);
		e_wsfe();
		io___48.ciunit = io_1.tabf;
		s_wsfe(&io___48);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	    fpnv2 = (grid_1.v2max - grid_1.v2min) / grid_1.v2inc + 1.;
	    grid_1.nv2 = i_dnnt(&fpnv2);
	    if ((d__1 = fpnv2 - (doublereal) grid_1.nv2, abs(d__1)) > 1e-6) {
		io___50.ciunit = io_1.wterm;
		s_wsfe(&io___50);
		e_wsfe();
		io___51.ciunit = io_1.tabf;
		s_wsfe(&io___51);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	if (grid_1.nv2 > 21) {
	    io___52.ciunit = io_1.wterm;
	    s_wsfe(&io___52);
	    do_fio(&c__1, (char *)&grid_1.nv2, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
	    e_wsfe();
	    io___53.ciunit = io_1.tabf;
	    s_wsfe(&io___53);
	    do_fio(&c__1, (char *)&grid_1.nv2, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
	grid_1.niso = 1;
	grid_1.isomin = 0.;
	grid_1.isomax = 0.;
	grid_1.isoinc = 0.;
	return 0;
    }
    if (icon_1.univar == 1) {
/* **        univariant curve option enabled */
	io___54.ciunit = io_1.iconf;
	s_rsle(&io___54);
	do_lio(&c__5, &c__1, (char *)&grid_1.kmin, (ftnlen)sizeof(doublereal))
		;
	do_lio(&c__5, &c__1, (char *)&grid_1.kmax, (ftnlen)sizeof(doublereal))
		;
	do_lio(&c__5, &c__1, (char *)&grid_1.kinc, (ftnlen)sizeof(doublereal))
		;
	e_rsle();
	if (grid_1.kmin == grid_1.kmax) {
	    grid_1.nlogk = 1;
	} else {
	    if (grid_1.kinc == 0.) {
		io___55.ciunit = io_1.wterm;
		s_wsfe(&io___55);
		e_wsfe();
		io___56.ciunit = io_1.tabf;
		s_wsfe(&io___56);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	    fplk = (grid_1.kmax - grid_1.kmin) / grid_1.kinc + 1.;
	    grid_1.nlogk = i_dnnt(&fplk);
	    if ((d__1 = fplk - (doublereal) grid_1.nlogk, abs(d__1)) > 1e-6) {
		io___58.ciunit = io_1.wterm;
		s_wsfe(&io___58);
		e_wsfe();
		io___59.ciunit = io_1.tabf;
		s_wsfe(&io___59);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	io___60.ciunit = io_1.iconf;
	s_rsle(&io___60);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal)
		);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal)
		);
	e_rsle();
	if (grid_1.v2min < grid_1.v2max) {
	    grid_1.v2inc = 0.;
	} else {
	    io___61.ciunit = io_1.wterm;
	    s_wsfe(&io___61);
	    do_fio(&c__1, tp + (icon_1.iplot - 1), (ftnlen)1);
	    do_fio(&c__1, tp + (icon_1.iplot - 1), (ftnlen)1);
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
    } else {
/* **        univariant curve option disabled */
	io___62.ciunit = io_1.iconf;
	s_rsle(&io___62);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal)
		);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal)
		);
	do_lio(&c__5, &c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(doublereal)
		);
	e_rsle();
	if (grid_1.v2min == grid_1.v2max) {
	    grid_1.nv2 = 1;
	} else {
	    if (grid_1.v2inc == 0.) {
		io___63.ciunit = io_1.wterm;
		s_wsfe(&io___63);
		e_wsfe();
		io___64.ciunit = io_1.tabf;
		s_wsfe(&io___64);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	    fpnv2 = (grid_1.v2max - grid_1.v2min) / grid_1.v2inc + 1.;
	    grid_1.nv2 = i_dnnt(&fpnv2);
	    if ((d__1 = fpnv2 - (doublereal) grid_1.nv2, abs(d__1)) > 1e-6) {
		io___65.ciunit = io_1.wterm;
		s_wsfe(&io___65);
		e_wsfe();
		io___66.ciunit = io_1.tabf;
		s_wsfe(&io___66);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	if (grid_1.nv2 > 21) {
	    io___67.ciunit = io_1.wterm;
	    s_wsfe(&io___67);
	    do_fio(&c__1, (char *)&grid_1.nv2, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
	    e_wsfe();
	    io___68.ciunit = io_1.tabf;
	    s_wsfe(&io___68);
	    do_fio(&c__1, (char *)&grid_1.nv2, (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
    }
    return 0;
} /* readcf_ */

/* *********************************************************************** */
/* ** makecf - Prompt for and create options / state conditions */
/* **          (CON) file. */
/* Subroutine */ int makecf_(logical *wetcon, logical *unirun)
{
    /* Initialized data */

    static char tp[1*2] = "T" "P";
    static char ptype2[1*2] = "D" "P";
    static char ptype1[6*2] = "CHORIC" "BARIC ";

    /* Format strings */
    static char fmt_10[] = "(/,\002 specify solvent phase region \002,/,\002"
	    "      1 = one-phase region \002,/,\002      2 = liq-vap saturati"
	    "on curve:\002,/)";
    static char fmt_20[] = "(/,\002 specify independent state variables: "
	    "\002,/,\002      1 = temperature (degC), density[H2O] (g/cc) "
	    "\002,/,\002      2 = temperature (degC), pressure (bars)\002,/)";
    static char fmt_30[] = "(/,\002 specify independent liq-vap saturation v"
	    "ariable:\002,/,\002      1 = temperature (degC)\002,/,\002      "
	    "2 = pressure (bars)\002,/)";
    static char fmt_40[] = "(/,\002 specify table-increment option: \002,/"
	    ",\002      1 = calculate tables having uniform increments\002,/"
	    ",\002      2 = calculate tables having unequal increments\002,/)";
    static char fmt_50[] = "(/,\002 specify \002,a10,\002 range:\002,/,\002 "
	    "min, max, increment:\002,/)";
    static char fmt_899[] = "(/,\002 Maximum saturation temperature \002,"
	    "\002(\002,f4.0,\002) > critical temperature\002,\002 (373.917)"
	    ".\002,/,\002 Revise specifications.\002)";
    static char fmt_935[] = "(/,\002 Ill-defined \002,\002 min,max,increment"
	    "  trio\002,/,\002 Revise specifications.\002)";
    static char fmt_31[] = "(/,\002 Number of specified isopleths\002,\002 "
	    "(\002,i4,\002) exceeds MAXINC (\002,i3,\002).\002,/,\002 Revise "
	    "specifications.\002)";
    static char fmt_60[] = "(/,\002 specify liq-vap saturation \002,a10,\002"
	    " values\002,/,\002 one per line, concluding with a zero:\002,/)";
    static char fmt_241[] = "(/,\002 Only \002,i3,\002 coordinates separate"
	    "d\002,\002 by unequal increments\002,/,\002 can be\002,\002 proc"
	    "essed during one SUPCRT92 execution\002,/)";
    static char fmt_70[] = "(/,\002 would you like to use the univariant cur"
	    "ve\002,\002 option;\002,/,\002 i.e., calculate T(logK,P) or P(lo"
	    "gK,T) \002,\002 (y/n)\002,/)";
    static char fmt_75[] = "(a1)";
    static char fmt_80[] = "(/,\002 specify tablulation option:\002,/,\002  "
	    "    1 = calculate ISO\002,a6,\002(T) tables, \002,/,\002      2 "
	    "= calculate ISOTHERMAL(\002,a1,\002) \002,\002tables \002,/)";
    static char fmt_100[] = "(/,\002 specify ISO\002,a12,/,\002 min, max, in"
	    "crement\002,/)";
    static char fmt_110[] = "(/,\002 specify \002,a10,\002 range\002,/,\002 "
	    "min, max, increment\002,/)";
    static char fmt_32[] = "(/,\002 Number of specified increments\002,"
	    "\002 (\002,i4,\002) exceeds MAXINC (\002,i3,\002).\002,/,\002 Re"
	    "vise specifications.\002)";
    static char fmt_120[] = "(/,\002 specify \002,a10,\002, \002,a10,\002 va"
	    "lues; \002,/,\002 one pair per line, concluding with 0,0:\002,/)";
    static char fmt_130[] = "(/,\002 specify univariant calculation option"
	    ":\002,/,\002      1 = calculate T(logK,isobars), \002,/,\002    "
	    "  2 = calculate P(logK,isotherms): \002,/)";
    static char fmt_140[] = "(/,\002 specify ISO\002,a12,/,\002 min, max, in"
	    "crement \002,/)";
    static char fmt_150[] = "(/,\002 specify logK range: \002,/,\002 Kmin, K"
	    "max, Kincrement: \002,/)";
    static char fmt_151[] = "(/,\002 specify bounding \002,a10,\002 range"
	    ":\002,/,1x,a1,\002min, \002,a1,\002max: \002,/)";
    static char fmt_152[] = "(/,1x,a1,\002min >= \002,a1,\002max \002,/,1x"
	    ",\002 revise specifications\002)";
    static char fmt_210[] = "(/,\002 would you like to save these reaction-i"
	    "ndependent\002,/,\002 parameters to a file (y/n):\002,/)";
    static char fmt_230[] = "(/,\002 specify file name:\002,/)";
    static char fmt_240[] = "(a20)";
    static char fmt_250[] = "(\002 Line 1 (free format):\002,\002 isat, iopt"
	    ", iplot, univar, noninc\002)";
    static char fmt_251[] = "(\002 Line 2 (free format): v2min, v2max, v2in"
	    "c\002)";
    static char fmt_256[] = "(66(\002*\002))";
    static char fmt_249[] = "(\002 Line 2 (free format): isomin, isomax, iso"
	    "inc\002)";
    static char fmt_252[] = "(\002 Lines i=2..\002,i2,\002 (free format): od"
	    "dv1(i)\002)";
    static char fmt_253[] = "(\002 Lines i=2..\002,i2,\002 (free format): od"
	    "dv1(i), oddv2(i)\002)";
    static char fmt_254[] = "(\002 Line 3 (free format):\002,\002 v2min, v2m"
	    "ax, v2inc\002)";
    static char fmt_255[] = "(\002 Line 3 (free format):\002,\002 Kmin, Kmax"
	    ", Kinc\002)";
    static char fmt_259[] = "(\002 Line 4 (free format):\002,\002 v2min, v2m"
	    "ax\002)";
    static char fmt_350[] = "(5(1x,i3))";

    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsle(cilist *), do_lio(integer *
	    , integer *, char *, ftnlen), e_rsle(void), do_fio(integer *, 
	    char *, ftnlen), i_dnnt(doublereal *), s_rsfe(cilist *), e_rsfe(
	    void), s_wsle(cilist *), e_wsle(void);

    /* Local variables */
    static integer i__;
    static char ans[1];
    static doublereal fpnk, fpnv2;
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);
    static doublereal fpniso;

    /* Fortran I/O blocks */
    static cilist io___72 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___73 = { 0, 0, 0, 0, 0 };
    static cilist io___74 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___75 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___76 = { 0, 0, 0, 0, 0 };
    static cilist io___77 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___78 = { 0, 0, 0, 0, 0 };
    static cilist io___79 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___80 = { 0, 0, 0, 0, 0 };
    static cilist io___81 = { 0, 0, 0, fmt_899, 0 };
    static cilist io___82 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___84 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___85 = { 0, 0, 0, fmt_31, 0 };
    static cilist io___86 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___87 = { 0, 0, 0, 0, 0 };
    static cilist io___88 = { 0, 0, 0, fmt_241, 0 };
    static cilist io___89 = { 0, 0, 0, fmt_70, 0 };
    static cilist io___90 = { 0, 0, 0, fmt_75, 0 };
    static cilist io___92 = { 0, 0, 0, fmt_80, 0 };
    static cilist io___93 = { 0, 0, 0, 0, 0 };
    static cilist io___94 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___95 = { 0, 0, 0, 0, 0 };
    static cilist io___96 = { 0, 0, 0, fmt_100, 0 };
    static cilist io___97 = { 0, 0, 0, 0, 0 };
    static cilist io___98 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___100 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___101 = { 0, 0, 0, fmt_31, 0 };
    static cilist io___102 = { 0, 0, 0, fmt_110, 0 };
    static cilist io___103 = { 0, 0, 0, 0, 0 };
    static cilist io___104 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___105 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___106 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___107 = { 0, 0, 0, fmt_120, 0 };
    static cilist io___108 = { 0, 0, 0, 0, 0 };
    static cilist io___109 = { 0, 0, 0, fmt_241, 0 };
    static cilist io___110 = { 0, 0, 0, fmt_130, 0 };
    static cilist io___111 = { 0, 0, 0, 0, 0 };
    static cilist io___112 = { 0, 0, 0, fmt_140, 0 };
    static cilist io___113 = { 0, 0, 0, 0, 0 };
    static cilist io___114 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___115 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___116 = { 0, 0, 0, fmt_31, 0 };
    static cilist io___117 = { 0, 0, 0, fmt_150, 0 };
    static cilist io___118 = { 0, 0, 0, 0, 0 };
    static cilist io___119 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___121 = { 0, 0, 0, fmt_935, 0 };
    static cilist io___122 = { 0, 0, 0, fmt_151, 0 };
    static cilist io___123 = { 0, 0, 0, 0, 0 };
    static cilist io___124 = { 0, 0, 0, fmt_152, 0 };
    static cilist io___125 = { 0, 0, 0, fmt_210, 0 };
    static cilist io___126 = { 0, 0, 0, fmt_75, 0 };
    static cilist io___127 = { 0, 0, 0, fmt_230, 0 };
    static cilist io___128 = { 0, 0, 0, fmt_240, 0 };
    static cilist io___129 = { 0, 0, 0, fmt_250, 0 };
    static cilist io___130 = { 0, 0, 0, fmt_251, 0 };
    static cilist io___131 = { 0, 0, 0, fmt_256, 0 };
    static cilist io___132 = { 0, 0, 0, fmt_249, 0 };
    static cilist io___133 = { 0, 0, 0, fmt_252, 0 };
    static cilist io___134 = { 0, 0, 0, fmt_253, 0 };
    static cilist io___135 = { 0, 0, 0, fmt_256, 0 };
    static cilist io___136 = { 0, 0, 0, fmt_254, 0 };
    static cilist io___137 = { 0, 0, 0, fmt_255, 0 };
    static cilist io___138 = { 0, 0, 0, fmt_259, 0 };
    static cilist io___139 = { 0, 0, 0, fmt_256, 0 };
    static cilist io___140 = { 0, 0, 0, fmt_350, 0 };
    static cilist io___141 = { 0, 0, 0, 0, 0 };
    static cilist io___142 = { 0, 0, 0, 0, 0 };
    static cilist io___144 = { 0, 0, 0, 0, 0 };
    static cilist io___145 = { 0, 0, 0, 0, 0 };
    static cilist io___146 = { 0, 0, 0, 0, 0 };
    static cilist io___147 = { 0, 0, 0, 0, 0 };
    static cilist io___148 = { 0, 0, 0, 0, 0 };


/* **** prompt for / read isat ***** */
L1:
    io___72.ciunit = io_1.wterm;
    s_wsfe(&io___72);
    e_wsfe();
    io___73.ciunit = io_1.rterm;
    s_rsle(&io___73);
    do_lio(&c__3, &c__1, (char *)&icon_1.isat, (ftnlen)sizeof(integer));
    e_rsle();
    if (icon_1.isat != 1 && icon_1.isat != 2) {
	goto L1;
    } else {
	--icon_1.isat;
    }
/* **** prompt for / read iopt ***** */
L2:
    if (icon_1.isat == 0) {
	io___74.ciunit = io_1.wterm;
	s_wsfe(&io___74);
	e_wsfe();
    } else {
	io___75.ciunit = io_1.wterm;
	s_wsfe(&io___75);
	e_wsfe();
    }
    io___76.ciunit = io_1.rterm;
    s_rsle(&io___76);
    do_lio(&c__3, &c__1, (char *)&icon_1.iopt, (ftnlen)sizeof(integer));
    e_rsle();
    if (icon_1.iopt != 1 && icon_1.iopt != 2) {
	goto L2;
    }
    *wetcon = icon_1.isat == 1 || icon_1.iopt == 1;
    if (icon_1.isat == 1) {
/* **** saturation curve option enabled ***** */
/* **** set univar and iplot ***** */
	icon_1.univar = 0;
	icon_1.iplot = 3;
/* **** prompt for / read noninc ***** */
L3:
	io___77.ciunit = io_1.wterm;
	s_wsfe(&io___77);
	e_wsfe();
	io___78.ciunit = io_1.rterm;
	s_rsle(&io___78);
	do_lio(&c__3, &c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	e_rsle();
	if (icon_1.noninc != 1 && icon_1.noninc != 2) {
	    goto L3;
	} else {
	    --icon_1.noninc;
	    if (icon_1.noninc == 0) {
/* **** prompt for / read state condition range along */
/* **** the saturation curve curve isopleth */
L444:
		io___79.ciunit = io_1.wterm;
		s_wsfe(&io___79);
		do_fio(&c__1, stvars_1.isosat + (icon_1.iopt - 1) * 10, (
			ftnlen)10);
		e_wsfe();
		io___80.ciunit = io_1.rterm;
		s_rsle(&io___80);
		do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(
			doublereal));
		e_rsle();
		if (grid_1.v2max > 373.917) {
		    io___81.ciunit = io_1.wterm;
		    s_wsfe(&io___81);
		    do_fio(&c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
			    doublereal));
		    e_wsfe();
		    goto L444;
		}
		if (grid_1.v2min == grid_1.v2max) {
		    grid_1.nv2 = 1;
		} else {
		    if (grid_1.v2inc == 0.) {
			io___82.ciunit = io_1.wterm;
			s_wsfe(&io___82);
			e_wsfe();
			goto L444;
		    }
		    fpnv2 = (grid_1.v2max - grid_1.v2min) / grid_1.v2inc + 1.;
		    grid_1.nv2 = i_dnnt(&fpnv2);
		    if ((d__1 = fpnv2 - (doublereal) grid_1.nv2, abs(d__1)) > 
			    1e-6) {
			io___84.ciunit = io_1.wterm;
			s_wsfe(&io___84);
			e_wsfe();
			goto L444;
		    }
		    if (grid_1.nv2 > 21) {
			io___85.ciunit = io_1.wterm;
			s_wsfe(&io___85);
			do_fio(&c__1, (char *)&grid_1.nv2, (ftnlen)sizeof(
				integer));
			do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer))
				;
			e_wsfe();
			goto L444;
		    }
		}
		grid_1.niso = 1;
		grid_1.isomin = 0.;
		grid_1.isomax = 0.;
		grid_1.isoinc = 0.;
	    } else {
/* **** prompt for / read [noninc] non-incremental state ***** */
/* **** condition points along saturation curve          ***** */
		io___86.ciunit = io_1.wterm;
		s_wsfe(&io___86);
		do_fio(&c__1, stvars_1.isosat + (icon_1.iopt - 1) * 10, (
			ftnlen)10);
		e_wsfe();
L4:
		io___87.ciunit = io_1.rterm;
		s_rsle(&io___87);
		do_lio(&c__5, &c__1, (char *)&grid_1.oddv1[icon_1.noninc - 1],
			 (ftnlen)sizeof(doublereal));
		e_rsle();
		if (grid_1.oddv1[icon_1.noninc - 1] != 0. && icon_1.noninc < 
			21) {
		    ++icon_1.noninc;
		    goto L4;
		}
		if (grid_1.oddv1[icon_1.noninc - 1] == 0.) {
		    --icon_1.noninc;
		} else {
		    io___88.ciunit = io_1.wterm;
		    s_wsfe(&io___88);
		    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
		    e_wsfe();
		}
	    }
	}
    } else {
/* **** saturation curve option curve disabled ***** */
	if (icon_1.iopt == 1) {
	    icon_1.univar = 0;
	} else {
/* **** prompt for / read univar ***** */
L5:
	    io___89.ciunit = io_1.wterm;
	    s_wsfe(&io___89);
	    e_wsfe();
	    io___90.ciunit = io_1.rterm;
	    s_rsfe(&io___90);
	    do_fio(&c__1, ans, (ftnlen)1);
	    e_rsfe();
	    if (*(unsigned char *)ans != 'y' && *(unsigned char *)ans != 'Y' 
		    && *(unsigned char *)ans != 'n' && *(unsigned char *)ans 
		    != 'N') {
		goto L5;
	    }
	    if (*(unsigned char *)ans == 'y' || *(unsigned char *)ans == 'Y') 
		    {
		icon_1.univar = 1;
	    } else {
		icon_1.univar = 0;
	    }
	}
	if (icon_1.univar == 0) {
/* **** univariant curve option disabled ***** */
/* **** prompt for / read iplot ***** */
L6:
	    io___92.ciunit = io_1.wterm;
	    s_wsfe(&io___92);
	    do_fio(&c__1, ptype1 + (icon_1.iopt - 1) * 6, (ftnlen)6);
	    do_fio(&c__1, ptype2 + (icon_1.iopt - 1), (ftnlen)1);
	    e_wsfe();
	    io___93.ciunit = io_1.rterm;
	    s_rsle(&io___93);
	    do_lio(&c__3, &c__1, (char *)&icon_1.iplot, (ftnlen)sizeof(
		    integer));
	    e_rsle();
	    if (icon_1.iplot != 1 && icon_1.iplot != 2) {
		goto L6;
	    }
/* **** prompt for / read noninc ***** */
L7:
	    io___94.ciunit = io_1.wterm;
	    s_wsfe(&io___94);
	    e_wsfe();
	    io___95.ciunit = io_1.rterm;
	    s_rsle(&io___95);
	    do_lio(&c__3, &c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(
		    integer));
	    e_rsle();
	    if (icon_1.noninc != 1 && icon_1.noninc != 2) {
		goto L7;
	    } else {
		--icon_1.noninc;
	    }
	    if (icon_1.noninc == 0) {
/* **** prompt for / read state condition ranges in one-phase region ***** */
L445:
		io___96.ciunit = io_1.wterm;
		s_wsfe(&io___96);
		do_fio(&c__1, stvars_1.isovar + (icon_1.iopt + (icon_1.iplot 
			<< 1) - 3) * 12, (ftnlen)12);
		e_wsfe();
		io___97.ciunit = io_1.rterm;
		s_rsle(&io___97);
		do_lio(&c__5, &c__1, (char *)&grid_1.isomin, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.isomax, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.isoinc, (ftnlen)sizeof(
			doublereal));
		e_rsle();
		if (grid_1.isomin == grid_1.isomax) {
		    grid_1.niso = 1;
		} else {
		    if (grid_1.isoinc == 0.) {
			io___98.ciunit = io_1.wterm;
			s_wsfe(&io___98);
			e_wsfe();
			goto L445;
		    }
		    fpniso = (grid_1.isomax - grid_1.isomin) / grid_1.isoinc 
			    + 1.;
		    grid_1.niso = i_dnnt(&fpniso);
		    if ((d__1 = fpniso - (doublereal) grid_1.niso, abs(d__1)) 
			    > 1e-6) {
			io___100.ciunit = io_1.wterm;
			s_wsfe(&io___100);
			e_wsfe();
			goto L445;
		    }
		}
		if (grid_1.niso > 11) {
		    io___101.ciunit = io_1.wterm;
		    s_wsfe(&io___101);
		    do_fio(&c__1, (char *)&grid_1.niso, (ftnlen)sizeof(
			    integer));
		    do_fio(&c__1, (char *)&c__11, (ftnlen)sizeof(integer));
		    e_wsfe();
		    goto L445;
		}
L446:
		io___102.ciunit = io_1.wterm;
		s_wsfe(&io___102);
		do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 
			1) - 3) * 10, (ftnlen)10);
		e_wsfe();
		io___103.ciunit = io_1.rterm;
		s_rsle(&io___103);
		do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(
			doublereal));
		e_rsle();
		if (grid_1.v2min == grid_1.v2max) {
		    grid_1.nv2 = 1;
		} else {
		    if (grid_1.v2inc == 0.) {
			io___104.ciunit = io_1.wterm;
			s_wsfe(&io___104);
			e_wsfe();
			goto L446;
		    }
		    fpnv2 = (grid_1.v2max - grid_1.v2min) / grid_1.v2inc + 1.;
		    grid_1.nv2 = i_dnnt(&fpnv2);
		    if ((d__1 = fpnv2 - (doublereal) grid_1.nv2, abs(d__1)) > 
			    1e-6) {
			io___105.ciunit = io_1.wterm;
			s_wsfe(&io___105);
			e_wsfe();
			goto L446;
		    }
		}
		if (grid_1.nv2 > 21) {
		    io___106.ciunit = io_1.wterm;
		    s_wsfe(&io___106);
		    do_fio(&c__1, (char *)&grid_1.nv2, (ftnlen)sizeof(integer)
			    );
		    do_fio(&c__1, (char *)&c__21, (ftnlen)sizeof(integer));
		    e_wsfe();
		    goto L446;
		}
	    } else {
/* **** prompt for / read [noninc] non-incremental state ***** */
/* **** condition points in the one-phase region        ***** */
		io___107.ciunit = io_1.wterm;
		s_wsfe(&io___107);
		do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (icon_1.iplot << 
			1) - 3) * 10, (ftnlen)10);
		do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 
			1) - 3) * 10, (ftnlen)10);
		e_wsfe();
L8:
		io___108.ciunit = io_1.rterm;
		s_rsle(&io___108);
		do_lio(&c__5, &c__1, (char *)&grid_1.oddv1[icon_1.noninc - 1],
			 (ftnlen)sizeof(doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.oddv2[icon_1.noninc - 1],
			 (ftnlen)sizeof(doublereal));
		e_rsle();
		if (grid_1.oddv1[icon_1.noninc - 1] != 0. && icon_1.noninc < 
			21) {
		    ++icon_1.noninc;
		    goto L8;
		}
		if (grid_1.oddv1[icon_1.noninc - 1] == 0.) {
		    --icon_1.noninc;
		} else {
		    io___109.ciunit = io_1.wterm;
		    s_wsfe(&io___109);
		    e_wsfe();
		}
	    }
	} else {
/* **** univariant curve option enabled ***** */
/* **** set noninc ***** */
	    icon_1.noninc = 0;
/* **** prompt for / read iplot ***** */
L9:
	    io___110.ciunit = io_1.wterm;
	    s_wsfe(&io___110);
	    e_wsfe();
	    io___111.ciunit = io_1.rterm;
	    s_rsle(&io___111);
	    do_lio(&c__3, &c__1, (char *)&icon_1.iplot, (ftnlen)sizeof(
		    integer));
	    e_rsle();
	    if (icon_1.iplot != 1 && icon_1.iplot != 2) {
		goto L9;
	    }
/* **** prompt for / read state condition ranges in one-phase region ***** */
L447:
	    io___112.ciunit = io_1.wterm;
	    s_wsfe(&io___112);
	    do_fio(&c__1, stvars_1.isovar + (icon_1.iopt + (icon_1.iplot << 1)
		     - 3) * 12, (ftnlen)12);
	    e_wsfe();
	    io___113.ciunit = io_1.rterm;
	    s_rsle(&io___113);
	    do_lio(&c__5, &c__1, (char *)&grid_1.isomin, (ftnlen)sizeof(
		    doublereal));
	    do_lio(&c__5, &c__1, (char *)&grid_1.isomax, (ftnlen)sizeof(
		    doublereal));
	    do_lio(&c__5, &c__1, (char *)&grid_1.isoinc, (ftnlen)sizeof(
		    doublereal));
	    e_rsle();
	    if (grid_1.isomin == grid_1.isomax) {
		grid_1.niso = 1;
	    } else {
		if (grid_1.isoinc == 0.) {
		    io___114.ciunit = io_1.wterm;
		    s_wsfe(&io___114);
		    e_wsfe();
		    goto L447;
		}
		fpniso = (grid_1.isomax - grid_1.isomin) / grid_1.isoinc + 1.;
		grid_1.niso = i_dnnt(&fpniso);
		if ((d__1 = fpniso - (doublereal) grid_1.niso, abs(d__1)) > 
			1e-6) {
		    io___115.ciunit = io_1.wterm;
		    s_wsfe(&io___115);
		    e_wsfe();
		    goto L447;
		}
	    }
	    if (grid_1.niso > 11) {
		io___116.ciunit = io_1.wterm;
		s_wsfe(&io___116);
		do_fio(&c__1, (char *)&grid_1.niso, (ftnlen)sizeof(integer));
		do_fio(&c__1, (char *)&c__11, (ftnlen)sizeof(integer));
		e_wsfe();
		goto L447;
	    }
L448:
	    io___117.ciunit = io_1.wterm;
	    s_wsfe(&io___117);
	    e_wsfe();
	    io___118.ciunit = io_1.rterm;
	    s_rsle(&io___118);
	    do_lio(&c__5, &c__1, (char *)&grid_1.kmin, (ftnlen)sizeof(
		    doublereal));
	    do_lio(&c__5, &c__1, (char *)&grid_1.kmax, (ftnlen)sizeof(
		    doublereal));
	    do_lio(&c__5, &c__1, (char *)&grid_1.kinc, (ftnlen)sizeof(
		    doublereal));
	    e_rsle();
	    if (grid_1.kmin == grid_1.kmax) {
		grid_1.nlogk = 1;
	    } else {
		if (grid_1.kinc == 0.) {
		    io___119.ciunit = io_1.wterm;
		    s_wsfe(&io___119);
		    e_wsfe();
		    goto L448;
		}
		fpnk = (grid_1.kmax - grid_1.kmin) / grid_1.kinc + 1.;
		grid_1.nlogk = i_dnnt(&fpnk);
		if ((d__1 = fpnk - (doublereal) grid_1.nlogk, abs(d__1)) > 
			1e-6) {
		    io___121.ciunit = io_1.wterm;
		    s_wsfe(&io___121);
		    e_wsfe();
		    goto L448;
		}
	    }
L449:
	    io___122.ciunit = io_1.wterm;
	    s_wsfe(&io___122);
	    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, tp + (icon_1.iplot - 1), (ftnlen)1);
	    do_fio(&c__1, tp + (icon_1.iplot - 1), (ftnlen)1);
	    e_wsfe();
	    io___123.ciunit = io_1.rterm;
	    s_rsle(&io___123);
	    do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(
		    doublereal));
	    do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
		    doublereal));
	    e_rsle();
	    if (grid_1.v2min < grid_1.v2max) {
		grid_1.v2inc = 0.;
	    } else {
		io___124.ciunit = io_1.wterm;
		s_wsfe(&io___124);
		do_fio(&c__1, tp + (icon_1.iplot - 1), (ftnlen)1);
		do_fio(&c__1, tp + (icon_1.iplot - 1), (ftnlen)1);
		e_wsfe();
		goto L449;
	    }
	}
    }
/* ************************************************************** */
/* ** variable assignments for distribution version */
    icon_1.uselvs = 1;
    icon_1.epseqn = 4;
    icon_1.geqn = 3;
/* ** select equation options for development version */
/*     CALL geteqn(useLVS,epseqn,geqn) */
/* ************************************************************** */
/* **** set unirun ****** */
    *unirun = icon_1.univar == 1;
/* **** write input parameters to new file if desired ***** */
L16:
    io___125.ciunit = io_1.wterm;
    s_wsfe(&io___125);
    e_wsfe();
    io___126.ciunit = io_1.rterm;
    s_rsfe(&io___126);
    do_fio(&c__1, ans, (ftnlen)1);
    e_rsfe();
    if (*(unsigned char *)ans != 'y' && *(unsigned char *)ans != 'Y' && *(
	    unsigned char *)ans != 'n' && *(unsigned char *)ans != 'N') {
	goto L16;
    }
    saveif_1.savecf = *(unsigned char *)ans == 'y' || *(unsigned char *)ans ==
	     'Y';
    if (saveif_1.savecf) {
L17:
	io___127.ciunit = io_1.wterm;
	s_wsfe(&io___127);
	e_wsfe();
	io___128.ciunit = io_1.rterm;
	s_rsfe(&io___128);
	do_fio(&c__1, fnames_1.namecf, (ftnlen)20);
	e_rsfe();
	if (! openf_(&io_1.wterm, &io_1.iconf, fnames_1.namecf, &c__2, &c__1, 
		&c__1, &c__132, (ftnlen)20)) {
	    goto L17;
	}
/* ***************************************************************** */
	io___129.ciunit = io_1.iconf;
	s_wsfe(&io___129);
	e_wsfe();
/* ** statement 250 for distribution versions */
/* ** statement 250 for development versions */
/* 250       FORMAT(' Line 1 (free format): isat, iopt, iplot,', */
/*    1            ' univar, noninc, useLVS, epseqn, geqn') */
/* ***************************************************************** */
	if (icon_1.noninc == 0) {
	    if (icon_1.isat == 1) {
		io___130.ciunit = io_1.iconf;
		s_wsfe(&io___130);
		e_wsfe();
		io___131.ciunit = io_1.iconf;
		s_wsfe(&io___131);
		e_wsfe();
	    } else {
		io___132.ciunit = io_1.iconf;
		s_wsfe(&io___132);
		e_wsfe();
	    }
	} else {
	    if (icon_1.isat == 1) {
		io___133.ciunit = io_1.iconf;
		s_wsfe(&io___133);
		i__1 = icon_1.noninc + 1;
		do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
		e_wsfe();
	    } else {
		io___134.ciunit = io_1.iconf;
		s_wsfe(&io___134);
		i__1 = icon_1.noninc + 1;
		do_fio(&c__1, (char *)&i__1, (ftnlen)sizeof(integer));
		e_wsfe();
	    }
	    io___135.ciunit = io_1.iconf;
	    s_wsfe(&io___135);
	    e_wsfe();
	}
	if (icon_1.isat == 0 && icon_1.noninc == 0) {
	    if (icon_1.univar == 0) {
		io___136.ciunit = io_1.iconf;
		s_wsfe(&io___136);
		e_wsfe();
	    } else {
		io___137.ciunit = io_1.iconf;
		s_wsfe(&io___137);
		e_wsfe();
		io___138.ciunit = io_1.iconf;
		s_wsfe(&io___138);
		e_wsfe();
	    }
	}
	if (icon_1.univar == 0) {
	    io___139.ciunit = io_1.iconf;
	    s_wsfe(&io___139);
	    e_wsfe();
	}
/* ************************************************************ */
/* ** WRITE statement for distribution version */
	io___140.ciunit = io_1.iconf;
	s_wsfe(&io___140);
	do_fio(&c__1, (char *)&icon_1.isat, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&icon_1.iopt, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&icon_1.iplot, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&icon_1.univar, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	e_wsfe();
/* ** WRITE statement for development version */
/*          WRITE(iconf,350) isat, iopt, iplot, univar, noninc, */
/*    1                      useLVS, epseqn, geqn */
/* 350       FORMAT(8(1x,i3)) */
/* ************************************************************ */
	if (icon_1.noninc == 0) {
	    if (icon_1.isat == 1) {
		io___141.ciunit = io_1.iconf;
		s_wsle(&io___141);
		do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(
			doublereal));
		e_wsle();
	    } else {
		io___142.ciunit = io_1.iconf;
		s_wsle(&io___142);
		do_lio(&c__5, &c__1, (char *)&grid_1.isomin, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.isomax, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.isoinc, (ftnlen)sizeof(
			doublereal));
		e_wsle();
	    }
	} else {
	    i__1 = icon_1.noninc;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		if (icon_1.isat == 1) {
		    io___144.ciunit = io_1.iconf;
		    s_wsle(&io___144);
		    do_lio(&c__5, &c__1, (char *)&grid_1.oddv1[i__ - 1], (
			    ftnlen)sizeof(doublereal));
		    e_wsle();
		} else {
		    io___145.ciunit = io_1.iconf;
		    s_wsle(&io___145);
		    do_lio(&c__5, &c__1, (char *)&grid_1.oddv1[i__ - 1], (
			    ftnlen)sizeof(doublereal));
		    do_lio(&c__5, &c__1, (char *)&grid_1.oddv2[i__ - 1], (
			    ftnlen)sizeof(doublereal));
		    e_wsle();
		}
/* L360: */
	    }
	}
	if (icon_1.isat == 0 && icon_1.noninc == 0) {
	    if (icon_1.univar == 0) {
		io___146.ciunit = io_1.iconf;
		s_wsle(&io___146);
		do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(
			doublereal));
		e_wsle();
	    } else {
		io___147.ciunit = io_1.iconf;
		s_wsle(&io___147);
		do_lio(&c__5, &c__1, (char *)&grid_1.kmin, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.kmax, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.kinc, (ftnlen)sizeof(
			doublereal));
		e_wsle();
		io___148.ciunit = io_1.iconf;
		s_wsle(&io___148);
		do_lio(&c__5, &c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(
			doublereal));
		do_lio(&c__5, &c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(
			doublereal));
		e_wsle();
	    }
	}
    }
    return 0;
} /* makecf_ */

/* *********************************************************************** */
/* ** geteqn - prompt for / read useLVS, epseqn, geqn. */
/* Subroutine */ int geteqn_(integer *uselvs, integer *epseqn, integer *geqn)
{
    /* Format strings */
    static char fmt_160[] = "(/,\002 would you like to use the Levelt Senger"
	    "s et al. (1983)\002,/,\002 equation of state for H2O in the crit"
	    "ical region (y/n)\002,/)";
    static char fmt_165[] = "(a1)";
    static char fmt_170[] = "(/,\002 specify dielectric option: \002,/,\002 "
	    "     1 = use Helgeson-Kirkham (1974) equation\002,/,\002      2 "
	    "= use Pitzer (1983) equation\002,/,\002      3 = use Uematsu-Fra"
	    "nck (1980) equation\002,/,\002      4 = use Johnson-Norton (1991"
	    ") equation\002,/,\002      5 = use Archer-Wang (1990) equatio"
	    "n\002,/)";
    static char fmt_180[] = "(/,\002 specify g-function option\002,/,\002   "
	    "   1 = use Tanger-Helgeson (1988) equation\002,/,\002      2 = u"
	    "se Shock et al. (in prep.) equation\002,/,\002      3 = use modi"
	    "fied Shock et al. equation\002,/)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsfe(cilist *), do_fio(integer *
	    , char *, ftnlen), e_rsfe(void), s_rsle(cilist *), do_lio(integer 
	    *, integer *, char *, ftnlen), e_rsle(void);

    /* Local variables */
    static char ans[1];

    /* Fortran I/O blocks */
    static cilist io___149 = { 0, 0, 0, fmt_160, 0 };
    static cilist io___150 = { 0, 0, 0, fmt_165, 0 };
    static cilist io___152 = { 0, 0, 0, fmt_170, 0 };
    static cilist io___153 = { 0, 0, 0, 0, 0 };
    static cilist io___154 = { 0, 0, 0, fmt_180, 0 };
    static cilist io___155 = { 0, 0, 0, 0, 0 };


L11:
    io___149.ciunit = io_1.wterm;
    s_wsfe(&io___149);
    e_wsfe();
    io___150.ciunit = io_1.rterm;
    s_rsfe(&io___150);
    do_fio(&c__1, ans, (ftnlen)1);
    e_rsfe();
    if (*(unsigned char *)ans != 'y' && *(unsigned char *)ans != 'Y' && *(
	    unsigned char *)ans != 'n' && *(unsigned char *)ans != 'N') {
	goto L11;
    }
    if (*(unsigned char *)ans == 'y' || *(unsigned char *)ans == 'Y') {
	*uselvs = 1;
    } else {
	*uselvs = 0;
    }
L12:
    io___152.ciunit = io_1.wterm;
    s_wsfe(&io___152);
    e_wsfe();
    io___153.ciunit = io_1.rterm;
    s_rsle(&io___153);
    do_lio(&c__3, &c__1, (char *)&(*epseqn), (ftnlen)sizeof(integer));
    e_rsle();
    if (*epseqn < 1 || *epseqn > 5) {
	goto L12;
    }
L13:
    io___154.ciunit = io_1.wterm;
    s_wsfe(&io___154);
    e_wsfe();
    io___155.ciunit = io_1.rterm;
    s_rsle(&io___155);
    do_lio(&c__3, &c__1, (char *)&(*geqn), (ftnlen)sizeof(integer));
    e_rsle();
    if (*geqn < 1 || *geqn > 3) {
	goto L13;
    }
    return 0;
} /* geteqn_ */

/* *********************************************************************** */
/* ** getrxn - Open and read an existing reaction (RXN) file or */
/* **          prompt for, create, [and save] a new reaction file. */
/* Subroutine */ int getrxn_(integer *nreac, logical *wetrxn)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 choose file option for specifying reacti"
	    "ons \002,/,\002      1 = use an existing reaction file\002,/,"
	    "\002      2 = build a new reaction file:\002,/)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsle(cilist *), do_lio(integer *
	    , integer *, char *, ftnlen), e_rsle(void);

    /* Local variables */
    static integer ifopt;
    extern /* Subroutine */ int readrf_(integer *, logical *), makerf_(
	    integer *, logical *);

    /* Fortran I/O blocks */
    static cilist io___156 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___157 = { 0, 0, 0, 0, 0 };


L1:
    io___156.ciunit = io_1.wterm;
    s_wsfe(&io___156);
    e_wsfe();
    io___157.ciunit = io_1.rterm;
    s_rsle(&io___157);
    do_lio(&c__3, &c__1, (char *)&ifopt, (ftnlen)sizeof(integer));
    e_rsle();
    if (ifopt != 1 && ifopt != 2) {
	goto L1;
    }
    if (ifopt == 1) {
	readrf_(nreac, wetrxn);
    } else {
	makerf_(nreac, wetrxn);
    }
    return 0;
} /* getrxn_ */

/* ******************************************************************* */
/* ** parse - If the first non-blank substring of the input character */
/* **         string (chrstr) represents a valid integer or */
/* **         non-exponential floating-point number, parse returns */
/* **         .TRUE. and converts this first substring into the */
/* **         corresponding real number (r8num), then transfers the */
/* **         second such subset into a CHAR*20 variable (name); */
/* **         otherwise, parse returns .FALSE. */
logical parse_(char *chrstr, doublereal *r8num, char *name__, ftnlen 
	chrstr_len, ftnlen name_len)
{
    /* System generated locals */
    integer i__1;
    logical ret_val;
    olist o__1;
    cllist cl__1;
    alist al__1;

    /* Builtin functions */
    integer i_len(char *, ftnlen), f_open(olist *), s_wsle(cilist *), do_lio(
	    integer *, integer *, char *, ftnlen), e_wsle(void), f_back(alist 
	    *), s_rsle(cilist *), e_rsle(void), f_clos(cllist *);

    /* Local variables */
    static integer i__, j;
    static logical deci, sign;
    static integer name1, nblank, chrlen, numlen;
    static char numstr[20];

    /* Fortran I/O blocks */
    static cilist io___167 = { 0, 0, 0, 0, 0 };
    static cilist io___168 = { 0, 0, 0, 0, 0 };


/* ** calculate length of chrstr *** */
    chrlen = i_len(chrstr, chrstr_len);
/* ** read through leading blanks *** */
    nblank = 0;
    i__1 = chrlen;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (*(unsigned char *)&chrstr[i__ - 1] == ' ') {
	    ++nblank;
	} else {
	    goto L2;
	}
/* L10: */
    }
/* ** initialize local variables *** */
L2:
    sign = FALSE_;
    deci = FALSE_;
/* ** extract numerical string (integer or */
/* ** non-exponentiated floating-point numbers only) */
    numlen = 0;
    i__1 = chrlen;
    for (i__ = nblank + 1; i__ <= i__1; ++i__) {
	if (*(unsigned char *)&chrstr[i__ - 1] == ' ') {
	    if (numlen == 1 && (sign || deci) || numlen == 2 && (sign && deci)
		    ) {
		ret_val = FALSE_;
		return ret_val;
	    } else {
/* **             valid integer or non-exponentiated floating-point */
/* **             number has been read; pad numerical string with blanks; */
/* **             read numerical numerical character string numstr into */
/* **             real*8 variable r8num; jump below to read in name. */
		ret_val = TRUE_;
		for (j = numlen + 1; j <= 20; ++j) {
		    *(unsigned char *)&numstr[j - 1] = ' ';
/* L30: */
		}
/* ** the following CHARACTER-to-DOUBLE PRECISION conversion is acceptable */
/* ** to most compilers ... but not all */
/*                    READ(numstr,*) r8num */
/* ** hence, portability considerations require use of the following */
/* ** procedure, which is equivalent and universally acceptable */
/* ** ... albeit ugly */
		o__1.oerr = 0;
		o__1.ounit = io2_1.tempf;
		o__1.ofnmlen = 8;
		o__1.ofnm = "zero.dat";
		o__1.orl = 0;
		o__1.osta = 0;
		o__1.oacc = 0;
		o__1.ofm = 0;
		o__1.oblnk = 0;
		f_open(&o__1);
		io___167.ciunit = io2_1.tempf;
		s_wsle(&io___167);
		do_lio(&c__9, &c__1, numstr, (ftnlen)20);
		e_wsle();
		al__1.aerr = 0;
		al__1.aunit = io2_1.tempf;
		f_back(&al__1);
		io___168.ciunit = io2_1.tempf;
		s_rsle(&io___168);
		do_lio(&c__5, &c__1, (char *)&(*r8num), (ftnlen)sizeof(
			doublereal));
		e_rsle();
		cl__1.cerr = 0;
		cl__1.cunit = io2_1.tempf;
		cl__1.csta = 0;
		f_clos(&cl__1);
		goto L3;
	    }
	}
	if (*(unsigned char *)&chrstr[i__ - 1] == '-' || *(unsigned char *)&
		chrstr[i__ - 1] == '+') {
	    if (! sign && numlen == 0) {
		sign = TRUE_;
		++numlen;
		*(unsigned char *)&numstr[numlen - 1] = *(unsigned char *)&
			chrstr[i__ - 1];
	    } else {
		ret_val = FALSE_;
		return ret_val;
	    }
	} else if (*(unsigned char *)&chrstr[i__ - 1] == '.') {
	    if (! deci) {
		deci = TRUE_;
		++numlen;
		*(unsigned char *)&numstr[numlen - 1] = *(unsigned char *)&
			chrstr[i__ - 1];
	    } else {
		ret_val = FALSE_;
		return ret_val;
	    }
	} else if (*(unsigned char *)&chrstr[i__ - 1] >= '0' && *(unsigned 
		char *)&chrstr[i__ - 1] <= '9') {
	    ++numlen;
	    *(unsigned char *)&numstr[numlen - 1] = *(unsigned char *)&chrstr[
		    i__ - 1];
	} else {
	    ret_val = FALSE_;
	    return ret_val;
	}
/* L20: */
    }
/* ** read through blanks that separate the */
/* ** number string from the name string */
L3:
    i__1 = chrlen;
    for (name1 = nblank + numlen + 1; name1 <= i__1; ++name1) {
	if (*(unsigned char *)&chrstr[name1 - 1] != ' ') {
	    goto L4;
	}
/* L40: */
    }
/* ** transfer non-blank substring beginning */
/* ** at chrstr(name1:name1) into name */
L4:
    j = 0;
    i__1 = chrlen;
    for (i__ = name1; i__ <= i__1; ++i__) {
	if (*(unsigned char *)&chrstr[i__ - 1] != ' ') {
	    ++j;
	    *(unsigned char *)&name__[j - 1] = *(unsigned char *)&chrstr[i__ 
		    - 1];
	} else {
	    if (j != 0) {
/* **             valid non-blank substring has been read into */
/* **             CHAR*20 variable name; pad name with blanks; */
/* **             return */
		goto L5;
	    } else {
		ret_val = FALSE_;
		return ret_val;
	    }
	}
/* L50: */
    }
L5:
    for (i__ = j + 1; i__ <= 20; ++i__) {
	*(unsigned char *)&name__[i__ - 1] = ' ';
/* L60: */
    }
    return ret_val;
} /* parse_ */

/* *********************************************************************** */
/* ** getout - Prompt for and read names for output files. */
/* Subroutine */ int getout_(void)
{
    /* Initialized data */

    static char suffx[4*8] = ".kxy" ".gxy" ".hxy" ".sxy" ".cxy" ".vxy" ".dxy" 
	    ".2xy";

    /* Format strings */
    static char fmt_10[] = "(/,\002 specify name for tabulated output file"
	    ":\002,/)";
    static char fmt_20[] = "(a20)";
    static char fmt_30[] = "(/,\002 specify option for x-y plot files:\002"
	    ",/,\002 logK, G, H, S, Cp, and V of reaction: \002,/,\002      1"
	    " = do not generate plot files \002,/,\002      2 = generate plot"
	    " files in generic format\002,/,\002      3 = generate plot files"
	    " in KaleidaGraph format\002,/)";
    static char fmt_35[] = "(/,\002 specify prefix for name of x-y plot fi"
	    "le;\002,/,\002 suffix will be \".uxy\"\002,/)";
    static char fmt_40[] = "(/,\002 specify prefix for names of x-y plot fil"
	    "es;\002,/,\002 suffix will be \".[d,[2],k,g,h,s,c,v]xy\"\002,/)";
    static char fmt_50[] = "(a16)";
    static char fmt_80[] = "(/,\002 specify prefix for names of x-y plot fil"
	    "es;\002,/,\002 suffix will be \"R#.axy\"\002,/)";
    static char fmt_90[] = "(a13)";
    static char fmt_100[] = "(/,\002 specify prefix for names of x-y plot fi"
	    "les;\002,/,\002 suffix will be \"R#.uxy\"\002,/)";
    static char fmt_110[] = "(/,\002 specify prefix for names of x-y plot fi"
	    "les;\002,/,\002 suffix will be \"R#.[d,[2],k,g,h,s,c,v]xy\"\002,"
	    "/)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsfe(cilist *), do_fio(integer *
	    , char *, ftnlen), e_rsfe(void), s_rsle(cilist *), do_lio(integer 
	    *, integer *, char *, ftnlen), e_rsle(void), i_len(char *, ftnlen)
	    ;
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__;
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);
    static char prefx1[16], prefx2[13];

    /* Fortran I/O blocks */
    static cilist io___171 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___172 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___173 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___174 = { 0, 0, 0, 0, 0 };
    static cilist io___175 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___176 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___177 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___180 = { 0, 0, 0, fmt_80, 0 };
    static cilist io___181 = { 0, 0, 0, fmt_90, 0 };
    static cilist io___183 = { 0, 0, 0, fmt_100, 0 };
    static cilist io___184 = { 0, 0, 0, fmt_90, 0 };
    static cilist io___185 = { 0, 0, 0, fmt_110, 0 };
    static cilist io___186 = { 0, 0, 0, fmt_90, 0 };


L1:
    io___171.ciunit = io_1.wterm;
    s_wsfe(&io___171);
    e_wsfe();
    io___172.ciunit = io_1.rterm;
    s_rsfe(&io___172);
    do_fio(&c__1, fnames_1.nametf, (ftnlen)20);
    e_rsfe();
    if (! openf_(&io_1.wterm, &io_1.tabf, fnames_1.nametf, &c__2, &c__1, &
	    c__1, &c__132, (ftnlen)20)) {
	goto L1;
    }
    if (icon_1.noninc > 0 && ! eq36_1.eq3run) {
	plottr_1.xyplot = 0;
	return 0;
    } else {
L2:
	io___173.ciunit = io_1.wterm;
	s_wsfe(&io___173);
	e_wsfe();
	io___174.ciunit = io_1.rterm;
	s_rsle(&io___174);
	do_lio(&c__3, &c__1, (char *)&plottr_1.xyplot, (ftnlen)sizeof(integer)
		);
	e_rsle();
	if (plottr_1.xyplot < 1 || plottr_1.xyplot > 3) {
	    goto L2;
	} else {
	    --plottr_1.xyplot;
	}
    }
    if (plottr_1.xyplot == 0) {
	return 0;
    }
    if (plottr_1.xyplot == 1) {
	if (eq36_1.eq3run) {
	    plottr_1.nplots = 8;
	} else {
	    if (icon_1.univar == 1) {
		plottr_1.nplots = 1;
	    } else {
		plottr_1.nplots = icon_1.isat + 7;
	    }
	}
	if (icon_1.univar == 1) {
	    io___175.ciunit = io_1.wterm;
	    s_wsfe(&io___175);
	    e_wsfe();
	} else {
	    io___176.ciunit = io_1.wterm;
	    s_wsfe(&io___176);
	    e_wsfe();
	}
	io___177.ciunit = io_1.rterm;
	s_rsfe(&io___177);
	do_fio(&c__1, prefx1, (ftnlen)16);
	e_rsfe();
	i__1 = i_len(prefx1, (ftnlen)16);
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (*(unsigned char *)&prefx1[i__ - 1] == ' ') {
		plottr_1.end = i__ - 1;
		goto L65;
	    }
/* L60: */
	}
L65:
	if (icon_1.univar == 1) {
	    s_copy(fnames_1.namepf, prefx1, plottr_1.end, plottr_1.end);
	    i__1 = plottr_1.end;
	    s_copy(fnames_1.namepf + i__1, ".uxy", plottr_1.end + 4 - i__1, (
		    ftnlen)4);
	} else {
	    i__1 = plottr_1.nplots;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		s_copy(fnames_1.namepf + (i__ - 1) * 20, prefx1, plottr_1.end,
			 plottr_1.end);
		i__2 = plottr_1.end;
		s_copy(fnames_1.namepf + ((i__ - 1) * 20 + i__2), suffx + (
			i__ - 1 << 2), plottr_1.end + 4 - i__2, (ftnlen)4);
/* L70: */
	    }
	}
	return 0;
    }
/* ** xyplot = 2 */
    if (icon_1.isat == 1 || eq36_1.eq3run) {
	plottr_1.nplots = 1;
	io___180.ciunit = io_1.wterm;
	s_wsfe(&io___180);
	e_wsfe();
	io___181.ciunit = io_1.rterm;
	s_rsfe(&io___181);
	do_fio(&c__1, prefx2, (ftnlen)13);
	e_rsfe();
    } else {
	if (icon_1.univar == 1) {
	    plottr_1.nplots = 1;
	    io___183.ciunit = io_1.wterm;
	    s_wsfe(&io___183);
	    e_wsfe();
	    io___184.ciunit = io_1.rterm;
	    s_rsfe(&io___184);
	    do_fio(&c__1, prefx2, (ftnlen)13);
	    e_rsfe();
	} else {
	    plottr_1.nplots = 7;
	    io___185.ciunit = io_1.wterm;
	    s_wsfe(&io___185);
	    e_wsfe();
	    io___186.ciunit = io_1.rterm;
	    s_rsfe(&io___186);
	    do_fio(&c__1, prefx2, (ftnlen)13);
	    e_rsfe();
	}
    }
    i__1 = i_len(prefx2, (ftnlen)13);
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (*(unsigned char *)&prefx2[i__ - 1] == ' ') {
	    plottr_1.end = i__ - 1;
	    goto L125;
	}
/* L120: */
    }
L125:
    if (icon_1.isat == 1 || eq36_1.eq3run) {
	s_copy(fnames_1.namepf, prefx2, plottr_1.end, plottr_1.end);
	i__1 = plottr_1.end;
	s_copy(fnames_1.namepf + i__1, "R01", plottr_1.end + 3 - i__1, (
		ftnlen)3);
	i__1 = plottr_1.end + 3;
	s_copy(fnames_1.namepf + i__1, ".axy", plottr_1.end + 7 - i__1, (
		ftnlen)4);
	return 0;
    }
    if (icon_1.univar == 1) {
	s_copy(fnames_1.namepf, prefx2, plottr_1.end, plottr_1.end);
	i__1 = plottr_1.end;
	s_copy(fnames_1.namepf + i__1, "R01", plottr_1.end + 3 - i__1, (
		ftnlen)3);
	i__1 = plottr_1.end + 3;
	s_copy(fnames_1.namepf + i__1, ".uxy", plottr_1.end + 7 - i__1, (
		ftnlen)4);
	return 0;
    }
    i__1 = plottr_1.nplots;
    for (i__ = 1; i__ <= i__1; ++i__) {
	s_copy(fnames_1.namepf + (i__ - 1) * 20, prefx2, plottr_1.end, 
		plottr_1.end);
	i__2 = plottr_1.end;
	s_copy(fnames_1.namepf + ((i__ - 1) * 20 + i__2), "R01", plottr_1.end 
		+ 3 - i__2, (ftnlen)3);
	i__2 = plottr_1.end + 3;
	s_copy(fnames_1.namepf + ((i__ - 1) * 20 + i__2), suffx + (i__ - 1 << 
		2), plottr_1.end + 7 - i__2, (ftnlen)4);
/* L130: */
    }
    return 0;
} /* getout_ */

/* *********************************************************************** */
/* ** getH2O - Calculate/store requisite H2O properties over the */
/* **          user-specified state condition grid. */
/* Subroutine */ int geth2o_(logical *unirun)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,0,0,0,0 };
    static doublereal states[4] = { 0.,0.,0.,0. };

    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    static logical error;
    static doublereal props[46];
    extern /* Subroutine */ int oddh2o_(void), oneh2o_(void), h2ostd_(
	    doublereal *, doublereal *), twoh2o_(void);

    specs[7] = icon_1.uselvs;
    specs[8] = icon_1.epseqn;
/* **************************************************************** */
/* ** assignment of [Z,Y]PrTr to Johnson-Norton (1991) */
/* ** values for distribution version */
    refval_1.zprtr = -.01278034682;
    refval_1.yprtr = -5.798650444e-5;
/* ** set ZPrTr and YPrTR per espeqn value for development version */
/*     CALL seteps(Tref-273.15d0,Pref,epseqn,ZPrTr,YPrTr) */
/* **************************************************************** */
/* **** calculate H2O properties at standard state of 25 degC, 1 bar */
    states[0] = refval_1.tref - 273.15;
    states[1] = refval_1.pref;
    specs[5] = 0;
    specs[6] = 2;
    h2o92_(specs, states, props, &error);
    h2ostd_(states, props);
    if (*unirun) {
	return 0;
    }
    if (icon_1.noninc > 0) {
	oddh2o_();
	return 0;
    }
    if (icon_1.isat == 0) {
	oneh2o_();
    } else {
	twoh2o_();
    }
    return 0;
} /* geth2o_ */

/* *********************************************************************** */
/* ** oddH2O - Calculate/store requisite H2O properties over the */
/* **          user-specified set of state conditions. */
/* Subroutine */ int oddh2o_(void)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,0,0,0,0 };
    static doublereal states[4] = { 0.,0.,0.,0. };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    static integer iodd;
    static logical error;
    static doublereal props[46];
    extern /* Subroutine */ int h2osav_(integer *, integer *, doublereal *, 
	    doublereal *);

    specs[5] = icon_1.isat;
    specs[6] = icon_1.iopt;
    specs[7] = icon_1.uselvs;
    specs[8] = icon_1.epseqn;
    i__1 = icon_1.noninc;
    for (iodd = 1; iodd <= i__1; ++iodd) {
	states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		grid_1.oddv1[iodd - 1];
	if (icon_1.isat == 0) {
	    states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1]
		     = grid_1.oddv2[iodd - 1];
	}
	h2o92_(specs, states, props, &error);
	badtd_1.h2oerr[iodd - 1] = error;
	if (! error) {
	    badtd_1.lvdome[iodd - 1] = icon_1.iplot != 3 && specs[5] == 1 && !
		     eq36_1.eq3run;
	    if (badtd_1.lvdome[iodd - 1]) {
		specs[5] = 0;
	    } else {
		if (eq36_1.eq3run && specs[5] == 1) {
		    icon_1.isat = 1;
		    specs[6] = 1;
		}
		h2osav_(&iodd, &c__1, states, props);
	    }
	}
/* L30: */
    }
    if (eq36_1.eq3run) {
	icon_1.isat = 0;
    }
    return 0;
} /* oddh2o_ */

/* *********************************************************************** */
/* ** oneH2O - Calculate/store requisite H2O properties over the */
/* **          user-specified state condition grid in the */
/* **          one-phase region. */
/* Subroutine */ int oneh2o_(void)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,0,0,0,0 };
    static doublereal states[4] = { 0.,0.,0.,0. };

    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer inc, iso;
    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    static logical error;
    static doublereal props[46];
    extern /* Subroutine */ int h2osav_(integer *, integer *, doublereal *, 
	    doublereal *);

    specs[5] = icon_1.isat;
    specs[6] = icon_1.iopt;
    specs[7] = icon_1.uselvs;
    specs[8] = icon_1.epseqn;
    i__1 = grid_1.niso;
    for (iso = 1; iso <= i__1; ++iso) {
	states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		grid_1.isomin + (iso - 1) * grid_1.isoinc;
	i__2 = grid_1.nv2;
	for (inc = 1; inc <= i__2; ++inc) {
	    specs[5] = icon_1.isat;
	    specs[6] = icon_1.iopt;
	    states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1]
		     = grid_1.v2min + (inc - 1) * grid_1.v2inc;
	    h2o92_(specs, states, props, &error);
	    badtd_1.h2oerr[inc + iso * 21 - 22] = error;
	    if (error) {
		states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] 
			- 1] = grid_1.isomin + (iso - 1) * grid_1.isoinc;
	    } else {
		badtd_1.lvdome[inc + iso * 21 - 22] = specs[5] == 1;
		if (badtd_1.lvdome[inc + iso * 21 - 22]) {
		    specs[5] = 0;
		    states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) 
			    - 3] - 1] = grid_1.isomin + (iso - 1) * 
			    grid_1.isoinc;
		} else {
		    h2osav_(&inc, &iso, states, props);
		}
	    }
/* L10: */
	}
    }
    return 0;
} /* oneh2o_ */

/* *********************************************************************** */
/* ** twoH2O - Calculate/store requisite H2O properties over the */
/* **          user-specified state condition grid along the */
/* **          vaporization boundary. */
/* Subroutine */ int twoh2o_(void)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,0,0,0,0 };
    static doublereal states[4] = { 0.,0.,0.,0. };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer inc;
    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    static logical error;
    static doublereal props[46];
    extern /* Subroutine */ int h2osav_(integer *, integer *, doublereal *, 
	    doublereal *);

    specs[5] = icon_1.isat;
    specs[6] = icon_1.iopt;
    specs[7] = icon_1.uselvs;
    specs[8] = icon_1.epseqn;
    lv1b_1.lv1bar = icon_1.iopt == 1 && grid_1.v2min <= 99.6324;
    i__1 = grid_1.nv2;
    for (inc = 1; inc <= i__1; ++inc) {
	if (inc == 1 && grid_1.v2min == 0.) {
	    states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1]
		     = satend_1.satmin[icon_1.iopt - 1];
	} else {
	    states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1]
		     = grid_1.v2min + (inc - 1) * grid_1.v2inc;
	}
	if (lv1b_1.lv1bar && states[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3] - 1] <= 99.6324) {
	    icon_1.isat = 0;
	    specs[5] = 0;
	    specs[6] = 2;
	    states[1] = refval_1.pref;
	} else {
	    if (lv1b_1.lv1bar) {
		icon_1.isat = 1;
	    }
	    specs[5] = icon_1.isat;
	    specs[6] = icon_1.iopt;
	}
	h2o92_(specs, states, props, &error);
	badtd_1.h2oerr[inc - 1] = error;
	if (! error) {
	    h2osav_(&inc, &c__1, states, props);
	}
/* L10: */
    }
    return 0;
} /* twoh2o_ */

/* *********************************************************************** */
/* ** seteps - Set ZPrTr and YPrTR per espeqn value. */
/* Subroutine */ int seteps_(doublereal *tcref, doublereal *pref, integer *
	epseqn, doublereal *zprtr, doublereal *yprtr)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,2,0,0,0 };

    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    static logical error;
    static doublereal props[46], states[4];

    specs[8] = *epseqn;
    states[0] = *tcref;
    states[1] = *pref;
    states[2] = 0.;
    h2o92_(specs, states, props, &error);
    *zprtr = props[36];
    *yprtr = props[38];
    return 0;
} /* seteps_ */

/* *********************************************************************** */
/* ** H2Ostd - Archive requisite H2O properties for the */
/* **          standard state of 25 degC, 1 bar. */
/* Subroutine */ int h2ostd_(doublereal *states, doublereal *props)
{
    /* Initialized data */

    static integer g = 3;
    static integer s = 5;
    static integer h__ = 9;
    static integer cp = 13;
    static integer al = 17;
    static integer be = 19;
    static integer z__ = 37;
    static integer y = 39;
    static integer q = 41;
    static integer daldt = 43;
    static integer x = 45;

    /* Parameter adjustments */
    --props;
    --states;

    /* Function Body */
/* ** archive requisite properties *** */
    h2oss_1.dwss = states[3];
    h2oss_1.vwss = refval_1.mwh2o / states[3];
    h2oss_1.bewss = props[be];
    h2oss_1.alwss = props[al];
    h2oss_1.dalwss = props[daldt];
    h2oss_1.swss = props[s];
    h2oss_1.cpwss = props[cp];
    h2oss_1.hwss = props[h__];
    h2oss_1.gwss = props[g];
    h2oss_1.zwss = props[z__];
    h2oss_1.qwss = props[q];
    h2oss_1.ywss = props[y];
    h2oss_1.xwss = props[x];
    return 0;
} /* h2ostd_ */

/* *********************************************************************** */
/* ** H2Osav - Archive requisite H2O properties over the */
/* **          user-specified state condition grid. */
/* Subroutine */ int h2osav_(integer *row, integer *col, doublereal *states, 
	doublereal *props)
{
    /* Initialized data */

    static integer g = 3;
    static integer s = 5;
    static integer h__ = 9;
    static integer cp = 13;
    static integer al = 17;
    static integer be = 19;
    static integer z__ = 37;
    static integer y = 39;
    static integer q = 41;
    static integer daldt = 43;
    static integer x = 45;

    /* Parameter adjustments */
    --props;
    --states;

    /* Function Body */
/* ** archive dependent state variables *** */
    if (icon_1.isat == 1) {
	if (eq36_1.eq3run) {
	    h2ogrd_1.dsvar[*row + *col * 21 - 22] = states[4];
	} else {
	    h2ogrd_1.dsvar[*row + *col * 21 - 22] = states[2 / icon_1.iopt];
	}
    } else {
	if (lv1b_1.lv1bar) {
	    h2ogrd_1.dsvar[*row + *col * 21 - 22] = states[2];
	} else {
	    h2ogrd_1.dsvar[*row + *col * 21 - 22] = states[tpdmap_1.mapv3[
		    icon_1.iopt + (icon_1.iplot << 1) - 3]];
	}
    }
/* ** archive requisite properties *** */
    h2ogrd_1.vw[*row + *col * 21 - 22] = refval_1.mwh2o / states[icon_1.isat 
	    + 3];
    h2ogrd_1.bew[*row + *col * 21 - 22] = props[be + icon_1.isat];
    h2ogrd_1.alw[*row + *col * 21 - 22] = props[al + icon_1.isat];
    h2ogrd_1.dalw[*row + *col * 21 - 22] = props[daldt + icon_1.isat];
    h2ogrd_1.sw[*row + *col * 21 - 22] = props[s + icon_1.isat];
    h2ogrd_1.cpw[*row + *col * 21 - 22] = props[cp + icon_1.isat];
    h2ogrd_1.hw[*row + *col * 21 - 22] = props[h__ + icon_1.isat];
    h2ogrd_1.gw[*row + *col * 21 - 22] = props[g + icon_1.isat];
    h2ogrd_1.zw[*row + *col * 21 - 22] = props[z__ + icon_1.isat];
    h2ogrd_1.qw[*row + *col * 21 - 22] = props[q + icon_1.isat];
    h2ogrd_1.yw[*row + *col * 21 - 22] = props[y + icon_1.isat];
    h2ogrd_1.xw[*row + *col * 21 - 22] = props[x + icon_1.isat];
    return 0;
} /* h2osav_ */

/* *********************************************************************** */
/* ** getmgi - Read standard state properties, equation of state */
/* **          parameters, and heat capacity coefficients for all */
/* **          mineral, gas, and aqueous species in the current */
/* **          reaction. */
/* Subroutine */ int getmgi_(integer *ireac)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;
    extern /* Subroutine */ int getgas_(integer *, integer *), getmin_(
	    integer *, integer *), getaqs_(integer *, integer *);

/* ** retrieve thermodynamic data for minerals */
    i__1 = reac2_1.nm[*ireac - 1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	getmin_(&i__, &reac2_1.rec1m[*ireac + i__ * 10 - 11]);
/* L10: */
    }
/* ** retrieve thermodynamic data for gases */
    i__1 = reac2_1.ng[*ireac - 1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	getgas_(&i__, &reac2_1.rec1g[*ireac + i__ * 10 - 11]);
/* L20: */
    }
/* ** retrieve thermodynamic data for aqueous species */
    i__1 = reac2_1.na[*ireac - 1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	getaqs_(&i__, &reac2_1.rec1a[*ireac + i__ * 10 - 11]);
/* L30: */
    }
    return 0;
} /* getmgi_ */

/* *********************************************************************** */
/* ** getmin - Read, from dprons.dat or an analogous database (starting */
/* **          at record rec1), standard state parameters for the i[th] */
/* **          one-phase mineral species in the current reaction; */
/* **          set ntran(i) to zero. */
/* Subroutine */ int getmin_(integer *i__, integer *rec1)
{
    /* Format strings */
    static char fmt_10[] = "(1x,a20,a30)";
    static char fmt_20[] = "(4x,2(2x,f12.1),2(2x,f8.3))";
    static char fmt_30[] = "(4x,3(2x,f12.6))";
    static char fmt_40[] = "(8x,f7.2)";
    static char fmt_50[] = "(4x,3(2x,f12.6),2x,f7.2,2x,f8.1,2(2x,f10.3))";

    /* Builtin functions */
    integer s_rdfe(cilist *), do_fio(integer *, char *, ftnlen), e_rdfe(void);

    /* Local variables */
    static integer j;

    /* Fortran I/O blocks */
    static cilist io___258 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___259 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___260 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___262 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___263 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___264 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___265 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___266 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___267 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___268 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___269 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___270 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___271 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___272 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___273 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___274 = { 0, 0, 0, fmt_40, 0 };


    if (*rec1 < rlimit_1.rec1m2) {
	minref_1.ntran[*i__ - 1] = 0;
	goto L1;
    }
    if (*rec1 < rlimit_1.rec1m3) {
	minref_1.ntran[*i__ - 1] = 1;
	goto L1;
    }
    if (*rec1 < rlimit_1.rec1m4) {
	minref_1.ntran[*i__ - 1] = 2;
	goto L1;
    }
    minref_1.ntran[*i__ - 1] = 3;
L1:
    io___258.ciunit = io_1.pronf;
    io___258.cirec = *rec1;
    s_rdfe(&io___258);
    do_fio(&c__1, mnames_1.mname + (*i__ - 1) * 20, (ftnlen)20);
    do_fio(&c__1, mnames_1.mform + (*i__ - 1) * 30, (ftnlen)30);
    e_rdfe();
    io___259.ciunit = io_1.pronf;
    io___259.cirec = *rec1 + 3;
    s_rdfe(&io___259);
    do_fio(&c__1, (char *)&minref_1.gfmin[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&minref_1.hfmin[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&minref_1.sprtrm[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&minref_1.vprtrm[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    e_rdfe();
    if (minref_1.ntran[*i__ - 1] == 0) {
	io___260.ciunit = io_1.pronf;
	io___260.cirec = *rec1 + 4;
	s_rdfe(&io___260);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk1[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk1[*i__ * 3 - 2] *= .001;
	minref_1.mk1[*i__ * 3 - 1] *= 1e5;
	io___262.ciunit = io_1.pronf;
	io___262.cirec = *rec1 + 5;
	s_rdfe(&io___262);
	do_fio(&c__1, (char *)&minref_1.tmaxm[*i__ - 1], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
	return 0;
    }
    if (minref_1.ntran[*i__ - 1] == 1) {
	io___263.ciunit = io_1.pronf;
	io___263.cirec = *rec1 + 4;
	s_rdfe(&io___263);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk1[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	do_fio(&c__1, (char *)&minref_1.ttran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.htran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.vtran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.dpdttr[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk1[*i__ * 3 - 2] *= .001;
	minref_1.mk1[*i__ * 3 - 1] *= 1e5;
	io___264.ciunit = io_1.pronf;
	io___264.cirec = *rec1 + 5;
	s_rdfe(&io___264);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk2[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk2[*i__ * 3 - 2] *= .001;
	minref_1.mk2[*i__ * 3 - 1] *= 1e5;
	io___265.ciunit = io_1.pronf;
	io___265.cirec = *rec1 + 6;
	s_rdfe(&io___265);
	do_fio(&c__1, (char *)&minref_1.tmaxm[*i__ - 1], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
	return 0;
    }
    if (minref_1.ntran[*i__ - 1] == 2) {
	io___266.ciunit = io_1.pronf;
	io___266.cirec = *rec1 + 4;
	s_rdfe(&io___266);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk1[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	do_fio(&c__1, (char *)&minref_1.ttran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.htran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.vtran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.dpdttr[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk1[*i__ * 3 - 2] *= .001;
	minref_1.mk1[*i__ * 3 - 1] *= 1e5;
	io___267.ciunit = io_1.pronf;
	io___267.cirec = *rec1 + 5;
	s_rdfe(&io___267);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk2[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	do_fio(&c__1, (char *)&minref_1.ttran[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.htran[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.vtran[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.dpdttr[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk2[*i__ * 3 - 2] *= .001;
	minref_1.mk2[*i__ * 3 - 1] *= 1e5;
	io___268.ciunit = io_1.pronf;
	io___268.cirec = *rec1 + 6;
	s_rdfe(&io___268);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk3[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk3[*i__ * 3 - 2] *= .001;
	minref_1.mk3[*i__ * 3 - 1] *= 1e5;
	io___269.ciunit = io_1.pronf;
	io___269.cirec = *rec1 + 7;
	s_rdfe(&io___269);
	do_fio(&c__1, (char *)&minref_1.tmaxm[*i__ - 1], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
	return 0;
    }
    if (minref_1.ntran[*i__ - 1] == 3) {
	io___270.ciunit = io_1.pronf;
	io___270.cirec = *rec1 + 4;
	s_rdfe(&io___270);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk1[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	do_fio(&c__1, (char *)&minref_1.ttran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.htran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.vtran[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.dpdttr[*i__ * 3 - 3], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk1[*i__ * 3 - 2] *= .001;
	minref_1.mk1[*i__ * 3 - 1] *= 1e5;
	io___271.ciunit = io_1.pronf;
	io___271.cirec = *rec1 + 5;
	s_rdfe(&io___271);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk2[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	do_fio(&c__1, (char *)&minref_1.ttran[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.htran[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.vtran[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.dpdttr[*i__ * 3 - 2], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk2[*i__ * 3 - 2] *= .001;
	minref_1.mk2[*i__ * 3 - 1] *= 1e5;
	io___272.ciunit = io_1.pronf;
	io___272.cirec = *rec1 + 6;
	s_rdfe(&io___272);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk3[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	do_fio(&c__1, (char *)&minref_1.ttran[*i__ * 3 - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.htran[*i__ * 3 - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.vtran[*i__ * 3 - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, (char *)&minref_1.dpdttr[*i__ * 3 - 1], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk3[*i__ * 3 - 2] *= .001;
	minref_1.mk3[*i__ * 3 - 1] *= 1e5;
	io___273.ciunit = io_1.pronf;
	io___273.cirec = *rec1 + 7;
	s_rdfe(&io___273);
	for (j = 1; j <= 3; ++j) {
	    do_fio(&c__1, (char *)&minref_1.mk4[j + *i__ * 3 - 4], (ftnlen)
		    sizeof(doublereal));
	}
	e_rdfe();
/* ****      adjust magnitude for Cp coeffs */
	minref_1.mk4[*i__ * 3 - 2] *= .001;
	minref_1.mk4[*i__ * 3 - 1] *= 1e5;
	io___274.ciunit = io_1.pronf;
	io___274.cirec = *rec1 + 8;
	s_rdfe(&io___274);
	do_fio(&c__1, (char *)&minref_1.tmaxm[*i__ - 1], (ftnlen)sizeof(
		doublereal));
	e_rdfe();
	return 0;
    }
    return 0;
} /* getmin_ */

/* *********************************************************************** */
/* ** getgas - Read, from dprons.dat or an analogous database (starting */
/* **          at record rec1), standard state parameters for the i[th] */
/* **          gas species in the current reaction. */
/* Subroutine */ int getgas_(integer *i__, integer *rec1)
{
    /* Format strings */
    static char fmt_20[] = "(1x,a20,a30)";
    static char fmt_30[] = "(4x,2(2x,f12.1),2(2x,f8.3))";
    static char fmt_40[] = "(4x,3(2x,f12.6))";
    static char fmt_50[] = "(8x,f7.2)";

    /* Builtin functions */
    integer s_rdfe(cilist *), do_fio(integer *, char *, ftnlen), e_rdfe(void);

    /* Fortran I/O blocks */
    static cilist io___275 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___276 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___277 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___278 = { 0, 0, 0, fmt_50, 0 };


    io___275.ciunit = io_1.pronf;
    io___275.cirec = *rec1;
    s_rdfe(&io___275);
    do_fio(&c__1, gnames_1.gname + (*i__ - 1) * 20, (ftnlen)20);
    do_fio(&c__1, gnames_1.gform + (*i__ - 1) * 30, (ftnlen)30);
    e_rdfe();
    io___276.ciunit = io_1.pronf;
    io___276.cirec = *rec1 + 3;
    s_rdfe(&io___276);
    do_fio(&c__1, (char *)&gasref_1.gfgas[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&gasref_1.hfgas[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&gasref_1.sprtrg[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&gasref_1.vprtrg[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    e_rdfe();
    io___277.ciunit = io_1.pronf;
    io___277.cirec = *rec1 + 4;
    s_rdfe(&io___277);
    do_fio(&c__1, (char *)&gasref_1.mkg[*i__ * 3 - 3], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&gasref_1.mkg[*i__ * 3 - 2], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&gasref_1.mkg[*i__ * 3 - 1], (ftnlen)sizeof(
	    doublereal));
    e_rdfe();
    io___278.ciunit = io_1.pronf;
    io___278.cirec = *rec1 + 5;
    s_rdfe(&io___278);
    do_fio(&c__1, (char *)&gasref_1.tmaxg[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    e_rdfe();
/* **** adjust magnitude for Cp coeffs */
    gasref_1.mkg[*i__ * 3 - 2] *= .001;
    gasref_1.mkg[*i__ * 3 - 1] *= 1e5;
    return 0;
} /* getgas_ */

/* *********************************************************************** */
/* ** getaqs - Read, from dprons.dat or an analogous database (starting */
/* **          at record rec1), standard state parameters for the i[th] */
/* **          aqueous species in the current reaction. */
/* Subroutine */ int getaqs_(integer *i__, integer *rec1)
{
    /* Format strings */
    static char fmt_20[] = "(1x,a20,a30)";
    static char fmt_30[] = "(4x,2(2x,f10.0),4x,f8.3)";
    static char fmt_40[] = "(4x,4(1x,f10.4,1x))";
    static char fmt_50[] = "(4x,3(2x,f8.4,2x),9x,f3.0)";

    /* Builtin functions */
    integer s_rdfe(cilist *), do_fio(integer *, char *, ftnlen), e_rdfe(void);

    /* Fortran I/O blocks */
    static cilist io___279 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___280 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___281 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___282 = { 0, 0, 0, fmt_50, 0 };


    io___279.ciunit = io_1.pronf;
    io___279.cirec = *rec1;
    s_rdfe(&io___279);
    do_fio(&c__1, anames_1.aname + (*i__ - 1) * 20, (ftnlen)20);
    do_fio(&c__1, anames_1.aform + (*i__ - 1) * 30, (ftnlen)30);
    e_rdfe();
    io___280.ciunit = io_1.pronf;
    io___280.cirec = *rec1 + 3;
    s_rdfe(&io___280);
    do_fio(&c__1, (char *)&aqsref_1.gfaqs[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.hfaqs[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.sprtra[*i__ - 1], (ftnlen)sizeof(
	    doublereal));
    e_rdfe();
    io___281.ciunit = io_1.pronf;
    io___281.cirec = *rec1 + 4;
    s_rdfe(&io___281);
    do_fio(&c__1, (char *)&aqsref_1.a[(*i__ << 2) - 4], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.a[(*i__ << 2) - 3], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.a[(*i__ << 2) - 2], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.a[(*i__ << 2) - 1], (ftnlen)sizeof(
	    doublereal));
    e_rdfe();
    io___282.ciunit = io_1.pronf;
    io___282.cirec = *rec1 + 5;
    s_rdfe(&io___282);
    do_fio(&c__1, (char *)&aqsref_1.c__[(*i__ << 1) - 2], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.c__[(*i__ << 1) - 1], (ftnlen)sizeof(
	    doublereal));
    do_fio(&c__1, (char *)&aqsref_1.wref[*i__ - 1], (ftnlen)sizeof(doublereal)
	    );
    do_fio(&c__1, (char *)&aqsref_1.chg[*i__ - 1], (ftnlen)sizeof(doublereal))
	    ;
    e_rdfe();
/* **** adjust magnitude for e-o-s coefficients and omega */
    aqsref_1.a[(*i__ << 2) - 4] *= .1;
    aqsref_1.a[(*i__ << 2) - 3] *= 100.;
    aqsref_1.a[(*i__ << 2) - 1] *= 1e4;
    aqsref_1.c__[(*i__ << 1) - 1] *= 1e4;
    aqsref_1.wref[*i__ - 1] *= 1e5;
    return 0;
} /* getaqs_ */

/* *********************************************************************** */
/* ** runrxn - Calculate the standard molal thermodynamic properties of */
/* **          the i[th] reaction over the range of user-specified state */
/* **          conditions. */
/* Subroutine */ int runrxn_(integer *i__, logical *wetrun)
{
    extern /* Subroutine */ int runodd_(integer *), rungrd_(integer *, 
	    logical *), rununi_(integer *);

    if (icon_1.univar == 1) {
/* **** univariant curve option enabled ***** */
	rununi_(i__);
	return 0;
    }
    if (icon_1.noninc == 0) {
/* **** run orthogonal T-d or T-P grid ***** */
	rungrd_(i__, wetrun);
    } else {
/* **** run "oddball" T,P or T,d pairs ***** */
	runodd_(i__);
    }
    return 0;
} /* runrxn_ */

/* *********************************************************************** */
/* ** rungrd - Calculate the standard molal thermodynamic properties of */
/* **          the i[th] reaction over the user-specified */
/* **          state-condition grid. */
/* Subroutine */ int rungrd_(integer *i__, logical *wetrun)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer inc;
    static doublereal tpd[3];
    static integer iso;
    extern /* Subroutine */ int reac92_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *), m2tran_(integer *, integer *, logical *,
	     integer *, logical *, integer *, doublereal *, doublereal *, 
	    logical *);
    static logical newiso, rptran;
    static integer ptrans[10];
    static doublereal tpdtrn[90]	/* was [10][3][3] */;
    extern /* Subroutine */ int report_(integer *, integer *, integer *, 
	    doublereal *, doublereal *, logical *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, logical *, logical *);

    i__1 = grid_1.niso;
    for (iso = 1; iso <= i__1; ++iso) {
	if (icon_1.isat == 0) {
	    tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		    grid_1.isomin + (iso - 1) * grid_1.isoinc;
	}
	i__2 = grid_1.nv2;
	for (inc = 1; inc <= i__2; ++inc) {
	    if (icon_1.isat == 0) {
		tpd[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 
			1] = grid_1.v2min + (inc - 1) * grid_1.v2inc;
		tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1]
			 = h2ogrd_1.dsvar[inc + iso * 21 - 22];
	    } else {
		if (inc == 1 && grid_1.v2min == 0.) {
		    tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3]
			     - 1] = satend_1.satmin[icon_1.iopt - 1];
		} else {
		    tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3]
			     - 1] = grid_1.v2min + (inc - 1) * grid_1.v2inc;
		}
		tpd[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 
			1] = h2ogrd_1.dsvar[inc + iso * 21 - 22];
		tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot << 1) - 3] - 
			icon_1.isat - 1] = refval_1.mwh2o / h2ogrd_1.vw[inc + 
			iso * 21 - 22];
	    }
	    if (! (badtd_1.lvdome[inc + iso * 21 - 22] || badtd_1.h2oerr[inc 
		    + iso * 21 - 22])) {
		reac92_(i__, &tpd[1], tpd, &tpd[2], &h2ogrd_1.vw[inc + iso * 
			21 - 22], &h2ogrd_1.bew[inc + iso * 21 - 22], &
			h2ogrd_1.alw[inc + iso * 21 - 22], &h2ogrd_1.dalw[inc 
			+ iso * 21 - 22], &h2ogrd_1.sw[inc + iso * 21 - 22], &
			h2ogrd_1.cpw[inc + iso * 21 - 22], &h2ogrd_1.hw[inc + 
			iso * 21 - 22], &h2ogrd_1.gw[inc + iso * 21 - 22], &
			h2ogrd_1.zw[inc + iso * 21 - 22], &h2ogrd_1.qw[inc + 
			iso * 21 - 22], &h2ogrd_1.yw[inc + iso * 21 - 22], &
			h2ogrd_1.xw[inc + iso * 21 - 22], &icon_1.geqn);
	    }
	    if (! reac2_1.m2reac[*i__ - 1]) {
		rptran = FALSE_;
	    } else {
		newiso = inc == 1 || badtd_1.lvdome[inc - 1 + iso * 21 - 22] 
			|| badtd_1.h2oerr[inc - 1 + iso * 21 - 22];
		m2tran_(&inc, &iso, &newiso, &reac2_1.nm[*i__ - 1], &rptran, 
			ptrans, tpd, tpdtrn, wetrun);
	    }
	    report_(i__, &iso, &inc, tpd, tpdtrn, &rptran, ptrans, &
		    fmeq_1.dvr, &fmeq_1.dsr, &fmeq_1.dcpr, &fmeq_1.dhr, &
		    fmeq_1.dgr, &fmeq_1.logkr, &badtd_1.lvdome[inc + iso * 21 
		    - 22], &badtd_1.h2oerr[inc + iso * 21 - 22], &c_false);
/* L20: */
	}
/* L10: */
    }
    return 0;
} /* rungrd_ */

/* ****************************************************************** */
/* ** m2tran - Returns rptran = .TRUE. if a phase transition occurs */
/* **          for one or more minerals in the current reaction between */
/* **          the immediately previous and current state conditions. */
/* Subroutine */ int m2tran_(integer *inc, integer *iso, logical *newiso, 
	integer *nmreac, logical *rptran, integer *ptrans, doublereal *tpd, 
	doublereal *tpdtrn, logical *wetrun)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer imin;
    extern /* Subroutine */ int getsct_(integer *, integer *, integer *, 
	    integer *, integer *, doublereal *, doublereal *, logical *);
    static integer prprev[10];

    /* Parameter adjustments */
    tpdtrn -= 41;
    --tpd;
    --ptrans;

    /* Function Body */
    *rptran = FALSE_;
    if (*newiso) {
	i__1 = *nmreac;
	for (imin = 1; imin <= i__1; ++imin) {
	    prprev[imin - 1] = minsp_1.phaser[imin - 1];
	    ptrans[imin] = 0;
/* L10: */
	}
    } else {
	i__1 = *nmreac;
	for (imin = 1; imin <= i__1; ++imin) {
	    if (prprev[imin - 1] == minsp_1.phaser[imin - 1]) {
		ptrans[imin] = 0;
	    } else {
		*rptran = TRUE_;
		ptrans[imin] = (i__2 = minsp_1.phaser[imin - 1] - prprev[imin 
			- 1], abs(i__2));
		prprev[imin - 1] = minsp_1.phaser[imin - 1];
		getsct_(inc, iso, &imin, &minsp_1.phaser[imin - 1], &ptrans[
			imin], &tpd[1], &tpdtrn[41], wetrun);
	    }
/* L20: */
	}
    }
    return 0;
} /* m2tran_ */

/* ******************************************************************** */
/* ** getsct - Get s[tate] c[onditions of phase] t[ransition] */
/* **          iphase for mineral imin. */
/* Subroutine */ int getsct_(integer *inc, integer *iso, integer *imin, 
	integer *iphase, integer *ntrans, doublereal *tpd, doublereal *tpdtrn,
	 logical *wetrun)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,0,0,0,0 };
    static doublereal states[4] = { 0.,0.,0.,0. };
    static doublereal tfssat = 139.8888149;

    /* Format strings */
    static char fmt_20[] = "(/,\002 State conditions fall beyond validity li"
	    "mits of\002,/,\002 the Haar et al. (1984) H2O equation of state"
	    ":\002,/,\002 T < Tfusion@P; T > 2250 degC; or P > 30kb.\002,/"
	    ",\002 SUPCRT92 stopped in SUBROUTINE getsct:\002,//,\002 T = "
	    "\002,e12.5,/,\002 P = \002,e12.5,/,\002 D = \002,e12.5,/)";

    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen), s_wsfe(cilist *), do_fio(
	    integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static doublereal p1, p2, t1, t2;
    static integer jjj;
    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    extern doublereal tint_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static integer itran;
    static logical error;
    static doublereal props[46];

    /* Fortran I/O blocks */
    static cilist io___302 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___304 = { 0, 0, 0, fmt_20, 0 };


    /* Parameter adjustments */
    tpdtrn -= 41;
    --tpd;

    /* Function Body */
    specs[5] = icon_1.isat;
    specs[6] = icon_1.iopt;
    specs[7] = icon_1.uselvs;
    specs[8] = icon_1.epseqn;
/* ** ntrans = # phase transitions for mineral imin between */
/* **          current and last isopleth locations. */
    for (itran = *ntrans; itran >= 1; --itran) {
	if (icon_1.isat == 1) {
/* **             vaporization boundary */
	    if (s_cmp(mnames_1.mname + (*imin - 1) * 20, "FERROSILITE", (
		    ftnlen)11, (ftnlen)11) == 0) {
		states[0] = tfssat;
	    } else {
		states[0] = pttran_1.ttranp[*iphase - itran + *imin * 3 - 4] 
			- 273.15;
	    }
	    if (specs[6] == 2) {
		specs[6] = 1;
	    }
	} else {
	    if (icon_1.iplot == 2) {
/* **                  isotherms(pres or dens) */
		states[0] = tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot <<
			 1) - 3]];
		states[1] = pttran_1.ptrant[*iphase - itran + 1 + *imin * 3 - 
			4];
		if (specs[6] == 1) {
		    specs[6] = 2;
		}
	    } else {
		if (icon_1.iopt == 2) {
/* **                       isobars(temp) */
		    states[0] = pttran_1.ttranp[*iphase - itran + *imin * 3 - 
			    4] - 273.15;
		    states[1] = tpd[tpdmap_1.mapiso[icon_1.iopt + (
			    icon_1.iplot << 1) - 3]];
		} else {
/* **                       isochores(temp) */
		    states[2] = tpd[tpdmap_1.mapiso[icon_1.iopt + (
			    icon_1.iplot << 1) - 3]];
		    if (minref_1.dpdttr[*iphase - 1 + *imin * 3 - 4] == 0.) {
			states[0] = pttran_1.ttranp[*iphase - itran + *imin * 
				3 - 4] - 273.15;
		    } else {
/* **                            special case, make */
/* **                            appropriate approximation */
			p1 = h2ogrd_1.dsvar[*inc - 1 + *iso * 21 - 22];
			p2 = h2ogrd_1.dsvar[*inc + *iso * 21 - 22];
			t1 = grid_1.v2min + (*inc - 2) * grid_1.v2inc;
			t2 = grid_1.v2min + (*inc - 1) * grid_1.v2inc;
			d__1 = pttran_1.ttranp[*iphase - itran + *imin * 3 - 
				4] - 273.15;
			states[0] = tint_(&p1, &p2, &t1, &t2, &d__1, &
				minref_1.dpdttr[*iphase - itran + *imin * 3 - 
				4]);
		    }
		}
	    }
	}
	if (*wetrun) {
	    h2o92_(specs, states, props, &error);
	} else {
	    error = FALSE_;
	    states[icon_1.isat + 2] = 0.;
	}
	if (error) {
	    io___302.ciunit = io_1.wterm;
	    s_wsfe(&io___302);
	    for (jjj = 1; jjj <= 3; ++jjj) {
		do_fio(&c__1, (char *)&states[jjj - 1], (ftnlen)sizeof(
			doublereal));
	    }
	    e_wsfe();
	    io___304.ciunit = io_1.tabf;
	    s_wsfe(&io___304);
	    for (jjj = 1; jjj <= 3; ++jjj) {
		do_fio(&c__1, (char *)&states[jjj - 1], (ftnlen)sizeof(
			doublereal));
	    }
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	} else {
	    tpdtrn[*imin + (itran + 3) * 10] = states[0];
	    tpdtrn[*imin + (itran + 6) * 10] = states[1];
	    tpdtrn[*imin + (itran + 9) * 10] = states[icon_1.isat + 2];
	}
/* L10: */
    }
    return 0;
} /* getsct_ */

/* ******************************************************************** */
/* ** Tint - Returns the temperature intersection of isochore(T) */
/* **        with a mineral phase transition boundary where */
/* **        (dP/dT)tr .NE. 0.  Approximation involves assumption */
/* **        that (dP/dT)isochore is linear between P1,T1,D */
/* **        and P2,T2,D (consecutive locations on isochore D(T)) */
/* **        that bridge the phase transition. */
doublereal tint_(doublereal *p1, doublereal *p2, doublereal *t1, doublereal *
	t2, doublereal *ttrnp2, doublereal *dpdttr)
{
    /* System generated locals */
    doublereal ret_val;

    /* Local variables */
    static doublereal bmin, biso, dpdti;

    bmin = *p2 - *dpdttr * *ttrnp2;
    dpdti = (*p2 - *p1) / (*t2 - *t1);
    biso = *p2 - dpdti * *t2;
    ret_val = (bmin - biso) / (dpdti - *dpdttr);
    return ret_val;
} /* tint_ */

/* *********************************************************************** */
/* ** runodd - Calculate the standard molal thermodynamic properties of */
/* **          the i[th] reaction over the user-specified set of */
/* **          nonincremental state condition pairs. */
/* Subroutine */ int runodd_(integer *i__)
{
    /* Initialized data */

    static doublereal tpddum[90]	/* was [10][3][3] */ = { 0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0. };
    static logical rptdum = FALSE_;
    static integer ptdumb[10] = { 0,0,0,0,0,0,0,0,0,0 };

    /* System generated locals */
    integer i__1;

    /* Local variables */
    static doublereal tpd[3];
    static integer iodd;
    extern /* Subroutine */ int reac92_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *), report_(integer *, integer *, integer *,
	     doublereal *, doublereal *, logical *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, logical *, logical *);

/* ** MAXMIN*MXTRAN*3 = 90 *** */
/* ** MAXMIN*0 */
    i__1 = icon_1.noninc;
    for (iodd = 1; iodd <= i__1; ++iodd) {
	tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		grid_1.oddv1[iodd - 1];
	if (icon_1.isat == 0) {
	    tpd[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		    grid_1.oddv2[iodd - 1];
	    tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		    h2ogrd_1.dsvar[iodd - 1];
	} else {
	    tpd[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		    h2ogrd_1.dsvar[iodd - 1];
	    tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot << 1) - 3] - 
		    icon_1.isat - 1] = refval_1.mwh2o / h2ogrd_1.vw[iodd - 1];
	}
	if (! (badtd_1.lvdome[iodd - 1] || badtd_1.h2oerr[iodd - 1])) {
	    reac92_(i__, &tpd[1], tpd, &tpd[2], &h2ogrd_1.vw[iodd - 1], &
		    h2ogrd_1.bew[iodd - 1], &h2ogrd_1.alw[iodd - 1], &
		    h2ogrd_1.dalw[iodd - 1], &h2ogrd_1.sw[iodd - 1], &
		    h2ogrd_1.cpw[iodd - 1], &h2ogrd_1.hw[iodd - 1], &
		    h2ogrd_1.gw[iodd - 1], &h2ogrd_1.zw[iodd - 1], &
		    h2ogrd_1.qw[iodd - 1], &h2ogrd_1.yw[iodd - 1], &
		    h2ogrd_1.xw[iodd - 1], &icon_1.geqn);
	}
	report_(i__, &c__1, &iodd, tpd, tpddum, &rptdum, ptdumb, &fmeq_1.dvr, 
		&fmeq_1.dsr, &fmeq_1.dcpr, &fmeq_1.dhr, &fmeq_1.dgr, &
		fmeq_1.logkr, &badtd_1.lvdome[iodd - 1], &badtd_1.h2oerr[iodd 
		- 1], &c_false);
/* L10: */
    }
    return 0;
} /* runodd_ */

/* *********************************************************************** */
/* ** rununi - Calculate the standard molal thermodynamic properties of */
/* **          the i[th] reaction over the user-specified set of T,logK */
/* **          or P,logK pairs. */
/* Subroutine */ int rununi_(integer *i__)
{
    /* Initialized data */

    static doublereal tpddum[90]	/* was [10][3][3] */ = { 0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0. };
    static logical rptdum = FALSE_;
    static integer ptdumb[10] = { 0,0,0,0,0,0,0,0,0,0 };

    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer inc;
    static doublereal tpd[3];
    static integer iso;
    static doublereal dh2o, v2val, kfind;
    extern logical foundk_(integer *, logical *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static logical kfound;
    static doublereal isoval;
    extern /* Subroutine */ int report_(integer *, integer *, integer *, 
	    doublereal *, doublereal *, logical *, integer *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, logical *, logical *, logical *);
    static logical wetrxn;

/* ** MAXMIN*MXTRAN*3 = 90 *** */
/* ** MAXMIN*0 */
    grid_1.nv2 = grid_1.nlogk;
    wetrxn = reac2_1.nw[*i__ - 1] > 0 || reac2_1.na[*i__ - 1] > 0;
    i__1 = grid_1.niso;
    for (iso = 1; iso <= i__1; ++iso) {
	isoval = grid_1.isomin + (iso - 1) * grid_1.isoinc;
	i__2 = grid_1.nlogk;
	for (inc = 1; inc <= i__2; ++inc) {
	    kfind = grid_1.kmin + (inc - 1) * grid_1.kinc;
	    kfound = foundk_(i__, &wetrxn, &kfind, &isoval, &grid_1.v2min, &
		    grid_1.v2max, &v2val, &dh2o);
	    if (! kfound) {
		fmeq_1.logkr = kfind;
	    }
	    tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		    isoval;
	    tpd[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		    v2val;
	    tpd[2] = dh2o;
	    report_(i__, &iso, &inc, tpd, tpddum, &rptdum, ptdumb, &
		    fmeq_1.dvr, &fmeq_1.dsr, &fmeq_1.dcpr, &fmeq_1.dhr, &
		    fmeq_1.dgr, &fmeq_1.logkr, &c_false, &c_false, &kfound);
/* L10: */
	}
    }
    return 0;
} /* rununi_ */

/* ******************************************************************* */
/* ** SUBRs report, wrtrxn, wrtssp, report */
/* ** SUBR  blanks */
/* ******************************************************************* */
/* ** foundK - Returns '.TRUE.' and v2Kfnd[T|P](isoval[P|T],Kfind) if */
/* **          (1) logK(isoval,var2=v2min..v2max) for the i[th] reaction */
/* **          is unimodal, and (2) logK value Kfind at isoval occurs */
/* **          within v2min..v2max; otherwise returns '.FALSE.'. */
/* **          v2Kfnd(usival,Kfind) is isolated using a straightforward */
/* **          implementation of the golden section search algorithm */
/* **          (e.g., Miller (1984), pp. 130-133.) */
logical foundk_(integer *i__, logical *wetrxn, doublereal *kfind, doublereal *
	isoval, doublereal *v2min, doublereal *v2max, doublereal *v2val, 
	doublereal *dh2o)
{
    /* Initialized data */

    static integer g = 3;
    static integer s = 5;
    static integer h__ = 9;
    static integer cp = 13;
    static integer al = 17;
    static integer be = 19;
    static integer z__ = 37;
    static integer y = 39;
    static integer q = 41;
    static integer daldt = 43;
    static integer x = 45;
    static integer specs[10] = { 2,2,2,5,1,0,2,0,0,0 };
    static doublereal props[46] = { 0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,0.,
	    0.,0.,0.,0.,0.,0.,0.,0.,0.,0. };
    static logical error = FALSE_;

    /* Format strings */
    static char fmt_10[] = "(/,\002 State conditions fall beyond validity li"
	    "mits of\002,/,\002 the Haar et al. (1984) H2O equation of state"
	    ":\002,/,\002 T < Tfusion@P; T > 2250 degC; or P > 30kb.\002,/"
	    ",\002 SUPCRT92 stopped in LOGICAL FUNCTION foundK:\002,//,\002 T"
	    " = \002,e12.5,/,\002 P = \002,e12.5,/,\002 D = \002,e12.5,/)";

    /* System generated locals */
    doublereal d__1;
    logical ret_val;

    /* Builtin functions */
    double sqrt(doublereal);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static doublereal a, b, c__, d__;
    static integer j;
    static doublereal r__, fc, fd;
    static integer jjj;
    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *), reac92_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *);
    static doublereal major, accept, states[4];

    /* Fortran I/O blocks */
    static cilist io___360 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___362 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___364 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___366 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___367 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___368 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___369 = { 0, 0, 0, fmt_10, 0 };


    ret_val = TRUE_;
    j = 0;
    a = *v2min;
    b = *v2max;
    r__ = (3. - sqrt(5.)) / 2.;
    major = r__ * (b - a);
    c__ = a + major;
    d__ = b - major;
/* ** set acceptance tolerance per TOL */
    accept = (abs(*kfind) + 1.) / 1e6;
    states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = *
	    isoval;
    states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = c__;
    states[2] = 1.;
    if (*wetrxn) {
	specs[7] = icon_1.uselvs;
	specs[8] = icon_1.epseqn;
	h2o92_(specs, states, props, &error);
	if (error) {
	    io___360.ciunit = io_1.wterm;
	    s_wsfe(&io___360);
	    for (jjj = 1; jjj <= 3; ++jjj) {
		do_fio(&c__1, (char *)&states[jjj - 1], (ftnlen)sizeof(
			doublereal));
	    }
	    e_wsfe();
	    io___362.ciunit = io_1.tabf;
	    s_wsfe(&io___362);
	    for (jjj = 1; jjj <= 3; ++jjj) {
		do_fio(&c__1, (char *)&states[jjj - 1], (ftnlen)sizeof(
			doublereal));
	    }
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
    }
    d__1 = refval_2.mwh2o / states[2];
    reac92_(i__, &states[1], states, &states[2], &d__1, &props[be - 1], &
	    props[al - 1], &props[daldt - 1], &props[s - 1], &props[cp - 1], &
	    props[h__ - 1], &props[g - 1], &props[z__ - 1], &props[q - 1], &
	    props[y - 1], &props[x - 1], &icon_1.geqn);
    fc = (d__1 = fmeq_1.logkr - *kfind, abs(d__1));
    states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = *
	    isoval;
    states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = d__;
    if (*wetrxn) {
	h2o92_(specs, states, props, &error);
	if (error) {
	    io___364.ciunit = io_1.wterm;
	    s_wsfe(&io___364);
	    e_wsfe();
	    s_stop("", (ftnlen)0);
	}
    }
    d__1 = refval_2.mwh2o / states[2];
    reac92_(i__, &states[1], states, &states[2], &d__1, &props[be - 1], &
	    props[al - 1], &props[daldt - 1], &props[s - 1], &props[cp - 1], &
	    props[h__ - 1], &props[g - 1], &props[z__ - 1], &props[q - 1], &
	    props[y - 1], &props[x - 1], &icon_1.geqn);
    fd = (d__1 = fmeq_1.logkr - *kfind, abs(d__1));
L1:
    if (fc <= accept) {
	states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = *
		isoval;
	states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		c__;
	if (*wetrxn) {
	    h2o92_(specs, states, props, &error);
	    if (error) {
		io___366.ciunit = io_1.wterm;
		s_wsfe(&io___366);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	d__1 = refval_2.mwh2o / states[2];
	reac92_(i__, &states[1], states, &states[2], &d__1, &props[be - 1], &
		props[al - 1], &props[daldt - 1], &props[s - 1], &props[cp - 
		1], &props[h__ - 1], &props[g - 1], &props[z__ - 1], &props[q 
		- 1], &props[y - 1], &props[x - 1], &icon_1.geqn);
	*v2val = c__;
	if (*wetrxn) {
	    *dh2o = states[2];
	} else {
	    *dh2o = 0.;
	}
	return ret_val;
    }
    if (fd <= accept) {
	states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = *
		isoval;
	states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		d__;
	if (*wetrxn) {
	    h2o92_(specs, states, props, &error);
	    if (error) {
		io___367.ciunit = io_1.wterm;
		s_wsfe(&io___367);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	d__1 = refval_2.mwh2o / states[2];
	reac92_(i__, &states[1], states, &states[2], &d__1, &props[be - 1], &
		props[al - 1], &props[daldt - 1], &props[s - 1], &props[cp - 
		1], &props[h__ - 1], &props[g - 1], &props[z__ - 1], &props[q 
		- 1], &props[y - 1], &props[x - 1], &icon_1.geqn);
	*v2val = d__;
	if (*wetrxn) {
	    *dh2o = states[2];
	} else {
	    *dh2o = 0.;
	}
	return ret_val;
    }
    if (j > 50) {
	ret_val = FALSE_;
	if (*wetrxn) {
	    *dh2o = states[2];
	} else {
	    *dh2o = 0.;
	}
	return ret_val;
    } else {
	++j;
    }
    if (fc < fd) {
	b = d__;
	d__ = c__;
	fd = fc;
	c__ = a + r__ * (b - a);
	states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = *
		isoval;
	states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		c__;
	if (*wetrxn) {
	    h2o92_(specs, states, props, &error);
	    if (error) {
		io___368.ciunit = io_1.wterm;
		s_wsfe(&io___368);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	d__1 = refval_2.mwh2o / states[2];
	reac92_(i__, &states[1], states, &states[2], &d__1, &props[be - 1], &
		props[al - 1], &props[daldt - 1], &props[s - 1], &props[cp - 
		1], &props[h__ - 1], &props[g - 1], &props[z__ - 1], &props[q 
		- 1], &props[y - 1], &props[x - 1], &icon_1.geqn);
	fc = (d__1 = fmeq_1.logkr - *kfind, abs(d__1));
    } else {
	a = c__;
	c__ = d__;
	fc = fd;
	d__ = b - r__ * (b - a);
	states[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = *
		isoval;
	states[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3] - 1] = 
		d__;
	if (*wetrxn) {
	    h2o92_(specs, states, props, &error);
	    if (error) {
		io___369.ciunit = io_1.wterm;
		s_wsfe(&io___369);
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
	d__1 = refval_2.mwh2o / states[2];
	reac92_(i__, &states[1], states, &states[2], &d__1, &props[be - 1], &
		props[al - 1], &props[daldt - 1], &props[s - 1], &props[cp - 
		1], &props[h__ - 1], &props[g - 1], &props[z__ - 1], &props[q 
		- 1], &props[y - 1], &props[x - 1], &icon_1.geqn);
	fd = (d__1 = fmeq_1.logkr - *kfind, abs(d__1));
    }
    goto L1;
} /* foundk_ */

/* ******************************************************************** */
/* ** makerf - Prompt for and create a reaction (RXN) file. */
/* Subroutine */ int makerf_(integer *nreac, logical *wetrxn)
{
    /* Format strings */
    static char fmt_5[] = "(/,\002 specify number of reactions to be process"
	    "ed: \002,/)";
    static char fmt_15[] = "(/,\002 input title for reaction \002,i2,\002 "
	    "of \002,i2,\002:\002,/)";
    static char fmt_25[] = "(a80)";
    static char fmt_35[] = "(/,\002 enter [coeff  species] pairs, separated "
	    "by\002/,\002 blanks, one pair per line, for reaction \002,i2,/"
	    ",\002 (conclude with [0 done]) \002,/)";
    static char fmt_112[] = "(a80)";
    static char fmt_113[] = "(/,\002 ill-defined [coeff species] pair; \002"
	    ",\002try again\002,/)";
    static char fmt_125[] = "(/,\002 would you like to save these reactions "
	    "to a file \002,\002(y/n)\002,/)";
    static char fmt_135[] = "(a1)";
    static char fmt_145[] = "(/,\002 specify file name:\002,/)";
    static char fmt_155[] = "(a20)";
    static char fmt_205[] = "(\002 Line 1:  nreac, iwet\002,12x,\002(free fo"
	    "rmat)\002)";
    static char fmt_210[] = "(\002 Line 2:  [blank]\002,16x,\002(free form"
	    "at)\002)";
    static char fmt_215[] = "(\002 Line 3:  descriptive title\002,6x,\002(a8"
	    "0)\002)";
    static char fmt_220[] = "(\002 Line 4:  nm, na, ng, nw\002,9x,\002(free "
	    "format)\002)";
    static char fmt_225[] = "(\002 nm Lines:  coeff  mname  mform\002,2x,"
	    "\002(1x,f9.3,2x,a20,2x,a30)\002)";
    static char fmt_230[] = "(\002 ng Lines:  coeff  aname  aform\002,2x,"
	    "\002(1x,f9.3,2x,a20,2x,a30)\002)";
    static char fmt_235[] = "(\002 na Lines:  coeff  gname  gform\002,2x,"
	    "\002(1x,f9.3,2x,a20,2x,a30)\002)";
    static char fmt_240[] = "(\002 [1 Line:   coeff  H2O    H2O] \002,2x,"
	    "\002(1x,f9.3,2x,a20,2x,a30)\002,/)";
    static char fmt_245[] = "(\002*** each of the nreac reaction blocks\002,"
	    "/,\002*** contains 3+nm+ng+na+nw lines\002,/)";
    static char fmt_250[] = "(55(\002*\002),/)";
    static char fmt_165[] = "(2(1x,i2))";
    static char fmt_175[] = "(/,1x,a80,/,4(1x,i3))";
    static char fmt_185[] = "(1x,f9.3,2x,a20,2x,a30)";
    static char fmt_195[] = "(1x,f9.3,2x,a20,2x,a30)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsle(cilist *), do_lio(integer *
	    , integer *, char *, ftnlen), e_rsle(void), do_fio(integer *, 
	    char *, ftnlen), s_rsfe(cilist *), e_rsfe(void), s_cmp(char *, 
	    char *, ftnlen, ftnlen);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static char ans[1];
    static integer rec1, ibad;
    static char sbad[20*10];
    static integer nmga, igas, nm1234, imin, iaqs;
    static char form[30];
    static integer iwet;
    static char namea[20*10*10];
    static doublereal coeff;
    static integer ireac;
    static char nameg[20*10*10];
    extern logical match_(char *, char *, integer *, integer *, integer *, 
	    integer *, integer *, ftnlen, ftnlen);
    static char namem[20*10*10], forma[30*10*10];
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);
    static char formg[30*10*10];
    extern logical parse_(char *, doublereal *, char *, ftnlen, ftnlen);
    static char formm[30*10*10];
    static logical rxnok;
    static char specie[20];
    extern /* Subroutine */ int wrtbad_(integer *, char *, ftnlen), umaker_(
	    integer *, doublereal *, char *, char *, integer *, char *, char *
	    , char *, char *, char *, char *, ftnlen, ftnlen, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen), chkrxn_(integer *, char *, char *
	    , char *, char *, char *, char *, logical *, ftnlen, ftnlen, 
	    ftnlen, ftnlen, ftnlen, ftnlen);
    static char string[80];

    /* Fortran I/O blocks */
    static cilist io___372 = { 0, 0, 0, fmt_5, 0 };
    static cilist io___373 = { 0, 0, 0, 0, 0 };
    static cilist io___375 = { 0, 0, 0, fmt_15, 0 };
    static cilist io___376 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___377 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___379 = { 0, 0, 0, fmt_112, 0 };
    static cilist io___383 = { 0, 0, 0, fmt_113, 0 };
    static cilist io___395 = { 0, 0, 0, fmt_125, 0 };
    static cilist io___396 = { 0, 0, 0, fmt_135, 0 };
    static cilist io___398 = { 0, 0, 0, fmt_145, 0 };
    static cilist io___399 = { 0, 0, 0, fmt_155, 0 };
    static cilist io___400 = { 0, 0, 0, fmt_205, 0 };
    static cilist io___401 = { 0, 0, 0, fmt_210, 0 };
    static cilist io___402 = { 0, 0, 0, fmt_215, 0 };
    static cilist io___403 = { 0, 0, 0, fmt_220, 0 };
    static cilist io___404 = { 0, 0, 0, fmt_225, 0 };
    static cilist io___405 = { 0, 0, 0, fmt_230, 0 };
    static cilist io___406 = { 0, 0, 0, fmt_235, 0 };
    static cilist io___407 = { 0, 0, 0, fmt_240, 0 };
    static cilist io___408 = { 0, 0, 0, fmt_245, 0 };
    static cilist io___409 = { 0, 0, 0, fmt_250, 0 };
    static cilist io___410 = { 0, 0, 0, fmt_165, 0 };
    static cilist io___411 = { 0, 0, 0, fmt_175, 0 };
    static cilist io___412 = { 0, 0, 0, fmt_185, 0 };
    static cilist io___414 = { 0, 0, 0, fmt_185, 0 };
    static cilist io___416 = { 0, 0, 0, fmt_185, 0 };
    static cilist io___418 = { 0, 0, 0, fmt_195, 0 };


    nm1234 = rlimit_1.nmin1 + rlimit_1.nmin2 + rlimit_1.nmin3 + 
	    rlimit_1.nmin4;
    nmga = nm1234 + rlimit_1.ngas + rlimit_1.naqs;
/* **** prompt for / read nreac ***** */
L1:
    io___372.ciunit = io_1.wterm;
    s_wsfe(&io___372);
    e_wsfe();
    io___373.ciunit = io_1.rterm;
    s_rsle(&io___373);
    do_lio(&c__3, &c__1, (char *)&(*nreac), (ftnlen)sizeof(integer));
    e_rsle();
    if (*nreac <= 0) {
	goto L1;
    }
    i__1 = *nreac;
    for (ireac = 1; ireac <= i__1; ++ireac) {
/* **** prompt for / read specifications for next reaction ***** */
	io___375.ciunit = io_1.wterm;
	s_wsfe(&io___375);
	do_fio(&c__1, (char *)&ireac, (ftnlen)sizeof(integer));
	do_fio(&c__1, (char *)&(*nreac), (ftnlen)sizeof(integer));
	e_wsfe();
	io___376.ciunit = io_1.rterm;
	s_rsfe(&io___376);
	do_fio(&c__1, reac1_1.rtitle + (ireac - 1) * 80, (ftnlen)80);
	e_rsfe();
L333:
	io___377.ciunit = io_1.wterm;
	s_wsfe(&io___377);
	do_fio(&c__1, (char *)&ireac, (ftnlen)sizeof(integer));
	e_wsfe();
	ibad = 0;
	reac2_1.m2reac[ireac - 1] = FALSE_;
	reac2_1.nm[ireac - 1] = 0;
	reac2_1.ng[ireac - 1] = 0;
	reac2_1.na[ireac - 1] = 0;
	reac2_1.nw[ireac - 1] = 0;
L111:
	io___379.ciunit = io_1.rterm;
	s_rsfe(&io___379);
	do_fio(&c__1, string, (ftnlen)80);
	e_rsfe();
	if (! parse_(string, &coeff, specie, (ftnlen)80, (ftnlen)20)) {
	    io___383.ciunit = io_1.wterm;
	    s_wsfe(&io___383);
	    e_wsfe();
	    goto L111;
	}
	if (coeff == 0.) {
/* *****          reaction stoichiometry complete ****** */
	    if (ibad != 0) {
		wrtbad_(&ibad, sbad, (ftnlen)20);
		goto L111;
	    } else {
/* *****               ensure that stoichiometry is correct */
		chkrxn_(&ireac, namem, namea, nameg, formm, forma, formg, &
			rxnok, (ftnlen)20, (ftnlen)20, (ftnlen)20, (ftnlen)30,
			 (ftnlen)30, (ftnlen)30);
		if (! rxnok) {
		    goto L333;
		}
	    }
	} else {
/* *****          determine disposition of current specie: either H2O, */
/* *****          found or not found within the current database */
	    if (s_cmp(specie, "H2O", (ftnlen)20, (ftnlen)3) == 0) {
		reac2_1.nw[ireac - 1] = 1;
		reac2_1.coefw[ireac - 1] = coeff;
	    } else {
		if (match_(specie, form, &rec1, &rlimit_1.rec1m1, &c__1, &
			nmga, &nm1234, (ftnlen)20, (ftnlen)30)) {
/* *****                    update [n|coef|rec1][m|g|a]; continue */
		    umaker_(&ireac, &coeff, specie, form, &rec1, namem, namea,
			     nameg, formm, forma, formg, (ftnlen)20, (ftnlen)
			    30, (ftnlen)20, (ftnlen)20, (ftnlen)20, (ftnlen)
			    30, (ftnlen)30, (ftnlen)30);
		} else {
		    ++ibad;
		    s_copy(sbad + (ibad - 1) * 20, specie, (ftnlen)20, (
			    ftnlen)20);
		}
	    }
	    goto L111;
	}
/* L10: */
    }
/* ***** set wetrxn variable ****** */
    iwet = 0;
    *wetrxn = FALSE_;
    if (icon_1.isat == 1 || icon_1.iopt == 1) {
	*wetrxn = TRUE_;
	iwet = 1;
    } else {
	i__1 = *nreac;
	for (ireac = 1; ireac <= i__1; ++ireac) {
	    if (reac2_1.nw[ireac - 1] == 1 || reac2_1.na[ireac - 1] > 0) {
		*wetrxn = TRUE_;
		iwet = 1;
		goto L444;
	    }
/* L70: */
	}
    }
/* ***** save reaction file if desired ****** */
L444:
    io___395.ciunit = io_1.wterm;
    s_wsfe(&io___395);
    e_wsfe();
    io___396.ciunit = io_1.rterm;
    s_rsfe(&io___396);
    do_fio(&c__1, ans, (ftnlen)1);
    e_rsfe();
    if (*(unsigned char *)ans != 'y' && *(unsigned char *)ans != 'Y' && *(
	    unsigned char *)ans != 'n' && *(unsigned char *)ans != 'N') {
	goto L444;
    }
    saveif_1.saverf = *(unsigned char *)ans == 'y' || *(unsigned char *)ans ==
	     'Y';
    if (saveif_1.saverf) {
L555:
	io___398.ciunit = io_1.wterm;
	s_wsfe(&io___398);
	e_wsfe();
	io___399.ciunit = io_1.rterm;
	s_rsfe(&io___399);
	do_fio(&c__1, fnames_1.namerf, (ftnlen)20);
	e_rsfe();
	if (! openf_(&io_1.wterm, &io_1.reacf, fnames_1.namerf, &c__2, &c__1, 
		&c__1, &c__132, (ftnlen)20)) {
	    goto L555;
	} else {
/* **             write generic header */
	    io___400.ciunit = io_1.reacf;
	    s_wsfe(&io___400);
	    e_wsfe();
	    io___401.ciunit = io_1.reacf;
	    s_wsfe(&io___401);
	    e_wsfe();
	    io___402.ciunit = io_1.reacf;
	    s_wsfe(&io___402);
	    e_wsfe();
	    io___403.ciunit = io_1.reacf;
	    s_wsfe(&io___403);
	    e_wsfe();
	    io___404.ciunit = io_1.reacf;
	    s_wsfe(&io___404);
	    e_wsfe();
	    io___405.ciunit = io_1.reacf;
	    s_wsfe(&io___405);
	    e_wsfe();
	    io___406.ciunit = io_1.reacf;
	    s_wsfe(&io___406);
	    e_wsfe();
	    io___407.ciunit = io_1.reacf;
	    s_wsfe(&io___407);
	    e_wsfe();
	    io___408.ciunit = io_1.reacf;
	    s_wsfe(&io___408);
	    e_wsfe();
	    io___409.ciunit = io_1.reacf;
	    s_wsfe(&io___409);
	    e_wsfe();
/* **             write reaction information */
	    io___410.ciunit = io_1.reacf;
	    s_wsfe(&io___410);
	    do_fio(&c__1, (char *)&(*nreac), (ftnlen)sizeof(integer));
	    do_fio(&c__1, (char *)&iwet, (ftnlen)sizeof(integer));
	    e_wsfe();
	}
	i__1 = *nreac;
	for (ireac = 1; ireac <= i__1; ++ireac) {
	    io___411.ciunit = io_1.reacf;
	    s_wsfe(&io___411);
	    do_fio(&c__1, reac1_1.rtitle + (ireac - 1) * 80, (ftnlen)80);
	    do_fio(&c__1, (char *)&reac2_1.nm[ireac - 1], (ftnlen)sizeof(
		    integer));
	    do_fio(&c__1, (char *)&reac2_1.na[ireac - 1], (ftnlen)sizeof(
		    integer));
	    do_fio(&c__1, (char *)&reac2_1.ng[ireac - 1], (ftnlen)sizeof(
		    integer));
	    do_fio(&c__1, (char *)&reac2_1.nw[ireac - 1], (ftnlen)sizeof(
		    integer));
	    e_wsfe();
	    if (reac2_1.nm[ireac - 1] > 0) {
		io___412.ciunit = io_1.reacf;
		s_wsfe(&io___412);
		i__2 = reac2_1.nm[ireac - 1];
		for (imin = 1; imin <= i__2; ++imin) {
		    do_fio(&c__1, (char *)&reac2_1.coefm[ireac + imin * 10 - 
			    11], (ftnlen)sizeof(doublereal));
		    do_fio(&c__1, namem + (ireac + imin * 10 - 11) * 20, (
			    ftnlen)20);
		    do_fio(&c__1, formm + (ireac + imin * 10 - 11) * 30, (
			    ftnlen)30);
		}
		e_wsfe();
	    }
	    if (reac2_1.na[ireac - 1] > 0) {
		io___414.ciunit = io_1.reacf;
		s_wsfe(&io___414);
		i__2 = reac2_1.na[ireac - 1];
		for (iaqs = 1; iaqs <= i__2; ++iaqs) {
		    do_fio(&c__1, (char *)&reac2_1.coefa[ireac + iaqs * 10 - 
			    11], (ftnlen)sizeof(doublereal));
		    do_fio(&c__1, namea + (ireac + iaqs * 10 - 11) * 20, (
			    ftnlen)20);
		    do_fio(&c__1, forma + (ireac + iaqs * 10 - 11) * 30, (
			    ftnlen)30);
		}
		e_wsfe();
	    }
	    if (reac2_1.ng[ireac - 1] > 0) {
		io___416.ciunit = io_1.reacf;
		s_wsfe(&io___416);
		i__2 = reac2_1.ng[ireac - 1];
		for (igas = 1; igas <= i__2; ++igas) {
		    do_fio(&c__1, (char *)&reac2_1.coefg[ireac + igas * 10 - 
			    11], (ftnlen)sizeof(doublereal));
		    do_fio(&c__1, nameg + (ireac + igas * 10 - 11) * 20, (
			    ftnlen)20);
		    do_fio(&c__1, formg + (ireac + igas * 10 - 11) * 30, (
			    ftnlen)30);
		}
		e_wsfe();
	    }
	    if (reac2_1.nw[ireac - 1] == 1) {
		io___418.ciunit = io_1.reacf;
		s_wsfe(&io___418);
		do_fio(&c__1, (char *)&reac2_1.coefw[ireac - 1], (ftnlen)
			sizeof(doublereal));
		do_fio(&c__1, "H2O                 ", (ftnlen)20);
		do_fio(&c__1, "H2O                           ", (ftnlen)30);
		e_wsfe();
	    }
/* L80: */
	}
    }
    return 0;
} /* makerf_ */

/* ************************************************************** */
/* ** nxtrec - Get rec1 for next database species. */
integer nxtrec_(integer *irec, integer *mga, integer *nm1234)
{
    /* System generated locals */
    integer ret_val;

    if (*mga <= rlimit_1.nmin1 || *mga > *nm1234) {
/* **        one-phase mineral, gas, or aqueous species */
	ret_val = *irec + 6;
	return ret_val;
    }
    if (*mga <= rlimit_1.nmin1 + rlimit_1.nmin2) {
/* **        two-phase mineral */
	ret_val = *irec + 7;
	return ret_val;
    }
    if (*mga <= rlimit_1.nmin1 + rlimit_1.nmin2 + rlimit_1.nmin3) {
/* **        three-phase mineral */
	ret_val = *irec + 8;
    } else {
/* **        four-phase mineral */
	ret_val = *irec + 9;
    }
    return ret_val;
} /* nxtrec_ */

/* ****************************************************************** */
/* ** readrf - Open/read user-specified reaction (RXN) file. */
/* Subroutine */ int readrf_(integer *nreac, logical *wetrxn)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 specify name of reaction file:\002,/)";
    static char fmt_20[] = "(a20)";
    static char fmt_25[] = "(////////////)";
    static char fmt_40[] = "(/,1x,a80)";
    static char fmt_51[] = "(1x,f9.3,2x,a20,2x,a30)";
    static char fmt_1000[] = "(//,\002 Reaction \002,i2,\002 species \002,a2"
	    "0,/,\002 not found in database \002,a20,//,\002 re-run with corr"
	    "ect database or re-create\002,/,\002 reaction file from this dat"
	    "abase.\002,/)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsfe(cilist *), do_fio(integer *
	    , char *, ftnlen), e_rsfe(void), s_rsle(cilist *), do_lio(integer 
	    *, integer *, char *, ftnlen), e_rsle(void);
    /* Subroutine */ int s_stop(char *, ftnlen);

    /* Local variables */
    static integer rec1, iend, nmga, igas, nm1234, imin, iaqs;
    static char form[30];
    static integer iwet, ireac;
    extern logical match_(char *, char *, integer *, integer *, integer *, 
	    integer *, integer *, ftnlen, ftnlen), openf_(integer *, integer *
	    , char *, integer *, integer *, integer *, integer *, ftnlen);
    static char spname[20];
    static integer istart;

    /* Fortran I/O blocks */
    static cilist io___421 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___422 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___423 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___424 = { 0, 0, 0, 0, 0 };
    static cilist io___427 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___428 = { 0, 0, 0, 0, 0 };
    static cilist io___430 = { 0, 0, 0, fmt_51, 0 };
    static cilist io___436 = { 0, 0, 0, fmt_51, 0 };
    static cilist io___439 = { 0, 0, 0, fmt_51, 0 };
    static cilist io___440 = { 0, 0, 0, 0, 0 };
    static cilist io___441 = { 0, 0, 0, fmt_1000, 0 };


    nm1234 = rlimit_1.nmin1 + rlimit_1.nmin2 + rlimit_1.nmin3 + 
	    rlimit_1.nmin4;
    nmga = nm1234 + rlimit_1.ngas + rlimit_1.naqs;
L1:
    io___421.ciunit = io_1.wterm;
    s_wsfe(&io___421);
    e_wsfe();
    io___422.ciunit = io_1.rterm;
    s_rsfe(&io___422);
    do_fio(&c__1, fnames_1.namerf, (ftnlen)20);
    e_rsfe();
    if (! openf_(&io_1.wterm, &io_1.reacf, fnames_1.namerf, &c__1, &c__1, &
	    c__1, &c__132, (ftnlen)20)) {
	goto L1;
    }
    saveif_1.saverf = TRUE_;
/* **** read number of reactions and their wet/dry character ****** */
/* **** skip first 13 comment lines */
    io___423.ciunit = io_1.reacf;
    s_rsfe(&io___423);
    e_rsfe();
    io___424.ciunit = io_1.reacf;
    s_rsle(&io___424);
    do_lio(&c__3, &c__1, (char *)&(*nreac), (ftnlen)sizeof(integer));
    do_lio(&c__3, &c__1, (char *)&iwet, (ftnlen)sizeof(integer));
    e_rsle();
    *wetrxn = iwet == 1;
    i__1 = *nreac;
    for (ireac = 1; ireac <= i__1; ++ireac) {
/* **** read title, nm, na, ng, nw for next reaction ***** */
	io___427.ciunit = io_1.reacf;
	s_rsfe(&io___427);
	do_fio(&c__1, reac1_1.rtitle + (ireac - 1) * 80, (ftnlen)80);
	e_rsfe();
	io___428.ciunit = io_1.reacf;
	s_rsle(&io___428);
	do_lio(&c__3, &c__1, (char *)&reac2_1.nm[ireac - 1], (ftnlen)sizeof(
		integer));
	do_lio(&c__3, &c__1, (char *)&reac2_1.na[ireac - 1], (ftnlen)sizeof(
		integer));
	do_lio(&c__3, &c__1, (char *)&reac2_1.ng[ireac - 1], (ftnlen)sizeof(
		integer));
	do_lio(&c__3, &c__1, (char *)&reac2_1.nw[ireac - 1], (ftnlen)sizeof(
		integer));
	e_rsle();
/* **** read mineral, aqueous species, gas, H2O stoichiometry ***** */
	reac2_1.m2reac[ireac - 1] = FALSE_;
	if (reac2_1.nm[ireac - 1] > 0) {
	    i__2 = reac2_1.nm[ireac - 1];
	    for (imin = 1; imin <= i__2; ++imin) {
		io___430.ciunit = io_1.reacf;
		s_rsfe(&io___430);
		do_fio(&c__1, (char *)&reac2_1.coefm[ireac + imin * 10 - 11], 
			(ftnlen)sizeof(doublereal));
		do_fio(&c__1, spname, (ftnlen)20);
		do_fio(&c__1, form, (ftnlen)30);
		e_rsfe();
		if (! match_(spname, form, &rec1, &rlimit_1.rec1m1, &c__1, &
			nm1234, &nm1234, (ftnlen)20, (ftnlen)30)) {
		    goto L999;
		} else {
		    reac2_1.rec1m[ireac + imin * 10 - 11] = rec1;
		    if (reac2_1.rec1m[ireac + imin * 10 - 11] >= 
			    rlimit_1.rec1m2) {
			reac2_1.m2reac[ireac - 1] = TRUE_;
		    }
		}
/* L50: */
	    }
	}
	if (reac2_1.na[ireac - 1] > 0) {
	    istart = nm1234 + rlimit_1.ngas + 1;
	    i__2 = reac2_1.na[ireac - 1];
	    for (iaqs = 1; iaqs <= i__2; ++iaqs) {
		io___436.ciunit = io_1.reacf;
		s_rsfe(&io___436);
		do_fio(&c__1, (char *)&reac2_1.coefa[ireac + iaqs * 10 - 11], 
			(ftnlen)sizeof(doublereal));
		do_fio(&c__1, spname, (ftnlen)20);
		do_fio(&c__1, form, (ftnlen)30);
		e_rsfe();
		if (! match_(spname, form, &rec1, &rlimit_1.rec1aa, &istart, &
			nmga, &nm1234, (ftnlen)20, (ftnlen)30)) {
		    goto L999;
		} else {
		    reac2_1.rec1a[ireac + iaqs * 10 - 11] = rec1;
		}
/* L60: */
	    }
	}
	if (reac2_1.ng[ireac - 1] > 0) {
	    istart = nm1234 + 1;
	    iend = nm1234 + rlimit_1.ngas;
	    i__2 = reac2_1.ng[ireac - 1];
	    for (igas = 1; igas <= i__2; ++igas) {
		io___439.ciunit = io_1.reacf;
		s_rsfe(&io___439);
		do_fio(&c__1, (char *)&reac2_1.coefg[ireac + igas * 10 - 11], 
			(ftnlen)sizeof(doublereal));
		do_fio(&c__1, spname, (ftnlen)20);
		do_fio(&c__1, form, (ftnlen)30);
		e_rsfe();
		if (! match_(spname, form, &rec1, &rlimit_1.rec1gg, &istart, &
			iend, &nm1234, (ftnlen)20, (ftnlen)30)) {
		    goto L999;
		} else {
		    reac2_1.rec1g[ireac + igas * 10 - 11] = rec1;
		}
/* L70: */
	    }
	}
	if (reac2_1.nw[ireac - 1] == 0) {
	    reac2_1.coefw[ireac - 1] = 0.;
	} else {
	    io___440.ciunit = io_1.reacf;
	    s_rsle(&io___440);
	    do_lio(&c__5, &c__1, (char *)&reac2_1.coefw[ireac - 1], (ftnlen)
		    sizeof(doublereal));
	    e_rsle();
	}
/* L30: */
    }
    return 0;
L999:
    io___441.ciunit = io_1.wterm;
    s_wsfe(&io___441);
    do_fio(&c__1, (char *)&ireac, (ftnlen)sizeof(integer));
    do_fio(&c__1, spname, (ftnlen)20);
    do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
    e_wsfe();
    s_stop("", (ftnlen)0);
    return 0;
} /* readrf_ */

/* *********************************************************************** */
/* ** wrtbad - Write the list of species not found in database pfname; */
/* **          prompt for repeats. */
/* Subroutine */ int wrtbad_(integer *ibad, char *sbad, ftnlen sbad_len)
{
    /* Format strings */
    static char fmt_45[] = "(/,\002 the following species were not\002,/,"
	    "\002 found in database \002,a20,/)";
    static char fmt_55[] = "(5x,a20)";
    static char fmt_65[] = "(/,\002 input new [coeff  species] pairs\002,/"
	    ",\002 to replace these incorrect entries\002,/,\002 (conclude wi"
	    "th [0 done]) \002,/)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__;

    /* Fortran I/O blocks */
    static cilist io___442 = { 0, 0, 0, fmt_45, 0 };
    static cilist io___444 = { 0, 0, 0, fmt_55, 0 };
    static cilist io___445 = { 0, 0, 0, fmt_65, 0 };


    /* Parameter adjustments */
    sbad -= 20;

    /* Function Body */
    io___442.ciunit = io_1.wterm;
    s_wsfe(&io___442);
    do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
    e_wsfe();
    i__1 = *ibad;
    for (i__ = 1; i__ <= i__1; ++i__) {
	io___444.ciunit = io_1.wterm;
	s_wsfe(&io___444);
	do_fio(&c__1, sbad + i__ * 20, (ftnlen)20);
	e_wsfe();
/* L20: */
    }
    io___445.ciunit = io_1.wterm;
    s_wsfe(&io___445);
    e_wsfe();
    *ibad = 0;
    return 0;
} /* wrtbad_ */

/* *********************************************************************** */
/* ** chkrxn - Give the user a chance to look over rxn stoichiometry; */
/* **          if it's ok, then rxnok returns .TRUE.; otherwise, */
/* **          returns .FALSE. */
/* Subroutine */ int chkrxn_(integer *ireac, char *namem, char *namea, char *
	nameg, char *formm, char *forma, char *formg, logical *rxnok, ftnlen 
	namem_len, ftnlen namea_len, ftnlen nameg_len, ftnlen formm_len, 
	ftnlen forma_len, ftnlen formg_len)
{
    /* Initialized data */

    static char namew[20] = "H2O                 ";
    static char formw[30] = "H2O                           ";

    /* Format strings */
    static char fmt_75[] = "(/,\002 reaction \002,i2,\002 stoichiometry:\002"
	    ",/)";
    static char fmt_85[] = "(6x,f7.3,3x,a20,3x,a30)";
    static char fmt_86[] = "(6x,f7.3,3x,a20,3x,a20)";
    static char fmt_95[] = "(/,\002 is this correct? (y/n)\002,/)";
    static char fmt_105[] = "(a1)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void),
	     s_rsfe(cilist *), e_rsfe(void);

    /* Local variables */
    static char ans[1];
    static integer igas, imin, iaqs;

    /* Fortran I/O blocks */
    static cilist io___448 = { 0, 0, 0, fmt_75, 0 };
    static cilist io___450 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___452 = { 0, 0, 0, fmt_86, 0 };
    static cilist io___454 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___455 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___456 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___457 = { 0, 0, 0, fmt_86, 0 };
    static cilist io___458 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___459 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___460 = { 0, 0, 0, fmt_95, 0 };
    static cilist io___461 = { 0, 0, 0, fmt_105, 0 };


    /* Parameter adjustments */
    formg -= 330;
    forma -= 330;
    formm -= 330;
    nameg -= 220;
    namea -= 220;
    namem -= 220;

    /* Function Body */
    io___448.ciunit = io_1.wterm;
    s_wsfe(&io___448);
    do_fio(&c__1, (char *)&(*ireac), (ftnlen)sizeof(integer));
    e_wsfe();
/* **** write reactants */
    i__1 = reac2_1.nm[*ireac - 1];
    for (imin = 1; imin <= i__1; ++imin) {
	if (reac2_1.coefm[*ireac + imin * 10 - 11] < 0.) {
	    io___450.ciunit = io_1.wterm;
	    s_wsfe(&io___450);
	    do_fio(&c__1, (char *)&reac2_1.coefm[*ireac + imin * 10 - 11], (
		    ftnlen)sizeof(doublereal));
	    do_fio(&c__1, namem + (*ireac + imin * 10) * 20, (ftnlen)20);
	    do_fio(&c__1, formm + (*ireac + imin * 10) * 30, (ftnlen)30);
	    e_wsfe();
	}
/* L30: */
    }
    i__1 = reac2_1.ng[*ireac - 1];
    for (igas = 1; igas <= i__1; ++igas) {
	if (reac2_1.coefg[*ireac + igas * 10 - 11] < 0.) {
	    io___452.ciunit = io_1.wterm;
	    s_wsfe(&io___452);
	    do_fio(&c__1, (char *)&reac2_1.coefg[*ireac + igas * 10 - 11], (
		    ftnlen)sizeof(doublereal));
	    do_fio(&c__1, formg + (*ireac + igas * 10) * 30, (ftnlen)20);
	    do_fio(&c__1, nameg + (*ireac + igas * 10) * 20, (ftnlen)20);
	    e_wsfe();
	}
/* L40: */
    }
    i__1 = reac2_1.na[*ireac - 1];
    for (iaqs = 1; iaqs <= i__1; ++iaqs) {
	if (reac2_1.coefa[*ireac + iaqs * 10 - 11] < 0.) {
	    io___454.ciunit = io_1.wterm;
	    s_wsfe(&io___454);
	    do_fio(&c__1, (char *)&reac2_1.coefa[*ireac + iaqs * 10 - 11], (
		    ftnlen)sizeof(doublereal));
	    do_fio(&c__1, namea + (*ireac + iaqs * 10) * 20, (ftnlen)20);
	    do_fio(&c__1, forma + (*ireac + iaqs * 10) * 30, (ftnlen)30);
	    e_wsfe();
	}
/* L50: */
    }
    if (reac2_1.nw[*ireac - 1] == 1 && reac2_1.coefw[*ireac - 1] < 0.) {
	io___455.ciunit = io_1.wterm;
	s_wsfe(&io___455);
	do_fio(&c__1, (char *)&reac2_1.coefw[*ireac - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, namew, (ftnlen)20);
	do_fio(&c__1, formw, (ftnlen)30);
	e_wsfe();
    }
/* **** write products */
    i__1 = reac2_1.nm[*ireac - 1];
    for (imin = 1; imin <= i__1; ++imin) {
	if (reac2_1.coefm[*ireac + imin * 10 - 11] > 0.) {
	    io___456.ciunit = io_1.wterm;
	    s_wsfe(&io___456);
	    do_fio(&c__1, (char *)&reac2_1.coefm[*ireac + imin * 10 - 11], (
		    ftnlen)sizeof(doublereal));
	    do_fio(&c__1, namem + (*ireac + imin * 10) * 20, (ftnlen)20);
	    do_fio(&c__1, formm + (*ireac + imin * 10) * 30, (ftnlen)30);
	    e_wsfe();
	}
/* L31: */
    }
    i__1 = reac2_1.ng[*ireac - 1];
    for (igas = 1; igas <= i__1; ++igas) {
	if (reac2_1.coefg[*ireac + igas * 10 - 11] > 0.) {
	    io___457.ciunit = io_1.wterm;
	    s_wsfe(&io___457);
	    do_fio(&c__1, (char *)&reac2_1.coefg[*ireac + igas * 10 - 11], (
		    ftnlen)sizeof(doublereal));
	    do_fio(&c__1, formg + (*ireac + igas * 10) * 30, (ftnlen)20);
	    do_fio(&c__1, nameg + (*ireac + igas * 10) * 20, (ftnlen)20);
	    e_wsfe();
	}
/* L41: */
    }
    i__1 = reac2_1.na[*ireac - 1];
    for (iaqs = 1; iaqs <= i__1; ++iaqs) {
	if (reac2_1.coefa[*ireac + iaqs * 10 - 11] > 0.) {
	    io___458.ciunit = io_1.wterm;
	    s_wsfe(&io___458);
	    do_fio(&c__1, (char *)&reac2_1.coefa[*ireac + iaqs * 10 - 11], (
		    ftnlen)sizeof(doublereal));
	    do_fio(&c__1, namea + (*ireac + iaqs * 10) * 20, (ftnlen)20);
	    do_fio(&c__1, forma + (*ireac + iaqs * 10) * 30, (ftnlen)30);
	    e_wsfe();
	}
/* L51: */
    }
    if (reac2_1.nw[*ireac - 1] == 1 && reac2_1.coefw[*ireac - 1] > 0.) {
	io___459.ciunit = io_1.wterm;
	s_wsfe(&io___459);
	do_fio(&c__1, (char *)&reac2_1.coefw[*ireac - 1], (ftnlen)sizeof(
		doublereal));
	do_fio(&c__1, namew, (ftnlen)20);
	do_fio(&c__1, formw, (ftnlen)30);
	e_wsfe();
    }
L222:
    io___460.ciunit = io_1.wterm;
    s_wsfe(&io___460);
    e_wsfe();
    io___461.ciunit = io_1.rterm;
    s_rsfe(&io___461);
    do_fio(&c__1, ans, (ftnlen)1);
    e_rsfe();
    if (*(unsigned char *)ans != 'Y' && *(unsigned char *)ans != 'y' && *(
	    unsigned char *)ans != 'N' && *(unsigned char *)ans != 'n') {
	goto L222;
    } else {
	*rxnok = *(unsigned char *)ans == 'Y' || *(unsigned char *)ans == 'y';
    }
    return 0;
} /* chkrxn_ */

/* ****************************************************************** */
/* ** match - Returns .TRUE. (and rec1sp) if specie is found in */
/* **         database pfname; otherwise returns .FALSE. */
logical match_(char *specie, char *form, integer *rec1sp, integer *rec1ty, 
	integer *first, integer *last, integer *nm1234, ftnlen specie_len, 
	ftnlen form_len)
{
    /* Format strings */
    static char fmt_115[] = "(1x,a20,a30)";

    /* System generated locals */
    integer i__1;
    logical ret_val;

    /* Builtin functions */
    integer s_rdfe(cilist *), do_fio(integer *, char *, ftnlen), e_rdfe(void),
	     s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer mga;
    static char name__[20];
    static integer irec;
    extern integer nxtrec_(integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___465 = { 0, 0, 0, fmt_115, 0 };


    irec = *rec1ty;
    i__1 = *last;
    for (mga = *first; mga <= i__1; ++mga) {
	io___465.ciunit = io_1.pronf;
	io___465.cirec = irec;
	s_rdfe(&io___465);
	do_fio(&c__1, name__, (ftnlen)20);
	do_fio(&c__1, form, (ftnlen)30);
	e_rdfe();
	if (s_cmp(specie, name__, (ftnlen)20, (ftnlen)20) == 0) {
	    ret_val = TRUE_;
	    *rec1sp = irec;
	    return ret_val;
	} else {
	    irec = nxtrec_(&irec, &mga, nm1234);
	}
/* L60: */
    }
    ret_val = FALSE_;
    return ret_val;
} /* match_ */

/* ****************************************************************** */
/* ** umaker - Update /reac/ arrays to include current species. */
/* Subroutine */ int umaker_(integer *ireac, doublereal *coeff, char *specie, 
	char *form, integer *rec1, char *namem, char *namea, char *nameg, 
	char *formm, char *forma, char *formg, ftnlen specie_len, ftnlen 
	form_len, ftnlen namem_len, ftnlen namea_len, ftnlen nameg_len, 
	ftnlen formm_len, ftnlen forma_len, ftnlen formg_len)
{
    /* Builtin functions */
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);

    /* Parameter adjustments */
    formg -= 330;
    forma -= 330;
    formm -= 330;
    nameg -= 220;
    namea -= 220;
    namem -= 220;

    /* Function Body */
    if (*rec1 >= rlimit_1.rec1aa) {
	++reac2_1.na[*ireac - 1];
	reac2_1.coefa[*ireac + reac2_1.na[*ireac - 1] * 10 - 11] = *coeff;
	reac2_1.rec1a[*ireac + reac2_1.na[*ireac - 1] * 10 - 11] = *rec1;
	s_copy(namea + (*ireac + reac2_1.na[*ireac - 1] * 10) * 20, specie, (
		ftnlen)20, (ftnlen)20);
	s_copy(forma + (*ireac + reac2_1.na[*ireac - 1] * 10) * 30, form, (
		ftnlen)30, (ftnlen)30);
	return 0;
    }
    if (*rec1 >= rlimit_1.rec1gg) {
	++reac2_1.ng[*ireac - 1];
	reac2_1.coefg[*ireac + reac2_1.ng[*ireac - 1] * 10 - 11] = *coeff;
	reac2_1.rec1g[*ireac + reac2_1.ng[*ireac - 1] * 10 - 11] = *rec1;
	s_copy(nameg + (*ireac + reac2_1.ng[*ireac - 1] * 10) * 20, specie, (
		ftnlen)20, (ftnlen)20);
	s_copy(formg + (*ireac + reac2_1.ng[*ireac - 1] * 10) * 30, form, (
		ftnlen)30, (ftnlen)30);
    } else {
	++reac2_1.nm[*ireac - 1];
	reac2_1.coefm[*ireac + reac2_1.nm[*ireac - 1] * 10 - 11] = *coeff;
	reac2_1.rec1m[*ireac + reac2_1.nm[*ireac - 1] * 10 - 11] = *rec1;
	s_copy(namem + (*ireac + reac2_1.nm[*ireac - 1] * 10) * 20, specie, (
		ftnlen)20, (ftnlen)20);
	s_copy(formm + (*ireac + reac2_1.nm[*ireac - 1] * 10) * 30, form, (
		ftnlen)30, (ftnlen)30);
	if (*rec1 >= rlimit_1.rec1m2) {
	    reac2_1.m2reac[*ireac - 1] = TRUE_;
	}
    }
    return 0;
} /* umaker_ */

/* *********************************************************************** */
/* ** openf -  Returns .TRUE. and opens the file specified by fname, */
/* **          fstat, facces, fform, and frecl if this file exists and is */
/* **          accessible; otherwise, returns .FALSE. and prints an */
/* **          appropriate error message to the device specified by iterm. */
/* ** */
logical openf_(integer *iterm, integer *iunit, char *fname, integer *istat, 
	integer *iacces, integer *iform, integer *irecl, ftnlen fname_len)
{
    /* Initialized data */

    static char fform[11*2] = "FORMATTED  " "UNFORMATTED";
    static char facces[10*2] = "SEQUENTIAL" "DIRECT    ";
    static char fstat[3*2] = "OLD" "NEW";

    /* Format strings */
    static char fmt_20[] = "(/,\002 nonexistant file or invalid specificatio"
	    "ns\002,\002 ... try again\002,/)";

    /* System generated locals */
    integer i__1;
    logical ret_val;
    olist o__1;

    /* Builtin functions */
    integer f_open(olist *), s_wsfe(cilist *), e_wsfe(void);

    /* Fortran I/O blocks */
    static cilist io___470 = { 0, 0, 0, fmt_20, 0 };


    ret_val = FALSE_;
    if (*iacces < 1 || *iacces > 2 || *iform < 1 || *iform > 2 || *istat < 1 
	    || *istat > 2) {
	goto L10;
    }
    if (*iacces == 1) {
	o__1.oerr = 1;
	o__1.ounit = *iunit;
	o__1.ofnmlen = 20;
	o__1.ofnm = fname;
	o__1.orl = 0;
	o__1.osta = fstat + (*istat - 1) * 3;
	o__1.oacc = facces + (*iacces - 1) * 10;
	o__1.ofm = fform + (*iform - 1) * 11;
	o__1.oblnk = 0;
	i__1 = f_open(&o__1);
	if (i__1 != 0) {
	    goto L10;
	}
	ret_val = TRUE_;
	return ret_val;
    } else {
	o__1.oerr = 1;
	o__1.ounit = *iunit;
	o__1.ofnmlen = 20;
	o__1.ofnm = fname;
	o__1.orl = *irecl;
	o__1.osta = fstat + (*istat - 1) * 3;
	o__1.oacc = facces + (*iacces - 1) * 10;
	o__1.ofm = fform + (*iform - 1) * 11;
	o__1.oblnk = 0;
	i__1 = f_open(&o__1);
	if (i__1 != 0) {
	    goto L10;
	}
	ret_val = TRUE_;
	return ret_val;
    }
L10:
    io___470.ciunit = *iterm;
    s_wsfe(&io___470);
    e_wsfe();
    return ret_val;
} /* openf_ */

/* ** H2O92 - Computes state, thermodynamic, transport, and electroststic */
/* **         properties of fluid H2O at T,[P,D] using equations and data */
/* **         given by Haar et al. (1984), Levelt Sengers et al. (1983), */
/* **         Johnson and Norton (1991), Watson et al. (1980), Sengers and */
/* **         Kamgar-Parsi (1984), Sengers et al. (1984), Helgeson and Kirkham */
/* **         (1974), Uematsu and Franck (1980), and Pitzer (1983). */
/* ** */
/* ********************************************************************** */
/* ** */
/* ** Author:     James W. Johnson */
/* **             Earth Sciences Dept., L-219 */
/* **             Lawrence Livermore National Laboratory */
/* **             Livermore, CA 94550 */
/* **             johnson@s05.es.llnl.gov */
/* ** */
/* ** Abandoned:  8 November 1991 */
/* ** */
/* ********************************************************************** */

/*   specs  - Input unit, triple point, saturation, and option specs: */

/* ****        it, id, ip, ih, itripl, isat, iopt, useLVS, epseqn, icrit; */

/*            note that the returned value of isat may differ from */
/*            its input value and that icrit need not be specified */
/*            prior to invocation. */


/*   states - State variables: */

/* ****          temp, pres, dens(1), dens(2); */

/*            note that the first three of these must be specified prior */
/*            to invocation and that, in the case of saturation, vapor */
/*            density is returned in dens(1), liquid in dens(2). */


/*   props  - Thermodynamic, transport, electrostatic, and combined */
/*            property values: */

/* ****        A, G, S, U, H, Cv, Cp, Speed, alpha, beta, diel, visc, */
/* ****        tcond, surten, tdiff, Prndtl, visck, albe, */
/* ****        ZBorn, YBorn, QBorn, daldT, XBorn */


/*   error  - LOGICAL argument that indicates success ("FALSE") or */
/*            failure ("TRUE") of the call, the latter value in */
/*            response to out-of-bounds specs or states variables. */

/* ********************************************************************** */
/* Subroutine */ int h2o92_(integer *specs, doublereal *states, doublereal *
	props, logical *error)
{
    extern /* Subroutine */ int load_(integer *, doublereal *, doublereal *);
    static doublereal dens[2];
    extern /* Subroutine */ int unit_(integer *, integer *, integer *, 
	    integer *, integer *);
    extern logical valid_(integer *, integer *, integer *, integer *, integer 
	    *, integer *, integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal tempy;
    extern /* Subroutine */ int hgkeqn_(integer *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, integer *);
    extern logical crtreg_(integer *, integer *, integer *, doublereal *, 
	    doublereal *, doublereal *);
    extern doublereal tdegus_(integer *, doublereal *);
    extern /* Subroutine */ int lvseqn_(integer *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, integer *);
    static logical uselvs;

    /* Parameter adjustments */
    --props;
    --states;
    --specs;

    /* Function Body */
    unit_(&specs[1], &specs[2], &specs[3], &specs[4], &specs[5]);
    if (! valid_(&specs[1], &specs[2], &specs[3], &specs[4], &specs[5], &
	    specs[6], &specs[7], &specs[8], &specs[9], &states[1], &states[2],
	     &states[3])) {
	*error = TRUE_;
	return 0;
    } else {
	*error = FALSE_;
    }
    if (crtreg_(&specs[6], &specs[7], &specs[1], &states[1], &states[2], &
	    states[3])) {
	specs[10] = 1;
	uselvs = specs[8] == 1;
    } else {
	specs[10] = 0;
	uselvs = FALSE_;
    }
    if (uselvs) {
	dens[0] = states[3];
	lvseqn_(&specs[6], &specs[7], &specs[5], &states[1], &states[2], dens,
		 &specs[9]);
	dens[0] /= 1e3;
	if (specs[6] == 1) {
	    dens[1] /= 1e3;
	}
    } else {
	dens[0] = states[3] / 1e3;
	hgkeqn_(&specs[6], &specs[7], &specs[5], &states[1], &states[2], dens,
		 &specs[9]);
    }
    load_(&c__1, wpvals_1.wprops, &props[1]);
    if (specs[6] == 1) {
	tempy = dens[0];
	dens[0] = dens[1];
	dens[1] = tempy;
	load_(&c__2, wpvals_1.wpliq, &props[1]);
    }
    states[1] = tdegus_(&specs[1], &states[1]);
    states[2] *= units_1.fp;
    states[3] = dens[0] / units_1.fd;
    if (specs[6] == 1) {
	states[4] = dens[1] / units_1.fd;
    }
    return 0;
} /* h2o92_ */

/* *********************************************************************** */
/* ** valid - Returns "TRUE" if unit and equation specifications */
/*           are valid and input state conditions fall within */
/*           the HGK equation's region of validity; */
/*           returns "FALSE" otherwise. */
logical valid_(integer *it, integer *id, integer *ip, integer *ih, integer *
	itripl, integer *isat, integer *iopt, integer *uselvs, integer *
	epseqn, doublereal *temp, doublereal *pres, doublereal *dens)
{
    /* System generated locals */
    logical ret_val;

    /* Local variables */
    static doublereal d__, p, t;
    extern doublereal tdegk_(integer *, doublereal *);
    extern logical valtd_(doublereal *, doublereal *, integer *, integer *);
    static doublereal pcrit, tcrit;
    extern logical valtp_(doublereal *, doublereal *), valspc_(integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *, 
	    integer *, integer *);
    static doublereal ttripl;

/* ** ensure validity of input specifications */
    if (! valspc_(it, id, ip, ih, itripl, isat, iopt, uselvs, epseqn)) {
	ret_val = FALSE_;
	return ret_val;
    }
/* ** convert to  degC, bars, g/cm3 *** */
    t = tdegk_(it, temp) - 273.15;
    d__ = *dens * units_1.fd;
    p = *pres / units_1.fp * 10.;
    ttripl = tpoint_1.ttr - 273.15;
    tcrit = crits_1.tc - 273.15;
    pcrit = crits_1.pc * 10.;
    if (*isat == 0) {
	if (*iopt == 1) {
	    ret_val = valtd_(&t, &d__, isat, epseqn);
	} else {
	    ret_val = valtp_(&t, &p);
	}
    } else {
	if (*iopt == 1) {
	    ret_val = t + tolers_1.fptol >= ttripl && t - tolers_1.fptol <= 
		    tcrit;
	} else {
	    ret_val = p + tolers_1.fptol >= tpoint_1.ptripl && p - 
		    tolers_1.fptol <= pcrit;
	}
    }
    return ret_val;
} /* valid_ */

/* **************************************************************** */
/* ** valspc - Returns "TRUE" if  it, id, ip, ih, itripl, isat, iopt, */
/*            useLVS, and epseqn values all define valid input; */
/*            returns "FALSE" otherwise. */
logical valspc_(integer *it, integer *id, integer *ip, integer *ih, integer *
	itripl, integer *isat, integer *iopt, integer *uselvs, integer *
	epseqn)
{
    /* System generated locals */
    logical ret_val;

    ret_val = 1 <= *it && *it <= 4 && 1 <= *id && *id <= 4 && 1 <= *ip && *ip 
	    <= 5 && 1 <= *ih && *ih <= 6 && 0 <= *itripl && *itripl <= 1 && 0 
	    <= *isat && *isat <= 1 && 1 <= *iopt && *iopt <= 2 && 0 <= *
	    uselvs && *uselvs <= 1 && 1 <= *epseqn && *epseqn <= 5;
    return ret_val;
} /* valspc_ */

/* **************************************************************** */
/* ** valTD - Returns "TRUE" if  T-D  defines liquid or vapor H2O */
/*           within validity limits of the HGK equation of state; */
/*           returns "FALSE" otherwise. */
logical valtd_(doublereal *t, doublereal *d__, integer *isat, integer *epseqn)
{
    /* System generated locals */
    logical ret_val;

    /* Local variables */
    static doublereal p;
    extern /* Subroutine */ int bb_(doublereal *);
    static doublereal dl, dv, tk, ps, dpdd, pmpa;
    extern doublereal pfind_(integer *, doublereal *, doublereal *);
    static doublereal tcrit;
    extern /* Subroutine */ int pcorr_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *), denhgk_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);
    static doublereal dlimit, dsublm, dguess;
    extern /* Subroutine */ int denlvs_(integer *, doublereal *, doublereal *)
	    ;
    static integer istemp;
    extern doublereal psublm_(doublereal *);
    static doublereal ttripl;
#define tmnlvs ((doublereal *)&coefs_1 + 40)

    if (*t - tolers_1.fptol > hgkbnd_1.ttop || *t + tolers_1.fptol < 
	    hgkbnd_1.tbtm || *d__ - tolers_1.fptol > hgkbnd_1.dtop || *d__ + 
	    tolers_1.fptol < hgkbnd_1.dbtm) {
	ret_val = FALSE_;
	return ret_val;
    }
    tcrit = crits_1.tc - 273.15;
    ttripl = tpoint_1.ttr - 273.15;
    if (*t + tolers_1.fptol >= tcrit || *t >= liqice_1.tnib30 && *d__ >= 
	    tpoint_1.dltrip) {
	dlimit = liqice_1.sdib30 * (*t - liqice_1.tnib30) + hgkbnd_1.dtop;
	ret_val = *d__ - tolers_1.fptol <= dlimit;
    } else {
	if (*d__ - tolers_1.fptol <= tpoint_1.dltrip) {
	    if (*t >= ttripl) {
		ret_val = TRUE_;
		tk = *t + 273.15;
		if (tk < *tmnlvs) {
		    rtcurr_1.rt = aconst_1.gascon * tk;
		    pcorr_(&c__0, &tk, &ps, &dl, &dv, epseqn);
		} else {
		    istemp = 1;
		    satur_1.dh2o = 0.;
		    p = pfind_(&istemp, &tk, &satur_1.dh2o);
		    denlvs_(&istemp, &tk, &p);
		    dv = satur_1.dvap / 1e3;
		    dl = satur_1.dliq / 1e3;
		}
		if (*d__ >= dv && *d__ <= dl) {
		    *isat = 1;
		}
	    } else {
		p = psublm_(t);
		pmpa = p / 10.;
		tk = *t + 273.15;
		dguess = pmpa / tk / .4;
		rtcurr_1.rt = aconst_1.gascon * tk;
		bb_(&tk);
		denhgk_(&dsublm, &pmpa, &dguess, &tk, &dpdd);
		ret_val = *d__ - tolers_1.fptol <= dsublm;
	    }
	} else {
	    if (*d__ <= liqice_1.dli13) {
		dlimit = liqice_1.sdli1 * (*t - liqice_1.tli13) + 
			liqice_1.dli13;
		ret_val = *d__ + tolers_1.fptol >= dlimit;
	    } else {
		dlimit = liqice_1.sdli37 * (*t - liqice_1.tli13) + 
			liqice_1.dli13;
		ret_val = *d__ - tolers_1.fptol <= dlimit;
	    }
	}
    }
    return ret_val;
} /* valtd_ */

#undef tmnlvs


/* **************************************************************** */
/* ** valTP - Returns "TRUE" if  T-P  defines liquid or vapor H2O */
/*           within validity limits of the HGK equation of state; */
/*           returns "FALSE" otherwise. */
logical valtp_(doublereal *t, doublereal *p)
{
    /* System generated locals */
    logical ret_val;

    /* Local variables */
    static doublereal psubl, plimit;
    extern doublereal psublm_(doublereal *);

    if (*t - tolers_1.fptol > hgkbnd_1.ttop || *t + tolers_1.fptol < 
	    hgkbnd_1.tbtm || *p - tolers_1.fptol > hgkbnd_1.ptop || *p + 
	    tolers_1.fptol < hgkbnd_1.pbtm) {
	ret_val = FALSE_;
	return ret_val;
    } else {
	ret_val = TRUE_;
    }
    if (*p >= liqice_1.pli13) {
	plimit = liqice_1.spli37 * (*t - liqice_1.tli13) + liqice_1.pli13;
	ret_val = *p - tolers_1.fptol <= plimit;
    } else {
	if (*p >= tpoint_1.ptripl) {
	    plimit = liqice_1.spli1 * (*t - liqice_1.tli13) + liqice_1.pli13;
	    ret_val = *p + tolers_1.fptol >= plimit;
	} else {
	    psubl = psublm_(t);
	    ret_val = *p - tolers_1.fptol <= psubl;
	}
    }
    return ret_val;
} /* valtp_ */

/* **************************************************************** */
/* ** Psublm - Returns  Psublimation(T)  computed from the */
/*            equation given by  Washburn (1924): Monthly */
/*            Weather Rev., v.52, pp.488-490. */
doublereal psublm_(doublereal *temp)
{
    /* System generated locals */
    doublereal ret_val, d__1;

    /* Builtin functions */
    double d_lg10(doublereal *);

    /* Local variables */
    static doublereal t, pmmhg;
    extern doublereal power_(doublereal *, doublereal *);

    t = *temp + 273.1;
    d__1 = -2445.5646 / t + d_lg10(&t) * 8.2312 - t * .01677006 + t * 
	    1.20514e-5 * t - 6.757169;
    pmmhg = power_(&c_b915, &d__1);
/* ** convert mmHg to bars *** */
    ret_val = pmmhg * .00133322;
    return ret_val;
} /* psublm_ */

/* *********************************************************************** */
/* ** HGKcon - Constant parameters for the H2O equation of state */
/*            given by  Haar, Gallagher, & Kell (1984): */
/*            bp, bq     = b(j), B(j) from Table A.1, p.272 */
/*            g1, g2, gf = alpha, beta, gamma from eq (A.2), p.272 */
/*            g, ii, jj  = g(i), k(i), l(i) from eq (A.5), p.272. */
/*            Note that  tz < tcHGK. */
/*                 Tolerence limits required in various real & inexact */
/*            comparisons are set and stored in COMMON /tolers/. */
/* Subroutine */ int hgkcon_(void)
{
    return 0;
} /* hgkcon_ */


/* ******************************************************************** */
/* ** LVScon - Constant parameters for the H2O critical region equation */
/*            of state given by  Levelt Sengers, Kamgar-Parsi, Balfour, */
/*            & Sengers (1983). */
/* Subroutine */ int lvscon_(void)
{
    return 0;
} /* lvscon_ */

/*     EQUIVALENCE (cc,     a(1) ),  (pointA, q(1) ),  (Tmin1,  x(1)), */
/*    1            (p3,     a(2) ),  (pointB, q(2) ),  (Tmin2,  x(2)), */
/*    2            (delroc, a(3) ),  (delpc,  q(3) ),  (Tmax,   x(3)), */
/*    3            (p2,     a(4) ),  (Tc,     q(4) ),  (Dmin,   x(4)), */
/*    4            (p1,     a(5) ),  (rhoc,   q(5) ),  (Dmax,   x(5)), */
/*    5            (beta,   a(6) ),  (Pc,     q(6) ),  (Pmin1,  x(6)), */
/*    6            (xko,    a(7) ),  (dPcdTc, q(7) ),  (Pmin2,  x(7)), */
/*    7            (delTc,  a(8) ),  (slopdi, q(8) ),  (Pmax1,  x(8)), */
/*    8            (besq,   a(9) ),  (p11,    q(9) ),  (Pmax2,  x(9)), */
/*    9            (aa,     a(10)),  (alpha,  q(10)),  (sl1,    x(10)), */
/*    0            (delta,  a(11)),  (p00,    q(11)),  (sl2,    x(11)), */
/*    1            (k1,     a(12)),  (p20,    q(12)), */
/*    2            (muc,    a(13)),  (p40,    q(13)), */
/*    3            (mu1,    a(14)),  (deli,   q(14)), */
/*    4            (mu2,    a(15)),  (alh1,   q(15)), */
/*    5            (mu3,    a(16)),  (beti,   q(16)), */
/*    6            (s00,    a(17)),  (gami,   q(17)), */
/*    7            (s20,    a(18)),  (p01,    q(18)), */
/*    8            (s01,    a(19)),  (p21,    q(19)), */
/*    9            (s21,    a(20)),  (p41,    q(20)) */

/* ****************************************************************** */
/* ** unit - Sets internal parameters according to user-specified */
/*          choice of units.  Internal program units are degK(T), */
/*          and gm/cm**3(D); all other properties are computed in */
/*          dimensionless form and dimensioned at output time. */
/*          NOTE:  conversion factors for j/g ---> cal/(g,mole) */
/*          (ffh (4 & 5)) are consistent with those given in */
/*          Table 1, Helgeson & Kirkham (1974a) for thermal calories, */
/*          and differ slightly with those given by Haar et al (1984) */
/*          for international calories. */
/* Subroutine */ int unit_(integer *it, integer *id, integer *ip, integer *ih,
	 integer *itripl)
{
    /* Initialized data */

    static doublereal fft[4] = { 1.,1.,.555555556,.555555556 };
    static doublereal ffd[4] = { .001,1.,.0180152,.016018 };
    static doublereal ffvd[4] = { 1.,10.,.555086816,.671968969 };
    static doublereal ffvk[4] = { 1.,1e4,1e4,10.76391042 };
    static doublereal ffs[4] = { 1.,100.,100.,3.280833 };
    static doublereal ffp[5] = { 1.,10.,9.869232667,145.038,10.1971 };
    static doublereal ffh[6] = { 1.,1.,18.0152,.23901,4.305816,.4299226 };
    static doublereal ffst[4] = { 1.,1e3,55.5086816,2.205061 };
    static doublereal ffcd[4] = { 1.,.01,.01,.3048 };
    static doublereal ffch[6] = { .001,1.,1.,.23901,.23901,9.47244e-4 };

    extern /* Subroutine */ int tpset_(void);

    units_1.ft = fft[(0 + (0 + (*it - 1 << 3))) / 8];
    units_1.fd = ffd[*id - 1];
    units_1.fvd = ffvd[*id - 1];
    units_1.fvk = ffvk[*id - 1];
    units_1.fs = ffs[*id - 1];
    units_1.fp = ffp[*ip - 1];
    units_1.fh = ffh[*ih - 1];
    units_1.fst = ffst[*id - 1];
    units_1.fc = ffcd[*id - 1] * ffch[*ih - 1];
    if (*itripl == 1) {
	tpset_();
    }
    return 0;
} /* unit_ */

/* ********************************************************************** */
/* ** crtreg - Returns "TRUE" if input state conditions fall within */
/*            the critical region of H2O; otherwise returns "FALSE". */
/*            T, P, D, input in user-specified units, are returned in */
/*            degK, MPa, kg/m3. */
logical crtreg_(integer *isat, integer *iopt, integer *it, doublereal *t, 
	doublereal *p, doublereal *d__)
{
    /* System generated locals */
    logical ret_val;

    /* Local variables */
#define dmin__ ((doublereal *)&coefs_1 + 43)
#define dmax__ ((doublereal *)&coefs_1 + 44)
    static logical llim;
    static doublereal pmin, pmax;
    static logical ulim;
#define tmax ((doublereal *)&coefs_1 + 42)
    static integer isat1;
#define tmin1 ((doublereal *)&coefs_1 + 40)
#define tmin2 ((doublereal *)&coefs_1 + 41)
    extern doublereal tdegk_(integer *, doublereal *), pfind_(integer *, 
	    doublereal *, doublereal *);
#define pbase1 ((doublereal *)&coefs_1 + 45)
#define pbase2 ((doublereal *)&coefs_1 + 46)
    static doublereal ddummy;
#define ptmins ((doublereal *)&coefs_1 + 49)
#define ptmaxs ((doublereal *)&coefs_1 + 50)
    static doublereal pstest;

    *t = tdegk_(it, t);
    if (*isat == 0) {
	if (*iopt == 1) {
	    *d__ = *d__ * units_1.fd * 1e3;
	    ret_val = *t >= *tmin1 && *t <= *tmax && *d__ >= *dmin__ && *d__ 
		    <= *dmax__;
	} else {
	    *p /= units_1.fp;
	    if (*t < *tmin1 || *t > *tmax) {
		ret_val = FALSE_;
	    } else {
		pmin = *pbase1 + *ptmins * (*t - *tmin1);
		pmax = *pbase2 + *ptmaxs * (*t - *tmin2);
		llim = *p >= pmin;
		ulim = *p <= pmax;
		if (llim && ulim) {
		    ret_val = TRUE_;
		} else {
		    if (llim && *t <= *tmin2) {
			isat1 = 1;
			ddummy = 0.;
			pstest = pfind_(&isat1, t, &ddummy);
			ret_val = *p <= pstest;
		    } else {
			ret_val = FALSE_;
		    }
		}
	    }
	}
    } else {
	if (*iopt == 1) {
	    ret_val = *t >= *tmin1;
	} else {
	    *p /= units_1.fp;
	    ret_val = *p >= *pbase1;
	}
    }
    return ret_val;
} /* crtreg_ */

#undef ptmaxs
#undef ptmins
#undef pbase2
#undef pbase1
#undef tmin2
#undef tmin1
#undef tmax
#undef dmax__
#undef dmin__


/* ******************************************************************** */
/* ** HGKeqn - Computes thermodynamic and transport properties of */
/*            of H2O from the equation of state given by */
/*            Haar, Gallagher, & Kell (1984). */
/* Subroutine */ int hgkeqn_(integer *isat, integer *iopt, integer *itripl, 
	doublereal *temp, doublereal *pres, doublereal *dens, integer *epseqn)
{
    static integer i__;
    extern /* Subroutine */ int bb_(doublereal *), calcv3_(integer *, integer 
	    *, doublereal *, doublereal *, doublereal *, integer *), dimhgk_(
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    integer *), hgksat_(integer *, integer *, integer *, doublereal *,
	     doublereal *, doublereal *, integer *), thmhgk_(doublereal *, 
	    doublereal *);

    /* Parameter adjustments */
    --dens;

    /* Function Body */
    rtcurr_1.rt = aconst_1.gascon * *temp;
    hgksat_(isat, iopt, itripl, temp, pres, &dens[1], epseqn);
    if (*isat == 0) {
	bb_(temp);
	calcv3_(iopt, itripl, temp, pres, &dens[1], epseqn);
	thmhgk_(&dens[1], temp);
	dimhgk_(isat, itripl, temp, pres, &dens[1], epseqn);
    } else {
	for (i__ = 1; i__ <= 23; ++i__) {
/* L10: */
	    wpvals_1.wpliq[i__ - 1] = wpvals_1.wprops[i__ - 1];
	}
	dimhgk_(&c__2, itripl, temp, pres, &dens[2], epseqn);
    }
    return 0;
} /* hgkeqn_ */

/* **************************************************************** */
/* ** HGKsat - If  isat=1, computes  Psat(T) or Tsat(P) (iopt=1,2), */
/*            liquid and vapor densities, and associated */
/*            thermodynamic and transport properties. */
/*            If  isat=0, checks whether  T-D or T-P (iopt=1,2) */
/*            falls on or within  TOL  of the liquid-vapor */
/*            surface; if so, sets isat <- 1 and computes */
/*            properties. */
/* Subroutine */ int hgksat_(integer *isat, integer *iopt, integer *itripl, 
	doublereal *temp, doublereal *pres, doublereal *dens, integer *epseqn)
{
    /* System generated locals */
    doublereal d__1, d__2, d__3;

    /* Local variables */
    extern /* Subroutine */ int pcorr_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *);
    static doublereal ptemp;
    extern /* Subroutine */ int tcorr_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *);
    static doublereal dltemp, dvtemp;

    /* Parameter adjustments */
    --dens;

    /* Function Body */
    if (*isat == 1) {
	if (*iopt == 1) {
	    pcorr_(itripl, temp, pres, &dens[1], &dens[2], epseqn);
	} else {
	    tcorr_(itripl, temp, pres, &dens[1], &dens[2], epseqn);
	}
    } else {
	if (*temp > crits_1.tc || *temp < tpoint_3.ttripl || *iopt == 2 && *
		pres > crits_1.pc) {
	    return 0;
	} else {
	    pcorr_(itripl, temp, &ptemp, &dltemp, &dvtemp, epseqn);
	    if (*iopt == 2 && (d__1 = *pres - ptemp, abs(d__1)) <= 
		    tolers_1.ptol || *iopt == 1 && ((d__2 = dens[1] - dltemp, 
		    abs(d__2)) <= tolers_1.dtol || (d__3 = dens[1] - dvtemp, 
		    abs(d__3)) <= tolers_1.dtol)) {
		*isat = 1;
		*pres = ptemp;
		dens[1] = dltemp;
		dens[2] = dvtemp;
	    }
	}
    }
    return 0;
} /* hgksat_ */

/* *********************************************************************** */
/* ** calcv3 - Compute the dependent state variable. */
/* Subroutine */ int calcv3_(integer *iopt, integer *itripl, doublereal *temp,
	 doublereal *pres, doublereal *dens, integer *epseqn)
{
    static doublereal ps, dll, dvv;
    extern /* Subroutine */ int base_(doublereal *, doublereal *), ideal_(
	    doublereal *), resid_(doublereal *, doublereal *), pcorr_(integer 
	    *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    integer *), denhgk_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal dguess;

    if (*iopt == 1) {
	resid_(temp, dens);
	base_(dens, temp);
	ideal_(temp);
	*pres = rtcurr_1.rt * *dens * aconst_2.z__ + qqqq_1.q0;
    } else {
	if (*temp < aconst_2.tz) {
	    pcorr_(itripl, temp, &ps, &dll, &dvv, epseqn);
	} else {
	    ps = 2e4;
	    dll = 0.;
	}
	if (*pres > ps) {
	    dguess = dll;
	} else {
	    dguess = *pres / *temp / .4;
	}
	denhgk_(dens, pres, &dguess, temp, &fcts_1.dpdd);
	ideal_(temp);
    }
    return 0;
} /* calcv3_ */

/* ***************************************************************************** */
/* ** thmHGK - Computes thermodynamic functions in dimensionless */
/*            units from the HGK equation of state:  Helmholtz, Gibbs, */
/*            internal energy, and enthalpy functions (ad, gd, ud, hd) are */
/*            per RT; entropy and heat capacities (sd, cvd, cpd) are per R. */
/* Subroutine */ int thmhgk_(doublereal *d__, doublereal *t)
{
    static doublereal z__;

    z__ = aconst_3.zb + qqqq_2.qp / rtcurr_1.rt / *d__;
    fcts_1.dpdd = rtcurr_1.rt * (aconst_3.zb + aconst_3.y * aconst_3.dzb) + 
	    qqqq_2.qdp;
    fcts_1.ad = basef_1.ab + resf_1.ar + idf_1.ai - aconst_3.uref / *t + 
	    aconst_3.sref;
    fcts_1.gd = fcts_1.ad + z__;
    fcts_1.ud = basef_1.ub + resf_1.ur + idf_1.ui - aconst_3.uref / *t;
    fcts_1.dpdt = rtcurr_1.rt * *d__ * basef_1.dpdtb + resf_1.dpdtr;
    fcts_1.cvd = basef_1.cvb + resf_1.cvr + idf_1.cvi;
    fcts_1.cpd = fcts_1.cvd + *t * fcts_1.dpdt * fcts_1.dpdt / (*d__ * *d__ * 
	    fcts_1.dpdd * aconst_3.gascon);
    fcts_1.hd = fcts_1.ud + z__;
    fcts_1.sd = basef_1.sb + resf_1.sr + idf_1.si - aconst_3.sref;
    fcts_1.dvdt = fcts_1.dpdt / fcts_1.dpdd / *d__ / *d__;
    fcts_1.cjtt = 1. / *d__ - *t * fcts_1.dvdt;
    fcts_1.cjth = -fcts_1.cjtt / fcts_1.cpd / aconst_3.gascon;
    return 0;
} /* thmhgk_ */

/* ************************************************************************ */
/* ** bb - Computes molecular parameters b, the "excluded volume" */
/*        (eq A.3), and B, the second virial coefficient (eq A.4), */
/*        in cm3/g (b1,b2) and their first and second derivatives */
/*        with respect to temperature (b1t,b1tt,b2t,b2tt). */
/* Subroutine */ int bb_(doublereal *t)
{
    /* Builtin functions */
    double log(doublereal);

    /* Local variables */
    static integer i__;
    static doublereal v[10];

    v[0] = 1.;
    for (i__ = 2; i__ <= 10; ++i__) {
/* L2: */
	v[i__ - 1] = v[i__ - 2] * aconst_2.tz / *t;
    }
    ellcon_1.b1 = bconst_1.bp[0] + bconst_1.bp[1] * log(1.f / v[1]);
    ellcon_1.b2 = bconst_1.bq[0];
    ellcon_1.b1t = bconst_1.bp[1] * v[1] / aconst_2.tz;
    ellcon_1.b2t = 0.;
    ellcon_1.b1tt = 0.;
    ellcon_1.b2tt = 0.;
    for (i__ = 3; i__ <= 10; ++i__) {
	ellcon_1.b1 += bconst_1.bp[i__ - 1] * v[i__ - 2];
	ellcon_1.b2 += bconst_1.bq[i__ - 1] * v[i__ - 2];
	ellcon_1.b1t -= (i__ - 2) * bconst_1.bp[i__ - 1] * v[i__ - 2] / *t;
	ellcon_1.b2t -= (i__ - 2) * bconst_1.bq[i__ - 1] * v[i__ - 2] / *t;
	ellcon_1.b1tt += bconst_1.bp[i__ - 1] * (i__ - 2) * (i__ - 2) * v[i__ 
		- 2] / *t / *t;
/* L4: */
	ellcon_1.b2tt += bconst_1.bq[i__ - 1] * (i__ - 2) * (i__ - 2) * v[i__ 
		- 2] / *t / *t;
    }
    ellcon_1.b1tt -= ellcon_1.b1t / *t;
    ellcon_1.b2tt -= ellcon_1.b2t / *t;
    return 0;
} /* bb_ */

/* ********************************************************************** */
/* ** base - Computes Abase, Gbase, Sbase, Ubase, Hbase, Cvbase */
/*          -- all per RT (dimensionless) --  as well as Pbase & dP/dT */
/*          -- both per (DRT) -- for the base function (ab, gb, sb, ub, */
/*          hb, cvb, pb, dpdtb).  See Haar, Gallagher & Kell (1979), eq(1). */
/* Subroutine */ int base_(doublereal *d__, doublereal *t)
{
    /* Builtin functions */
    double log(doublereal);

    /* Local variables */
    static doublereal x, z0, dz0, bb2tt;

    aconst_4.y = ellcon_1.b1 * .25 * *d__;
    x = 1. - aconst_4.y;
    z0 = (ellcon_1.g1 * aconst_4.y + 1. + ellcon_1.g2 * aconst_4.y * 
	    aconst_4.y) / (x * x * x);
    aconst_4.z__ = z0 + aconst_4.y * 4. * (ellcon_1.b2 / ellcon_1.b1 - 
	    ellcon_1.gf);
    dz0 = (ellcon_1.g1 + ellcon_1.g2 * 2. * aconst_4.y) / (x * x * x) + (
	    ellcon_1.g1 * aconst_4.y + 1. + ellcon_1.g2 * aconst_4.y * 
	    aconst_4.y) * 3. / (x * x * x * x);
    aconst_4.dz = dz0 + (ellcon_1.b2 / ellcon_1.b1 - ellcon_1.gf) * 4.;
    basef_1.pb = aconst_4.z__;
    basef_1.ab = -log(x) - (ellcon_1.g2 - 1.) / x + 28.16666667 / x / x + 
	    aconst_4.y * 4. * (ellcon_1.b2 / ellcon_1.b1 - ellcon_1.gf) + 
	    15.166666667 + log(*d__ * *t * aconst_4.gascon / .101325);
    basef_1.gb = basef_1.ab + aconst_4.z__;
    basef_1.ub = -(*t) * ellcon_1.b1t * (aconst_4.z__ - 1. - *d__ * 
	    ellcon_1.b2) / ellcon_1.b1 - *d__ * *t * ellcon_1.b2t;
    basef_1.sb = basef_1.ub - basef_1.ab;
    basef_1.hb = aconst_4.z__ + basef_1.ub;
    bb2tt = *t * *t * ellcon_1.b2tt;
    basef_1.cvb = basef_1.ub * 2. + (z0 - 1.) * (*t * ellcon_1.b1t / 
	    ellcon_1.b1 * (*t * ellcon_1.b1t / ellcon_1.b1) - *t * *t * 
	    ellcon_1.b1tt / ellcon_1.b1) - *d__ * (bb2tt - ellcon_1.gf * 
	    ellcon_1.b1tt * *t * *t) - *t * ellcon_1.b1t / ellcon_1.b1 * (*t *
	     ellcon_1.b1t / ellcon_1.b1) * aconst_4.y * dz0;
    basef_1.dpdtb = basef_1.pb / *t + *d__ * (aconst_4.dz * ellcon_1.b1t / 4. 
	    + ellcon_1.b2t - ellcon_1.b2 / ellcon_1.b1 * ellcon_1.b1t);
    return 0;
} /* base_ */

/* ********************************************************************** */
/* ** resid - Computes residual contributions to pressure (q), the */
/*           Helmloltz function (ar) , dP/dD (q5), the Gibbs function */
/*           (gr), entropy (sr), internal energy (ur), enthalpy (hr), */
/*           isochoric heat capacity (cvr), and dP/dT.  The first 36 */
/*           terms of the residual function represent a global */
/*           least-squares fit to experimental data outside the */
/*           critical region, terms 37-39 affect only the immediate */
/*           vicinity of the critical point, and the last term (40) */
/*           contributes only in the high pressure, low temperature */
/*           region. */
/* Subroutine */ int resid_(doublereal *t, doublereal *d__)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;
    static doublereal equiv_0[11], equiv_1[10];

    /* Builtin functions */
    double exp(doublereal);

    /* Local variables */
    static doublereal e;
    static integer i__, j, k, l;
    static doublereal v, q10, q20;
    static integer km;
    static doublereal qm, qp;
#define qr (equiv_0)
#define qt (equiv_1)
    static doublereal tx, zz, d2f, q2a, ex1, ex2, q5t, del, fct, dex, ddz, 
	    dpt, att, tau, tex;
#define qzr (equiv_0 + 2)
#define qzt (equiv_1 + 1)
    static doublereal dadt, dfdt;
    extern doublereal power_(doublereal *, doublereal *);

    qr[0] = 0.;
    qqqq_3.q5 = 0.;
    qqqq_3.q = 0.;
    resf_1.ar = 0.;
    dadt = 0.;
    resf_1.cvr = 0.;
    resf_1.dpdtr = 0.;
    e = exp(-aconst_2.aa * *d__);
    q10 = *d__ * *d__ * e;
    q20 = 1. - e;
    qr[1] = q10;
    v = aconst_2.tz / *t;
    qt[0] = *t / aconst_2.tz;
    for (i__ = 2; i__ <= 10; ++i__) {
	qr[i__] = qr[i__ - 1] * q20;
/* L4: */
	qt[i__ - 1] = qt[i__ - 2] * v;
    }
    i__1 = nconst_2.n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	k = nconst_2.ii[i__ - 1] + 1;
	l = nconst_2.jj[i__ - 1];
	zz = (doublereal) k;
	if (k == 1) {
	    qp = nconst_2.g[i__ - 1] * aconst_2.aa * qr[1] * qzt[l - 1];
	} else {
	    qp = nconst_2.g[i__ - 1] * aconst_2.aa * qzr[k - 2] * qzt[l - 1];
	}
	qqqq_3.q += qp;
	qqqq_3.q5 += aconst_2.aa * (2.f / *d__ - aconst_2.aa * (1.f - e * (k 
		- 1) / q20)) * qp;
	resf_1.ar += nconst_2.g[i__ - 1] * qzr[k - 1] * qzt[l - 1] / q10 / zz 
		/ rtcurr_1.rt;
	d__1 = (doublereal) k;
	dfdt = power_(&q20, &d__1) * (1 - l) * qzt[l] / aconst_2.tz / k;
	d2f = l * dfdt;
	dpt = dfdt * q10 * aconst_2.aa * k / q20;
	dadt += nconst_2.g[i__ - 1] * dfdt;
	resf_1.dpdtr += nconst_2.g[i__ - 1] * dpt;
/* L10: */
	resf_1.cvr += nconst_2.g[i__ - 1] * d2f / aconst_2.gascon;
    }
    qp = 0.;
    q2a = 0.;
    for (j = 37; j <= 40; ++j) {
	if (nconst_2.g[j - 1] == 0.) {
	    goto L20;
	}
	k = nconst_2.ii[j - 1];
	km = nconst_2.jj[j - 1];
	ddz = addcon_1.adz[j - 37];
	del = *d__ / ddz - 1.;
	if (abs(del) < 1e-10) {
	    del = 1e-10;
	}
	d__1 = (doublereal) k;
	ex1 = -addcon_1.aad[j - 37] * power_(&del, &d__1);
	if (ex1 < tolers_1.exptol) {
	    dex = 0.;
	} else {
	    d__1 = (doublereal) km;
	    dex = exp(ex1) * power_(&del, &d__1);
	}
	att = addcon_1.aat[j - 37];
	tx = addcon_1.atz[j - 37];
	tau = *t / tx - 1.;
	ex2 = -att * tau * tau;
	if (ex2 <= tolers_1.exptol) {
	    tex = 0.;
	} else {
	    tex = exp(ex2);
	}
	q10 = dex * tex;
	d__1 = (doublereal) (k - 1);
	qm = km / del - k * addcon_1.aad[j - 37] * power_(&del, &d__1);
	fct = qm * *d__ * *d__ * q10 / ddz;
	d__1 = (doublereal) (k - 2);
	q5t = fct * (2. / *d__ + qm / ddz) - *d__ / ddz * (*d__ / ddz) * q10 *
		 (km / del / del + k * (k - 1) * addcon_1.aad[j - 37] * 
		power_(&del, &d__1));
	qqqq_3.q5 += q5t * nconst_2.g[j - 1];
	qp += nconst_2.g[j - 1] * fct;
	dadt -= nconst_2.g[j - 1] * 2. * att * tau * q10 / tx;
	resf_1.dpdtr -= nconst_2.g[j - 1] * 2. * att * tau * fct / tx;
	q2a += *t * nconst_2.g[j - 1] * (att * 4. * ex2 + att * 2.) * q10 / 
		tx / tx;
	resf_1.ar += q10 * nconst_2.g[j - 1] / rtcurr_1.rt;
L20:
	;
    }
    resf_1.sr = -dadt / aconst_2.gascon;
    resf_1.ur = resf_1.ar + resf_1.sr;
    resf_1.cvr += q2a / aconst_2.gascon;
    qqqq_3.q += qp;
    return 0;
} /* resid_ */

#undef qzt
#undef qzr
#undef qt
#undef qr


/* *********************************************************************** */
/* ** ideal - Computes thermodynamic properties for H2O in the */
/*           ideal gas state using equations given by Woolley (1979). */
/* Subroutine */ int ideal_(doublereal *t)
{
    /* Initialized data */

    static doublereal c__[18] = { 19.730271018,20.9662681977,-.483429455355,
	    6.05743189245,22.56023885,-9.87532442,-4.3135538513,.458155781,
	    -.047754901883,.0041238460633,-2.7929052852e-4,1.4481695261e-5,
	    -5.6473658748e-7,1.6200446e-8,-3.303822796e-10,4.51916067368e-12,
	    -3.70734122708e-14,1.37546068238e-16 };

    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double log(doublereal);

    /* Local variables */
    static integer i__;
    static doublereal tl, tt, emult;
    extern doublereal power_(doublereal *, doublereal *);

    tt = *t / 100.;
    tl = log(tt);
    idf_1.gi = -(c__[0] / tt + c__[1]) * tl;
    idf_1.hi = c__[1] + c__[0] * (1. - tl) / tt;
    idf_1.cpi = c__[1] - c__[0] / tt;
    for (i__ = 3; i__ <= 18; ++i__) {
	d__1 = (doublereal) (i__ - 6);
	emult = power_(&tt, &d__1);
	idf_1.gi -= c__[i__ - 1] * emult;
	idf_1.hi += c__[i__ - 1] * (i__ - 6) * emult;
/* L8: */
	idf_1.cpi += c__[i__ - 1] * (i__ - 6) * (i__ - 5) * emult;
    }
    idf_1.ai = idf_1.gi - 1.;
    idf_1.ui = idf_1.hi - 1.;
    idf_1.cvi = idf_1.cpi - 1.;
    idf_1.si = idf_1.ui - idf_1.ai;
    return 0;
} /* ideal_ */

/* ***************************************************************************** */
/* ** dalHGK - Computes/returns (d(alpha)/dt)p(d,t,alpha) */
/*            for the Haar et al. (1983) equation of state. */
doublereal dalhgk_(doublereal *d__, doublereal *t, doublereal *alpha)
{
    /* System generated locals */
    integer i__1;
    doublereal ret_val, d__1, d__2, d__3, d__4, d__5;

    /* Builtin functions */
    double exp(doublereal);

    /* Local variables */
    static integer i__;
    static doublereal k, l, x, e1, e2, ai, bi, di, km, lm, kp, lp, ti, qm, 
	    ex1, ex2, del, ex12, dex, tau, tex, tzt, dbdd, drdd, db2dd, dr2dd,
	     xtzt, xdelk, xdell, dydtp;
    extern doublereal power_(doublereal *, doublereal *);
    static doublereal db2ddt, db3ddt, db2dtp, dr2ddt, db3dtt, dr3ddt, dr2dtp, 
	    dr3dtt;

/* ** evaluate derivatives for the base function */
    aconst_4.y = ellcon_1.b1 * .25 * *d__;
    x = 1. - aconst_4.y;
    dydtp = *d__ / 4. * (ellcon_1.b1t - ellcon_1.b1 * *alpha);
    dbdd = aconst_4.gascon * *t * (ellcon_1.b1 / 4. / x * (1. - (ellcon_1.g2 
	    - 1.) / x + (ellcon_1.g1 + ellcon_1.g2 + 1.) / x / x) + 
	    ellcon_1.b2 - ellcon_1.b1 * ellcon_1.gf + 1. / *d__);
    db2dd = aconst_4.gascon * *t * (ellcon_1.b1 * ellcon_1.b1 / 16. / x / x * 
	    (1. - (ellcon_1.g2 - 1.) * 2. / x + (ellcon_1.g1 + ellcon_1.g2 + 
	    1.) * 3. / x / x) - 1. / *d__ / *d__);
    db2ddt = aconst_4.gascon * *t * (ellcon_1.b1t / 4. / x / x * (1. - (
	    ellcon_1.g2 - 1.) * (aconst_4.y + 1.) / x + (ellcon_1.g1 + 
	    ellcon_1.g2 + 1.) * (aconst_4.y * 2. + 1.) / x / x) + 
	    ellcon_1.b2t - ellcon_1.gf * ellcon_1.b1t) + dbdd / *t;
    db2dtp = dbdd / *t + aconst_4.gascon * *t * (ellcon_1.b1 * dydtp / 4. / x 
	    / x / x * (1. - ellcon_1.g2 + (ellcon_1.g1 + ellcon_1.g2 + 1.) * 
	    2. / x) + (x * ellcon_1.b1t + ellcon_1.b1 * dydtp) / 4. / x / x * 
	    (1. - (ellcon_1.g2 - 1.) / x + (ellcon_1.g1 + ellcon_1.g2 + 1.) / 
	    x / x) + ellcon_1.b2t - ellcon_1.gf * ellcon_1.b1t + *alpha / *
	    d__);
    db3ddt = db2dd / *t + aconst_4.gascon * *t * (ellcon_1.b1 * ellcon_1.b1 * 
	    dydtp / 8. / x / x / x / x * (1. - ellcon_1.g2 + (ellcon_1.g1 + 
	    ellcon_1.g2 + 1.) * 3. / x) + ellcon_1.b1 * (x * ellcon_1.b1t + 
	    ellcon_1.b1 * dydtp) / 8. / x / x / x * (1. - (ellcon_1.g2 - 1.) *
	     2. / x + (ellcon_1.g1 + ellcon_1.g2 + 1.) * 3. / x / x) - *alpha 
	    * 2. / *d__ / *d__);
    db3dtt = (db2ddt - dbdd / *t) / *t + aconst_4.gascon * *t * (ellcon_1.b1t 
	    * dydtp / 2. / x / x / x / x * (1. - ellcon_1.g2 + (ellcon_1.g1 + 
	    ellcon_1.g2 + 1.) * (aconst_4.y + 2.) / x) + (x * ellcon_1.b1tt + 
	    ellcon_1.b1t * 2. * dydtp) / 4. / x / x / x * (1. - (ellcon_1.g2 
	    - 1.) * (aconst_4.y + 1) / x + (ellcon_1.g1 + ellcon_1.g2 + 1.) * 
	    (aconst_4.y * 2. + 1.) / x / x) + ellcon_1.b2tt - ellcon_1.gf * 
	    ellcon_1.b1tt) + (*t * db2dtp - dbdd) / *t / *t;
/* ********************************************************** */
/* ** evaluate derivatives for the residual function */
/*      drdd   = q/d/d */
/*      dr2dd  = (q5 - 2.0d0/d*q)/d/d */
/*      dr2ddt = dpdtr/d/d */
    e1 = exp(-aconst_4.a * *d__);
    e2 = 1. - e1;
    tzt = aconst_4.tz / *t;
    drdd = 0.;
    dr2dd = 0.;
    dr2ddt = 0.;
    dr2dtp = 0.;
    dr3ddt = 0.;
    dr3dtt = 0.;
/* ** evaluate terms 1-36 */
    i__1 = nconst_3.n;
    for (i__ = 1; i__ <= i__1; ++i__) {
	k = (doublereal) nconst_3.kk[i__ - 1] + 1.;
	l = (doublereal) nconst_3.ll[i__ - 1] - 1.;
	km = k - 1.;
	lm = l - 1.;
	kp = k + 1.;
	lp = l + 1.;
	xtzt = power_(&tzt, &l);
	drdd += nconst_3.g[i__ - 1] * xtzt * power_(&e2, &km) * e1;
	dr2dd += nconst_3.g[i__ - 1] * e1 * xtzt * power_(&e2, &km) * (km * 
		e1 / e2 - 1.);
	dr2ddt -= nconst_3.g[i__ - 1] * e1 * l * power_(&e2, &km) * power_(&
		tzt, &lp) / aconst_4.tz;
	dr2dtp += nconst_3.g[i__ - 1] * e1 * power_(&e2, &km) * xtzt * (*d__ *
		 *alpha - l / *t - km * e1 * *d__ * *alpha / e2);
	d__1 = k - 3.;
	dr3ddt += nconst_3.g[i__ - 1] * (km * *d__ * *alpha * e1 * e1 * xtzt *
		 power_(&e2, &d__1) + e1 * xtzt * power_(&e2, &km) * (km * e1 
		/ e2 - 1.) * (*d__ * *alpha - l / *t - km * *d__ * *alpha * 
		e1 / e2));
	dr3dtt += nconst_3.g[i__ - 1] * l * e1 * power_(&e2, &km) * power_(&
		tzt, &lp) / aconst_4.tz * (lp / *t + *d__ * *alpha * km * e1 /
		 e2 - *d__ * *alpha);
/* L10: */
    }
/* ** evaluate terms 37-40 */
    for (i__ = 37; i__ <= 40; ++i__) {
	k = (doublereal) nconst_3.kk[i__ - 1];
	l = (doublereal) nconst_3.ll[i__ - 1];
	km = k - 1.;
	lm = l - 1.;
	kp = k + 1.;
	lp = l + 1.;
	ai = addcon_2.alphai[i__ - 37];
	bi = addcon_2.betai[i__ - 37];
	di = addcon_2.densi[i__ - 37];
	ti = addcon_2.tempi[i__ - 37];
	tau = *t / ti - 1.;
	del = *d__ / di - 1.;
	if (abs(del) < 1e-10) {
	    del = 1e-10;
	}
	ex1 = -ai * power_(&del, &k);
	if (ex1 < tolers_1.exptol) {
	    dex = 0.;
	} else {
	    dex = exp(ex1);
	}
	ex2 = -bi * tau * tau;
	if (ex2 <= tolers_1.exptol) {
	    tex = 0.;
	} else {
	    tex = exp(ex2);
	}
	ex12 = dex * tex;
	qm = l / del - k * ai * power_(&del, &km);
	xdell = power_(&del, &l);
	xdelk = power_(&del, &k);
	drdd += nconst_3.g[i__ - 1] * xdell * ex12 / di * qm;
	d__1 = k - 2.;
	dr2dd += nconst_3.g[i__ - 1] * xdell * ex12 / di / di * (qm * qm - l /
		 di / di - ai * k * km * power_(&del, &d__1));
	dr2ddt -= nconst_3.g[i__ - 1] * 2. * bi * tau * ex12 * xdell / ti / 
		di * qm;
	dr2dtp += nconst_3.g[i__ - 1] / di * (*d__ * *alpha * xdell * ex12 / 
		di / del / del * (l + ai * k * km * xdelk) + qm * (ex12 * (
		xdell * (k * ai * *d__ * *alpha * power_(&del, &km) / di - bi 
		* 2. * tau / ti) - l * *d__ * *alpha * power_(&del, &lm) / di)
		));
	d__1 = k - 2.;
	d__2 = k - 3.;
	d__3 = k - 2.;
	d__4 = k - 1.;
	d__5 = l - 1.;
	dr3ddt += nconst_3.g[i__ - 1] / di / di * (xdell * ex12 * (qm * 2. * (
		l * *d__ * *alpha / di / del / del + ai * k * km * *d__ * *
		alpha * power_(&del, &d__1) / di) - l * 2. * *d__ * *alpha / 
		di / del / del / del + ai * k * km * (k - 2.) * power_(&del, &
		d__2) * *d__ * *alpha / di) + (qm * qm - l / del / del - ai * 
		k * km * power_(&del, &d__3)) * (ex12 * xdell * (ai * k * 
		power_(&del, &d__4) * *d__ * *alpha / di - bi * 2. * tau / ti)
		 - ex12 * l * power_(&del, &d__5) * *d__ * *alpha / di));
	dr3dtt -= nconst_3.g[i__ - 1] * 2. * bi / ti / di * (tau * xdell * 
		ex12 * *d__ * *alpha / del / del / di * (l + ai * k * km * 
		power_(&del, &k)) + qm * (xdell * ex12 * (ai * k * *d__ * *
		alpha * tau * power_(&del, &km) / di + (1. - bi * 2. * tau * 
		tau) / ti - tau * l * *d__ * *alpha / di / del)));
/* L20: */
    }
/* ** compute (d(alpha)/dT)P */
    ret_val = ((db3dtt + dr3dtt) * ((dbdd + drdd) * 2. + *d__ * (db2dd + 
	    dr2dd)) - (db2ddt + dr2ddt) * ((db2dtp + dr2dtp) * 2. + *d__ * (
	    db3ddt + dr3ddt) - *d__ * *alpha * (db2dd + dr2dd))) / ((dbdd + 
	    drdd) * 2. + *d__ * (db2dd + dr2dd)) / ((dbdd + drdd) * 2. + *d__ 
	    * (db2dd + dr2dd));
    return ret_val;
} /* dalhgk_ */

/* ***************************************************************************** */
/* ** denHGK - Computes density (d in g/cm3) and dP/dD (dPdd) as */
/*            f(p(MPa),t(degK)) from an initial density guess (dguess). */
/* Subroutine */ int denhgk_(doublereal *d__, doublereal *p, doublereal *
	dguess, doublereal *t, doublereal *dpdd)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    static integer i__;
    static doublereal x, dp, pp;
    extern /* Subroutine */ int base_(doublereal *, doublereal *);
    static doublereal dpdx;
    extern /* Subroutine */ int resid_(doublereal *, doublereal *);

    i__ = 0;
    *d__ = *dguess;
L10:
    ++i__;
    if (*d__ <= 0.) {
	*d__ = 1e-8;
    }
    if (*d__ > 1.9) {
	*d__ = 1.9;
    }
    resid_(t, d__);
    base_(d__, t);
    pp = rtcurr_1.rt * *d__ * basef_1.pb + qqqq_1.q0;
    *dpdd = rtcurr_1.rt * (aconst_2.z__ + aconst_2.y * aconst_2.dz) + 
	    qqqq_1.q5;
/* ** if  dpdd < 0  assume d in 2-phase region and adjust accordingly *** */
    if (*dpdd > 0.) {
	goto L20;
    }
    if (*dguess >= .2967) {
	*d__ *= 1.02;
    }
    if (*dguess < .2967) {
	*d__ *= .98;
    }
    if (i__ <= 10) {
	goto L10;
    }
L20:
    dpdx = *dpdd * 1.1;
    if (dpdx < .1) {
	dpdx = .1;
    }
    dp = (d__1 = 1. - pp / *p, abs(d__1));
    if (dp < 1e-8 || *dguess > .3 && dp < 1e-7 || *dguess > .7 && dp < 1e-6) {
	return 0;
    }
    x = (*p - pp) / dpdx;
    if (abs(x) > .1) {
	x = x * .1 / abs(x);
    }
    *d__ += x;
    if (*d__ <= 0.) {
	*d__ = 1e-8;
    }
    if (i__ <= 30) {
	goto L10;
    }
    return 0;
} /* denhgk_ */

/* ********************************************************************** */
/* ** PsHGK - Returns an approximation to Psaturation(T) that agrees */
/*           to within 0.02% of that predicted by the HGK surface */
/*           for temperatures up to within roughly a degree of */
/*           the critical point. */
doublereal pshgk_(doublereal *t)
{
    /* Initialized data */

    static doublereal a[8] = { -7.8889166,2.5514255,-6.716169,33.239495,
	    -105.38479,174.35319,-148.39348,48.631602 };

    /* System generated locals */
    doublereal ret_val, d__1;

    /* Builtin functions */
    double exp(doublereal);

    /* Local variables */
    static doublereal b;
    static integer i__;
    static doublereal q, v, w, z__, pl;
    extern doublereal power_(doublereal *, doublereal *);

    if (*t <= 314.) {
	pl = 6.3573118 - 8858.843 / *t + power_(t, &c_b943) * 607.56335;
	ret_val = exp(pl) * .1;
    } else {
	v = *t / 647.25;
	w = (d__1 = 1. - v, abs(d__1));
	b = 0.;
	for (i__ = 1; i__ <= 8; ++i__) {
	    z__ = (doublereal) i__;
	    d__1 = (z__ + 1.) / 2.;
	    b += a[i__ - 1] * power_(&w, &d__1);
/* L4: */
	}
	q = b / v;
	ret_val = exp(q) * 22.093;
    }
    return ret_val;
} /* pshgk_ */

/* ********************************************************************** */
/* ** TsHGK - Returns Tsaturation(P). */
doublereal tshgk_(doublereal *p)
{
    /* System generated locals */
    doublereal ret_val, d__1;

    /* Builtin functions */
    double log(doublereal);

    /* Local variables */
    static integer k;
    static doublereal dp, tg, pl, pp;
    extern doublereal pshgk_(doublereal *), tdpsdt_(doublereal *);

    ret_val = 0.;
    if (*p > 22.05) {
	return ret_val;
    }
    k = 0;
    pl = log(*p) + 2.302585;
    tg = pl * (pl * (pl * (pl * .0193855 + .24834) + 2.3819) + 27.7589) + 
	    372.83;
L1:
    if (tg < 273.15) {
	tg = 273.15;
    }
    if (tg > 647.) {
	tg = 647.;
    }
    if (k >= 8) {
	ret_val = tg;
    } else {
	++k;
	pp = pshgk_(&tg);
	dp = tdpsdt_(&tg);
	if ((d__1 = 1. - pp / *p, abs(d__1)) < 1e-5) {
	    ret_val = tg;
	} else {
	    tg *= (*p - pp) / dp + 1.;
	    goto L1;
	}
    }
    return ret_val;
} /* tshgk_ */

/* ********************************************************************** */
/* ** TdPsdT - Returns  T*(dPsat/dT). */
doublereal tdpsdt_(doublereal *t)
{
    /* Initialized data */

    static doublereal a[8] = { -7.8889166,2.5514255,-6.716169,33.239495,
	    -105.38479,174.35319,-148.39348,48.631602 };

    /* System generated locals */
    doublereal ret_val, d__1;

    /* Builtin functions */
    double exp(doublereal);

    /* Local variables */
    static doublereal b, c__;
    static integer i__;
    static doublereal q, v, w, y, z__;
    extern doublereal power_(doublereal *, doublereal *);

    v = *t / 647.25;
    w = 1.f - v;
    b = 0.;
    c__ = 0.;
    for (i__ = 1; i__ <= 8; ++i__) {
	z__ = (doublereal) i__;
	d__1 = (z__ + 1.) / 2.;
	y = a[i__ - 1] * power_(&w, &d__1);
	c__ += y / w * (.5 - z__ * .5 - 1. / v);
/* L4: */
	b += y;
    }
    q = b / v;
    ret_val = exp(q) * 22.093 * c__;
    return ret_val;
} /* tdpsdt_ */

/* ********************************************************************** */
/* ** corr - Computes liquid and vapor densities (dliq & dvap) */
/*          and  (Gl-Gv)/RT  (delg) for T-P conditions on or */
/*          near the saturation surface. */
/* Subroutine */ int corr_(integer *itripl, doublereal *t, doublereal *p, 
	doublereal *dl, doublereal *dv, doublereal *delg, integer *epseqn)
{
    extern /* Subroutine */ int bb_(doublereal *);
    static doublereal gl, gv;
    extern /* Subroutine */ int ideal_(doublereal *), denhgk_(doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *), dimhgk_(
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    integer *), thmhgk_(doublereal *, doublereal *);
    static doublereal dguess;

    bb_(t);
    dguess = *dl;
    if (*dl <= 0.) {
	dguess = 1.11 - *t * 4e-4;
    }
    denhgk_(dl, p, &dguess, t, &fcts_1.dpdd);
    ideal_(t);
    thmhgk_(dl, t);
/* ** save liquid properties */
    dimhgk_(&c__1, itripl, t, p, dl, epseqn);
    gl = fcts_1.gd;
    dguess = *dv;
    if (*dv <= 0.) {
	dguess = *p / rtcurr_1.rt;
    }
    denhgk_(dv, p, &dguess, t, &fcts_1.dpdd);
    if (*dv < 5e-7) {
	*dv = 5e-7;
    }
    ideal_(t);
    thmhgk_(dv, t);
/* ** vapor properties will be available */
/* ** in COMMON /fcts/ (dimensionless) after */
/* ** pcorr's final call of corr (delg < 10d-4) */
    gv = fcts_1.gd;
    *delg = gl - gv;
    return 0;
} /* corr_ */

/* ********************************************************************** */
/* ** pcorr - Computes Psaturation(T) (p) and liquid and vapor */
/*           densities (dl & dv) from refinement of an initial */
/*           approximation (PsHGK(t)) in accord with  Gl = Gv. */
/* Subroutine */ int pcorr_(integer *itripl, doublereal *t, doublereal *p, 
	doublereal *dl, doublereal *dv, integer *epseqn)
{
    static doublereal dp, delg;
    extern /* Subroutine */ int corr_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *);
    extern doublereal pshgk_(doublereal *);

    *p = pshgk_(t);
    *dl = 0.;
    *dv = 0.;
L2:
    corr_(itripl, t, p, dl, dv, &delg, epseqn);
    dp = delg * aconst_1.gascon * *t / (1. / *dv - 1. / *dl);
    *p += dp;
    if (abs(delg) > 1e-4) {
	goto L2;
    }
    return 0;
} /* pcorr_ */

/* *********************************************************** */
/* ** tcorr - Computes Tsaturation(P) (t) and liquid and vapor */
/*           densities (dl & dv) from refinement of an initial */
/*           approximation (TsHGK(p)) in accord with  Gl = Gv. */
/* Subroutine */ int tcorr_(integer *itripl, doublereal *t, doublereal *p, 
	doublereal *dl, doublereal *dv, integer *epseqn)
{
    static doublereal dp, delg;
    extern /* Subroutine */ int corr_(integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, integer *);
    extern doublereal tshgk_(doublereal *), tdpsdt_(doublereal *);

    *t = tshgk_(p);
    if (*t == 0.) {
	return 0;
    }
    *dl = 0.;
    *dv = 0.;
L1:
    rtcurr_1.rt = *t * aconst_1.gascon;
    corr_(itripl, t, p, dl, dv, &delg, epseqn);
    dp = delg * aconst_1.gascon * *t / (1. / *dv - 1. / *dl);
    *t *= 1. - dp / tdpsdt_(t);
    if (abs(delg) > 1e-4) {
	goto L1;
    }
    return 0;
} /* tcorr_ */

/* ************************************************************** */
/* ** LVSeqn - Computes thermodynamic and transport properties of */
/*            critical region H2O (369.85-419.85 degC, */
/*            0.20-0.42 gm/cm3) from the fundamental equation given */
/*            by Levelt Sengers, et al (1983): J.Phys.Chem.Ref.Data, */
/*            V.12, No.1, pp.1-28. */
/* Subroutine */ int lvseqn_(integer *isat, integer *iopt, integer *itripl, 
	doublereal *t, doublereal *p, doublereal *dens, integer *epseqn)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    static doublereal dl, dv, cdens;
    static logical cpoint;
    extern /* Subroutine */ int denlvs_(integer *, doublereal *, doublereal *)
	    , dimlvs_(integer *, integer *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *)
	    , cpswap_(void), lvssat_(integer *, integer *, doublereal *, 
	    doublereal *, doublereal *), thmlvs_(integer *, doublereal *, 
	    doublereal *, doublereal *);
    static integer ioptsv;

    /* Parameter adjustments */
    --dens;

    /* Function Body */
    cpoint = FALSE_;
    satur_1.dh2o = dens[1];
L10:
    lvssat_(iopt, isat, t, p, &satur_1.dh2o);
    if (*isat != 0 || *iopt != 1) {
	denlvs_(isat, t, p);
    }
    if (*isat == 0) {
	dens[1] = satur_1.dh2o;
    } else {
	dens[1] = satur_1.dliq;
	dens[2] = satur_1.dvap;
    }
    if (*isat == 0) {
	thmlvs_(isat, t, &param_1.r1, &param_1.th1);
	d__1 = *p * 10.;
	dimlvs_(isat, itripl, &param_1.th1, t, &d__1, &dl, &dv, 
		wpvals_1.wprops, epseqn);
	if (cpoint) {
	    cpswap_();
	    dens[1] = cdens;
	    dens[2] = cdens;
	    *isat = 1;
	    *iopt = ioptsv;
	}
    } else {
	param_1.th1 = -1.;
	thmlvs_(isat, t, &param_1.r1, &param_1.th1);
	d__1 = *p * 10.;
	dimlvs_(isat, itripl, &param_1.th1, t, &d__1, &dl, &dv, 
		wpvals_1.wprops, epseqn);
	param_1.th1 = 1.;
	thmlvs_(isat, t, &param_1.r1, &param_1.th1);
	d__1 = *p * 10.;
	dimlvs_(isat, itripl, &param_1.th1, t, &d__1, &dl, &dv, 
		wpvals_1.wpliq, epseqn);
	if (dl == dv) {
	    cpoint = TRUE_;
	    cdens = dl;
	    *t = 647.0670000003;
	    *p = 22.0460000008;
	    ioptsv = *iopt;
	    *iopt = 2;
	    *isat = 0;
	    goto L10;
	}
    }
    return 0;
} /* lvseqn_ */

/* ******************************************************************** */
/* ** cpswap - Load critical point A, G, U, H, S, Vs, Di, ZB, */
/*            albe values from wpliq into wprops and */
/*            approximations to critical Cv, Cp, alpha, beta, */
/*            visc, tcond, Prndtl, tdiff, visck, YB, QB, XB, */
/*            daldT, st values from wprops into wpliq. */
/* Subroutine */ int cpswap_(void)
{
    /* Initialized data */

    static integer aw = 1;
    static integer gw = 2;
    static integer sw = 3;
    static integer uw = 4;
    static integer hw = 5;
    static integer cvw = 6;
    static integer cpw = 7;
    static integer vsw = 8;
    static integer alw = 9;
    static integer bew = 10;
    static integer diw = 11;
    static integer viw = 12;
    static integer tcw = 13;
    static integer stw = 14;
    static integer tdw = 15;
    static integer prw = 16;
    static integer vikw = 17;
    static integer albew = 18;
    static integer zbw = 19;
    static integer ybw = 20;
    static integer qbw = 21;
    static integer dalwdt = 22;
    static integer xbw = 23;

    wpvals_1.wprops[(0 + (0 + (aw - 1 << 3))) / 8] = wpvals_1.wpliq[(184 + (0 
	    + (aw - 1 << 3)) - 184) / 8];
    wpvals_1.wprops[gw - 1] = wpvals_1.wpliq[gw - 1];
    wpvals_1.wprops[sw - 1] = wpvals_1.wpliq[sw - 1];
    wpvals_1.wprops[uw - 1] = wpvals_1.wpliq[uw - 1];
    wpvals_1.wprops[hw - 1] = wpvals_1.wpliq[hw - 1];
    wpvals_1.wprops[diw - 1] = wpvals_1.wpliq[diw - 1];
    wpvals_1.wprops[zbw - 1] = wpvals_1.wpliq[zbw - 1];
    wpvals_1.wprops[stw - 1] = wpvals_1.wpliq[stw - 1];
    wpvals_1.wpliq[cvw - 1] = wpvals_1.wprops[cvw - 1];
    wpvals_1.wpliq[cpw - 1] = wpvals_1.wprops[cpw - 1];
    wpvals_1.wpliq[alw - 1] = wpvals_1.wprops[alw - 1];
    wpvals_1.wpliq[bew - 1] = wpvals_1.wprops[bew - 1];
    wpvals_1.wpliq[ybw - 1] = wpvals_1.wprops[ybw - 1];
    wpvals_1.wpliq[qbw - 1] = wpvals_1.wprops[qbw - 1];
    wpvals_1.wpliq[xbw - 1] = wpvals_1.wprops[xbw - 1];
    wpvals_1.wpliq[tcw - 1] = wpvals_1.wprops[tcw - 1];
    wpvals_1.wpliq[tdw - 1] = wpvals_1.wprops[tdw - 1];
    wpvals_1.wpliq[prw - 1] = wpvals_1.wprops[prw - 1];
    wpvals_1.wpliq[dalwdt - 1] = wpvals_1.wprops[dalwdt - 1];
    wpvals_1.wpliq[albew - 1] = wpvals_1.wprops[albew - 1];
    wpvals_1.wprops[vsw - 1] = units_1.fs * 42.9352766443498;
    wpvals_1.wprops[viw - 1] = 1e6;
    wpvals_1.wprops[vikw - 1] = 1e6;
    wpvals_1.wpliq[vsw - 1] = wpvals_1.wprops[vsw - 1];
    wpvals_1.wpliq[viw - 1] = wpvals_1.wprops[viw - 1];
    wpvals_1.wpliq[vikw - 1] = wpvals_1.wprops[vikw - 1];
    return 0;
} /* cpswap_ */

/* ******************************************************************** */
/* ** LVSsat - If  isat=1,  computes  Psat(T) or Tsat(P) (iopt=1,2). */
/*            If  isat=0,  checks whether  T-D or T-P (iopt=1,2) */
/*            falls on or within  TOL  of the liq-vap surface; if so, */
/*            isat <- 1  and  T <- Tsat. */
/* Subroutine */ int lvssat_(integer *iopt, integer *isat, doublereal *t, 
	doublereal *p, doublereal *d__)
{
    /* Initialized data */

    static doublereal errtol = 1e-12;
    static doublereal tctol = .01;

    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    static doublereal tsat;
    extern doublereal pfind_(integer *, doublereal *, doublereal *), tslvs_(
	    integer *, doublereal *);
    extern /* Subroutine */ int backup_(void), restor_(void);

    if (*isat == 1) {
	if (*iopt == 1) {
	    *p = pfind_(isat, t, d__);
	}
	*t = tslvs_(isat, p);
    } else {
	if (*iopt == 1) {
	    *p = pfind_(isat, t, d__);
	}
	if (*p - errtol > crits_1.pc) {
	    return 0;
	} else {
	    backup_();
	    tsat = tslvs_(isat, p);
	    if ((d__1 = tsat - *t, abs(d__1)) < tctol) {
		*t = tsat;
		*isat = 1;
	    } else {
		restor_();
	    }
	}
    }
    return 0;
} /* lvssat_ */

/* ******************************************************************** */
/* ** denLVS - Calculates  DH2O(T,P)  or  Dvap,Dliq(T,P) from the */
/*            Levelt Sengers, et al (1983) critical region */
/*            equation of state. */
/* Subroutine */ int denlvs_(integer *isat, doublereal *t, doublereal *p)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    static integer i__;
    static doublereal s[2], sd[2];
    extern /* Subroutine */ int ss_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal tw;
#define xk0 ((doublereal *)&coefs_1 + 6)
#define xk1 ((doublereal *)&coefs_1 + 11)
#define pw11 ((doublereal *)&coefs_1 + 28)
    static doublereal dtw, rho1, rho2, deld, pdif;
#define dmin__ ((doublereal *)&coefs_1 + 43)
#define dmax__ ((doublereal *)&coefs_1 + 44)
    extern doublereal pfind_(integer *, doublereal *, doublereal *), power_(
	    doublereal *, doublereal *);
    static doublereal pnext;

    if (*isat == 0) {
	satur_1.dh2o = crits_1.rhoc;
	for (i__ = 1; i__ <= 20; ++i__) {
	    pnext = pfind_(isat, t, &satur_1.dh2o);
	    pdif = pnext - *p;
	    if (satur_1.iphase == 2) {
		if (abs(pdif) <= 0.) {
		    return 0;
		} else {
		}
		if (pdif < 0.) {
		    satur_1.dh2o = *dmax__;
		} else {
		    satur_1.dh2o = *dmin__;
		}
	    } else {
		deld = -pdif / deri2_1.dpdd;
		satur_1.dh2o += deld;
		if (satur_1.dh2o < *dmin__) {
		    satur_1.dh2o = *dmin__;
		}
		if (satur_1.dh2o > *dmax__) {
		    satur_1.dh2o = *dmax__;
		}
		if ((d__1 = deld / satur_1.dh2o, abs(d__1)) < 1e-6) {
		    return 0;
		}
	    }
/* L10: */
	}
    } else {
	tw = -crits_1.tc / *t;
	dtw = tw + 1.;
	ss_(&param_1.r1, &param_1.th1, s, sd);
	rho1 = *pw11 * dtw + 1. + coefs_1.a[0] * (s[0] + s[1]);
	rho2 = *xk0 * power_(&param_1.r1, &coefs_1.a[5]) + *xk1 * power_(&
		param_1.r1, &coefs_1.q[15]);
	satur_1.dvap = crits_1.rhoc * (rho1 - rho2);
	satur_1.dliq = crits_1.rhoc * (rho1 + rho2);
	return 0;
    }
    return 0;
} /* denlvs_ */

#undef dmax__
#undef dmin__
#undef pw11
#undef xk1
#undef xk0


/* ******************************************************************** */
/* ** TsLVS - Returns saturation T(P) */
doublereal tslvs_(integer *isat, doublereal *p)
{
    /* System generated locals */
    doublereal ret_val, d__1;

    /* Local variables */
    static doublereal d__;
    static integer i__;
    static doublereal dt;
    extern doublereal pfind_(integer *, doublereal *, doublereal *);
    static doublereal pnext, tslvs2;

    tslvs2 = crits_1.tc - 1.;
    d__ = crits_1.rhoc;
    for (i__ = 1; i__ <= 20; ++i__) {
	pnext = pfind_(isat, &tslvs2, &d__);
	dt = (pnext - *p) / deri2_1.dpdt;
	tslvs2 -= dt;
	if (tslvs2 > crits_1.tc) {
	    tslvs2 = crits_1.tc;
	} else {
	    if ((d__1 = dt / tslvs2, abs(d__1)) < 1e-8) {
		goto L20;
	    } else {
	    }
	}
/* L10: */
    }
L20:
    ret_val = tslvs2;
    return ret_val;
} /* tslvs_ */

/* ******************************************************************** */
/* ** Pfind - Returns P(T,D).  Computes (dP/dD)T when invoked by SUB */
/*           Dfind (isat=0) and (dP/dT)D when invoked by SUB TsLVS */
/*           (isat=1).  Also computes 1st & 2nd partial derivatives */
/*           the singular part of the potential (Delta P tilde) that */
/*           are used in SUB thmLVS. */
doublereal pfind_(integer *isat, doublereal *t, doublereal *d__)
{
    /* System generated locals */
    doublereal ret_val, d__1;

    /* Local variables */
#define aa ((doublereal *)&coefs_1 + 9)
#define p00 ((doublereal *)&coefs_1 + 30)
#define p01 ((doublereal *)&coefs_1 + 37)
#define p20 ((doublereal *)&coefs_1 + 31)
#define p40 ((doublereal *)&coefs_1 + 32)
#define p21 ((doublereal *)&coefs_1 + 38)
    static doublereal sd[2];
#define p41 ((doublereal *)&coefs_1 + 39)
    extern /* Subroutine */ int ss_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal uw;
#define am1 ((doublereal *)&coefs_1 + 13)
#define am2 ((doublereal *)&coefs_1 + 14)
#define am3 ((doublereal *)&coefs_1 + 15)
#define xk0 ((doublereal *)&coefs_1 + 6)
#define xk1 ((doublereal *)&coefs_1 + 11)
    static doublereal pw0;
#define pw1 ((doublereal *)&coefs_1 + 4)
#define pw2 ((doublereal *)&coefs_1 + 3)
#define pw3 ((doublereal *)&coefs_1 + 1)
    static doublereal tt1, tt2;
#define amc ((doublereal *)&coefs_1 + 12)
    static doublereal tee;
#define pw11 ((doublereal *)&coefs_1 + 28)
    static doublereal rho, err, dpw;
    extern /* Subroutine */ int aux_(doublereal *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal rho1, dpw0, dpw1;
#define alhi ((doublereal *)&coefs_1 + 34)
#define besq ((doublereal *)&coefs_1 + 8)
    static doublereal pwmu;
#define alpha ((doublereal *)&coefs_1 + 29)
    static doublereal rhodi;
    extern doublereal power_(doublereal *, doublereal *);
    static doublereal dpdtcd, cvcoex;
    extern /* Subroutine */ int conver_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);
    static doublereal dpwdtw;

/* ************************************** */
/* ************************************** */
    deriv_1.xk[0] = *xk0;
    deriv_1.xk[1] = *xk1;
    if ((d__1 = *t - crits_1.tc, abs(d__1)) < tolers_1.fptol) {
	*t = crits_1.tc;
    }
    tee = (*t - crits_1.tc) / crits_1.tc;
    deriv_1.tw = -crits_1.tc / *t;
    deriv_1.dtw = deriv_1.tw + 1.;
    if (*isat == 0) {
	rho = *d__ / crits_1.rhoc;
	conver_(&rho, &tee, &deriv_1.amu, &param_1.th1, &param_1.r1, &rho1, 
		deriv_1.s, &rhodi, &err);
    } else {
	param_1.th1 = -1.;
	abc2_1.th = param_1.th1;
	param_1.r1 = deriv_1.dtw / (1. - *besq);
	abc2_1.r__ = param_1.r1;
	ss_(&param_1.r1, &param_1.th1, deriv_1.s, sd);
	rho = param_1.th1 * (*xk0 * power_(&param_1.r1, &coefs_1.a[5]) + *xk1 
		* power_(&param_1.r1, &coefs_1.q[15])) + coefs_1.a[0] * (
		deriv_1.s[0] + deriv_1.s[1]);
	rho = *pw11 * deriv_1.dtw + 1. + rho;
	deriv_1.amu = 0.;
	*d__ = rho * crits_1.rhoc;
    }
    tt1 = param_1.th1 * param_1.th1;
    tt2 = tt1 * tt1;
    pw0 = deriv_1.dtw * (*pw1 + deriv_1.dtw * (*pw2 + deriv_1.dtw * *pw3)) + 
	    1.;
    if (*isat == 0) {
	pwmu = deriv_1.amu * rhodi;
    } else {
	pwmu = 0.;
    }
    deriv_1.p0th = *p00 + *p20 * tt1 + *p40 * tt2;
    deriv_1.p1th = *p01 + *p21 * tt1 + *p41 * tt2;
    d__1 = 2. - *alpha;
    dpw0 = *xk0 * deriv_1.p0th * power_(&param_1.r1, &d__1);
    d__1 = 2. - *alhi;
    dpw1 = *xk1 * deriv_1.p1th * power_(&param_1.r1, &d__1);
    dpw = *aa * (dpw0 + dpw1);
    deriv_1.pw = pw0 + pwmu + dpw;
    ret_val = deriv_1.pw * crits_1.pcon * *t;
    if (abs(param_1.th1) < 1.) {
	satur_1.iphase = 1;
    } else {
	satur_1.iphase = 2;
	deriv_1.dp0dt = *pw1 + deriv_1.dtw * (*pw2 * 2. + *pw3 * 3. * 
		deriv_1.dtw);
	deriv_1.dm0dt = *am1 + deriv_1.dtw * (*am2 * 2. + *am3 * 3. * 
		deriv_1.dtw);
	uw = deriv_1.dp0dt - rho * deriv_1.dm0dt + *pw11 * deriv_1.amu + 
		deriv_1.s[0] + deriv_1.s[1];
	dpdtcd = uw + rho * deriv_1.dm0dt;
	dpwdtw = deriv_1.pw - deriv_1.tw * dpdtcd;
	deri2_1.dpdt = crits_1.pcon * dpwdtw;
    }
    aux_(&param_1.r1, &param_1.th1, &deriv_1.d2pdt2, &deriv_1.d2pdmt, &
	    deriv_1.d2pdm2, aa, deriv_1.xk, sd, &cvcoex);
    if (satur_1.iphase == 1) {
	deri2_1.dpdd = crits_1.dpcon * *d__ * *t / deriv_1.d2pdm2;
    }
    return ret_val;
} /* pfind_ */

#undef alpha
#undef besq
#undef alhi
#undef pw11
#undef amc
#undef pw3
#undef pw2
#undef pw1
#undef xk1
#undef xk0
#undef am3
#undef am2
#undef am1
#undef p41
#undef p21
#undef p40
#undef p20
#undef p01
#undef p00
#undef aa


/* ************************************************************** */
/* ** aux - Calculates some second derivatives of the */
/*         anomalous part of the equation of state. */
/* Subroutine */ int aux_(doublereal *r1, doublereal *th1, doublereal *d2pdt2,
	 doublereal *d2pdmt, doublereal *d2pdm2, doublereal *aa, doublereal *
	xk, doublereal *sd, doublereal *cvcoex)
{
    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    static doublereal g;
    static integer i__;
    static doublereal s[2], w[2], y[2], z__[2], a1, a2, a4, f1;
#define cc ((doublereal *)&coefs_1)
#define s00 ((doublereal *)&coefs_1 + 16)
#define s01 ((doublereal *)&coefs_1 + 18)
#define s20 ((doublereal *)&coefs_1 + 17)
#define s21 ((doublereal *)&coefs_1 + 19)
    static doublereal ww, yy, zz, tt1, ter;
#define beta ((doublereal *)&coefs_1 + 5)
    static doublereal deli, alhi, gami, beti;
#define besq ((doublereal *)&coefs_1 + 8)
    static doublereal coex[2], gamma;
#define alpha ((doublereal *)&coefs_1 + 29)
#define delta ((doublereal *)&coefs_1 + 10)
    extern doublereal power_(doublereal *, doublereal *);

    /* Parameter adjustments */
    --sd;
    --xk;

    /* Function Body */
    deli = 0.;
    s[0] = *s00 + *s20 * *th1 * *th1;
    s[1] = *s01 + *s21 * *th1 * *th1;
    sd[1] = *th1 * 2.f * *s20;
    sd[2] = *th1 * 2.f * *s21;
    ww = 0.;
    yy = 0.;
    zz = 0.;
    gamma = *beta * (*delta - 1.);
    tt1 = *th1 * *th1;
    ter = *beta * 2. * *delta - 1.;
    g = (*besq * ter - 3.f) * tt1 + 1.f - *besq * (ter - 2.f) * tt1 * tt1;
    *cvcoex = 0.;
    for (i__ = 1; i__ <= 2; ++i__) {
	alhi = *alpha - deli;
	beti = *beta + deli;
	gami = gamma - deli;
	if (*r1 != 0.) {
	    w[i__ - 1] = (1.f - alhi) * (1.f - tt1 * 3.f) * s[i__ - 1] - *
		    beta * *delta * (1.f - tt1) * *th1 * sd[i__];
	    d__1 = -alhi;
	    w[i__ - 1] = w[i__ - 1] * power_(r1, &d__1) / g;
	    w[i__ - 1] *= xk[i__];
	    ww += w[i__ - 1];
	    y[i__ - 1] = beti * (1. - tt1 * 3.) * *th1 - *beta * *delta * (1. 
		    - tt1) * *th1;
	    d__1 = beti - 1.;
	    y[i__ - 1] = y[i__ - 1] * power_(r1, &d__1) * xk[i__] / g;
	    yy += y[i__ - 1];
	    z__[i__ - 1] = 1. - *besq * (1. - beti * 2.) * tt1;
	    d__1 = -gami;
	    z__[i__ - 1] = z__[i__ - 1] * power_(r1, &d__1) * xk[i__] / g;
	    zz += z__[i__ - 1];
	    a1 = (*beta * (*delta - 3.) - deli * 3. - *besq * alhi * gami) / (
		    *besq * 2. * *besq * (2. - alhi) * (1. - alhi) * alhi);
	    a2 = (*beta * (*delta - 3.) - deli * 3. - *besq * alhi * ter) / (*
		    besq * 2. * (1. - alhi) * alhi) + 1;
	    a2 = -a2;
	    a4 = (ter - 2.) / (alhi * 2.) + 1.;
	    f1 = a1 + a2 + a4;
	    d__1 = -alhi;
	    coex[i__ - 1] = (2. - alhi) * (1. - alhi) * power_(r1, &d__1) * 
		    f1 * xk[i__];
	    *cvcoex += coex[i__ - 1];
	}
	deli = .5;
/* L30: */
    }
    *d2pdt2 = *aa * ww;
    *d2pdmt = yy + *aa * *cc * ww;
    *d2pdm2 = zz / *aa + *cc * 2. * yy + *cc * *cc * *aa * ww;
    return 0;
} /* aux_ */

#undef delta
#undef alpha
#undef besq
#undef beta
#undef s21
#undef s20
#undef s01
#undef s00
#undef cc


/* ************************************************************** */
/* ** conver - Transforms  T,D  to  parametric variables  r,theta */
/*            according to the revised and scaled equations. */
/* Subroutine */ int conver_(doublereal *rho, doublereal *tee, doublereal *
	amu, doublereal *th1, doublereal *r1, doublereal *rho1s, doublereal *
	s1, doublereal *rhodi, doublereal *error1)
{
    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double d_sign(doublereal *, doublereal *);

    /* Local variables */
    static doublereal y1;
#define aa ((doublereal *)&coefs_1 + 9)
#define cc ((doublereal *)&coefs_1)
#define p11 ((doublereal *)&coefs_1 + 28)
#define s00 ((doublereal *)&coefs_1 + 16)
#define s20 ((doublereal *)&coefs_1 + 17)
    static doublereal sd[2];
    extern /* Subroutine */ int ss_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal tt;
#define xk0 ((doublereal *)&coefs_1 + 6)
#define xk1 ((doublereal *)&coefs_1 + 11)
#define p1w ((doublereal *)&coefs_1 + 37)
#define p2w ((doublereal *)&coefs_1 + 38)
#define p4w ((doublereal *)&coefs_1 + 39)
    static doublereal den1, den2, den12;
#define beta ((doublereal *)&coefs_1 + 5)
#define alhi ((doublereal *)&coefs_1 + 34)
#define deli ((doublereal *)&coefs_1 + 33)
    static doublereal hold;
#define besq ((doublereal *)&coefs_1 + 8)
    static doublereal drho;
    static integer isig;
#define betai ((doublereal *)&coefs_1 + 35)
#define alpha ((doublereal *)&coefs_1 + 29)
#define delta ((doublereal *)&coefs_1 + 10)
    static doublereal rho1s2, slope;
    extern doublereal power_(doublereal *, doublereal *);
    static doublereal tstar, rho1co, error2;
    extern /* Subroutine */ int rtheta_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal rhodit, rhoweg, dtstin, twofaz;

/* ************************************************************* */
/* ************************************************************* */
    /* Parameter adjustments */
    --s1;

    /* Function Body */
    tstar = *tee + 1.;
    dtstin = 1. - 1. / tstar;
    *r1 = dtstin;
    if (dtstin <= 0.) {
	*r1 = dtstin / (1. - *besq);
	*th1 = 1.;
    } else {
	*th1 = 0.;
    }
    ss_(r1, th1, &s1[1], sd);
    *rhodi = *p11 * dtstin + 1.;
    rhodit = *rhodi + *cc * s1[1] + *cc * s1[2];
    drho = *rho - rhodit;
    *amu = 0.;
    if (dtstin <= 0.) {
	rho1co = *xk0 * power_(r1, beta) + *xk1 * power_(r1, betai);
	twofaz = rho1co;
	if (abs(drho) <= twofaz) {
	    *rho1s = d_sign(&rho1co, &drho) + *cc * s1[1];
	    *th1 = d_sign(&c_b968, &drho);
	    *error1 = 1.;
	    abc2_1.r__ = *r1;
	    abc2_1.th = *th1;
	    return 0;
	}
    }
    if (drho == 0.) {
	*th1 = 0.;
	*r1 = dtstin;
	*rho1s = *cc * s1[1];
    }
/* ** rule for first pass *** */
    y1 = dtstin;
    den1 = *rho - rhodit;
    rtheta_(r1, th1, &den1, &y1);
    tt = *th1 * *th1;
    d__1 = *beta * *delta;
    *amu = *aa * power_(r1, &d__1) * *th1 * (1. - tt);
    y1 = dtstin + *cc * *amu;
    ss_(r1, th1, &s1[1], sd);
    rhoweg = *xk1 * power_(r1, betai) * *th1 + *cc * s1[2];
    *rho1s = den1 + *cc * s1[1] + rhoweg;
    *error1 = *rho - *rhodi - *rho1s;
    abc2_1.r__ = *r1;
    abc2_1.th = *th1;
    if (abs(*error1) < 1e-5) {
	return 0;
    }
/* ** rule for second pass *** */
    den12 = *rho - *rhodi - *cc * s1[1] + rhoweg;
    if (den12 == den1) {
	den12 = den1 - 1e-6;
    }
    rtheta_(r1, th1, &den12, &y1);
    tt = *th1 * *th1;
    d__1 = *beta * *delta;
    *amu = *aa * power_(r1, &d__1) * *th1 * (1. - tt);
    y1 = dtstin + *cc * *amu;
    ss_(r1, th1, &s1[1], sd);
    rhoweg = *xk1 * power_(r1, betai) * *th1 + *cc * s1[2];
    rho1s2 = den12 + *cc * s1[1] + rhoweg;
    error2 = *rho - *rhodi - rho1s2;
    if (abs(error2) <= 1e-5) {
	abc2_1.r__ = *r1;
	abc2_1.th = *th1;
	*error1 = error2;
	*rho1s = rho1s2;
	return 0;
    }
/* ** rule for nth pass *** */
    den2 = den12;
    for (isig = 1; isig <= 10; ++isig) {
	slope = (error2 - *error1) / (den2 - den1);
	hold = den2;
	den2 = den1 - *error1 / slope;
	rtheta_(r1, th1, &den2, &y1);
	tt = *th1 * *th1;
	d__1 = *beta * *delta;
	*amu = *aa * power_(r1, &d__1) * *th1 * (1. - tt);
	y1 = dtstin + *cc * *amu;
	ss_(r1, th1, &s1[1], sd);
	rhoweg = *xk1 * power_(r1, betai) * *th1 + *cc * s1[2];
	*rho1s = den2 + *cc * s1[1] + rhoweg;
	*error1 = error2;
	error2 = *rho - *rhodi - *rho1s;
	abc2_1.r__ = *r1;
	abc2_1.th = *th1;
	if (abs(error2) < 1e-6) {
	    return 0;
	}
	den1 = hold;
/* L44: */
    }
    return 0;
} /* conver_ */

#undef delta
#undef alpha
#undef betai
#undef besq
#undef deli
#undef alhi
#undef beta
#undef p4w
#undef p2w
#undef p1w
#undef xk1
#undef xk0
#undef s20
#undef s00
#undef p11
#undef cc
#undef aa


/* ******************************************************************** */
/* ** rtheta - Fits data for  1.0 < theta < 1.000001. */
/*            Solves: */
/*                     rho = em*theta*(r**beta) */
/*                     Tee = r*(1.0d0-besq*theta*theta) */

/*   Routine given by Moldover (1978): Jour. Res. NBS, v. 84, n. 4, */
/*   p. 329 - 334. */
/* Subroutine */ int rtheta_(doublereal *r__, doublereal *theta, doublereal *
	rho, doublereal *tee)
{
    /* System generated locals */
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    double sqrt(doublereal), d_sign(doublereal *, doublereal *);

    /* Local variables */
    static doublereal c__;
    static integer n;
    static doublereal z__, z2, z3;
#define em ((doublereal *)&coefs_1 + 6)
    static doublereal dz, bee;
#define beta ((doublereal *)&coefs_1 + 5)
#define besq ((doublereal *)&coefs_1 + 8)
    extern doublereal power_(doublereal *, doublereal *);
    static doublereal absrho;

    if (*em <= 0. || *besq <= 1.) {
	goto L600;
    }
    absrho = abs(*rho);
    if (absrho < 1e-12) {
	goto L600;
    }
    bee = sqrt(*besq);
    if (abs(*tee) < 1e-12) {
	goto L495;
    }
    if (*tee < 0.) {
	d__1 = *em / absrho;
	d__2 = 1. / *beta;
	z__ = 1. - (1. - bee) * *tee / (1. - *besq) * power_(&d__1, &d__2);
    } else {
	d__2 = *em / bee / absrho;
	d__3 = 1. / *beta;
	d__1 = *tee * power_(&d__2, &d__3) + 1.;
	d__4 = -(*beta);
	z__ = power_(&d__1, &d__4);
    }
    if (z__ > bee * 1.00234) {
	goto L496;
    }
    d__1 = abs(*tee);
    c__ = -(*rho) * bee / *em / power_(&d__1, beta);
    z__ = d_sign(&z__, rho);
    for (n = 1; n <= 16; ++n) {
	z2 = z__ * z__;
	z3 = 1. - z2;
	d__1 = abs(z3);
	dz = z3 * (z__ + c__ * power_(&d__1, beta)) / (z3 + *beta * 2. * z2);
	z__ -= dz;
	if ((d__1 = dz / z__, abs(d__1)) < 1e-12) {
	    goto L498;
	}
/* L500: */
    }
L601:
    if (abs(*theta) > 1.0001) {
	*theta /= abs(*theta);
    }
    return 0;
L498:
    *theta = z__ / bee;
    *r__ = *tee / (1. - z__ * z__);
    *r__ = abs(*r__);
    return 0;
L495:
    *theta = d_sign(&c_b968, rho) / bee;
    d__1 = *rho / (*em * *theta);
    d__2 = 1. / *beta;
    *r__ = power_(&d__1, &d__2);
    return 0;
L496:
    *theta = d_sign(&c_b968, rho);
    *r__ = *tee / (1. - *besq);
    *r__ = abs(*r__);
    return 0;
L600:
    if (abs(*tee) < 1e-12) {
	goto L601;
    }
    if (*tee < 0.) {
	goto L496;
    }
    *theta = 1e-12;
    *r__ = *tee;
    return 0;
} /* rtheta_ */

#undef besq
#undef beta
#undef em


/* ******************************************************************** */
/* ** ss - Computes terms of the summation that defines  dPotl/dT */
/*        and the 1st derivative of the theta (s) square polynomial. */
/* Subroutine */ int ss_(doublereal *r__, doublereal *th, doublereal *s, 
	doublereal *sd)
{
    /* System generated locals */
    doublereal d__1, d__2;

    /* Local variables */
#define p00 ((doublereal *)&coefs_1 + 30)
#define p01 ((doublereal *)&coefs_1 + 37)
#define s00 ((doublereal *)&coefs_1 + 16)
#define s01 ((doublereal *)&coefs_1 + 18)
#define s20 ((doublereal *)&coefs_1 + 17)
#define s21 ((doublereal *)&coefs_1 + 19)
    static doublereal tt, sx[2];
#define beta ((doublereal *)&coefs_1 + 5)
#define deli ((doublereal *)&coefs_1 + 33)
#define alhi ((doublereal *)&coefs_1 + 34)
#define gami ((doublereal *)&coefs_1 + 36)
#define beti ((doublereal *)&coefs_1 + 35)
#define besq ((doublereal *)&coefs_1 + 8)
#define alpha ((doublereal *)&coefs_1 + 29)
#define delta ((doublereal *)&coefs_1 + 10)
    extern doublereal power_(doublereal *, doublereal *);

/* ************************************************************** */
/* ************************************************************** */
    /* Parameter adjustments */
    --sd;
    --s;

    /* Function Body */
    tt = *th * *th;
    sx[0] = *s00 + *s20 * tt;
    sd[1] = *s20 * 2. * *th;
    sx[1] = *s01 + *s21 * tt;
    sd[2] = *s21 * 2. * *th;
    d__1 = 1. - *alpha;
    s[1] = sx[0] * coefs_1.a[9] * coefs_1.a[6] * power_(r__, &d__1);
    d__1 = 1. - *alhi;
    s[2] = sx[1] * coefs_1.a[9] * coefs_1.a[11] * power_(r__, &d__1);
    d__1 = 1. - *alpha;
    d__2 = 1. - *alhi;
    abc1_1.dpdm = power_(r__, beta) * coefs_1.a[6] * *th + coefs_1.a[0] * 
	    power_(r__, &d__1) * coefs_1.a[9] * coefs_1.a[6] * sx[0] + power_(
	    r__, beti) * coefs_1.a[11] * *th + coefs_1.a[0] * power_(r__, &
	    d__2) * coefs_1.a[9] * coefs_1.a[11] * sx[1];
    return 0;
} /* ss_ */

#undef delta
#undef alpha
#undef besq
#undef beti
#undef gami
#undef alhi
#undef deli
#undef beta
#undef s21
#undef s20
#undef s01
#undef s00
#undef p01
#undef p00


/* **************************************************************** */
/* ** thmLVS - Calculates thermodynamic and transport properties */
/*            of critical region H2O using the Levelt Sengers, et al */
/*            (1983) equation of state. */
/* Subroutine */ int thmlvs_(integer *isat, doublereal *t, doublereal *r1, 
	doublereal *th1)
{
    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
#define aa ((doublereal *)&coefs_1 + 9)
    static doublereal sd[2], hw;
    extern /* Subroutine */ int ss_(doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal sw, uw;
#define am1 ((doublereal *)&coefs_1 + 13)
#define am2 ((doublereal *)&coefs_1 + 14)
#define am3 ((doublereal *)&coefs_1 + 15)
#define xk0 ((doublereal *)&coefs_1 + 6)
#define xk1 ((doublereal *)&coefs_1 + 11)
#define pw1 ((doublereal *)&coefs_1 + 4)
#define pw2 ((doublereal *)&coefs_1 + 3)
#define pw3 ((doublereal *)&coefs_1 + 1)
#define amc ((doublereal *)&coefs_1 + 12)
#define pw11 ((doublereal *)&coefs_1 + 28)
    static doublereal rho, cpw;
    extern /* Subroutine */ int aux_(doublereal *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal cvw;
#define alhi ((doublereal *)&coefs_1 + 34)
#define besq ((doublereal *)&coefs_1 + 8)
    static doublereal dpdt2;
#define alpha ((doublereal *)&coefs_1 + 29)
    static doublereal d2m0dt, d2p0dt, scond;
    extern doublereal power_(doublereal *, doublereal *);
    static doublereal cvitw2, dpdtal, cvcoex, dpwdtw;

/* ************************************************************ */
/* ************************************************************ */
    d2p0dt = *pw2 * 2. + *pw3 * 6. * deriv_1.dtw;
    d2m0dt = *am2 * 2. + *am3 * 6. * deriv_1.dtw;
    deriv_1.dp0dt = *pw1 + deriv_1.dtw * (*pw2 * 2. + *pw3 * 3. * deriv_1.dtw)
	    ;
    deriv_1.dm0dt = *am1 + deriv_1.dtw * (*am2 * 2. + *am3 * 3. * deriv_1.dtw)
	    ;
    if (*isat == 0) {
	rho = satur_1.dh2o / crits_1.rhoc;
	uw = deriv_1.dp0dt - rho * deriv_1.dm0dt + *pw11 * deriv_1.amu + 
		deriv_1.s[0] + deriv_1.s[1];
    } else {
	rho = *th1 * (*xk0 * power_(r1, &coefs_1.a[5]) + *xk1 * power_(r1, &
		coefs_1.q[15])) + coefs_1.a[0] * (deriv_1.s[0] + deriv_1.s[1])
		;
	rho = *pw11 * deriv_1.dtw + 1. + rho;
	uw = deriv_1.dp0dt - rho * deriv_1.dm0dt + *pw11 * deriv_1.amu + 
		deriv_1.s[0] + deriv_1.s[1];
	satur_1.dh2o = rho * crits_1.rhoc;
	dpdt2 = deriv_1.pw - deriv_1.tw * (uw + rho * deriv_1.dm0dt);
	therm_1.heat = *t * 1e3 * (crits_1.pcon * dpdt2) * (1. / satur_1.dvap 
		- 1. / satur_1.dliq);
	ss_(r1, th1, deriv_1.s, sd);
	aux_(r1, th1, &deriv_1.d2pdt2, &deriv_1.d2pdmt, &deriv_1.d2pdm2, aa, 
		deriv_1.xk, sd, &cvcoex);
	if (*r1 != 0.) {
	    deri2_1.dpdd = crits_1.dpcon * satur_1.dh2o * *t / deriv_1.d2pdm2;
	}
    }
    if (*r1 != 0.) {
	abc3_1.dpdtcd = deriv_1.dp0dt + *pw11 * (deriv_1.amu - rho / 
		deriv_1.d2pdm2) + deriv_1.s[0] + deriv_1.s[1] - 
		deriv_1.d2pdmt * rho / deriv_1.d2pdm2;
	dpwdtw = deriv_1.pw - deriv_1.tw * abc3_1.dpdtcd;
	dpdtal = crits_1.pcon * dpwdtw;
	cvitw2 = d2p0dt - rho * d2m0dt + deriv_1.d2pdt2 - (*pw11 + 
		deriv_1.d2pdmt) * (*pw11 + deriv_1.d2pdmt) / deriv_1.d2pdm2;
	cvw = cvitw2 * deriv_1.tw * deriv_1.tw;
	cpw = cvw + deriv_1.d2pdm2 * dpwdtw * dpwdtw / (rho * rho);
	therm_1.betaw = 1. / (satur_1.dh2o * deri2_1.dpdd);
	therm_1.alphw = therm_1.betaw * dpdtal;
	therm_1.speed = sqrt(cpw / cvw * deri2_1.dpdd) * 1e3;
    } else {
	cvw = 1.;
	cpw = 1.;
	therm_1.betaw = 1.;
	therm_1.alphw = 1.;
	therm_1.speed = 0.;
    }
    hw = deriv_1.pw - deriv_1.tw * uw;
    sw = hw - rho * (deriv_1.amu + *amc + deriv_1.dtw * (*am1 + deriv_1.dtw * 
	    (*am2 + deriv_1.dtw * *am3)));
    scond = crits_1.scon / satur_1.dh2o;
    therm_1.u = uw * crits_1.ucon / satur_1.dh2o;
    therm_1.h__ = hw * scond * *t;
    therm_1.entrop = sw * scond;
    therm_1.ae = therm_1.u - *t * therm_1.entrop;
    therm_1.ge = therm_1.h__ - *t * therm_1.entrop;
    therm_1.cv = cvw * scond;
    therm_1.cp = cpw * scond;
    return 0;
} /* thmlvs_ */

#undef alpha
#undef besq
#undef alhi
#undef pw11
#undef amc
#undef pw3
#undef pw2
#undef pw1
#undef xk1
#undef xk0
#undef am3
#undef am2
#undef am1
#undef aa


/* ******************************************************* */
/* ** dalLVS - Computes/returns (d(alpha)/dt)p(D,T,alpha) */
/*            for the Levelt Sengers et al. (1983) */
/*            equation of state.  Note that D (kg/m**3), */
/*            T (degK), P (MPa), alpha (degK**-1). */
doublereal dallvs_(doublereal *d__, doublereal *t, doublereal *p, doublereal *
	alpha)
{
    /* System generated locals */
    doublereal ret_val, d__1, d__2, d__3, d__4, d__5, d__6, d__7;

    /* Local variables */
#define a ((doublereal *)&coefs_2 + 9)
#define c__ ((doublereal *)&coefs_2)
    static integer i__;
    static doublereal k[2], q, s[2], u[2], v[2], w[2], b1, b2;
#define p1 ((doublereal *)&coefs_2 + 4)
#define p2 ((doublereal *)&coefs_2 + 3)
#define p3 ((doublereal *)&coefs_2 + 1)
    static doublereal a01, a02;
#define p11 ((doublereal *)&coefs_2 + 28)
#define s00 ((doublereal *)&coefs_2 + 16)
#define s01 ((doublereal *)&coefs_2 + 18)
#define s20 ((doublereal *)&coefs_2 + 17)
#define s21 ((doublereal *)&coefs_2 + 19)
    static doublereal sp[2], ar1, ar2;
#define bsq ((doublereal *)&coefs_2 + 8)
    static doublereal d0dt, delt, dqdt, drdt, dsdt[2], dudt[2], dvdt[2], dwdt[
	    2], cbeta[2];
#define delta ((doublereal *)&coefs_2 + 10)
    static doublereal dspdt[2], dpdtt, amult, pterm, dpptt;
    extern doublereal power_(doublereal *, doublereal *);
#define delta1 ((doublereal *)&coefs_2 + 33)
    static doublereal dp0dtt, cgamma[2], calpha[2], ddelmt, dpdmmt, dpdmtt;

/* ************************************************************ */
/* ************************************************************ */
    if (abc2_1.r__ == 0.) {
	ret_val = 1e6;
	return ret_val;
    }
    k[0] = coefs_2.aa[6];
    k[1] = coefs_2.aa[11];
    calpha[0] = coefs_2.qq[9];
    calpha[1] = coefs_2.qq[14];
    cbeta[0] = coefs_2.aa[5];
    cbeta[1] = coefs_2.qq[15];
    cgamma[0] = cbeta[0] * (*delta - 1.);
    cgamma[1] = cgamma[0] - *delta1;
    delt = (*t - crits_3.tc) / *t;
/* Computing 2nd power */
    d__1 = abc2_1.th;
    s[0] = *s00 + *s20 * (d__1 * d__1);
/* Computing 2nd power */
    d__1 = abc2_1.th;
    s[1] = *s01 + *s21 * (d__1 * d__1);
    sp[0] = *s20 * 2. * abc2_1.th;
    sp[1] = *s21 * 2. * abc2_1.th;
/* ******************************************************************** */
/* ** */
/* ** Compute drdT and d0dT from solution of the linear system */
/* ** */
/* **                      ax = b */
/* ** */
/* ** d(dPdM)/dT = -D/Dc*alpha - P11*Tc/T**2 = ar1*drdT + a01*d0dT = b1 */
/* ** d(delT)/dT =           Tc/T**2         = ar2*drdT + a02*d0dT = b2 */
/* ** */
    b1 = -(*d__) / crits_3.dc * *alpha - *p11 * crits_3.tc / *t / *t;
/* Computing 2nd power */
    d__1 = *t;
    b2 = crits_3.tc / (d__1 * d__1);
    ar1 = 0.;
    a01 = 0.;
    for (i__ = 1; i__ <= 2; ++i__) {
	d__1 = cbeta[i__ - 1] - 1.;
	d__2 = -calpha[i__ - 1];
	ar1 += k[i__ - 1] * (cbeta[i__ - 1] * abc2_1.th * power_(&abc2_1.r__, 
		&d__1) + *a * *c__ * (1. - calpha[i__ - 1]) * power_(&
		abc2_1.r__, &d__2) * s[i__ - 1]);
	d__1 = 1. - calpha[i__ - 1];
	a01 += k[i__ - 1] * (power_(&abc2_1.r__, &cbeta[i__ - 1]) + *a * *c__ 
		* sp[i__ - 1] * power_(&abc2_1.r__, &d__1));
/* L10: */
    }
/* Computing 2nd power */
    d__1 = abc2_1.th;
/* Computing 2nd power */
    d__2 = abc2_1.th;
    d__3 = cbeta[0] * *delta - 1.;
    ar2 = 1. - *bsq * (d__1 * d__1) - *a * *c__ * cbeta[0] * *delta * (1. - 
	    d__2 * d__2) * abc2_1.th * power_(&abc2_1.r__, &d__3);
/* Computing 2nd power */
    d__1 = abc2_1.th;
    d__2 = cbeta[0] * *delta;
    d__3 = cbeta[0] * *delta;
    a02 = *a * 3. * *c__ * (d__1 * d__1) * power_(&abc2_1.r__, &d__2) - *bsq *
	     2. * abc2_1.r__ * abc2_1.th - *a * *c__ * power_(&abc2_1.r__, &
	    d__3);
/* ******************************************************************** */
/* ** solve the linear system with simplistic GE w/ partial pivoting */
/* ******************************************************************** */
    if (abs(ar1) > abs(ar2)) {
	amult = -ar2 / ar1;
	d0dt = (b2 + amult * b1) / (a02 + amult * a01);
	drdt = (b1 - a01 * d0dt) / ar1;
    } else {
	amult = -ar1 / ar2;
	d0dt = (b1 + amult * b2) / (a01 + amult * a02);
	drdt = (b2 - a02 * d0dt) / ar2;
    }
/* ******************************************************************** */
/* ** */
/* ** Compute theta polynomials and their tempertaure derivatives */
/* ** */
    dsdt[0] = *s20 * 2. * abc2_1.th * d0dt;
    dsdt[1] = *s21 * 2. * abc2_1.th * d0dt;
    dspdt[0] = *s20 * 2. * d0dt;
    dspdt[1] = *s21 * 2. * d0dt;
/* Computing 2nd power */
    d__1 = abc2_1.th;
/* Computing 4th power */
    d__2 = abc2_1.th, d__2 *= d__2;
    q = (*bsq * (cbeta[0] * 2. * *delta - 1.) - 3.) * (d__1 * d__1) + 1. - *
	    bsq * (cbeta[0] * 2. * *delta - 3.) * (d__2 * d__2);
/* Computing 3rd power */
    d__1 = abc2_1.th;
    dqdt = (*bsq * (cbeta[0] * 2. * *delta - 1.) - 3.) * 2. * abc2_1.th * 
	    d0dt - *bsq * 4. * (cbeta[0] * 2. * *delta - 3.) * (d__1 * (d__1 *
	     d__1)) * d0dt;
    for (i__ = 1; i__ <= 2; ++i__) {
/* Computing 2nd power */
	d__1 = abc2_1.th;
	u[i__ - 1] = (1. - *bsq * (1. - cbeta[i__ - 1] * 2.) * (d__1 * d__1)) 
		/ q;
	dudt[i__ - 1] = (*bsq * -2. * (1. - cbeta[i__ - 1] * 2.) * abc2_1.th *
		 d0dt - u[i__ - 1] * dqdt) / q;
/* Computing 3rd power */
	d__1 = abc2_1.th;
	v[i__ - 1] = ((cbeta[i__ - 1] - cbeta[0] * *delta) * abc2_1.th + (
		cbeta[0] * *delta - cbeta[i__ - 1] * 3.) * (d__1 * (d__1 * 
		d__1))) / q;
/* Computing 2nd power */
	d__1 = abc2_1.th;
	dvdt[i__ - 1] = ((cbeta[i__ - 1] - cbeta[0] * *delta) * d0dt + (cbeta[
		0] * *delta - cbeta[i__ - 1] * 3.) * 3. * (d__1 * d__1) * 
		d0dt - v[i__ - 1] * dqdt) / q;
/* Computing 2nd power */
	d__1 = abc2_1.th;
/* Computing 3rd power */
	d__2 = abc2_1.th;
	w[i__ - 1] = ((1. - calpha[i__ - 1]) * (1. - d__1 * d__1 * 3.) * s[
		i__ - 1] - cbeta[0] * *delta * (abc2_1.th - d__2 * (d__2 * 
		d__2)) * sp[i__ - 1]) / q;
/* Computing 2nd power */
	d__1 = abc2_1.th;
/* Computing 3rd power */
	d__2 = abc2_1.th;
/* Computing 2nd power */
	d__3 = abc2_1.th;
	dwdt[i__ - 1] = ((1. - calpha[i__ - 1]) * ((1. - d__1 * d__1 * 3.) * 
		dsdt[i__ - 1] - abc2_1.th * 6. * s[i__ - 1] * d0dt) - cbeta[0]
		 * *delta * ((abc2_1.th - d__2 * (d__2 * d__2)) * dspdt[i__ - 
		1] + sp[i__ - 1] * (d0dt - d__3 * d__3 * 3. * d0dt)) - w[i__ 
		- 1] * dqdt) / q;
/* L20: */
    }
/* ******************************************************************** */
/* ** */
/* ** Compute dP0dTT, ddelMT, dPdTT, dPdMMT, dPdMTT, dPPTT */
/* ** */
/* Computing 2nd power */
    d__1 = *t;
    dp0dtt = crits_3.tc / (d__1 * d__1) * (*p2 * 2. + *p3 * 6. * delt);
    d__1 = cbeta[0] * *delta;
/* Computing 2nd power */
    d__2 = abc2_1.th;
/* Computing 2nd power */
    d__3 = abc2_1.th;
    ddelmt = *a * power_(&abc2_1.r__, &d__1) * (cbeta[0] * *delta * abc2_1.th 
	    / abc2_1.r__ * (1. - d__2 * d__2) * drdt + (1. - d__3 * d__3 * 3.)
	     * d0dt);
    dpdtt = 0.;
    dpdmmt = 0.;
    dpdmtt = 0.;
    for (i__ = 1; i__ <= 2; ++i__) {
	d__1 = 1. - calpha[i__ - 1];
	d__2 = -calpha[i__ - 1];
	dpdtt += *a * k[i__ - 1] * (power_(&abc2_1.r__, &d__1) * dsdt[i__ - 1]
		 + s[i__ - 1] * (1. - calpha[i__ - 1]) * power_(&abc2_1.r__, &
		d__2) * drdt);
	d__1 = -cgamma[i__ - 1];
	d__2 = -1. - cgamma[i__ - 1];
	d__3 = cbeta[i__ - 1] - 1.;
	d__4 = cbeta[i__ - 1] - 2.;
/* Computing 2nd power */
	d__5 = *c__;
	d__6 = -calpha[i__ - 1];
	d__7 = -1. - calpha[i__ - 1];
	dpdmmt += k[i__ - 1] * ((power_(&abc2_1.r__, &d__1) * dudt[i__ - 1] - 
		u[i__ - 1] * cgamma[i__ - 1] * power_(&abc2_1.r__, &d__2) * 
		drdt) / *a + *c__ * 2. * (power_(&abc2_1.r__, &d__3) * dvdt[
		i__ - 1] + v[i__ - 1] * (cbeta[i__ - 1] - 1.) * power_(&
		abc2_1.r__, &d__4) * drdt) + *a * (d__5 * d__5) * (power_(&
		abc2_1.r__, &d__6) * dwdt[i__ - 1] - calpha[i__ - 1] * w[i__ 
		- 1] * power_(&abc2_1.r__, &d__7) * drdt));
	d__1 = cbeta[i__ - 1] - 1.;
	d__2 = cbeta[i__ - 1] - 2.;
	d__3 = -calpha[i__ - 1];
	d__4 = -1. - calpha[i__ - 1];
	dpdmtt += k[i__ - 1] * (power_(&abc2_1.r__, &d__1) * dvdt[i__ - 1] + 
		v[i__ - 1] * (cbeta[i__ - 1] - 1.) * power_(&abc2_1.r__, &
		d__2) * drdt + *a * *c__ * (power_(&abc2_1.r__, &d__3) * dwdt[
		i__ - 1] - calpha[i__ - 1] * power_(&abc2_1.r__, &d__4) * 
		drdt * w[i__ - 1]));
/* L30: */
    }
/* Computing 2nd power */
    d__1 = deriv_2.d2pdm2;
    dpptt = dp0dtt + dpdtt + *p11 * ddelmt - *d__ / crits_3.dc * dpdmtt / 
	    deriv_2.d2pdm2 + (*p11 + deriv_2.d2pdmt) * (*d__ / crits_3.dc * *
	    alpha / deriv_2.d2pdm2 + *d__ / crits_3.dc * dpdmmt / (d__1 * 
	    d__1));
    pterm = *p / crits_3.pc + abc3_1.dpdtcd;
/* ** compute (d(alpha)/dT)P */
/* Computing 2nd power */
    d__1 = crits_3.dc;
/* Computing 2nd power */
    d__2 = *d__;
/* Computing 2nd power */
    d__3 = *t;
    ret_val = crits_3.tc * (d__1 * d__1) / (d__2 * d__2) / (d__3 * d__3) * (
	    -2. / *t * deriv_2.d2pdm2 * pterm + *alpha * 2. * deriv_2.d2pdm2 *
	     pterm + pterm * dpdmmt + deriv_2.d2pdm2 * dpptt);
    return ret_val;
} /* dallvs_ */

#undef delta1
#undef delta
#undef bsq
#undef s21
#undef s20
#undef s01
#undef s00
#undef p11
#undef p3
#undef p2
#undef p1
#undef c__
#undef a


/* ******************************************************************** */
/* ** backup - Save Pfind COMMON values during saturation check. */
/* Subroutine */ int backup_(void)
{
    store_1.isav1 = satur_1.iphase;
    store_1.sav2 = param_1.r1;
    store_1.sav3 = param_1.th1;
    store_1.sav4 = deriv_1.amu;
    store_1.sav5 = deriv_1.s[0];
    store_1.sav6 = deriv_1.s[1];
    store_1.sav7 = deriv_1.pw;
    store_1.sav8 = deriv_1.tw;
    store_1.sav9 = deriv_1.dtw;
    store_1.sav10 = deriv_1.dm0dt;
    store_1.sav11 = deriv_1.dp0dt;
    store_1.sav12 = deriv_1.d2pdm2;
    store_1.sav13 = deriv_1.d2pdmt;
    store_1.sav14 = deriv_1.d2pdt2;
    store_1.sav15 = deriv_1.p0th;
    store_1.sav16 = deriv_1.p1th;
    store_1.sav17 = deriv_1.xk[0];
    store_1.sav18 = deriv_1.xk[1];
    store_1.sav19 = deri2_1.dpdd;
    return 0;
} /* backup_ */

/* ******************************************************************** */
/* ** restor - Restore Pfind COMMON values after saturation check. */
/* Subroutine */ int restor_(void)
{
    satur_1.iphase = store_1.isav1;
    param_1.r1 = store_1.sav2;
    param_1.th1 = store_1.sav3;
    deriv_1.amu = store_1.sav4;
    deriv_1.s[0] = store_1.sav5;
    deriv_1.s[1] = store_1.sav6;
    deriv_1.pw = store_1.sav7;
    deriv_1.tw = store_1.sav8;
    deriv_1.dtw = store_1.sav9;
    deriv_1.dm0dt = store_1.sav10;
    deriv_1.dp0dt = store_1.sav11;
    deriv_1.d2pdm2 = store_1.sav12;
    deriv_1.d2pdmt = store_1.sav13;
    deriv_1.d2pdt2 = store_1.sav14;
    deriv_1.p0th = store_1.sav15;
    deriv_1.p1th = store_1.sav16;
    deriv_1.xk[0] = store_1.sav17;
    deriv_1.xk[1] = store_1.sav18;
    deri2_1.dpdd = store_1.sav19;
    return 0;
} /* restor_ */

/* ********************************************************************* */
/* ** load - Load thermodynamic and transport property values from */
/*          ptemp into props. */
/* Subroutine */ int load_(integer *phase, doublereal *ptemp, doublereal *
	props)
{
    /* Initialized data */

    static integer key[46]	/* was [23][2] */ = { 1,3,5,7,9,11,13,15,17,
	    19,21,23,25,27,29,31,33,35,37,39,41,43,45,2,4,6,8,10,12,14,16,18,
	    20,22,24,26,28,30,32,34,36,38,40,42,44,46 };

    static integer i__;

    /* Parameter adjustments */
    --props;
    --ptemp;

    /* Function Body */
    for (i__ = 1; i__ <= 23; ++i__) {
/* L10: */
	props[key[i__ + *phase * 23 - 24]] = ptemp[i__];
    }
    return 0;
} /* load_ */

/* ***************************************************************** */
/* ** tpset - Dimension triple point  U, S, H, A, G  values (in J/g from */
/*           Table 2, Helgeson & Kirkham, 1974a) into user-specified units. */
/* Subroutine */ int tpset_(void)
{
    /* Initialized data */

    static doublereal utr = -15766.;
    static doublereal str = 3.5144;
    static doublereal htr = -15971.;
    static doublereal atr = -12870.;
    static doublereal gtr = -13073.;

    tpoint_2.utripl = utr * units_1.fh;
    tpoint_2.stripl = str * units_1.fh;
    tpoint_2.htripl = htr * units_1.fh;
    tpoint_2.atripl = atr * units_1.fh;
    tpoint_2.gtripl = gtr * units_1.fh;
    return 0;
} /* tpset_ */

/* *************************************************************************** */
/* ** triple - Convert  U, S, H, A, G  values computed with reference to */
/*            zero triple point properties (Haar et al., 1984; */
/*            Levelt Sengers et al., 1983) into values referenced to */
/*            triple point properties given by  Helgeson and Kirkham, 1974a. */
/* Subroutine */ int triple_(doublereal *t, doublereal *wpzero)
{
    /* Initialized data */

    static integer a = 1;
    static integer g = 2;
    static integer s = 3;
    static integer u = 4;
    static integer h__ = 5;

    static doublereal ts;

    /* Parameter adjustments */
    --wpzero;

    /* Function Body */
    wpzero[(0 + (0 + (s << 3))) / 8] += tpoint_3.str;
    ts = *t * wpzero[s] - tpoint_3.ttripl * tpoint_3.str;
    wpzero[g] = wpzero[h__] - ts + tpoint_3.gtr;
    wpzero[a] = wpzero[u] - ts + tpoint_3.atr;
    wpzero[h__] += tpoint_3.htr;
    wpzero[u] += tpoint_3.utr;
    return 0;
} /* triple_ */

/* ******************************************************************** */
/* ** power - Returns  base**exp  utilizing the intrinsic FORTRAN */
/*           exponentiation function in such a manner so as to */
/*           insure computation of machine-independent values */
/*           for all defined exponentiations.  Attempted undefined */
/*           exponentiations produce an error message and cause */
/*           program termination. */
doublereal power_(doublereal *base, doublereal *exp__)
{
    /* Initialized data */

    static doublereal tol = 1e-7;

    /* Format strings */
    static char fmt_10[] = "(/,\002 neg base ** real exp is complex\002,/"
	    ",\002 base,exp: \002,2e20.13,/)";
    static char fmt_20[] = "(/,\002 zero base ** (exp <= 0) is undefined\002"
	    ",/,\002 base,exp: \002,2e20.13)";

    /* System generated locals */
    doublereal ret_val, d__1;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *);
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);
    /* Subroutine */ int s_stop(char *, ftnlen);
    double d_mod(doublereal *, doublereal *);

    /* Fortran I/O blocks */
    static cilist io___919 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___920 = { 0, 0, 0, fmt_20, 0 };


    if (*base > 0.) {
	ret_val = pow_dd(base, exp__);
    } else {
	if (abs(*base) > tol) {
	    if ((doublereal) ((integer) (*exp__)) != *exp__) {
		io___919.ciunit = io_2.wterm;
		s_wsfe(&io___919);
		do_fio(&c__1, (char *)&(*base), (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&(*exp__), (ftnlen)sizeof(doublereal));
		e_wsfe();
		s_stop("", (ftnlen)0);
	    } else {
		if (d_mod(exp__, &c_b997) == 0.) {
		    d__1 = -(*base);
		    ret_val = pow_dd(&d__1, exp__);
		} else {
		    d__1 = -(*base);
		    ret_val = -pow_dd(&d__1, exp__);
		}
	    }
	} else {
	    if (*exp__ > 0.) {
		ret_val = 0.;
	    } else {
		io___920.ciunit = io_2.wterm;
		s_wsfe(&io___920);
		do_fio(&c__1, (char *)&(*base), (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&(*exp__), (ftnlen)sizeof(doublereal));
		e_wsfe();
		s_stop("", (ftnlen)0);
	    }
	}
    }
    return ret_val;
} /* power_ */

/* ********************************************************************** */
/* ** TdegK - Returns input temperature  t  converted from */
/*           user-specified units to degK. */
doublereal tdegk_(integer *it, doublereal *t)
{
    /* System generated locals */
    doublereal ret_val;

    switch (*it) {
	case 1:  goto L1;
	case 2:  goto L2;
	case 3:  goto L3;
	case 4:  goto L4;
    }
L1:
    ret_val = *t;
    return ret_val;
L2:
    ret_val = *t + 273.15;
    return ret_val;
L3:
    ret_val = *t / 1.8;
    return ret_val;
L4:
    ret_val = (*t + 459.67) / 1.8;
    return ret_val;
} /* tdegk_ */

/* ********************************************************************** */
/* ** TdegUS - Returns input temperature  t  converted */
/*            from degK to user-specified units. */
doublereal tdegus_(integer *it, doublereal *t)
{
    /* System generated locals */
    doublereal ret_val;

    switch (*it) {
	case 1:  goto L1;
	case 2:  goto L2;
	case 3:  goto L3;
	case 4:  goto L4;
    }
L1:
    ret_val = *t;
    return ret_val;
L2:
    ret_val = *t - 273.15;
    return ret_val;
L3:
    ret_val = *t * 1.8;
    return ret_val;
L4:
    ret_val = *t * 1.8 - 459.67;
    return ret_val;
} /* tdegus_ */

/* ******************************************************************** */
/* ** dim[HGK,LVS] - Dimensioning routines for H2O88. */
/* ******************************************************************** */
/* ** dimHGK - Dimensions thermodynamic and transport property values */
/*            computed from the HGK equation of state per user-specified */
/*            choice of units. */
/* Subroutine */ int dimhgk_(integer *isat, integer *itripl, doublereal *t, 
	doublereal *p, doublereal *d__, integer *epseqn)
{
    /* Initialized data */

    static integer aw = 1;
    static integer gw = 2;
    static integer sw = 3;
    static integer uw = 4;
    static integer hw = 5;
    static integer cvw = 6;
    static integer cpw = 7;
    static integer vsw = 8;
    static integer alw = 9;
    static integer bew = 10;
    static integer diw = 11;
    static integer viw = 12;
    static integer tcw = 13;
    static integer stw = 14;
    static integer tdw = 15;
    static integer prw = 16;
    static integer vikw = 17;
    static integer albew = 18;
    static integer zbw = 19;
    static integer ybw = 20;
    static integer qbw = 21;
    static integer dalwdt = 22;
    static integer xbw = 23;

    /* System generated locals */
    doublereal d__1;

    /* Builtin functions */
    double sqrt(doublereal);

    /* Local variables */
    static doublereal dkgm3, betab, pbars;
    extern /* Subroutine */ int born92_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *);
    extern doublereal dalhgk_(doublereal *, doublereal *, doublereal *);
    static doublereal betapa, cpjkkg;
    extern doublereal thcond_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    extern /* Subroutine */ int triple_(doublereal *, doublereal *);
    extern doublereal viscos_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), surten_(doublereal *);

    wpvals_1.wprops[(0 + (0 + (aw - 1 << 3))) / 8] = fcts_1.ad * rtcurr_1.rt *
	     units_1.fh;
    wpvals_1.wprops[gw - 1] = fcts_1.gd * rtcurr_1.rt * units_1.fh;
    wpvals_1.wprops[sw - 1] = fcts_1.sd * aconst_2.gascon * units_1.fh * 
	    units_1.ft;
    wpvals_1.wprops[uw - 1] = fcts_1.ud * rtcurr_1.rt * units_1.fh;
    wpvals_1.wprops[hw - 1] = fcts_1.hd * rtcurr_1.rt * units_1.fh;
    wpvals_1.wprops[cvw - 1] = fcts_1.cvd * aconst_2.gascon * units_1.fh * 
	    units_1.ft;
    wpvals_1.wprops[cpw - 1] = fcts_1.cpd * aconst_2.gascon * units_1.fh * 
	    units_1.ft;
    wpvals_1.wprops[vsw - 1] = sqrt((d__1 = fcts_1.cpd * fcts_1.dpdd * 1e3 / 
	    fcts_1.cvd, abs(d__1))) * units_1.fs;
    wpvals_1.wprops[bew - 1] = 1. / (*d__ * fcts_1.dpdd * units_1.fp);
    wpvals_1.wprops[alw - 1] = *d__ * fcts_1.dvdt;
    wpvals_1.wprops[dalwdt - 1] = dalhgk_(d__, t, &wpvals_1.wprops[alw - 1]);
    pbars = *p * 10.;
    dkgm3 = *d__ * 1e3;
    betapa = wpvals_1.wprops[bew - 1] * units_1.fp / 1e6;
    betab = wpvals_1.wprops[bew - 1] * units_1.fp / 10.;
    cpjkkg = wpvals_1.wprops[cpw - 1] / units_1.fh / units_1.ft * 1e3;
    wpvals_1.wprops[viw - 1] = viscos_(t, &pbars, &dkgm3, &betapa) * 
	    units_1.fvd;
    wpvals_1.wprops[tcw - 1] = thcond_(t, &pbars, &dkgm3, &wpvals_1.wprops[
	    alw - 1], &betapa) * units_1.fc * units_1.ft;
    if (*isat == 0 || *isat == 2) {
	wpvals_1.wprops[stw - 1] = 0.;
    } else {
	wpvals_1.wprops[stw - 1] = surten_(t) * units_1.fst;
    }
    d__1 = dkgm3 / 1e3;
    born92_(t, &pbars, &d__1, &betab, &wpvals_1.wprops[alw - 1], &
	    wpvals_1.wprops[dalwdt - 1], &wpvals_1.wprops[diw - 1], &
	    wpvals_1.wprops[zbw - 1], &wpvals_1.wprops[qbw - 1], &
	    wpvals_1.wprops[ybw - 1], &wpvals_1.wprops[xbw - 1], epseqn);
    wpvals_1.wprops[tdw - 1] = wpvals_1.wprops[tcw - 1] / units_1.fc / 
	    units_1.ft / (dkgm3 * cpjkkg) * units_1.fvk;
    if (wpvals_1.wprops[tcw - 1] != 0.) {
	wpvals_1.wprops[prw - 1] = wpvals_1.wprops[viw - 1] / units_1.fvd * 
		cpjkkg / (wpvals_1.wprops[tcw - 1] / units_1.fc / units_1.ft);
    } else {
	wpvals_1.wprops[prw - 1] = 0.;
    }
    wpvals_1.wprops[vikw - 1] = wpvals_1.wprops[viw - 1] / units_1.fvd / 
	    dkgm3 * units_1.fvk;
    wpvals_1.wprops[albew - 1] = wpvals_1.wprops[alw - 1] / wpvals_1.wprops[
	    bew - 1];
    if (*itripl == 1) {
	triple_(t, wpvals_1.wprops);
    }
    return 0;
} /* dimhgk_ */

/* **************************************************************************** */
/* ** dimLVS - Dimension critical region properties per user-specs */
/*            and load into tprops. */
/* Subroutine */ int dimlvs_(integer *isat, integer *itripl, doublereal *
	theta, doublereal *t, doublereal *pbars, doublereal *dl, doublereal *
	dv, doublereal *tprops, integer *epseqn)
{
    /* Initialized data */

    static integer aw = 1;
    static integer gw = 2;
    static integer sw = 3;
    static integer uw = 4;
    static integer hw = 5;
    static integer cvw = 6;
    static integer cpw = 7;
    static integer vsw = 8;
    static integer alw = 9;
    static integer bew = 10;
    static integer diw = 11;
    static integer viw = 12;
    static integer tcw = 13;
    static integer stw = 14;
    static integer tdw = 15;
    static integer prw = 16;
    static integer vikw = 17;
    static integer albew = 18;
    static integer zbw = 19;
    static integer ybw = 20;
    static integer qbw = 21;
    static integer dalwdt = 22;
    static integer xbw = 23;

    /* System generated locals */
    doublereal d__1;

    /* Local variables */
    static doublereal dkgm3, betab;
    extern /* Subroutine */ int born92_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *);
    static doublereal betapa, cpjkkg;
    extern doublereal thcond_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), dallvs_(doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    extern /* Subroutine */ int triple_(doublereal *, doublereal *);
    extern doublereal viscos_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), surten_(doublereal *);

/* **************************************************************** */
/* **************************************************************** */
    /* Parameter adjustments */
    --tprops;

    /* Function Body */
    if (*isat == 1) {
	*dv = satur_1.dvap;
	*dl = satur_1.dliq;
    }
    tprops[aw] = therm_1.ae * units_1.fh;
    tprops[gw] = therm_1.ge * units_1.fh;
    tprops[sw] = therm_1.entrop * units_1.fh * units_1.ft;
    tprops[uw] = therm_1.u * units_1.fh;
    tprops[hw] = therm_1.h__ * units_1.fh;
    tprops[cvw] = therm_1.cv * units_1.fh * units_1.ft;
    tprops[cpw] = therm_1.cp * units_1.fh * units_1.ft;
    tprops[vsw] = therm_1.speed * units_1.fs;
    tprops[bew] = therm_1.betaw / units_1.fp;
    tprops[alw] = therm_1.alphw;
/* **************************************************************** */
    abc2_1.th = *theta;
    d__1 = *pbars / 10.;
    tprops[dalwdt] = dallvs_(&satur_1.dh2o, t, &d__1, &tprops[alw]);
/* **************************************************************** */
    cpjkkg = therm_1.cp * 1e3;
    betapa = therm_1.betaw / 1e6;
    betab = therm_1.betaw / 10.;
    if (abs(*theta) != 1.) {
	dkgm3 = satur_1.dh2o;
	tprops[stw] = 0.;
    } else {
	if (*theta < 0.) {
	    dkgm3 = satur_1.dvap;
	    tprops[stw] = 0.;
	} else {
	    dkgm3 = satur_1.dliq;
	    dkgm3 = satur_1.dliq;
	    tprops[stw] = surten_(t) * units_1.fst;
	}
    }
    d__1 = dkgm3 / 1e3;
    born92_(t, pbars, &d__1, &betab, &tprops[alw], &tprops[dalwdt], &tprops[
	    diw], &tprops[zbw], &tprops[qbw], &tprops[ybw], &tprops[xbw], 
	    epseqn);
    tprops[viw] = viscos_(t, pbars, &dkgm3, &betapa) * units_1.fvd;
    tprops[tcw] = thcond_(t, pbars, &dkgm3, &tprops[alw], &betapa) * 
	    units_1.fc * units_1.ft;
    tprops[tdw] = tprops[tcw] / units_1.fc / units_1.ft / (dkgm3 * cpjkkg) * 
	    units_1.fvk;
    tprops[prw] = tprops[viw] / units_1.fvd * cpjkkg / (tprops[tcw] / 
	    units_1.fc / units_1.ft);
    tprops[vikw] = tprops[viw] / units_1.fvd / dkgm3 * units_1.fvk;
    tprops[albew] = tprops[alw] / tprops[bew];
    if (*itripl == 1) {
	triple_(t, &tprops[1]);
    }
    return 0;
} /* dimlvs_ */

/* ********************************************************************* */
/* ** tran88 - Set of FORTRAN77 functions that compute transport */
/*            properties of fluid H2O.  Input state parameters */
/*            should be computed from the Haar et al. (1984) */
/*            and Levelt Sengers et al. (1983) equations of state in */
/*            order to facilitate comparision with published tabular */
/*            values referenced below for each function. */

/* ********************************************************************* */
/* **   programmer:  James W. Johnson */
/* **   abandoned:   20 January 1988 */
/* ********************************************************************* */
/* ** viscos - Returns dynamic viscosity of H2O in kg/m*s (= Pa*s) */
/*            if  Tk, Pbars  falls within the validity region (specified */
/*            by the initial IF statement) of the Watson et al. (1980) */
/*            equation; otherwise returns zero.  See equations 3.1-2 and */
/*            4.1-5 and Tables 1, 6, and 8 from Sengers and */
/*            Kamgar-Parsi (1984). */
doublereal viscos_(doublereal *tk, doublereal *pbars, doublereal *dkgm3, 
	doublereal *betapa)
{
    /* Initialized data */

    static doublereal a[4] = { .0181583,.0177624,.0105287,-.0036744 };
    static doublereal b[42]	/* was [6][7] */ = { .5132047,.3205656,0.,0.,
	    -.7782567,.1885447,.2151778,.7317883,1.241044,1.476783,0.,0.,
	    -.2818107,-1.070786,-1.263184,0.,0.,0.,.1778064,.460504,.2340379,
	    -.4924179,0.,0.,-.0417661,0.,0.,.1600435,0.,0.,0.,-.01578386,0.,
	    0.,0.,0.,0.,0.,0.,-.003629481,0.,0. };
    static doublereal tol = .01;

    /* System generated locals */
    doublereal ret_val, d__1, d__2;

    /* Builtin functions */
    double pow_di(doublereal *, integer *), sqrt(doublereal), exp(doublereal);

    /* Local variables */
    static doublereal d__;
    static integer i__, j;
    static doublereal t, u0, u1, u2, xt, sum, tdegc;
    extern doublereal power_(doublereal *, doublereal *);

    ret_val = 0.;
    tdegc = *tk - 273.15;
    if (*pbars > tol + 5e3 || *pbars > tol + 3500. && tdegc > tol + 150. || *
	    pbars > tol + 3e3 && tdegc > tol + 600. || tdegc > tol + 900.) {
	return ret_val;
    }
    t = *tk / 647.27;
    d__ = *dkgm3 / 317.763;
    sum = 0.;
    for (i__ = 0; i__ <= 3; ++i__) {
/* L10: */
	sum += a[i__] / pow_di(&t, &i__);
    }
    u0 = sqrt(t) * 1e-6 / sum;
    sum = 0.;
    for (i__ = 0; i__ <= 5; ++i__) {
	for (j = 0; j <= 6; ++j) {
/* L20: */
	    d__1 = 1. / t - 1;
	    d__2 = d__ - 1;
	    sum += b[i__ + 1 + (j + 1) * 6 - 7] * pow_di(&d__1, &i__) * 
		    pow_di(&d__2, &j);
	}
    }
    u1 = exp(d__ * sum);
    if (.997 <= t && t <= 1.0082 && .755 <= d__ && d__ <= 1.29) {
/* Computing 2nd power */
	d__1 = *dkgm3;
	xt = *betapa * 219.01824251112026 * (d__1 * d__1);
	if (xt < 22.) {
	    u2 = 1.;
	} else {
	    u2 = power_(&xt, &c_b1018) * .922f;
	}
    } else {
	u2 = 1.;
    }
    ret_val = u0 * u1 * u2;
    return ret_val;
} /* viscos_ */

/* **************************************************************** */
/* ** thcond - Returns thermal conductivity of H2O in J/m*deg*s (=W/m*deg) */
/*            if  Tk, Pbars  falls within the validity region (specified */
/*            by the initial IF statement) of the Sengers et al. (1984) */
/*            equation; returns zero otherwise.  See equations 3.2-14 */
/*            and tables 2-5 and I.5-6 from the above reference. */
doublereal thcond_(doublereal *tk, doublereal *pbars, doublereal *dkgm3, 
	doublereal *alph, doublereal *betapa)
{
    /* Initialized data */

    static doublereal al[4] = { 2.02223,14.11166,5.25597,-2.0187 };
    static doublereal au[4] = { .0181583,.0177624,.0105287,-.0036744 };
    static doublereal bl[30]	/* was [6][5] */ = { 1.3293046,-.40452437,
	    .2440949,.018660751,-.12961068,.044809953,1.7018363,-2.2156845,
	    1.6511057,-.76736002,.37283344,-.1120316,5.2246158,-10.124111,
	    4.9874687,-.27297694,-.43083393,.13333849,8.7127675,-9.5000611,
	    4.3786606,-.91783782,0.,0.,-1.8525999,.9340469,0.,0.,0.,0. };
    static doublereal bu[30]	/* was [5][6] */ = { .501938,.235622,-.274637,
	    .145831,-.0270448,.162888,.789393,-.743539,.263129,-.0253093,
	    -.130356,.673665,-.959456,.347247,-.0267758,.907919,1.207552,
	    -.687343,.213486,-.0822904,-.551119,.0670665,-.497089,.100754,
	    .0602253,.146543,-.084337,.195286,-.032932,-.0202595 };
    static doublereal tol = .01;

    /* System generated locals */
    doublereal ret_val, d__1, d__2, d__3, d__4;

    /* Builtin functions */
    double pow_di(doublereal *, integer *), sqrt(doublereal), exp(doublereal);

    /* Local variables */
    static doublereal d__;
    static integer i__, j;
    static doublereal t, l0, l1, l2, u0, u1, xt, sum, dpdt, tdegc;
    extern doublereal power_(doublereal *, doublereal *);

    ret_val = 0.;
    tdegc = *tk - 273.15;
    if (*pbars > tol + 4e3 || *pbars > tol + 2e3 && tdegc > tol + 125. || *
	    pbars > tol + 1500. && tdegc > tol + 400. || tdegc > tol + 800.) {
	return ret_val;
    }
    t = *tk / 647.27;
    d__ = *dkgm3 / 317.763;
    sum = 0.;
    for (i__ = 0; i__ <= 3; ++i__) {
/* L10: */
	sum += al[i__] / pow_di(&t, &i__);
    }
    l0 = sqrt(t) / sum;
    sum = 0.;
    for (i__ = 0; i__ <= 4; ++i__) {
	for (j = 0; j <= 5; ++j) {
/* L20: */
	    d__1 = 1. / t - 1;
	    d__2 = d__ - 1;
	    sum += bl[j + 1 + (i__ + 1) * 6 - 7] * pow_di(&d__1, &i__) * 
		    pow_di(&d__2, &j);
	}
    }
    l1 = exp(d__ * sum);
    sum = 0.;
    for (i__ = 0; i__ <= 3; ++i__) {
/* L40: */
	sum += au[i__] / pow_di(&t, &i__);
    }
    u0 = sqrt(t) * 1e-6 / sum;
    sum = 0.;
    for (i__ = 0; i__ <= 5; ++i__) {
	for (j = 0; j <= 4; ++j) {
/* L50: */
	    d__1 = 1. / t - 1;
	    d__2 = d__ - 1;
	    sum += bu[j + 1 + (i__ + 1) * 5 - 6] * pow_di(&d__1, &i__) * 
		    pow_di(&d__2, &j);
	}
    }
    u1 = exp(d__ * sum);
/* Computing 2nd power */
    d__1 = *dkgm3;
    xt = *betapa * 219.01824251112026 * (d__1 * d__1);
    dpdt = *alph * 2.9268369884693646e-5 / *betapa;
/* Computing 2nd power */
    d__1 = t / d__;
/* Computing 2nd power */
    d__2 = dpdt;
/* Computing 2nd power */
    d__3 = t - 1;
/* Computing 4th power */
    d__4 = d__ - 1, d__4 *= d__4;
    l2 = 3.7711e-8 / (u0 * u1) * (d__1 * d__1) * (d__2 * d__2) * power_(&xt, &
	    c_b1024) * sqrt(d__) * exp(d__3 * d__3 * -18.66 - d__4 * d__4);
    ret_val = l0 * l1 + l2;
    return ret_val;
} /* thcond_ */

/* ***************************************************************** */
/* ** surten - Returns the surface tension of vapor-saturated liquid */
/*            H2O in MPa*cm (converted from N/m) as computed from */
/*            the Vargaftik et al. (1983) equation.  See equations */
/*            10.1-2, Kestin et al. (1984); compare also equation */
/*            C.5 and table 11, Haar et al. (1984). */
doublereal surten_(doublereal *tsatur)
{
    /* System generated locals */
    doublereal ret_val;

    /* Local variables */
    extern doublereal power_(doublereal *, doublereal *);
    static doublereal tnorm;

    if (*tsatur < 273.16 || *tsatur > 647.067) {
	ret_val = 0.;
	return ret_val;
    }
    if (*tsatur >= 647.06699999989996) {
	tnorm = 0.;
    } else {
	tnorm = (.999686 - *tsatur / 647.27) / .999686;
    }
    ret_val = power_(&tnorm, &c_b1026) * .2358 * (tnorm * -.625 + 1.);
    return ret_val;
} /* surten_ */

/* ***************************************************************** */
/* ** Born92 - Computes the Z, Q, Y, and X Born functions at TK, Pbars. */
/* ** */
/* **        epseqn = 1 ...... use Helgeson-Kirkham (1974) equation */
/* **        epseqn = 2 ...... use Pitzer (1983) equation */
/* **        epseqn = 3 ...... use Uematsu-Franck (1980) equation */
/* **        epseqn = 4 ...... use Johnson-Norton (1991) equation */
/* **        epseqn = 5 ...... use Archer-Wang (1990) equation */
/* ** */
/* Subroutine */ int born92_(doublereal *tk, doublereal *pbars, doublereal *
	dgcm3, doublereal *betab, doublereal *alphak, doublereal *daldt, 
	doublereal *eps, doublereal *z__, doublereal *q, doublereal *y, 
	doublereal *x, integer *epseqn)
{
    extern /* Subroutine */ int jn91_(doublereal *, doublereal *, doublereal *
	    , doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);
    static doublereal dedp, dedt, d2edt2, tdegc;
    extern /* Subroutine */ int epsbrn_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *);

    *eps = 0.;
    *z__ = 0.;
    *y = 0.;
    *q = 0.;
    *x = 0.;
    tdegc = *tk - 273.15;
/* **   The following line can be commented out to facilitate probably */
/* **   unreliable, yet potentially useful, predictive calculations */
/* **   at state conditions beyond the validity limits of the aqueous */
/* **   species equation of state. */
    if (tdegc > 1000.001 || *pbars > 5000.0010000000002) {
	return 0;
    }
/*      IF (epseqn .EQ. 1) THEN */
/*           CALL HK74(TK,Dgcm3,betab,alphaK,daldT, */
/*     1               eps,dedP,dedT,d2edT2) */
/*           CALL epsBrn(eps,dedP,dedT,d2edT2,Z,Q,Y,X) */
/*           RETURN */
/*      END IF */
/*      IF (epseqn .EQ. 2) THEN */
/*           CALL Pitz83(TK,Dgcm3,betab,alphaK,daldT, */
/*     1                 eps,dedP,dedT,d2edT2) */
/*           CALL epsBrn(eps,dedP,dedT,d2edT2,Z,Q,Y,X) */
/*           RETURN */
/*      END IF */
/*      IF (epseqn .EQ. 3) THEN */
/*           CALL UF80(TK,Dgcm3,betab,alphaK,daldT, */
/*     1               eps,dedP,dedT,d2edT2) */
/*           CALL epsBrn(eps,dedP,dedT,d2edT2,Z,Q,Y,X) */
/*           RETURN */
/*      END IF */
    if (*epseqn == 4) {
	jn91_(tk, dgcm3, betab, alphak, daldt, eps, &dedp, &dedt, &d2edt2);
	epsbrn_(eps, &dedp, &dedt, &d2edt2, z__, q, y, x);
	return 0;
    }
/*      IF (epseqn .EQ. 5) THEN */
/*           Dkgm3 = Dgcm3 * 1.0d3 */
/*           PMPa  = Pbars / 1.0d1 */
/*           betam = betab * 1.0d1 */
/*           CALL AW90(TK,PMPa,Dkgm3,betam,alphaK,daldT, */
/*     1                 eps,dedP,dedT,d2edT2) */
/* ***        convert  dedP  FROM  MPa**-1  TO  bars**-1 */
/*           dedP = dedP / 1.0d1 */
/*           CALL epsBrn(eps,dedP,dedT,d2edT2,Z,Q,Y,X) */
/*           RETURN */
/*      END IF */
    return 0;
} /* born92_ */

/* ******************************************************************** */
/* ** JN91 - Compute (eps, dedP, dedT, d2edT2)(T,D) using equations */
/* **        given by Johnson and Norton (1991); fit parameters */
/* **        regressed from least squares fit to dielectric data */
/* **        consistent with the HK74 equation and low temperatures, */
/* **        and with the Pitz83 equation at high temperatures. */
/* ** */
/* **          Units: T ............... K */
/* **                 D ............... g/cm**3 */
/* **                 beta, dedP ...... bar**(-1) */
/* **                 alpha, dedT ..... K**(-1) */
/* **                 daldT, d2edT2 ... K**(-2) */
/* Subroutine */ int jn91_(doublereal *t, doublereal *d__, doublereal *beta, 
	doublereal *alpha, doublereal *daldt, doublereal *eps, doublereal *
	dedp, doublereal *dedt, doublereal *d2edt2)
{
    /* Initialized data */

    static doublereal tref = 298.15;
    static doublereal a[10] = { 14.70333593,212.8462733,-115.4445173,
	    19.55210915,-83.3034798,32.13240048,-6.694098645,-37.86202045,
	    68.87359646,-27.29401652 };

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2, d__3;

    /* Builtin functions */
    double pow_di(doublereal *, integer *);

    /* Local variables */
    static doublereal c__[5];
    static integer j, k;
    static doublereal tn, dcdt[5], dc2dtt[5];

    tn = *t / tref;
    c__[0] = 1.;
    dcdt[0] = 0.;
    dc2dtt[0] = 0.;
    c__[1] = a[0] / tn;
/* Computing 2nd power */
    d__1 = *t;
    dcdt[1] = -a[0] * tref / (d__1 * d__1);
/* Computing 3rd power */
    d__1 = *t;
    dc2dtt[1] = a[0] * 2. * tref / (d__1 * (d__1 * d__1));
    c__[2] = a[1] / tn + a[2] + a[3] * tn;
/* Computing 2nd power */
    d__1 = *t;
    dcdt[2] = -a[1] * tref / (d__1 * d__1) + a[3] / tref;
/* Computing 3rd power */
    d__1 = *t;
    dc2dtt[2] = a[1] * 2. * tref / (d__1 * (d__1 * d__1));
/* Computing 2nd power */
    d__1 = tn;
    c__[3] = a[4] / tn + a[5] * tn + a[6] * (d__1 * d__1);
/* Computing 2nd power */
    d__1 = *t;
/* Computing 2nd power */
    d__2 = tref;
    dcdt[3] = -a[4] * tref / (d__1 * d__1) + a[5] / tref + a[6] * 2. * *t / (
	    d__2 * d__2);
/* Computing 3rd power */
    d__1 = *t;
/* Computing 2nd power */
    d__2 = tref;
    dc2dtt[3] = a[4] * 2. * tref / (d__1 * (d__1 * d__1)) + a[6] * 2. / (d__2 
	    * d__2);
/* Computing 2nd power */
    d__1 = tn;
    c__[4] = a[7] / (d__1 * d__1) + a[8] / tn + a[9];
/* Computing 2nd power */
    d__1 = tref;
/* Computing 3rd power */
    d__2 = *t;
/* Computing 2nd power */
    d__3 = *t;
    dcdt[4] = a[7] * -2. * (d__1 * d__1) / (d__2 * (d__2 * d__2)) - a[8] * 
	    tref / (d__3 * d__3);
/* Computing 2nd power */
    d__1 = tref;
/* Computing 4th power */
    d__2 = *t, d__2 *= d__2;
/* Computing 3rd power */
    d__3 = *t;
    dc2dtt[4] = a[7] * 6. * (d__1 * d__1) / (d__2 * d__2) + a[8] * 2. * tref /
	     (d__3 * (d__3 * d__3));
    *eps = 0.;
    for (k = 1; k <= 5; ++k) {
/* L50: */
	i__1 = k - 1;
	*eps += c__[k - 1] * pow_di(d__, &i__1);
    }
    *dedp = 0.;
    for (j = 0; j <= 4; ++j) {
/* L100: */
	*dedp += j * c__[j] * pow_di(d__, &j);
    }
    *dedp = *beta * *dedp;
    *dedt = 0.;
    for (j = 0; j <= 4; ++j) {
/* L200: */
	*dedt += pow_di(d__, &j) * (dcdt[j] - j * *alpha * c__[j]);
    }
    *d2edt2 = 0.;
    for (j = 0; j <= 4; ++j) {
/* L300: */
	*d2edt2 += pow_di(d__, &j) * (dc2dtt[j] - j * (*alpha * dcdt[j] + c__[
		j] * *daldt) - j * *alpha * (dcdt[j] - j * *alpha * c__[j]));
    }
    return 0;
} /* jn91_ */

/* ************************************************************** */
/* ** epsBrn - Compute the Z, Q, Y, and X Born functions from their */
/* **          eps, dedP, dedT, and d2edT2 counterparts. */
/* Subroutine */ int epsbrn_(doublereal *eps, doublereal *dedp, doublereal *
	dedt, doublereal *d2edt2, doublereal *z__, doublereal *q, doublereal *
	y, doublereal *x)
{
    /* System generated locals */
    doublereal d__1, d__2;

    *z__ = -1. / *eps;
/* Computing 2nd power */
    d__1 = *eps;
    *q = 1. / (d__1 * d__1) * *dedp;
/* Computing 2nd power */
    d__1 = *eps;
    *y = 1. / (d__1 * d__1) * *dedt;
/* Computing 2nd power */
    d__1 = *eps;
/* Computing 2nd power */
    d__2 = *y;
    *x = 1. / (d__1 * d__1) * *d2edt2 - *eps * 2. * (d__2 * d__2);
    return 0;
} /* epsbrn_ */

/* ** reac92 - Calculates the standard molal Gibbs free energy, enthalpy, */
/* **          entropy, heat capacity, and volume of the i[th] reaction */
/* **          (specified in common blocks /icon/ and /reac/) among */
/* **          <= MAXMIN minerals, <= MAXAQS aqueous species, <= MAXGAS */
/* **          gases, and H2O using equations and data given by Helgeson */
/* **          et al. (1978), Tanger and Helgeson (1988), Shock and */
/* **          Helgeson (1988, 1990), Shock et al. (1989, 1991), Johnson */
/* **          and Norton (1991), Johnson et al. (1991), and Sverjensky */
/* **          et al. (1991). */
/* ** */
/* **          Computed reaction properties are stored in COMMON blocks */
/* **          /minsp/, /gassp/, /aqsp/, /solvn/, and /fmeq/. */
/* ** */
/* ****************************************************************** */
/* ** */
/* ** Author:     James W. Johnson */
/* **             Earth Sciences Department, L-219 */
/* **             Lawrence Livermore National Laboratory */
/* **             Livermore, CA 94550 */
/* **             johnson@s05.es.llnl.gov */
/* ** */
/* ** Abandoned:  8 November 1991 */
/* ** */
/* ****************************************************************** */
/* Subroutine */ int reac92_(integer *i__, doublereal *p, doublereal *tc, 
	doublereal *dw, doublereal *vw, doublereal *betaw, doublereal *alphaw,
	 doublereal *daldtw, doublereal *sw, doublereal *cpw, doublereal *hw, 
	doublereal *gw, doublereal *zw, doublereal *qw, doublereal *yw, 
	doublereal *xw, integer *geqn)
{
    static doublereal tk;
    extern /* Subroutine */ int gases_(integer *, doublereal *), aqsps_(
	    integer *, doublereal *, doublereal *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, integer *), reactn_(integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), solids_(integer *, doublereal *, 
	    doublereal *);

/* ****************************************************************** */
/* ** argument units:  (w suffixes denote H2O properties) */
/* ** */
/* **         P ............ bars */
/* **         TC ........... degC */
/* **         Dw ........... g/cm**3 */
/* **         Vw ........... cm**3/mol */
/* **         betaw, Qw .... bars**(-1) */
/* **         alphaw, Yw ... K**(-1) */
/* **         daldTw, Xw ... K**(-2) */
/* **         Sw, Cpw ...... cal/(mol*K) */
/* **         Hw, Gw ....... cal/mol */
/* **         Zw ........... dimensionless */
/* ******************************************** */
    tk = *tc + 273.15;
    solids_(&reac2_1.nm[*i__ - 1], p, &tk);
    gases_(&reac2_1.ng[*i__ - 1], &tk);
    aqsps_(&reac2_1.na[*i__ - 1], p, &tk, dw, betaw, alphaw, daldtw, zw, qw, 
	    yw, xw, geqn);
    reactn_(i__, &tk, vw, sw, cpw, hw, gw);
    return 0;
} /* reac92_ */

/* ******************************************************************* */
/* ** Solids - Computes the standard molal thermodynamic properties of */
/*            nmin minerals at P,T using equations given by */
/*            Helgeson et al. (1978). */
/* Subroutine */ int solids_(integer *nmin, doublereal *p, doublereal *t)
{
    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__;
    static doublereal vdp;
    static integer cpreg;
    static doublereal cprdt;
    extern integer getcpr_(integer *, doublereal *);
    static doublereal cprdlt;
    extern integer getphr_(integer *, doublereal *, doublereal *, doublereal *
	    );
    extern /* Subroutine */ int cptrms_(char *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen);
    static doublereal gpttrm, hpttrm;
    extern /* Subroutine */ int vterms_(integer *, doublereal *, doublereal *,
	     integer *, doublereal *, doublereal *, doublereal *), quartz_(
	    char *, doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, ftnlen), pttrms_(
	    integer *, integer *, doublereal *, doublereal *, doublereal *, 
	    doublereal *);
    static doublereal spttrm;

    i__1 = *nmin;
    for (i__ = 1; i__ <= i__1; ++i__) {
	minsp_1.phaser[i__ - 1] = getphr_(&i__, p, t, pttran_1.ttranp);
	cpreg = getcpr_(&i__, t);
	vterms_(&i__, p, t, &minsp_1.phaser[i__ - 1], &minsp_1.vmin[i__ - 1], 
		&vdp, pttran_1.ptrant);
	cptrms_("min", &i__, &cpreg, t, &minsp_1.cpmin[i__ - 1], &cprdt, &
		cprdlt, (ftnlen)3);
	pttrms_(&i__, &minsp_1.phaser[i__ - 1], t, &spttrm, &hpttrm, &gpttrm);
	minsp_1.smin[i__ - 1] = minref_1.sprtrm[i__ - 1] + cprdlt + spttrm;
	minsp_1.hmin[i__ - 1] = minref_1.hfmin[i__ - 1] + cprdt + vdp + 
		hpttrm;
	minsp_1.gmin[i__ - 1] = minref_1.gfmin[i__ - 1] - minref_1.sprtrm[i__ 
		- 1] * (*t - refval_1.tref) + cprdt - *t * cprdlt + vdp + 
		gpttrm;
	if (s_cmp(mnames_1.mname + (i__ - 1) * 20, "QUARTZ", (ftnlen)20, (
		ftnlen)6) == 0 || s_cmp(mnames_1.mname + (i__ - 1) * 20, 
		"COESITE", (ftnlen)20, (ftnlen)7) == 0) {
	    minsp_1.hmin[i__ - 1] -= vdp;
	    minsp_1.gmin[i__ - 1] -= vdp;
	    quartz_(mnames_1.mname + (i__ - 1) * 20, p, t, &minref_1.ttran[
		    i__ * 3 - 3], &minsp_1.vmin[i__ - 1], &minsp_1.smin[i__ - 
		    1], &minsp_1.hmin[i__ - 1], &minsp_1.gmin[i__ - 1], (
		    ftnlen)20);
	}
/* L10: */
    }
    return 0;
} /* solids_ */

/* *********************************************************************** */
/* ** getCpr - Returns the effective phase region for temperature */
/* **          integration of Cpr(T) for mineral imin (i.e., the */
/* **          phase region specified by T at 1 bar). */
integer getcpr_(integer *imin, doublereal *t)
{
    /* System generated locals */
    integer ret_val, i__1;

    /* Local variables */
    static integer i__;

    ret_val = 1;
    i__1 = minref_1.ntran[*imin - 1];
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (*t > minref_1.ttran[i__ + *imin * 3 - 4]) {
	    ++ret_val;
	}
/* L10: */
    }
    return ret_val;
} /* getcpr_ */

/* ********************************************************************** */
/* ** quartz - Revises the standard molal Gibbs free energy (G), enthalpy */
/* **          (H), entropy (S), and volume (V) of quartz or coesite to */
/* **          account for V(T) > 0 using equations (109) through (115), */
/* **          Helgeson et al. (1978). */
/* Subroutine */ int quartz_(char *mname, doublereal *p, doublereal *t, 
	doublereal *ttpr, doublereal *v, doublereal *s, doublereal *h__, 
	doublereal *g, ftnlen mname_len)
{
    /* Initialized data */

    static doublereal vprtra = 22.688;
    static doublereal vdiff = 2.047;
    static doublereal k = 38.5;

    /* System generated locals */
    doublereal d__1, d__2;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);
    double log(doublereal);

    /* Local variables */
    static doublereal pstar, sstar;
    static integer qphase;
    static doublereal gvterm, svterm;

/* ** VPrTra = VPrTr(a-quartz) */
/* ** Vdiff  = VPrTr(a-quartz) - VPrTr(coesite) */
/* ** k      = dPdTtr(a/b-quartz) */
/* **** set qphase = phase region of quartz */
    if (*t <= *ttpr || *p >= refval_3.pr + k * (*t - *ttpr)) {
	qphase = 1;
    } else {
	qphase = 2;
    }
/* **** set Pstar and Sstar ***** */
    if (*t <= *ttpr) {
	pstar = refval_3.pr;
	sstar = 0.;
    } else {
	if (qphase == 2) {
	    pstar = *p;
	    sstar = 0.;
	} else {
	    pstar = refval_3.pr + k * (*t - *ttpr);
	    sstar = qtzcon_1.stran;
	}
    }
    if (qphase == 2) {
/* **** set volume to beta-quartz ***** */
	*v = qtzcon_1.vprttb;
    } else {
/* **** calculate volume of alpha-quartz per eqn (109) ***** */
	*v = vprtra + qtzcon_1.ca * (*p - refval_3.pr) + (qtzcon_1.vpttta - 
		vprtra - qtzcon_1.ca * (*p - refval_3.pr)) * (*t - 
		refval_3.tr) / (*ttpr + (*p - refval_3.pr) / k - refval_3.tr);
    }
    if (s_cmp(mname, "COESITE", (ftnlen)20, (ftnlen)7) == 0) {
	*v -= vdiff;
    }
/* **** leading constant for [G,S]Vterm below */
/* **** is a coversion factor (cal/cm**3/bar) */
    if (s_cmp(mname, "QUARTZ", (ftnlen)20, (ftnlen)6) == 0) {
/* Computing 2nd power */
	d__1 = *p;
/* Computing 2nd power */
	d__2 = pstar;
	gvterm = (vprtra * (*p - pstar) + qtzcon_1.vprttb * (pstar - 
		refval_3.pr) - qtzcon_1.ca * .5 * (refval_3.pr * 2. * (*p - 
		pstar) - (d__1 * d__1 - d__2 * d__2)) - qtzcon_1.ca * k * (*t 
		- refval_3.tr) * (*p - pstar) + k * (qtzcon_1.ba + 
		qtzcon_1.aa * qtzcon_1.ca * k) * (*t - refval_3.tr) * log((
		qtzcon_1.aa + *p / k) / (qtzcon_1.aa + pstar / k))) * 
		.023901488;
    } else {
/* Computing 2nd power */
	d__1 = *p;
/* Computing 2nd power */
	d__2 = pstar;
	gvterm = ((vprtra - vdiff) * (*p - pstar) + (qtzcon_1.vprttb - vdiff) 
		* (pstar - refval_3.pr) - qtzcon_1.ca * .5 * (refval_3.pr * 
		2. * (*p - pstar) - (d__1 * d__1 - d__2 * d__2)) - 
		qtzcon_1.ca * k * (*t - refval_3.tr) * (*p - pstar) + k * (
		qtzcon_1.ba + qtzcon_1.aa * qtzcon_1.ca * k) * (*t - 
		refval_3.tr) * log((qtzcon_1.aa + *p / k) / (qtzcon_1.aa + 
		pstar / k))) * .023901488;
    }
    svterm = (-k * (qtzcon_1.ba + qtzcon_1.aa * qtzcon_1.ca * k) * log((
	    qtzcon_1.aa + *p / k) / (qtzcon_1.aa + pstar / k)) + qtzcon_1.ca *
	     k * (*p - pstar)) * .023901488 - sstar;
    *g += gvterm;
    *s += svterm;
    *h__ = *h__ + gvterm + *t * svterm;
    return 0;
} /* quartz_ */

/* *********************************************************************** */
/* ** getphr - Returns phase region for mineral imin at P, T; and, as a */
/* **          side effect, TtranP(1..MXTRAN,imin) as f(P). */
/* ** */
/* **          getphr = 1 ... TtranP(1,imin) > T  [or imin lacks transn] */
/* **          getphr = 2 ... TtranP(1,imin) < T  [< TtranP(2,imin)] */
/* **          getphr = 3 ... TtranP(2,imin) < T  [< TtranP(3,imin)] */
/* **          getphr = 4 ... TtranP(3,imin) < T */
integer getphr_(integer *imin, doublereal *p, doublereal *t, doublereal *
	ttranp)
{
    /* System generated locals */
    integer ret_val;

/* ***** phase region 1 ****** */
    /* Parameter adjustments */
    ttranp -= 4;

    /* Function Body */
    ret_val = 1;
    if (minref_1.ntran[*imin - 1] == 0) {
	return ret_val;
    }
    if (minref_1.dpdttr[*imin * 3 - 3] == 0.) {
	ttranp[*imin * 3 + 1] = minref_1.ttran[*imin * 3 - 3];
    } else {
	ttranp[*imin * 3 + 1] = minref_1.ttran[*imin * 3 - 3] + (*p - 
		refval_1.pref) / minref_1.dpdttr[*imin * 3 - 3];
    }
    if (*t <= ttranp[*imin * 3 + 1]) {
	return ret_val;
    }
/* ***** phase region 2 ****** */
    ret_val = 2;
    if (minref_1.ntran[*imin - 1] == 1) {
	return ret_val;
    }
    if (minref_1.dpdttr[*imin * 3 - 2] == 0.) {
	ttranp[*imin * 3 + 2] = minref_1.ttran[*imin * 3 - 2];
    } else {
	ttranp[*imin * 3 + 2] = minref_1.ttran[*imin * 3 - 2] + (*p - 
		refval_1.pref) / minref_1.dpdttr[*imin * 3 - 2];
    }
    if (*t <= ttranp[*imin * 3 + 2]) {
	return ret_val;
    }
/* ***** phase region 3 ****** */
    ret_val = 3;
    if (minref_1.ntran[*imin - 1] == 2) {
	return ret_val;
    }
    if (minref_1.dpdttr[*imin * 3 - 1] == 0.) {
	ttranp[*imin * 3 + 3] = minref_1.ttran[*imin * 3 - 1];
    } else {
	ttranp[*imin * 3 + 3] = minref_1.ttran[*imin * 3 - 1] + (*p - 
		refval_1.pref) / minref_1.dpdttr[*imin * 3 - 1];
    }
    if (*t <= ttranp[*imin * 3 + 3]) {
	return ret_val;
    }
/* ***** phase region 4 ****** */
    ret_val = 4;
    return ret_val;
} /* getphr_ */

/* *********************************************************************** */
/* ** Vterms - Computes Vmin(P,T), Vmin*dP, and (if necesary) PtranT. */
/* Subroutine */ int vterms_(integer *imin, doublereal *p, doublereal *t, 
	integer *phaser, doublereal *vmin, doublereal *vdp, doublereal *
	ptrant)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer i__;

    /* Parameter adjustments */
    ptrant -= 4;

    /* Function Body */
    *vmin = minref_1.vprtrm[*imin - 1];
    i__1 = *phaser - 1;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* L10: */
	*vmin += minref_1.vtran[i__ + *imin * 3 - 4];
    }
    *vdp = *vmin * (*p - refval_1.pref) * .023901488;
/* ***** return if Pressure integration does not cross */
/* ***** phase transition boundaries */
    if (minref_1.ntran[*imin - 1] == 0) {
	return 0;
    }
    if (minref_1.dpdttr[*imin * 3 - 3] == 0.) {
	return 0;
    }
    if (*t <= minref_1.ttran[*imin * 3 - 3]) {
	return 0;
    }
    if (minref_1.ntran[*imin - 1] == 1 && *phaser == 2) {
	return 0;
    }
    if (minref_1.ntran[*imin - 1] == 2 && *phaser == 3) {
	return 0;
    }
    if (minref_1.ntran[*imin - 1] == 2 && *phaser == 2 && *t < minref_1.ttran[
	    *imin * 3 - 2]) {
	return 0;
    }
/* ***** take account of cross-boundary pressure integration */
    if (minref_1.ntran[*imin - 1] == 1 || *phaser == 1 && *t < minref_1.ttran[
	    *imin * 3 - 2]) {
	ptrant[*imin * 3 + 1] = refval_1.pref + (*t - minref_1.ttran[*imin * 
		3 - 3]) * minref_1.dpdttr[*imin * 3 - 3];
	*vdp = (minref_1.vprtrm[*imin - 1] * (*p - refval_1.pref) + 
		minref_1.vtran[*imin * 3 - 3] * (ptrant[*imin * 3 + 1] - 
		refval_1.pref)) * .023901488;
	return 0;
    }
/* ***** ntran(imin) = 2 and T .GE. Ttran(2,imin) ****** */
    ptrant[*imin * 3 + 2] = refval_1.pref + (*t - minref_1.ttran[*imin * 3 - 
	    2]) * minref_1.dpdttr[*imin * 3 - 2];
    if (*phaser == 2) {
	*vdp = ((minref_1.vprtrm[*imin - 1] + minref_1.vtran[*imin * 3 - 3]) *
		 (*p - refval_1.pref) + minref_1.vtran[*imin * 3 - 2] * (
		ptrant[*imin * 3 + 2] - refval_1.pref)) * .023901488;
    } else {
	ptrant[*imin * 3 + 1] = refval_1.pref + (*t - minref_1.ttran[*imin * 
		3 - 3]) * minref_1.dpdttr[*imin * 3 - 3];
	*vdp = (minref_1.vprtrm[*imin - 1] * (*p - refval_1.pref) + 
		minref_1.vtran[*imin * 3 - 3] * (ptrant[*imin * 3 + 1] - 
		refval_1.pref) + minref_1.vtran[*imin * 3 - 2] * (ptrant[*
		imin * 3 + 2] - refval_1.pref)) * .023901488;
    }
    return 0;
} /* vterms_ */

/* *********************************************************************** */
/* ** Cptrms - Computes the standard molal heat capacity and heat capacity */
/* **          temperature integrals, evaluated from Tref to T at 1 bar. */
/* Subroutine */ int cptrms_(char *phase, integer *i__, integer *cpreg, 
	doublereal *t, doublereal *cpr, doublereal *cprdt, doublereal *cprdlt,
	 ftnlen phase_len)
{
    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    extern doublereal cp_(doublereal *, doublereal *, doublereal *, 
	    doublereal *), cpdt_(doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *), cpdlnt_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *);

    if (s_cmp(phase, "gas", (ftnlen)3, (ftnlen)3) == 0) {
	*cpr = cp_(t, &gasref_1.mkg[*i__ * 3 - 3], &gasref_1.mkg[*i__ * 3 - 2]
		, &gasref_1.mkg[*i__ * 3 - 1]);
	*cprdt = cpdt_(&refval_1.tref, t, &gasref_1.mkg[*i__ * 3 - 3], &
		gasref_1.mkg[*i__ * 3 - 2], &gasref_1.mkg[*i__ * 3 - 1]);
	*cprdlt = cpdlnt_(&refval_1.tref, t, &gasref_1.mkg[*i__ * 3 - 3], &
		gasref_1.mkg[*i__ * 3 - 2], &gasref_1.mkg[*i__ * 3 - 1]);
	return 0;
    }
/* **** phase = "min" ***** */
    if (*cpreg == 1) {
	*cpr = cp_(t, &minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2]
		, &minref_1.mk1[*i__ * 3 - 1]);
	*cprdt = cpdt_(&refval_1.tref, t, &minref_1.mk1[*i__ * 3 - 3], &
		minref_1.mk1[*i__ * 3 - 2], &minref_1.mk1[*i__ * 3 - 1]);
	*cprdlt = cpdlnt_(&refval_1.tref, t, &minref_1.mk1[*i__ * 3 - 3], &
		minref_1.mk1[*i__ * 3 - 2], &minref_1.mk1[*i__ * 3 - 1]);
	return 0;
    }
    if (*cpreg == 2) {
	*cpr = cp_(t, &minref_1.mk2[*i__ * 3 - 3], &minref_1.mk2[*i__ * 3 - 2]
		, &minref_1.mk2[*i__ * 3 - 1]);
	*cprdt = cpdt_(&refval_1.tref, &minref_1.ttran[*i__ * 3 - 3], &
		minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2], &
		minref_1.mk1[*i__ * 3 - 1]) + cpdt_(&minref_1.ttran[*i__ * 3 
		- 3], t, &minref_1.mk2[*i__ * 3 - 3], &minref_1.mk2[*i__ * 3 
		- 2], &minref_1.mk2[*i__ * 3 - 1]);
	*cprdlt = cpdlnt_(&refval_1.tref, &minref_1.ttran[*i__ * 3 - 3], &
		minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2], &
		minref_1.mk1[*i__ * 3 - 1]) + cpdlnt_(&minref_1.ttran[*i__ * 
		3 - 3], t, &minref_1.mk2[*i__ * 3 - 3], &minref_1.mk2[*i__ * 
		3 - 2], &minref_1.mk2[*i__ * 3 - 1]);
	return 0;
    }
    if (*cpreg == 3) {
	*cpr = cp_(t, &minref_1.mk3[*i__ * 3 - 3], &minref_1.mk3[*i__ * 3 - 2]
		, &minref_1.mk3[*i__ * 3 - 1]);
	*cprdt = cpdt_(&refval_1.tref, &minref_1.ttran[*i__ * 3 - 3], &
		minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2], &
		minref_1.mk1[*i__ * 3 - 1]) + cpdt_(&minref_1.ttran[*i__ * 3 
		- 3], &minref_1.ttran[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 
		3], &minref_1.mk2[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 1]) 
		+ cpdt_(&minref_1.ttran[*i__ * 3 - 2], t, &minref_1.mk3[*i__ *
		 3 - 3], &minref_1.mk3[*i__ * 3 - 2], &minref_1.mk3[*i__ * 3 
		- 1]);
	*cprdlt = cpdlnt_(&refval_1.tref, &minref_1.ttran[*i__ * 3 - 3], &
		minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2], &
		minref_1.mk1[*i__ * 3 - 1]) + cpdlnt_(&minref_1.ttran[*i__ * 
		3 - 3], &minref_1.ttran[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 
		- 3], &minref_1.mk2[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 1]
		) + cpdlnt_(&minref_1.ttran[*i__ * 3 - 2], t, &minref_1.mk3[*
		i__ * 3 - 3], &minref_1.mk3[*i__ * 3 - 2], &minref_1.mk3[*i__ 
		* 3 - 1]);
	return 0;
    }
/* **** Cpreg = 4 ***** */
    *cpr = cp_(t, &minref_1.mk4[*i__ * 3 - 3], &minref_1.mk4[*i__ * 3 - 2], &
	    minref_1.mk4[*i__ * 3 - 1]);
    *cprdt = cpdt_(&refval_1.tref, &minref_1.ttran[*i__ * 3 - 3], &
	    minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2], &
	    minref_1.mk1[*i__ * 3 - 1]) + cpdt_(&minref_1.ttran[*i__ * 3 - 3],
	     &minref_1.ttran[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 3], &
	    minref_1.mk2[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 1]) + cpdt_(&
	    minref_1.ttran[*i__ * 3 - 2], &minref_1.ttran[*i__ * 3 - 1], &
	    minref_1.mk3[*i__ * 3 - 3], &minref_1.mk3[*i__ * 3 - 2], &
	    minref_1.mk3[*i__ * 3 - 1]) + cpdt_(&minref_1.ttran[*i__ * 3 - 1],
	     t, &minref_1.mk4[*i__ * 3 - 3], &minref_1.mk4[*i__ * 3 - 2], &
	    minref_1.mk4[*i__ * 3 - 1]);
    *cprdlt = cpdlnt_(&refval_1.tref, &minref_1.ttran[*i__ * 3 - 3], &
	    minref_1.mk1[*i__ * 3 - 3], &minref_1.mk1[*i__ * 3 - 2], &
	    minref_1.mk1[*i__ * 3 - 1]) + cpdlnt_(&minref_1.ttran[*i__ * 3 - 
	    3], &minref_1.ttran[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 3], &
	    minref_1.mk2[*i__ * 3 - 2], &minref_1.mk2[*i__ * 3 - 1]) + 
	    cpdlnt_(&minref_1.ttran[*i__ * 3 - 2], &minref_1.ttran[*i__ * 3 - 
	    1], &minref_1.mk3[*i__ * 3 - 3], &minref_1.mk3[*i__ * 3 - 2], &
	    minref_1.mk3[*i__ * 3 - 1]) + cpdlnt_(&minref_1.ttran[*i__ * 3 - 
	    1], t, &minref_1.mk4[*i__ * 3 - 3], &minref_1.mk4[*i__ * 3 - 2], &
	    minref_1.mk4[*i__ * 3 - 1]);
    return 0;
} /* cptrms_ */

/* ******************************************************************** */
/* ** Cp - Returns the standard molal heat capacity at T. */
doublereal cp_(doublereal *t, doublereal *a, doublereal *b, doublereal *c__)
{
    /* System generated locals */
    doublereal ret_val, d__1;

/* Computing 2nd power */
    d__1 = *t;
    ret_val = *a + *b * *t + *c__ / (d__1 * d__1);
    return ret_val;
} /* cp_ */

/* ******************************************************************** */
/* ** CpdT - Returns the integral CpdT evaluated from T1 to T2. */
doublereal cpdt_(doublereal *t1, doublereal *t2, doublereal *a, doublereal *b,
	 doublereal *c__)
{
    /* System generated locals */
    doublereal ret_val, d__1, d__2;

/* Computing 2nd power */
    d__1 = *t2;
/* Computing 2nd power */
    d__2 = *t1;
    ret_val = *a * (*t2 - *t1) + *b / 2. * (d__1 * d__1 - d__2 * d__2) - *c__ 
	    * (1. / *t2 - 1. / *t1);
    return ret_val;
} /* cpdt_ */

/* ******************************************************************** */
/* ** CpdlnT - Returns the integral CpdlnT evaluated from T1 to T2. */
doublereal cpdlnt_(doublereal *t1, doublereal *t2, doublereal *a, doublereal *
	b, doublereal *c__)
{
    /* System generated locals */
    doublereal ret_val, d__1, d__2;

    /* Builtin functions */
    double log(doublereal);

/* Computing 2nd power */
    d__1 = *t2;
/* Computing 2nd power */
    d__2 = *t1;
    ret_val = *a * log(*t2 / *t1) + *b * (*t2 - *t1) - *c__ / 2. * (1. / (
	    d__1 * d__1) - 1. / (d__2 * d__2));
    return ret_val;
} /* cpdlnt_ */

/* ******************************************************************** */
/* ** pttrms - Computes phase transition terms for Smin, Hmin, and Gmin. */
/* Subroutine */ int pttrms_(integer *imin, integer *phaser, doublereal *t, 
	doublereal *spttrm, doublereal *hpttrm, doublereal *gpttrm)
{
    /* System generated locals */
    integer i__1;

    /* Local variables */
    static integer phtran;

    *spttrm = 0.;
    *hpttrm = 0.;
    *gpttrm = 0.;
    i__1 = *phaser - 1;
    for (phtran = 1; phtran <= i__1; ++phtran) {
	*spttrm += minref_1.htran[phtran + *imin * 3 - 4] / minref_1.ttran[
		phtran + *imin * 3 - 4];
	*hpttrm += minref_1.htran[phtran + *imin * 3 - 4];
	*gpttrm += minref_1.htran[phtran + *imin * 3 - 4] * (1. - *t / 
		minref_1.ttran[phtran + *imin * 3 - 4]);
/* L10: */
    }
    return 0;
} /* pttrms_ */

/* ********************************************************************* */
/* ** gases - Computes the standard molal thermodynamic properties of */
/*           ngas gases at P,T using equations given by */
/*           Helgeson et al. (1978). */
/* Subroutine */ int gases_(integer *ngas, doublereal *t)
{
    /* Initialized data */

    static integer specs[10] = { 2,2,2,5,1,0,2,0,4,0 };
    static doublereal states[4] = { 0.,1.,0.,0. };

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static integer i__;
    static doublereal tc;
    extern /* Subroutine */ int h2o92_(integer *, doublereal *, doublereal *, 
	    logical *);
    static doublereal cprdt;
    static logical error;
    static doublereal props[46], cprdlt;
    extern /* Subroutine */ int cptrms_(char *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, ftnlen);

    tc = *t - 273.15;
    i__1 = *ngas;
    for (i__ = 1; i__ <= i__1; ++i__) {
	if (s_cmp(gnames_1.gname + (i__ - 1) * 20, "H2O,g", (ftnlen)20, (
		ftnlen)5) == 0 && tc >= 99.6324) {
/* **             use Haar et al. (1984) equation of state to */
/* **             compute H2O,g properties at 1 bar, T > Tsat(1 bar) = */
/* **             99.6324 C.  Note that for T < Tsat(1 bar), */
/* **             thermodynamic properties of metastable H2O,g are */
/* **             calculated using parameters estimated by J. W. Johnson */
/* **             (3/90) that facilitate smooth transition into the */
/* **             Haar et al. (1984) equation at Tsat. */
/* ** */
/* **             Beacuse (1) P = 1 bar, and (2) thermodynamic properties */
/* **             of steam are independent of dielectric properties, */
/* **             specs(8..9) can be safely hardwired, as above. */
	    states[0] = tc;
	    h2o92_(specs, states, props, &error);
	    gassp_1.vgas[i__ - 1] = gasref_1.vprtrg[i__ - 1];
	    gassp_1.sgas[i__ - 1] = props[4];
	    gassp_1.hgas[i__ - 1] = props[8];
	    gassp_1.ggas[i__ - 1] = props[2];
	    gassp_1.cpgas[i__ - 1] = props[12];
	} else {
	    gassp_1.vgas[i__ - 1] = gasref_1.vprtrg[i__ - 1];
	    cptrms_("gas", &i__, &c__1, t, &gassp_1.cpgas[i__ - 1], &cprdt, &
		    cprdlt, (ftnlen)3);
	    gassp_1.sgas[i__ - 1] = gasref_1.sprtrg[i__ - 1] + cprdlt;
	    gassp_1.hgas[i__ - 1] = gasref_1.hfgas[i__ - 1] + cprdt;
	    gassp_1.ggas[i__ - 1] = gasref_1.gfgas[i__ - 1] - gasref_1.sprtrg[
		    i__ - 1] * (*t - refval_1.tref) + cprdt - *t * cprdlt;
	}
/* L10: */
    }
    return 0;
} /* gases_ */

/* *********************************************************************** */
/* ** aqsps - Computes the standard partial molal thermodynamic properties */
/* **         of naqs aqueous species at P,T using equations given by */
/* **         Tanger and Helgeson (1988), Shock et al. (1991), and */
/* **         Johnson et al. (1991). */
/* Subroutine */ int aqsps_(integer *naqs, doublereal *p, doublereal *t, 
	doublereal *dw, doublereal *betaw, doublereal *alphaw, doublereal *
	daldtw, doublereal *z__, doublereal *q, doublereal *y, doublereal *x, 
	integer *geqn)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2;

    /* Builtin functions */
    double log(doublereal);

    /* Local variables */
    static doublereal g;
    static integer j;
    static doublereal w, dgdp, dgdt, dwdp, dwdt, d2gdt2;
    extern /* Subroutine */ int omeg92_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, char *, 
	    ftnlen);
    static doublereal d2wdt2;
    extern /* Subroutine */ int gfun92_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, integer *)
	    ;

    if (*naqs == 0) {
	return 0;
    } else {
	d__1 = *t - 273.15;
	gfun92_(&d__1, p, dw, betaw, alphaw, daldtw, &g, &dgdp, &dgdt, &
		d2gdt2, geqn);
    }
    i__1 = *naqs;
    for (j = 1; j <= i__1; ++j) {
/* ***** compute w, dwdP, dwdT, d2wdT2 ****** */
	omeg92_(&g, &dgdp, &dgdt, &d2gdt2, &aqsref_1.wref[j - 1], &
		aqsref_1.chg[j - 1], &w, &dwdp, &dwdt, &d2wdt2, 
		anames_1.aname + (j - 1) * 20, (ftnlen)20);
	solvn_1.vqterm[j - 1] = (-w * *q + (-(*z__) - 1.) * dwdp) * 41.84004;
/* ** the leading constant converts cal/(mol*bar) -> cm3/mol */
	aqsp_1.vaqs[j - 1] = (aqsref_1.a[(j << 2) - 4] + aqsref_1.a[(j << 2) 
		- 3] / (aqscon_1.psi + *p) + aqsref_1.a[(j << 2) - 2] / (*t - 
		aqscon_1.theta) + aqsref_1.a[(j << 2) - 1] / (aqscon_1.psi + *
		p) / (*t - aqscon_1.theta)) * 41.84004 + solvn_1.vqterm[j - 1]
		;
	solvn_1.syterm[j - 1] = w * *y - (-(*z__) - 1.) * dwdt - 
		aqsref_1.wref[j - 1] * refval_1.yprtr;
/* Computing 2nd power */
	d__1 = 1. / (*t - aqscon_1.theta);
	aqsp_1.saqs[j - 1] = aqsref_1.sprtra[j - 1] + aqsref_1.c__[(j << 1) - 
		2] * log(*t / refval_1.tref) - aqsref_1.c__[(j << 1) - 1] / 
		aqscon_1.theta * (1. / (*t - aqscon_1.theta) - 1. / (
		refval_1.tref - aqscon_1.theta) + 1. / aqscon_1.theta * log(
		refval_1.tref * (*t - aqscon_1.theta) / *t / (refval_1.tref - 
		aqscon_1.theta))) + (aqsref_1.a[(j << 2) - 2] * (*p - 
		refval_1.pref) + aqsref_1.a[(j << 2) - 1] * log((aqscon_1.psi 
		+ *p) / (aqscon_1.psi + refval_1.pref))) * (d__1 * d__1) + 
		solvn_1.syterm[j - 1];
	solvn_1.cpxtrm[j - 1] = w * *t * *x + *t * 2. * *y * dwdt + *t * (*
		z__ + 1.) * d2wdt2;
/* Computing 2nd power */
	d__1 = *t - aqscon_1.theta;
/* Computing 3rd power */
	d__2 = *t - aqscon_1.theta;
	aqsp_1.cpaqs[j - 1] = aqsref_1.c__[(j << 1) - 2] + aqsref_1.c__[(j << 
		1) - 1] / (d__1 * d__1) - *t * 2. / (d__2 * (d__2 * d__2)) * (
		aqsref_1.a[(j << 2) - 2] * (*p - refval_1.pref) + aqsref_1.a[(
		j << 2) - 1] * log((aqscon_1.psi + *p) / (aqscon_1.psi + 
		refval_1.pref))) + solvn_1.cpxtrm[j - 1];
	solvn_1.hyterm[j - 1] = w * (-(*z__) - 1.) + w * *t * *y - *t * (-(*
		z__) - 1.) * dwdt - aqsref_1.wref[j - 1] * (-refval_1.zprtr - 
		1.) - aqsref_1.wref[j - 1] * refval_1.tref * refval_1.yprtr;
/* Computing 2nd power */
	d__1 = *t - aqscon_1.theta;
	aqsp_1.haqs[j - 1] = aqsref_1.hfaqs[j - 1] + aqsref_1.c__[(j << 1) - 
		2] * (*t - refval_1.tref) - aqsref_1.c__[(j << 1) - 1] * (1. /
		 (*t - aqscon_1.theta) - 1. / (refval_1.tref - aqscon_1.theta)
		) + aqsref_1.a[(j << 2) - 4] * (*p - refval_1.pref) + 
		aqsref_1.a[(j << 2) - 3] * log((aqscon_1.psi + *p) / (
		aqscon_1.psi + refval_1.pref)) + (aqsref_1.a[(j << 2) - 2] * (
		*p - refval_1.pref) + aqsref_1.a[(j << 2) - 1] * log((
		aqscon_1.psi + *p) / (aqscon_1.psi + refval_1.pref))) * ((*t *
		 2. - aqscon_1.theta) / (d__1 * d__1)) + solvn_1.hyterm[j - 1]
		;
	solvn_1.gzterm[j - 1] = w * (-(*z__) - 1.) - aqsref_1.wref[j - 1] * (
		-refval_1.zprtr - 1.) + aqsref_1.wref[j - 1] * refval_1.yprtr 
		* (*t - refval_1.tref);
/* Computing 2nd power */
	d__1 = aqscon_1.theta;
	aqsp_1.gaqs[j - 1] = aqsref_1.gfaqs[j - 1] - aqsref_1.sprtra[j - 1] * 
		(*t - refval_1.tref) - aqsref_1.c__[(j << 1) - 2] * (*t * log(
		*t / refval_1.tref) - *t + refval_1.tref) + aqsref_1.a[(j << 
		2) - 4] * (*p - refval_1.pref) + aqsref_1.a[(j << 2) - 3] * 
		log((aqscon_1.psi + *p) / (aqscon_1.psi + refval_1.pref)) - 
		aqsref_1.c__[(j << 1) - 1] * ((1. / (*t - aqscon_1.theta) - 
		1. / (refval_1.tref - aqscon_1.theta)) * ((aqscon_1.theta - *
		t) / aqscon_1.theta) - *t / (d__1 * d__1) * log(refval_1.tref 
		* (*t - aqscon_1.theta) / (*t * (refval_1.tref - 
		aqscon_1.theta)))) + 1. / (*t - aqscon_1.theta) * (aqsref_1.a[
		(j << 2) - 2] * (*p - refval_1.pref) + aqsref_1.a[(j << 2) - 
		1] * log((aqscon_1.psi + *p) / (aqscon_1.psi + refval_1.pref))
		) + solvn_1.gzterm[j - 1];
	solvn_1.gzterm[j - 1] = w * (-(*z__) - 1.);
/* L10: */
    }
    return 0;
} /* aqsps_ */

/* *********************************************************************** */
/* ** omeg92 - Computes the conventinal Born coefficient (w) of the */
/* **          current aqueous species, dwdP, dwdP, and dw2dT2 as a */
/* **          function of g, dgdP, dgdT, d2gdT2, wref, and Z using */
/* **          equations given by Johnson et al. (1991). */
/* Subroutine */ int omeg92_(doublereal *g, doublereal *dgdp, doublereal *
	dgdt, doublereal *d2gdt2, doublereal *wref, doublereal *z__, 
	doublereal *w, doublereal *dwdp, doublereal *dwdt, doublereal *d2wdt2,
	 char *aname, ftnlen aname_len)
{
    /* System generated locals */
    doublereal d__1, d__2, d__3, d__4;

    /* Builtin functions */
    integer s_cmp(char *, char *, ftnlen, ftnlen);

    /* Local variables */
    static doublereal z3, z4, re, reref;

    if (*z__ == 0. || s_cmp(aname, "H+", (ftnlen)20, (ftnlen)2) == 0) {
/* **        neutral aqueous species or H+ */
	*w = *wref;
	*dwdp = 0.;
	*dwdt = 0.;
	*d2wdt2 = 0.;
	return 0;
    } else {
/* **        charged aqueous species other than H+ */
/* Computing 2nd power */
	d__1 = *z__;
	reref = d__1 * d__1 / (*wref / aqscon_1.eta + *z__ / (aqscon_1.gref + 
		3.082));
	re = reref + abs(*z__) * *g;
/* Computing 2nd power */
	d__1 = *z__;
	*w = aqscon_1.eta * (d__1 * d__1 / re - *z__ / (*g + 3.082));
/* Computing 3rd power */
	d__2 = *z__;
/* Computing 2nd power */
	d__3 = re;
/* Computing 2nd power */
	d__4 = *g + 3.082;
	z3 = (d__1 = d__2 * (d__2 * d__2), abs(d__1)) / (d__3 * d__3) - *z__ /
		 (d__4 * d__4);
/* Computing 4th power */
	d__2 = *z__, d__2 *= d__2;
/* Computing 3rd power */
	d__3 = re;
/* Computing 3rd power */
	d__4 = *g + 3.082;
	z4 = (d__1 = d__2 * d__2, abs(d__1)) / (d__3 * (d__3 * d__3)) - *z__ /
		 (d__4 * (d__4 * d__4));
	*dwdp = -aqscon_1.eta * z3 * *dgdp;
	*dwdt = -aqscon_1.eta * z3 * *dgdt;
/* Computing 2nd power */
	d__1 = *dgdt;
	*d2wdt2 = aqscon_1.eta * 2. * z4 * (d__1 * d__1) - aqscon_1.eta * z3 *
		 *d2gdt2;
    }
    return 0;
} /* omeg92_ */

/* *********************************************************************** */
/* ** reactn - Computes the standard molal thermodynamic properties */
/* **          of the i[th] reaction. */
/* Subroutine */ int reactn_(integer *i__, doublereal *t, doublereal *vw, 
	doublereal *sw, doublereal *cpw, doublereal *hw, doublereal *gw)
{
    /* System generated locals */
    integer i__1;
    doublereal d__1;

    /* Local variables */
    static integer j;
    static doublereal dgra, dhra, dgrg, dhrg, dgrm, dhrm, dvrg, dsrg, dsrm, 
	    dvrm, dvra, dsra, dhrw, dgrw, dsrw, dvrw, dcpra, dcprg, dcprm, 
	    dcprw;

/* **** sum mineral contributions ***** */
    dvrm = 0.;
    dcprm = 0.;
    dsrm = 0.;
    dhrm = 0.;
    dgrm = 0.;
    i__1 = reac2_1.nm[*i__ - 1];
    for (j = 1; j <= i__1; ++j) {
	dvrm += reac2_1.coefm[*i__ + j * 10 - 11] * minsp_1.vmin[j - 1];
	dcprm += reac2_1.coefm[*i__ + j * 10 - 11] * minsp_1.cpmin[j - 1];
	dsrm += reac2_1.coefm[*i__ + j * 10 - 11] * minsp_1.smin[j - 1];
	dhrm += reac2_1.coefm[*i__ + j * 10 - 11] * minsp_1.hmin[j - 1];
	dgrm += reac2_1.coefm[*i__ + j * 10 - 11] * minsp_1.gmin[j - 1];
/* L10: */
    }
/* **** sum gas contributions ***** */
    dvrg = 0.;
    dcprg = 0.;
    dsrg = 0.;
    dhrg = 0.;
    dgrg = 0.;
    i__1 = reac2_1.ng[*i__ - 1];
    for (j = 1; j <= i__1; ++j) {
	dvrg += reac2_1.coefg[*i__ + j * 10 - 11] * gassp_1.vgas[j - 1];
	dcprg += reac2_1.coefg[*i__ + j * 10 - 11] * gassp_1.cpgas[j - 1];
	dsrg += reac2_1.coefg[*i__ + j * 10 - 11] * gassp_1.sgas[j - 1];
	dhrg += reac2_1.coefg[*i__ + j * 10 - 11] * gassp_1.hgas[j - 1];
	dgrg += reac2_1.coefg[*i__ + j * 10 - 11] * gassp_1.ggas[j - 1];
/* L20: */
    }
/* **** sum aqueous species contributions ***** */
    dvra = 0.;
    dcpra = 0.;
    dsra = 0.;
    dhra = 0.;
    dgra = 0.;
    i__1 = reac2_1.na[*i__ - 1];
    for (j = 1; j <= i__1; ++j) {
	dvra += reac2_1.coefa[*i__ + j * 10 - 11] * aqsp_1.vaqs[j - 1];
	dcpra += reac2_1.coefa[*i__ + j * 10 - 11] * aqsp_1.cpaqs[j - 1];
	dsra += reac2_1.coefa[*i__ + j * 10 - 11] * aqsp_1.saqs[j - 1];
	dhra += reac2_1.coefa[*i__ + j * 10 - 11] * aqsp_1.haqs[j - 1];
	dgra += reac2_1.coefa[*i__ + j * 10 - 11] * aqsp_1.gaqs[j - 1];
/* L30: */
    }
/* **** calculate H2O contributions ***** */
    dvrw = reac2_1.coefw[*i__ - 1] * *vw;
    dsrw = reac2_1.coefw[*i__ - 1] * *sw;
    dcprw = reac2_1.coefw[*i__ - 1] * *cpw;
    dhrw = reac2_1.coefw[*i__ - 1] * *hw;
    dgrw = reac2_1.coefw[*i__ - 1] * *gw;
/* **** calculate reaction properties ***** */
    fmeq_1.dvr = dvrm + dvrg + dvra + dvrw;
    fmeq_1.dsr = dsrm + dsrg + dsra + dsrw;
    fmeq_1.dcpr = dcprm + dcprg + dcpra + dcprw;
    fmeq_1.dhr = dhrm + dhrg + dhra + dhrw;
    fmeq_1.dgr = dgrm + dgrg + dgra + dgrw;
    fmeq_1.logkr = -fmeq_1.dgr / (refval_1.r__ * 2.302585 * *t);
/* Computing 2nd power */
    d__1 = *t;
    fmeq_1.dlogkt = fmeq_1.dhr / (refval_1.r__ * 2.302585 * (d__1 * d__1));
    fmeq_1.dlogkp = fmeq_1.dvr * -.023901488 / (refval_1.r__ * 2.302585 * *t);
    return 0;
} /* reactn_ */

/* ***************************************************************** */
/* ** gfun92 - Computes the g function (Tanger and Helgeson, 1988; */
/* **          Shock et al., 1991) and its partial derivatives */
/* **          (dgdP, dgdT, d2gdT2) at TdegC, Pbars using the */
/* **          computational algorithm specified by geqn. */
/* ** */
/* **        geqn = 1 ...... use Tanger-Helgeson (1988) equations */
/* **        geqn = 2 ...... use Shock et al. (1991) equations */
/* **                        without the f(P,T) difference function */
/* **        geqn = 3 ...... use Shock et al. (1991) equations */
/* **                        with the f(P,T) difference function */
/* Subroutine */ int gfun92_(doublereal *tdegc, doublereal *pbars, doublereal 
	*dgcm3, doublereal *betab, doublereal *alphak, doublereal *daldt, 
	doublereal *g, doublereal *dgdp, doublereal *dgdt, doublereal *d2gdt2,
	 integer *geqn)
{
    extern /* Subroutine */ int gshok2_(doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *);

/* ***** initialize g and derivatives to zero */
    *g = 0.;
    *dgdp = 0.;
    *dgdt = 0.;
    *d2gdt2 = 0.;
    if (*tdegc > 1000.0001 || *pbars > 5000.0001000000002) {
	return 0;
    }
/*     IF (geqn .EQ. 1) THEN */
/* *****     use Tanger-Helgeson (1988) equations */
/*          CALL gTangr(Pbars,TdegC+273.15d0,Dgcm3,betab,alphaK,daldT, */
/*    2                 g,dgdP,dgdT,d2gdT2) */
/*          RETURN */
/*     END IF */
/*     IF (geqn .EQ. 2) THEN */
/* *****     use Shock et al. (1991) equations */
/* *****     without f(P,T) difference function */
/*          CALL gShok1(TdegC,Pbars,Dgcm3,betab,alphaK,daldT, */
/*    2                 g,dgdP,dgdT,d2gdT2) */
/*          RETURN */
/*     END IF */
    if (*geqn == 3) {
/* *****     use Shock et al. (1991) equations */
/* *****     with f(P,T) difference function */
	gshok2_(tdegc, pbars, dgcm3, betab, alphak, daldt, g, dgdp, dgdt, 
		d2gdt2);
	return 0;
    }
    return 0;
} /* gfun92_ */

/* **************************************************************** */
/* ** gShok2- Computes g, dgdP, dgdT, and d2gdT2 using equations given */
/* **         by Shock et al. (1991) */
/* ** */
/* ** units:   T ................. C */
/* **          D ................. g/cm**3 */
/* **          beta, dgdP ........ bars**(-1) */
/* **          alpha, dgdT ....... K**(-1) */
/* **          daldT, d2gdT2 ..... K**(-2) */
/* Subroutine */ int gshok2_(doublereal *t, doublereal *p, doublereal *d__, 
	doublereal *beta, doublereal *alpha, doublereal *daldt, doublereal *g,
	 doublereal *dgdp, doublereal *dgdt, doublereal *d2gdt2)
{
    /* Initialized data */

    static doublereal c__[6] = { -2.037662,.005747,-6.557892e-6,6.107361,
	    -.01074377,1.268348e-5 };
    static doublereal cc[3] = { 36.66666,-1.504956e-10,5.017997e-14 };

    /* System generated locals */
    doublereal d__1, d__2, d__3, d__4, d__5, d__6, d__7, d__8, d__9, d__10;

    /* Builtin functions */
    double pow_dd(doublereal *, doublereal *), log(doublereal);

    /* Local variables */
    static doublereal a, b, f, db, fp, ft, dgdd, dadt, dbdt, dddt, dddp, dfdp,
	     dfdt, dgdd2, ddbdt, d2fdt2, dfpdp, dadtt, dbdtt, dddtt, dftdt, 
	    ddbdtt, dftdtt;

    if (*d__ >= 1.) {
	return 0;
    }
/* Computing 2nd power */
    d__1 = *t;
    a = c__[0] + c__[1] * *t + c__[2] * (d__1 * d__1);
/* Computing 2nd power */
    d__1 = *t;
    b = c__[3] + c__[4] * *t + c__[5] * (d__1 * d__1);
    d__1 = 1. - *d__;
    *g = a * pow_dd(&d__1, &b);
    d__1 = 1. - *d__;
    d__2 = b - 1.;
    dgdd = -a * b * pow_dd(&d__1, &d__2);
    d__1 = 1. - *d__;
    d__2 = b - 2.;
    dgdd2 = a * b * (b - 1.) * pow_dd(&d__1, &d__2);
    dadt = c__[1] + c__[2] * 2. * *t;
    dadtt = c__[2] * 2.;
    dbdt = c__[4] + c__[5] * 2. * *t;
    dbdtt = c__[5] * 2.;
    dddt = -(*d__) * *alpha;
    dddp = *d__ * *beta;
/* Computing 2nd power */
    d__1 = *alpha;
    dddtt = -(*d__) * (*daldt - d__1 * d__1);
    d__1 = 1. - *d__;
    db = pow_dd(&d__1, &b);
    d__1 = 1. - *d__;
    d__2 = b - 1.;
    ddbdt = -b * pow_dd(&d__1, &d__2) * dddt + log(1. - *d__) * db * dbdt;
    d__1 = 1. - *d__;
    d__2 = b - 1.;
    d__3 = 1. - *d__;
    d__4 = b - 1.;
    d__5 = 1. - *d__;
    d__6 = b - 2.;
    d__7 = 1. - *d__;
    d__8 = b - 1.;
    d__9 = 1. - *d__;
    d__10 = 1. - *d__;
    ddbdtt = -(b * pow_dd(&d__1, &d__2) * dddtt + pow_dd(&d__3, &d__4) * dddt 
	    * dbdt + b * dddt * (-(b - 1.) * pow_dd(&d__5, &d__6) * dddt + 
	    log(1. - *d__) * pow_dd(&d__7, &d__8) * dbdt)) + log(1. - *d__) * 
	    pow_dd(&d__9, &b) * dbdtt - pow_dd(&d__10, &b) * dbdt * dddt / (
	    1. - *d__) + log(1. - *d__) * dbdt * ddbdt;
    *dgdp = dgdd * dddp;
    *dgdt = a * ddbdt + db * dadt;
    *d2gdt2 = a * ddbdtt + ddbdt * 2. * dadt + db * dadtt;
    if (*t < 155. || *p > 1e3 || *t > 355.) {
	return 0;
    }
    d__1 = (*t - 155.) / 300.;
/* Computing 16th power */
    d__2 = (*t - 155.) / 300., d__2 *= d__2, d__2 *= d__2, d__2 *= d__2;
    ft = pow_dd(&d__1, &c_b1070) + cc[0] * (d__2 * d__2);
    d__1 = (*t - 155.) / 300.;
/* Computing 15th power */
    d__2 = (*t - 155.) / 300., d__3 = d__2, d__2 *= d__2, d__3 *= d__2, d__2 
	    *= d__2, d__3 *= d__2;
    dftdt = pow_dd(&d__1, &c_b1071) * .016 + cc[0] * .053333333333333337 * (
	    d__3 * (d__2 * d__2));
    d__1 = (*t - 155.) / 300.;
/* Computing 14th power */
    d__2 = (*t - 155.) / 300., d__2 *= d__2, d__3 = d__2, d__2 *= d__2, d__3 
	    *= d__2;
    dftdtt = pow_dd(&d__1, &c_b1072) * 2.0266666666666664e-4 + cc[0] * 
	    .0026666666666666666 * (d__3 * (d__2 * d__2));
/* Computing 3rd power */
    d__1 = 1e3 - *p;
/* Computing 4th power */
    d__2 = 1e3 - *p, d__2 *= d__2;
    fp = cc[1] * (d__1 * (d__1 * d__1)) + cc[2] * (d__2 * d__2);
/* Computing 2nd power */
    d__1 = 1e3 - *p;
/* Computing 3rd power */
    d__2 = 1e3 - *p;
    dfpdp = cc[1] * -3. * (d__1 * d__1) - cc[2] * 4. * (d__2 * (d__2 * d__2));
    f = ft * fp;
    dfdp = ft * dfpdp;
    dfdt = fp * dftdt;
    d2fdt2 = fp * dftdtt;
    *g -= f;
    *dgdp -= dfdp;
    *dgdt -= dfdt;
    *d2gdt2 -= d2fdt2;
    return 0;
} /* gshok2_ */

/* ** rep92 - Collection of routines that write the calculated standard */
/* **         molal thermodynamic properties of reactions to the TAB */
/* **         file and, optionally, the PLOT files. */
/* ** */
/* ******************************************************************** */
/* ** */
/* ** Author:     James W. Johnson */
/* **             Earth Sciences Department, L-219 */
/* **             Lawrence Livermore National Laboratory */
/* **             Livermore, CA 94550 */
/* **             johnson@s05.es.llnl.gov */
/* ** */
/* ** Abandoned:  8 November 1991 */
/* ** */
/* ******************************************************************** */
/* ** tabtop - Write global header for output file. */
/* Subroutine */ int tabtop_(void)
{
    /* Initialized data */

    static char nosave[20] = "file not saved      ";

    /* Format strings */
    static char fmt_5[] = "(/,\002 ***** SUPCRT92: input/output specificatio"
	    "ns for\002,\002 this run\002,/)";
    static char fmt_15[] = "(\002            USER-SPECIFIED  CON FILE  conta"
	    "ining \002,/,\002            T-P-D grid & option switches: \002,"
	    "a20,/)";
    static char fmt_25[] = "(\002            USER-SPECIFIED  RXN FILE  conta"
	    "ining \002,/,\002            chemical reactions: \002,a20,/)";
    static char fmt_35[] = "(\002            THERMODYNAMIC DATABASE: \002,a2"
	    "0,/)";
    static char fmt_45[] = "(\002            SUPCRT-GENERATED  TAB FILE  con"
	    "taining \002,/,\002            tabulated reaction properties "
	    "\002,\002(this file): \002,a20)";
    static char fmt_47[] = "(/,\002            SUPCRT-GENERATED  PLT FILES  "
	    "containing \002,/,\002            reaction properties for x-y pl"
	    "ots: \002,a20,\002 etc.\002)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int wrtopt_(void);

    /* Fortran I/O blocks */
    static cilist io___1108 = { 0, 0, 0, fmt_5, 0 };
    static cilist io___1109 = { 0, 0, 0, fmt_15, 0 };
    static cilist io___1110 = { 0, 0, 0, fmt_15, 0 };
    static cilist io___1111 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___1112 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___1113 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1114 = { 0, 0, 0, fmt_45, 0 };
    static cilist io___1115 = { 0, 0, 0, fmt_47, 0 };


    io___1108.ciunit = io_1.tabf;
    s_wsfe(&io___1108);
    e_wsfe();
    if (saveif_1.savecf) {
	io___1109.ciunit = io_1.tabf;
	s_wsfe(&io___1109);
	do_fio(&c__1, fnames_1.namecf, (ftnlen)20);
	e_wsfe();
    } else {
	io___1110.ciunit = io_1.tabf;
	s_wsfe(&io___1110);
	do_fio(&c__1, nosave, (ftnlen)20);
	e_wsfe();
    }
    if (saveif_1.saverf) {
	io___1111.ciunit = io_1.tabf;
	s_wsfe(&io___1111);
	do_fio(&c__1, fnames_1.namerf, (ftnlen)20);
	e_wsfe();
    } else {
	io___1112.ciunit = io_1.tabf;
	s_wsfe(&io___1112);
	do_fio(&c__1, nosave, (ftnlen)20);
	e_wsfe();
    }
    io___1113.ciunit = io_1.tabf;
    s_wsfe(&io___1113);
    do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
    e_wsfe();
    io___1114.ciunit = io_1.tabf;
    s_wsfe(&io___1114);
    do_fio(&c__1, fnames_1.nametf, (ftnlen)20);
    e_wsfe();
    if (plottr_1.xyplot > 0) {
	io___1115.ciunit = io_1.tabf;
	s_wsfe(&io___1115);
	do_fio(&c__1, fnames_1.namepf, (ftnlen)20);
	e_wsfe();
    }
    wrtopt_();
    return 0;
} /* tabtop_ */

/* ****************************************************************** */
/* ** wrtopt - Write various switch options to tabular output file. */
/* Subroutine */ int wrtopt_(void)
{
    /* Format strings */
    static char fmt_75[] = "(/,\002 ***** summary of option switches \002,/)";
    static char fmt_85[] = "(\002            isat, iopt, iplot, univar, noni"
	    "nc: \002,5i3)";
    static char fmt_115[] = "(/,\002 ***** summary of state conditions \002,"
	    "/)";
    static char fmt_125[] = "(12x,\002ISO\002,a12,\002:  min, max, increme"
	    "nt:\002,3(2x,f10.4))";
    static char fmt_135[] = "(12x,a10,\002 range: min, max, increment:\002,3"
	    "(2x,f10.4))";
    static char fmt_145[] = "(12x,\002saturation \002,a10,\002 range: min, m"
	    "ax,\002,\002 increment:\002,3(2x,f10.4))";
    static char fmt_155[] = "(12x,\002nonincremental \002,a10,\002, \002,a"
	    "10,\002 coordinates: \002,i2,\002 pair\002)";
    static char fmt_165[] = "(12x,\002nonincremental saturation \002,a10,"
	    "\002: \002,i2,\002 points\002)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1116 = { 0, 0, 0, fmt_75, 0 };
    static cilist io___1117 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___1118 = { 0, 0, 0, fmt_115, 0 };
    static cilist io___1119 = { 0, 0, 0, fmt_125, 0 };
    static cilist io___1120 = { 0, 0, 0, fmt_135, 0 };
    static cilist io___1121 = { 0, 0, 0, fmt_145, 0 };
    static cilist io___1122 = { 0, 0, 0, fmt_155, 0 };
    static cilist io___1123 = { 0, 0, 0, fmt_165, 0 };


    io___1116.ciunit = io_1.tabf;
    s_wsfe(&io___1116);
    e_wsfe();
    io___1117.ciunit = io_1.tabf;
    s_wsfe(&io___1117);
    do_fio(&c__1, (char *)&icon_1.isat, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.iopt, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.iplot, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.univar, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
    e_wsfe();
/* ** useLVS, epseqn, geqn not written to TAB for distribution copies */
/* ** */
/*      WRITE(tabf,105) useLVS,epseqn,geqn */
/* 105  FORMAT(  '            useLVS, epseqn, geqn:              ',3i3) */
    io___1118.ciunit = io_1.tabf;
    s_wsfe(&io___1118);
    e_wsfe();
    if (icon_1.noninc == 0) {
	if (icon_1.isat == 0) {
	    io___1119.ciunit = io_1.tabf;
	    s_wsfe(&io___1119);
	    do_fio(&c__1, stvars_1.isovar + (icon_1.iopt + (icon_1.iplot << 1)
		     - 3) * 12, (ftnlen)12);
	    do_fio(&c__1, (char *)&grid_1.isomin, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.isomax, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.isoinc, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1120.ciunit = io_1.tabf;
	    s_wsfe(&io___1120);
	    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	} else {
	    io___1121.ciunit = io_1.tabf;
	    s_wsfe(&io___1121);
	    do_fio(&c__1, stvars_1.isosat + (icon_1.iopt - 1) * 10, (ftnlen)
		    10);
	    do_fio(&c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
    } else {
	if (icon_1.isat == 0) {
	    io___1122.ciunit = io_1.tabf;
	    s_wsfe(&io___1122);
	    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	    e_wsfe();
	} else {
	    io___1123.ciunit = io_1.tabf;
	    s_wsfe(&io___1123);
	    do_fio(&c__1, stvars_1.isosat + (icon_1.iopt - 1) * 10, (ftnlen)
		    10);
	    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	    e_wsfe();
	}
    }
    return 0;
} /* wrtopt_ */

/* ******************************************************************** */
/* ** wrtop2 - Write various switch options to plot file k. */
/* Subroutine */ int wrtop2_(integer *k)
{
    /* Format strings */
    static char fmt_75[] = "(/,\002 ***** summary of option switches \002,/)";
    static char fmt_85[] = "(\002            isat, iopt, iplot, univar, noni"
	    "nc: \002,5i3)";
    static char fmt_115[] = "(/,\002 ***** summary of state conditions \002,"
	    "/)";
    static char fmt_125[] = "(12x,\002ISO\002,a12,\002:  min, max, increme"
	    "nt:\002,3(2x,f10.4))";
    static char fmt_135[] = "(12x,a10,\002 range: min, max, increment:\002,3"
	    "(2x,f10.4))";
    static char fmt_145[] = "(12x,\002saturation \002,a10,\002 range: min, m"
	    "ax,\002,\002 increment:\002,3(2x,f10.4))";
    static char fmt_155[] = "(12x,\002nonincremental \002,a10,\002, \002,a"
	    "10,\002 coordinates: \002,i2,\002 pair\002)";
    static char fmt_165[] = "(12x,\002nonincremental saturation \002,a10,"
	    "\002: \002,i2,\002 points\002)";
    static char fmt_175[] = "(/,86(\002*\002))";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Fortran I/O blocks */
    static cilist io___1124 = { 0, 0, 0, fmt_75, 0 };
    static cilist io___1125 = { 0, 0, 0, fmt_85, 0 };
    static cilist io___1126 = { 0, 0, 0, fmt_115, 0 };
    static cilist io___1127 = { 0, 0, 0, fmt_125, 0 };
    static cilist io___1128 = { 0, 0, 0, fmt_135, 0 };
    static cilist io___1129 = { 0, 0, 0, fmt_145, 0 };
    static cilist io___1130 = { 0, 0, 0, fmt_155, 0 };
    static cilist io___1131 = { 0, 0, 0, fmt_165, 0 };
    static cilist io___1132 = { 0, 0, 0, fmt_175, 0 };


    io___1124.ciunit = io_1.plotf[*k - 1];
    s_wsfe(&io___1124);
    e_wsfe();
    io___1125.ciunit = io_1.plotf[*k - 1];
    s_wsfe(&io___1125);
    do_fio(&c__1, (char *)&icon_1.isat, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.iopt, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.iplot, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.univar, (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
    e_wsfe();
/* ** useLVS, epseqn, geqn not written to PLOT for distribution copies */
/* ** */
/*      WRITE(plotf(k),105) useLVS,epseqn,geqn */
/* 105  FORMAT(  '            useLVS, epseqn, geqn:              ',3i3) */
    io___1126.ciunit = io_1.plotf[*k - 1];
    s_wsfe(&io___1126);
    e_wsfe();
    if (icon_1.noninc == 0) {
	if (icon_1.isat == 0) {
	    io___1127.ciunit = io_1.plotf[*k - 1];
	    s_wsfe(&io___1127);
	    do_fio(&c__1, stvars_1.isovar + (icon_1.iopt + (icon_1.iplot << 1)
		     - 3) * 12, (ftnlen)12);
	    do_fio(&c__1, (char *)&grid_1.isomin, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.isomax, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.isoinc, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1128.ciunit = io_1.plotf[*k - 1];
	    s_wsfe(&io___1128);
	    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	} else {
	    io___1129.ciunit = io_1.plotf[*k - 1];
	    s_wsfe(&io___1129);
	    do_fio(&c__1, stvars_1.isosat + (icon_1.iopt - 1) * 10, (ftnlen)
		    10);
	    do_fio(&c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&grid_1.v2inc, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
    } else {
	if (icon_1.isat == 0) {
	    io___1130.ciunit = io_1.plotf[*k - 1];
	    s_wsfe(&io___1130);
	    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	    e_wsfe();
	} else {
	    io___1131.ciunit = io_1.plotf[*k - 1];
	    s_wsfe(&io___1131);
	    do_fio(&c__1, stvars_1.isosat + (icon_1.iopt - 1) * 10, (ftnlen)
		    10);
	    do_fio(&c__1, (char *)&icon_1.noninc, (ftnlen)sizeof(integer));
	    e_wsfe();
	}
    }
    io___1132.ciunit = io_1.plotf[*k - 1];
    s_wsfe(&io___1132);
    e_wsfe();
    return 0;
} /* wrtop2_ */

/* ************************************************************** */
/* ** wrtrxn - Write header information for the i[th] reaction to */
/* **          tabulated output file tabf and (if appropriate) to */
/* **          the plot files. */
/* Subroutine */ int wrtrxn_(integer *i__)
{
    /* Format strings */
    static char fmt_5[] = "(//,36(\002*\002),\002 REACTION \002,i2,2x,36("
	    "\002*\002),/)";
    static char fmt_15[] = "(\002 REACTION TITLE: \002,//,6x,a80,//)";
    static char fmt_25[] = "(\002 REACTION STOICHIOMETRY: \002,/)";
    static char fmt_26[] = "(8x,\002 COEFF.\002,3x,\002NAME\002,16x,3x,\002F"
	    "ORMULA\002,22x,/,8x,7(\002-\002),3x,20(\002-\002),3x,20(\002-"
	    "\002))";
    static char fmt_35[] = "(6x,f9.3,3x,a20,3x,a30)";
    static char fmt_36[] = "(6x,f9.3,3x,a20,3x,a20)";
    static char fmt_55[] = "(6x,f9.3,3x,\002H2O\002,17x,3x,\002H2O\002)";
    static char fmt_65[] = "(//,\002 STANDARD STATE PROPERTIES OF THE REACTI"
	    "ON\002,\002 AT ELEVATED TEMPERATURES AND PRESSURES \002,//)";
    static char fmt_888[] = "(\002 CAUTION: INCOMPLETE DATA FOR ONE OR MORE "
	    "SPECIES\002,//)";
    static char fmt_75[] = "(50x,\002 DELTA G  \002,1x,1x,\002 DELTA H  \002"
	    ",1x,1x,\002 DELTA S  \002,1x,1x,\002 DELTA V  \002,1x,\002 DELTA"
	    " Cp \002,1x,/,2x,a10,2x,a10,2x,a10,2x,\002  LOG K   \002,1x,1x"
	    ",\002  (cal)   \002,1x,1x,\002  (cal)   \002,1x,1x,\002 (cal/K)  "
	    "\002,1x,1x,\002   (cc)   \002,1x,1x,\002 (cal/K)  \002,1x,/,3(2x"
	    ",10(\002-\002)),1x,6(1x,10(\002-\002),1x))";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer j;
    extern /* Subroutine */ int zero_(integer *, logical *);
    static logical nullrx;
    extern /* Subroutine */ int pltrxn_(integer *), wrtssp_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1133 = { 0, 0, 0, fmt_5, 0 };
    static cilist io___1134 = { 0, 0, 0, fmt_15, 0 };
    static cilist io___1135 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___1136 = { 0, 0, 0, fmt_26, 0 };
    static cilist io___1138 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1139 = { 0, 0, 0, fmt_36, 0 };
    static cilist io___1140 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1141 = { 0, 0, 0, fmt_55, 0 };
    static cilist io___1142 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1143 = { 0, 0, 0, fmt_36, 0 };
    static cilist io___1144 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1145 = { 0, 0, 0, fmt_55, 0 };
    static cilist io___1146 = { 0, 0, 0, fmt_65, 0 };
    static cilist io___1148 = { 0, 0, 0, fmt_888, 0 };
    static cilist io___1149 = { 0, 0, 0, fmt_75, 0 };


/* **** write header to TAB file ***** */
    io___1133.ciunit = io_1.tabf;
    s_wsfe(&io___1133);
    do_fio(&c__1, (char *)&(*i__), (ftnlen)sizeof(integer));
    e_wsfe();
/* **** write reaction title and stoichiometry to TAB file ***** */
    io___1134.ciunit = io_1.tabf;
    s_wsfe(&io___1134);
    do_fio(&c__1, reac1_1.rtitle + (*i__ - 1) * 80, (ftnlen)80);
    e_wsfe();
    io___1135.ciunit = io_1.tabf;
    s_wsfe(&io___1135);
    e_wsfe();
    io___1136.ciunit = io_1.tabf;
    s_wsfe(&io___1136);
    e_wsfe();
/* **** write reactants ***** */
    if (reac2_1.nm[*i__ - 1] > 0) {
	i__1 = reac2_1.nm[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (reac2_1.coefm[*i__ + j * 10 - 11] < 0.) {
		io___1138.ciunit = io_1.tabf;
		s_wsfe(&io___1138);
		do_fio(&c__1, (char *)&reac2_1.coefm[*i__ + j * 10 - 11], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)20);
		do_fio(&c__1, mnames_1.mform + (j - 1) * 30, (ftnlen)30);
		e_wsfe();
	    }
/* L10: */
	}
    }
    if (reac2_1.ng[*i__ - 1] > 0) {
	i__1 = reac2_1.ng[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (reac2_1.coefg[*i__ + j * 10 - 11] < 0.) {
		io___1139.ciunit = io_1.tabf;
		s_wsfe(&io___1139);
		do_fio(&c__1, (char *)&reac2_1.coefg[*i__ + j * 10 - 11], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, gnames_1.gform + (j - 1) * 30, (ftnlen)20);
		do_fio(&c__1, gnames_1.gname + (j - 1) * 20, (ftnlen)20);
		e_wsfe();
	    }
/* L20: */
	}
    }
    if (reac2_1.na[*i__ - 1] > 0) {
	i__1 = reac2_1.na[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (reac2_1.coefa[*i__ + j * 10 - 11] < 0.) {
		io___1140.ciunit = io_1.tabf;
		s_wsfe(&io___1140);
		do_fio(&c__1, (char *)&reac2_1.coefa[*i__ + j * 10 - 11], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, anames_1.aname + (j - 1) * 20, (ftnlen)20);
		do_fio(&c__1, anames_1.aform + (j - 1) * 30, (ftnlen)30);
		e_wsfe();
	    }
/* L30: */
	}
    }
    if (reac2_1.nw[*i__ - 1] > 0 && reac2_1.coefw[*i__ - 1] < 0.) {
	io___1141.ciunit = io_1.tabf;
	s_wsfe(&io___1141);
	do_fio(&c__1, (char *)&reac2_1.coefw[*i__ - 1], (ftnlen)sizeof(
		doublereal));
	e_wsfe();
    }
/* **** write products ***** */
    if (reac2_1.nm[*i__ - 1] > 0) {
	i__1 = reac2_1.nm[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (reac2_1.coefm[*i__ + j * 10 - 11] > 0.) {
		io___1142.ciunit = io_1.tabf;
		s_wsfe(&io___1142);
		do_fio(&c__1, (char *)&reac2_1.coefm[*i__ + j * 10 - 11], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)20);
		do_fio(&c__1, mnames_1.mform + (j - 1) * 30, (ftnlen)30);
		e_wsfe();
	    }
/* L11: */
	}
    }
    if (reac2_1.ng[*i__ - 1] > 0) {
	i__1 = reac2_1.ng[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (reac2_1.coefg[*i__ + j * 10 - 11] > 0.) {
		io___1143.ciunit = io_1.tabf;
		s_wsfe(&io___1143);
		do_fio(&c__1, (char *)&reac2_1.coefg[*i__ + j * 10 - 11], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, gnames_1.gform + (j - 1) * 30, (ftnlen)20);
		do_fio(&c__1, gnames_1.gname + (j - 1) * 20, (ftnlen)20);
		e_wsfe();
	    }
/* L21: */
	}
    }
    if (reac2_1.na[*i__ - 1] > 0) {
	i__1 = reac2_1.na[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (reac2_1.coefa[*i__ + j * 10 - 11] > 0.) {
		io___1144.ciunit = io_1.tabf;
		s_wsfe(&io___1144);
		do_fio(&c__1, (char *)&reac2_1.coefa[*i__ + j * 10 - 11], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, anames_1.aname + (j - 1) * 20, (ftnlen)20);
		do_fio(&c__1, anames_1.aform + (j - 1) * 30, (ftnlen)30);
		e_wsfe();
	    }
/* L31: */
	}
    }
    if (reac2_1.nw[*i__ - 1] > 0 && reac2_1.coefw[*i__ - 1] > 0.) {
	io___1145.ciunit = io_1.tabf;
	s_wsfe(&io___1145);
	do_fio(&c__1, (char *)&reac2_1.coefw[*i__ - 1], (ftnlen)sizeof(
		doublereal));
	e_wsfe();
    }
/* **** write standard state properties, equation-of-state */
/* **** parameters, and heat capacity coefficients */
    wrtssp_(i__);
/* **** write header for property tabulation ***** */
    io___1146.ciunit = io_1.tabf;
    s_wsfe(&io___1146);
    e_wsfe();
    zero_(i__, &nullrx);
    if (nullrx) {
	io___1148.ciunit = io_1.tabf;
	s_wsfe(&io___1148);
	e_wsfe();
    }
    io___1149.ciunit = io_1.tabf;
    s_wsfe(&io___1149);
    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (icon_1.iplot << 1) - 3) * 
	    10, (ftnlen)10);
    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) - 3) * 
	    10, (ftnlen)10);
    do_fio(&c__1, headmp_1.var3 + (icon_1.iopt + (icon_1.iplot << 1) - 3) * 
	    10, (ftnlen)10);
    e_wsfe();
    if (plottr_1.xyplot > 0) {
	pltrxn_(i__);
    }
    return 0;
} /* wrtrxn_ */

/* **************************************************************** */
/* **** zero - Zero-out NULL values for reaction i to eliminate their */
/* ****        contribution to standard molal properties at elevated */
/* ****        temperatures and pressures; set nullrx to .TRUE. if Gf */
/* ****        missing for mineral species or a1..4 for aqueous species. */
/* Subroutine */ int zero_(integer *i__, logical *nullrx)
{
    /* System generated locals */
    integer i__1, i__2;

    /* Local variables */
    static integer j, k;

    *nullrx = FALSE_;
    i__1 = reac2_1.nm[*i__ - 1];
    for (j = 1; j <= i__1; ++j) {
	if (minref_1.gfmin[j - 1] == null_1.xnullm) {
	    *nullrx = TRUE_;
	    minref_1.gfmin[j - 1] = 0.;
	    minref_1.hfmin[j - 1] = 0.;
	}
	if (minref_1.ntran[j - 1] > 0) {
	    i__2 = minref_1.ntran[j - 1];
	    for (k = 1; k <= i__2; ++k) {
		if (minref_1.htran[k + j * 3 - 4] == null_1.xnullm) {
		    minref_1.htran[k + j * 3 - 4] = 0.;
		}
		if (minref_1.vtran[k + j * 3 - 4] == null_1.xnullm) {
		    minref_1.vtran[k + j * 3 - 4] = 0.;
		}
		if (minref_1.dpdttr[k + j * 3 - 4] == null_1.xnullm) {
		    minref_1.dpdttr[k + j * 3 - 4] = 0.;
		}
/* L20: */
	    }
	}
/* L10: */
    }
    i__1 = reac2_1.na[*i__ - 1];
    for (j = 1; j <= i__1; ++j) {
	if (aqsref_1.a[(j << 2) - 2] == null_1.xnulla) {
	    *nullrx = TRUE_;
	    for (k = 1; k <= 4; ++k) {
		aqsref_1.a[k + (*i__ << 2) - 5] = 0.;
/* L40: */
	    }
	}
/* L30: */
    }
    return 0;
} /* zero_ */

/* **************************************************************** */
/* ** wrtssp - Write, to tabf, standard molal thermodynamic */
/* **          properties at 25 C and 1 bar, equation-of-state */
/* **          parameters, and heat capacity coefficients for all */
/* **          species in the i[th] reaction. */
/* Subroutine */ int wrtssp_(integer *i__)
{
    /* Initialized data */

    static char wname[20] = "H2O                 ";

    /* Format strings */
    static char fmt_5[] = "(//,\002 STANDARD STATE PROPERTIES OF THE SPECIES"
	    " AT\002,\002 25 DEG C AND 1 BAR\002)";
    static char fmt_6[] = "(//,42x,\002 ...... MINERALS ...... \002,///,24x"
	    ",\002   DELTA G   \002,1x,1x,\002   DELTA H   \002,1x,1x,\002   "
	    "   S      \002,1x,1x,\002      V      \002,1x,1x,\002      Cp   "
	    "  \002,1x,/,7x,\002NAME\002,12x,1x,\002  (cal/mol)  \002,1x,1x"
	    ",\002  (cal/mol)  \002,1x,1x,\002 (cal/mol/K) \002,1x,1x,\002  ("
	    "cc/mol)   \002,1x,1x,\002 (cal/mol/K) \002,/,2x,20(\002-\002),2x"
	    ",13(\002-\002),2x,13(\002-\002),2x,13(\002-\002),2x,13(\002-\002"
	    "),2x,13(\002-\002))";
    static char fmt_14[] = "(2x,a20,30x,5x,f8.3,3x,4x,f8.3,4x,4x,f6.1,6x)";
    static char fmt_15[] = "(2x,a20,3x,f10.0,2x,3x,f10.0,2x,5x,f8.3,3x,4x,f8"
	    ".3,4x,4x,f6.1,6x)";
    static char fmt_7[] = "(//,29x,\002MAIER-KELLY COEFFICIENTS\002,32x,\002"
	    "PHASE TRANSITION DATA\002,/,7x,\002NAME\002,11x,1x,\002  a(10**0"
	    ")  \002,\002  b(10**3)  \002,\002  c(10**-5) \002,\002 T limit ("
	    "C)\002,1x,\002 Htr (cal/mol)\002,1x,1x,\002 Vtr (cc/mol) \002,1x"
	    ",1x,\002dPdTtr (bar/K)\002,1x,/,2x,20(\002-\002),2x,10(\002-\002"
	    "),1x,1x,10(\002-\002),1x,1x,10(\002-\002),1x,1x,11(\002-\002),1x"
	    ",1x,13(\002-\002),1x,1x,14(\002-\002),1x,1x,14(\002-\002),1x)";
    static char fmt_16[] = "(2x,a20,1x,f9.3,2x,1x,f9.3,2x,1x,f9.3,2x,3x,f7.2)"
	    ;
    static char fmt_19[] = "(2x,a20,1x,f9.3,2x,1x,f9.3,2x,1x,f9.3,2x,3x,f7.2"
	    ",3x)";
    static char fmt_119[] = "(2x,a20,1x,f9.3,2x,1x,f9.3,2x,1x,f9.3,2x,3x,f7."
	    "2,3x,5x,f6.0,5x)";
    static char fmt_219[] = "(2x,a20,1x,f9.3,2x,1x,f9.3,2x,1x,f9.3,2x,3x,f7."
	    "2,3x,5x,f6.0,5x,4x,f7.3,5x,4x,f7.3,5x)";
    static char fmt_25[] = "(4x,\002post-transition \002,i1,1x,1x,f9.3,2x,1x"
	    ",f9.3,2x,1x,f9.3,2x,3x,f7.2,3x)";
    static char fmt_125[] = "(4x,\002post-transition \002,i1,1x,1x,f9.3,2x,1"
	    "x,f9.3,2x,1x,f9.3,2x,3x,f7.2,3x,5x,f6.0,5x)";
    static char fmt_225[] = "(4x,\002post-transition \002,i1,1x,1x,f9.3,2x,1"
	    "x,f9.3,2x,1x,f9.3,2x,3x,f7.2,3x,5x,f6.0,5x,4x,f7.3,5x,4x,f7.3,5x)"
	    ;
    static char fmt_39[] = "(4x,\002post-transition \002,i1,1x,1x,f9.3,2x,1x"
	    ",f9.3,2x,1x,f9.3,2x,3x,f7.2)";
    static char fmt_8[] = "(///,42x,\002 ...... GASES ...... \002,///,24x"
	    ",\002   DELTA G   \002,1x,1x,\002   DELTA H   \002,1x,1x,\002   "
	    "   S      \002,1x,1x,\002      V      \002,1x,1x,\002      Cp   "
	    "  \002,1x,/,7x,\002NAME\002,12x,1x,\002  (cal/mol)  \002,1x,1x"
	    ",\002  (cal/mol)  \002,1x,1x,\002 (cal/mol/K) \002,1x,1x,\002   "
	    "(cc/mol)  \002,1x,1x,\002 (cal/mol/K) \002,/,2x,20(\002-\002),2x"
	    ",13(\002-\002),2x,13(\002-\002),2x,13(\002-\002),2x,13(\002-\002"
	    "),2x,13(\002-\002))";
    static char fmt_17[] = "(2x,a20,3x,f10.0,2x,3x,f10.0,2x,5x,f8.3,3x,7x,f2"
	    ".0,7x,4x,f6.1,6x)";
    static char fmt_9[] = "(//,29x,\002MAIER-KELLY COEFFICIENTS\002,/,7x,"
	    "\002NAME\002,11x,1x,\002  a(10**0)  \002,\002  b(10**3)  \002"
	    ",\002  c(10**-5) \002,\002 T limit (C)\002,/,2x,20(\002-\002),2x"
	    ",10(\002-\002),1x,1x,10(\002-\002),1x,1x,10(\002-\002),1x,1x,10"
	    "(\002-\002))";
    static char fmt_46[] = "(///,36x,\002 ...... AQUEOUS SPECIES ...... \002"
	    ",///,24x,\002   DELTA G   \002,1x,1x,\002   DELTA H   \002,1x,1x,"
	    "\002      S      \002,1x,1x,\002      V      \002,1x,1x,\002    "
	    "  Cp     \002,1x,/,7x,\002NAME\002,12x,1x,\002  (cal/mol)  \002,"
	    "1x,1x,\002  (cal/mol)  \002,1x,1x,\002 (cal/mol/K) \002,1x,1x"
	    ",\002   (cc/mol)  \002,1x,1x,\002 (cal/mol/K) \002,/,2x,20(\002"
	    "-\002),2x,13(\002-\002),2x,13(\002-\002),2x,13(\002-\002),2x,13"
	    "(\002-\002),2x,13(\002-\002))";
    static char fmt_79[] = "(2x,a20,3x,f10.0,2x,3x,f10.0,2x,5x,f8.3,3x,5x,f6"
	    ".1,5x,4x,f6.1,6x)";
    static char fmt_56[] = "(//,50x,\002EQUATION-OF-STATE COEFFICIENTS\002,/"
	    ",7x,\002NAME\002,11x,2x,\002 a1(10**1)  \002,1x,1x,\002 a2(10**-"
	    "2) \002,1x,1x,\002 a3(10**0)  \002,1x,1x,\002 a4(10**-4) \002,1x"
	    ",1x,\002 c1(10**0)  \002,1x,1x,\002 c2(10**-4) \002,2x,\002omega"
	    "(10**-5)\002,/,2x,20(\002-\002),2x,12(\002-\002),1x,1x,12(\002"
	    "-\002),1x,1x,12(\002-\002),1x,1x,12(\002-\002),1x,1x,12(\002-"
	    "\002),1x,1x,12(\002-\002),1x,1x,13(\002-\002))";
    static char fmt_64[] = "(2x,a20,1x,56x,3(3x,f8.4,3x))";
    static char fmt_65[] = "(2x,a20,1x,7(2x,f10.4,2x))";
    static char fmt_74[] = "(///,\002 STANDARD STATE PROPERTIES OF THE REACT"
	    "ION AT\002,\002 25 DEG C AND 1 BAR\002,//)";
    static char fmt_888[] = "(\002 CAUTION: INCOMPLETE DATA FOR ONE OR MORE "
	    "SPECIES\002,//)";
    static char fmt_75[] = "(50x,\002 DELTA G  \002,1x,1x,\002 DELTA H  \002"
	    ",1x,1x,\002 DELTA S  \002,1x,1x,\002 DELTA V  \002,1x,\002 DELTA"
	    " Cp \002,1x,/,2x,a10,2x,a10,2x,a10,2x,\002  LOG K   \002,1x,1x"
	    ",\002  (cal)   \002,1x,1x,\002  (cal)   \002,1x,1x,\002 (cal/K)  "
	    "\002,1x,1x,\002   (cc)   \002,1x,1x,\002 (cal/K)  \002,1x,/,3(2x"
	    ",10(\002-\002)),1x,6(1x,10(\002-\002),1x))";
    static char fmt_85[] = "(3(2x,f9.3,1x),1x,f10.3,1x,1x,f10.0,1x,1x,f10.0,"
	    "1x,1x,f9.1,2x,1x,f9.1,2x,1x,f9.1,/)";

    /* System generated locals */
    integer i__1;
    doublereal d__1, d__2, d__3, d__4, d__5, d__6, d__7;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Local variables */
    static integer j, iii, jjj;
    extern /* Subroutine */ int reac92_(integer *, doublereal *, doublereal *,
	     doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, integer *);
    static doublereal a3temp[10], tpdref[4], gftemp[10], hftemp[10];
    static logical nullrx;

    /* Fortran I/O blocks */
    static cilist io___1159 = { 0, 0, 0, fmt_5, 0 };
    static cilist io___1160 = { 0, 0, 0, fmt_6, 0 };
    static cilist io___1162 = { 0, 0, 0, fmt_14, 0 };
    static cilist io___1163 = { 0, 0, 0, fmt_15, 0 };
    static cilist io___1164 = { 0, 0, 0, fmt_7, 0 };
    static cilist io___1165 = { 0, 0, 0, fmt_16, 0 };
    static cilist io___1166 = { 0, 0, 0, fmt_19, 0 };
    static cilist io___1167 = { 0, 0, 0, fmt_119, 0 };
    static cilist io___1168 = { 0, 0, 0, fmt_219, 0 };
    static cilist io___1169 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___1170 = { 0, 0, 0, fmt_125, 0 };
    static cilist io___1171 = { 0, 0, 0, fmt_225, 0 };
    static cilist io___1172 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___1173 = { 0, 0, 0, fmt_125, 0 };
    static cilist io___1174 = { 0, 0, 0, fmt_225, 0 };
    static cilist io___1175 = { 0, 0, 0, fmt_39, 0 };
    static cilist io___1176 = { 0, 0, 0, fmt_39, 0 };
    static cilist io___1177 = { 0, 0, 0, fmt_39, 0 };
    static cilist io___1178 = { 0, 0, 0, fmt_8, 0 };
    static cilist io___1179 = { 0, 0, 0, fmt_17, 0 };
    static cilist io___1180 = { 0, 0, 0, fmt_9, 0 };
    static cilist io___1181 = { 0, 0, 0, fmt_16, 0 };
    static cilist io___1182 = { 0, 0, 0, fmt_46, 0 };
    static cilist io___1183 = { 0, 0, 0, fmt_79, 0 };
    static cilist io___1184 = { 0, 0, 0, fmt_79, 0 };
    static cilist io___1185 = { 0, 0, 0, fmt_56, 0 };
    static cilist io___1186 = { 0, 0, 0, fmt_64, 0 };
    static cilist io___1187 = { 0, 0, 0, fmt_65, 0 };
    static cilist io___1188 = { 0, 0, 0, fmt_74, 0 };
    static cilist io___1189 = { 0, 0, 0, fmt_888, 0 };
    static cilist io___1190 = { 0, 0, 0, fmt_75, 0 };
    static cilist io___1192 = { 0, 0, 0, fmt_85, 0 };


/* **** remove NULL contributions to standard state calculations */
    nullrx = FALSE_;
    i__1 = reac2_1.nm[*i__ - 1];
    for (iii = 1; iii <= i__1; ++iii) {
	gftemp[iii - 1] = 0.;
	if (minref_1.gfmin[iii - 1] == null_1.xnullm) {
	    nullrx = TRUE_;
	    gftemp[iii - 1] = minref_1.gfmin[iii - 1];
	    hftemp[iii - 1] = minref_1.hfmin[iii - 1];
	    minref_1.gfmin[iii - 1] = 0.;
	    minref_1.hfmin[iii - 1] = 0.;
	}
/* L456: */
    }
    i__1 = reac2_1.na[*i__ - 1];
    for (iii = 1; iii <= i__1; ++iii) {
	a3temp[iii - 1] = 0.;
	if (aqsref_1.a[(iii << 2) - 2] == null_1.xnulla) {
	    nullrx = TRUE_;
	    a3temp[iii - 1] = aqsref_1.a[(iii << 2) - 2];
	    for (jjj = 1; jjj <= 4; ++jjj) {
		aqsref_1.a[jjj + (iii << 2) - 5] = 0.;
/* L557: */
	    }
	}
/* L556: */
    }
/* **** calculate all reaction species heat capacities and reactant */
/* **** aqueous species standard partial molal volumes at */
/* **** 25 degC, 1 bar */
    d__1 = refval_1.tref - 273.15;
    reac92_(i__, &refval_1.pref, &d__1, &h2oss_1.dwss, &h2oss_1.vwss, &
	    h2oss_1.bewss, &h2oss_1.alwss, &h2oss_1.dalwss, &h2oss_1.swss, &
	    h2oss_1.cpwss, &h2oss_1.hwss, &h2oss_1.gwss, &h2oss_1.zwss, &
	    h2oss_1.qwss, &h2oss_1.ywss, &h2oss_1.xwss, &icon_1.geqn);
/* **** return NULL contributions to faciltate blanking */
    i__1 = reac2_1.nm[*i__ - 1];
    for (iii = 1; iii <= i__1; ++iii) {
	if (gftemp[iii - 1] == null_1.xnullm) {
	    minref_1.gfmin[iii - 1] = gftemp[iii - 1];
	    minref_1.hfmin[iii - 1] = hftemp[iii - 1];
	}
/* L457: */
    }
    i__1 = reac2_1.na[*i__ - 1];
    for (iii = 1; iii <= i__1; ++iii) {
	if (a3temp[iii - 1] == null_1.xnulla) {
	    aqsref_1.a[(iii << 2) - 2] = a3temp[iii - 1];
	}
/* L656: */
    }
    io___1159.ciunit = io_1.tabf;
    s_wsfe(&io___1159);
    e_wsfe();
    if (reac2_1.nm[*i__ - 1] > 0) {
	io___1160.ciunit = io_1.tabf;
	s_wsfe(&io___1160);
	e_wsfe();
/* **** write mineral G, H, S, V, Cp at 25 C, 1 bar ***** */
	i__1 = reac2_1.nm[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    if (minref_1.gfmin[j - 1] == null_1.xnullm) {
		io___1162.ciunit = io_1.tabf;
		s_wsfe(&io___1162);
		do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)20);
		do_fio(&c__1, (char *)&minref_1.sprtrm[j - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&minref_1.vprtrm[j - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&minsp_1.cpmin[j - 1], (ftnlen)sizeof(
			doublereal));
		e_wsfe();
	    } else {
		io___1163.ciunit = io_1.tabf;
		s_wsfe(&io___1163);
		do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)20);
		do_fio(&c__1, (char *)&minref_1.gfmin[j - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&minref_1.hfmin[j - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&minref_1.sprtrm[j - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&minref_1.vprtrm[j - 1], (ftnlen)sizeof(
			doublereal));
		do_fio(&c__1, (char *)&minsp_1.cpmin[j - 1], (ftnlen)sizeof(
			doublereal));
		e_wsfe();
	    }
/* L10: */
	}
	io___1164.ciunit = io_1.tabf;
	s_wsfe(&io___1164);
	e_wsfe();
	i__1 = reac2_1.nm[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
/* **** write mineral Maier-Kelly heat capacity coefficients */
/* **** a, b, c and phase transition T, H, V, dPdT */
	    if (minref_1.ntran[j - 1] == 0) {
		io___1165.ciunit = io_1.tabf;
		s_wsfe(&io___1165);
		do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)20);
		d__1 = minref_1.mk1[j * 3 - 3] * 1.;
		do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		d__2 = minref_1.mk1[j * 3 - 2] * 1e3;
		do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		d__3 = minref_1.mk1[j * 3 - 1] * 1e-5;
		do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		d__4 = minref_1.tmaxm[j - 1] - 273.15;
		do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
		e_wsfe();
	    } else {
/* **                  following block IFs designed to eliminate */
/* **                  printing of unknown (i.e., zero-valued) */
/* **                  Htran, Vtran, dPdTtr. */
		if (minref_1.htran[j * 3 - 3] == null_1.xnullm) {
		    io___1166.ciunit = io_1.tabf;
		    s_wsfe(&io___1166);
		    do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)20);
		    d__1 = minref_1.mk1[j * 3 - 3] * 1.;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    d__2 = minref_1.mk1[j * 3 - 2] * 1e3;
		    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		    d__3 = minref_1.mk1[j * 3 - 1] * 1e-5;
		    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		    d__4 = minref_1.ttran[j * 3 - 3] - 273.15;
		    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
		    e_wsfe();
		} else {
		    if (minref_1.vtran[j * 3 - 3] == null_1.xnullm) {
			io___1167.ciunit = io_1.tabf;
			s_wsfe(&io___1167);
			do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)
				20);
			d__1 = minref_1.mk1[j * 3 - 3] * 1.;
			do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				doublereal));
			d__2 = minref_1.mk1[j * 3 - 2] * 1e3;
			do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				doublereal));
			d__3 = minref_1.mk1[j * 3 - 1] * 1e-5;
			do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				doublereal));
			d__4 = minref_1.ttran[j * 3 - 3] - 273.15;
			do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				doublereal));
			do_fio(&c__1, (char *)&minref_1.htran[j * 3 - 3], (
				ftnlen)sizeof(doublereal));
			e_wsfe();
		    } else {
			io___1168.ciunit = io_1.tabf;
			s_wsfe(&io___1168);
			do_fio(&c__1, mnames_1.mname + (j - 1) * 20, (ftnlen)
				20);
			d__1 = minref_1.mk1[j * 3 - 3] * 1.;
			do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				doublereal));
			d__2 = minref_1.mk1[j * 3 - 2] * 1e3;
			do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				doublereal));
			d__3 = minref_1.mk1[j * 3 - 1] * 1e-5;
			do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				doublereal));
			d__4 = minref_1.ttran[j * 3 - 3] - 273.15;
			do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				doublereal));
			do_fio(&c__1, (char *)&minref_1.htran[j * 3 - 3], (
				ftnlen)sizeof(doublereal));
			do_fio(&c__1, (char *)&minref_1.vtran[j * 3 - 3], (
				ftnlen)sizeof(doublereal));
			do_fio(&c__1, (char *)&minref_1.dpdttr[j * 3 - 3], (
				ftnlen)sizeof(doublereal));
			e_wsfe();
		    }
		}
		if (minref_1.ntran[j - 1] >= 2) {
		    if (minref_1.htran[j * 3 - 2] == null_1.xnullm) {
			io___1169.ciunit = io_1.tabf;
			s_wsfe(&io___1169);
			do_fio(&c__1, (char *)&c__1, (ftnlen)sizeof(integer));
			d__1 = minref_1.mk2[j * 3 - 3] * 1.;
			do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				doublereal));
			d__2 = minref_1.mk2[j * 3 - 2] * 1e3;
			do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				doublereal));
			d__3 = minref_1.mk2[j * 3 - 1] * 1e-5;
			do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				doublereal));
			d__4 = minref_1.ttran[j * 3 - 2] - 273.15;
			do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				doublereal));
			e_wsfe();
		    } else {
			if (minref_1.vtran[j * 3 - 2] == null_1.xnullm) {
			    io___1170.ciunit = io_1.tabf;
			    s_wsfe(&io___1170);
			    do_fio(&c__1, (char *)&c__1, (ftnlen)sizeof(
				    integer));
			    d__1 = minref_1.mk2[j * 3 - 3] * 1.;
			    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				    doublereal));
			    d__2 = minref_1.mk2[j * 3 - 2] * 1e3;
			    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				    doublereal));
			    d__3 = minref_1.mk2[j * 3 - 1] * 1e-5;
			    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				    doublereal));
			    d__4 = minref_1.ttran[j * 3 - 2] - 273.15;
			    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				    doublereal));
			    do_fio(&c__1, (char *)&minref_1.htran[j * 3 - 2], 
				    (ftnlen)sizeof(doublereal));
			    e_wsfe();
			} else {
			    io___1171.ciunit = io_1.tabf;
			    s_wsfe(&io___1171);
			    do_fio(&c__1, (char *)&c__1, (ftnlen)sizeof(
				    integer));
			    d__1 = minref_1.mk2[j * 3 - 3] * 1.;
			    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				    doublereal));
			    d__2 = minref_1.mk2[j * 3 - 2] * 1e3;
			    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				    doublereal));
			    d__3 = minref_1.mk2[j * 3 - 1] * 1e-5;
			    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				    doublereal));
			    d__4 = minref_1.ttran[j * 3 - 2] - 273.15;
			    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				    doublereal));
			    do_fio(&c__1, (char *)&minref_1.htran[j * 3 - 2], 
				    (ftnlen)sizeof(doublereal));
			    do_fio(&c__1, (char *)&minref_1.vtran[j * 3 - 2], 
				    (ftnlen)sizeof(doublereal));
			    do_fio(&c__1, (char *)&minref_1.dpdttr[j * 3 - 2],
				     (ftnlen)sizeof(doublereal));
			    e_wsfe();
			}
		    }
		}
		if (minref_1.ntran[j - 1] >= 3) {
		    if (minref_1.htran[j * 3 - 1] == null_1.xnullm) {
			io___1172.ciunit = io_1.tabf;
			s_wsfe(&io___1172);
			do_fio(&c__1, (char *)&c__2, (ftnlen)sizeof(integer));
			d__1 = minref_1.mk3[j * 3 - 3] * 1.;
			do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				doublereal));
			d__2 = minref_1.mk3[j * 3 - 2] * 1e3;
			do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				doublereal));
			d__3 = minref_1.mk3[j * 3 - 1] * 1e-5;
			do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				doublereal));
			d__4 = minref_1.ttran[j * 3 - 1] - 273.15;
			do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				doublereal));
			e_wsfe();
		    } else {
			if (minref_1.vtran[j * 3 - 1] == null_1.xnullm) {
			    io___1173.ciunit = io_1.tabf;
			    s_wsfe(&io___1173);
			    do_fio(&c__1, (char *)&c__2, (ftnlen)sizeof(
				    integer));
			    d__1 = minref_1.mk3[j * 3 - 3] * 1.;
			    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				    doublereal));
			    d__2 = minref_1.mk3[j * 3 - 2] * 1e3;
			    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				    doublereal));
			    d__3 = minref_1.mk3[j * 3 - 1] * 1e-5;
			    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				    doublereal));
			    d__4 = minref_1.ttran[j * 3 - 1] - 273.15;
			    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				    doublereal));
			    do_fio(&c__1, (char *)&minref_1.htran[j * 3 - 1], 
				    (ftnlen)sizeof(doublereal));
			    e_wsfe();
			} else {
			    io___1174.ciunit = io_1.tabf;
			    s_wsfe(&io___1174);
			    do_fio(&c__1, (char *)&c__2, (ftnlen)sizeof(
				    integer));
			    d__1 = minref_1.mk3[j * 3 - 3] * 1.;
			    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(
				    doublereal));
			    d__2 = minref_1.mk3[j * 3 - 2] * 1e3;
			    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(
				    doublereal));
			    d__3 = minref_1.mk3[j * 3 - 1] * 1e-5;
			    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(
				    doublereal));
			    d__4 = minref_1.ttran[j * 3 - 1] - 273.15;
			    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(
				    doublereal));
			    do_fio(&c__1, (char *)&minref_1.htran[j * 3 - 1], 
				    (ftnlen)sizeof(doublereal));
			    do_fio(&c__1, (char *)&minref_1.vtran[j * 3 - 1], 
				    (ftnlen)sizeof(doublereal));
			    do_fio(&c__1, (char *)&minref_1.dpdttr[j * 3 - 1],
				     (ftnlen)sizeof(doublereal));
			    e_wsfe();
			}
		    }
		}
		if (minref_1.ntran[j - 1] == 1) {
		    io___1175.ciunit = io_1.tabf;
		    s_wsfe(&io___1175);
		    do_fio(&c__1, (char *)&minref_1.ntran[j - 1], (ftnlen)
			    sizeof(integer));
		    d__1 = minref_1.mk2[j * 3 - 3] * 1.;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    d__2 = minref_1.mk2[j * 3 - 2] * 1e3;
		    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		    d__3 = minref_1.mk2[j * 3 - 1] * 1e-5;
		    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		    d__4 = minref_1.tmaxm[j - 1] - 273.15;
		    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
		    e_wsfe();
		}
		if (minref_1.ntran[j - 1] == 2) {
		    io___1176.ciunit = io_1.tabf;
		    s_wsfe(&io___1176);
		    do_fio(&c__1, (char *)&minref_1.ntran[j - 1], (ftnlen)
			    sizeof(integer));
		    d__1 = minref_1.mk3[j * 3 - 3] * 1.;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    d__2 = minref_1.mk3[j * 3 - 2] * 1e3;
		    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		    d__3 = minref_1.mk3[j * 3 - 1] * 1e-5;
		    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		    d__4 = minref_1.tmaxm[j - 1] - 273.15;
		    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
		    e_wsfe();
		}
		if (minref_1.ntran[j - 1] == 3) {
		    io___1177.ciunit = io_1.tabf;
		    s_wsfe(&io___1177);
		    do_fio(&c__1, (char *)&minref_1.ntran[j - 1], (ftnlen)
			    sizeof(integer));
		    d__1 = minref_1.mk4[j * 3 - 3] * 1.;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    d__2 = minref_1.mk4[j * 3 - 2] * 1e3;
		    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		    d__3 = minref_1.mk4[j * 3 - 1] * 1e-5;
		    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		    d__4 = minref_1.tmaxm[j - 1] - 273.15;
		    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
		    e_wsfe();
		}
	    }
/* L11: */
	}
    }
    if (reac2_1.ng[*i__ - 1] > 0) {
	io___1178.ciunit = io_1.tabf;
	s_wsfe(&io___1178);
	e_wsfe();
/* **** write gas G, H, S, V, Cp at 25 C, 1 bar and Maier-Kelly */
/* **** heat capacity coefficients a, b, c */
	i__1 = reac2_1.ng[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    io___1179.ciunit = io_1.tabf;
	    s_wsfe(&io___1179);
	    do_fio(&c__1, gnames_1.gname + (j - 1) * 20, (ftnlen)20);
	    do_fio(&c__1, (char *)&gasref_1.gfgas[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&gasref_1.hfgas[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&gasref_1.sprtrg[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&gasref_1.vprtrg[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&gassp_1.cpgas[j - 1], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
/* L30: */
	}
	io___1180.ciunit = io_1.tabf;
	s_wsfe(&io___1180);
	e_wsfe();
	i__1 = reac2_1.ng[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    io___1181.ciunit = io_1.tabf;
	    s_wsfe(&io___1181);
	    do_fio(&c__1, gnames_1.gname + (j - 1) * 20, (ftnlen)20);
	    d__1 = gasref_1.mkg[j * 3 - 3] * 1.;
	    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
	    d__2 = gasref_1.mkg[j * 3 - 2] * 1e3;
	    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
	    d__3 = gasref_1.mkg[j * 3 - 1] * 1e-5;
	    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
	    d__4 = gasref_1.tmaxg[j - 1] - 273.15;
	    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
	    e_wsfe();
/* L31: */
	}
    }
    if (reac2_1.na[*i__ - 1] > 0 || reac2_1.nw[*i__ - 1] > 0) {
	io___1182.ciunit = io_1.tabf;
	s_wsfe(&io___1182);
	e_wsfe();
/* **** write aqueous species G, H, S, V, Cp at 25 C, 1 bar */
	i__1 = reac2_1.na[*i__ - 1];
	for (j = 1; j <= i__1; ++j) {
	    io___1183.ciunit = io_1.tabf;
	    s_wsfe(&io___1183);
	    do_fio(&c__1, anames_1.aname + (j - 1) * 20, (ftnlen)20);
	    do_fio(&c__1, (char *)&aqsref_1.gfaqs[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&aqsref_1.hfaqs[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&aqsref_1.sprtra[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&aqsp_1.vaqs[j - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&aqsp_1.cpaqs[j - 1], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
/* L40: */
	}
	if (reac2_1.nw[*i__ - 1] > 0) {
/* **** write H2O G, H, S, V, Cp at 25 C, 1 bar ****** */
	    io___1184.ciunit = io_1.tabf;
	    s_wsfe(&io___1184);
	    do_fio(&c__1, wname, (ftnlen)20);
	    do_fio(&c__1, (char *)&h2oss_1.gwss, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&h2oss_1.hwss, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&h2oss_1.swss, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&h2oss_1.vwss, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&h2oss_1.cpwss, (ftnlen)sizeof(doublereal));
	    e_wsfe();
	}
	if (reac2_1.na[*i__ - 1] > 0) {
	    io___1185.ciunit = io_1.tabf;
	    s_wsfe(&io___1185);
	    e_wsfe();
/* **** write aqueous species equation-of-state parameters */
/* **** wref, c[1..4], a[1..2] */
	    i__1 = reac2_1.na[*i__ - 1];
	    for (j = 1; j <= i__1; ++j) {
		if (aqsref_1.a[(j << 2) - 2] == null_1.xnulla) {
		    io___1186.ciunit = io_1.tabf;
		    s_wsfe(&io___1186);
		    do_fio(&c__1, anames_1.aname + (j - 1) * 20, (ftnlen)20);
		    d__1 = aqsref_1.c__[(j << 1) - 2] * 1.;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    d__2 = aqsref_1.c__[(j << 1) - 1] * 1e-4;
		    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		    d__3 = aqsref_1.wref[j - 1] * 1e-5;
		    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		    e_wsfe();
		} else {
		    io___1187.ciunit = io_1.tabf;
		    s_wsfe(&io___1187);
		    do_fio(&c__1, anames_1.aname + (j - 1) * 20, (ftnlen)20);
		    d__1 = aqsref_1.a[(j << 2) - 4] * 10.;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    d__2 = aqsref_1.a[(j << 2) - 3] * .01;
		    do_fio(&c__1, (char *)&d__2, (ftnlen)sizeof(doublereal));
		    d__3 = aqsref_1.a[(j << 2) - 2] * 1.;
		    do_fio(&c__1, (char *)&d__3, (ftnlen)sizeof(doublereal));
		    d__4 = aqsref_1.a[(j << 2) - 1] * 1e-4;
		    do_fio(&c__1, (char *)&d__4, (ftnlen)sizeof(doublereal));
		    d__5 = aqsref_1.c__[(j << 1) - 2] * 1.;
		    do_fio(&c__1, (char *)&d__5, (ftnlen)sizeof(doublereal));
		    d__6 = aqsref_1.c__[(j << 1) - 1] * 1e-4;
		    do_fio(&c__1, (char *)&d__6, (ftnlen)sizeof(doublereal));
		    d__7 = aqsref_1.wref[j - 1] * 1e-5;
		    do_fio(&c__1, (char *)&d__7, (ftnlen)sizeof(doublereal));
		    e_wsfe();
		}
/* L50: */
	    }
	}
    }
/* **** write reaction properties at 25 C, 1 bar */
    io___1188.ciunit = io_1.tabf;
    s_wsfe(&io___1188);
    e_wsfe();
    if (nullrx) {
	io___1189.ciunit = io_1.tabf;
	s_wsfe(&io___1189);
	e_wsfe();
    }
    io___1190.ciunit = io_1.tabf;
    s_wsfe(&io___1190);
    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (icon_1.iplot << 1) - 3) * 
	    10, (ftnlen)10);
    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) - 3) * 
	    10, (ftnlen)10);
    do_fio(&c__1, headmp_1.var3 + (icon_1.iopt + (icon_1.iplot << 1) - 3) * 
	    10, (ftnlen)10);
    e_wsfe();
    tpdref[0] = refval_1.tref - 273.15;
    tpdref[1] = refval_1.pref;
    tpdref[2] = h2oss_1.dwss;
    tpdref[3] = h2oss_1.dwss;
    io___1192.ciunit = io_1.tabf;
    s_wsfe(&io___1192);
    do_fio(&c__1, (char *)&tpdref[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot 
	    << 1) - 3] - 1], (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&tpdref[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot 
	    << 1) - 3] - 1], (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&tpdref[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot 
	    << 1) - 3] - 1], (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&fmeq_1.logkr, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&fmeq_1.dgr, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&fmeq_1.dhr, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&fmeq_1.dsr, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&fmeq_1.dvr, (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&fmeq_1.dcpr, (ftnlen)sizeof(doublereal));
    e_wsfe();
    return 0;
} /* wrtssp_ */

/* ***************************************************************** */
/* ** pltrxn - Write header information and reaction titles */
/* **          to plot files. */
/* Subroutine */ int pltrxn_(integer *ireac)
{
    /* Initialized data */

    static integer rlen[2] = { 80,249 };
    static char rstamp[3*10] = "R01" "R02" "R03" "R04" "R05" "R06" "R07" 
	    "R08" "R09" "R10";

    /* Format strings */
    static char fmt_40[] = "(//,\002 REACTION \002,i2,/,\002 TITLE: \002,a80)"
	    ;

    /* System generated locals */
    integer i__1, i__2;
    cllist cl__1;

    /* Builtin functions */
    integer f_clos(cllist *);
    /* Subroutine */ int s_copy(char *, char *, ftnlen, ftnlen);
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void), s_wsfe(cilist *), do_fio(integer *, char *, ftnlen),
	     e_wsfe(void);

    /* Local variables */
    static integer i__;
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);
    extern /* Subroutine */ int plttop_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1196 = { 0, 0, 0, 0, 0 };
    static cilist io___1197 = { 0, 0, 0, fmt_40, 0 };


/* ** close files if necessary */
    if (plottr_1.xyplot == 2 && *ireac > 1) {
	i__1 = plottr_1.nplots;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    cl__1.cerr = 0;
	    cl__1.cunit = io_1.plotf[i__ - 1];
	    cl__1.csta = 0;
	    f_clos(&cl__1);
	    i__2 = plottr_1.end;
	    s_copy(fnames_1.namepf + ((i__ - 1) * 20 + i__2), rstamp + (*
		    ireac - 1) * 3, plottr_1.end + 3 - i__2, (ftnlen)3);
/* L10: */
	}
    }
/* ** if necessary, open files and write header */
    if (plottr_1.xyplot == 2 || *ireac == 1) {
	i__1 = plottr_1.nplots;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (openf_(&io_1.wterm, &io_1.plotf[i__ - 1], fnames_1.namepf + (
		    i__ - 1) * 20, &c__2, &c__1, &c__1, &rlen[plottr_1.xyplot 
		    - 1], (ftnlen)20)) {
		plttop_(&i__);
	    } else {
		io___1196.ciunit = io_1.wterm;
		s_wsle(&io___1196);
		do_lio(&c__9, &c__1, " cannot open plot file ", (ftnlen)23);
		do_lio(&c__3, &c__1, (char *)&i__, (ftnlen)sizeof(integer));
		e_wsle();
	    }
/* L20: */
	}
    }
/* ** write reaction number and title */
    i__1 = plottr_1.nplots;
    for (i__ = 1; i__ <= i__1; ++i__) {
	io___1197.ciunit = io_1.plotf[i__ - 1];
	s_wsfe(&io___1197);
	do_fio(&c__1, (char *)&(*ireac), (ftnlen)sizeof(integer));
	do_fio(&c__1, reac1_1.rtitle + (*ireac - 1) * 80, (ftnlen)80);
	e_wsfe();
/* L30: */
    }
    return 0;
} /* pltrxn_ */

/* ********************************************************************* */
/* ** plttop - Write global banner to plot file i. */
/* Subroutine */ int plttop_(integer *i__)
{
    /* Initialized data */

    static char nosave[20] = "file not saved      ";

    /* Format strings */
    static char fmt_20[] = "(/,\002 ***** SUPCRT92: input/output specificati"
	    "ons for\002,\002 this run\002,/)";
    static char fmt_30[] = "(\002            USER-SPECIFIED  CON FILE  conta"
	    "ining \002,/,\002            T-P-D grid & option switches: \002,"
	    "a20,/)";
    static char fmt_40[] = "(\002            USER-SPECIFIED  RXN FILE  conta"
	    "ining \002,/,\002            chemical reactions: \002,a20,/)";
    static char fmt_50[] = "(\002            THERMODYNAMIC DATABASE: \002,a2"
	    "0,/)";
    static char fmt_60[] = "(\002            SUPCRT-GENERATED  TAB FILE  con"
	    "taining \002,/,\002            tabulated reaction properties:"
	    " \002,a20)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Local variables */
    extern /* Subroutine */ int wrtop2_(integer *);

    /* Fortran I/O blocks */
    static cilist io___1199 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___1200 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___1201 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___1202 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___1203 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___1204 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___1205 = { 0, 0, 0, fmt_60, 0 };


    io___1199.ciunit = io_1.plotf[(24 + (0 + (*i__ - 1 << 2)) - 24) / 4];
    s_wsfe(&io___1199);
    e_wsfe();
    if (saveif_1.savecf) {
	io___1200.ciunit = io_1.plotf[*i__ - 1];
	s_wsfe(&io___1200);
	do_fio(&c__1, fnames_1.namecf, (ftnlen)20);
	e_wsfe();
    } else {
	io___1201.ciunit = io_1.plotf[*i__ - 1];
	s_wsfe(&io___1201);
	do_fio(&c__1, nosave, (ftnlen)20);
	e_wsfe();
    }
    if (saveif_1.saverf) {
	io___1202.ciunit = io_1.plotf[*i__ - 1];
	s_wsfe(&io___1202);
	do_fio(&c__1, fnames_1.namerf, (ftnlen)20);
	e_wsfe();
    } else {
	io___1203.ciunit = io_1.plotf[*i__ - 1];
	s_wsfe(&io___1203);
	do_fio(&c__1, nosave, (ftnlen)20);
	e_wsfe();
    }
    io___1204.ciunit = io_1.plotf[*i__ - 1];
    s_wsfe(&io___1204);
    do_fio(&c__1, dapron_1.pfname, (ftnlen)20);
    e_wsfe();
    io___1205.ciunit = io_1.plotf[*i__ - 1];
    s_wsfe(&io___1205);
    do_fio(&c__1, fnames_1.nametf, (ftnlen)20);
    e_wsfe();
    wrtop2_(i__);
    return 0;
} /* plttop_ */

/* ******************************************************************** */
/* ** report - Report computed reaction properties. */
/* Subroutine */ int report_(integer *ireac, integer *iso, integer *inc, 
	doublereal *tpd, doublereal *tpdtrn, logical *rptran, integer *ptrans,
	 doublereal *dvr, doublereal *dsr, doublereal *dcpr, doublereal *dhr, 
	doublereal *dgr, doublereal *logkr, logical *lvdome, logical *h2oerr, 
	logical *kfound)
{
    /* Initialized data */

    static char pt[1*2] = "P" "T";
    static logical mkdone = FALSE_;
    static logical pdone = FALSE_;

    /* Format strings */
    static char fmt_10[] = "()";
    static char fmt_15[] = "(2(2x,f9.3,1x),23x,\002 T-DH2O FALLS WITHIN LIQ-"
	    "VAP T-DH2O DOME \002,/)";
    static char fmt_16[] = "(2(2x,f9.3,1x),14x,\002 *** BEYOND RANGE OF\002"
	    ",\002 APPLICABILITY OF H2O EQUATION OF STATE **\002,/)";
    static char fmt_17[] = "(2x,f9.3,26x,f10.3,4x,\002 LOG K NOT FOUND: \002"
	    ",f10.3,\002 <= \002,a1,\002 <= \002,f10.3,/)";
    static char fmt_30[] = "(3(2x,f9.3,1x),11x,\002 PHASE TRANSITION #\002,i"
	    "1,\002 for \002,a20,/)";
    static char fmt_40[] = "(3(2x,f9.3,1x),2x,\002 *** BEYOND RANGE OF\002"
	    ",\002 APPLICABILITY OF AQUEOUS SPECIES EQNS ***\002,/)";
    static char fmt_45[] = "(3(2x,f9.3,1x),1x,f10.3,1x,1x,f10.0,3x,\002*** D"
	    "ELTA G ONLY (CHARGED AQUEOUS SPECIES) ***\002,/)";
    static char fmt_50[] = "(3(2x,f9.3,1x),2x,\002 *** CAUTION: BEYOND T LIM"
	    "IT\002,\002 OF CP COEFFS FOR A MINERAL OR GAS ***\002,/)";
    static char fmt_55[] = "(3(2x,f9.3,1x),2x,\002 *** CAUTION: BEYOND P LIM"
	    "IT\002,\002 OF APPROXIMATIONS IN MINERAL/GAS EQNS ***\002,/)";
    static char fmt_60[] = "(3(2x,f9.3,1x),1x,f10.3,1x,1x,f10.0,1x,1x,f10.0,"
	    "1x,1x,f9.1,2x,1x,f9.1,2x,\002  TRANSITION\002,/)";
    static char fmt_70[] = "(3(2x,f9.3,1x),1x,f10.3,1x,1x,f10.0,1x,1x,f10.0,"
	    "1x,1x,f9.1,2x,1x,f9.1,2x,1x,f9.1,/)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), do_fio(integer *, char *, ftnlen);

    /* Local variables */
    static logical xcp;
    static integer imin;
    static logical xall;
    static integer itran;
    static logical pwarn, klost;
    extern /* Subroutine */ int pltln1_(doublereal *), blanks_(doublereal *, 
	    doublereal *, doublereal *, logical *, integer *, integer *, 
	    integer *, logical *, logical *, logical *, logical *, logical *);
    static logical mkwarn;
    extern /* Subroutine */ int pltrep_(doublereal *, integer *, integer *, 
	    doublereal *, doublereal *, doublereal *, doublereal *, 
	    doublereal *, doublereal *, logical *, logical *, logical *, 
	    logical *, logical *, logical *);
    static logical xhsvcp;
    static integer iptnum;

    /* Fortran I/O blocks */
    static cilist io___1210 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1211 = { 0, 0, 0, fmt_15, 0 };
    static cilist io___1212 = { 0, 0, 0, fmt_16, 0 };
    static cilist io___1214 = { 0, 0, 0, fmt_17, 0 };
    static cilist io___1218 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___1224 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___1225 = { 0, 0, 0, fmt_45, 0 };
    static cilist io___1226 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___1227 = { 0, 0, 0, fmt_55, 0 };
    static cilist io___1228 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___1229 = { 0, 0, 0, fmt_70, 0 };


    /* Parameter adjustments */
    --ptrans;
    tpdtrn -= 41;
    --tpd;

    /* Function Body */
    if (*inc == 1) {
	mkdone = FALSE_;
	pdone = FALSE_;
	io___1210.ciunit = io_1.tabf;
	s_wsfe(&io___1210);
	e_wsfe();
	if (plottr_1.xyplot == 1) {
	    pltln1_(&tpd[1]);
	}
    }
    if (plottr_1.xyplot == 2 && *iso == 1 && *inc == 1) {
	pltln1_(&tpd[1]);
    }
    if (*lvdome) {
/* ****      T,DH2O location falls within liquid-vapor dome */
	io___1211.ciunit = io_1.tabf;
	s_wsfe(&io___1211);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (*h2oerr) {
/* ****      T-P-d beyond fluid H2O equation of state */
	io___1212.ciunit = io_1.tabf;
	s_wsfe(&io___1212);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    klost = icon_1.univar == 1 && ! (*kfound);
    if (klost) {
	io___1214.ciunit = io_1.tabf;
	s_wsfe(&io___1214);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&grid_1.v2min, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, pt + (2 / icon_1.iplot - 1), (ftnlen)1);
	do_fio(&c__1, (char *)&grid_1.v2max, (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (*rptran) {
	i__1 = reac2_1.nm[*ireac - 1];
	for (imin = 1; imin <= i__1; ++imin) {
	    for (itran = ptrans[imin]; itran >= 1; --itran) {
		if (icon_1.iplot == 2 && icon_1.isat == 0) {
/* **                       isotherms(P,D): phase c -> b -> a */
		    iptnum = minsp_1.phaser[imin - 1];
		} else {
/* **                       iso[bars,chores](T): phase a -> b -> c */
		    iptnum = minsp_1.phaser[imin - 1] - itran;
		}
		io___1218.ciunit = io_1.tabf;
		s_wsfe(&io___1218);
		do_fio(&c__1, (char *)&tpdtrn[imin + (itran + tpdmap_1.mapiso[
			icon_1.iopt + (icon_1.iplot << 1) - 3] * 3) * 10], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&tpdtrn[imin + (itran + tpdmap_1.mapinc[
			icon_1.iopt + (icon_1.iplot << 1) - 3] * 3) * 10], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&tpdtrn[imin + (itran + (tpdmap_1.mapv3[
			icon_1.iopt + (icon_1.iplot << 1) - 3] - icon_1.isat) 
			* 3) * 10], (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&iptnum, (ftnlen)sizeof(integer));
		do_fio(&c__1, mnames_1.mname + (imin - 1) * 20, (ftnlen)20);
		e_wsfe();
/* L25: */
	    }
/* L20: */
	}
    }
/* ****************************************************************** */
/* ** Assignment of "blank" variables below facilitates full */
/* ** reporting of reaction properties beyond certain limits */
/* ** of certain equations when the CALL to SUBR blank is */
/* ** commented-out for the development version. */
    xall = FALSE_;
    xhsvcp = FALSE_;
    xcp = FALSE_;
    mkwarn = FALSE_;
    pwarn = FALSE_;
/* ** SUBR blanks to be called for distribution version; */
/* ** not called for development version */
    if (! (*lvdome || *h2oerr)) {
	blanks_(&tpd[1], &tpd[2], &tpd[3], &reac2_1.m2reac[*ireac - 1], &
		reac2_1.nm[*ireac - 1], &reac2_1.ng[*ireac - 1], &reac2_1.na[*
		ireac - 1], &xall, &xhsvcp, &xcp, &mkwarn, &pwarn);
    }
/* **** write reaction properties to plot files */
    if (plottr_1.xyplot > 0) {
	pltrep_(&tpd[1], iso, inc, logkr, dgr, dhr, dsr, dcpr, dvr, lvdome, 
		h2oerr, &klost, &xall, &xhsvcp, &xcp);
    }
    if (*lvdome || *h2oerr || klost) {
	return 0;
    }
/* ****************************************************************** */
/* **** write reaction properties to tab file */
    if (xall) {
/* **** P-T beyond validity limits of */
/* **** aqueous species equations */
	io___1224.ciunit = io_1.tabf;
	s_wsfe(&io___1224);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot 
		<< 1) - 3] - icon_1.isat], (ftnlen)sizeof(doublereal));
	e_wsfe();
	return 0;
    }
    if (xhsvcp) {
/* **** P-T within region where only Gibbs free energy can */
/* **** be computed for charged aqueous species */
	io___1225.ciunit = io_1.tabf;
	s_wsfe(&io___1225);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot 
		<< 1) - 3] - icon_1.isat], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	e_wsfe();
	return 0;
    }
    if (mkwarn && ! mkdone) {
/* **** beyond temperature limit of Maier-Kelly Cp coefficients; */
/* **** issue warning to this effect. */
	mkdone = TRUE_;
	io___1226.ciunit = io_1.tabf;
	s_wsfe(&io___1226);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot 
		<< 1) - 3] - icon_1.isat], (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (pwarn && ! pdone) {
/* **** beyond qualitative pressure limit of mineral/gas calculations; */
/* **** issue warning to this effect. */
	pdone = TRUE_;
	io___1227.ciunit = io_1.tabf;
	s_wsfe(&io___1227);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot 
		<< 1) - 3] - icon_1.isat], (ftnlen)sizeof(doublereal));
	e_wsfe();
    }
    if (xcp) {
/* **** within +/- 25 degC of mineral phase transition; */
/* **** report all property values except dCpr */
	io___1228.ciunit = io_1.tabf;
	s_wsfe(&io___1228);
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot 
		<< 1) - 3] - icon_1.isat], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*dhr), (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*dsr), (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&(*dvr), (ftnlen)sizeof(doublereal));
	e_wsfe();
	return 0;
    }
/* **** .NOT. (xall .OR. xHSVCp .OR. xCp); */
/* **** report all property values */
    io___1229.ciunit = io_1.tabf;
    s_wsfe(&io___1229);
    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (icon_1.iplot << 
	    1) - 3]], (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 
	    1) - 3]], (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (icon_1.iplot << 
	    1) - 3] - icon_1.isat], (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&(*dhr), (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&(*dsr), (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&(*dvr), (ftnlen)sizeof(doublereal));
    do_fio(&c__1, (char *)&(*dcpr), (ftnlen)sizeof(doublereal));
    e_wsfe();
    return 0;
} /* report_ */

/* ****************************************************************** */
/* ** pltln1 - Write top line of next property block. */
/* Subroutine */ int pltln1_(doublereal *tpd)
{
    /* Initialized data */

    static char pprop[13*8] = "logK         " "delG (cal)   " "delH (cal)   " 
	    "delS (cal/K) " "delCp (cal/K)" "delV (cc)    " "dvar1        " 
	    "dvar2        ";
    static char isov2[7*2*3] = "D(g/cc)" "P(bars)" "T(degC)" "T(degC)" "T(de"
	    "gC)" "P(bars)";
    static char incv2[7*2*3] = "T(degC)" "T(degC)" "D(g/cc)" "P(bars)" "P(ba"
	    "rs)" "T(degC)";
    static char var32[7*2*3] = "P(bars)" "D(g/cc)" "P(bars)" "D(g/cc)" "D(g/"
	    "cc)" "D(g/cc)";

    /* Format strings */
    static char fmt_20[] = "(/,1x,a10,\002 = \002,f10.4,\002 ; \002,a10,\002"
	    ", \002,a10,/)";
    static char fmt_25[] = "(/,1x,a10,\002 = \002,f10.4,\002 ; \002,a10,\002"
	    ", \002,a13,/)";
    static char fmt_30[] = "(/,1x,a10,\002, \002,a13,/)";
    static char fmt_35[] = "(/,1x,a10,\002, \002,a10,/)";
    static char fmt_50[] = "(/,3(a7,2x),6(a13,2x))";
    static char fmt_60[] = "(/,1x,a1,\002-\002,a1,\002 grid: \002,a13)";
    static char fmt_65[] = "(/,1x,a1,\002-\002,a1,\002 grid: \002,a7)";
    static char fmt_70[] = "(/,a7,11(2x,a7,\002 =\002,e11.4))";
    static char fmt_80[] = "(/,1x,a1,\002-logK grid: \002,a7)";
    static char fmt_90[] = "(/,a7,11(2x,a1,\002(logK =\002,e11.4,\002)\002))";

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__, j, k;

    /* Fortran I/O blocks */
    static cilist io___1235 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___1236 = { 0, 0, 0, fmt_25, 0 };
    static cilist io___1237 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___1238 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1239 = { 0, 0, 0, fmt_35, 0 };
    static cilist io___1240 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___1242 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___1243 = { 0, 0, 0, fmt_65, 0 };
    static cilist io___1244 = { 0, 0, 0, fmt_70, 0 };
    static cilist io___1245 = { 0, 0, 0, fmt_80, 0 };
    static cilist io___1246 = { 0, 0, 0, fmt_90, 0 };


    /* Parameter adjustments */
    --tpd;

    /* Function Body */
    if (plottr_1.xyplot == 1) {
	if (icon_1.univar == 1) {
	    return 0;
	}
	i__1 = plottr_1.nplots;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (icon_1.isat == 0 && ! eq36_1.eq3run) {
		if (i__ == 7) {
		    io___1235.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1235);
		    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
			    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(
			    doublereal));
		    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, headmp_1.var3 + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    e_wsfe();
		} else {
		    io___1236.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1236);
		    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
			    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(
			    doublereal));
		    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, pprop + (i__ - 1) * 13, (ftnlen)13);
		    e_wsfe();
		}
	    } else {
		if (i__ < 7) {
		    io___1237.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1237);
		    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, pprop + (i__ - 1) * 13, (ftnlen)13);
		    e_wsfe();
		}
		if (i__ == 7) {
		    io___1238.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1238);
		    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    e_wsfe();
		}
		if (i__ == 8) {
		    io___1239.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1239);
		    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    do_fio(&c__1, headmp_1.var3 + (icon_1.iopt + (
			    icon_1.iplot << 1) - 3) * 10, (ftnlen)10);
		    e_wsfe();
		}
	    }
/* L10: */
	}
    } else {
	i__1 = plottr_1.nplots;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (icon_1.isat == 1 || eq36_1.eq3run) {
		io___1240.ciunit = io_1.plotf[i__ - 1];
		s_wsfe(&io___1240);
		do_fio(&c__1, isov2 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)7);
		do_fio(&c__1, incv2 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)7);
		do_fio(&c__1, var32 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)7);
		for (j = 1; j <= 6; ++j) {
		    do_fio(&c__1, pprop + (j - 1) * 13, (ftnlen)13);
		}
		e_wsfe();
		goto L40;
	    }
	    if (icon_1.univar == 0) {
		if (i__ < 7) {
		    io___1242.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1242);
		    do_fio(&c__1, isov2 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)1);
		    do_fio(&c__1, incv2 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)1);
		    do_fio(&c__1, pprop + (i__ - 1) * 13, (ftnlen)13);
		    e_wsfe();
		} else {
		    io___1243.ciunit = io_1.plotf[i__ - 1];
		    s_wsfe(&io___1243);
		    do_fio(&c__1, isov2 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)1);
		    do_fio(&c__1, incv2 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)1);
		    do_fio(&c__1, var32 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)7);
		    e_wsfe();
		}
		io___1244.ciunit = io_1.plotf[i__ - 1];
		s_wsfe(&io___1244);
		do_fio(&c__1, incv2 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)7);
		i__2 = min(grid_1.niso,11);
		for (j = 1; j <= i__2; ++j) {
		    do_fio(&c__1, isov2 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)7);
		    d__1 = grid_1.isomin + (j - 1) * grid_1.isoinc;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		}
		e_wsfe();
/* **                  MXIPLT = 11 */
	    } else {
		io___1245.ciunit = io_1.plotf[i__ - 1];
		s_wsfe(&io___1245);
		do_fio(&c__1, isov2 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)1);
		do_fio(&c__1, incv2 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)7);
		e_wsfe();
		io___1246.ciunit = io_1.plotf[i__ - 1];
		s_wsfe(&io___1246);
		do_fio(&c__1, isov2 + (icon_1.iopt + (icon_1.iplot << 1) - 3) 
			* 7, (ftnlen)7);
		i__2 = min(grid_1.nlogk,11);
		for (k = 1; k <= i__2; ++k) {
		    do_fio(&c__1, incv2 + (icon_1.iopt + (icon_1.iplot << 1) 
			    - 3) * 7, (ftnlen)1);
		    d__1 = grid_1.kmin + (k - 1) * grid_1.kinc;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		}
		e_wsfe();
/* **                  MXIPLT = 11 */
	    }
L40:
	    ;
	}
/* ** initialize property buffer */
	i__1 = plottr_1.nplots;
	for (k = 1; k <= i__1; ++k) {
	    i__2 = min(grid_1.niso,11);
	    for (j = 1; j <= i__2; ++j) {
		i__3 = grid_1.nv2;
		for (i__ = 1; i__ <= i__3; ++i__) {
		    pbuffr_1.pbuff[i__ + (j + k * 11) * 21 - 253] = 0.;
/* L100: */
		}
	    }
	}
    }
    return 0;
} /* pltln1_ */

/* ********************************************************************** */
/* ** pltrep - Report calculated property values to plot files or buffers */
/* Subroutine */ int pltrep_(doublereal *tpd, integer *iso, integer *inc, 
	doublereal *logkr, doublereal *dgr, doublereal *dhr, doublereal *dsr, 
	doublereal *dcpr, doublereal *dvr, logical *lvdome, logical *h2oerr, 
	logical *klost, logical *xall, logical *xhsvcp, logical *xcp)
{
    /* Format strings */
    static char fmt_10[] = "(2(1x,e14.7))";
    static char fmt_22[] = "(/,\002 LOG K = \002,e12.5,\002 ; \002,a10,"
	    "\002, \002,a10,\002 = \002,/)";
    static char fmt_40[] = "(9(e14.7,2x))";
    static char fmt_70[] = "(12(e14.7,2x))";
    static char fmt_90[] = "(12(e14.7,2x))";

    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1;

    /* Builtin functions */
    integer s_wsfe(cilist *), do_fio(integer *, char *, ftnlen), e_wsfe(void);

    /* Local variables */
    static integer i__, j, k;

    /* Fortran I/O blocks */
    static cilist io___1248 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1249 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1250 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1251 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1252 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1253 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1254 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1255 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1256 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1257 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1258 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1259 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1260 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1261 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1262 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1263 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1264 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1265 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1266 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1267 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1268 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1269 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1270 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1271 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1272 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1273 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1274 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1275 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1276 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1277 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1278 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1279 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1280 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1281 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1282 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1284 = { 0, 0, 0, fmt_22, 0 };
    static cilist io___1286 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___1287 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___1289 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___1290 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___1291 = { 0, 0, 0, fmt_70, 0 };
    static cilist io___1292 = { 0, 0, 0, fmt_90, 0 };


/* **************************  xyplot = 1  ******************************* */
    /* Parameter adjustments */
    --tpd;

    /* Function Body */
    if (plottr_1.xyplot == 1 && (*lvdome || *h2oerr || *xall)) {
	return 0;
    }
    if (plottr_1.xyplot == 1 && icon_1.univar == 1) {
	if (*klost) {
	    pbuffr_1.pbuff[*inc + (*iso + 11) * 21 - 253] = 0.;
	} else {
	    pbuffr_1.pbuff[*inc + (*iso + 11) * 21 - 253] = tpd[
		    tpdmap_1.mapinc[icon_1.iopt + (icon_1.iplot << 1) - 3]];
	}
	goto L20;
    }
    if (plottr_1.xyplot == 1 && *xhsvcp) {
	if (icon_1.isat == 1 || eq36_1.eq3run) {
	    io___1248.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1248);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1249.ciunit = io_1.plotf[1];
	    s_wsfe(&io___1249);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1250.ciunit = io_1.plotf[6];
	    s_wsfe(&io___1250);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1251.ciunit = io_1.plotf[7];
	    s_wsfe(&io___1251);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (
		    icon_1.iplot << 1) - 3] - icon_1.isat], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	}
	if (icon_1.univar == 0) {
	    io___1252.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1252);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1253.ciunit = io_1.plotf[1];
	    s_wsfe(&io___1253);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1254.ciunit = io_1.plotf[6];
	    s_wsfe(&io___1254);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (
		    icon_1.iplot << 1) - 3] - icon_1.isat], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	}
	return 0;
    }
    if (plottr_1.xyplot == 1 && *xcp) {
	if (icon_1.isat == 1 || eq36_1.eq3run) {
	    io___1255.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1255);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1256.ciunit = io_1.plotf[1];
	    s_wsfe(&io___1256);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1257.ciunit = io_1.plotf[2];
	    s_wsfe(&io___1257);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dhr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1258.ciunit = io_1.plotf[3];
	    s_wsfe(&io___1258);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dsr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1259.ciunit = io_1.plotf[5];
	    s_wsfe(&io___1259);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dvr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1260.ciunit = io_1.plotf[6];
	    s_wsfe(&io___1260);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1261.ciunit = io_1.plotf[7];
	    s_wsfe(&io___1261);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (
		    icon_1.iplot << 1) - 3] - icon_1.isat], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	    return 0;
	}
	if (icon_1.univar == 0) {
	    io___1262.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1262);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1263.ciunit = io_1.plotf[1];
	    s_wsfe(&io___1263);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1264.ciunit = io_1.plotf[2];
	    s_wsfe(&io___1264);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dhr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1265.ciunit = io_1.plotf[3];
	    s_wsfe(&io___1265);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dsr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1266.ciunit = io_1.plotf[5];
	    s_wsfe(&io___1266);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dvr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1267.ciunit = io_1.plotf[6];
	    s_wsfe(&io___1267);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (
		    icon_1.iplot << 1) - 3] - icon_1.isat], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	}
	return 0;
    }
    if (plottr_1.xyplot == 1) {
/* **        .NOT. (xall .OR. xHSVCp .OR. xCp) */
	if (icon_1.isat == 1 || eq36_1.eq3run) {
	    io___1268.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1268);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1269.ciunit = io_1.plotf[1];
	    s_wsfe(&io___1269);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1270.ciunit = io_1.plotf[2];
	    s_wsfe(&io___1270);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dhr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1271.ciunit = io_1.plotf[3];
	    s_wsfe(&io___1271);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dsr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1272.ciunit = io_1.plotf[4];
	    s_wsfe(&io___1272);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dcpr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1273.ciunit = io_1.plotf[5];
	    s_wsfe(&io___1273);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dvr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1274.ciunit = io_1.plotf[6];
	    s_wsfe(&io___1274);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1275.ciunit = io_1.plotf[7];
	    s_wsfe(&io___1275);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapiso[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (
		    icon_1.iplot << 1) - 3] - icon_1.isat], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	}
	if (icon_1.isat == 0 && ! eq36_1.eq3run) {
	    io___1276.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1276);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*logkr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1277.ciunit = io_1.plotf[1];
	    s_wsfe(&io___1277);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dgr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1278.ciunit = io_1.plotf[2];
	    s_wsfe(&io___1278);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dhr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1279.ciunit = io_1.plotf[3];
	    s_wsfe(&io___1279);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dsr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1280.ciunit = io_1.plotf[4];
	    s_wsfe(&io___1280);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dcpr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1281.ciunit = io_1.plotf[5];
	    s_wsfe(&io___1281);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&(*dvr), (ftnlen)sizeof(doublereal));
	    e_wsfe();
	    io___1282.ciunit = io_1.plotf[6];
	    s_wsfe(&io___1282);
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapinc[icon_1.iopt + (
		    icon_1.iplot << 1) - 3]], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tpd[tpdmap_1.mapv3[icon_1.iopt + (
		    icon_1.iplot << 1) - 3] - icon_1.isat], (ftnlen)sizeof(
		    doublereal));
	    e_wsfe();
	}
	return 0;
    }
/* **************************  xyplot = 2  ******************************* */
    if (*lvdome || *h2oerr || *klost || *xall) {
	goto L20;
    }
    if (icon_1.univar == 1) {
	pbuffr_1.pbuff[*inc + (*iso + 11) * 21 - 253] = tpd[tpdmap_1.mapinc[
		icon_1.iopt + (icon_1.iplot << 1) - 3]];
	goto L20;
    }
    if (*xhsvcp) {
	pbuffr_1.pbuff[*inc + (*iso + 11) * 21 - 253] = *logkr;
	pbuffr_1.pbuff[*inc + (*iso + 22) * 21 - 253] = *dgr;
	pbuffr_1.pbuff[*inc + (*iso + 77) * 21 - 253] = tpd[tpdmap_1.mapv3[
		icon_1.iopt + (icon_1.iplot << 1) - 3] - icon_1.isat];
	pbuffr_1.pbuff[*inc + (*iso + 88) * 21 - 253] = tpd[tpdmap_1.mapinc[
		icon_1.iopt + (icon_1.iplot << 1) - 3]];
	goto L20;
    }
    if (*xcp) {
	pbuffr_1.pbuff[*inc + (*iso + 11) * 21 - 253] = *logkr;
	pbuffr_1.pbuff[*inc + (*iso + 22) * 21 - 253] = *dgr;
	pbuffr_1.pbuff[*inc + (*iso + 33) * 21 - 253] = *dhr;
	pbuffr_1.pbuff[*inc + (*iso + 44) * 21 - 253] = *dsr;
	pbuffr_1.pbuff[*inc + (*iso + 66) * 21 - 253] = *dvr;
	pbuffr_1.pbuff[*inc + (*iso + 77) * 21 - 253] = tpd[tpdmap_1.mapv3[
		icon_1.iopt + (icon_1.iplot << 1) - 3] - icon_1.isat];
	pbuffr_1.pbuff[*inc + (*iso + 88) * 21 - 253] = tpd[tpdmap_1.mapinc[
		icon_1.iopt + (icon_1.iplot << 1) - 3]];
    } else {
	pbuffr_1.pbuff[*inc + (*iso + 11) * 21 - 253] = *logkr;
	pbuffr_1.pbuff[*inc + (*iso + 22) * 21 - 253] = *dgr;
	pbuffr_1.pbuff[*inc + (*iso + 33) * 21 - 253] = *dhr;
	pbuffr_1.pbuff[*inc + (*iso + 44) * 21 - 253] = *dsr;
	pbuffr_1.pbuff[*inc + (*iso + 55) * 21 - 253] = *dcpr;
	pbuffr_1.pbuff[*inc + (*iso + 66) * 21 - 253] = *dvr;
	pbuffr_1.pbuff[*inc + (*iso + 77) * 21 - 253] = tpd[tpdmap_1.mapv3[
		icon_1.iopt + (icon_1.iplot << 1) - 3] - icon_1.isat];
	pbuffr_1.pbuff[*inc + (*iso + 88) * 21 - 253] = tpd[tpdmap_1.mapinc[
		icon_1.iopt + (icon_1.iplot << 1) - 3]];
    }
L20:
    if (*iso < grid_1.niso || *inc < grid_1.nv2 || eq36_1.eq3run && *inc < 
	    icon_1.noninc) {
	return 0;
    }
/* ** flush buffers */
    if (plottr_1.xyplot == 1) {
	i__1 = grid_1.nlogk;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    io___1284.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1284);
	    d__1 = grid_1.kmin + (i__ - 1) * grid_1.kinc;
	    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, headmp_1.isov + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    do_fio(&c__1, headmp_1.incv + (icon_1.iopt + (icon_1.iplot << 1) 
		    - 3) * 10, (ftnlen)10);
	    e_wsfe();
	    i__2 = grid_1.niso;
	    for (j = 1; j <= i__2; ++j) {
		if (pbuffr_1.pbuff[i__ + (j + 11) * 21 - 253] != 0.) {
		    io___1286.ciunit = io_1.plotf[0];
		    s_wsfe(&io___1286);
		    d__1 = grid_1.isomin + (j - 1) * grid_1.isoinc;
		    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		    do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + (j + 11) * 21 
			    - 253], (ftnlen)sizeof(doublereal));
		    e_wsfe();
		}
/* L25: */
	    }
	}
	return 0;
    }
    if (eq36_1.eq3run) {
	i__2 = icon_1.noninc;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    io___1287.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1287);
	    do_fio(&c__1, (char *)&grid_1.oddv1[i__ - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&grid_1.oddv2[i__ - 1], (ftnlen)sizeof(
		    doublereal));
	    do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + 1385], (ftnlen)sizeof(
		    doublereal));
	    for (k = 1; k <= 6; ++k) {
		do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + (k * 11 + 1) * 21 
			- 253], (ftnlen)sizeof(doublereal));
	    }
	    e_wsfe();
/* L30: */
	}
	return 0;
    }
    if (icon_1.isat == 1) {
	i__2 = grid_1.nv2;
	for (i__ = 1; i__ <= i__2; ++i__) {
	    if (i__ == 1 && grid_1.v2min == 0.) {
		io___1289.ciunit = io_1.plotf[0];
		s_wsfe(&io___1289);
		do_fio(&c__1, (char *)&satend_1.satmin[icon_1.iopt - 1], (
			ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + 1616], (ftnlen)
			sizeof(doublereal));
		do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + 1385], (ftnlen)
			sizeof(doublereal));
		for (k = 1; k <= 6; ++k) {
		    do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + (k * 11 + 1) *
			     21 - 253], (ftnlen)sizeof(doublereal));
		}
		e_wsfe();
	    } else {
		io___1290.ciunit = io_1.plotf[0];
		s_wsfe(&io___1290);
		d__1 = grid_1.v2min + (i__ - 1) * grid_1.v2inc;
		do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + 1616], (ftnlen)
			sizeof(doublereal));
		do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + 1385], (ftnlen)
			sizeof(doublereal));
		for (k = 1; k <= 6; ++k) {
		    do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + (k * 11 + 1) *
			     21 - 253], (ftnlen)sizeof(doublereal));
		}
		e_wsfe();
	    }
/* L50: */
	}
	return 0;
    }
    if (icon_1.univar == 0) {
	i__2 = plottr_1.nplots;
	for (k = 1; k <= i__2; ++k) {
	    i__1 = grid_1.nv2;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		io___1291.ciunit = io_1.plotf[k - 1];
		s_wsfe(&io___1291);
		d__1 = grid_1.v2min + (i__ - 1) * grid_1.v2inc;
		do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
		i__3 = min(grid_1.niso,11);
		for (j = 1; j <= i__3; ++j) {
		    do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + (j + k * 11) *
			     21 - 253], (ftnlen)sizeof(doublereal));
		}
		e_wsfe();
/* L60: */
	    }
	}
    } else {
	i__1 = grid_1.niso;
	for (j = 1; j <= i__1; ++j) {
	    io___1292.ciunit = io_1.plotf[0];
	    s_wsfe(&io___1292);
	    d__1 = grid_1.isomin + (j - 1) * grid_1.isoinc;
	    do_fio(&c__1, (char *)&d__1, (ftnlen)sizeof(doublereal));
	    i__2 = min(grid_1.nlogk,11);
	    for (i__ = 1; i__ <= i__2; ++i__) {
		do_fio(&c__1, (char *)&pbuffr_1.pbuff[i__ + (j + 11) * 21 - 
			253], (ftnlen)sizeof(doublereal));
	    }
	    e_wsfe();
/* L80: */
	}
    }
    return 0;
} /* pltrep_ */

/* ********************************************************************** */
/* ** blanks - Set xall, xCp, and MKwarn per current state conditions. */
/* Subroutine */ int blanks_(doublereal *t, doublereal *p, doublereal *d__, 
	logical *m2reac, integer *nm, integer *ng, integer *na, logical *xall,
	 logical *xhsvcp, logical *xcp, logical *mkwarn, logical *pwarn)
{
    /* System generated locals */
    integer i__1, i__2, i__3;
    doublereal d__1, d__2;

    /* Local variables */
    static integer i__, j;
    static doublereal tk;
    static logical aqschg;

    *xall = FALSE_;
    *xhsvcp = FALSE_;
    *xcp = FALSE_;
    *mkwarn = FALSE_;
    *pwarn = FALSE_;
    aqschg = FALSE_;
    if (*nm > 0 || *ng > 0) {
/* **** consider validity limitations of mineral/gas equations */
	if (*p > 10000.00001) {
/* ****      P exceeds pressure limit; issue warning */
	    *pwarn = TRUE_;
	}
	tk = *t + 273.15;
	i__1 = *ng;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (tk > gasref_1.tmaxg[i__ - 1]) {
/* ****           T beyond limit of Maier-Kelly coefficients; */
/* ****           issue appropriate warning in calling routine */
		*mkwarn = TRUE_;
	    }
/* L10: */
	}
	i__1 = *nm;
	for (i__ = 1; i__ <= i__1; ++i__) {
	    if (tk > minref_1.tmaxm[i__ - 1]) {
/* ****           T beyond limit of Maier-Kelly coefficients; */
/* ****           issue appropriate warning in calling routine */
		*mkwarn = TRUE_;
	    }
/* L11: */
	}
	if (*m2reac) {
	    i__1 = *nm;
	    for (i__ = 1; i__ <= i__1; ++i__) {
		if (minref_1.ntran[i__ - 1] > 0) {
/* ****                blank-out Cp if T within +/- 25 degC of */
/* ****                phase transition */
		    if (minsp_1.phaser[i__ - 1] == 1) {
			*xcp = (d__1 = tk - pttran_1.ttranp[minsp_1.phaser[
				i__ - 1] + i__ * 3 - 4], abs(d__1)) <= 25.;
		    } else {
/* Computing MIN */
			i__2 = minsp_1.phaser[i__ - 1], i__3 = minref_1.ntran[
				i__ - 1];
			*xcp = (d__1 = tk - pttran_1.ttranp[minsp_1.phaser[
				i__ - 1] - 1 + i__ * 3 - 4], abs(d__1)) <= 
				25. || (d__2 = tk - pttran_1.ttranp[min(i__2,
				i__3) + i__ * 3 - 4], abs(d__2)) <= 25.;
		    }
		    if (*xcp) {
			goto L25;
		    }
		}
/* L20: */
	    }
	}
    }
L25:
    if (*na > 0) {
/* **** consider validity limitations of aqueous species equations */
	if (*p > 5000.0000099999997 || *t > 1000.00001) {
/* ****      P or T exceeds validity limits of equations */
	    *xall = TRUE_;
	    return 0;
	}
	i__1 = *na;
	for (j = 1; j <= i__1; ++j) {
	    if (aqsref_1.chg[j - 1] != 0.) {
		aqschg = TRUE_;
	    }
/* L30: */
	}
	if (aqschg) {
/* ****      check limits for charged aqueous species */
	    *xall = *d__ < .35 || *p < 500. && *t > 350. && *t < 400.;
	    *xhsvcp = *xall || *p < 1e3 && *t > 350.;
	} else {
/* ****      check limits for neutral aqueous species */
	    *xall = *d__ < .05;
	}
    }
    return 0;
} /* blanks_ */

/* Main program alias */ int supcrt_ () { MAIN__ (); return 0; }
