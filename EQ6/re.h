c-----------------------------------------------------------------------        
c re.h                                                                          
      common /re/ mwtrc(nrctpa),vreac(nrctpa),morr(nrctpa),                     
     $ modr(nrctpa),moor(nrctpa),morrs(nrctpa),modrs(nrctpa),                   
     $ zircts(nrctpa),zirct(nrctpa),rxbar(iktpar,nxrtpa),                       
     $ rxbarb(iktpar,nxrtpa),cesrb(nctpar,nsrtpa),cesr(nctpar,nsrtpa),          
     $ jreac(nrctpa),jcode(nrctpa),nrndex(nrctpa),nxridx(nxrtpa)                
c                                                                               
      character*24 ureac,uendb                                                  
      common /rec/ ureac(nrctpa),uendb(iktpar,nxrtpa),                          
     $  uesrb(nctpar,nsrtpa)                                                    
