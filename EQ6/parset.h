c-----------------------------------------------------------------------        
c parset.h  last modified  12/01/87 by tjw                                      
c                                                                               
c     nctpar = nctmax = maximum number of elements                              
c     nsqpar = nsqmax = maximum number of expanded basis species                
c     nstpar = nstmax = maximum number of aqueous species                       
c     nrstpa = nrstmx = maximum number of intra-aqueous reactions               
c                       = nstmax-nctmax-1                                       
c     nmtpar = nmtmax = maximum number of pure minerals                         
c     nxtpar = nxtmax = maximum number of solid solutions                       
c     iktpar = iktmax = maximum number of end-member components in a            
c                       solid solution                                          
c     narxpa = narxmx = maximum number of coefficients in a fitting             
c                       polynomial describing the temperature dependence        
c                       of a thermodynamic property                             
c     ntprpa = ntprmx = maximum number of temperature ranges for                
c                       polynomial coefficients describing a                    
c                       thermodynamic property                                  
c     iapxpa = iapxmx = leading dimension of the apx array,                     
c                       solid solution non-ideality parameters                  
c     nsspar = nssmax = number of solid solutions times number of               
c                       endmembers                                              
c     ngtpar = ngtmax = maximum number of gas species                           
c     nordpa = nordmx = maximum order of taylor's series                        
c     kpar   =  kmax  = maximum number of principal variables used in the       
c                       mass transfer calculations                              
c     nrctpa = nrctmx = maximum number of specified reactant species            
c     nsrtpa = nsrtmx = maximum number of input special reactant species        
c                       (don't confuse this with nrstmx)                        
c     imchpa = imchmx = maximum number of parallel mechanisms for an            
c                       irreversible reaction                                   
c     ndctpa = ndctmx = maximum number of species whose activities can          
c                       appear in a transition-state theory rate law term       
c     nprspa =  nprsmx = maximum number of species in the physically            
c                       removed subsystem that can be read on the               
c                       input file                                              
c     nxoppa = nxopmx = the maximum number of subset-selection options for      
c                       suppressing minerals                                    
c     nxpepa = nxpamx = the maximum number of specified exceptions to           
c                       the mineral subset-selection suppression options        
c     nxmdpa = nxmdmx = maximum number of species/reactions that can be         
c                       affected by the suppress/alter options                  
c                                                                               
      parameter(iktpar=20,imchpa=4,kpar=90)                                     
      parameter(nctpar=70,ndctpa=4,nffgpa=15)                                   
      parameter(ngtpar=15,nmtpar=750,nordpa=6)                                  
      parameter(nprspa=50,nrctpa=25,nsqpar=100)                                 
      parameter(nsrtpa=5,nstpar=750,nxmdpa=40)                                  
      parameter(nxoppa=10,nxpepa=15,nxrtpa=12)                                  
      parameter(nxtpar=20,iapxpa=12,narxpa=5,ntprpa=2)                          
c                                                                               
      parameter(nctpa1=nctpar+1,nsqpa1=nsqpar+1,nordp1=nordpa+1)                
      parameter(nrstpa=nstpar-nctpar-1)                                         
      parameter(nsspar=nxtpar*iktpar)                                           
