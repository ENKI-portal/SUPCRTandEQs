c-----------------------------------------------------------------------        
c hh.h                                                                          
c                                                                               
c       tzero = last temp for which tstep was called                            
c       told = temp (c) at previous point of reaction prograess                 
c       tempc = temp (c)                                                        
c       tempk = temp (k)                                                        
c       press = pressure, bars                                                  
c       tempc = temperature (c)                                                 
c       tempc0 = temp (c) at zi1 = 0 or time = 0                                
c       tk1, tk2, tk3 = temperature tracking coefficients (see                  
c                       routine tstep)                                          
c       jtemp = temperature tracking option parameter (see routine              
c               tstep)                                                          
c                                                                               
      common /hh/ tzero,told,tempc,tempc0,tempc1,tempc2,tempk,press,            
     $ tk1,tk2,tk3,jtemp                                                        
