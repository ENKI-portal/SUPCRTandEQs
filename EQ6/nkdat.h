c--------------------------------------------------------------------           
c nkdat.h                                                                       
c

	block data nkdat

      parameter (nordpa=6,nordp1=nordpa+1)	                                                                               
      common /nk/ binm(nordp1,nordp1)
                                                 
      data (binm(1,i),i=1,7) /7*1./                                             
      data (binm(2,i),i=1,7) /7*1./                                             
      data (binm(3,i),i=1,7) /1.,2.,5*1./                                       
      data (binm(4,i),i=1,7) /1.,3.,3.,4*1./                                    
      data (binm(5,i),i=1,7) /1.,4.,6.,4.,3*1./                                 
      data (binm(6,i),i=1,7) /1.,5.,10.,10.,5.,2*1./                            
      data (binm(7,i),i=1,7) /1.,6.,15.,20.,15.,6.,1./                          
	
	end
	