c-----------------------------------------------------------------------        
c nk.h                                                                          
      common /nk/ binm(nordp1,nordp1)                                           
c                                                                               
c     binm contains the binomial coefficients up to order nordpa                
c     where each index starts at 1 instead of zero.                             
c     all peculiar values are defaulted to 1.                                   
c                                                                               
c     that is, binm(n+1,k+1) = (n) =  (n| * k|)/(n-k)|                          
c                              (k)                                              
c                                                                               
c     for example,  binm(4,3) = (3)                                             
c                               (2)                                             
c                                                                               
c      and          binm(4,1) = (3)                                             
c                               (0)                                             
c                                                                               
