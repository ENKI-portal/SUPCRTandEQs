c-----------------------------------------------------------------------        
c rk.h                                                                          
      common /rk/ affrct(nrctpa),afftot,fk(nrctpa),sk(nrctpa),                  
     $ csigma(imchpa,nrctpa),rreac0(nrctpa),rreac1(nrctpa),                     
     $ rrelrp(nrctpa),drelr0(nordpa,nrctpa),rirec0,rirec1,                      
     $ drirc0(nordpa),rrelr0(nrctpa),rrelr1(nrctpa),                            
     $ rk(imchpa,nrctpa),trk(imchpa,nrctpa),eact(imchpa,nrctpa),                
     $ cdac(ndctpa,imchpa,nrctpa),rirecp,ssk(nrctpa),                           
     $ ndact(imchpa,nrctpa),ndac(ndctpa,imchpa,nrctpa),ndctmx,                  
     $ nrk(nrctpa),nsk(nrctpa),ncorr,imech(nrctpa),imchmx,qnocor,qnocut         
c                                                                               
      common /rkc/ udac(ndctpa,imchpa,nrctpa)                                   
