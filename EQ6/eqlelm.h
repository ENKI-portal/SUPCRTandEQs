c--------------------------------------------------------------------                  
c eqlelm.h                                                                      
c                                                                               
c     elam  triples                                                             
c      elam(1,i,j)   e lambda for charges i,j (abs values)                      
c      elam(2,i,j)   e lambda"    "        "     "                              
c      elam(3,i,j)   e lambda""   "        "     "                              
c                                                                               
      common /eqlelm/ elam(3,10,10)                                             
