c-------------------------------------------------------------------            
c vx.h                                                                          
      common /vx/ xbar(iktpar,nxtpar),xbarlm(iktpar,nxtpar),                    
     $ xbarlg(iktpar,nxtpar),lamlg(iktpar,nxtpar),                              
     $ apx(iapxpa,nxtpar),w(iktpar,nxtpar),sx(iktpar,iktpar),                   
     $ si(iktpar,nxtpar),siss(iktpar,nxtpar),ncomp(nxtpar),                     
     $ nend(iktpar,nxtpar),jsol(nxtpar)                                         
c                                                                               
      character*32 uxtype                                                       
      character*24 ussnp                                                        
      common /vxc/ uxtype(10),ussnp(iktpar,nxtpar)                              
