c-----------------------------------------------------------------------        
c bt.h                                                                          
      common /bt/ beta(kpar),betamx,betmxo,bbig,bneg,bgamx,betchb,              
     $ betamc,alpha(kpar),alphmx,bfac(kpar),efac(kpar),nfac(kpar),              
     $ ibetmx,ibetmc,ialfmx                                                     
c                                                                               
      character*24 ubbig,ubneg,ubgamx                                           
c                                                                               
      common /btc/ ubbig,ubneg,ubgamx                                           
