#!/bin/bash
#
# build and install f2c 
# script should be run as sudo
#
cd f2c
cd libf2c
make clean
make
mkdir -p /usr/local/include
cp f2c.h /usr/local/include
mkdir -p /usr/local/lib
cp libf2c.a /usr/local/lib
chmod 755 /usr/local/lib/libf2c.a 
cd ../src
make clean
make
mkdir -p /usr/local/bin
cp f2c /usr/local/bin
cd ../..
