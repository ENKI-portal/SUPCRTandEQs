c eqlib    version 3245R136, last revised 02/09/88 by rmm                       
c*eqlib  f77 check, ok 04/06/87 by tjw                                          
c   Be sure to put the correct release and stage numbers in parameters          
c   urelno and ustage in routine eqlib.                                         
c                                                                               
c     This is the principal subroutine of the EQLIB library, part of            
c     the EQ3/6 software package.  This library is copyrighted as               
c     follows-                                                                  
c                                                                               
c     Copyright (c) 1987 The Regents of the University of California,           
c     Lawrence Livermore National Laboratory. All rights reserved.              
c                                                                               
c     The following subroutines have been taken and in some cases               
c     adapted from the non-proprietary libraries of others and are              
c     excluded from the above copyright: dsiplx, sgeco, sgefa, sgesl,           
c     isamax, sasum, saxpy, sdot, and sscal.                                    
c                                                                               
c     The person responsible for this library is                                
c                                                                               
c               Thomas J. Wolery                                                
c               L-219                                                           
c               Lawrence Livermore National Laboratory                          
c               P.O. Box 808                                                    
c               Livermore, CA  94550                                            
c               (415) 422-5789                                                  
c               FTS 532-5789                                                    
c                                                                               
c     The modification history is in file eqlib.his.                            
c                                                                               
      subroutine eqlib(u1,u2,noutpt,nttyo,qtrue,qfalse)                         
c                                                                               
c     this routine provides eqlib version identification to the                 
c     calling code.  it also sets nttyol to nttyo and noutpl to                 
c     noutpt.  noutpl is used to make debugging prints by eqlib                 
c     routines.                                                                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
                                                                               
                                                                               
c   ************************************************                             
	include "implicit.h"
                                               
c                                                                               
	include "eqlpar.h"                                                  
c                                                                               
	include "eqltxp.h"                                                  
	include "eqlun.h"                                                   
	include "eqleps.h"                                                  
	include "eqlpal.h"                                                  
	include "eqlpsa.h"                                                  
	include "eqlslm.h"                                                  
	include "eqlpmu.h"                                                  
	include	"eqlpsl.h"                                                  
	include	"eqlpmx.h"                                                  
	include	"eqlpmb.h"                                                  
	include	"eqlpsb.h"                                                  
	include	"eqlhkf.h"                                                  
c                                                                               
      data urelno /'3245    '/, ustage /'R136    '/                             
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     set two logical variables, qtrue and qfalse, which can be used as         
c     a test in a debugger to confirm which of the possible integer             
c     values (-1, 0, or 1) correspond to a logical true or a logical            
c     false.                                                                    
c                                                                               
      qtrue  = .true.                                                           
      qfalse = .false.                                                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     set identification variables                                              
c                                                                               
      u1 = urelno                                                               
      u2 = ustage                                                               
c                                                                               
c     equate device number variables for the output file                        
c     and the tty file                                                          
c                                                                               
      noutpl=noutpt                                                             
      nttyol=nttyo                                                              
c                                                                               
c     print version numbers and copyright notice                                
c                                                                               
      write (noutpl,1000) u1,u2                                                 
      write (nttyol,1000) u1,u2                                                 
c                                                                               
 1000 format(/' Supported by EQLIB, version ',a4,a4,//                          
     $ '   Copyright (c) 1987 The Regents of the University of',                
     $ ' California,',/'   Lawrence Livermore National Laboratory. All',        
     $ ' rights reserved.',//)                                                  
c                                                                               
c     set parameters to variables for hkf and pitzer arrays                     
c
                                                                               
      nmut = nmupar                                                             
      nslmt = nslpar                                                            
      napt = nappar                                                             
      ibjmax = ibjpar                                                           
      nsxt = nsxpar                                                             
      nmxt = nmxpar                                                             
      islmax = islpar                                                           
c                                                                               
c     avoid underflow trapping on the ridge computer.  if not operating         
c     on a ridge computer, comment out this call and/or make undflw             
c     (another eqlib routine) into a dummy routine.                             
c                                                                               
      call undflw                                                               
c                                                                               
c     get the real*8 machine epsilon, smallest positive number, and             
c     the exponent range (assumed to be symmetrical).  also get                 
c     parameters for routine texp.  flpars is another eqlib routine.            
c                                                                               
      call flpars                                                               
c                                                                               
c     test for sufficient precision and exponent range                          
c                                                                               
      call chump                                                                
c                                                                               
      end                                                                       
c alters   last revised 12/12/87 by tjw                                         
c*alters created in f77 12/08/87 by tjw                                         
      subroutine alters(nxmod,jxmod,kxmod,xlkmod,uxmod,ars,                     
     $ amn,ags,apress,cdrs,cdrm,cdrg,tempc,afcnst,uspec,umin,                   
     $ ugas,usolx,nsb,nsq,nsq1,nst,nmt,ngt,nxt,ntpr,nsqmx1,narxmx,              
     $ ntprmx,noutpt,nttyo)                                                     
c                                                                               
c     This routine may appear first in the concatenated source file             
c     of the eqlib library, part of the eq3/6 software package.  The            
c     copyright notice and other information are contained in                   
c     eqlib, the principal subroutine of the eqlib library.                     
c                                                                               
c     this routine alters the log k values of reactions after they are          
c     read from the data file.  this is done before the reactions are           
c     re-written by any basis switching.  the reactions are identified          
c     on the input file by the names (uxmod) of the associated species.         
c     the log k values (actually here the corresponding interpolating           
c     polynomial coefficients) are altered according the following              
c     options-                                                                  
c                                                                               
c        kxmod = 0   replace the log k value by xlkmod                          
c        kxmod = 1   augment the log k value by xlkmod                          
c        kxmod = 2   destablize the associated species by xlkmod kcal           
c                                                                               
c     this routine maps any kxmod = 0 or 2 options into the equivalent          
c     kxmod = 1 option.  an unaltered pickup file (written by eq3nr or          
c     eq6) therefore may have only the kxmod = 1 alter option in                
c     addition to the kxmod = -1 suppression option (which is handled           
c     by routine supprs).  the effect of this is that if temperature            
c     in eq6 is a function of reaction progress, the augmentation is            
c     constant, corresponding to a fixed offset in log k, a corres-             
c     ponding fixed offset in the gibbs energy and enthalpy of reaction,        
c     and a corresponding fixed offset in the gibbs energy and enthalpy         
c     of formation  of the associated species.  the entropy and heat            
c     capacity of reaction and the corresponding functions of the               
c     associated species are unchanged.                                         
c                                                                               
c     changing the log k value to 500. (values that would exceed 500.           
c     are truncated) has the effect of suppressing the associated               
c     species.  a suppressed species is excluded from the model.                
c     some species on the data file may be automatically suppressed             
c     because no log k data are available (the data file gives the              
c     equivalent of log k = 500.).                                              
c                                                                               
c     user-defined suppression, however, is more properly done by               
c     specifying kxmod = -1.  the species status flags are then marked          
c     for suppression, but the log k values are preserved.  this allows         
c     calculation of saturation indices, which would otherwise be               
c     impossible.                                                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include:'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*(*) uxmod(*),uspec(*),umin(*),ugas(*),usolx(*)                  
      character*24 uaq,umn,ugs,uss,unamsp                                       
c                                                                               
      dimension xlkmod(*),ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),            
     $ ags(narxmx,ntprmx,*),apress(narxmx,*),cdrs(nsqmx1,*),                    
     $ cdrm(nsqmx1,*),cdrg(nsqmx1,*),jxmod(*),kxmod(*)                          
c                                                                               
      data uaq /'aqueous species         '/                                     
      data umn /'minerals                '/                                     
      data ugs /'gases                   '/                                     
      data uss /'solid solutions         '/                                     
      data ufix /'fix     '/                                                    
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
      if (nxmod.le.0) go to 999                                                 
c                                                                               
c     compute the grid pressure                                                 
c                                                                               
      call evdatc(pgrid,apress,tempc,ntpr,narxmx,ntprmx)                        
c                                                                               
      do 1200 n=1,nxmod                                                         
      if (kxmod(n) .lt. 0) go to 1200                                           
      unamsp = uxmod(n)                                                         
      xlkdel = xlkmod(n)                                                        
c                                                                               
c     aqueous species                                                           
c                                                                               
      if (jxmod(n) .eq. 0) then                                                 
        do 1005 ns=1,nst                                                        
        if (unamsp .eq. uspec(ns)) go to 1007                                   
 1005   continue                                                                
c                                                                               
        write (noutpt,237) unamsp,uaq                                           
        write (nttyo,237) unamsp,uaq                                            
  237   format(' * note- alter species ',a24,' was not among the',              
     $   /7x,'loaded ',a24,' (eqlib/alters)',/)                                 
        go to 1200                                                              
c                                                                               
 1007   if (ns .le. nsb) then                                                   
          write (noutpt,1017) unamsp                                            
          write (nttyo,1017) unamsp                                             
 1017     format(' * note- ',a24,' is in the strict basis so',                  
     $    /7x,'it has no alter function (eqlib/alters)',/)                      
          go to 1200                                                            
        endif                                                                   
c                                                                               
        nrs = ns - nsb                                                          
        call evdatk(xlkold,nrs,ars,tempc,ntpr,narxmx,ntprmx)                    
c                                                                               
        if (kxmod(n) .eq. 0) then                                               
          kxmod(n) = 1                                                          
          xlkdel = xlkdel - xlkold                                              
          xlkmod(n) = xlkdel                                                    
        elseif (kxmod(n) .eq. 2) then                                           
          xlkdel = -cdrs(nsq1,nrs)*xlkdel/afcnst                                
          kxmod(n) = 1                                                          
          xlkmod(n) = xlkdel                                                    
        endif                                                                   
        do 300 j = 1,ntprmx                                                     
        ars(1,j,nrs) = ars(1,j,nrs) + xlkdel                                    
  300   continue                                                                
c                                                                               
        call prreac(cdrs,uspec,unamsp,nrs,nsq,nsq1,nsqmx1,noutpt)               
        call prreac(cdrs,uspec,unamsp,nrs,nsq,nsq1,nsqmx1,nttyo)                
        xlknew = xlkold + xlkdel                                                
        write (noutpt,1026) tempc,pgrid,xlkold,xlknew                           
        write (nttyo,1026) tempc,pgrid,xlkold,xlknew                            
 1026   format(/7x,'log k of the above reaction at ',f10.3,                     
     $  /7x,' deg celsius and ',f10.3,' bars was changed from ',                
     $  /7x,f10.4,' to ',f10.4,/)                                               
        go to 1200                                                              
      endif                                                                     
c                                                                               
c     minerals                                                                  
c                                                                               
      if (jxmod(n) .eq. 1) then                                                 
        if (unamsp(1:8) .eq. ufix(1:8)) then                                    
          write (noutpt,1216) unamsp                                            
          write (nttyo,1216) unamsp                                             
 1216     format(' * note- ',a24,' is a fictive fugacity fixing',               
     $    /7x'mineral so it has no alter function  (eqlib/alters)',/)           
          go to 1200                                                            
        endif                                                                   
c                                                                               
        do 1035 nm=1,nmt                                                        
        if (unamsp .eq. umin(nm)) go to 1037                                    
 1035   continue                                                                
c                                                                               
        write (noutpt,237) unamsp,umn                                           
        write (nttyo,237) unamsp,umn                                            
        go to 1200                                                              
c                                                                               
 1037   call evdatk(xlkold,nm,amn,tempc,ntpr,narxmx,ntprmx)                     
c                                                                               
        if (kxmod(n) .eq. 0) then                                               
          kxmod(n) = 1                                                          
          xlkdel = xlkdel - xlkold                                              
          xlkmod(n) = xlkdel                                                    
        elseif (kxmod(n) .eq. 2) then                                           
          xlkdel = -cdrm(nsq1,nm)*xlkdel/afcnst                                 
          kxmod(n) = 1                                                          
          xlkmod(n) = xlkdel                                                    
        endif                                                                   
        do 305 j = 1,ntprmx                                                     
        amn(1,j,nm) = amn(1,j,nm) + xlkdel                                      
  305   continue                                                                
c                                                                               
        call prreac(cdrm,uspec,unamsp,nm,nsq,nsq1,nsqmx1,noutpt)                
        call prreac(cdrm,uspec,unamsp,nm,nsq,nsq1,nsqmx1,nttyo)                 
        xlknew = xlkold + xlkdel                                                
        write (noutpt,1026) tempc,pgrid,xlkold,xlknew                           
        write (nttyo,1026) tempc,pgrid,xlkold,xlknew                            
        go to 1200                                                              
      endif                                                                     
c                                                                               
c     gases                                                                     
c                                                                               
      if (jxmod(n) .eq. 2) then                                                 
        do 1065 ng=1,ngt                                                        
        if (unamsp .eq. ugas(ng)) go to 1067                                    
 1065   continue                                                                
c                                                                               
        write (noutpt,237) unamsp,ugs                                           
        write (nttyo,237) unamsp,ugs                                            
        go to 1200                                                              
c                                                                               
 1067   call evdatk(xlkold,ng,ags,tempc,ntpr,narxmx,ntprmx)                     
c                                                                               
        if (kxmod(n) .eq. 0) then                                               
          kxmod(n) = 1                                                          
          xlkdel = xlkdel - xlkold                                              
          xlkmod(n) = xlkdel                                                    
        elseif (kxmod(n) .eq. 2) then                                           
          xlkdel = -cdrg(nsq1,nm)*xlkdel/afcnst                                 
          kxmod(n) = 1                                                          
          xlkmod(n) = xlkdel                                                    
        endif                                                                   
        do 310 j = 1,ntprmx                                                     
        ags(1,j,ng) = ags(1,j,ng) + xlkdel                                      
  310   continue                                                                
c                                                                               
        call prreac(cdrg,uspec,unamsp,ng,nsq,nsq1,nsqmx1,noutpt)                
        call prreac(cdrg,uspec,unamsp,ng,nsq,nsq1,nsqmx1,nttyo)                 
        xlknew = xlkold + xlkdel                                                
        write (noutpt,1026) tempc,pgrid,xlkold,xlknew                           
        write (nttyo,1026) tempc,pgrid,xlkold,xlknew                            
        go to 1200                                                              
      endif                                                                     
c                                                                               
c     solid solutions                                                           
c                                                                               
      if (jxmod(n) .eq. 3) then                                                 
        do 1095 nx=1,nxt                                                        
        if (unamsp .ne. usolx(nx)) go to 1095                                   
        go to 1097                                                              
 1095   continue                                                                
c                                                                               
        write (noutpt,237) unamsp,uss                                           
        write (nttyo,237) unamsp,uss                                            
        go to 1200                                                              
c                                                                               
 1097   write (noutpt,1098) unamsp                                              
        write (nttyo,1098) unamsp                                               
 1098   format(' * note- ',a24,' is a solid solution',                          
     $  /7x,'so it has no alter function (eqlib/alters)',/)                     
      endif                                                                     
c                                                                               
 1200 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c autosw   last revised 12/11/87 by tjw                                         
      subroutine autosw(icode,ars,amn,ags,cess,cdrs,cdrm,cdrg,                  
     $ cxistq,mwtss,z,zsq2,titr,azero,hydn,csp,uzvec1,uspec,jflag,              
     $ jsflag,iindx1,ibswx,ibasis,nsp,iopg1,iacion,iebal,nhydr,                 
     $ nchlor,nct,nsb,nsq,nsq1,nst,nrst,nmt,ngt,ksq,nctmax,nsqmx1,              
     $ narxmx,ntprmx,nerr,noutpt,nttyo,qhydth,qpt4,qbassw)                      
c                                                                               
c     this routine executes automatic basis switching for the purpose           
c     of reducing mass balance residuals.  the eqlib routine                    
c     fbassw finds candidates for basis switching, and eqlib routine            
c     gabswx resolves any conflicts.                                            
c                                                                               
c     input                                                                     
c       icode = 3 if this routine is being used by eq3nr                        
c       uzvec1 = array of names of components in the matrix.                    
c       uspec = array of names of aqueous species                               
c       iindx1 = array relating matrix indexing to the regular indexing         
c         of species                                                            
c       ibswx = array of indices of non-master species that are                 
c         candidates for basis switching                                        
c       ibasis = array of indices of data file non-master species that          
c         are currently used in basis switching                                 
c       iebal = 'ns' index of the aqueous species whose concentration           
c               is adjusted to achieve electrical balance; = 0                  
c               if no such adjustment is being made                             
c       ksq = matrix index of last auxiliary basis species                      
c       iopg1 = activity coefficient option switch                              
c       nerr = error flag                                                       
c                                                                               
c     output                                                                    
c       none (makes basis switches)                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*(*) uzvec1(*),uspec(*)                                          
c                                                                               
      dimension ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),                      
     $ ags(narxmx,ntprmx,*),cess(nctmax,*),cdrs(nsqmx1,*),                      
     $ cdrm(nsqmx1,*),cdrg(nsqmx1,*),cxistq(*),mwtss(*),z(*),zsq2(*),           
     $ titr(*),azero(*),hydn(*),csp(*),jflag(*),jsflag(*),iindx1(*),            
     $ ibswx(*),ibasis(*),nsp(*)                                                
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 25 krow=2,ksq                                                          
      ns2=ibswx(krow)                                                           
      if (ns2.eq.0) go to 25                                                    
      ns1=iindx1(krow)                                                          
      is2=ibasis(ns1)                                                           
c                                                                               
c     if the ns1-th species is not currently involved in a switch,              
c     go make the switch.  if it is, that switch must first be undone,          
c     unless it is a discretionary switch (switch of a strict basis             
c     species with an auxiliary basis species).  if the switch called           
c     for undoes the switch made previously, avoid undoing it twice.            
c                                                                               
      if (ns2.eq.is2) then                                                      
c                                                                               
c       case 1. the switch called for undoes a current switch                   
c                                                                               
        ibasis(ns2)=ns2                                                         
        ibasis(ns1)=ns1                                                         
      else                                                                      
c                                                                               
c       case 2.  ns1 is not already involved in a switch                        
c       case 3.  ns1 is already switched with another species                   
c                                                                               
        ibasis(ns1)=ns2                                                         
        ibasis(ns2)=ns1                                                         
        if (is2.ne.ns1) then                                                    
c                                                                               
c         case 3.  undo the existing switch                                     
c                                                                               
          ibasis(is2)=is2                                                       
          call switch(ns1,is2,icode,ars,amn,ags,cess,cdrs,cdrm,                 
     $    cdrg,cxistq,mwtss,z,zsq2,titr,azero,hydn,csp,uspec,jflag,             
     $    jsflag,ibasis,nsp,iopg1,iacion,iebal,nhydr,nchlor,nct,nsb,            
     $    nsq,nsq1,nst,nrst,nmt,ngt,nctmax,nsqmx1,narxmx,ntprmx,nerr,           
     $    noutpt,nttyo,qhydth,qpt4,qbassw)                                      
        endif                                                                   
      endif                                                                     
c                                                                               
      call switch(ns1,ns2,icode,ars,amn,ags,cess,cdrs,cdrm,                     
     $ cdrg,cxistq,mwtss,z,zsq2,titr,azero,hydn,csp,uspec,jflag,                
     $ jsflag,ibasis,nsp,iopg1,iacion,iebal,nhydr,nchlor,nct,nsb,               
     $ nsq,nsq1,nst,nrst,nmt,ngt,nctmax,nsqmx1,narxmx,ntprmx,nerr,              
     $ noutpt,nttyo,qhydth,qpt4,qbassw)                                         
c                                                                               
c     reset names in the uzvec1 array                                           
c                                                                               
      uzvec1(krow) = uspec(ns1)                                                 
c                                                                               
   25 continue                                                                  
      end                                                                       
c azpt4      last modified 11/25/87 by tjw and rmm                              
      subroutine azpt4(nst,nsq,nsb,msum,conc,azero,z,concbs)                    
c                                                                               
c              created 9/29/87 by rmm                                           
c                                                                               
c              calculates azero used in hkf equations                           
c     input   nst     number of aqueous species read in from datafile           
c             nsq     number of basis species                                   
c             nsb     number of strict basis species                            
c             conc    array of concentrations  mi                               
c             azero   array of ion size parameters (hydn. theory)               
c             z       array of charges                                          
c             concbs                                                            
c     output  hkfaz   in eqlhkf                                                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlpar.h"                                                  
c                                                                               
      include "eqlhkf.h"                                                  
      include "eqlun.h"                                                   
c                                                                               
      dimension conc(*),azero(*),z(*),concbs(*)                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      qflag = .false.                                                           
      limit=.001                                                                
        if (qflag) then                                                         
c               hkf using l.h.s. eqn 124                                        
          hkfaz=0.0                                                             
          omegns=0.0                                                            
          ii=0                                                                  
          do 100 ns = 2,nst                                                     
          conrat=conc(ns)/msum                                                  
          if (conrat .le. limit) go to 100                                      
          ii=ii + 1                                                             
          zdum=z(ns)                                                            
          call omega4(ns,omegns,zdum,ierr)                                      
          hkfaz=hkfaz + (zdum*zdum/omegns)                                      
  100     continue                                                              
          rtot=real(ii)                                                         
          hkfaz = (2.0*1.66027e05*hkfaz)/rtot                                   
c                                                                               
        else                                                                    
c              hkf   using  r.h.s.                                              
c              calculate azero for hkf equations using salts                    
c                                                                               
          sum=0.0                                                               
          nusalt=0                                                              
          const=2.0*1.66027e05                                                  
c                                                                               
          do 200 ii=1,icnt                                                      
c              cation                                                           
          icat=islt(1,ii)                                                       
          conrat = concbs(icat)/msum                                            
          if (conrat .le. limit) go to 200                                      
c              anion                                                            
          ian=islt(2,ii)                                                        
          conrat = concbs(ian)/msum                                             
          if (conrat .le. limit) go to 200                                      
c              both cation and anion ok - get information                       
          icatsf=islt(3,ii)                                                     
          iansf=islt(4,ii)                                                      
          zcat=z(icat)                                                          
          zan=z(ian)                                                            
          call omega4(icat,omecat,zcat,ierr1)                                   
          call omega4(ian,omean,zan,ierr2)                                      
c              calculate both sums used                                         
          if ((ierr1 .eq. 0) .and. (ierr2 .eq. 0)) then                         
            nusalt=nusalt + icatsf + iansf                                      
            sum = sum + real(icatsf)*(zcat*zcat/omecat) +                       
     $               real(iansf)*(zan*zan/omean)                                
          endif                                                                 
  200     continue                                                              
c                                                                               
          if (nusalt .eq. 0) then                                               
            write(nttyol,9999)                                                  
            write(noutpl,9999)                                                  
 9999       format(' fatal error in azpt4 ',/,'  ns        concbs')             
            do 9998 ns=1,nsq                                                    
            write(noutpl,9997) ns,concbs(ns)                                    
 9997       format(1x,i4,x,g13.5)                                               
 9998       continue                                                            
c                                                                               
            write(noutpl,9996)                                                  
 9996       format(' hypothetical salt solution ',/,                            
     $         ' cat    an    catsf   ansf       mol. salt')                    
            write(noutpl,9995) ((islt(jj,ii),jj=1,4),                           
     $                          mslt(ii),ii=1,icnt)                             
 9995       format(4(5x,i5),5x,g13.5)                                           
            go to 999                                                           
          endif                                                                 
c                                                                               
c                   r.h.s. in eqn 124 hkf                                       
        hkfaz=const*sum/real(nusalt)                                            
c                                                                               
      endif                                                                     
  999 continue                                                                  
      end                                                                       
c bdmlx    last revised 12/12/87 by tjw                                         
c*bdmlx  f77 rewrite 04/06/87 by tjw                                            
c                                                                               
      subroutine bdmlx(nst)                                                     
c                                                                               
c     input  nst =  number of species                                           
c     output derived mu arrays nmxi, nmxx                                       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlpar.h"                                                  
c                                                                               
      include "eqlpmx.h"                                                  
      include "eqlpmu.h"                                                  
      include "eqlun.h"                                                      
c                                                                               
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      nxx = 1                                                                   
c                                                                               
      do 40 ns=2,nst                                                            
c                                                                               
c     set first index into nmxx                                                 
c                                                                               
      nmxi(1,ns) = nxx                                                          
c                             *-----------------------------------------        
c                             * search col 1, nmux array                        
c                             *-----------------------------------------        
      do 20 k=1,nmu                                                             
      if (nmux(1,k) .eq. ns) then                                               
c                                                                               
c       found one, get two other indices and nmux index                         
c                                                                               
        nmxx(1,nxx) = nmux(2,k)                                                 
        nmxx(2,nxx) = nmux(3,k)                                                 
        nmxx(3,nxx) = k                                                         
        nxx = nxx+1                                                             
        if (nxx .gt. nmxt) then                                                 
          write (noutpl,1010)                                                   
          write (nttyol,1010)                                                   
 1010     format(' * error - mu index array index overflow ',                   
     $    '(eqlib/bdmlx)')                                                      
          stop                                                                  
        endif                                                                   
      endif                                                                     
   20 continue                                                                  
c                             *-----------------------------------------        
c                             * search col 2, nmux array                        
c                             *-----------------------------------------        
      do 25 k=1,nmu                                                             
      if (nmux(2,k) .eq. ns) then                                               
c                                                                               
c       found one, get other index                                              
c                                                                               
        nx1 = nmux(1,k)                                                         
c                                                                               
c       skip if duplicate                                                       
c                                                                               
        if (ns .ne. nx1) then                                                   
          nmxx(1,nxx) = nx1                                                     
          nmxx(2,nxx) = nmux(3,k)                                               
          nmxx(3,nxx) = k                                                       
          nxx = nxx+1                                                           
          if (nxx .gt. nmxt) then                                               
            write (noutpl,1010)                                                 
            write (nttyol,1010)                                                 
            stop                                                                
          endif                                                                 
        endif                                                                   
      endif                                                                     
   25 continue                                                                  
c                             *-----------------------------------------        
c                             * search col 3, nmux array                        
c                             *-----------------------------------------        
      do 30 k=1,nmu                                                             
      if (nmux(3,k) .eq. ns) then                                               
c                                                                               
c       found one, get other indices                                            
c                                                                               
        nx1 = nmux(1,k)                                                         
        nx2 = nmux(2,k)                                                         
c                                                                               
c       skip if duplicate                                                       
c                                                                               
        if (ns .ne. nx1 .and. ns .ne. nx2) then                                 
          nmxx(1,nxx) = nx1                                                     
          nmxx(2,nxx) = nx2                                                     
          nmxx(3,nxx) = k                                                       
          nxx = nxx+1                                                           
          if (nxx .gt. nmxt) then                                               
            write (noutpl,1010)                                                 
            write (nttyol,1010)                                                 
            stop                                                                
          endif                                                                 
        endif                                                                   
      endif                                                                     
   30 continue                                                                  
c                                                                               
c     set last index into nmxx                                                  
c                                                                               
      nmxi(2,ns) = nxx-1                                                        
   40 continue                                                                  
c                                                                               
      nmlx = nxx-1                                                              
c                                                                               
      end                                                                       
c bdslx    last revised 07/24/87 by rmm                                         
c*bdslx  f77 rewrite 04/06/87 by tjw                                            
      subroutine bdslx(nst)                                                     
c                                                                               
c     input  nst = number of species                                            
c     output derived s-lambda arrays nsxi, nsxx                                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlpar.h"                                                  
c                                                                               
      include "eqlpsl.h"                                                  
      include "eqlpsa.h"                                                  
      include "eqlun.h"                                                   
c                                                                               
c                                                                               
c-----------------------------------------------------------------------        
c                                                                                                                                
      nxx = 1                                                                   
c                                                                               
      do 40 ns=2,nst                                                            
c                                                                               
c     set first species index into nsxx                                         
c                                                                               
      nsxi(1,ns) = nxx                                                          
c                                                                               
c     search col 1, nslmx array                                                 
c                                                                               
      do 20 k=1,nslm                                                            
      if (nslmx(1,k) .eq. ns) then                                              
c                                                                               
c       found one.  get other species index, nslmx index                        
c                                                                               
        nsxx(1,nxx) = nslmx(2,k)                                                
        nsxx(2,nxx) = k                                                         
        nxx = nxx+1                                                             
        if (nxx .gt. nsxt) then                                                 
          write (noutpl,1010)                                                   
          write (nttyol,1010)                                                   
 1010     format(' * error - s-lambda index array overflow ',                   
     $    '(eqlib/bdslx)')                                                      
          stop                                                                  
        endif                                                                   
      endif                                                                     
   20 continue                                                                  
c                                                                               
c     search col 2                                                              
c                                                                               
      do 30 k=1,nslm                                                            
      nsl2 = nslmx(2,k)                                                         
      nsl1 = nslmx(1,k)                                                         
      if (nsl2 .eq. ns) then                                                    
c                                                                               
c       found one - skip if equal pair                                          
c                                                                               
        if (nsl1 .ne. nsl2) then                                                
          nsxx(1,nxx) = nsl1                                                    
          nsxx(2,nxx) = k                                                       
          nxx = nxx+1                                                           
          if (nxx .gt. nsxt) then                                               
            write (noutpl,1010)                                                 
            write (nttyol,1010)                                                 
            stop                                                                
          endif                                                                 
        endif                                                                   
      endif                                                                     
   30 continue                                                                  
c                                                                               
c     set last index into nsxx                                                  
c                                                                               
      nsxi(2,ns) = nxx-1                                                        
   40 continue                                                                  
c                                                                               
      nslx = nxx-1                                                              
c                                                                               
      end                                                                       
c betgam   last revised 10/28/87 by tjw                                         
c*betgam created in f77 10/22/87 by tjw                                         
      subroutine betgam(uspec,conc,glgc,glgo,nst,bgamx,ubgamx)                  
c                                                                               
c     this routine finds the activity coefficient residual with the             
c     largest magnitude (bgamx).  the range of activiity coefficient            
c     residuals covers all aqueous species, including water.                    
c                                                                               
c     input                                                                     
c       uspec = name array for aqueous species                                  
c       conc = concentration array for aqueous species                          
c       glgc = most recently calculated values of the log of the                
c              activity coefficients                                            
c       glgo = previous values of the log of the activity coefficients          
c       nst = total number of aqueous species                                   
c                                                                               
c       note- glgc(1) is the most recently calculated value of the              
c         activity of water, and glgo(1) is the previous value of the           
c         same quantity.                                                        
c                                                                               
c     output                                                                    
c       bgamx = the largest magntitude of any activity coefficient              
c               residual for an aqueous species.  an activity coefficient       
c               residual is the difference between the most recently            
c               calculated value of the activity coefficient and                
c               the corresponding previous value                                
c       ubgamx = the name of the aquoeus species the value of                   
c               whose activity coefficient residual is bgamx                    
c                                                                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlun.h"                                                   
c                                                                               
      character*24 uspec(*), ubgamx                                             
      dimension conc(*),glgc(*),glgo(*)                                         
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (iopg1 .ge. 101) then                                                  
        write (nttyol,1000) iopg1                                               
        write (noutpl,1000) iopg1                                               
 1000   format(' * error- entry to betgam with iopg1 = ',i5,                    
     $  ' (eqlib/betgam)')                                                      
        stop                                                                    
      endif                                                                     
c                                                                               
      bgamx=-1.                                                                 
c                                                                               
c     calculate activity coefficient residuals                                  
c                                                                               
      do 540 ns=1,nst                                                           
      if (conc(ns) .gt. 0.) then                                                
        dgam = glgc(ns) - glgo(ns)                                              
        adgam=abs(dgam)                                                         
        if (adgam.gt.bgamx) then                                                
          bgamx=adgam                                                           
          ubgamx=uspec(ns)                                                      
        endif                                                                   
      endif                                                                     
  540 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c chump    last revised 04/17/87 by tjw                                         
c*chump  f77 check, ok 04/17/87 by tjw                                          
      subroutine chump                                                          
c                                                                               
c     this routine tests the floating point precision and exponent              
c     range for adequacy for eq3/6 calculations.  this is essentially           
c     a trap to insure that most floating point variables are real*8.           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqleps.h"                                                  
      include "eqlun.h"                                                   
c                                                                               
c     epstst = test on floating point epsilon                                   
c       (should be at least 1.e-14)                                             
c     irgtst = requirement on the floating point exponent range                 
c       (should be at least 75)                                                 
c                                                                               
      data epstst/1.e-14/,irgtst/38/                                            
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      qstop=.false.                                                             
      if (eps.gt.epstst) then                                                   
        write (noutpl,1007) eps,epstst                                          
        write (nttyol,1007) eps,epstst                                          
 1007   format(' * error - insufficient floating point epsilon = ',e12.5,      
     $  /5x,'needs to be at least as small as ',e12.5,' (eqlib/chump)')          
        qstop=.true.                                                            
      endif                                                                     
      if (irang.lt.irgtst) then                                                 
        write (noutpl,1008) irang                                               
        write (nttyol,1008) irang                                               
 1008   format(' * error - insufficient floating point exponent range =',       
     $  ' +/- ',i4,/5x,'needs to be +/- at least ',i4,' (eqlib/chump)')         
        qstop=.true.                                                            
      endif                                                                     
      if (qstop) then                                                           
        write (noutpl,1009)                                                     
        write (nttyol,1009)                                                     
 1009   format(/' * sorry, your computer is not adequate',                      
     $  ' (eqlib/chump)')                                                       
        stop                                                                    
      endif                                                                     
      end                                                                       
c dscram   last revised 04/07/87 by tjw                                         
c*dscram f77 check, ok 04/07/87 by tjw                                          
      subroutine dscram(nf1,nf2)                                                
c                                                                               
c     this routine unscrambles a file of tables whose lines are                 
c     interspersed, but which are marked 'a', 'b', 'c', etc., in                
c     column one.                                                               
c                                                                               
c     input                                                                     
c       nf1 = unit number of the scrambled file.                                
c       nf2 = unit number of the unscrambled file, which must                   
c         already be open.  the record length will be 128 characters.           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*128 uline                                                       
      character*1 ualph(26),utm,utx                                             
c                                                                               
      data ialph/26/                                                            
c                                                                               
      data (ualph(n), n=1,26) /'a','b','c','d','e','f','g','h','i','j',         
     $ 'k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'/         
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      rewind nf2                                                                
      do 20 i=1,ialph                                                           
      utx=ualph(i)                                                              
      rewind nf1                                                                
      do 10 j=1,10000                                                           
      read (nf1,1000,end=20) utm,uline                                          
 1000 format(a1,a128)                                                           
      if (utm.eq.utx) write (nf2,1005) uline                                    
 1005 format(a128)                                                              
   10 continue                                                                  
   20 continue                                                                  
      end                                                                       
c ssfunc   last revised 11/25/87 by tjw                                         
c*ssfunc f77 check, 07/08/87 by tjw                                             
c                                                                               
c     routine that computes the value of the saturation                         
c     index of a solid solution given the composition of the                    
c     solid solution and the composition of the aqueous phase                   
c     in equilibrium with it. this routine is called by simplex                 
c     subroutine.                                                               
c                                                                               
c     comp is array containing composition of solid solution                    
c     (note that dsiplx provides ncomp-1 compositions of endmember              
c     phases, ss computes the last one)                                         
c                                                                               
      function ssfunc(comp,nend,w,ndbug1,xbar,lamlg,ncomp,jsol,nn,xqk,          
     $ nnm1,nx,tempk,press,al10,rconst,afkst,idbg,iktmax)                       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      dimension nend(iktmax,*),w(iktmax,*),xbar(iktmax,*),                      
     $ lamlg(iktmax,*),comp(*),ncomp(*),jsol(*),xqk(*)                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     compute comp(nn) which is dependent variable                              
c                                                                               
      sum = 0.0                                                                 
      do 9 i=1,nnm1                                                             
      sum = sum+comp(i)                                                         
    9 continue                                                                  
      comp(nn) = 1.0-sum                                                        
c                                                                               
c     need to put current values of comp back into xbar                         
c                                                                               
      j = 1                                                                     
      do 10 i=1,ncomp(nx)                                                       
      nm = nend(i,nx)                                                           
c                                                                               
c     component not present                                                     
c                                                                               
      if (nm.eq.0) go to 10                                                     
      xbar(i,nx)=comp(j)                                                        
      j = j+1                                                                   
   10 continue                                                                  
c                                                                               
      call lamda(al10,jsol,lamlg,ncomp,nx,rconst,tempk,xbar,w,iktmax)           
c                                                                               
      if (idbg.ge.1) write(noutpl,12)(xbar(i,nx),i=1,ncomp(nx)),ssfunc          
      if (idbg.ge.1) write(noutpl,12)(lamlg(i,nx),i=1,ncomp(nx))                
   12 format(5x,16(d10.4,2x))                                                   
c                                                                               
      ssi=0.0                                                                   
      do 11 i=1,ncomp(nx)                                                       
      if (nend(i,nx).eq.0) go to 11                                             
      lamdax = 10**lamlg(i,nx)                                                  
      xdum = xbar(i,nx)                                                         
c                                                                               
c     avoid xbar>1 or xbar<0 areas                                              
c                                                                               
      if (xdum.le.0.0) then                                                     
        xdum=-xdum                                                              
        ssi = ssi-abs(xdum*tlg(xqk(i)/(xdum*lamdax)))                           
c                                                                               
c       lower limit for calculation                                             
c                                                                               
        xbar(i,nx) = 1.0e-20                                                    
      elseif (xdum.ge.1.0) then                                                 
        ssi = ssi-abs(xdum*tlg(xqk(i)/(xdum*lamdax)))                           
      else                                                                      
        ssi = ssi+xdum*tlg(xqk(i)/(xdum*lamdax))                                
      endif                                                                     
   11 continue                                                                  
c                                                                               
c     ssfunc is the value that is maximized                                     
c                                                                               
c     affinity for solid solution                                               
c                                                                               
      ssfunc = ssi                                                              
c                                                                               
      end                                                                       
c dsiplx   last revised 11/25/87 by tjw                                         
c*dsiplx f77 partial rewrite 04/07/87 by tjw (needs a lot more work)            
      subroutine dsiplx(ssfunc,axx,n,nm,alpha,deps,nmin,itr,xbarh,fmin,         
     $          nit,nend,w,ndbug1,ntty,noutpt,xbar,lamlg,ncomp,iktmax,          
     $          jsol,nn,xqkx,nx,tempk,press,al10,rconst,afkst,idbg)             
c                                                                               
c     *** to find a local minimum or maximum of a function by use of            
c           simplex method proposed by j.a.nelder & r.mead ***                  
c                                                                               
c     Adapted from a nonpproprietary routine from the computer library          
c     of Tokyo University.                                                      
c                                                                               
c     this routine is used to find maximum of affinity function                 
c     in hypothetical solid solution subroutine hpsat                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension  axx(iktmax,*),xbarh(*),w(iktmax,*),xbar(iktmax,*),             
     $ lamlg(iktmax,*),xqkx(*),nend(iktmax,*),ncomp(*),jsol(*),                 
     $ alpha(*),deps(*)                                                         
c                                                                               
      dimension xr(100),f(101),test(200)                                        
c                                                                               
      external ssfunc                                                           
c                                                                               
      data small/1.e-25/                                                        
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     *** argument check and preparation ***                                    
c                                                                               
      nnm1 = n                                                                  
      ier = 0                                                                   
c                                                                               
c     wlb - itest = flag for oscillating solution                               
c                                                                               
      itest = 0                                                                 
      if (n.le.0 .or. n.gt.100) then                                            
        write (noutpt,1000) n                                                   
 1000   format(' (subr. dsiplx) the argument n is invalid.',5x,'n=',i7)         
        ier = 3                                                                 
      endif                                                                     
c                                                                               
      n1 = n+1                                                                  
      if ( nm.lt.n .or. nm.lt.1 ) then                                          
        write (noutpt,1010)  nm                                                 
 1010   format(' (subr. dsiplx) the argument nm is invalid.',5x,'m=',i7)        
        ier = 3                                                                 
      endif                                                                     
c                                                                               
      ker=0                                                                     
      do 30 i=1, 3                                                              
      if ( alpha(i).lt.-small )  ker = 1                                        
   30 continue                                                                  
c                                                                               
      if ( ker.eq.1 ) go to 40                                                  
      if ( alpha(2).ge.1.)  go to 40                                            
      if ( alpha(3).le.1..and. alpha(3).gt.small )  go to 40                    
c                                                                               
      alp = alpha(1)                                                            
      bet = alpha(2)                                                            
      gam = alpha(3)                                                            
      if ( abs(alpha(1)).le.small )  alp = 1.0                                  
      if ( abs(alpha(2)).le.small )  bet = 5e-1                                 
      if ( abs(alpha(3)).le.small )  gam = 2.0                                  
      go to 60                                                                  
c                                                                               
   40 write (noutpt,1020) (alpha(j), j=1,3)                                     
 1020 format(' (subr. dsiplx) the argument array alpha invalid.',5x,            
     $ 'alpha=',1p3e24.16)                                                      
      ier = 3                                                                   
c                                                                               
60    if ( deps(1).gt.-small .and. deps(2).gt.-small )  go to 80                
c                                                                               
70    write (noutpt,1030) deps(1),deps(2)                                       
1030   format(' (subr. dsiplx) the argument deps is invalid.',5x,               
     $           'deps=',1p2e24.16)                                             
      ier = 3                                                                   
      go to 140                                                                 
c                                                                               
   80 if ( abs(deps(1)) .gt. small ) go to 110                                  
      if ( abs(deps(2)) .le. small ) go to 70                                   
      ieps = 2                                                                  
      go to 140                                                                 
110   if ( abs(deps(2))-small )  120, 120, 130                                  
120   ieps = 1                                                                  
      go to 140                                                                 
130   ieps = 3                                                                  
140   if ( itr )  150, 160, 170                                                 
150   write (noutpt,1040)  itr                                                  
1040   format(' (subr. dsiplx) the argument itr is invalid.',5x,                
     $           'itr=',i7)                                                     
      ier = 3                                                                   
      go to 180                                                                 
160   nstop = 500                                                               
      go to 180                                                                 
170   nstop = itr                                                               
180   if ( n.le.0 .or. n.gt.100 ) go to 195                                     
      do 190 i=1, n                                                             
      if ( abs( axx(i,n1) ).gt.small ) go to 190                                
      write (noutpt,1050)  i, axx(i,n1)                                         
1050   format(' (subr. dsiplx) the argument axx is invalid.',5x,                
     $           'axx(',i2,',n+1 )=',1pe24.16)                                  
      ier = 3                                                                   
190   continue                                                                  
195   if ( ier.ne.3 )  go to 197                                                
      go to 999                                                                 
c                                                                               
c        *** set the vertices of the initial simplex ***                        
c                                                                               
197   do 210 i=2, n1                                                            
         i1 = i-1                                                               
          do 210 j=1, n                                                         
          if ( i1.eq.j )  go to 200                                             
          axx(j,i) = axx(j,1)                                                   
          go to 210                                                             
200          axx(j,i) = axx(j,1) + axx(j,n1)                                    
210    continue                                                                 
       icode = 1                                                                
       nit   = 1                                                                
c                                                                               
c        *** beginning of the loop for the simplex method ***                   
220    if ( nit.le.nstop )  go to 230                                           
       nit = nit-1                                                              
       ier = 1                                                                  
       if ( icode )  640, 640, 235                                              
c                                                                               
c       *** values of the function at the vertices ***                          
230    if ( icode.eq.0 )  go to 300                                             
235    do 270 i=1, n1                                                           
       do 240 j=1, n                                                            
240       xbarh(j) = axx(j,i)                                                   
          if ( nmin )  250, 250, 260                                            
250       f(i) = ssfunc(xbarh,nend,w,ndbug1,xbar,                               
     $                lamlg,ncomp,jsol,nn,xqkx,nnm1,nx,tempk,                   
     $                press,al10,rconst,afkst,idbg,iktmax)                      
      go to 270                                                                 
260   f(i) = -ssfunc(xbarh,nend,w,ndbug1,xbar,                                  
     $              lamlg,ncomp,jsol,nn,xqkx,nnm1,nx,tempk,                     
     $              press,al10,rconst,afkst,idbg,iktmax)                        
270   continue                                                                  
      icode = 0                                                                 
c                                                                               
c        *** find the maximum and minimum points ***                            
c                                                                               
300    imax = 1                                                                 
       imin = 1                                                                 
       fmax = f(1)                                                              
       fmin = f(1)                                                              
       do 340 i=2, n1                                                           
       if ( f(i)-fmax )  320, 320, 310                                          
310     fmax = f(i)                                                             
        imax = i                                                                
        go to 340                                                               
320    if ( f(i)-fmin )  330, 340, 340                                          
330     fmin = f(i)                                                             
        imin = i                                                                
340    continue                                                                 
       if ( ier.eq.1 )  go to 645                                               
c                                                                               
c        *** calculate the centroid of the points with i.ne.imax ***            
       do 360 j=1, n                                                            
          sum = 0.0                                                             
          do 350 i=1, n1                                                        
350       sum = sum + axx(j,i)                                                  
          xbarh(j)=(sum-axx(j,imax))/n                                          
360    continue                                                                 
c                                                                               
c        *** reflection of the maximum point ***                                
       alp1 = alp+1.0                                                           
       do 370 j=1, n                                                            
370    xr(j)=alp1*xbarh(j)-alp*axx(j,imax)                                      
       fr   =  ssfunc(xr,nend,w,ndbug1,xbar,                                    
     *                lamlg,ncomp,jsol,nn,xqkx,nnm1,nx,tempk,                   
     *                press,al10,rconst,afkst,idbg,iktmax)                      
       if ( nmin.gt.0 )  fr = -fr                                               
c                                                                               
c        *** see if func(reflec)<fmin ***                                       
       if ( fr-fmin )  420, 380, 380                                            
c                                                                               
c        *** find the next maximum point (when func(reflec).ge.fmin ) ***       
380    fsmax = -1d30                                                            
       do 390 i=1, n1                                                           
       if ( i.eq.imax )  go to 390                                              
       if ( f(i).le.fsmax )  go to 390                                          
       fsmax = f(i)                                                             
390    continue                                                                 
c                                                                               
c        *** see if func(reflec)>fsmax ***                                      
       if ( fr-fsmax )  400, 400, 460                                           
400    f(imax) = fr                                                             
       do 410 j=1, n                                                            
410    xbarh(j) = xr(j)                                                         
       go to 445                                                                
c                                                                               
c        *** expansion of the reflection (when  func(reflec).lt.fmin ) ***      
420    gam1 = 1.0-gam                                                           
       do 430 j=1, n                                                            
430    xbarh(j) = gam*xr(j)+gam1*xbarh(j)                                       
       fe = ssfunc(xbarh,nend,w,ndbug1,xbar,                                    
     *                lamlg,ncomp,jsol,nn,xqkx,nnm1,nx,tempk,                   
     *                press,al10,rconst,afkst,idbg,iktmax)                      
       if ( nmin.gt.0 )  fe = -fe                                               
c                                                                               
c        *** see if func(expans)<func(reflec) ***                               
       if ( fe-fr )  440, 400, 400                                              
440    f(imax) = fe                                                             
c                                                                               
c        *** replace the maximum point with a new vertex ***                    
445    do 450 j=1, n                                                            
450    axx(j,imax) = xbarh(j)                                                   
       go to 500                                                                
c                                                                               
c        *** in case of func(reflec)>fsmax ***                                  
460    if ( fr.ge.fmax )  go to 480                                             
       do 470 j=1, n                                                            
470    axx(j,imax) = xr(j)                                                      
c                                                                               
c        *** contraction of the maximum point or of its reflection ***          
480    bet1 = 1.0-bet                                                           
       do 490 j=1, n                                                            
490    xbarh(j)=bet*axx(j,imax)+bet1*xbarh(j)                                   
       fc = ssfunc(xbarh,nend,w,ndbug1,xbar,                                    
     *             lamlg,ncomp,jsol,nn,xqkx,nnm1,nx,tempk,                      
     *             press,al10,rconst,afkst,idbg,iktmax)                         
       if ( nmin.gt.0 )  fc = -fc                                               
c                                                                               
c        *** see if func(contrac)>fmax ***                                      
       if ( fc-fmax )  495, 620, 620                                            
495    f(imax) = fc                                                             
       go to 445                                                                
c                                                                               
c        *** convergence check ***                                              
500    go to ( 520, 550, 520 ), ieps                                            
520    sum2=0.0                                                                 
       sum=0.0                                                                  
       do 530 i=1,n1                                                            
       sum=sum+f(i)                                                             
530    continue                                                                 
       sum=sum/n1                                                               
       do 535 i=1,n1                                                            
       sum2=(f(i)-sum)**2+sum2                                                  
535    continue                                                                 
       sd=dsqrt(sum2/n1)                                                        
       if ( sd-deps(1) )  540, 540, 550                                         
540    if ( ieps.eq.1 )  go to 640                                              
       ieps = 4                                                                 
550    aintm = -1d30                                                            
       do 580 i=1, n                                                            
       i1 = i+1                                                                 
       do 570 k=i1, n1                                                          
          do 560 j=1, n                                                         
          aint = abs( axx(j,i)-axx(j,k) )                                       
560       if ( aint.gt.aintm )  aintm = aint                                    
570       continue                                                              
580    continue                                                                 
       go to ( 600, 590, 600, 590 ), ieps                                       
590    if ( aintm.le.deps(2) )  go to 640                                       
600    if ( aintm.gt.1.e15   )  go to 680                                       
       if ( ieps.eq.4 )  ieps = 3                                               
610    nit = nit+1                                                              
c                                                                               
c   wlb  add test for oscillating solution.  if the solution is                 
c   oscillating back and forth between two solutions that                       
c   are identical every other iteration, terminate with message.                
c                                                                               
      test(nit) = fmin                                                          
      if (nit.le.2) go to 220                                                   
      nitm2 = nit-2                                                             
      if (test(nit).eq.test(nitm2)) itest= itest+1                              
      if (itest.le.150) xlast=xbarh(1)                                          
      if (itest.le.150) go to 220                                               
c                                                                               
      nitm1 = nit-1                                                             
      check = abs(xlast - xbarh(1))                                             
c     check how big oscillation is                                              
      ier = 0                                                                   
      if (check.le.1.0e-4) ier=4                                                
      if (check.le.1.0e-5) ier=5                                                
      if (check.le.1.0e-7) ier=7                                                
      if (check.le.1.0e-9) ier=9                                                
      go to 640                                                                 
c                                                                               
c        *** reduce the simplex size ***                                        
620    icode = 1                                                                
       do 630 i=1, n1                                                           
       do 630 j=1, n                                                            
630    axx(j,i) = 5e-1*( axx(j,i)+axx(j,imin) )                                 
       go to 610                                                                
c                                                                               
c        *** terminate ***                                                      
640    if (    fmin.gt.f(imax) )  go to 660                                     
645    do 650 j=1, n                                                            
650    xbarh(j) = axx(j,imin)                                                   
       go to 690                                                                
660    fmin    = f(imax)                                                        
       go to 690                                                                
680    ier = 2                                                                  
690    itr = nit                                                                
       if ( nmin.gt.0 )  fmin = -fmin                                           
  999 continue                                                                  
c                                                                               
      end                                                                       
c echolk   last revised 12/22/87 by tjw                                         
c*echolk f77 rewrite and move from eq3nr to eqlib 12/02/87 by tjw               
      subroutine echolk(ilevel,nf,tempc,press,cdrs,cdrm,cdrg,ars,               
     $ amn,ags,xlks,xlkm,xlkg,uspec,umin,ugas,jsflag,jmflag,                    
     $ jgflag,nsb,nsq,nsq1,nrst,nst,nmt,ngt,nxt,ntpr,nsqmx1,                    
     $ narxmx,ntprmx)                                                           
c                                                                               
c     this routine prints the species and reactions that are active             
c     in the current problem, along with the log k values that                  
c     correspond to the reactions.  optionally, the coefficients                
c     of the interpolating polynomials may also be printed.                     
c                                                                               
c     input                                                                     
c       ilevel = print level switch                                             
c                  =  0  no print                                               
c                  =  1  print species and reactions only                       
c                  =  2  also print equilibrium constants                       
c                  =  3  also print the coefficients of the                     
c                        interpolating polynomials                              
c       nf = unit number of the file to write on                                
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*(*) uspec(*),umin(*),ugas(*)                                    
      character*24 unamsp                                                       
c                                                                               
      dimension cdrs(nsqmx1,*),cdrm(nsqmx1,*),cdrg(nsqmx1,*),                   
     $ ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),                               
     $ ags(narxmx,ntprmx,*),xlks(*),xlkm(*),xlkg(*),jsflag(*),                  
     $ jmflag(*),jgflag(*)                                                      
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
      if (ilevel .le. 0) go to 999                                              
c                                                                               
      write (nf,2005)                                                           
 2005 format(//' ---listing of species and reactions ---',/)                    
      if (ilevel .ge. 2) write (nf,2040) tempc,press                            
 2040 format(7x,'temperature= ',f10.3,' degrees celsius',                       
     $ /7x,'pressure= ',g13.6,' bars',/)                                        
      write (nf,2007)                                                           
 2007 format(8x,'--- strict basis species ---',/)                               
c                                                                               
      do 5 ns = 1,nsb                                                           
      if (jsflag(ns) .le. 0) then                                               
        write (nf,2009) uspec(ns)                                               
 2009   format(30x,a24)                                                         
      endif                                                                     
    5 continue                                                                  
c                                                                               
      write (nf,2010)                                                           
 2010 format(//8x,'--- aqueous species dissociation reactions ---',/)           
      do 15 nrs = 1,nrst                                                        
      ns = nrs + nsb                                                            
      if (jsflag(ns) .le. 0) then                                               
        write (nf,2050)                                                         
 2050   format(' --------------------------------------------------')           
        unamsp = uspec(ns)                                                      
        call prreac(cdrs,uspec,unamsp,nrs,nsq,nsq1,nsqmx1,nf)                   
        if (ilevel .ge. 2) write (nf,2060) xlks(nrs)                            
 2060   format(/10x,'log k= ',f12.4,/)                                          
        if (ilevel .ge. 3) then                                                 
          do 105 j = 1,ntprmx                                                   
          write (nf,2070) j,(ars(i,j,nrs), i = 1,narxmx)                        
 2070     format(/3x,'coefficients for range ',i2,/3x,3(2x,g13.6),              
     $    /3x,3(2x,g13.6),/)                                                    
  105     continue                                                              
        endif                                                                   
      endif                                                                     
   15 continue                                                                  
      write (nf,2050)                                                           
c                                                                               
      write (nf,2020)                                                           
 2020 format(/8x,'--- mineral dissolution reactions ----',/)                    
      do 20 nm = 1,nmt                                                          
      if (jmflag(nm) .le. 0) then                                               
        write (nf,2050)                                                         
        unamsp = umin(nm)                                                       
        call prreac(cdrm,uspec,unamsp,nm,nsq,nsq1,nsqmx1,nf)                    
        if (ilevel .ge. 2) write (nf,2060) xlkm(nm)                             
        if (ilevel .ge. 3) then                                                 
          do 110 j = 1,ntprmx                                                   
          write (nf,2070) j,(amn(i,j,nm), i = 1,narxmx)                         
  110     continue                                                              
        endif                                                                   
      endif                                                                     
   20 continue                                                                  
      write (nf,2050)                                                           
c                                                                               
      write (nf,2025)                                                           
 2025 format(/8x,'--- gas dissolution reactions ---',/)                         
      do 25 ng = 1,ngt                                                          
      if (jgflag(ng) .le. 0) then                                               
        write (nf,2050)                                                         
        unamsp = ugas(ng)                                                       
        call prreac(cdrg,uspec,unamsp,ng,nsq,nsq1,nsqmx1,nf)                    
        if (ilevel .ge. 2) write (nf,2060) xlkg(ng)                             
        if (ilevel .ge. 3) then                                                 
          do 115 j = 1,ntprmx                                                   
          write (nf,2070) j,(ags(i,j,ng), i = 1,narxmx)                         
  115     continue                                                              
        endif                                                                   
      endif                                                                     
   25 continue                                                                  
      write (nf,2050)                                                           
c                                                                               
  999 continue                                                                  
      end                                                                       
c elim    last revised 12/10/87 by tjw                                          
c*elim f77 rewrite 12/08/87 by tjw                                              
      subroutine elim(nse,ars,amn,ags,cdrs,cdrm,cdrg,eps100,                    
     $ nsb,nsq1,nrst,nmt,ngt,nsqmx1,narxmx,ntprmx,noutpt,nttyo)                 
c                                                                               
c     this routine rewrites reaction equations so that the auxiliary            
c     basis species with index nse and jflag = 30 is eliminated from            
c     all reactions except the one linking it with its corresponding            
c     strict basis variable.  the polynomial coefficients for computing         
c     the equilibrium coefficients are recomputed accordingly.                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),                      
     $ ags(narxmx,ntprmx,*),cdrs(nsqmx1,*),cdrm(nsqmx1,*),                      
     $ cdrg(nsqmx1,*)                                                           
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
      nrse = nse - nsb                                                          
      cdrs(nse,nrse) = cdrs(nsq1,nrse)                                          
      cdrs(nsq1,nrse) = 0.                                                      
c                                                                               
      do 120 nrs = 1,nrst                                                       
      if (nrs .eq. nrse) go to 120                                              
      if (cdrs(nse,nrs) .ne. 0.) then                                           
        stofac = cdrs(nse,nrs)/cdrs(nse,nrse)                                   
        do 105 nss = 1,nsq1                                                     
        cx = cdrs(nss,nrs) - (stofac*cdrs(nss,nrse))                            
        if (abs(cx) .le. eps100) cx = 0.                                        
        cdrs(nss,nrs) = cx                                                      
  105   continue                                                                
        do 115 j = 1,ntprmx                                                     
        if (ars(1,j,nrs) .le. 500.) then                                        
          do 110 i = 1,narxmx                                                   
          ars(i,j,nrs) = ars(i,j,nrs) - (stofac*ars(i,j,nrse))                  
  110     continue                                                              
        endif                                                                   
  115   continue                                                                
      endif                                                                     
  120 continue                                                                  
c                                                                               
      do 150 nm = 1,nmt                                                         
      if (cdrm(nse,nm) .ne. 0.) then                                            
        stofac = cdrm(nse,nm)/cdrs(nse,nrse)                                    
        do 135 nss = 1,nsq1                                                     
        cx = cdrm(nss,nm) - (stofac*cdrs(nss,nrse))                             
        if (abs(cx) .le. eps100) cx = 0.                                        
        cdrm(nss,nm) = cx                                                       
  135   continue                                                                
        do 145 j = 1,ntprmx                                                     
        if (amn(1,j,nm) .le. 500.) then                                         
          do 140 i = 1,narxmx                                                   
          amn(i,j,nm) = amn(i,j,nm) - (stofac*ars(i,j,nrse))                    
  140     continue                                                              
        endif                                                                   
  145   continue                                                                
      endif                                                                     
  150 continue                                                                  
c                                                                               
      do 170 ng = 1,ngt                                                         
      if (cdrg(nse,ng) .ne. 0.) then                                            
        stofac = cdrg(nse,ng)/cdrs(nse,nrse)                                    
        do 155 nss = 1,nsq1                                                     
        cx = cdrg(nss,ng) - (stofac*cdrs(nss,nrse))                             
        if (abs(cx) .le. eps100) cx = 0.                                        
        cdrg(nss,ng) = cx                                                       
  155   continue                                                                
        do 165 j = 1,ntprmx                                                     
        if (ags(1,j,ng) .le. 500.) then                                         
          do 160 i = 1,narxmx                                                   
          ags(i,j,ng) = ags(i,j,ng) - (stofac*ars(i,j,nrse))                    
  160     continue                                                              
        endif                                                                   
  165   continue                                                                
      endif                                                                     
  170 continue                                                                  
c                                                                               
      cdrs(nsq1,nrse) = cdrs(nse,nrse)                                          
      cdrs(nse,nrse) = 0.                                                       
c                                                                               
      end                                                                       
c elmdd    last revised 04/07/87 by tjw                                         
c*elmdd f77 rewrite 04/07/87 by tjw                                             
      subroutine elmdd(zi,zj,a,xi,jopt,el,elp,elpp,ier)                         
c                                                                               
c     input                                                                     
c       zi, zj  charges                                                         
c       a       a phi                                                           
c       xi      i                                                               
c       jopt     j option = 0/1  pitzer j / harvie j                            
c                                                                               
c     output                                                                    
c       el      e lambda (i,j)                                                  
c       elp        "           first derivative                                 
c       elpp       "           second derivative                                
c                                                                               
c     ier = 0/-1  ok/error in computing j function                              
c                                                                               
c     note -   el = elp = elpp = 0                                              
c              if zi*zj .le. 0.                                                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      ier = 0                                                                   
      el = 0.                                                                   
      elp = 0.                                                                  
      elpp = 0.                                                                 
c                                                                               
      zp = zi*zj                                                                
      if (zp .le. 0.) go to 999                                                 
c                                                                               
      sqi = sqrt(xi)                                                            
      c = 3.*zp*a                                                               
      x = 2.*c*sqi                                                              
c                                                                               
c     compute x' and x''                                                        
c                                                                               
      xp = c / sqi                                                              
      xpp = -xp / (2.*xi)                                                       
c                                                                               
c     get j, j', j'', functions of x                                            
c                                                                               
      if (jopt .eq. 0) then                                                     
c                                                                               
c       pitzer j                                                                
c                                                                               
        call pjdd(x,rj,rjp,rjpp,ier)                                            
      else                                                                      
c                                                                               
c       harvie j                                                                
c                                                                               
        call hjdd(x,rj,rjp,rjpp,ier)                                            
      endif                                                                     
c                                                                               
      if (ier .eq. 0) then                                                      
c                                                                               
c       convert derivatives to same wrt i                                       
c                                                                               
        sjp = rjp*xp                                                            
        sjpp = rjp*xpp + rjpp*xp*xp                                             
c                                                                               
        ck = zp / 4.                                                            
        ri = 1. / xi                                                            
c                                                                               
        el = ri*ck*rj                                                           
        elp = ri*(ck*sjp - el)                                                  
        elpp = ri*(ck*sjpp - 2.*elp)                                            
c                                                                               
      endif                                                                     
c                                                                               
  999 continue                                                                  
      end                                                                       
c evdata  last revised 12/12/87 by tjw                                          
c*evdata created in f77 12/09/87 by tjw                                         
      subroutine evdata(tempc,press,ars,amn,ags,ac2,awo,apress,                 
     $ aadh,abdh,abdot,aeh,apx,w,xlks,xlkm,xlkg,cco2,ch2o,al10,                 
     $ rconst,ehfac,tempk,pgrid,adh,bdh,bdot,xlkeh,aphi,bt,uspec,               
     $ usolx,jsol,iopt1,iopt4,iopg1,nrst,nst,nmt,ngt,nxt,ntpr,                  
     $ narxmx,ntprmx,iktmax,iapxmx,noutpt,nttyo)                                
c                                                                               
c     this routine evaluates thermodynamic properties as a function             
c     of temperature and pressure.                                              
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*(*) uspec(*),usolx(*)                                           
c                                                                               
      dimension ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),                      
     $ ags(narxmx,ntprmx,*),ac2(narxmx,ntprmx,*),                               
     $ awo(narxmx,ntprmx,*),apress(narxmx,*),aadh(narxmx,*),                    
     $ abdh(narxmx,*),abdot(narxmx,*),aeh(narxmx,*),apx(iapxmx,*),              
     $ w(iktmax,*),xlks(*),xlkm(*),xlkg(*),cco2(*),ch2o(*),jsol(*)              
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
c     compute the grid pressure                                                 
c                                                                               
      call evdatc(pgrid,apress,tempc,ntpr,narxmx,ntprmx)                        
      press = pgrid                                                             
c                                                                               
c     compute the debye-huckel 'a' parameter                                    
c                                                                               
      call evdatc(adh,aadh,tempc,ntpr,narxmx,ntprmx)                            
      aphi = adh*al10/3.                                                        
c                                                                               
c     compute the debye-huckel 'b' parameter                                    
c                                                                               
      call evdatc(bdh,abdh,tempc,ntpr,narxmx,ntprmx)                            
      bt = 1.2 * bdh/0.3283                                                     
c                                                                               
c     note   0.3283 = b(25c)                                                    
c                                                                               
c     if using pitzer's equations, set bt to the pitzer value                   
c                                                                               
      if (iopg1.eq.1) bt=1.2                                                    
c                                                                               
c     compute the helgeson bdot parameter                                       
c                                                                               
      call evdatc(bdot,abdot,tempc,ntpr,narxmx,ntprmx)                          
c                                                                               
c     compute the polynomial coefficients for computing gamma                   
c     of co2(aq) as a function of ionic strength                                
c                                                                               
      do 205 k=1,4                                                              
      call evdatk(prop,k,ac2,tempc,ntpr,narxmx,ntprmx)                          
      cco2(k) = prop                                                            
  205 continue                                                                  
c                                                                               
c     compute the polynomial coefficients for computing the activity            
c     of water as a function of equivalent stoichiometric ionic                 
c     strength.                                                                 
c                                                                               
      do 210 k=1,4                                                              
      call evdatk(prop,k,awo,tempc,ntpr,narxmx,ntprmx)                          
      ch2o(k) = prop                                                            
  210 continue                                                                  
c                                                                               
c     compute the log k for the 'eh' reaction                                   
c                                                                               
      call evdatc(xlkeh,aeh,tempc,ntpr,narxmx,ntprmx)                           
c                                                                               
c     compute the log k values for aqueous dissocation reactions                
c     and  mineral and gas dissolution reactions                                
c                                                                               
      call evdatr(tempc,tempk,press,pgrid,ars,amn,ags,                          
     $ xlks,xlkm,xlkg,rconst,nrst,nst,nmt,ngt,nxt,ntpr,narxmx,                  
     $ ntprmx,noutpt,nttyo)                                                     
c                                                                               
c     do temperature correction of pitzer parameters                            
c                                                                               
      if (iopg1.eq.1) call tmpcor(tempc)                                        
c                                                                               
      if (iopt4.ge.1)                                                           
     $ call wterm(apx,w,tempk,press,rconst,usolx,iopt4,jsol,nxt,                
     $ iktmax,iapxmx,noutpt,nttyo)                                              
c                                                                               
      end                                                                       
c evdatc  last revised 12/03/87 by tjw                                          
c*evdatc created in f77 12/02/87 by tjw                                         
      subroutine evdatc(prop,arr,tempc,ntpr,narxmx,ntprmx)                      
c                                                                               
c     this routine evaluates a thermodynamic property as a function of          
c     temperature, using an interpolating polynomial whose coefficients         
c     are stored in a two-dimensional array arr.  the second dimension          
c     of this array corresponds to a temperature range.  this routine           
c     this routine is identical to evdatk.f, except that in that                
c     routine the array arr is three-dimensional and its calling sequence       
c     contains an additional variable.                                          
c                                                                               
c     input                                                                     
c       arr = two dimensional array of polynomial coefficients                  
c             describing some thermodynamic function                            
c       tempc = temperature, deg celsius                                        
c       ntpr = temperature range flag                                           
c       narxmx = first dimension of the arr array, the number                   
c                of coefficients per temperature range                          
c       ntprmx = second dimension of the arr array, the number                  
c                of temperature ranges.                                         
c                                                                               
c     output                                                                    
c       prop = the calculated property                                          
c                                                                               
c     other                                                                     
c       tempc1 = boundary between the first and second temperature              
c                ranges                                                         
c       tempc2 = boundary between the second and third temperature              
c                ranges                                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension arr(narxmx,*)                                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     evaluate the polynomial                                                   
c                                                                               
      prop = 0.                                                                 
      do 10 ii = 1,narxmx                                                       
      i = narxmx + 1 - ii                                                       
      prop = arr(i,ntpr) + tempc*prop                                           
   10 continue                                                                  
c                                                                               
      end                                                                       
c evdatk  last revised 12/03/87 by tjw                                          
c*evdatk created in f77 12/02/87 by tjw                                         
      subroutine evdatk(prop,k,arr,tempc,ntpr,narxmx,ntprmx)                    
c                                                                               
c     this routine evaluates a thermodynamic property as a function of          
c     temperature, using an interpolating polynomial whose coefficients         
c     are stored in a two-dimensional array arr.  the second dimension          
c     of this array corresponds to a temperature range.  this routine           
c     this routine is identical to evdatk.c, except that in that                
c     routine the array arr is two-dimensional and its calling sequence         
c     does not contain the variabale k.                                         
c                                                                               
c     input                                                                     
c       k = index for the third dimension of array arr                          
c       arr = two dimensional array of polynomial coefficients                  
c             describing some thermodynamic function                            
c       tempc = temperature, deg celsius                                        
c       ntpr = temperature range flag                                           
c       narxmx = first dimension of the arr array, the number                   
c                of coefficients per temperature range                          
c       ntprmx = second dimension of the arr array, the number                  
c                of temperature ranges.                                         
c                                                                               
c     output                                                                    
c       prop = the calculated property                                          
c                                                                               
c     other                                                                     
c       tempc1 = boundary between the first and second temperature              
c                ranges                                                         
c       tempc2 = boundary between the second and third temperature              
c                ranges                                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension arr(narxmx,ntprmx,*)                                            
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     evaluate the polynomial                                                   
c                                                                               
      prop = 0.                                                                 
      do 10 ii = 1,narxmx                                                       
      i = narxmx + 1 - ii                                                       
      prop = arr(i,ntpr,k) + tempc*prop                                         
   10 continue                                                                  
c                                                                               
      end                                                                       
c evdatr  last revised 12/09/87 by tjw                                          
c*evdatr created in f77 12/09/87 by tjw                                         
      subroutine evdatr(tempc,tempk,press,pgrid,ars,amn,ags,                    
     $ xlks,xlkm,xlkg,rconst,nrst,nst,nmt,ngt,nxt,ntpr,narxmx,                  
     $ ntprmx,noutpt,nttyo)                                                     
c                                                                               
c     this routine evaluates equilibrium constants as a function                
c     of temperature and pressure.  note- pressure corrections off              
c     the standard grid have not yet been incorporated.                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),                      
     $ ags(narxmx,ntprmx,*),xlks(*),xlkm(*),xlkg(*)                             
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
c     compute the log k values for aqueous dissocation reactions                
c                                                                               
      do 220 nrs=1,nrst                                                         
      call evdatk(prop,nrs,ars,tempc,ntpr,narxmx,ntprmx)                        
      xlks(nrs) = prop                                                          
  220 continue                                                                  
c                                                                               
c     compute the log k values for mineral dissolution reactions                
c                                                                               
      do 230 nm=1,nmt                                                           
      call evdatk(prop,nm,amn,tempc,ntpr,narxmx,ntprmx)                         
      xlkm(nm) = prop                                                           
  230 continue                                                                  
c                                                                               
c     compute the log k values for gas dissolution reactions                    
c                                                                               
      do 250 ng=1,ngt                                                           
      call evdatk(prop,ng,ags,tempc,ntpr,narxmx,ntprmx)                         
      xlkg(ng) = prop                                                           
  250 continue                                                                  
c                                                                               
      end                                                                       
c fbassw   last revised 12/27/87 by tjw                                         
c*fbassw f77 check, ok 04/08/87 by tjw                                          
      subroutine fbassw(nse,jsort,weight,conc,ibasis,jflag,nsb,nsq,nst,         
     $ ise,wse)                                                                 
c                                                                               
c     this routine finds the optimal species for basis switching.               
c     the function here is much like that of routine fdomsp, which              
c     finds the species that dominates a mass balance.  however,                
c     this routine takes into account basis switching rules, which              
c     may sometimes not allow selection of the species which is the             
c     largest contributor to a mass balance.                                    
c                                                                               
c     input                                                                     
c       nse = aqueous species index that defines the balance                    
c       jsort = array of aqueous species indices in order of increasing         
c         concentration                                                         
c       weight = stoichiometric weighting factor                                
c       conc = array of concentrations                                          
c       (note - total = sum over ns of weight(ns) * conc(ns) )                  
c       ibasis = array of aqueous species indices defining basis                
c         switches                                                              
c       jflag = jflag array defining constraint types imposed on                
c         aqueous species                                                       
c       nsb = index of last strict basis species                                
c       nsq = index of last auxiliary basis species                             
c       nst = number of aqeuous species                                         
c     output                                                                    
c       ise = index of the aqeuous species that would represent the             
c         optimal basis switch                                                  
c       wse = weight(ise)                                                       
c                                                                               
c     note - to save time, it is assumed that the ratio of the                  
c     largest value of weight to the smallest non-zero value is                 
c     no greater than 100.                                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqleps.h"                                                  
c                                                                               
      dimension jsort(*),weight(*),conc(*),ibasis(*),jflag(*)                   
c                                                                               
      data test/100./                                                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     notes on restrictions on candidates for basis switching-                  
c       a species which is currently in the master set (even if                 
c       it has been switched in) can not be a candidate.  the variable          
c       q1 = .true. if ns is in the master set.  furthermore, a species         
c       is ineligible for consideration if it is already involved               
c       in a basis switch.  each current switch involves a pair o               
c       species, one of which is in the current master set and hence            
c       has already been eliminated from consideration by the q1 test.          
c       the other will be a data file master species that has been              
c       switched out of the active master set.  a species currently             
c       involved in a switch can be identified by the condition that            
c       ibasis(ns) is not ns.  there is one execption to this.  if              
c       a switch has already occurred, the code should be able to               
c       switch back in the data file master species.  if ns is such             
c       a species, then ibasis(ns) will be nse.                                 
c                                                                               
      c0 = 0.                                                                   
      w0 = 1.                                                                   
      p0 = 0.                                                                   
      ise = 0                                                                   
      do 15 nsi = 1,nst                                                         
      nss = nst + 1 - nsi                                                       
      ns = jsort(nss)                                                           
      w1 = weight(ns)                                                           
      c1 = conc(ns)                                                             
      if (w1 .ne. 0.) then                                                      
        q1 = ns.gt.nsq                                                          
        if (.not.q1) q1 = jflag(ns).eq.30                                       
        if (q1) then                                                            
          is = ibasis(ns)                                                       
          if (is.eq.ns .or. is.eq.nse) then                                     
            p1 = w1*c1                                                          
            if (p1 .gt. p0) then                                                
              c0 = c1                                                           
              w0 = w1                                                           
              p0 = p1                                                           
              ise = ns                                                          
              go to 15                                                          
            endif                                                               
          endif                                                                 
        endif                                                                   
      endif                                                                     
      if (c1 .le. smp100) then                                                  
        if (ns .gt. 1) go to 20                                                 
      else                                                                      
        rat = c0/c1                                                             
        if (rat .gt. test) go to 20                                             
      endif                                                                     
   15 continue                                                                  
c                                                                               
   20 continue                                                                  
      wse = w0                                                                  
      end                                                                       
c fcopya   last revised 04/10/87 by tjw                                         
c*fcopya f77 check, ok 04/10/87 by tjw                                          
      subroutine fcopya(nf1,nf2)                                                
c                                                                               
c     this routine appends the contents of the file whose unit                  
c     number is nf1 to the file whose unit number is nf2.  line                 
c     length is assumed to be 128 characters.                                   
c                                                                               
c     input                                                                     
c       nf1 = unit number of the first file.                                    
c       nf2 = unit number of the second file.  it must already be               
c         open and correctly positioned.                                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*128 uline                                                       
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      rewind nf1                                                                
      do 10 j=1,10000                                                           
      read (nf1,1000,end=20) uline                                              
 1000 format(a128)                                                              
      write (nf2,1000) uline                                                    
 1005 format(a128)                                                              
   10 continue                                                                  
   20 continue                                                                  
      end                                                                       
c fdd      last revised 11/02/87 by tjw                                         
c*fdd f77 rewrite 04/10/87 by tjw                                               
      subroutine fdd(xi,a,b,iocflg,f,fp,fpp,fgam,fphi,ier)                      
c                                                                               
c     input xi, a, b values,                                                    
c           iocflg   0/1  f(dho) / f(dhc)                                       
c                                                                               
c     compute f                                                                 
c             fp  = f'                                                          
c             fpp = f''                                                         
c             fgam = f'/2                                                       
c             fphi = (f' - f/xi) / 2                                            
c                                                                               
c     ier = 0/-1  ok/xi is .lt. zero                                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      if (xi .ge. 0.) then                                                      
        ier = 0                                                                 
      else                                                                      
        ier = -1                                                                
        go to 999                                                               
      endif                                                                     
c                                                                               
      sqi = sqrt(xi)                                                            
      v = b * sqi                                                               
      vp1 = v + 1.                                                              
      vp1sq = vp1*vp1                                                           
      alvp1 = log(vp1)                                                          
      if (iocflg .le. 0) then                                                   
c                                                                               
c       f(dho) here                                                             
c                                                                               
        fconst = -4. * a / b                                                    
        f = fconst * xi * alvp1                                                 
        fp = fconst * ((v / (2.*vp1)) + alvp1)                                  
        fpp = (-a / sqi) * ((1. + 2.*vp1) / vp1sq)                              
c                                                                               
      else                                                                      
c                                                                               
c       f(dhc) here                                                             
c                                                                               
        fcp = -6.*a                                                             
        fc = fcp/(b**3)                                                         
        fcpp = -3.*a                                                            
c                                                                               
        f = fc*(v*(v-2.) + 2.*alvp1)                                            
        fp = fcp*sqi / vp1                                                      
        fpp = fcpp / (sqi*vp1sq)                                                
c                                                                               
      endif                                                                     
c                                                                               
      fgam = fp / 2.                                                            
      fphi = (fp - f/xi) / 2.                                                   
c                                                                               
  999 continue                                                                  
      end                                                                       
c fdomsp   last revised 12/27/87 by tjw                                         
c*fdomsp f77 check, ok 04/10/87 by tjw                                          
      subroutine fdomsp(nse,jsort,weight,conc,nst,ise,wse)                      
c                                                                               
c     this routine finds the species that dominates a mass balance.             
c     it is very similar in function to routine fbassw, which finds             
c     the optimal species for a basis switch.  certain rules govern             
c     basis switching which may not allow the dominant species to               
c     be chosen for a basis switch.                                             
c                                                                               
c     input                                                                     
c       nse = aqueous species index that defines the balance                    
c       jsort = array of aqueous species indices in order of increasing         
c         concentration                                                         
c       weight = stoichiometric weighting factor                                
c       conc = array of concentrations                                          
c       nst = number of aqueous species                                         
c       (note - total = sum over ns of weight(ns) * conc(ns) )                  
c     output                                                                    
c       ise = index of the dominant aqeuous species                             
c       wse = weight(ise)                                                       
c                                                                               
c     note - to save time, it is assumed that the ratio of the                  
c     largest value of weight to the smallest non-zero value is                 
c     no greater than 100.                                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqleps.h"                                                  
c                                                                               
      dimension jsort(*),weight(*),conc(*)                                      
c                                                                               
      data test/100./                                                           
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
      c0 = 0.                                                                   
      w0 = 1.                                                                   
      p0 = 0.                                                                   
      ise = 0                                                                   
      do 15 nsi = 1,nst                                                         
      nss = nst + 1 - nsi                                                       
      ns = jsort(nss)                                                           
      w1 = weight(ns)                                                           
      c1 = conc(ns)                                                             
      if (w1 .ne. 0.) then                                                      
        p1 = w1*c1                                                              
        if (p1 .gt. p0) then                                                    
          c0 = c1                                                               
          w0 = w1                                                               
          p0 = p1                                                               
          ise = ns                                                              
          go to 15                                                              
        endif                                                                   
      endif                                                                     
      if (c1 .le. smp100) then                                                  
        if (ns .gt. 1) go to 20                                                 
      else                                                                      
        rat = c0/c1                                                             
        if (rat .gt. test) go to 20                                             
      endif                                                                     
   15 continue                                                                  
c                                                                               
   20 continue                                                                  
      wse = w0                                                                  
      end                                                                       
c flen     last revised 04/10/87 by tjw                                         
c*flen f77 rewrite 04/10/87 by tjw                                              
      subroutine flen(ust,nch,len)                                              
c                                                                               
c     find the length of a string                                               
c                                                                               
c     input  ust  ascii string                                                  
c            nch  number of characters allowed in ust                           
c                  (maximum of 80)                                              
c     output len  length of ust up to last non-blank character                  
c                                                                               
c     ust may be right-delimited by $                                           
c      if so, length is from the first character to but not                     
c             including the $.                                                  
c      else,  length is from the first character to and including               
c             the right-most non-blank.                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "impliciti.h"                                               
c                                                                               
      character*80 ust                                                          
c                                                                               
      data ublank,umark /' ','$'/                                               
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     find number of trailing blanks                                            
c                                                                               
      do 10 k=nch,1,-1                                                          
      if (ust(k:k) .ne. ublank(1:1)) go to 15                                   
   10 continue                                                                  
c                                                                               
c     all blank                                                                 
c                                                                               
      len = 0                                                                   
      go to 999                                                                 
c                                                                               
   15 if (ust(k:k) .eq. umark(1:1)) k = k-1                                     
      len = k                                                                   
c                                                                               
  999 continue                                                                  
      end                                                                       
c flpars   last revised 11/18/87 by tjw                                         
c*flpars f77 check, ok 04/10/87 by tjw                                          
      subroutine flpars                                                         
c                                                                               
c     this routines calculates                                                  
c       eps = the real*8 machine epsilon                                        
c       eps10 = 10*eps                                                          
c       eps100 = 100*eps                                                        
c       smpos = smallest positive real*8 number                                 
c       smp10 = 10*smpos                                                        
c       smp100 = 100*smpos                                                      
c       irang = int ( -log (smpos ) )                                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqleps.h"                                                  
      include "eqlpp.h"                                                   
      include "eqltxp.h"                                                  
      include "eqlun.h"                                                   
c                                                                               
      data i1/1/,j1/1/                                                          
c                                                                               
c     set loop parameters.  setting n=31 would be more efficient,               
c     but does not work because 2**31 is returned as a negative                 
c     number (at least on a ridge 32 computer).                                 
c                                                                               
      n=30                                                                      
      n1=n+1                                                                    
c                                                                               
c     find eps                                                                  
c                                                                               
      enum=1.0                                                                  
      eps=enum                                                                  
      do 10 i=1,n                                                               
      idiv=2**(n1-i)                                                            
      do 5 j=1,100                                                              
      enum=enum/idiv                                                            
      etest=i1+enum                                                             
      etest=etest-j1                                                            
      if (etest.gt.0.) then                                                     
        eps=enum                                                                
        go to 5                                                                 
      else                                                                      
        enum=eps                                                                
        go to 10                                                                
      endif                                                                     
    5 continue                                                                  
   10 continue                                                                  
c                                                                               
c     find smpos                                                                
c                                                                               
      snum=eps                                                                  
      smpos=snum                                                                
      do 20 i=1,n                                                               
      idiv=2**(n1-i)                                                            
      do 15 j=1,100                                                             
      snum=snum/idiv                                                            
      if (snum.gt.0.) then                                                      
        smpos=snum                                                              
        go to 15                                                                
      else                                                                      
        snum=smpos                                                              
        go to 20                                                                
      endif                                                                     
   15 continue                                                                  
   20 continue                                                                  
c                                                                               
      if (eps.le.smpos) then                                                    
        write (noutpl,100) eps,smpos                                            
        write (nttyol,100) eps,smpos                                            
  100   format(' * error - machine epsilon .le. smallest positive ',            
     $  'number',/7x,'eps = ',e12.5,', smpos = ',e12.5,/                        
     $  5x,'the compiler is too clever (eqlib/flpars')                          
        stop                                                                    
      endif                                                                     
c                                                                               
      eps10=10*eps                                                              
      eps100=100*eps                                                            
      smp10=10*smpos                                                            
      smp100=100*smpos                                                          
c                                                                               
c     calculate the exponent range                                              
c                                                                               
      irang=int(-log10(smpos))                                                  
c                                                                               
c     calculate natural log of 10.                                              
c                                                                               
      x10=10.                                                                   
      al10=log(x10)                                                             
c                                                                               
c     calculate parameters required for routine texp                            
c                                                                               
      xltxp=-irang                                                              
      xutxp=irang                                                               
      idum=nint(0.9*irang)                                                      
      idum=min(300,idum)                                                        
      efac=al10*idum                                                            
      yutxp=exp(efac)                                                           
c                                                                               
      end                                                                       
c gabar    last revised 10/30/87 by tjw                                         
c*gabar f77 rewrite, 07/06/87 by tjw                                            
      subroutine gabar(sigmam,conc,jsort,nst,azero,abar)                        
c                                                                               
c     this routine calculates a compositionally averaged ion size               
c     parameter (abar).  note that a sorted summation is used.                  
c                                                                               
c       input                                                                   
c         sigmam      sum(mi)                                                   
c         conc    array of concentrations  mi                                   
c         jsort   array of aqueous species indices, in order of                 
c                 increasing concentration                                      
c         nst     number of elements in array                                   
c         azero   array of ion size parameters                                  
c                                                                               
c       output                                                                  
c         abar    sum(mi*ai)/sum(mi)                                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension conc(*),jsort(*),azero(*)                                       
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sma = 0.                                                                  
      abar = 0.                                                                 
c                                                                               
c     note - this calculation presumes that either conc(1) or                   
c     azero(1) is zero, so that there is no contribution from                   
c     the solvent.                                                              
c                                                                               
      do 10 nss=1,nst                                                           
      ns=jsort(nss)                                                             
      sma = sma + conc(ns)*azero(ns)                                            
   10 continue                                                                  
      abar = sma/sigmam                                                         
c                                                                               
      end                                                                       
c gabswx  last revised 10/16/87 by tjw                                          
c*gabswx f77 check, ok 07/01/87 by tjw                                          
      subroutine gabswx(beta,ksq,ibswx)                                         
c                                                                               
c     this routine supports automatic basis switching as a means of             
c     optimization prior to newton-raphson iteration by resolving               
c     conflicts in the ibswx array.  this array could call for a                
c     given aqueous species to be switched with more than one master            
c     species.  the approach here is to use the size of the associated          
c     residual (beta) to resolve conflicts.  this routine is therefore          
c     somewhat similar to routine gbfac, which resolves conflicts               
c     affecting continued fraction calculations.                                
c                                                                               
c     input                                                                     
c       ibswx = array of indices of non-master species that are                 
c         candidates for basis switching                                        
c       beta = array of mass balance residuals                                  
c       ksq = matrix index of last auxiliary basis species                      
c     output                                                                    
c       ibswx = input array modified to remove conflicts.                       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
      include "eqleps.h"                                                  
c                                                                               
      dimension beta(*),ibswx(*)                                                
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     find the largest residual among potential basis switching cases           
c                                                                               
      kbig=0                                                                    
      bbig=0.                                                                   
      do 5 kcol=1,ksq                                                           
      if (ibswx(kcol).gt.0) then                                                
        if (beta(kcol).gt.bbig) then                                            
          kbig=kcol                                                             
          bbig=beta(kcol)                                                       
        endif                                                                   
      endif                                                                     
    5 continue                                                                  
c                                                                               
c     do basis switching only for the cases of the largest residuals            
c                                                                               
      if (bbig.gt.eps100) then                                                  
        do 7 kcol=1,ksq                                                         
        if (ibswx(kcol).gt.0) then                                              
          frac=beta(kcol)/bbig                                                  
          if (frac.le.1.e-2) ibswx(kcol)=0                                      
        endif                                                                   
    7   continue                                                                
      endif                                                                     
c                                                                               
      ksqm1=ksq-1                                                               
      do 15 krow=2,ksqm1                                                        
      isej=ibswx(krow)                                                          
      if (isej.gt.0) then                                                       
        klim=krow+1                                                             
        do 10 kcol=klim,ksq                                                     
        isj=ibswx(kcol)                                                         
        if (isj.eq.isej) then                                                   
          if (beta(krow).gt.beta(kcol)) then                                    
            ibswx(kcol)=0                                                       
          else                                                                  
            ibswx(krow)=0                                                       
            go to 15                                                            
          endif                                                                 
        endif                                                                   
   10   continue                                                                
      endif                                                                     
   15 continue                                                                  
c                                                                               
      end                                                                       
c gafscl   last revised 12/17/87 by tjw                                         
c*gafscl created in f77  12/17/87 by tjw                                        
      subroutine gafscl(cdrm,cscale,nsq,nmt,nsqmx1)                             
c                                                                               
c     compute the cscale array of affinity scaling factors.                     
c     affinity scaling is used to help choose which one of a set                
c     of supersaturated phases is the best choice to precipitate.               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension cdrm(nsqmx1,*),cscale(*)                                        
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 105 nm = 1,nmt                                                         
      cx = 0.                                                                   
      do 100 ns = 1,nsq                                                         
      cx = cx + abs(cdrm(ns,nm))                                                
  100 continue                                                                  
      cscale(nm) = cx                                                           
  105 continue                                                                  
c                                                                               
      end                                                                       
c gbdot    last revised 11/02/87 by tjw                                         
c*gbdot created in  f77  10/18/87 by tjw                                        
      subroutine gbdot(iacion,iopg5,nst,xi,xisteq,z,zsq2,azero,glgc)            
c                                                                               
c     this routine computes activity coefficients of aqueous                    
c     species using the 'bdot equation plus others' model.  the activity        
c     coefficient of charged species is calculated using an extended            
c     debye-huckel approximation due to helgeson (1969).  the activity of       
c     water and the activity coefficient of dissolved co2 (whose value is       
c     assigned to the activity coefficients of neutral nonpolar species)        
c     are special cases.  the equations for these are not actually              
c     consistent with the bdot equation, but the numerical results may be       
c     a reasonable approximation in dilute solutions.                           
c     all activity coefficients are in logarithmic form.                        
c                                                                               
c      input  iacion  index of na+ or cl-                                       
c             iopg5   option switch to replace the polynomial                   
c                     for the activity coefficient of aqeuous co2               
c                     by a bdot term                                            
c             nst     number of aqueous species in model                        
c             xi      ionic strength                                            
c             xisteq  equiv. stoich. ionic strength                             
c             z       array of charges                                          
c             zsq2    array of one-half the charge squared                      
c             azero   array of d-h ion size parameters                          
c                                                                               
c      output glgc    array of molal activity coefficients                      
c          note- glgc(1) contains the activity of water                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlpar.h"                                                  
c                                                                               
      include "eqldd.h"                                                   
      include "eqlej.h"                                                   
      include "eqlpp.h"                                                   
      include "eqlun.h"                                                   
c                                                                               
      dimension z(*),zsq2(*),azero(*),glgc(*)                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     log(aw)                                                                   
c                                                                               
      glgc(1)=0.                                                                
      if (iacion.gt.0) then                                                     
        dterm=al10*adh/(xisteq*ch2o(1)**3)                                      
        bterm=1.+ch2o(1)*sqrt(xisteq)                                           
        xjterm=bterm-2.*log(bterm)-(1./bterm)                                   
        oscoff=1.-dterm*xjterm+ch2o(2)*xisteq+ch2o(3)*xisteq**2                 
        oscoff=oscoff+ch2o(4)*xisteq**3                                         
        glgc(1)=-2.*xisteq*oscoff/(al10*om)                                     
      endif                                                                     
c                                                                               
c     note- the formula below tends to blow up in concentrated                  
c     solutions.  setting iopg5=1 replaces it by a more stable                  
c     expression.                                                               
c                                                                               
      if (iopg5 .le. 0) then                                                    
        glgco2=(cco2(1)+(cco2(2)+(cco2(3)+cco2(4)*xi)*xi)*xi)*xi                
        if (glgco2 .gt. 3.) then                                                
          write (noutpl,1000) glgco2,xi                                         
          write (nttyol,1000) glgco2,xi                                         
 1000     format(' * trouble- activity coefficient of co2(aq)',/                
     $    7x,'calculated as ',1pe12.5,' from special polynomial,',/             
     $    7x,'ionic strength= ',0pf10.5,', try setting iopg5 = 1')              
          stop                                                                  
        endif                                                                   
      else                                                                      
         glgco2=bdot*xi                                                         
      endif                                                                     
c                                                                               
c     log(gi) calc here, b dot equations                                        
c                                                                               
      xisqrt=sqrt(xi)                                                           
      art=2*adh*xisqrt                                                          
      brt=bdh*xisqrt                                                            
c                                                                               
      bdotxi=bdot*xi                                                            
      do 30 ns=2,nst                                                            
      zz=z(ns)                                                                  
      az=azero(ns)                                                              
      if (zz.ne.0.) then                                                        
        glgc(ns)  = -( (art*zsq2(ns)) / (1.0 + brt*az) ) + bdotxi               
      else                                                                      
c                                                                               
c       note- nonpolar neutral solute species are marked on the data            
c       file by a negative value of the 'ion size'                              
c                                                                               
        if (az .ge. 0.) then                                                    
c                                                                               
c         for polar neutral solutes                                             
c                                                                               
          glgc(ns)=0.                                                           
        else                                                                    
c                                                                               
c         for nonpolar neutral solutes                                          
c                                                                               
          glgc(ns)=glgco2                                                       
        endif                                                                   
      endif                                                                     
   30 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c gbfac   last revised 12/27/87 by tjw                                          
c*gbfac f77 check, ok 07/01/87 by tjw                                           
      subroutine gbfac(nfac,beta,efac,ksq,bfac)                                 
c                                                                               
c     this routine gets the bfac array, which is used in making                 
c     continued fraction corrections.  it resolves conflicts when the           
c     same aqueous species dominates more than one mass balance (the            
c     species dominating a given mass balance is determined by routine          
c     fdomsp).  the continued fraction algorithm can be applied                 
c     to the master species associated with only one mass balance in            
c     such a set, otherwise, oscillatory behavior will occur.                   
c     in each set of mass balances with a common dominant species,              
c     this routine finds the mass balance with the greatest residual and        
c     completes the calculation of its bfac factor by doing the                 
c     appropriate exponentiation.  it sets bfac to unity for the others         
c     in the set.                                                               
c                                                                               
c     input                                                                     
c       nfac = array of indices of dominant aqueous species                     
c       beta = array of mass balance residuals                                  
c       efac = array of corresponding reciprocal stoichiometric weights         
c       ksq = matrix index of last auxiliary basis species                      
c     output                                                                    
c       bfac = array (in terms of matrix indexing) of the quantity              
c         used in making a continued fraction correction                        
c         ( e.g., conc (new) = conc (old) / bfac ).  in the absence of          
c         a conflict, bfac = ( beta + 1. )**efac.                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension nfac(*),beta(*),efac(*),bfac(*)                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 5 krow = 1,ksq                                                         
      bfac(krow) = beta(krow) + 1.                                              
    5 continue                                                                  
c                                                                               
      ksqm1 = ksq - 1                                                           
      do 15 krow = 1,ksqm1                                                      
      if (nfac(krow) .gt. 0) then                                               
        klim = krow + 1                                                         
        do 10 kcol = klim,ksq                                                   
        if (nfac(kcol) .eq. nfac(krow)) then                                    
          if (bfac(krow) .gt. bfac(kcol)) then                                  
            nfac(kcol) = 0                                                      
          else                                                                  
            nfac(krow) = 0                                                      
            go to 15                                                            
          endif                                                                 
        endif                                                                   
   10   continue                                                                
      endif                                                                     
   15 continue                                                                  
c                                                                               
      do 20 krow = 1,ksq                                                        
      if (nfac(krow) .gt. 0) then                                               
        bfac(krow) = bfac(krow)**efac(krow)                                     
      else                                                                      
        bfac(krow) = 1.                                                         
      endif                                                                     
   20 continue                                                                  
c                                                                               
      end                                                                       
c gcdrst   last revised 12/22/87 by tjw                                         
c*gcdrst created in f77  12/16/87 by tjw                                        
      subroutine gcdrst(cdrs,cdrm,cdrg,cdrst,cdrmt,cdrgt,                       
     $ nsb,nsq,nsq1,nrst,nmt,ngt,nsqmx1)                                        
c                                                                               
c     this routine computes the cdrst, cdrmt, and cdrgt arrays.                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension cdrs(nsqmx1,*),cdrm(nsqmx1,*),cdrg(nsqmx1,*),                   
     $ cdrst(*),cdrmt(*),cdrgt(*)                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     programming note - if ns .eq. nsb in the following do loops               
c     there should be no contribution to the sum accumulated in the             
c     variable cx.  the contribution is effectively subtracted before           
c     each do loop in order to avoid an if test inside the loop.                
c                                                                               
      do 105 nrs = 1,nrst                                                       
      cx = -cdrs(nsb,nrs)                                                       
      do 100 ns = 2,nsq1                                                        
      cx = cx + cdrs(ns,nrs)                                                    
  100 continue                                                                  
      cdrst(nrs) = cx                                                           
  105 continue                                                                  
c                                                                               
      do 115 nm = 1,nmt                                                         
      cx = -cdrm(nsb,nm)                                                        
      do 110 ns = 2,nsq                                                         
      cx = cx + cdrm(ns,nm)                                                     
  110 continue                                                                  
      cdrmt(nm) = cx                                                            
  115 continue                                                                  
c                                                                               
      do 125 ng = 1,ngt                                                         
      cx = -cdrg(nsb,ng)                                                        
      do 120 ns = 2,nsq                                                         
      cx = cx + cdrg(ns,ng)                                                     
  120 continue                                                                  
      cdrgt(ng) = cx                                                            
  125 continue                                                                  
c                                                                               
      end                                                                       
c gcoeff   last revised 11/25/87 by tjw                                         
c*gcoeff f77 rewrite, 07/01/87 by tjw                                           
      subroutine gcoeff(conc,z,zsq2,azero,hydn,concbs,glgc,xi,xisteq,           
     $ dshm,tempc,press,jsflag,jsort,iacion,nsb,nst,nsq,nhydr,nchlor)           
c                                                                               
c     this routine computes activity coefficients of aqueous                    
c     species using various models.  the exact model used is determined         
c     by the variable iopg1-                                                    
c                                                                               
c        -1  davies equation                                                    
c         0  b-dot equation plus others                                         
c         1  pitzer equations (standard).  the use or non-use of                
c              higher order electrostatic terms is determined by                
c              the data file.                                                   
c         2  not used                                                           
c         3  dh(o/c)a + e-l hydr. theory                                        
c         4  dh(o/c)a hydration theory                                          
c         5  hkf equations - part iv                                            
c                                                                               
c     after their calculation, activity coefficients may be normalized          
c     to make them consistent with a given ph convention.  this is              
c     determined by the variable iopg2-                                         
c                                                                               
c        -1  no normalization                                                   
c         0  normalization to the nbs ph scale                                  
c         1  normaliation to the rational ph scale                              
c                                                                               
c     logical flags such as qhydth only mark generic classes of                 
c     models and may be set to .true. for more than one value of                
c     iopg1.                                                                    
c                                                                               
c      input  iacion  index of na+ or cl-                                       
c             tempc   temperature,deg celsius                                   
c             press   pressure, bars                                            
c             jsflag  array of status switches, aqueous species                 
c             nsb     index of o2(g)                                            
c             nst     number of aqueous species in model                        
c             conc    array of concentrations                                   
c             concbs  array of master species total concentrations              
c             jsort   array of aqueous species indices, arranged in             
c                     order of increasing concentration                         
c             z       array of charges                                          
c             zsq2    array of factors equal to one-half the charge             
c                     squared                                                   
c             xi      ionic strength                                            
c             xisteq  equiv. stoich. ionic strength                             
c             azero   array of d-h ion size parameters                          
c             hydn    array of hydration numbers                                
c             ielam  -1/0/1  omit e-lambda / include:e-lambda terms /           
c     include:absolute e-lambda terms                                           
c             nsq     number of aqueous master species                          
c             nhydr     ns index of the hydrogen ion                            
c             nchlor    ns index of the chloride ion                            
c                                                                               
c      output glgc    array of molal activity coefficients                      
c          note- glgc(1) contains the activity of water                         
c                                                                               
c      other major variables                                                    
c                                                                               
c             iopg1   activity coefficient switch                               
c             iopg6   0/1  pitzer j / harvie j function                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlpar.h"                                                  
c                                                                               
      include "eqldd.h"                                                   
      include "eqlelm.h"                                                  
      include "eqlej.h"                                                   
      include "eqlgp.h"                                                   
      include "eqlpmu.h"                                                  
      include "eqlpmx.h"                                                  
      include "eqlpp.h"                                                   
      include "eqlpsa.h"                                                  
      include "eqlpsl.h"                                                  
      include "eqlslm.h"                                                  
      include "eqlsz.h"                                                   
      include "eqlsza.h"                                                  
      include "eqlhkf.h"                                                  
      include "eqlun.h"                                                   
c                                                                               
      dimension jsflag(*),conc(*),jsort(*),z(*),zsq2(*),azero(*),               
     $ hydn(*),glgc(*),concbs(*)                                                
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (iopg1 .le. 0) then                                                    
        if (iopg1 .eq. 0) then                                                  
c                                                                               
c                            *------------------------------------------        
c                            * b dot equations                                  
c                            *------------------------------------------        
c                                                                               
          call gbdot(iacion,iopg5,nst,xi,xisteq,z,zsq2,azero,glgc)              
        elseif (iopg1 .eq. -1) then                                             
c                                                                               
c                            *------------------------------------------        
c                            * the davies equation                              
c                            *------------------------------------------        
c                                                                               
          glgc(1)=0.0                                                           
          xisqrt = sqrt(xi)                                                     
ctom                                                                            
c         test- make the davies equation closer numerically to the              
c         bdot equation                                                         
c         factor = ( xisqrt / ( 1. + ( 1.5 * xisqrt ) ) ) - 0.07 * xi           
cend                                                                            
          factor = ( xisqrt / ( 1. + xisqrt ) ) - 0.2 * xi                      
          factor = - 2 * adh * factor                                           
          do 201 ns=2,nst                                                       
          glgc(ns) = zsq2(ns) * factor                                          
  201     continue                                                              
        else                                                                    
c                                                                               
c                            *------------------------------------------        
c                            * other simple equations                           
c                            *------------------------------------------        
c                                                                               
          write (noutpl,205)                                                    
          write (nttyol,205)                                                    
  205     format(' * error- bad activity coefficient option encountered',       
     $     ' (eqlib/gcoeff)')                                                   
          stop                                                                  
        endif                                                                   
      endif                                                                     
c                                                                               
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -         
c                                                                               
      if (iopg1.eq.3 .or. iopg1.eq.4) then                                      
c                                                                               
c                             *----------------------------------------         
c                             * hydration theory here                           
c                             *----------------------------------------         
c                                                                               
c     get e-lambda, deriv arrays, e-lambda sums                                 
c                                                                               
      call gelam(xi,aphi,iopg6,ielam,izm,elam)                                  
      call gesum(elam,nst,conc,z,xi,esum,elpsum,dum)                            
      call gselm(elam,nst,jsflag,conc,z,izoff,selm,selmp)                       
c                                                                               
c     get sigma m (sigmam) and the average ion size (abar)                      
c                                                                               
      call gsigm(conc,jsort,nst,sigmam)                                         
      call gabar(sigmam,conc,jsort,nst,azero,abar)                              
c                                                                               
c     get hydration theory b (b-gamma * azero)                                  
c                                                                               
      b = bdh*abar                                                              
c                                                                               
c     get f terms                                                               
c                                                                               
      call fdd(xi,aphi,b,iopg4,f,fp,fpp,fg,fphi,ier)                            
      if (ier .ne. 0) then                                                      
        write (noutpl,1011)                                                     
        write (nttyo,1011)                                                      
 1011   format(' * error - error, bad fdd call (eqlib/gcoeff)')                 
        stop                                                                    
      endif                                                                     
c                                                                               
      alm = log(1. + sigmam/(dshm*om))                                          
      aldshm = log(dshm)                                                        
      dshmsq = dshm*dshm                                                        
c                                                                               
      lnaw = -(alm + 2.*xi*fphi/om) - (esum/(om*dshm))                          
      glgc(1) = lnaw / al10                                                     
      fqt = (2.*xi*fp - 3.*f) / (abar*sigmam/dshm)                              
c                                                                               
      do 28 ns=2,nst                                                            
      glgc(ns) = 0.                                                             
      if (ns .eq. nsb) go to 28                                                 
      if (jsflag(ns) .ne. 0) go to 28                                           
      zz = z(ns)                                                                
      aterm = 0.                                                                
c                                                                               
c     get esum                                                                  
c                                                                               
      iz = nint(zz) + izoff                                                     
      elsum = selm(iz)                                                          
      lnacti = -alm - aldshm - hydn(ns)*lnaw +                                  
     $ zsq2(ns)*(fp + elpsum/dshmsq) + 2.* elsum/dshm                           
      aterm = fqt*(azero(ns) - abar)                                            
      glgc(ns) = (lnacti + aterm) / al10                                        
   28 continue                                                                  
c                                                                               
      endif                                                                     
c                                                                               
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -         
c                             *----------------------------------------         
c                             * standard pitzer equations here                  
c                             *----------------------------------------         
c                                                                               
      if (iopg1.eq.1) then                                                      
c                                                                               
c     get e-lambda, deriv arrays, e-lambda sums                                 
c                                                                               
      call gelam(xi,aphi,iopg6,ielam,izm,elam)                                  
      call gesum(elam,nst,conc,z,xi,esum,elpsum,dum)                            
c                                                                               
c     get sum(e-lambda*mj)                                                      
c                                                                               
      call gselm(elam,nst,jsflag,conc,z,izoff,selm,selmp)                       
      call gsigm(conc,jsort,nst,sigmam)                                         
c                                                                               
c     get f terms                                                               
c                                                                               
      call fdd(xi,aphi,bt,iopg4,f,fp,fpp,fg,fphi,ier)                           
      if (ier .ne. 0) then                                                      
        write (noutpl,1010)                                                     
        write (nttyo,1010)                                                      
 1010   format(' * error - error, bad fdd call (eqlib/gcoeff)')                 
        stop                                                                    
      endif                                                                     
c                                                                               
c     get s-lambda's and s-lambda sum                                           
c                                                                               
      call gslam(xi,nslm,pslm,nalpha,pslam)                                     
      call gssum(xi,nslm,nslmx,pslam,conc,ssum)                                 
c                                                                               
c     get mu sum                                                                
c                                                                               
      call gmsum(nmu,nmux,pmu,conc,musum)                                       
c                                                                               
      glgc(1) = -(1./(al10*om)) *                                               
     $  (sigmam + xi*fp - f + esum + ssum + 2.0*musum)                          
c                                                                               
c     log(gi) calc here, standard pitzer equations                              
c                                                                               
c     get s-lambda prime sum . . slpsum                                         
c                                                                               
      call gsdsm(nslm,nslmx,pslam,conc,slpsum,dum)                              
c                                                                               
      dum3 = (fp + elpsum + slpsum) / 2.                                        
c                                                                               
      do 40 ns=2,nst                                                            
      glgc(ns) = 0.                                                             
      if (ns .eq. nsb) go to 40                                                 
      if (jsflag(ns) .ne. 0) go to 40                                           
      zz = z(ns)                                                                
c                                                                               
c     get e-lambda sum . . elsum                                                
c                                                                               
      iz = nint(zz) + izoff                                                     
      elsum = selm(iz)                                                          
c                                                                               
c     get s-lambda sum . . slsum                                                
c                                                                               
      call gsgsm(ns,nsxi,nsxx,pslam,conc,slsum,dum)                             
c                                                                               
c     get mu sum . . musum                                                      
c                                                                               
      call gmdsm(ns,nmxi,nmxx,pmu,conc,musum)                                   
c                                                                               
      glgc(ns) = (2*zsq2(ns)*dum3 + 2.*(elsum + slsum) + 1. * musum)            
     $  / al10                                                                  
   40 continue                                                                  
      endif                                                                     
c                                                                               
c - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -         
c                                                                               
c                                                                               
c                             *----------------------------------------         
c                             * hkf equations (part iv) here                    
c                             *----------------------------------------         
c                                                                               
      if (iopg1.eq.5) then                                                      
c                                                                               
c              set up - qck is logical flag for error rept. writes              
c                                                                               
      qck = .false.                                                             
c              calculate sum of molalities                                      
      call gsigm(conc,jsort,nst,sigmam)                                         
c                                                                               
c     note gabar call is commmented out pending a revision of                   
c     how the hkf model treats the average ion size.                            
c                                                                               
c     call gabar(sigmam,conc,jsort,nst,azero,abar)                              
c              calculate moles of salts                                         
      call molslt(z,concbs,nsq)                                                 
c              calculate hkfaz -- azero for hkf equations                       
      call azpt4(nst,nsq,nsb,sigmam,conc,azero,z,concbs)                        
c              calculate ionic strength variables                               
      call gxisto(zsq2,conc,nsq,concbs,xistoc)                                  
c                                                                               
c                                                                               
      capgam= -tlg(1+omi*sigmam)                                                
      osmgam= capgam/(omi*sigmam)                                               
      sltsum = 0.                                                               
      osmsum = 0.                                                               
      xitmp=sqrt(xi)                                                            
      acube=hkfaz**3                                                            
      bb = bdh**3                                                               
      xx = xitmp**3                                                             
      caplam= 1.0 + hkfaz*bdh*xitmp                                             
      lamtm= caplam- (1.0/caplam) - (2.0*log(caplam))                           
      sigma= (3.0*lamtm)/(acube*bb*xx)                                          
c                                                                               
c              do third term for a.c. of species                                
c                                                                               
      call kisum(conc,xistoc,z,nst,sum1)                                        
c                                                                               
c              debug writes                                                     
c                                                                               
      if (qck) then                                                             
        write(noutpl,2000)                                                      
 2000   format(' xi,xistoc,sigmam,sum1,hkfaz,caplam,capgam')                    
        write(noutpl,2001) xi,xistoc,sigmam,sum1,hkfaz,caplam,capgam            
 2001   format(7(1x,g13.5))                                                     
c                                                                               
        write(noutpl,2002)                                                      
 2002   format(' ns,conc,concbs ')                                              
        do 2004 i=1,nst                                                         
        write(noutpl,2003) i,conc(i),concbs(i)                                  
 2003   format(1x,i3,2(1x,g13.5))                                               
 2004   continue                                                                
c                                                                               
        write(noutpl,2005)                                                      
 2005   format(' hypothetical salt solution ',/,                                
     $         ' cat    an    catsf   ansf       mol. salt')                    
        write(noutpl,2006) ((islt(jj,ii),jj=1,4),mslt(ii),ii=1,icnt)            
 2006   format(4(5x,i5),5x,g13.5)                                               
        write(noutpl,2007)                                                      
 2007   format(' major loop in gcoeff - a.c. and logaw ',/,                     
     $         ' ns     z      anum     omega    sum2  ',                       
     $         ' partb    glgc   sltsum   third    osmsum')                     
      endif                                                                     
c                                                                               
c              loop through all species in model                                
c                                                                               
c -----------  main loop for glgc -----------------                             
      do 90 ns = 2,nst                                                          
      if (jsflag(ns) .ne. 0) go to 90                                           
      zdum=z(ns)                                                                
      anum= adh*zdum*zdum*xitmp                                                 
c                                                                               
c              get omega                                                        
      call omega4(ns,omegns,zdum,ierr)                                          
c                                                                               
c              calculate fourth term in a.c. of species                         
c                                                                               
      call kilsum(ns,indx2,ibjius,conc,bji,sum2)                                
c                                                                               
c              calculate activity coefficient of species                        
c                                                                               
      partb=omegns*xi*sum1                                                      
      if (abs(zdum) .le. 1.0e-10) then                                          
        glgc(ns) = capgam + partb + sum2                                        
      else                                                                      
        glgc(ns) = -(anum/caplam) + capgam + partb + sum2                       
      endif                                                                     
c                                                                               
c              calculate sum over salts of (nu sub j,k * msub k)                
      sltsum = 0.0                                                              
      do 85 i=1,icnt                                                            
      salmol=mslt(i)                                                            
      if (ns .eq. islt(1,i)) then                                               
        nu = islt(3,i)                                                          
        go to 80                                                                
      elseif (ns .eq. islt(2,i)) then                                           
        nu = islt(4,i)                                                          
        go to 80                                                                
      else                                                                      
        go to 85                                                                
      endif                                                                     
   80 continue                                                                  
      sltsum=sltsum+nu*salmol                                                   
   85 continue                                                                  
c                                                                               
c              major sum for osmotic coefficient                                
      third = anum*sigma/3.0                                                    
      osmsum=osmsum+sltsum*(third+osmgam-(xi*omegns*sum1+sum2)/2.0)             
c                                                                               
c              debug writes                                                     
c                                                                               
      if (qck) then                                                             
        write(noutpl,2008) ns,znum,anum,omegns,sum2,partb,glgc(ns),             
     $                     sltsum,third,osmsum                                  
 2008   format(1x,i3,6(1x,g10.4),'*',3(1x,g10.4))                               
       endif                                                                    
c                                                                               
   90 continue                                                                  
c                                                                               
c              calculate log(activity water) -- base 10                         
c                                                                               
        osmcof = -al10*osmsum/sigmam                                            
        glgc(1) = -(osmcof*sigmam*omi)/al10                                     
c                                                                               
        if (qck) then                                                           
          write(noutpl,9000) osmcof,glgc(1)                                     
 9000     format('osmcof,glgc(1) = ',2(x,e13.5))                                
        endif                                                                   
      endif                                                                     
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
c     make activity coefficients consistent with a specified ph                 
c     scale                                                                     
c                                                                               
c       iopg2 = -1   use activity coefficients as is                            
c              = 0   make consistent with modified nbs ph scale                 
c              = 2   make consistent with the rational ph scale                 
c                                                                               
      if (iopg2 .eq. 0)  then                                                   
        call nbsgam(glgnbs,xi,adh,nchlor)                                       
        delglg = glgnbs - glgc(nchlor)                                          
        call gcscal(glgc,z,delglg,nchlor,nst)                                   
      endif                                                                     
c                                                                               
      if (iopg2 .eq. 1) then                                                    
        delglg = - glgc(nhydr)                                                  
        call gcscal(glgc,z,delglg,nhydr,nst)                                    
      endif                                                                     
c                                                                               
  999 continue                                                                  
      end                                                                       
c gcscal   last revised 11/20/87 by tjw                                         
c*gcscal created in f77 11/19/87 by tjw                                         
      subroutine gcscal(glgc,z,delglg,nref,nst)                                 
c                                                                               
c     this routine rescales the activity coefficients to make them              
c     consistent with a given ph scale, which has been used to                  
c     define the correction parameter delglg.                                   
c                                                                               
c     input                                                                     
c       z = array of charges of aqeuous species                                 
c       delglg = log gamma of the reference ion (new scale) -                   
c                log gamma of the reference ion (old scale) -                   
c       nref = the ns index of the reference ion                                
c                                                                               
c     input/output                                                              
c       glgc = array of activity coefficients                                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlun.h"                                                   
c                                                                               
      dimension glgc(*),z(*)                                                    
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      zref = z(nref)                                                            
      do 10 ns = 2,nst                                                          
      zz = z(ns)                                                                
      if (zz .ne. 0.) glgc(ns) = glgc(ns) + ( zz/zref ) * delglg                
   10 continue                                                                  
c                                                                               
      end                                                                       
c gcsts    last revised 12/15/87 by tjw                                         
c*gcsts created in f77 12/14/87 by tjw                                          
      subroutine gcsts(csts,cdrs,cstor,ibasis,jsflag,jflag,                     
     $ nhydr,nct,nsb,nsb1,nsq,nsq1,nst,nsqmax,nsqmx1,nstmax,qbassw)             
c                                                                               
c     this routine computes stoichiometric factors which relate                 
c     each aqueous species to the nse-th basis species.  these                  
c     stoichiometric factors permit calculation of sums which                   
c     correspond to 'total' masses or concentrations of master                  
c     species that have physical meaning.  if we just used the                  
c     ration -cdrs(nse,nrs)/cdrs(nsq1,nrs), using the definition                
c     of 'mark reed' components, the resulting calculated 'total'               
c     concentration of, say, chloride, might be non-physical; i.e.,             
c     it would not correspond to the quantity determined in an                  
c     analytical laboratory.  to see this, consider the effect of               
c     switching hgcl3- into the basis for hg++.  before the switch,             
c     we have-                                                                  
c       hg++ (master species)                                                   
c       cl- (master species)                                                    
c       hgcl3- = hg++ + 3 cl-                                                   
c       hgbr3- = hg++ + 3 br-                                                   
c     after the switch, we have                                                 
c       hgcl3- (master species)                                                 
c       cl- (master species)                                                    
c       hg++ + 3 cl- = hgcl3-                                                   
c       hgbr3- + 3 cl- = hgcl3- + 3 br-                                         
c     if we use the 'mark reed' component approach, we find that                
c     when we go to calculate total cl-, hgbr3- has a stoichiometric            
c     factor of -3!.  with the approach used in this routine, the               
c     three chlorides produced by the formation of hgcl3- are                   
c     accounted for and hgbr3- then has a stoichiometric factor                 
c     of zero.                                                                  
c                                                                               
c     the use of the cstor array in this routine is only temporary              
c     until this routine, extended to produce parallel stoichiometric           
c     factors for minerals and gases, is applied in eq6.  presently             
c     that code caclulates mass balances in terms of stoichiometric             
c     numbers describing elemental composition (e.g., the cess, cemn,           
c     and cegs arrays).  the cstor array presently just provides a              
c     temporary matchup so that eq3nr can write a pickup file that              
c     is consistent with the present version of eq6.  once that is              
c     no longer necessary, the stoichiometric factors for h2o and h+            
c     should be calculated the same as for the other master species.            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension csts(nsqmax,*),cdrs(nsqmx1,*),cstor(nstmax,2),                  
     $ ibasis(*),jsflag(*),jflag(*)                                             
c                                                                               
c------------------------------------------------------------------             
c                                                                               
      do 100 nse = 1,nsqmax                                                     
c                                                                               
      do 10 ns = 1,nst                                                          
      csts(nse,ns) = 0.                                                         
   10 continue                                                                  
c                                                                               
c     get stoichiometric factors for h2o (o--)                                  
c                                                                               
      if (nse .eq. 1) then                                                      
        do 15 ns = 1,nst                                                        
        is=ibasis(ns)                                                           
        csts(nse,ns) = cstor(is,1)                                              
   15   continue                                                                
        go to 100                                                               
      endif                                                                     
c                                                                               
c     get stoichiometric factors for h+ (h+)                                    
c                                                                               
      if (nse .eq. nhydr) then                                                  
        do 20 ns = 1,nst                                                        
        is=ibasis(ns)                                                           
        csts(nse,ns) = cstor(is,2)                                              
   20   continue                                                                
        go to 100                                                               
      endif                                                                     
c                                                                               
c     compute stoichiometric factors for other master species                   
c                                                                               
      csts(nse,nse) = 1.                                                        
c                                                                               
      if (qbassw) then                                                          
        do 25 ns = 1,nct                                                        
        nsi = ibasis(ns)                                                        
        if (ns.ne.nsi .and. ns.ne.nse) then                                     
          nrsi = nsi - nsb                                                      
          csts(nse,ns) = -cdrs(nse,nrsi)/cdrs(ns,nrsi)                          
        endif                                                                   
   25   continue                                                                
      endif                                                                     
c                                                                               
c     compute stoichiometric factors for species not in the strict              
c     basis.                                                                    
c                                                                               
      do 35 ns = nsb1,nst                                                       
      nrs = ns - nsb                                                            
      if (jsflag(ns) .gt. 0) go to 35                                           
      if (ns .lt. nsq) then                                                     
        if (jflag(ns) .ne. 30) go to 35                                         
      endif                                                                     
      csum = -cdrs(nse,nrs)/cdrs(nsq1,nrs)                                      
c                                                                               
      if (qbassw) then                                                          
        do 30 nsg = 2,nsq                                                       
        cx1a = cdrs(nsg,nrs)                                                    
        if (cx1a .ne. 0.) then                                                  
          nsi = ibasis(nsg)                                                     
          if (nsg.ne.nsi .and. nsg.ne.nse) then                                 
            nrsi = nsi - nsb                                                    
            cx1b = cdrs(nse,nrsi)                                               
            if (cx1b .ne. 0.) then                                              
              cx3a = -cx1a/cdrs(nsq1,nrs)                                       
              cx3b = -cx1b/cdrs(nsg,nrsi)                                       
              csum = csum + cx3a*cx3b                                           
            endif                                                               
          endif                                                                 
        endif                                                                   
   30   continue                                                                
      endif                                                                     
c                                                                               
      csts(nse,ns) = csum                                                       
   35 continue                                                                  
c                                                                               
  100 continue                                                                  
c                                                                               
      end                                                                       
c gdd      last revised 07/24/87 by rmm                                         
c*gdd f77 rewrite, 07/06/87 by tjw                                              
      subroutine gdd(xi,nax,g,gp,gpp,ier)                                       
c                                                                               
c     input                                                                     
c       xi  real value of i                                                     
c       nax  index to alpha pair array palpha                                   
c                                                                               
c     compute 6 g values, in sets of 2 -                                        
c                                                                               
c       g(1) = g(i,a1)    a1 = palpha(1,nax)                                    
c         2  = g(i,a2)    a2 = palpha(2,nax)                                    
c                                                                               
c       gpx(1) = dg(x)/dx, where x = a1*sqrt(i)                                 
c           2  = as above, but with x = a2*sqrt(i)                              
c                                                                               
c       gp(1)  = dg(i,a1)/di = dg(x)/dx * dx/di, where x = a1*sqrt(i)           
c          2   = dg(i,a2)/di = as above, but with x = a2*sqrt(i)                
c                                                                               
c       gppx(1) = d2g(x)/dx, where x = a1*sqrt(i)                               
c            2    as above, but with x = a2*sqrt(i)                             
c                                                                               
c       gpp(1) = d2g(i,a1)/di = d2g(x)/dx2 * dx/di + dg(x)/dx * d2x/di2,        
c              = dxdi( d2g(x)/dx2*dx/di - (dg(x)/dx)/(2i) ),                    
c                                  where x = a1*sqrt(i)                         
c           2  = d2g(i,a2)/di = as above, but with x = a2*sqrt(i)               
c                                                                               
c     note-                                                                     
c      d2x/di2 = -(dx/di)/2i                                                    
c     note -                                                                    
c      a1=0 implies g(i,a1) = g'(i,a1) = g''(i,a1) = 0                          
c      a2=0   "         a2         a2          a2  = 0                          
c                                                                               
c     ier = 0/-1  ok/xi out of range                                            
c     xi must be .gt. 0. and .le. 50.                                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
      include "eqlpar.h"                                                  
c                                                                               
      include "eqlpal.h"                                                  
c                                                                               
      dimension g(2),gp(2),gpp(2),gpx(2),gppx(2)                                
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      ier = 1                                                                   
c                                                                               
      if (xi .le. 0. .or. xi .gt. 50.) go to 999                                
      ier = 0                                                                   
      fac=1./(2.*xi)                                                            
c                                                                               
      do 40 k=1,2                                                               
      g(k) = 0.                                                                 
      gpx(k)=0.                                                                 
      gppx(k)=0.                                                                
      gp(k) = 0.                                                                
      gpp(k) = 0.                                                               
      a = palpha(k,nax)                                                         
c                                                                               
      if (a .ne. 0.) then                                                       
c                                                                               
        x = a * sqrt(xi)                                                        
        xsq = x*x                                                               
        emx = exp(-x)                                                           
c                                                                               
        g(k) = (2./xsq) * (1. - emx*(1. + x))                                   
c                                                                               
c       calculate derivatives with respect to x                                 
c                                                                               
        gdump = (-2./(xsq*x)) * (2. - emx*(2. + x*(2. + x)))                    
        gpx(k)=gdump                                                            
        gdumpp = (-2./(xsq*xsq))                                                
     $  * (-6. + emx*(6. + x*(6. + x*(3. + x))))                                
        gppx(k)=gdumpp                                                          
c                                                                               
c       calculate derivatives with respect to xi.                               
c                                                                               
        dxdi = a/(2.*sqrt(xi))                                                  
        gpp(k) = dxdi * (gdumpp*dxdi - fac*gdump)                               
        gp(k) = dxdi * gdump                                                    
      endif                                                                     
c                                                                               
   40 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c gelam    last revised 07/06/87 by tjw                                         
c*gelam f77 rewrite, 07/06/87 by tjw                                            
      subroutine gelam(xi,aphi,iopthp,ielam,izm,elam)                           
c                                                                               
c     input  xi       i value                                                   
c            aphi     adh/3                                                     
c            iopthp   0/1  use pitzer j / use harvie j                          
c            ielam  -1/0/1  set e-lambda values = 0 / compute them              
c                     in the standard way / compute them in the                 
c                     absolute way                                              
c            izm      index to zero charge                                      
c     output elam     array of e-lambda and                                     
c                                  '    first derivative                        
c                                  '    second derivative                       
c                     corresponding to all legal charge pairs                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlun.h"                                                   
c                                                                               
      dimension pelam(3,100),elam(3,10,10)                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 40 j=1,izm                                                             
c                                                                               
c     associated charge value                                                   
c                                                                               
      zj = real(j)                                                              
c                                                                               
      do 30 i=1,izm                                                             
      ij = i*j                                                                  
      if (ielam .lt. 0) then                                                    
c                                                                               
c       skip e-lambda's here                                                    
c                                                                               
        pelam(1,ij) = 0.                                                        
        pelam(2,ij) = 0.                                                        
        pelam(3,ij) = 0.                                                        
        elam(1,i,j) = 0.                                                        
        elam(2,i,j) = 0.                                                        
        elam(3,i,j) = 0.                                                        
      else                                                                      
c                                                                               
c       associated charge value                                                 
c                                                                               
        zi = real(i)                                                            
c                                                                               
c       reduce to triangular set                                                
c                                                                               
        if (i .ge. j) then                                                      
c                                                                               
c         get e-lambda's and derivs                                             
c                                                                               
          call elmdd(zj,zi,aphi,xi,iopthp,el,elp,elpp,ier)                      
          if (ier .ne. 0) then                                                  
            write (noutpl,1010)                                                 
            write (nttyol,1010)                                                 
 1010       format(' * error in e-lambda calc (eqlib/gelam)')                   
            write(noutpl,1012) zj,zi,xi                                         
            write(nttyol,1012) zj,zi,xi                                         
 1012       format(10x,'zj=',f6.0,2x,'zi=',f6.0,2x,'xip=',e15.5)                
            stop                                                                
          endif                                                                 
          pelam(1,ij) = el                                                      
          pelam(2,ij) = elp                                                     
          pelam(3,ij) = elpp                                                    
        endif                                                                   
      endif                                                                     
c                                                                               
   30 continue                                                                  
c                                                                               
   40 continue                                                                  
c                                                                               
      if (ielam .le. -1) go to 999                                              
c                                                                               
c     compute elam values                                                       
c                                                                               
      do 70 j=1,izm                                                             
      zj = real(j)                                                              
      jj = j*j                                                                  
c                                                                               
      do 60 i=1,izm                                                             
      ij = i*j                                                                  
      ii = i*i                                                                  
      zi = real(i)                                                              
c                                                                               
      wj = zi/(2.*zj)                                                           
      wi = zj/(2.*zi)                                                           
c                                                                               
      if (ielam.le.0) then                                                      
        elam(1,i,j) = pelam(1,ij) -wj*pelam(1,jj) -wi*pelam(1,ii)               
        elam(2,i,j) = pelam(2,ij) -wj*pelam(2,jj) -wi*pelam(2,ii)               
        elam(3,i,j) = pelam(3,ij) -wj*pelam(3,jj) -wi*pelam(3,ii)               
      else                                                                      
        elam(1,i,j) = pelam(1,ij)                                               
        elam(2,i,j) = pelam(2,ij)                                               
        elam(3,i,j) = pelam(3,ij)                                               
      endif                                                                     
   60 continue                                                                  
c                                                                               
   70 continue                                                                  
c                                                                               
      if (ielam.le.0) then                                                      
c                                                                               
c       set diagonal elements to zero                                           
c                                                                               
        do 75 i=1,izm                                                           
        elam(1,i,i) = 0.                                                        
        elam(2,i,i) = 0.                                                        
        elam(3,i,i) = 0.                                                        
   75   continue                                                                
      endif                                                                     
c                                                                               
  999 continue                                                                  
      end                                                                       
c gesum    last revised 10/27/87 by tjw                                         
c*gesum f77 rewrite, 07/06/87 by tjw                                            
      subroutine gesum(elam,nst,conc,z,xi,sumw,sums,sump)                       
c                                                                               
c     input  elam   array of e-lambda values                                    
c            nst    number of species                                           
c            conc   array of concentrations                                     
c            z      array of charges                                            
c            xi     i value                                                     
c     output sumw   sum(elam(ij) + i*elam'(ij))*mi*mj                           
c            sums   sum(elam'(ij)*mi*mj                                         
c            sump   sum(elam''(ij)*mi*mj                                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension z(*),elam(3,10,10),conc(*)                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sumw = 0.                                                                 
      sums = 0.                                                                 
      sump = 0.                                                                 
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
      do 20 i=2,nst                                                             
      mi = conc(i)                                                              
      if (mi .lt. xl) go to 20                                                  
      zi = z(i)                                                                 
      if (zi .eq. 0.) go to 20                                                  
c                                                                               
      do 10 j=2,nst                                                             
c                                                                               
c     skip diagonal terms                                                       
c                                                                               
      if (i .eq. j) go to 10                                                    
      mj = conc(j)                                                              
      if (mj .eq. 0.) go to 10                                                  
      zj = z(j)                                                                 
      if (zj .eq. 0.) go to 10                                                  
      ij = nint(zi*zj)                                                          
      if (ij .lt. 0) go to 10                                                   
c                                                                               
      iz = nint(abs(zi))                                                        
      jz = nint(abs(zj))                                                        
      el = elam(1,iz,jz)                                                        
      elp = elam(2,iz,jz)                                                       
      elpp = elam(3,iz,jz)                                                      
      cp = mi*mj                                                                
c                                                                               
      sumw = sumw + (el + xi*elp)*cp                                            
      sums = sums + elp*cp                                                      
      sump = sump + elpp*cp                                                     
   10 continue                                                                  
c                                                                               
   20 continue                                                                  
c                                                                               
      end                                                                       
c getlu    last revised 07/06/87 by tjw                                         
c*getlu f77 rewrite, 07/06/87 by tjw                                            
      subroutine getlu(nlu,nerr)                                                
c                                                                               
c     purpose  to get a currently unused unit number.                           
c     output arguments                                                          
c         nlu   unit number  not currently in use.                              
c         nerr  0/1  ok/error                                                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      data iumax,iumin /19,0/                                                   
c                                                                               
c-------------------------------------------------------------------            
c                                                                               
      nerr = 0                                                                  
c                                                                               
c    loop through all valid file numbers, beginning with the largest            
c                                                                               
      do 10 nlu=iumax,iumin,-1                                                  
      inquire(unit=nlu,opened=qopen)                                            
      if (.not.qopen) go to 999                                                 
   10 continue                                                                  
c                                                                               
      nerr = 1                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c gkey     last revised 10/20/87 by tjw                                         
c*gkey f77 rewrite, 07/06/87 by tjw                                             
      subroutine gkey(uflag,udata)                                              
c                                                                               
c     this routine checks an alphanumeric flag (uflag) read from data1,         
c     data2, or data3 and checks it against a key-list to test whether          
c     or not the data files provided are consistent with the activity           
c     coefficient option that has been chosen.                                  
c                                                                               
c           option                  iopg1      legal key                        
c                                                                               
c        davies equation             -1          stfipc                         
c        b-dot equation               0          stfipc                         
c        standard pitzer equations    1          stpitz                         
c        hkf equations - part iv      5          sthkf                          
c        standard pitzer equations  101          stpitz                         
c                                                                               
c     'stfipc' = standard full ion pairs and complexes'                         
c     'stpitz' = standard pitzer equations                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlgp.h"                                                   
      include "eqlun.h"                                                   
c                                                                               
      data usfipc/'stfipc  '/,uspitz/'stpitz  '/                                
      data ushkf/'sthkf   '/                                                    
c                                                                               
c-----------------------------------------------------------------              
c                                                                               
c     set the correct key                                                       
c                                                                               
      ukey=usfipc                                                               
      if (iopg1.eq.1 .or. iopg1.eq.101) then                                    
        ukey = uspitz                                                           
      elseif (iopg1 .eq. 5) then                                                
        ukey = ushkf                                                            
      endif                                                                     
c                                                                               
      if (uflag(1:6).ne.ukey(1:6)) then                                         
c                                                                               
c       error, bad combination                                                  
c                                                                               
        write (noutpl,300) udata,iopg1,uflag,ukey                               
        write (nttyol,300) udata,iopg1,uflag,ukey                               
  300   format(' * error - act. coeff. option not compatible with',             
     $  ' supporting data file ',a8,/3x,'iopg1= ',i3,', data file key= ',       
     $   a6,', correct key= ',a6,/                                              
     $  5x,'provide the correct set of data files and try again',/              
     $  5x,'do not change the keys on the data files (eqlib/gkey)')             
        stop                                                                    
      endif                                                                     
c                                                                               
      end                                                                       
c gmdsm    last revised 11/02/87 by tjw                                         
c*gmdsm f77 rewrite, 07/06/87 by tjw                                            
      subroutine gmdsm(i,nmxi,nmxx,pmu,conc,sum)                                
c                                                                               
c     find mu sum contribution for derivatives of water                         
c       with respect to molality                                                
c                                                                               
c     input  i       species index                                              
c            nmxi,                                                              
c            nmxx    arrays of mu indices                                       
c            pmu     array of mu values                                         
c            conc    array of concentration values                              
c     output sum     sum(mu(ijk)*mj*mk)                                         
c                     note that symmetry requires multipliers for               
c                     various terms                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension pmu(*),nmxi(2,*),nmxx(3,*),conc(*)                              
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sum = 0.                                                                  
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
c     get first, last indices of contributing values                            
c                                                                               
      ifx = nmxi(1,i)                                                           
      ilx = nmxi(2,i)                                                           
c                                                                               
c     exit if no values                                                         
c                                                                               
      if (ilx .lt. ifx) go to 999                                               
c                                                                               
      do 20 kk=ifx,ilx                                                          
      sm = 1.                                                                   
      j = nmxx(1,kk)                                                            
      k = nmxx(2,kk)                                                            
      ii = nmxx(3,kk)                                                           
      if (conc(j) .lt. xl) go to 20                                             
      if (conc(k) .lt. xl) go to 20                                             
      if (i .ne. j .and. i .ne. k) then                                         
        if (j. ne. k) sm = 2.                                                   
        sum = sum + sm*pmu(ii)*conc(j)*conc(k)                                  
      else                                                                      
        sum = sum + 2.0*pmu(ii)*conc(j)*conc(k)                                 
      endif                                                                     
   20 continue                                                                  
c                                                                               
      sum = 3.0*sum                                                             
  999 continue                                                                  
      end                                                                       
c gmsum    last revised 11/02/87 by tjw                                         
c*gmsum f77 rewrite, 07/06/87 by tjw                                            
      subroutine gmsum(nmu,nmux,pmu,conc,sum)                                   
c                                                                               
c     input  nmu     number of mu values                                        
c            nmux    array of triples of species indices                        
c            pmu     array of mu values                                         
c            conc    array of concentration values                              
c     output sum     sum(mu(ijk)*mi*mj*mk)                                      
c                     note that data includes single occurrence of              
c                     each value - triple sum symmetry requires                 
c                     multiplication by 3                                       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension nmux(3,*),pmu(*),conc(*)                                        
c                                                                               
c----------------------------------------------------------------------         
      sum = 0.                                                                  
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
      do 10 kk=1,nmu                                                            
      i = nmux(1,kk)                                                            
      j = nmux(2,kk)                                                            
      k = nmux(3,kk)                                                            
      if (conc(i) .lt. xl) go to 10                                             
      if (conc(j) .lt. xl) go to 10                                             
      if (conc(k) .lt. xl) go to 10                                             
      sum = sum + pmu(kk) * conc(i)*conc(j)*conc(k)                             
   10 continue                                                                  
c                                                                               
      sum = sum * 3.                                                            
  999 continue                                                                  
      end                                                                       
c gntpr    last revised 12/12/87 by tjw                                         
c*gntpr created in f77  12/12/87 by tjw                                         
      subroutine gntpr(tempc,al10,rconst,farad,tempk,afcnst,                    
     $ ehfac,ntpr)                                                              
c                                                                               
c     this routine finds the value of the temperature range flag                
c     ntpr.                                                                     
c                                                                               
c     input                                                                     
c       tempc = temperature, degrees celsius                                    
c       al10 = ln 10                                                            
c       rconst = the gas constant                                               
c       farad = the faraday constant                                            
c                                                                               
c     output                                                                    
c       tempk = temperature, degrees kelvin                                     
c       afcnst = affinity constant                                              
c       ehfac = eh factor                                                       
c       ntpr = temperature range flag                                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      data tconst/273.15/,tempc1/450./,tempc2/999./                             
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      tempk = tempc + tconst                                                    
      afcnst = 0.001*al10*rconst*tempk                                          
      ehfac = (al10*rconst*tempk)/farad                                         
c                                                                               
c                                                                               
c     determine the temperature range flag (ntpr)                               
c       ntpr = 1  tempc .le. tempc1                                             
c            = 2  tempc1 .lt. tempc .le. tempc2                                 
c            = 3  tempc .gt. tempc2                                             
c                                                                               
      if (tempc .le. tempc1) then                                               
        ntpr = 1                                                                
      elseif (tempc .le. tempc2) then                                           
        ntpr = 2                                                                
      else                                                                      
        ntpr = 3                                                                
      endif                                                                     
c                                                                               
      end                                                                       
c gpheh    last revised 12/26/87 by tjw                                         
c*gpheh created in f77  12/12/87 by tjw                                         
      subroutine gpheh(actlg,glg,fo2lg,xi,adh,xlkeh,ehfac,ph,                   
     $ phnbs,phrat,phcl,eh,ehnbs,ehrat,pe,penbs,perat,iopg2,                    
     $ nhydr,nchlor,qnochb)                                                     
c                                                                               
c     this routine computes the ph, redox potential, and pe                     
c     (electron activity function) on the operational ph scale used             
c     in the calculations (see iopg2), the modified nbs ph scale,               
c     and the rational ph scale.                                                
c                                                                               
c     input                                                                     
c       actlg = array of logarithms of aqueous species activities;              
c               actlg(1) is the log activity of water                           
c       glg = array of logarithms of aqueous species activity                   
c             coefficients                                                      
c       fo2lg = log oxygen fugacity                                             
c       xi = ionic strength                                                     
c       adh = debye-huckel 'a' parameter                                        
c       xlkeh = log k for special reaction for calculating eh                   
c               from log oxygen fugacity                                        
c       ehfac = eh factor                                                       
c       iopg2 = activity coefficient option switch, defines the                 
c               operational ph scale used in the computation                    
c               = -1   internal scale defined for the activity                  
c                      coefficinet model defined by iopg1                       
c                = 0   the modified nbs ph scale (log gamma cl-                 
c                      defined by bates-guggenheim expression)                  
c                = 2   the rational ph scale (log gamma h+ defined              
c                      as zero)                                                 
c       nhydr = species index of the hydrogen ion                               
c       nchlor = species index of the chloride ion                              
c       qnochb = logical flag, = .true. if no oxidation-reduction               
c                in the modeled system                                          
c                                                                               
c     output                                                                    
c       ph = ph on scale used in computation                                    
c       phnbs = ph on the modified nbs scale                                    
c       phrat = ph on the rational scale                                        
c       phcl = ph + pcl                                                         
c       eh = eh corresponding to the ph used in the computation                 
c       ehnbs = eh corresponding to the phnbs                                   
c       ehrat = eh corresponding to the phrat                                   
c       pe = pe function corresonding to eh                                     
c       penbs = pe function corresonding to ehnbs                               
c       perat = pe function corresonding to ehrat                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension actlg(*),glg(*)                                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     compute ph, eh, and pe for the operational ph scale                       
c                                                                               
      ph = -actlg(nhydr)                                                        
      eh = -999.                                                                
      pe = -999.                                                                
      if (.not.qnochb) then                                                     
        efac = 0.25*ehfac                                                       
        eterm = fo2lg - 4.*ph - 2.*actlg(1) - xlkeh                             
        eh = efac*eterm                                                         
        pe = eh/ehfac                                                           
      endif                                                                     
c                                                                               
c     compute ph, eh, and pe consistent with the modified nbs ph scale          
c     note that this can not be done if the activity coefficient of             
c     chloride ion on the operational scale is not available.                   
c                                                                               
      phnbs = -999.                                                             
      ehnbs = -999.                                                             
      penbs = -999.                                                             
      if (nchlor. gt. 0) then                                                   
        if (iopg2 .eq. 0)  then                                                 
          phnbs = ph                                                            
          ehnbs = eh                                                            
          penbs = pe                                                            
        else                                                                    
          call nbsgam(glgnbs,xi,adh,nchlor)                                     
          delglg = glgnbs - glg(nchlor)                                         
          phnbs = ph - delglg                                                   
          if (.not.qnochb) then                                                 
            eterm0 = eterm + 4.*(ph - phnbs)                                    
            ehnbs = efac*eterm0                                                 
            penbs = ehnbs/ehfac                                                 
          endif                                                                 
        endif                                                                   
      endif                                                                     
c                                                                               
      ehrat = -999.                                                             
      perat = -999.                                                             
      if (iopg2 .eq. 1) then                                                    
        phrat = ph                                                              
        ehrat = eh                                                              
        perat = pe                                                              
      else                                                                      
        delglg = - glg(nhydr)                                                   
        phrat = ph - delglg                                                     
        if (.not.qnochb) then                                                   
          eterm1 = eterm + 4.*(ph - phrat)                                      
          ehrat = efac*eterm1                                                   
          perat = ehrat/ehfac                                                   
        endif                                                                   
      endif                                                                     
c                                                                               
c     compute phcl = ph + pcl                                                   
c                                                                               
      phcl = -999.                                                              
      if (nchlor .gt. 0) then                                                   
        pcl = -actlg(nchlor)                                                    
        phcl = ph + pcl                                                         
      endif                                                                     
c                                                                               
      end                                                                       
c gsdsm    last revised 07/06/87 by tjw                                         
c*gsdsm  f77 rewrite, 07/06/87 by tjw                                           
      subroutine gsdsm(nslm,nslmx,pslam,conc,sum,sump)                          
c                                                                               
c     input  nslm    number of s-lambda's                                       
c            nslmx   array of species index pairs                               
c            pslam   array of s-lambda values                                   
c            conc    array of concentration values                              
c     output sum     sum(slam'(jk)*mj*mk)                                       
c            sump    sum(slam''(jk)*mj*mk)                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension nslmx(2,*),pslam(3,*),conc(*)                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sum = 0.                                                                  
      sump = 0.                                                                 
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
      do 20 n=1,nslm                                                            
      i = nslmx(1,n)                                                            
      j = nslmx(2,n)                                                            
      if (conc(i) .lt. xl) go to 20                                             
      if (conc(j) .lt. xl) go to 20                                             
      cp = conc(i)*conc(j)                                                      
      sum = sum + pslam(2,n)*cp                                                 
      sump = sump + pslam(3,n)*cp                                               
   20 continue                                                                  
c                                                                               
      sum = 2.*sum                                                              
      sump = 2.*sump                                                            
c                                                                               
      end                                                                       
c gselm    last revised 08/07/87 by rmm                                         
c*gselm f77 rewrite, 07/06/87 by tjw                                            
      subroutine gselm(elam,nst,jsflag,conc,z,izoff,selm,selmp)                 
c  references to int replaced by nint 07/06/87 by tjw                           
c                                                                               
c     input  elam    array of e-lambda values                                   
c            nst     number of species                                          
c            jsflag  array of status flags, aqueous species                     
c            conc    array of concentrations                                    
c            z       array of charges                                           
c            izoff   index to zero charge                                       
c     output selm    array of sums  sum(el(ij)*mj)                              
c            selmp   array of sums  sum(el'(ij)*mj)                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension z(*),elam(3,10,10),conc(*),selm(*),selmp(*),jsflag(*)           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      selm(izoff) = 0.                                                          
      selmp(izoff) = 0.                                                         
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
      do 20 i=2,nst                                                             
      if (jsflag(i) .ne. 0) go to 20                                            
      zi = z(i)                                                                 
      if (zi .eq. 0.) go to 20                                                  
      iz = nint(zi)                                                             
      ise = iz+izoff                                                            
      sum = 0.                                                                  
      sump = 0.                                                                 
c                                                                               
      do 10 j=2,nst                                                             
      if (jsflag(j) .ne. 0) go to 10                                            
c                                                                               
c      skip diagonal terms                                                      
c                                                                               
      if (i .eq. j) go to 10                                                    
      mj = conc(j)                                                              
      if (mj .lt. xl) go to 10                                                  
      zj = z(j)                                                                 
      if (zj .eq. 0.) go to 10                                                  
      ij = nint(zi*zj)                                                          
      if (ij .lt. 0) go to 10                                                   
c                                                                               
      iz = nint(abs(zi))                                                        
      jz = nint(abs(zj))                                                        
      sum = sum + elam(1,iz,jz)*mj                                              
      sump = sump + elam(2,iz,jz)*mj                                            
   10 continue                                                                  
c                                                                               
      selm(ise) = sum                                                           
      selmp(ise) = sump                                                         
   20 continue                                                                  
c                                                                               
      end                                                                       
c gsgsm    last revised 07/06/87 by tjw                                         
c*gsgsm f77 rewrite, 07/06/87 by tjw                                            
      subroutine gsgsm(i,nsxi,nsxx,pslam,conc,sum,sump)                         
c                                                                               
c     input  i      species index                                               
c            nsxi,                                                              
c            nsxx   sets of s-lambda indices                                    
c            pslam  array of s-lambda values                                    
c            conc   array of concentration values                               
c     output sum    sum(slam(ij)*mj)                                            
c            sump   sum(slam'(ij)*mj)                                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension nsxi(2,*),nsxx(2,*),pslam(3,*),conc(*)                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sum = 0.                                                                  
      sump = 0.                                                                 
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
c     get first and last indices to nsxx array                                  
c                                                                               
      ixf = nsxi(1,i)                                                           
      ixl = nsxi(2,i)                                                           
c                                                                               
c     exit if no entries                                                        
c                                                                               
      if (ixl .ge. ixf) then                                                    
        do 20 ii = ixf,ixl                                                      
        k = nsxx(2,ii)                                                          
        j = nsxx(1,ii)                                                          
        cj = conc(j)                                                            
        if (cj .ge. xl) then                                                    
          sum = sum + pslam(1,k) * cj                                           
          sump = sump + pslam(2,k) * cj                                         
        endif                                                                   
   20   continue                                                                
      endif                                                                     
      end                                                                       
c gshm     last revised 11/02/87 by tjw                                         
c*gshm f77 rewrite, 07/06/87 by tjw                                             
      subroutine gshm(hydn,conc,jsort,nst,shmc,dshmc)                           
c                                                                               
c     input   hydn   array of hydration numbers                                 
c             conc   array of concentrations  mj                                
c             jsort   array of aqueous species indices, in order of             
c                     increasing concentration                                  
c             nst    number of elements in arrays                               
c     output  shmc    sum(hj*mj)                                                
c             dshmc  ( 1 - (shmc/om) )                                          
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlpp.h"                                                   
      include "eqlun.h"                                                   
c                                                                               
      dimension hydn(*),conc(*),jsort(*)                                        
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      shmc = 0.                                                                 
c                                                                               
c     note - this calculation presumes that either conc(1) or                   
c     hydn(1) is zero, so that there is no contribution from                    
c     the solvent.  it should be always safe to assume that hydn(1) = 0.        
c                                                                               
      do 20 nss=1,nst                                                           
      ns=jsort(nss)                                                             
      shmc = shmc+hydn(ns)*conc(ns)                                             
   20 continue                                                                  
      dshmc=1.-(shmc/om)                                                        
      end                                                                       
c gsigm    last revised 11/05/87 by tjw                                         
c*gsigm f77 rewrite, 07/06/87 by tjw                                            
      subroutine gsigm(conc,jsort,nst,sigmmc)                                   
c                                                                               
c     this routine calculates the sum of concentrations ('sigma m').            
c     note that a sorted summation is used.                                     
c                                                                               
c       input                                                                   
c         conc    array of concentrations  mi                                   
c         jsort   array of aqueous species indices, in order of                 
c                 increasing concentration                                      
c         nst     number of elements in array                                   
c                                                                               
c       output                                                                  
c         sigmmc      sum(mi)                                                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension conc(*),jsort(*)                                                
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     note - this calculation presumes that conc(1) 1) is zero, so that         
c     there is no contribution from the solvent.                                
c                                                                               
      sigmmc = 0.                                                               
      do 10 nss=1,nst                                                           
      ns=jsort(nss)                                                             
      sigmmc = sigmmc + conc(ns)                                                
   10 continue                                                                  
c                                                                               
      end                                                                       
c gslam    last revised 11/02/87 by tjw                                         
c*gslam f77 rewrite, 07/06/87 by tjw                                            
      subroutine gslam(xi,nslm,pslm,nalpha,pslam)                               
c                                                                               
c     input  xi       i value                                                   
c            nslm     number of lambda values                                   
c            pslm     array of lambda triples                                   
c            nalpha   indices into palpha array                                 
c     output pslam    array of s-lambda and                                     
c                                 '     first derivative                        
c                                 '     second derivative                       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      include "eqlun.h"                                                   
c                                                                               
      dimension pslm(3,*),nalpha(*),pslam(3,*)                                  
      dimension g(2),gp(2),gpp(2)                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 20 k=1,nslm                                                            
c                                                                               
c     get g and derivatives                                                     
c                                                                               
      call gdd(xi,nalpha(k),g,gp,gpp,ier)                                       
      if (ier .ne. 0) then                                                      
        write(noutpl,1010)                                                      
        write(nttyol,1010)                                                      
 1010   format(' * error - bad call to gdd for s-lambda calc.',                 
     $  ' (eqlib/gslam)')                                                       
        stop                                                                    
      endif                                                                     
c                                                                               
      pl0 = pslm(1,k)                                                           
      pl1 = pslm(2,k)                                                           
      pl2 = pslm(3,k)                                                           
c                                                                               
      pslam(1,k) = pl0 + g(1)*pl1 + g(2)*pl2                                    
      pslam(2,k) = gp(1)*pl1 + gp(2)*pl2                                        
      pslam(3,k) = gpp(1)*pl1 + gpp(2)*pl2                                      
c                                                                               
   20 continue                                                                  
c                                                                               
      end                                                                       
c gspion   last revised 12/18/87 by tjw                                         
c*gspion created in f77 12/18/87 by tjw                                         
      subroutine gspion(uspec,nst,nhydr,nchlor)                                 
c                                                                               
c     this routine finds the indices of the h+ and cl- ions                     
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*(*) uspec(*)                                                    
c                                                                               
      data uhydr/'H+      '/,uchlor/'CL-     '/                                 
c                                                                               
c------------------------------------------------------------------             
c                                                                               
      nhydr = 0                                                                 
      nchlor = 0                                                                
c                                                                               
      do 10 ns = 1,nst                                                          
      if (uspec(ns)(1:8) .eq. uhydr(1:8)) then                                  
        nhydr = ns                                                              
        go to 15                                                                
      endif                                                                     
   10 continue                                                                  
c                                                                               
   15 do 20 ns = 1,nst                                                          
      if (uspec(ns)(1:8) .eq. uchlor(1:8)) then                                 
        nchlor = ns                                                             
        go to 25                                                                
      endif                                                                     
   20 continue                                                                  
c                                                                               
   25 continue                                                                  
c                                                                               
      end                                                                       
c gssum    last revised 07/06/87 by tjw                                         
c*gssum f77 rewrite, 07/06/87 by tjw                                            
      subroutine gssum(xi,nslm,nslmx,pslam,conc,sum)                            
c                                                                               
c     input  xi     i value                                                     
c            nslm   number of s-lambda values                                   
c            nslmx  array of s-lambda species index pairs                       
c            pslam  array of s-lambda values                                    
c            conc   array of concentration values                               
c     output sum    sum((slam(ij) + i*slam'(ij))*mi*mj)                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension nslmx(2,*),pslam(3,*),conc(*)                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sum = 0.                                                                  
c                                                                               
c     xl is lower limit for conc of one species                                 
c                                                                               
      xl = 1.0e-09                                                              
c                                                                               
      do 20 n=1,nslm                                                            
      i = nslmx(1,n)                                                            
      j = nslmx(2,n)                                                            
      if (conc(i) .lt. xl) go to 20                                             
      if (conc(j) .lt. xl) go to 20                                             
      sum = sum + (pslam(1,n) + xi*pslam(2,n))*conc(i)*conc(j)                  
   20 continue                                                                  
c                                                                               
      sum = 2.*sum                                                              
c                                                                               
      end                                                                       
c gszm     last revised 10/30/87 by tjw                                         
c*gszm created in f77  10/27/87 by tjw                                          
      subroutine gszm(z,conc,jsort,nst,sigzc,sigza,sigzm,sigzi)                 
c                                                                               
c     this routine calculates the sums of equivalent concentrations             
c     and the charge imbalance.  note that a sorted summation is                
c     used.                                                                     
c                                                                               
c       input                                                                   
c         z       array of charges zi                                           
c         conc    array of concentrations  mi                                   
c         jsort   array of aqueous species indices, in order of                 
c                 increasing concentration                                      
c         nst     number of elements in array                                   
c                                                                               
c       output                                                                  
c         sigzc    the sum of equivalent concentrations of aqueous              
c                  cations, sigma(i) zi * mi, for zi .gt. 0                     
c         sigza    the sum of equivalent concentrations of aqueous              
c                  anions, sigma(i) abs(zi) * mi, for zi .lt. 0                 
c         sigzm    the sum of equivalent concentrations of aqueous              
c                  ions, sigzc + sigza                                          
c         sigzi    the calculated charge imbalance, sigzc - sigza               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension z(*),conc(*),jsort(*)                                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sigzc=0.                                                                  
      sigza=0.                                                                  
      sigzm=0.                                                                  
      sigzi=0.                                                                  
c                                                                               
c     note - this calculation presumes that either conc(1) or                   
c     z(1) is zero, so that there is no contribution from                       
c     the solvent.  it is always safe to assume that z(1) = 0.                  
c                                                                               
      do 10 nss=1,nst                                                           
      ns=jsort(nss)                                                             
      if (z(ns) .gt. 0.) then                                                   
        ec = z(ns)*conc(ns)                                                     
        sigzc = sigzc + ec                                                      
        sigzi = sigzi + ec                                                      
        sigzm = sigzm + ec                                                      
      elseif (z(ns).lt.0.) then                                                 
        ec = - z(ns)*conc(ns)                                                   
        sigza = sigza + ec                                                      
        sigzi = sigzi - ec                                                      
        sigzm = sigzm + ec                                                      
      endif                                                                     
   10 continue                                                                  
c                                                                               
      end                                                                       
c gtime    last revised 07/06/87 by tjw                                         
c*gtime f77 rewrite, 07/06/87 by tjw                                            
      subroutine gtime(utime)                                                   
c                                                                               
c     get time in ascii (ridge/sun dependent)                                   
c     see also timdat                                                           
c     utime is returned in the form  hh:mm:ss                                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      character*24 utd                                                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     fdate is a ridge/sun routine                                              
c                                                                               
CDAS      call fdate(utd)                                                           
CDAS      utime = utd(12:19)                                                        
      end                                                                       
c gxi      last revised 10/30/87 by tjw                                         
c*gxi created in f77  10/27/87 by tjw                                           
      subroutine gxi(zsq2,conc,jsort,nst,xic)                                   
c                                                                               
c     this routine calculates the ion strength.  note that a sorted             
c     summation is used.                                                        
c                                                                               
c       input                                                                   
c         zsq2    array of charge squared factors 0.5 * zi * zi                 
c         conc    array of concentrations  mi                                   
c         jsort   array of aqueous species indices, in order of                 
c                 increasing concentration                                      
c         nst     number of elements in array                                   
c                                                                               
c       output                                                                  
c         xic     ionic strength                                                
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension zsq2(*),conc(*),jsort(*)                                        
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      xic=0.                                                                    
c                                                                               
c     note - this calculation presumes that either conc(1) or                   
c     zsq2(1) is zero, so that there is no contribution from                    
c     the solvent.  it is always safe to assume that zsq2(1) = 0.               
c                                                                               
      do 10 nss=1,nst                                                           
      ns=jsort(nss)                                                             
      xic = xic + zsq2(ns)*conc(ns)                                             
   10 continue                                                                  
c                                                                               
      end                                                                       
c gxisto   last revised 11/03/87 by tjw                                         
c*gxisto f77 check, ok 07/08/87 by tjw                                          
      subroutine gxisto(zsq2,conc,nsq,concbs,xistoc)                            
c                                                                               
c     this routine was formerly known as truexi.                                
c     for hkf equations--calculates xistoc (stoichiometric ionic strength)      
c                                                                               
c     input                                                                     
c       zsq2    array of one-half the charge squared                            
c       conc    array of concentrations  mi                                     
c       nsq     number of basis species                                         
c     output                                                                    
c       xistoc  stochiometric ionic strength --                                 
c                     sum of (1/2)*z*z*(total conc of basis species)            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
      include "eqlpar.h"                                                  
c                                                                               
      include "eqlhkf.h"                                                  
c                                                                               
      dimension zsq2(*),conc(*),concbs(*)                                       
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     calculate xistoc                                                          
c                                                                               
      xistoc = 0.0                                                              
      do 20 ns=2,nsq                                                            
      xistoc = xistoc + zsq2(ns)*concbs(ns)                                     
   20 continue                                                                  
c                                                                               
      end                                                                       
c gxistq   last revised 10/30/87 by tjw                                         
c*gxistq created in f77  10/27/87 by tjw                                        
      subroutine gxistq(store,conc,jsort,nst,xistqc)                            
c                                                                               
c     this routine calculates the equivalent stoichiometric                     
c     ionic strength used to estimate the activity of water                     
c     when using the bdot option.  note that a sorted summation is used.        
c                                                                               
c       input                                                                   
c         store   array of stoichiometric factors                               
c         conc    array of concentrations  mi                                   
c         jsort   array of aqueous species indices, in order of                 
c                 increasing concentration                                      
c         nst     number of elements in array                                   
c                                                                               
c       output                                                                  
c         xistqc     equivalent stoichiometric ionic strength                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension store(*),conc(*),jsort(*)                                       
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      xistqc=0.                                                                 
c                                                                               
c     note - this calculation presumes that either conc(1) or                   
c     store(1) is zero, so that there is no contribution from                   
c     the solvent.  it should be safe to assume that store(1) = 0.              
c                                                                               
      do 10 nss=1,nst                                                           
      ns=jsort(nss)                                                             
      xistqc = xistqc + store(ns)*conc(ns)                                      
   10 continue                                                                  
c                                                                               
      end                                                                       
c hjdd     last revised 07/06/87 by tjw                                         
c*hjdd f77 rewrite, 07/06/87 by tjw                                             
      subroutine hjdd(x,rjz,rjz1,rjz2,ier)                                      
c                                                                               
c     input x                                                                   
c     compute rjz = j0(x)                                                       
c             rjz1 = j0'(x)                                                     
c             rjz2 = j0''(x)                                                    
c                                                                               
c     ier = 0/-1  ok/x out of range                                             
c     x must be .gt. 0 and .le. 900.                                            
c                                                                               
c     reference - harvie (1981), appendix b                                     
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
c                                                                               
      dimension ai(21),aii(21),b(23),d(23),e(23)                                
c                                                                               
      data b(22),b(23) /0.,0./                                                  
      data d(22),d(23) /0.,0./                                                  
      data e(22),e(23) /0.,0./                                                  
c                                                                               
      data (ai(i),i=1,10)                                                       
     $         /1.92515 40148 14667,                                            
     $          -.06007 64777 53119,                                            
     $          -.02977 90774 56514,                                            
     $          -.00729 94996 90937,                                            
     $           .00038 82606 36404,                                            
     $           .00063 68745 99598,                                            
     $           .00003 65836 01823,                                            
     $          -.00004 50369 75204,                                            
     $          -.00000 45378 95710,                                            
     $           .00000 29377 06971/                                            
      data (ai(i),i=11,21)                                                      
     $          /.00000 03965 66462,                                            
     $          -.00000 02020 99617,                                            
     $          -.00000 00252 67769,                                            
     $           .00000 00135 22610,                                            
     $           .00000 00012 29405,                                            
     $          -.00000 00008 21969,                                            
     $          -.00000 00000 50847,                                            
     $           .00000 00000 46333,                                            
     $           .00000 00000 01943,                                            
     $          -.00000 00000 02563,                                            
     $          -.00000 00000 10991/                                            
c                                                                               
      data (aii(i),i=1,10)                                                      
     $         / .62802 33205 20852,                                            
     $           .46276 29853 38493,                                            
     $           .15004 46371 87895,                                            
     $          -.02879 60576 04906,                                            
     $          -.03655 27459 10311,                                            
     $          -.00166 80879 45272,                                            
     $           .00651 98403 98744,                                            
     $           .00113 03780 79086,                                            
     $          -.00088 71713 10131,                                            
     $          -.00024 21076 41309/                                            
      data (aii(i),i=11,21)                                                     
     $          /.00008 72944 51594,                                            
     $           .00003 46821 22751,                                            
     $          -.00000 45837 68938,                                            
     $          -.00000 35486 84306,                                            
     $          -.00000 02504 53880,                                            
     $           .00000 02169 91779,                                            
     $           .00000 00807 79570,                                            
     $           .00000 00045 58555,                                            
     $          -.00000 00069 44757,                                            
     $          -.00000 00028 49257,                                            
     $           .00000 00002 37816/                                            
c                                                                               
      data cii /.22222 22222 22222/                                             
      data d1j,d1k,d1l /.8, -.44444 44444 44444, -1.1/                          
      data d2i,d2j,d2k,d2l /-.64, -1.8,                                         
     $   .48888 88888 88889, -2.1/                                              
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      rjz = 0.                                                                  
      rjz1 = 0.                                                                 
      rjz2 = 0.                                                                 
      ier = -1                                                                  
c                                                                               
      if (x .le. 0. .or. x .gt. 900.) go to 999                                 
      ier = 0                                                                   
      alx = log(x)                                                              
c                                                                               
      if (x .le. 1.) then                                                       
c                             *----------------------------------------         
c                             * case i here                                     
c                             *----------------------------------------         
        z = 4.0 * exp(alx / 5.) - 2.                                            
        dz = d1j * exp(-alx * d1j)                                              
        dz2 = d2i * exp(alx * d2j)                                              
c                                                                               
c       generate the b's, d's and e's                                           
c                                                                               
        do 10 ki = 1,21                                                         
        k = 22 - ki                                                             
        b(k) = z * b(k+1) - b(k+2) + ai(k)                                      
        d(k) = b(k+1) + z * d(k+1) - d(k+2)                                     
        e(k) = 2. * d(k+1) + z * e(k+1) - e(k+2)                                
   10   continue                                                                
c                                                                               
      else                                                                      
c                             *----------------------------------------         
c                             * case ii here                                    
c                             *----------------------------------------         
        z = cii * (20.*exp(-alx / 10.) - 11.)                                   
        dz = d1k * exp(alx * d1l)                                               
        dz2 = d2k * exp(alx * d2l)                                              
c                                                                               
c       generate the b's, d's and e's                                           
c                                                                               
        do 50 ki = 1,21                                                         
        k = 22 - ki                                                             
        b(k) = z * b(k+1) - b(k+2) + aii(k)                                     
        d(k) = b(k+1) + z * d(k+1) - d(k+2)                                     
        e(k) = 2. * d(k+1) + z * e(k+1) - e(k+2)                                
   50   continue                                                                
      endif                                                                     
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     compute (b0 - b2), (d0 - d2), and (e0 - e2)                               
c                                                                               
      bdif = b(1) - b(3)                                                        
      ddif = d(1) - d(3)                                                        
      edif = e(1) - e(3)                                                        
c                                                                               
      rjz = (x / 4.) - 1.0 + bdif / 2.                                          
      rjz1 = .25 + .5 * dz * ddif                                               
      rjz2 = .5 * (edif * dz**2 + ddif * dz2)                                   
c                                                                               
  999 continue                                                                  
      end                                                                       
c hkfrd    last revised 07/24/87 by rmm                                         
c*hkfrd f77 rewrite, 07/06/87 by tjw                                            
      subroutine hkfrd(uspec,jsflag,nad1,nst)                                   
c                                                                               
c        created 17sept86  rmm                                                  
c                                                                               
c     read hkf (part iv) parameter base values from data file nad1              
c                                                                               
c     called by subprogram indpt                                                
c      input  uspec   array of species names                                    
c             jsflag  array of status switches, aqueous species                 
c             nad1    unit number of input file                                 
c             nst     number of aqueous species                                 
c                                                                               
c      output parameters are in common /eqlhkf/                                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                                
      include "eqlpar.h"                                                  
c                                                                               
      include "eqlhkf.h"                                                  
      include "eqlgp.h"                                                   
      include "eqlun.h"                                                  
c                                                                               
      dimension jsflag(*)                                                       
c                                                                               
      character*24 uspec(*),unam1,unam2                                         
      data uendit /'endit.  '/                                                  
      data ufnam1 /'hkf     '/                                                  
c                                                                               
c----------------------------------------------------------------------         
      jj=0                                                                      
c                                                                               
c     read header line                                                          
c                                                                               
      read (nad1) udum                                                          
      if (udum .ne. ufnam1) then                                                
        write (noutpl,1003) udum,ufnam1                                         
        write (nttyol,1003) udum,ufnam1                                         
 1003   format(' * error - data file error at start of hkf data',/              
     $  3x,'key= ',a8,', correct key= ',a8,/                                    
     $  5x,'append the correct hkf data to',                                    
     $  ' data1 or data2 and try again',                                        
     $  /5x,'do not change the key (eqlib/hkfrd)')                              
        stop                                                                    
      endif                                                                     
c                                                                               
c     read a four line title                                                    
c                                                                               
      do 5 i=1,4                                                                
      read (nad1) udhkf(i)                                                      
    5 continue                                                                  
c                                                                               
c                                                                               
c               single ion parameter - omega                                    
c               read in one line                                                
c                                                                               
  310 continue                                                                  
      read (nad1) unam1,omgtmp                                                  
      if (unam1(1:8) .eq. uendit(1:8)) go to 400                                
c                                                                               
c               map ion name to index                                           
c                                                                               
      call srchn(uspec,nst,unam1,ix1)                                           
c                                                                               
c               skip if species not found                                       
c                                                                               
      if (ix1 .le. 0) go to 310                                                 
c                                                                               
c               skip if ion is not in current model                             
c                                                                               
      if (jsflag(ix1) .ne. 0) go to 310                                         
c                                                                               
c               put into common                                                 
c                                                                               
      jj = jj + 1                                                               
      if (jj .gt. ibjmax) then                                                  
        write (noutpl,1010)                                                     
        write (nttyol,1010)                                                     
 1010   format(' * error, omega array overflow (eqlib/hkfrd)')                  
        stop                                                                    
      endif                                                                     
c                                                                               
      indx1(jj) = ix1                                                           
      omega(jj) = omgtmp                                                        
c                                                                               
      go to 310                                                                 
c                                                                               
  400 continue                                                                  
      ibjuse = jj                                                               
      jj = 0                                                                    
c                                                                               
c               do ion-ion parameters                                           
c                                                                               
  410 continue                                                                  
      read (nad1) unam1,unam2,bjitmp                                            
      if (unam1(1:8) .eq. uendit(1:8)) go to 500                                
c                                                                               
c               map ion names to indexes                                        
c                                                                               
      call srchn(uspec,nst,unam1,ix1)                                           
      call srchn(uspec,nst,unam2,ix2)                                           
c                                                                               
c               skip if species not found                                       
c                                                                               
      if (ix1 .le. 0 .or. ix2 .le. 0) go to 410                                 
c                                                                               
c               skip if not in current model                                    
c                                                                               
      if (jsflag(ix1) .ne. 0 .or. jsflag(ix2) .ne. 0) go to 410                 
c                                                                               
c                put into common                                                
c                                                                               
      jj = jj + 1                                                               
      indx2(1,jj) = ix1                                                         
      indx2(2,jj) = ix2                                                         
      bji(jj) = bjitmp                                                          
c                                                                               
      go to 410                                                                 
c                                                                               
  500 continue                                                                  
      ibjius = jj                                                               
c                                                                               
c              print out both single and double interaction parms               
c                                                                               
      write (noutpl,1030)                                                       
 1030 format(/,'    name                   omega             ')                 
c                                                                               
      do 600 n=1,ibjuse                                                         
      write (noutpl,1040) uspec(indx1(n))(1:24),omega(n)                        
 1040 format(x,a24,2x,e15.5)                                                    
  600 continue                                                                  
c                                                                               
      write (noutpl,1050)                                                       
 1050 format(/,'    name            name              bji   ')                  
c                                                                               
      do 700 n = 1, ibjius                                                      
      write (noutpl,1060) uspec(indx2(1,n))(1:24),                              
     $     uspec(indx2(2,n))(1:24),bji(n)                                       
 1060 format(x,a24,2x,a24,2x,e15.5)                                             
  700 continue                                                                  
c                                                                               
      end                                                                       

c hpsat    last revised 01/26/88 by tjw                                         
c*hpsat partial f77 rewrite, 07/06/87 by tjw                                    
      subroutine hpsat(axx,si,affx,lamlg,xbarlg,xbar,w,xbarh,xqkx,              
     $ xlkm,afflcx,siss,aff,al10,fo2lg,tempk,rconst,ussnp,umin,                 
     $ nend,jsflag,jmflag,jxflag,ncomp,jsol,itracx,nsb,nsq,nsq1,                
     $ nnm1,iktmax,nx,ier,nn,idbg,ndbug1)                                       
c                                                                               
c     dimension axx(iktpar,iktpar),xbarh(iktpar),xqkx(iktpar),itracx(iktpar)    
c                                                                               
c     this routine calculates the most stable (least soluble)                   
c     composition of a given solid solution, given the composition              
c     of the aqueous phase it is in equilibrium with.  it first                 
c     solves for the case of ideal mixing.  if the input data for               
c     the solid solution include data for a non-ideal mixing model,             
c     the ideal composition is used as a starting estimate for the              
c     solution to the non-ideal case                                            
c                                                                               
c     routines called:                                                          
c                                                                               
c     ssfunc    function that computes value of saturation index                
c     dsiplx    routine that finds maximum of function using                    
c               simplex method (non-linear)                                     
c     lamda    routine that returns activity coefficients                       
c               for components of solid solution (regular soln)                 
c                                                                               
c     variables:                                                                
c                                                                               
c     ncomp     number of components in solid solution mineral in data          
c               base                                                            
c     nn        number of components in solid solution present in               
c               problem                                                         
c     nnm1      number of independent components (nn-1)                         
c     xlkm      array of log solubility products of endmembers of               
c               solid solutions                                                 
c     xqkx      saturation indices for endmember components                     
c     aff       array holding affinities for all mineral phases                 
c     lamlg     array of log activity coefficients for components of            
c               solid solution                                                  
c     xbarh     array that holds solid solution composition in                  
c               in subroutine dsiplx                                            
c     xbar      array that holds composition of solid solution                  
c               in subroutines hpsat, ssfunc and at output                      
c               (note that xbar has the same dimension as the                   
c                total number of components for a solid solution                
c                in the data base; comp holds a compressed version              
c                of xbar with the zeros removed.  comp is passed                
c                to dsiplx)                                                     
c     axx       the first column of this 2-d array initially holds the          
c               ideal composition used as a starting estimate. the              
c               last column x(10,nn) holds the simplex net coordinates.         
c               after convergence, the array 'comp' is returned that            
c               holds the solution.                                             
c     siss      saturation index for solid solution                             
c     jmflag    suppress endmember solid (2-suppress 0-leave it)                
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      character*(*) ussnp(iktmax,*),umin(*)                                     
c                                                                               
      dimension axx(iktmax,*),affx(iktmax,*),xbar(iktmax,*),                    
     $ lamlg(iktmax,*),si(iktmax,*),w(iktmax,*),xbarlg(iktmax,*),               
     $ xlkm(*),afflcx(*),siss(*),aff(*),xbarh(*),xqkx(*)                        
      dimension nend(iktmax,*),jsflag(*),jmflag(*),ncomp(*),jsol(*),            
     $ jxflag(*),itracx(*)                                                      
c                                                                               
      dimension alpha(3),deps(2)                                                
c                                                                               
      external ssfunc                                                           
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     note- in the future, should pass afcnst instead of defining afkst;        
c           also, should pass and use jkflag instead of jmflag - tjw            
c                                                                               
      afkst = 0.001*al10*rconst*tempk                                           
c                                                                               
c     if solid solution is suppressed, return                                   
c                                                                               
      if (jxflag(nx).eq.4) go to 999                                            
c                                                                               
c     initialize error flag, affinity function, and sum of q/k values           
c                                                                               
      ier = 0                                                                   
      afflcx(nx) = -999.                                                        
      sum = 0.                                                                  
c                                                                               
c     look through array of solid solution endmembers to see                    
c     how many are present in this problem and calculate q/k                    
c     ratios for those present                                                  
c                                                                               
      ncpt = ncomp(nx)                                                          
      nn = ncpt                                                                 
      do 10 ik = 1,ncpt                                                         
      xbar(ik,nx) = 0.                                                          
      xbarlg(ik,nx) = -999.                                                     
      affx(ik,nx) = -999.                                                       
      nm = nend(ik,nx)                                                          
c                                                                               
c     transfer name of solid not present to ussnp                               
c                                                                               
c     note- should test on jkflag here.  then we might be able to               
c     eliminate the test below on sum - tjw                                     
c                                                                               
      if (jmflag(nm) .ge. 2) then                                               
        ussnp(ik,nx) = umin(nm)                                                 
        nend(ik,nx)=0                                                           
        nm = 0                                                                  
      endif                                                                     
      if (nm .eq. 0) then                                                       
        nn=nn-1                                                                 
        go to 10                                                                
      endif                                                                     
c                                                                               
c     get q/k values from array arr and sum them                                
c                                                                               
      xxx = aff(nm)/afkst                                                       
      xqkx(ik) = texp(xxx)                                                      
      sum = sum+ xqkx(ik)                                                       
   10 continue                                                                  
c                                                                               
c     return if no components present                                           
c                                                                               
      if (nn .lt. 1) go to 999                                                  
c     save this nn value                                                        
      nold = nn                                                                 
c                                                                               
c     return if all components are effectively suppressed                       
c                                                                               
      if (sum .le. 0.) go to 999                                                
c                                                                               
c   now calculate composition assuming ideal solution                           
c                                                                               
c     analytic solution is this:                                                
c                                                                               
c        xi = q/k / (sum of all the q/k values)                                 
c                                                                               
      do 30 i=1,ncomp(nx)                                                       
c     initialize itracx array                                                   
      itracx(i) = 0                                                             
      if (nend(i,nx).eq.0) go to 30                                             
      xbar(i,nx) = xqkx(i)/sum                                                  
      if (xbar(i,nx).lt.0.) write (noutpl,*)'xbar(',i,nx,'< 0'                  
c                                                                               
c     check for trace components - if one is present and a non-ideal            
c     solution model is operative, remove that component and recalculate        
c     its mole fraction after the non-ideal solution is obtained.               
c                                                                               
      if (xbar(i,nx).lt.1.0e-08) then                                           
c       reduce number of active components                                      
        nn = nn-1                                                               
c       flag for trace components                                               
        itracx(i) = 1                                                           
c       log mole fraction                                                       
      endif                                                                     
      xbarlg(i,nx) = tlg(xbar(i,nx))                                            
   30 continue                                                                  
c                                                                               
c     compute value of saturation index and affinity                            
c                                                                               
c     afflcx - affinity for solid solution                                      
c     affx   - affinity for endmember of solid solution                         
c     si     - saturation index for solid solution endmember                    
c     siss   - saturation index for solid solution                              
c                                                                               
      siss(nx) = 0.0                                                            
      do 110 i=1,ncomp(nx)                                                      
c     skip if component not present                                             
      if (nend(i,nx).eq.0) go to 110                                            
c     def of si for endmember                                                   
      xbarf = xbar(i,nx)                                                        
      if (xbarf.le.0.) xbarf=1.0e-25                                            
      si(i,nx) =  tlg(xqkx(i)/xbarf)                                            
c     affinity for endmember                                                    
      affx(i,nx)= afkst*si(i,nx)                                                
c     affinity for solid solution                                               
      siss(nx) = siss(nx)+si(i,nx)*xbar(i,nx)                                   
  110 continue                                                                  
c     si for solid solution                                                     
      afflcx(nx) = siss(nx)*afkst                                               
c                                                                               
c     output results to dbug1                                                   
c                                                                               
      if (idbg.le.0) go to 1001                                                 
      write (noutpl,131)                                                        
  131 format(' for ideal solution')                                             
      write (noutpl,121)                                                        
      sum = 0.0                                                                 
      do 120 i=1,ncomp(nx)                                                      
      write (noutpl,122) i,100.*xbar(i,nx),xlkm(nend(i,nx))                     
      sum = sum+xbar(i,nx)                                                      
  120 continue                                                                  
      write (noutpl,123) sum*100.                                               
  121 format(' component  mole percent     ksp')                                
  122 format(4x,i2,7x,g9.3,5x,f6.2,5x,g10.4,1x,g10.3)                           
  123 format(9x,'sum=',f6.1)                                                    
      write (noutpl,1050) afflcx(nx)                                            
 1050 format(2x,'optimum value =',1pd10.2)                                      
c                                                                               
c     test for non-ideal solution                                               
c                                                                               
 1001 if (jsol(nx).eq.0) go to 1000                                             
c     if only one major component-abort                                         
      if (nn.eq.1) go to 1000                                                   
c                                                                               
c     non-ideal solid solution                                                  
c                                                                               
      data nsize/12/,alpha/1.0,0.5,2.0/,                                        
     *     deps/0.0,1.0e-7/,imin/1/,amult/0.05/                                 
c     initialize max number iterations                                          
      itr = 200                                                                 
c                                                                               
c     parameters needed by dsiplx                                               
c     itr       - maximum number of iterations                                  
c     imin      - find maximum for function when imin=1                         
c     alpha     - see subroutine writeup                                        
c     amulvt    - multiplier for simplex grid                                   
c     deps(1)    - absolute precision desired  (= 0.0)                          
c     deps(2)    - maximum difference between last two iterations.              
c                                                                               
c     put ideal solution into first column of matrix x that dsiplx uses         
c     as starting estimate                                                      
c                                                                               
      nnm1 = nn-1                                                               
      j = 0                                                                     
c     strip array holding composition of                                        
      do 14 i=1,nnm1                                                            
c     all absent and trace components                                           
      j = j+1                                                                   
   15 if (nend(j,nx).ne.0. .and.                                                
     *    itracx(j).eq.0) go to 16                                              
      j = j+1                                                                   
      go to 15                                                                  
   16 axx(i,1)=xbar(j,nx)                                                       
   14 continue                                                                  
c                                                                               
c     create simplex net                                                        
c                                                                               
      do 13 i=1,nnm1                                                            
c     put initial guess in last col                                             
      axx(i,nn) = amult*axx(i,1)                                                
   13 continue                                                                  
      if (idbg.ge.1) write (noutpl,1002)                                        
 1002 format(5x,'  mole fractions                function value')               
c                                                                               
c     call simplex routine                                                      
c                                                                               
      call dsiplx (ssfunc,axx,nnm1,nsize,alpha,deps,imin,itr,xbarh,fmin,        
     $        nit,nend,w,ndbug1,nttyol,noutpl,xbar,lamlg,ncomp,iktmax,          
     $        jsol,nn,xqkx,nx,tempk,press,al10,rconst,afkst,idbg)               
c                                                                               
c     composition of solid solution is now in array 'xbarh'                     
c     this array contains only nn-1 mole fractions, the last                    
c     component is fixed by the sum of the others.                              
c                                                                               
      sum = 0.                                                                  
      do 9 i=1,nnm1                                                             
      sum = sum+xbarh(i)                                                        
    9 continue                                                                  
c     composition of last component                                             
      xbarh(nn) = 1.0-sum                                                       
c                                                                               
c     convert array xbarh back to xbar putting back in zeros for                
c     components not present in current problem                                 
c                                                                               
      j=1                                                                       
c     nn # components present, check to see if xbarh is present,                
c     if not, increment index                                                   
      do 26 i=1,nn                                                              
   44 if (nend(i,nx).ne.0  .and.                                                
     *   itracx(i).eq.0) go to 45                                               
      j=j+1                                                                     
      go to 44                                                                  
   45 xbar(j,nx) = xbarh(i)                                                     
      if (xbar(j,nx).lt.0.) write (noutpl,*)'xbar(',j,nx,' < 0'                 
c     log mole fraction                                                         
      xbarlg(j,nx) = tlg(xbarh(i))                                              
      j=j+1                                                                     
   26 continue                                                                  
c                                                                               
c     recompute mole fractions of trace components using activity               
c     coefficients for major components                                         
c                                                                               
      call lamda(al10,jsol,lamlg,ncomp,nx,rconst,tempk,xbar,w,iktmax)           
c     initialize sum of q/k values                                              
      sum = 0.0                                                                 
      do 17 i=1,ncomp(nx)                                                       
      nm = nend(i,nx)                                                           
      if (nm.eq.0) go to 17                                                     
      xxx = lamlg(i,nx)                                                         
      lamdax = texp(xxx)                                                        
      sum = sum+ xqkx(i)/lamdax                                                 
   17 continue                                                                  
c     recalculate only trace components                                         
      do 32 i=1,ncomp(nx)                                                       
      if (nend(i,nx).eq.0 .or. itracx(i).eq.0) go to 32                         
      xbar(i,nx) = xqkx(i)/sum                                                  
      xbarlg(i,nx) = tlg(xbar(i,nx))                                            
   32 continue                                                                  
      siss(nx) = 0.0                                                            
      do 111 i=1,ncomp(nx)                                                      
      lamdax = 10**lamlg(i,nx)                                                  
c     skip if component not present                                             
      if (nend(i,nx).eq.0) go to 111                                            
c     def of si for endmember                                                   
      si(i,nx) =  tlg(xqkx(i)/xbar(i,nx)/lamdax)                                
c     affinity for endmember                                                    
      affx(i,nx)= afkst*si(i,nx)                                                
c     affinity for solid solution                                               
      siss(nx) = siss(nx)+si(i,nx)*xbar(i,nx)                                   
  111 continue                                                                  
c                                                                               
c     si for solid solution                                                     
      afflcx(nx) = siss(nx)*afkst                                               
c                                                                               
c     now print it out                                                          
c                                                                               
c     skip debug output if desired                                              
      if (idbg.eq.0) go to 1000                                                 
c                                                                               
      write (noutpl,126)                                                        
      sum = 0.0                                                                 
      do 125 i=1,ncomp(nx)                                                      
      write (noutpl,127)i,100.*xbar(i,nx),xlkm(nend(i,nx)),                     
     *  10**lamlg(i,nx)                                                         
      sum = sum+xbar(i,nx)                                                      
  125 continue                                                                  
      write (noutpl,128) sum*100.                                               
  126 format(' component  mole percent     ksp        act coef')                
  127 format(4x,i2,7x,g9.3,5x,f6.2,5x,g10.4)                                    
  128 format(9x,'sum=',f6.1)                                                    
      write (noutpl,1051)nit,ier,fmin,(xbarh(j),j=1,ncomp(nx))                  
 1051 format(2x,'itr=',i5,5x,'ier=',i2,                                         
     */2x,'optimum value =',1pd10.2,                                            
     */2x ,'mole fractions = ',10(/,g11.3))                                     
 1000 continue                                                                  
      nn = nold                                                                 
c                                                                               
  999 continue                                                                  
      end                                                                       
c indatc  last revised 12/02/87 by tjw                                          
c*indatc created in f77 12/02/87 by tjw                                         
      subroutine indatc(arr,nf,narxmx,ntprmx)                                   
c                                                                               
c     this routine reads an unformatted data file in order to load              
c     a two-dimensional array arr with polynomial coefficients                  
c     for describing a thermodynamic property as a function of                  
c     temperature.  this routine is identical to indatk.f, except               
c     that in that routine the array arr is three-dimensional and its           
c     calling sequence contains an additional variable.                         
c                                                                               
c     input                                                                     
c       nf = file unit number of an unformatted data file                       
c       narxmx = first dimension of the arr array, the number                   
c                of coefficients per temperature range                          
c       ntprmx = second dimension of the arr array, the number                  
c                of temperature ranges.                                         
c                                                                               
c     output                                                                    
c       arr = two dimensional array of polynomial coefficients                  
c             describing some thermodynamic function                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension arr(narxmx,*)                                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     read (nf) ustr,((arr(i,j), i=1,narxmx), j=1,ntprmx)                       
      read (nf) ustr,(arr(i,1), i=1,narxmx),(arr(i,2), i=1,narxmx)              
c                                                                               
      end                                                                       
c indatk  last revised 12/01/87 by tjw                                          
c*indatk created in f77 12/01/87 by tjw                                         
      subroutine indatk(k,arr,nf,narxmx,ntprmx)                                 
c                                                                               
c     this routine reads an unformatted data file in order to load              
c     a three-dimensional array arr with polynomial coefficients                
c     for describing a thermodynamic property as a function of                  
c     temperature.  this routine is identical to indatk.c, except               
c     that in that routine the array arr is two-dimensional and                 
c     its calling seqence does not contain the variable k.                      
c                                                                               
c     input                                                                     
c       k = index for the third dimension of array arr                          
c       nf = file unit number of an unformatted data file                       
c       narxmx = first dimension of the arr array, the number                   
c                of coefficients per temperature range                          
c       ntprmx = second dimension of the arr array, the number                  
c                of temperature ranges.                                         
c                                                                               
c     output                                                                    
c       arr = two dimensional array of polynomial coefficients                  
c             describing some thermodynamic function                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension arr(narxmx,ntprmx,*)                                            
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     read (nf) ustr,((arr(i,j,k), i=1,narxmx), j=1,ntprmx)                     
      read (nf) ustr,(arr(i,1,k), i=1,narxmx),(arr(i,2,k), i=1,narxmx)          
c                                                                               
      end                                                                       
c inupt    last revised 01/25/88 by rmm                                         
c*inupt f77 rewrite, 07/06/87 by tjw                                            
      subroutine inupt(uspec,jsflag,ndats1,nst,iopr9)                           
c                                                                               
c     adapted from indpt 1/20/87 by rlh                                         
c     read pitzer parameter base values from                                    
c      unformatted data file ndats1                                             
c                                                                               
c      input  uspec   array of species names                                    
c             jsflag  array of status switches, aqueous species                 
c             ndats1  unit number of unformatted input file                     
c             nst     number of aqueous species                                 
c                                                                               
c      output mu values in common /eqlpmu/                                      
c             lambda values in common /eqlpsa/                                  
c             associated alpha values in common /eqlpal/                        
c                                                                               
c     ndats1 is assumed to be positioned after the line                         
c     'endit.' which terminates the solid solution data                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqlpal.h"                                                 
      include "eqlpmu.h"                                                 
      include "eqlpmb.h"                                                 
      include "eqlpsa.h"                                                 
      include "eqlpsb.h"                                                 
      include "eqlgp.h"                                                  
      include "eqlun.h"                                                  
c                                                                               
      dimension jsflag(*)                                                       
      dimension lam(3),alph(2)                                                  
      dimension dl1(3),dl2(3)                                                   
c                                                                               
      character*24 uspec(*),unam1,unam2,unam3                                   
      character*80 uline                                                        
      character*72 udum72                                                       
c                                                                               
      data ufnam /'dpt2    '/                                                   
      data uendit /'endit.  '/                                                  
      data uterm /'+-------'/                                                   
      data uoff/'off     '/,uon/'on      '/,uonp/'onplus  '/                    
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c      read header line                                                         
c                                                                               
      read (ndats1) udum                                                        
      if (udum .eq. ufnam) go to 10                                             
      write (noutpl,1003) udum,ufnam                                            
      write (nttyol,1003) udum,ufnam                                            
 1003 format(' * error - data file error at start of pitzer data',/             
     $  3x,'key= ',a8,', correct key= ',a8,/                                    
     $  5x,'append the correct pitzer data (in lambda and mu form) to',         
     $  ' data1 or data2 and try again',/5x,'do not change the key',            
     $  ' (eqlib/inupt)')                                                       
      stop                                                                      
c                                                                               
   10 continue                                                                  
c                                                                               
c     read a four line title                                                    
c                                                                               
      do 5 i=1,4                                                                
      read (ndats1) udpitz(i)                                                   
    5 continue                                                                  
c                                                                               
c                                                                               
c     read and decode the elambda flag                                          
c                                                                               
      uelam = ' '                                                               
      read (ndats1) uline                                                       
      uelam(1:6) = uline(16:21)                                                 
      if (uelam.ne.uon) go to 305                                               
      ielam=0                                                                   
      go to 320                                                                 
  305 if (uelam.ne.uoff) go to 310                                              
      ielam=-1                                                                  
      go to 320                                                                 
  310 if (uelam.ne.uonp) go to 315                                              
      ielam=1                                                                   
      go to 320                                                                 
  315 write (noutpl,317) uelam                                                  
      write (nttyol,317) uelam                                                  
  317 format(' * error - bad elambda flag on the data file = ',a8,/             
     $ 3x,'allowed values are off/on/onplus (eqlib/inupt)')                     
      stop                                                                      
c                                                                               
  320 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * read lambda's and alpha's  (2 sets)             
c                             *----------------------------------------         
      ja = 0                                                                    
      jj = 0                                                                    
c                                                                               
      do 100 kk=1,2                                                             
c                                                                               
   15 continue                                                                  
c                                                                               
c      read a block                                                             
c       species names                                                           
c                                                                               
      read (ndats1) unam1, unam2                                                
c1050 format(a12,2x,a12)                                                        
      if (unam1(1:8) .eq. uendit) go to 100                                     
c                                                                               
c      lambda's                                                                 
c                                                                               
      read (ndats1) (lam(i),i=1,3)                                              
c1052 format(3(13x,f8.4))                                                       
c                                                                               
c      alpha's                                                                  
c                                                                               
      read (ndats1) (alph(i),i=1,2)                                             
c1054 format(18x,2(16x,f5.1))                                                   
c                                                                               
c      derivatives                                                              
c                                                                               
      do 350 i=1,3                                                              
      read (ndats1) dl1(i),dl2(i)                                               
c1056 format(13x,e10.4,13x,e10.4)                                               
  350 continue                                                                  
c                                                                               
c      skip to block terminator                                                 
c                                                                               
   18 continue                                                                  
      read (ndats1) udum72                                                      
      if (udum72(1:8) .ne. uterm) go to 18                                      
c                                                                               
c      map 2 species names to indices                                           
c                                                                               
      call srchn(uspec,nst,unam1,ix1)                                           
      call srchn(uspec,nst,unam2,ix2)                                           
c                                                                               
c      skip if either species not found                                         
c                                                                               
      if (ix1 .le. 0 .or. ix2 .le. 0) go to 15                                  
c                                                                               
c      skip if either species is not in current model                           
c                                                                               
      if (jsflag(ix1) .ne. 0 .or. jsflag(ix2) .ne. 0) go to 15                  
c                                                                               
c      put two indices into common                                              
c                                                                               
      jj = jj+1                                                                 
      if (jj .le. nslmt) go to 22                                               
      write (noutpl,1014)                                                       
      write (nttyol,1014)                                                       
 1014 format(' * error - lambda array index overflow (eqlib/inupt)')            
      stop                                                                      
c                                                                               
   22 continue                                                                  
      nslmx(1,jj) = ix1                                                         
      nslmx(2,jj) = ix2                                                         
c                                                                               
c      put lambda's and derivatives into common                                 
c                                                                               
      do 25 k=1,3                                                               
      bslm(k,jj) = lam(k)                                                       
      dslm1(k,jj) = dl1(k)                                                      
      dslm2(k,jj) = dl2(k)                                                      
   25 continue                                                                  
c                                                                               
c      test for alpha pair in palpha array                                      
c                                                                               
      if (ja .eq. 0) go to 32                                                   
c                                                                               
      do 30 k=1,ja                                                              
      if (palpha(1,k) .ne. alph(1)) go to 30                                    
      if (palpha(2,k) .ne. alph(2)) go to 30                                    
c                                                                               
c      found a pair at k                                                        
c                                                                               
      alx = k                                                                   
      go to 40                                                                  
c                                                                               
   30 continue                                                                  
c                                                                               
   32 continue                                                                  
c                                                                               
c      pair not in palpha - put them in                                         
c                                                                               
      ja = ja+1                                                                 
      if (ja .le. napt) go to 35                                                
      write (noutpl,1020)                                                       
      write (nttyol,1020)                                                       
 1020 format(' * error - palpha array index overflow (eqlib/inupt)')            
      stop                                                                      
c                                                                               
   35 continue                                                                  
      alx = ja                                                                  
      palpha(1,ja) = alph(1)                                                    
      palpha(2,ja) = alph(2)                                                    
   40 continue                                                                  
      nalpha(jj) = alx                                                          
      go to 15                                                                  
c                                                                               
  100 continue                                                                  
c                                                                               
c      set number of entries, large arrays                                      
c                                                                               
      nslm = jj                                                                 
c-----------------------------------------------------------------------        
c                             *----------------------------------------         
c                             * read mu's  (2 sets)                             
c                             *----------------------------------------         
      jj = 0                                                                    
c                                                                               
      do 200 kk = 1,2                                                           
c                                                                               
  215 continue                                                                  
c                                                                               
c      read a block                                                             
c       species names                                                           
c                                                                               
      read (ndats1) unam1,unam2,unam3                                           
c1060 format(a12,2(2x,a12))                                                     
      if (unam1(1:8) .eq. uendit) go to 200                                     
c                                                                               
c      mu, derivative                                                           
c                                                                               
      read (ndats1) mu,deriv1,deriv2                                            
c1062 format(13x,f9.5,12x,e10.3,14x,e10.3)                                      
c                                                                               
c      note - kk=1  deriv1 is dcphi/dt, deriv2 is d2cphi/dt2                    
c                2            dpsi/dt,            d2psi/dt2                     
c                                                                               
c      skip to block terminator                                                 
c                                                                               
  218 continue                                                                  
      read (ndats1) udum72                                                      
      if (udum72(1:8) .ne. uterm) go to 218                                     
c                                                                               
c      map 3 species names to indices                                           
c                                                                               
      call srchn(uspec,nst,unam1,ix1)                                           
      call srchn(uspec,nst,unam2,ix2)                                           
      call srchn(uspec,nst,unam3,ix3)                                           
c                                                                               
c      skip if any species name not found                                       
c                                                                               
      if (ix1 .le. 0 .or. ix2 .le. 0 .or. ix3 .le. 0) go to 215                 
c                                                                               
c      skip if any of 3 species is not in current model                         
c                                                                               
      if (jsflag(ix1) .ne. 0                                                    
     $   .or. jsflag(ix2) .ne. 0                                                
     $   .or. jsflag(ix3) .ne. 0) go to 215                                     
c                                                                               
c      put indices into common                                                  
c                                                                               
      jj = jj+1                                                                 
      if (jj .le. nmut) go to 222                                               
      write (noutpl,1032)                                                       
      write (nttyol,1032)                                                       
 1032 format(' * error -  mu array index overflow (eqlib/inupt)')               
      stop                                                                      
c                                                                               
  222 continue                                                                  
c                                                                               
      nmux(1,jj) = ix1                                                          
      nmux(2,jj) = ix2                                                          
      nmux(3,jj) = ix3                                                          
c                                                                               
c      put mu and derivatives into common                                       
c                                                                               
      bmu(jj) = mu                                                              
      dmu1(jj) = deriv1                                                         
      dmu2(jj) = deriv2                                                         
      go to 215                                                                 
c                                                                               
  200 continue                                                                  
c                                                                               
      nmu = jj                                                                  
      call ptztab(nslm,nslmx,nmu,nmux,nst,uspec,jsflag,iopr9)                   
c                                                                               
      end                                                                       
c isamax   last revised 07/07/87 by tjw                                         
c*isamax f77 rewrite, 07/07/87 by tjw                                           
      integer function isamax(n,sx)                                             
c***date written  october 1979                                                  
c***author lawson c. (jpl),hanson r. (sla),                                     
c                            kincaid d. (u texas), krogh f. (jpl)               
c***purpose                                                                     
c    find largest component of s.p. vector                                      
c***description                                                                 
c                b l a s  subprogram                                            
c    description of parameters                                                  
c                                                                               
c     --input--                                                                 
c        n  number of elements in input vector(s)                               
c       sx  single precision vector with n elements                             
c                                                                               
c     --output--                                                                
c   isamax  smallest index (zero if n.le.0)                                     
c                                                                               
c     find smallest index of maximum magnitude of single precision sx.          
c      isamax = first i, i=1 to n, to minimize abs(sx(i))                       
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension sx(*)                                                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      isamax = 0                                                                
      if (n.le.0) go to 999                                                     
      isamax = 1                                                                
      if (n.le.1) go to 999                                                     
c                                                                               
      smax = abs(sx(1))                                                         
      do 30 i = 2,n                                                             
      xmag = abs(sx(i))                                                         
      if (xmag.gt.smax) then                                                    
        isamax = i                                                              
        smax = xmag                                                             
      endif                                                                     
   30 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c iswch    last revised 07/07/87 by tjw                                         
c*iswch f77 rewrite, 07/07/87 by tjw                                            
      subroutine iswch(n1,n2,i)                                                 
c                                                                               
c     this routine is called by routine swchlm, which is in turn                
c     called by routine switch                                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (i .eq. n1) then                                                       
        i = n2                                                                  
      elseif (i .eq. n2) then                                                   
        i = n1                                                                  
      endif                                                                     
c                                                                               
      end                                                                       
c itrefn   last revised 07/07/87 by tjw                                         
c*itrefn f77 rewrite, 07/07/87 by tjw                                           
      subroutine itrefn(a,alu,ilda,n,ipvt,b,x0,rcond,res,                       
     $ x,qprnt,ier)                                                             
c                                                                               
c     purpose-                                                                  
c                                                                               
c       iterative refinement of a solution to the linear system ax=b,           
c       real*8 case                                                             
c                                                                               
c     description-                                                              
c                                                                               
c       itrefn is designed to work with the modified slatek linear              
c       system routines  sgeco, sgefa, and sgesl.  itrefn computes              
c       residuals, using a partial product algorithm, and uses                  
c       these to obtain a newton-raphson correction to the x vector.            
c       the best possible value of x (as measured by the lowest norm            
c       of the residual vector) is returned as the solution.  this may          
c       be the input vector x.                                                  
c                                                                               
c       convergence is achieved if either                                       
c         1. rsnrm .le. 0.                                                      
c         2. (xnrm+dxnrm).eq.xnrm (additive underflow)                          
c         3. xtest.le.0. (when iter. ge. 3)                                     
c                                                                               
c       other stop criteria are                                                 
c         1. ten iterations without convergence                                 
c         2. rfunc .le. 0. (iter .ge. 1) (residuals are not decreasing)         
c         3. dfunc .le. 0. (iter. ge. 2) (errors in x are not                   
c            decreasing)                                                        
c                                                                               
c       note- if the condition number of the matrix (cond = 1/rcond)            
c       becomes very high, then the likelihood of convergence becomes           
c       poor.  this is a result of the imperfect representation of              
c       the matrix and the right-hand-side vector by a 51 bit mantissa          
c       (Ridge), combined with ill-conditioning of the                          
c       matrix.                                                                 
c                                                                               
c     reference-                                                                
c        wilkinson, j.w. (1967)  the solution of ill-conditioned                
c      linear equations.  in ralston, a., and wilf, h.s., eds.,                 
c      mathematical methods for digital computers, vol. 2,                      
c      john wiley and sons, pages 65-93.                                        
c                                                                               
c     parameters-                                                               
c                                                                               
c       on entry                                                                
c                                                                               
c         a       real (ilda,n)  the original matrix                            
c         alu     real (ilda,n)  the l-u decomposition                          
c                         ( output from sgeco or sgesl)                         
c         ilda    integer, the leading dimension of the array a                 
c         n       the order of the matrix a                                     
c         ipvt    integer array, the pivot vector from sgeco or sgesl           
c         b       the right-hand-side vector                                    
c         x0      the solution vector from sgeco or sgesl, contains             
c                 the improved vector, if any improvement, upon return          
c         rcond   the reciprocal condition number of the matrix                 
c         qprnt   print option switch, turned on if .true                       
c                                                                               
c       internal                                                                
c                                                                               
c         iter    the iteration counter                                         
c         itermx  the maximum number of iterations                              
c         res     a working vector, contains the residual vector upon           
c                 entry to routine sgesl, contains the correction               
c                 upon return from it                                           
c         rsnrm   the norm of the residual vector                               
c         rsnrm0  the smallest rsnrm encountered                                
c         rsnrmi  the rsnrm of the input x vector                               
c         dsnrm   norm of the correction vector (the res vector,                
c                 upon return from routine sgesl)                               
c         xnrm    norm of x vector                                              
c         dxnrm   norm of the correction vector (dx)                            
c         dx      the correction vector, actually the array res                 
c         dxbig   the value of the element of dx with the largest               
c                 absolute value                                                
c         xerr    error in x, measured by dxnrm/xnrm                            
c         xerrp   the value of xerr on the previous iteration                   
c         xtest   convergence tolerance test parameter-                         
c                 xtest.le.0. is equivalent to xerr.le.eps, where               
c                 eps is the machine epsilon.                                   
c         dfunc   convergence function defined by (xerrp-xerr)/xerrp            
c         x       the working x vector                                          
c         x0      the x vector encountered with the smallest residual           
c         sum     variable for accumulating the terms in the residual           
c                 vector, must be higher than standard precision, else          
c                 iterative refinement should be skipped.                       
c                                                                               
c       on return                                                               
c                                                                               
c         x0      the improved solution vector, or the input vector             
c                 if there was no improvement                                   
c         ier     error flag, returns warning values only                       
c                   =   0  converged, satisfied (dxnrm+xnrm) .eq. xnrm          
c                          or xtest .le. 0.                                     
c                   =  -1  did not converge, but the error, as measured         
c                          by xerr was reduced                                  
c                   =  -2  did not converge, the error was not reduced.         
c                          the input solution vector was returned               
c                          as the answer                                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      parameter(imaxp=100,imaxp2=200)                                           
c                                                                               
      dimension dotp1(imaxp)                                                    
      dimension dotp2(imaxp2)                                                   
      dimension dotp3(imaxp)                                                    
      dimension dotm1(imaxp)                                                    
      dimension dotm2(imaxp2)                                                   
      dimension dotm3(imaxp)                                                    
      dimension a(ilda,*),alu(ilda,*),ipvt(*),b(*),x(*),res(*),x0(*)            
c                                                                               
      data itermx/10/                                                           
c     const = 2**23                                                             
      data const /8388608./                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      imax = imaxp                                                              
      imax2 = imaxp2                                                            
      ier = 0                                                                   
      if (qprnt) then                                                           
        write (noutpl,370) rcond                                                
  370   format(1x,'--- itrefn summary ---',                                     
     $   5x,'rcond= ',e10.3)                                                    
        write (noutpl,377)                                                      
  377   format(/5x,'iter',6x,'rsnrm',8x,'rfunc',8x,'xerr',9x,'dfunc',8x,        
     $ 'dxnrm',8x,'xnrm',8x,'dxbig',/)                                          
      endif                                                                     
c                                                                               
c     copy the x0 vector into the working vector x                              
c                                                                               
      do 7 i=1,n                                                                
      x(i)=x0(i)                                                                
    7 continue                                                                  
c                                                                               
c     return if degree = 1                                                      
c                                                                               
      if (n.le.1) then                                                          
        if (qprnt) write (noutpl,375)                                           
  375   format(1x,'--- itrefn - n = 1, no iteration ---')                       
        go to 999                                                               
      endif                                                                     
c                                                                               
      dfunc=0.                                                                  
      rfunc=0.                                                                  
      iter=0                                                                    
      xerr=0.                                                                   
      rsnrm=0.                                                                  
      dxnrm=0.                                                                  
      dxbig=0.                                                                  
      xnrm=abs(x(1))                                                            
      do 17 i=2,n                                                               
      ax=abs(x(i))                                                              
      if (xnrm .lt. ax) xnrm=ax                                                 
   17 continue                                                                  
c                                                                               
c     this is a return point for further iteration                              
c                                                                               
   20 rsnrmp=rsnrm                                                              
c                                                                               
c     compute residual vector and its norm                                      
c                                                                               
      rsnrm=0.                                                                  
      do 50 i=1,n                                                               
      dp1max = 0.                                                               
      dp2max = 0.                                                               
      dp3max = 0.                                                               
      dp1min = 1.e30                                                            
      dp2min = 1.e30                                                            
      dp3min = 1.e30                                                            
c                                                                               
      dm1max = 0.                                                               
      dm2max = 0.                                                               
      dm3max = 0.                                                               
      dm1min = 1.e30                                                            
      dm2min = 1.e30                                                            
      dm3min = 1.e30                                                            
c                                                                               
      ip1 = 0                                                                   
      ip2 = 0                                                                   
      ip3 = 0                                                                   
      im1 = 0                                                                   
      im2 = 0                                                                   
      im3 = 0                                                                   
c                                                                               
      do 40 j=1,n                                                               
c                                                                               
c      split x(j) and a(i,j)                                                    
c       for example, x1 = high order part of x,                                 
c                    x2 = low  order part of x                                  
c                                                                               
      x2 = mod(x(j),const)                                                      
      x1 = (x(j) - x2)/const                                                    
      a2 = mod(a(i,j),const)                                                    
      a1 = (a(i,j) - a2)/const                                                  
c                                                                               
c                                                                               
c      compute the partial products,                                            
c       place into dotp or dotm arrays                                          
c       find max and min values for each array                                  
c                                                                               
      prod = a1*x1                                                              
      call stpp(prod,dotp1,ip1,dp1max,dp1min,                                   
     $   dotm1,im1,dm1max,dm1min,imax)                                          
c                                                                               
      prod = a1*x2                                                              
      call stpp(prod,dotp2,ip2,dp2max,dp2min,                                   
     $   dotm2,im2,dm2max,dm2min,imax2)                                         
c                                                                               
      prod = a2*x1                                                              
      call stpp(prod,dotp2,ip2,dp2max,dp2min,                                   
     $   dotm2,im2,dm2max,dm2min,imax2)                                         
c                                                                               
      prod = a2*x2                                                              
      call stpp(prod,dotp3,ip3,dp3max,dp3min,                                   
     $   dotm3,im3,dm3max,dm3min,imax)                                          
   40 continue                                                                  
c                                                                               
c      put b(i) into appropriate dotp or dotm array                             
c                                                                               
c      put into some dotp                                                       
c                                                                               
        ba = -b(i)                                                              
        b3 = mod(ba,const)                                                      
        b2 = mod((ba-b3)/const,const)                                           
        b1 = (ba-b2*const-b3)/(const*const)                                     
        if (b3 .ne. 0) then                                                     
          ip3 = ip3+1                                                           
          dotp3(ip3) = b3                                                       
        endif                                                                   
        if (b2 .ne. 0) then                                                     
          ip2 = ip2+1                                                           
          dotp2(ip2) = b2                                                       
        endif                                                                   
        if (b1 .ne. 0) then                                                     
          ip1 = ip1+1                                                           
          dotp1(ip1) = b1                                                       
        endif                                                                   
c                                                                               
c      sort into increasing order, and sum each set                             
c                                                                               
      call srtsum(dotp1,ip1,sump1)                                              
      call srtsum(dotp2,ip2,sump2)                                              
      call srtsum(dotp3,ip3,sump3)                                              
c                                                                               
c                                                                               
      d1 = -sump1                                                               
      d2 = -sump2                                                               
      d3 = -sump3                                                               
c                                                                               
      d2 = d2*const                                                             
      d1 = d1*const*const                                                       
      rdum = d3+d2+d1                                                           
c                                                                               
      res(i)=rdum                                                               
      rdum=abs(rdum)                                                            
      if (rdum.gt.rsnrm) rsnrm=rdum                                             
   50 continue                                                                  
c                                                                               
      if (iter.le.0) then                                                       
        rsnrm0=rsnrm                                                            
        rsnrmi=rsnrm                                                            
      endif                                                                     
c                                                                               
      if (iter.gt.0) then                                                       
        if (rsnrm.lt.rsnrm0) then                                               
c                                                                               
c         have found a better solution, load it into x0                         
c                                                                               
          rsnrm0=rsnrm                                                          
          do 240 i=1,n                                                          
          x0(i)=x(i)                                                            
  240     continue                                                              
        endif                                                                   
      endif                                                                     
c                                                                               
      if (iter.ge.1) rfunc=(rsnrmp-rsnrm)/rsnrmp                                
      if (iter.ge.2) dfunc=(xerrp-xerr)/xerrp                                   
      if (qprnt) write (noutpl,380) iter,rsnrm,rfunc,xerr,dfunc,                
     $ dxnrm,xnrm,dxbig                                                         
  380 format(6x,i2,7(3x,e10.3))                                                 
c                                                                               
c     convergence tests                                                         
c                                                                               
      if (rsnrm.le.0.) go to 90                                                 
      if (iter.gt.0) then                                                       
        if ((xnrm+dxnrm).eq.xnrm) go to 90                                      
      endif                                                                     
      if (iter.le.2) go to 53                                                   
      xtest=xerr+1.0                                                            
      xtest=xtest-1.0                                                           
      if (xtest.le.0.) go to 90                                                 
c                                                                               
c     stop tests                                                                
c                                                                               
   53 if (iter.gt.0) then                                                       
        if (rfunc.le.0.) go to 100                                              
      endif                                                                     
      if (iter.gt.2) then                                                       
        if (dfunc.le.0.) go to 100                                              
      endif                                                                     
c                                                                               
      if (iter.ge.itermx) go to 100                                             
      iter=iter+1                                                               
c                                                                               
c     solve the system a(dx) = res                                              
c                                                                               
      call sgesl(alu,ilda,n,ipvt,res)                                           
c                                                                               
c     note that res now holds the correction vector (symbolized by dx).         
c     correct the x vector and compute its norm.                                
c                                                                               
      dxi=res(1)                                                                
      dxbig=dxi                                                                 
      dxnrm = abs(dxi)                                                          
      xi=x(1)                                                                   
      xi = xi + dxi                                                             
      x(1)=xi                                                                   
      xnrm= abs(xi)                                                             
c                                                                               
      do 60 i=2,n                                                               
      dxi = res(i)                                                              
      adx = abs(dxi)                                                            
      if (dxnrm .lt. adx) then                                                  
        dxnrm = adx                                                             
        dxbig=dxi                                                               
      endif                                                                     
      xi=x(i)                                                                   
      xi = xi + dxi                                                             
      x(i)=xi                                                                   
      ax = abs(xi)                                                              
      if (xnrm .lt. ax) xnrm = ax                                               
   60 continue                                                                  
c                                                                               
      xerrp=xerr                                                                
      xdum=xnrm                                                                 
      if (xdum.le.0.) xdum=1.                                                   
      xerr=dxnrm/xdum                                                           
c                                                                               
      go to 20                                                                  
c                                                                               
c     convergence                                                               
c                                                                               
   90 if (qprnt) then                                                           
        write (noutpl,387)                                                      
  387   format(' --- itrefn converged ---')                                     
      endif                                                                     
      go to 999                                                                 
c                                                                               
c     no convergence.  determine whether or not the x vector was                
c     improved                                                                  
c                                                                               
  100 qrsnrm=rsnrm0.lt.rsnrmi                                                   
      if (.not.qrsnrm) then                                                     
        ier=-2                                                                  
        if (qprnt) then                                                         
          write (noutpl,300)                                                    
  300     format(' --- itrefn- the x vector was not improved ---')              
        endif                                                                   
        go to 999                                                               
      endif                                                                     
c                                                                               
      ier=-1                                                                    
      if (qprnt) then                                                           
        write (noutpl,305)                                                      
  305   format(' --- itrefn- the x vector was improved but not to',             
     $  ' within convergence tolerance ---')                                    
        write (noutpl,307) rsnrm0,rsnrmi                                        
  307   format(5x,'best norm of residual = ',e10.3,                             
     $  ', norm upon entry = ',e10.3)                                           
      endif                                                                     
c                                                                               
  999 continue                                                                  
      end                                                                       
c kilsum   last revised 07/07/87 by tjw                                         
c*kilsum f77 check, 07/07/87 by tjw                                             
      subroutine kilsum(ns,jkndex,ibjius,conc,bji,sum)                          
c                                                                               
c     created 11sep86 rmm                                                       
c                                                                               
c     purpose:  calculate fourth term in hkf equation for activity              
c               coefficient (part iv equations)                                 
c                                                                               
c      input  ns      index to ion of interest                                  
c             jkndex  array of indices of both ions in bji array                
c             ibjius  number of entries in bji array                            
c             conc    concentration of ions                                     
c             bji     array of ion-ion interaction parameters                   
c                                                                               
c      output sum     sum over j of (bji(j)*conc(j))                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension conc(*),bji(*),jkndex(2,*)                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sum = 0.0                                                                 
c                                                                               
      do 20 i=1,ibjius                                                          
      jj = jkndex(1,i)                                                          
      kk = jkndex(2,i)                                                          
c                                                                               
c         calculate the sum if:                                                 
c         ion of interest,ns, is present as first ion in pair                   
c         or if                                                                 
c         ion of interest present as second ion in pair                         
c                                                                               
      if (jj .eq. ns) then                                                      
        sum = sum + (bji(i)*conc(kk))                                           
      elseif (kk .eq. ns) then                                                  
        sum = sum + (bji(i)*conc(jj))                                           
      endif                                                                     
c                                                                               
   20 continue                                                                  
c                                                                               
      end                                                                       
c kisum    last revised 09/25/87 by rmm                                         
c*kisum f77 check, 07/07/87 by tjw                                              
      subroutine kisum(conc,xistoc,z,nst,sum)                                   
c                                                                               
c     created 11sep86 rmm                                                       
c                                                                               
c     purpose:  calculate third term in hkf equation for activity               
c               coefficient (part iv equations)                                 
c                                                                               
c     input   conc    concentration of ions                                     
c             xistoc  ionic strength                                            
c             z       charge array                                              
c             nst     total number of species in the model                      
c                                                                               
c      output sum     sum over k of (b(k)  * conc(k))                           
c                     = (1.e-11/xistoc)*sum over k of (conc(k)*                 
c                           sum over j of (nu sub j,k * omega(j))               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
      include "eqlpar.h"                                                 
      include "eqlhkf.h"                                                 
c                                                                               
      dimension conc(*),z(*)                                                    
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sum = 0.0                                                                 
      allslt = 0.0                                                              
c                                                                               
c              for each salt,k                                                  
      do 100 k = 1,icnt                                                         
      omecat=0.                                                                 
      omean=0.                                                                  
      omeslt=0.                                                                 
c                                                                               
c              get indexes and stoich. rxn. factor for each ion in salt         
      icat=islt(1,k)                                                            
      ian =islt(2,k)                                                            
      icatsf=islt(3,k)                                                          
      iansf=islt(4,k)                                                           
c                                                                               
c              get omega for ions                                               
      zcat=z(icat)                                                              
      zan=z(ian)                                                                
      call omega4(icat,omecat,zcat,ierr)                                        
      call omega4(ian,omean,zan,ierr)                                           
c                                                                               
c              calculate omega for salt                                         
c                                                                               
      omeslt=real(icatsf)*omecat + real(iansf)*omean                            
c                                                                               
c              calculate over all salts (product of molality and omega)         
c                                                                               
      mk = mslt(k)                                                              
      allslt=allslt + omeslt*mk                                                 
  100 continue                                                                  
c                                                                               
c              final value                                                      
c                                                                               
      sum = (allslt*1.0e-11)/xistoc                                             
c                                                                               
      end                                                                       
c lamda    last revised 11/25/87 by tjw                                         
c*lamda f77 rewrite, 07/07/87 by tjw                                            
      subroutine lamda(al10,jsol,lamlg,ncomp,nx,rconst,tempk,                   
     *                 xbar,w,iktmax)                                           
c                                                                               
c     this routine computes activity coefficients for solid-solution            
c     component end-members                                                     
c                                                                               
c   jsol      array holding mixing model code                                   
c   lamlg     log of activity coefficients for endmember components             
c   ncomp     number of components of solid solution nx                         
c   xbar      mole fractions of endmember components of solid solutions         
c   w         array of non-ideal mixing parameters calculated from              
c             apx array                                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      dimension lamlg(iktmax,*),xbar(iktmax,*),w(iktmax,*),jsol(*),             
     $ ncomp(*)                                                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      fac=1000./(al10*rconst*tempk)                                             
      k=jsol(nx)                                                                
      ncpt=ncomp(nx)                                                            
c                                                                               
      if (k.eq.0) then                                                          
c                                                                               
c       ideal solution                                                          
c                                                                               
c       gxs = 0                                                                 
c                                                                               
        do 20 ik=1,ncpt                                                         
        lamlg(ik,nx)=0.                                                         
   20   continue                                                                
c                                                                               
      elseif (k.eq.1) then                                                      
c                                                                               
c       binary solution, third-order maclaurin expansion                        
c       gxs = w1*x2 + (w2/2)*x2**2 + (w3/6)*x2**3                               
c       original pathi solid-solution model                                     
c                                                                               
        zx=xbar(2,nx)                                                           
        zx2=zx*zx                                                               
        lamlg(1,nx)=-fac*((w(2,nx)*zx2)/2.+(w(3,nx)*zx*zx2)/3.)                 
        b=-(w(3,nx)+w(2,nx))/2.                                                 
        c=w(3,nx)/3.                                                            
        zx=xbar(1,nx)                                                           
        zx2=zx*zx                                                               
        lamlg(2,nx)=fac*(a+b*zx2+c*zx*zx2)                                      
c                                                                               
      elseif (k.eq.2) then                                                      
c                                                                               
c       binary solution, parabolic maclaurin expansion                          
c                                                                               
c       gxs=w1*x1*x2                                                            
c                                                                               
        zx=xbar(2,nx)                                                           
        lamlg(1,nx)=fac*w(1,nx)*zx*zx                                           
        zx=xbar(1,nx)                                                           
        lamlg(2,nx)=fac*w(1,nx)*zx*zx                                           
c                                                                               
      elseif (k.eq.3) then                                                      
c                                                                               
c       binary solution, cubic macluarin with p,t dependence                    
c                                                                               
c       gxs = w1*x1*x2**2 + w2*x2*x1**2                                         
c                                                                               
        zx=xbar(2,nx)                                                           
        a=2.*w(2,nx)-w(1,nx)                                                    
        b=2.*(w(1,nx)-w(2,nx))                                                  
        zx2=zx*zx                                                               
        lamlg(1,nx)=fac*(a*zx2+b*zx*zx2)                                        
        zx=xbar(1,nx)                                                           
        a=2.*w(1,nx)-w(2,nx)                                                    
        b=2.*(w(2,nx)-w(1,nx))                                                  
        zx2=zx*zx                                                               
        lamlg(2,nx)=fac*(a*zx2+b*zx*zx2)                                        
c                                                                               
      elseif (k.eq.4) then                                                      
c                                                                               
c       binary solution, guggenheim polynomial with t-dependence                
c       gxs = x1*x2*(w1 + w2*(x1-x2) + w3*(x1-x2)**2 )                          
c                                                                               
        a=w(1,nx)+3.*w(2,nx)+5.*w(3,nx)                                         
        b=-4.*w(2,nx)-16.*w(3,nx)                                               
        c=12.*w(3,nx)                                                           
        zx=xbar(2,nx)                                                           
        zx2=zx*zx                                                               
        lamlg(1,nx)=fac*zx2*(a+b*zx+c*zx2)                                      
        zx=xbar(1,nx)                                                           
        zx2=zx*zx                                                               
        a=w(1,nx)-3.*w(2,nx)+5.*w(3,nx)                                         
        b=4.*w(2,nx)-16.*w(3,nx)                                                
        lamlg(2,nx)=fac*zx2*(a+b*zx+c*zx2)                                      
c                                                                               
      elseif (k.eq.5) then                                                      
c                                                                               
c       ternary regular solution                                                
c       see prigogine and defay, p. 257                                         
c                                                                               
        a12=w(1,nx)                                                             
        a13=w(2,nx)                                                             
        a23=w(3,nx)                                                             
        zx1=xbar(1,nx)                                                          
        zx2=xbar(2,nx)                                                          
        zx3=xbar(3,nx)                                                          
        zx12=zx1*zx1                                                            
        zx22=zx2*zx2                                                            
        zx32=zx3*zx3                                                            
        lamlg(1,nx)=fac*(a12*zx22+a13*zx32+zx2*zx3*(a12-a23+a13))               
        lamlg(2,nx)=fac*(a12*zx12+a23*zx32+zx1*zx3*(a23-a13+a12))               
        lamlg(3,nx)=fac*(a13*zx12+a23*zx22+zx1*zx2*(a13-a12+a23))               
c                                                                               
      elseif (k.eq.6) then                                                      
c                                                                               
c       need to put in extrapolation of Newton et al. plag model here           
        continue                                                                
c                                                                               
      else                                                                      
        write(noutpl,100) jsol(nx)                                              
        write(nttyol,100) jsol(nx)                                              
100     format(' * error- jsol value ',i2,' is invalid (eqlib/lamda)')          
        stop                                                                    
      endif                                                                     
c                                                                               
      end                                                                       
c lsqp     last revised 11/16/87 by tjw                                         
c*lsqp f77 rewrite, 07/07/87 by tjw                                             
      subroutine lsqp (x, y, w, n, kd, c, ier)                                  
c                                                                               
c     least squares fit of a polynomial, real*8 case                            
c                                                                               
c     input                                                                     
c      x     array of independent variable values                               
c      y     corresponding array of dependent variable values                   
c      w         "          "    "   weights                                    
c      n     number of values in x,y  and w                                     
c      kd    degree of polynomial to fit                                        
c            note that maximum kd = 10                                          
c                                                                               
c     output                                                                    
c      c     array of coefficients of a polynomial of degree kd                 
c             fitting the set (x,y)                                             
c             y = c(1) + c(2)*x + c(3)*x**2 + ...                               
c      ier   0/1  ok/unable to fit                                              
c             note that degree kd must be .lt. number of points n               
c                                                                               
c     if degree = n-1,                                                          
c        call polx for an exact polynomial through (x,y)                        
c     else  -                                                                   
c     method is to form the normal equations in matrix form  ac = b             
c     where (using d for degree)                                                
c                                                                               
c      a = (sum(w)      sum(wx)      sum(wx**2) ...   )                         
c          (sum(wx)     sum(wx**2)   sum(wx**3) ...   )                         
c          (sum(wx**2)  sum(wx**3)   sum(wx**4) ...   )                         
c          ( ...                                      )                         
c          (sum(wx**d)  sum(wx**d+1) sum(wx**d+2) ... )                         
c                                                                               
c      c = (c(1), c(2), ... , c(d+1))                                           
c                                                                               
c      b = (sum(wy)     )                                                       
c          (sum(wxy)    )                                                       
c          (sum(wx**2y  )                                                       
c          (...         )                                                       
c          (sum(wx**dy) )                                                       
c                                                                               
c     and solve for the coefficients c(i)                                       
c                                                                               
c     reference                                                                 
c      "numerical methods and computers", kuo, shan s.,                         
c      addison-wesley, 1965, chapter 11.                                        
c                                                                               
c     this code needs the machine epsilon, eps, which is calculated             
c     by flpars.  this is usually initialized by a call to eqlib, which         
c     calls flpars.                                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqleps.h"                                                 
c                                                                               
      dimension x(*),y(*),w(*),c(*)                                             
      dimension a(ipfpa1,ipfpa1),gm(ipfpa1,ipfpa1),s(ipfdpa),p(ipfdpa)          
      dimension rhs(ipfpa1),ipvt(ipfpa1),res(ipfpa1),ee(ipfpa1)                 
c                                                                               
      data qfalse/.false./                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      ier = 0                                                                   
      ipfmx1 = ipfpa1                                                           
c                                                                               
      kdp1 = kd+1                                                               
      if ( kdp1 .gt. ipfpar) then                                               
        ier = 1                                                                 
        go to 999                                                               
      endif                                                                     
c                                                                               
      if (kd .ge. n) then                                                       
        ier = 1                                                                 
        go to 999                                                               
      endif                                                                     
c                                                                               
      if (kd .ge. n-1) then                                                     
c                                                                               
c       exact polynomial here - degree n-1                                      
c                                                                               
        call polx(x,y,n,c,ier)                                                  
        go to 999                                                               
      endif                                                                     
c                                                                               
      kdp2 = kd+2                                                               
      kd2p1 = 2*kd+1                                                            
      kd2p2 = 2*kd+2                                                            
c                             *----------------------------------------         
c                             * compute all distinct terms of matrix a          
c                             *  place into array  s                            
c                             *----------------------------------------         
      do 10 k=1,kd2p1                                                           
      s(k) = 0.                                                                 
   10 continue                                                                  
c                                                                               
      do 40 i=1,n                                                               
c                                                                               
c      make up products of x's                                                  
c                                                                               
      p(1) = 1.0                                                                
      do 20 k=2,kd2p1                                                           
      p(k) = p(k-1) * x(i)                                                      
   20 continue                                                                  
c                                                                               
c      weight products and sum                                                  
c                                                                               
      do 30 k=1,kd2p1                                                           
      s(k) = s(k) + p(k) * w(i)                                                 
   30 continue                                                                  
c                                                                               
   40 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * fill matrix a from array s                      
c                             *----------------------------------------         
      j = 1                                                                     
      js = 2                                                                    
c                                                                               
c      js = sum of indices                                                      
c                                                                               
   50 continue                                                                  
      i = js-j                                                                  
      a(i,j) = s(js-1)                                                          
      j = j+1                                                                   
      if (j .lt. js) go to 50                                                   
      j = 1                                                                     
      js = js+1                                                                 
      if (js .le. kdp2) go to 50                                                
c                                                                               
c      upper left triangle now full                                             
c                                                                               
      i = kdp1                                                                  
      js = kd+3                                                                 
c                                                                               
   60 continue                                                                  
      j = js-i                                                                  
      a(i,j) = s(js-1)                                                          
      i = i-1                                                                   
      if (j .le. kdp1) go to 60                                                 
      i = kdp1                                                                  
      js = js+1                                                                 
      if (js .le. kd2p2) go to 60                                               
c                                                                               
c      matrix a is full                                                         
c                                                                               
c                             *----------------------------------------         
c                             * compute values for rhs                          
c                             *----------------------------------------         
      do 70 k=1,kdp1                                                            
      rhs(k) = 0.                                                               
   70 continue                                                                  
c                                                                               
      do 100 i = 1,n                                                            
c                                                                               
c      make up products of x's                                                  
c                                                                               
      p(1) = 1.0                                                                
      do 80 k=2,kdp1                                                            
      p(k) = p(k-1) * x(i)                                                      
   80 continue                                                                  
c                                                                               
c      weight products and sum                                                  
c                                                                               
      do 90 k = 1,kdp1                                                          
      rhs(k) = rhs(k) + w(i)*p(k)*y(i)                                          
   90 continue                                                                  
c                                                                               
  100 continue                                                                  
c                                                                               
c                             *----------------------------------------         
c                             * solve ac = rhs                                  
c                             *----------------------------------------         
c                                                                               
c      factor a                                                                 
c                                                                               
      call msolvr(a,gm,rhs,c,res,ee,ipvt,kdp1,ipfmx1,ier,qfalse,qfalse)         
c                                                                               
  999 continue                                                                  
      end                                                                       
c modah    last revised 07/08/87 by tjw                                         
c*modah f77 check, 07/08/87 by tjw                                              
      subroutine modah(nahv,nst,azero,uspec,hydn)                               
c                                                                               
c     input   nahv    unit number of a-h file                                   
c             nst     number of species in uspec                                
c     output  modified values in arrays                                         
c              azero and hydn                                                   
c              corresponding to species names on nahv                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      dimension azero(*),hydn(*),uspec(*)                                       
      character*24 uspec,unam,uendit                                            
c                                                                               
      data uendit /'endit.                  '/                                  
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     link to the ahv input file to read the set of modified azero              
c      and hydn values                                                          
c                                                                               
      open(nahv,file='ahv',status='old',err=900)                                
      goto 3                                                                    
c                                                                               
  900 write (nttyol,1000)                                                       
 1000 format(' * error - can not open file ahv (eqlib/modah)')                  
      stop                                                                      
c                                                                               
   3  continue                                                                  
c                                                                               
      write(noutpl,1005)                                                        
 1005 format(/,' azero and hydration number modifications',/)                   
c                                                                               
   10 continue                                                                  
      read (nahv,1010) unam,av,hv                                               
 1010 format(a24,2x,f6.3,2x,f5.2)                                               
c                                                                               
c      test for exit                                                            
c                                                                               
      if (unam .eq. uendit) go to 999                                           
c                                                                               
      write (noutpl,1012) unam,av,hv                                            
 1012 format(2x,a24,2x,f6.3,2x,f5.2)                                            
c                                                                               
c      search for species name in uspec                                         
c                                                                               
      do 20 ns=2,nst                                                            
      if (unam .ne. uspec(ns)) go to 20                                         
c                                                                               
c      found it                                                                 
c                                                                               
      go to 25                                                                  
c                                                                               
   20 continue                                                                  
c                                                                               
c      not found                                                                
c                                                                               
      write (noutpl,1014)                                                       
 1014 format(' *** species not found in array uspec',/)                         
      go to 10                                                                  
c                                                                               
   25 continue                                                                  
      azero(ns) = av                                                            
      hydn(ns) = hv                                                             
      go to 10                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c molint   last revised 11/25/87 by tjw and rmm                                 
c*molint f77 check, 07/08/87 by tjw                                             
      subroutine molint(conc,nsq,nst,cdrs,ndim,nsb,itype,concbs)                
c                                                                               
c      created 19may87 by rmm                                                   
c                                                                               
c     this routine decomposes the molality of complexes into                    
c     the basis species ( m sub j ,total -- j is basis species)                 
c     input       conc     concentration array                                  
c                 nsq      number of basis species                              
c                 nst      total number of species read from datafile           
c                 cdrs     coefficient arrray                                   
c                 ndim     dimension for cdrs array                             
c                 nsb      number of strict basis species                       
c                 itype    3/6  eq3/eq6                                         
c                                                                               
c     output      concbs   concentration array for basis species                
c                                                                               
c                                                                               
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      dimension cdrs(ndim,*),conc(*),concbs(*)                                  
c                                                                               
c-------------------------------------------------------------------            
c                                                                               
c      initialize                                                               
c                                                                               
      nsb1 = nsb + 1                                                            
      if (itype .eq. 3) then                                                    
        ndesco = nsq + 1                                                        
      else if (itype .eq. 6) then                                               
        ndesco = nsb + 1                                                        
      else                                                                      
        write(noutpl,9997)                                                      
        write(nttyol,9997)                                                      
 9997   format('  error invalid type of program  eqlib/molint')                 
      endif                                                                     
c                                                                               
      qck = .false.                                                             
      do 10 i=1,nsq                                                             
      concbs(i) = 0.0                                                           
   10 continue                                                                  
c                                                                               
      if (qck) then                                                             
        nrst=nst-nsb                                                            
        write(noutpl,9998) ndim                                                 
 9998   format('    in molint   no. rxn    cdrs  ndim=',i5)                     
        do 999 ns=1,ndim                                                        
        write(noutpl,9999) (cdrs(ns,nrxn),nrxn=1,10)                            
 9999   format(10(1x,f10.3))                                                    
  999   continue                                                                
      endif                                                                     
      cdum1 = 0.                                                                
      cdum2 = 0.                                                                
      mdum = 0.                                                                 
c                                                                               
c              for each basis species (strict + auxiliary)                      
c              except water                                                     
c                                                                               
      if (qck) write (noutpl,1001)                                              
 1001 format('line 2 nrs,nsc,cdum1,cdum2,conc,mdum,mol')                        
      do 100 ns = 2,nsq                                                         
      if (ns .eq. nsb) go to 100                                                
      mol = 0.                                                                  
c                                                                               
c              for each reaction look for basis species selected above          
c                                                                               
      if (qck) write (noutpl,1000) ns                                           
 1000 format(/,'  **** the basis species ns-th = ',i3)                          
      do 50 nsc= nsb1,nst                                                       
      nrs = nsc - nsb                                                           
      cdum1 = cdrs(ns,nrs)                                                      
c                                                                               
c              is the ns-th species in this reaction                            
c                                                                               
      if (cdum1 .gt. 0.0) then                                                  
c                                                                               
c              get the reaction coefficient for the                             
c              destroyed dependent species                                      
c                                                                               
        cdum2 = cdrs(ndesco,nrs)                                                
c                                                                               
c              calculate the molality of the ns-th species from this            
c              dependent species                                                
c                                                                               
        cdum2 = abs(cdum2)                                                      
        mdum = (cdum1/cdum2)*conc(nsc)                                          
        mol = mol + mdum                                                        
        if (qck) write (noutpl,1200) nrs,nsc,cdum1,cdum2,conc(nsc),             
     $                      mdum,mol                                            
 1200   format(2(x,i3),5(x,f10.3))                                              
      endif                                                                     
   50 continue                                                                  
c                                                                               
      concbs(ns) = mol + conc(ns)                                               
c                                                                               
  100 continue                                                                  
c                                                                               
      if (qck) then                                                             
        write (noutpl,1250)                                                     
 1250   format(' i    conc     concbs')                                         
        do 900 i=1,nst                                                          
        write (noutpl,1300) i,conc(i),concbs(i)                                 
 1300   format(x,i3,2(x,e13.5))                                                 
  900   continue                                                                
      endif                                                                     
c                                                                               
      end                                                                       
c molslt   last revised 11/25/87 by tjw and rmm                                 
c*molslt f77 check, 07/08/87 by tjw                                             
      subroutine molslt(z,concbs,nsq)                                           
c       created 21jan87 rmm                                                     
c                                                                               
c     this routine calculates the molality of hypothetical                      
c     salts for the hkf equations (part iv)                                     
c     input       z        charge array                                         
c                 nsq      number of basis species                              
c                                                                               
c     output      concbs   array with summed conc. of basis species             
c                                                                               
c     output      mslt     array with molalities of salts                       
c     in eqlhkf   islt     2-d array with indices and stoich.                   
c                          reaction factors for anion and cation                
c                 icnt     number of salts created                              
c                                                                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqlhkf.h"                                                 
      include "eqlun.h"                                                  
c                                                                               
      dimension z(*),concbs(*)                                                  
c                                                                               
c-------------------------------------------------------------------            
c                                                                               
c      initialize                                                               
c      qplus, qmin -- if true go through loop and pick up new species           
c      qck --  debug write switch                                               
      qck = .false.                                                             
      qmin = .true.                                                             
      qplus = .true.                                                            
      nsplus = 1                                                                
      nsmin = 1                                                                 
      icnt = 0                                                                  
      xl = 1.0e-10                                                              
      zplus = 0.0                                                               
      zmin= 0.0                                                                 
      plus = 0.0                                                                
      minus = 0.0                                                               
      sfplus = 0.0                                                              
      sfmin = 0.0                                                               
      msalt = 0.0                                                               
      if (qck) then                                                             
        do 900 i=1,nsq                                                          
        write(noutpl,9200) i,concbs(i)                                          
 9200   format(' molslt-- i,conc ',i3,x,e15.5)                                  
  900   continue                                                                
      endif                                                                     
c                                                                               
c              cation loop                                                      
c                                                                               
  100 continue                                                                  
      if (qplus) then                                                           
  200   continue                                                                
        nsplus = nsplus + 1                                                     
c                                                                               
c              exit if no cations left to search                                
c                                                                               
        if (nsplus .ge. nsq) go to 600                                          
        dumcon=concbs(nsplus)                                                   
        dumz=z(nsplus)                                                          
        if ((dumcon .gt. xl) .and. (dumz .gt. xl)) then                         
          plus = dumcon                                                         
          zplus = dumz                                                          
        else                                                                    
          go to 200                                                             
        endif                                                                   
      endif                                                                     
c                                                                               
c              anion loop                                                       
c                                                                               
  300 continue                                                                  
      if (qmin) then                                                            
  400   continue                                                                
        nsmin= nsmin+ 1                                                         
c                                                                               
c              exit if no anions left to search                                 
c                                                                               
        if (nsmin .ge. nsq) go to 600                                           
        dumcon=concbs(nsmin)                                                    
        dumz=z(nsmin)                                                           
        if ((dumcon .gt. xl) .and. (dumz .lt.-xl)) then                         
          minus = dumcon                                                        
          zmin= dumz                                                            
        else                                                                    
          go to 400                                                             
        endif                                                                   
      endif                                                                     
c                                                                               
  500 continue                                                                  
c                                                                               
c              if equiv. chg. -- make 1:1 salt                                  
c                                                                               
      if (abs(abs(zplus) - abs(zmin)) .le. xl) then                             
        sfplus = 1.0                                                            
        sfmin = 1.0                                                             
      else                                                                      
        sfplus = abs(zmin)                                                      
        sfmin = zplus                                                           
      endif                                                                     
c                                                                               
c              use up all of cation                                             
c                                                                               
      if ((plus/sfplus) .lt. (minus/sfmin)) then                                
        msalt = plus/sfplus                                                     
        plus = 0.0                                                              
c                                                                               
        if (qck) then                                                           
          write(noutpl,9300) minus,sfmin                                        
 9300     format(' no cation left- minus before',e15.5,' sfplus',e15.5)         
        endif                                                                   
c                                                                               
        minus = minus - (msalt*sfmin)                                           
c                                                                               
        if (qck) then                                                           
          write(noutpl,9000) nsplus,nsmin,minus,msalt                           
 9000     format(' cation gone--nsplus,nsmin,minus left,msalt created',         
     $          2(x,i3),2(e15.5))                                               
        endif                                                                   
c                                                                               
        qplus = .true.                                                          
        qmin = .false.                                                          
        if (minus .lt. xl) then                                                 
          minus = 0.0                                                           
          qmin = .true.                                                         
        endif                                                                   
      else                                                                      
c                                                                               
c                    use up all of anion                                        
c                                                                               
        msalt = minus/sfmin                                                     
        minus = 0.0                                                             
c                                                                               
        if (qck) then                                                           
          write(noutpl,9400) plus,sfplus                                        
 9400     format(' no anion left- plus before',e15.5,' sfplus',e15.5)           
        endif                                                                   
c                                                                               
        plus = plus - (msalt*sfplus)                                            
c                                                                               
        if (qck) then                                                           
          write(noutpl,9100) nsplus,nsmin,plus,msalt                            
 9100     format(' anion gone --nsplus,nsmin,plus left,msalt created',          
     $          2(x,i3),2(e15.5))                                               
        endif                                                                   
c                                                                               
        qplus = .false.                                                         
        qmin = .true.                                                           
        if (plus .lt. xl) then                                                  
          plus = 0.0                                                            
          qplus = .true.                                                        
        endif                                                                   
      endif                                                                     
c                                                                               
c              fill in array islt with indices and                              
c              stoich. reaction coefficients for cations and anions             
c                                                                               
      icnt = icnt + 1                                                           
      mslt(icnt) = msalt                                                        
      islt(1,icnt) = nsplus                                                     
      islt(2,icnt) = nsmin                                                      
      islt(3,icnt) = int(sfplus)                                                
      islt(4,icnt) = int(sfmin)                                                 
c                                                                               
c              go back for next salt                                            
c                                                                               
      go to 100                                                                 
c                                                                               
c              exit and check to see if normal termination                      
c                                                                               
  600 continue                                                                  
      if (qck) then                                                             
        if ((minus .lt. xl) .and. (plus .lt. xl)) then                          
          write(noutpl,1000)                                                    
 1000     format(' normal exit --molslt - after making moles of salt')          
        else                                                                    
          write(noutpl,2000)                                                    
 2000     format(' ** warning in molslt -- conc. of remaining ion is',          
     $         ' significant')                                                  
        endif                                                                   
      endif                                                                     
c                                                                               
c              write out list of salts                                          
c                                                                               
      if (qck) then                                                             
        write(noutpl,3000)                                                      
 3000   format(' cation      anion  cat. s.f. ',                                
     $       '  anion s.f.      mol. salt     ')                                
        write(noutpl,4000) ((islt(j,i),j=1,4),mslt(i),i=1,icnt)                 
 4000   format(4(5x,i5),5x,e13.5)                                               
      endif                                                                     
c                                                                               
      end                                                                       
c msolvr    last modified 01/25/88 by rmm                                       
cb*msolvr created in f77 11/10/87 by tjw                                        
      subroutine msolvr(aa,gm,rhs,del,res,ee,ir,kdim,kmax,ier,qpr,              
     $ qpri)                                                                    
c                                                                               
c     this routine solves the matrix equation (aa) (del) = (rhs).               
c     the method employed is l-u decomposition.  if the reciprocal              
c     condition number (rcond) is less than 100 times the machine               
c     epsilon, the solution is iteratively improved.                            
c                                                                               
c     input                                                                     
c       aa = matrix                                                             
c       kdim = the actual dimention of the matrix                               
c       kmax = the maximum dimension of the matrix                              
c       gm = work space for a copy of the matrix                                
c       rhs = the right hand side vector                                        
c       res = work space vector                                                 
c       ee = work space vector                                                  
c       ir = work space vector                                                  
c       qpr = logical switch to print messages from msolvr                      
c       qpri = logical switch to turn on print messages from                    
c                routine itrefn                                                 
c     output                                                                    
c       del = the solution vector                                               
c       rcond = the reciprocal condition number                                 
c       ier = error flag                                                        
c         =  -1  l-u decomposition worked, but iterative improvement            
c                failed (this is just a warning condition)                      
c         =   0  the solution worked okay                                       
c         = 800  l-u decomposition failed; rcond .lt. smpos                     
c     other                                                                     
c       smpos = the smallest positive number (real*8)                           
c       eps100 = 100 times the machine epsilon (real*8)                         
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
      include "eqlun.h"                                                  
c                                                                               
      dimension aa(kmax,*),gm(kmax,*),rhs(*),del(*),res(*),ee(*),               
     $ ir(*)                                                                    
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
c     copy aa to working array gm                                               
c                                                                               
      do 20 j=1,kdim                                                            
      do 10 k=1,kdim                                                            
      gm(j,k) = aa(j,k)                                                         
   10 continue                                                                  
   20 continue                                                                  
c                                                                               
c     factor the matrix                                                         
c                                                                               
      call sgeco(gm,kmax,kdim,ir,rcond,ee)                                      
c                                                                               
c     note- the test rcond .lt. eps, is not sufficient to guarantee             
c     singularity.  in practice, it has been observed that this                 
c     condition is frequently true, but the results of the matrix               
c     decomposition are useful anyway.  on the other hand, it has               
c     also been observed that a theoretically singular matrix can               
c     sometimes be factored, due to rounding/truncation in the                  
c     decomposition process.  see documentation in the eqlib routines           
c     sgeco and sgefa.                                                          
c                                                                               
      if (rcond.lt.smpos) then                                                  
        ier = 800                                                               
        if (qpr) write (noutpl,1000)                                            
 1000   format(' --- sgeco (l-u decomp.) failed (eqlib/msolvr) ---')            
        go to 999                                                               
      endif                                                                     
c                                                                               
c      copy rhs to del                                                          
c                                                                               
      do 30 j=1,kdim                                                            
      del(j)=rhs(j)                                                             
   30 continue                                                                  
c                                                                               
c     solve the matrix equation                                                 
c                                                                               
      call sgesl(gm,kmax,kdim,ir,del)                                           
c                                                                               
      if (rcond .le. eps100) then                                               
c                                                                               
c       iteratively improve the solution to the matrix equation                 
c                                                                               
        call itrefn(aa,gm,kmax,kdim,ir,rhs,del,rcond,res,ee,qpri,ier)           
c                                                                               
        if (ier.gt.0) then                                                      
         ier = -1                                                               
         if (qpr) write (noutpl,1010)                                           
 1010    format(' --- itrefn (iter. impr.) failed (eqlib/msolvr) ---')          
        endif                                                                   
      endif                                                                     
c                                                                               
  999 continue                                                                  
      end                                                                       
c nactop   last revised 11/02/87 by tjw                                         
c*nactop f77 check, 07/08/87 by tjw                                             
      subroutine nactop                                                         
c                                                                               
c     this routine sets the name of the option for the activity                 
c     coefficients of aqueous species.  it also sets associated                 
c     logical flags concerning the generic type of activity                     
c     coefficient option.  the variable iopg1 determines the exact              
c     activity coefficient model as well as exactly how the model is            
c     to be treated numerically.  more than one value of iopg1 may              
c     set the same logical flag (e.g., qhydth, qpt4) to .true..                 
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*32 um1,uze,up1,up3,up4,up5,uerr                                 
c                                                                               
      include "eqlgp.h"                                                  
      include "eqlun.h"                                                  
c                                                                               
      data uoff/'off     '/,uon/'on      '/,uonplu/'onplus  '/                  
      data uerr/'error                           '/                             
      data um1 /'davies equation                 '/                             
      data uze /'b-dot equation plus others      '/                             
      data up1 /'pitzer equations (standard)     '/                             
      data up3 /'dh(o/c)a + e-l hydr. theory     '/                             
      data up4 /'dh(o/c)a hydration theory       '/                             
      data up5 /'hkf equations - part iv         '/                             
c                                                                               
c-------------------------------------------------------------------            
c                                                                               
c     qhydth is set to .true. if the activity coefficient option                
c     involves hydration theory.  this activates usage of the parameter         
c     dshm (= 1. - ( sigma(hm)/om ).                                            
c                                                                               
      qhydth=.false.                                                            
c                                                                               
c     qpt4 is set to .true. if hkf equations are being used                     
c                                                                               
      qpt4 = .false.                                                            
c                                                                               
c     set for null e-lambda terms.  override later for                          
c     specific options requiring such terms.                                    
c                                                                               
      uelam=uoff                                                                
      ielam=-1                                                                  
c                                                                               
c     iopg3 = switch for e-lambda treatment- 0/1 = on/onplus                    
c                                                                               
      if (iopg1.ne.3) iopg3=0                                                   
c                                                                               
c     iopg4 = switch for hydration theory f term- 0/1 = dhoa/dhca               
c                                                                               
      if (iopg1.ne.3. .and. iopg1.ne.4) iopg4=0                                 
c                                                                               
c     note- iopg1 .le. 0 is used for simple ionic strength dependent            
c     models.  iopg1 .ge. 1 is used for specific composition dependent          
c     models, such as pitzer's equations.                                       
c                                                                               
      if (iopg1 .eq. 0) then                                                    
c                                                                               
c       b-dot equation and others (b-dot package)                               
c                                                                               
        uactop = uze                                                            
      elseif (iopg1 .eq. -1) then                                               
c                                                                               
c       the davies equation                                                     
c                                                                               
        uactop = um1                                                            
c                                                                               
      elseif (iopg1 .eq. 1) then                                                
c                                                                               
c       pitzer's equations (standard)                                           
c                                                                               
        uactop = up1                                                            
c                                                                               
c       note- ielam is determined in routine indpt from the pitzer              
c       data file input                                                         
c                                                                               
      elseif (iopg1 .eq. 2) then                                                
c                                                                               
c       reserved for pitzer style option                                        
c                                                                               
        uactop = uerr                                                           
      elseif (iopg1 .eq. 3) then                                                
c                                                                               
c       wolery - jackson dh(o/c)a + e-l hydration theory                        
c                                                                               
        uactop = up3                                                            
        qhydth = .true.                                                         
        if (iopg3 .gt. 0) then                                                  
          ielam = 1                                                             
          uelam = uonplu                                                        
        else                                                                    
          ielam = 0                                                             
          uelam = uon                                                           
        endif                                                                   
      elseif (iopg1 .eq. 4) then                                                
c                                                                               
c       wolery - jackson dh(o/c)a hydration theory                              
c                                                                               
        uactop = up4                                                            
        qhydth = .true.                                                         
        ielam = -1                                                              
      elseif (iopg1 .eq. 5) then                                                
c                                                                               
c       hkf equations - part iv                                                 
c                                                                               
        uactop = up5                                                            
        qpt4 = .true.                                                           
      else                                                                      
c                                                                               
c       error                                                                   
c                                                                               
        uactop = uerr                                                           
        write (nttyol,1005) iopg1                                               
        write (noutpl,1005) iopg1                                               
 1005   format(' * error- iopg1= ',i5,' is not a valid activity',               
     $  ' coefficient',/11x,'option (eqlib/nactop)')                            
          stop                                                                  
      endif                                                                     
c                                                                               
      end                                                                       
c nbsgam   last revised 11/20/87 by tjw                                         
c*nbsgam created in f77 11/19/87 by tjw                                         
      subroutine nbsgam(glgnbs,xi,adh,nchlor)                                   
c                                                                               
c     this routine calculates the log activity coefficient of the               
c     chloride ion according to the nbs ph convention.  the convention          
c     itself may be extended outside the specified limit on the                 
c     ionic strength.                                                           
c                                                                               
c     reference- covington, a.k., bates, r.g., and durst, r.a., 1985,           
c     defintion of ph scales, standard reference values, measurement            
c     of ph and related terminology (recommendations, 1984), pure               
c     app. chem., v. 57, p. 533-542.                                            
c                                                                               
c     input                                                                     
c       xi = ionic strength                                                     
c       adh = the debye-huckel 'a' parameter                                    
c       nchlor = the ns index of the chloride ion                               
c                                                                               
c     output                                                                    
c       glgnbs = log gamma cl-, according to the bates-guggenheim               
c                convention                                                     
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     test the index of the chloride ion                                        
c                                                                               
      if (nchlor .le. 0) then                                                   
        write (noutpl,1000)                                                     
        write (nttyol,1000)                                                     
 1000   format(' * error- no index for the chloride ion',                       
     $  ' (eqlib/nbsgam)',/7x,'can not use the extended nbs ph scale')          
        stop                                                                    
      endif                                                                     
c                                                                               
c     evaluate the nbs expression for the molal activity coefficient            
c     of the chloride ion                                                       
c                                                                               
      xisqrt = sqrt(xi)                                                         
      glgnbs = - ( adh * xisqrt ) / ( 1.0 + ( 1.5 * xisqrt ) )                  
c                                                                               
      end                                                                       
c newton   last revised 01/25/88 by rmm                                         
      subroutine newton(cdrs,aa,gm,zvclg1,del,rhs,ee,res,conc,cxistq,           
     $ beta,alpha,z,zsq2,azero,hydn,glg,glgo,betao,delo,concbs,screwd,          
     $ screwn,tolbt,toldl,tempc,press,betamx,bbig,bneg,bgamx,xi,xisteq,         
     $ dshm,shm,shmlim,uspec,uzvec1,uqdel,uqbeta,ubbig,ubneg,ubgamx,            
     $ jsflag,jsort,ir,iter,itermx,idelmx,ibetmx,iacion,kmax,kdim,              
     $ nsqmx1,nsb,nsq,nst,nhydr,nchlor,ier,matrxe,ncmpe,betae,                  
     $ qpra,qprb,qprc)                                                          
c                                                                               
c     this routine performs hybrid newton-raphson iteration to solve            
c     for the equilibrium distribution of species which corresponds             
c     to a set of constraints (total concentrations, ph, eh,                    
c     temperature, pressure, etc.) on an aqueous solution.                      
c                                                                               
c     input                                                                     
c       cdrs = array of coefficients for aqueous complex dissociation           
c              reactions                                                        
c       cxistq = array of stoichiometric factors for calculating xisteq         
c       z = electrical charge array                                             
c       zsq2 = one half the charge squared array                                
c       azero = ion size array                                                  
c       hydn = hydration number array                                           
c       screwd = under-relaxation control parameter.  it is used to             
c                reduce the magnitude of the del vector, if necessary           
c                so that the magnitude of the largest element of that           
c                vector does not exceed screwd.                                 
c       screwn = under-relaxation control parameter                             
c       tolbt = convergence tolerance on betamx                                 
c       toldl = convergence tolerance on delmax                                 
c       tempc = temperature, deg celsius                                        
c       press = pressure, bars                                                  
c       shmlim = temporary upper limit imposed on shm                           
c       uspec = aqueous species name array                                      
c       uzvec1 = name array corresponding to zvclg1                             
c       uqdel = description of quantity type for del corrections;               
c                e.g., 'conc' or 'moles'                                        
c       uqbeta = description of quantity type for beta vector;                  
c                e.g., 'conc' or 'moles'                                        
c       jsflag = status flag for aqeuous species, species active if jsflag      
c                .le. 0                                                         
c       itermx = maximum number of iterations                                   
c       iacion = index of the species (na+ or cl-) used to define xisteq        
c       kmax = maximum dimension of aa                                          
c       kdim = dimension of aa                                                  
c       nsqmx1 = first dimension of the cdrs array                              
c       nsb = number of strict master species                                   
c       nsq = number of master species                                          
c       nst = number of aqeuous species                                         
c       nhydr = ns index of the hydrogen ion                                    
c       nchlor = ns index of the chloride ion                                   
c       matrxe = external that prints aa, matrix for eq3nr, matrxz for eq6      
c       ncmpe = external that expands the system description so that            
c               residuals can be calculated, ncmpx for eq3nr, ncmpz for         
c               eq6.  mostly this involves calculating the concentrations       
c               of dependent aqueous species.                                   
c       betae = external that calculates the beta array, betas for eq3nr,       
c               betaz for eq6.                                                  
c       qpra = debugging print flag, if true, prints summary information        
c       qprb = debugging print flag, if true, causes most details to be         
c              printed (does not cause the jacobian matrix to be printed)       
c       qprc = debugging print flag, if true, causes the jacobian matrix        
c              to be printed                                                    
c                                                                               
c     input/output                                                              
c       zvclg1 = the 'log z' array, the array corrected by                      
c                newton-raphson iteration                                       
c       conc = concentration array                                              
c       glg = activity coefficient array                                        
c       xi = ionic strength                                                     
c       xisteq = equivalent stoichiometric ionic strength                       
c       dshm = hydration theory 'd' parameter                                   
c       shm = hydration theory 'sigma hm' parameter                             
c                                                                               
c     work space/output                                                         
c       aa = jacobian matrix                                                    
c       gm = copy of aa                                                         
c       del = correction array                                                  
c       rhs = right hand side vector                                            
c       ee = floating point work space vector                                   
c       res = floating point work space vector                                  
c       beta = normalized residual function array                               
c       alpha = residual function array                                         
c       glgo = old activity coefficient array                                   
c       betao = old beta array                                                  
c       delo = old delo array                                                   
c       concbs = total concentrations of master species array                   
c       betamx = norm (largest magnitude) of the beta array                     
c       bbig = largest magnitude positive mass balance residual                 
c       bneg = largest magnitude negative mass balance residual                 
c       bgamx = norm (largest magnitude) aqeuous phase activity                 
c               coefficient residual                                            
c       ubbig = name of species corresponding to bbig                           
c       ubneg = name of species corresponding to bneg                           
c       ubgamx = name of species corresponding to bgamx                         
c       ir = integer work space vector                                          
c       iter = newton-raphson iteration number                                  
c       jsort = array of aqueous species indices, arranged in order of          
c               increasing concentration or mole number                         
c       idelmx = kcol index corresponding to delmax                             
c       ibetmx = kcol index corresponding to betamx                             
c       ier = error flag, okay if .le. 0                                        
c                                                                               
c------------------------------------------------------------------------       
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
      include "eqlgp.h"                                                  
      include "eqlun.h"                                                  
c                                                                               
      character*(*) uspec(*),uzvec1(*),uqdel(*),uqbeta(*),ubbig,ubneg,          
     $ ubgamx                                                                   
c                                                                               
      dimension cdrs(nsqmx1,*),aa(kmax,*),gm(kmax,*),zvclg1(*),del(*),          
     $ rhs(*),ee(*),res(*),conc(*),cxistq(*),beta(*),alpha(*),z(*),             
     $ zsq2(*),azero(*),hydn(*),glg(*),glgo(*),betao(*),delo(*),                
     $ concbs(*),jsflag(*),jsort(*),ir(*)                                       
c                                                                               
      external matrxe,ncmpe,betae                                               
c                                                                               
      data ublank/'        '/                                                   
      data qfalse/.false./                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      iter = 0                                                                  
      ier = 0                                                                   
      idelmx = 0                                                                
      ibetmx = 0                                                                
      npconv = 0                                                                
c                                                                               
      delmax = 0.                                                               
      betfnc = 0.                                                               
      delfnc = 0.                                                               
      betmxo = 0.                                                               
      delmxo = 0.                                                               
      negdfc = 0                                                                
      negbfc = 0                                                                
      do 40 kcol = 1,kdim                                                       
      del(kcol) = 0.                                                            
   40 continue                                                                  
c                                                                               
c     debugging prints- state of the system prior to newton-raphson             
c     iteration.                                                                
c                                                                               
      if (qprb) then                                                            
        write (noutpl,2160)                                                     
 2160   format(/3x,'kcol',3x,'uzvec1',10x,'correction',3x,'zvclg1',/)           
        do 90 kcol = 1,kdim                                                     
        write (noutpl,2145) kcol,uzvec1(kcol),del(kcol),zvclg1(kcol)            
 2145   format(3x,i3,2x,a12,3x,1pe15.8,3x,0pf15.10)                             
   90   continue                                                                
c                                                                               
        write (noutpl,2150)                                                     
 2150   format(//7x,'kcol',3x,'uzvec1',10x,'beta',/)                            
        do 105 kcol = 1,kdim                                                    
        write (noutpl,2145) kcol,uzvec1(kcol),beta(kcol)                        
  105   continue                                                                
      endif                                                                     
c                                                                               
      if (qprb) then                                                            
        write (noutpl,2300) xi                                                  
 2300   format(/3x,'xi = ',1pe12.5)                                             
        if (iopg1 .eq. 0) write (noutpl,2302) xisteq                            
 2302   format(/3x,'xisteq = ',1pe12.5)                                         
        if (qhydth) write (noutpl,2304) shm                                     
 2304   format(/3x,'shm = ',1pe12.5)                                            
      endif                                                                     
c                                                                               
      xio = xi                                                                  
      if (iopg1 .eq. 0) xistqo = xisteq                                         
      if (qhydth) then                                                          
        shmo = shm                                                              
        dshmo = dshm                                                            
      endif                                                                     
c                                                                               
      do 206 ns = 1,nst                                                         
      glgo(ns) = glg(ns)                                                        
  206 continue                                                                  
c                                                                               
c     the label below is the return point for iter .ge. 1.                      
c     note- do not call ncmpe here to calculate the concentrations              
c     of dependent species, etc.  this should be unnecessary.                   
c                                                                               
c     recompute xi, xisteq, shm, dshm.  recompute activity coefficients.        
c     then recompute the concentrations of dependent aqueous species.           
c     then compute the residual functions.  then do a newton-raphson            
c     step.                                                                     
c                                                                               
  200 if (qprb) write (noutpl,2137)                                             
 2137 format(/3x,'--- pre-newton-raphson update of activity',                   
     $ ' coefficients ---',/)                                                   
c                                                                               
      call ngcadv(cdrs,glg,glgo,z,zsq2,azero,hydn,conc,concbs,                  
     $ cxistq,xi,xisteq,shm,shmlim,dshm,tempc,press,xio,xistqo,shmo,            
     $ uspec,jsflag,jsort,nsb,nsq,nsqmx1,nst,nhydr,nchlor,iacion,               
     $ iter,ier,ncmpe,qprb)                                                     
c                                                                               
c     calculate xi, etc., related residual functions.  save old values          
c     of xi, etc., and the activity coefficients                                
c                                                                               
      bxi = (xi - xio)/xio                                                      
      if (iopg1 .eq. 0) bxistq = (xisteq - xistqo)/xistqo                       
      if (qhydth) bshm = (shm - shmo)/shmo                                      
      call betgam(uspec,conc,glg,glgo,nst,bgamx,ubgamx)                         
c                                                                               
      xio = xi                                                                  
      if (iopg1 .eq. 0) xistqo = xisteq                                         
      if (qhydth) then                                                          
        shmo = shm                                                              
        dshmo = dshm                                                            
      endif                                                                     
      do 207 ns = 1,nst                                                         
      glgo(ns) = glg(ns)                                                        
  207 continue                                                                  
c                                                                               
c---------------------------------------------------------------------          
c                                                                               
c     set up for the newton-raphson step.  important- do not                    
c     recompute activity coefficients within this block.  it                    
c     might interfere with the resdual tracking under-relaxation                
c     algorithm.                                                                
c                                                                               
c     recalculate the newton-raphson residual functions.                        
c                                                                               
      call betae(qfalse,qfalse)                                                 
c                                                                               
c     save the current values of beta and del for use in                        
c     under-relaxation schemes.                                                 
c                                                                               
      do 195 kcol = 1,kdim                                                      
      betao(kcol) = abs(beta(kcol))                                             
      delo(kcol) = del(kcol)                                                    
  195 continue                                                                  
c                                                                               
      if (qprb) then                                                            
        write (noutpl,2150)                                                     
        do 965 kcol = 1,kdim                                                    
        write (noutpl,2145) kcol,uzvec1(kcol),beta(kcol)                        
  965   continue                                                                
        write (noutpl,2135)                                                     
 2135   format(1x)                                                              
      endif                                                                     
c                                                                               
      bx = 0.                                                                   
      dx = 0.                                                                   
      if (ibetmx.gt.0) bx = beta(ibetmx)                                        
      if (idelmx.gt.0) dx = del(idelmx)                                         
      delfnc = 0.                                                               
      if (delmxo .ge. smpos) delfnc = (delmxo - delmax) / delmxo                
      delmxo = delmax                                                           
      betfnc = 0.                                                               
      if (betmxo .ge. smpos) betfnc = (betmxo - betamx)/betmxo                  
      betmxo = betamx                                                           
c                                                                               
      if (qpra) then                                                            
c                                                                               
        ubetmx = ublank                                                         
        utb = ublank                                                            
        udelmx = ublank                                                         
        utd = ublank                                                            
c                                                                               
        if (ibetmx .gt. 0) then                                                 
          utb = uqbeta(ibetmx)                                                  
          ubetmx = uzvec1(ibetmx)                                               
        endif                                                                   
c                                                                               
        if (idelmx .gt. 0) then                                                 
          utd = uqdel(idelmx)                                                   
          udelmx = uzvec1(idelmx)                                               
        endif                                                                   
c                                                                               
        write (noutpl,2095) iter,delmax,utd,udelmx,dx,delfnc,                   
     $  utb,ubetmx,bx,betfnc                                                    
 2095   format(' iter= ',i3,', delmax= ',1pe12.5,' (unrelaxed)',                
     $   /7x,' del(',2a8,')= ',1pe12.5,', delfnc = ',1pe12.5,                   
     $   /7x,'beta(',2a8,')= ',1pe12.5,', betfnc = ',1pe12.5)                   
c                                                                               
        write (noutpl,2092) ncut                                                
 2092   format(7x,'no. of under-relaxation cycles= ',i3)                        
        write (noutpl,2093) bbig,ubbig                                          
 2093   format(8x,'bbig= ',1pe12.5,', ubbig= ',a24)                             
        write (noutpl,2094) bneg,ubneg                                          
 2094   format(8x,'bneg= ',1pe12.5,', ubneg= ',a24)                             
        write (noutpl,2096) bgamx,ubgamx                                        
 2096   format(7x,'bgamx= ',1pe12.5,', ubgamx= ',a24)                           
        if (iopg1 .eq. 0) then                                                  
          write (noutpl,2097) bxi,bxistq                                        
 2097     format(9x,'bxi= ',1pe12.5,', bxistq= ',1pe12.5)                       
        elseif (qhydth) then                                                    
          write (noutpl,2098) bxi,bshm                                          
 2098     format(9x,'bxi= ',1pe12.5,', bshm= ',1pe12.5)                         
        else                                                                    
          write (noutpl,2099) bxi                                               
 2099     format(9x,'bxi= ',1pe12.5)                                            
        endif                                                                   
      endif                                                                     
c                                                                               
c     check to see if the iteration satisfies the convergence criteria.         
c     note that the residual norm for activity coefficients (bgamx) is          
c     tested, but the residuals for ionic strength, etc., are not.              
c     both residual functions and correction terms are tested.                  
c     iteration may terminate acceptably without satisfying the                 
c     constraint on the correction terms (see below).                           
c                                                                               
      qcbeta = betamx .le. tolbt                                                
      qcdel = delmax .le. toldl                                                 
      qconv = qcbeta .and. qcdel                                                
      qcgam = bgamx .le. tolbt                                                  
c                                                                               
c     force to run at least one iteration                                       
c                                                                               
      if (iter .ge. 1) then                                                     
c                                                                               
c       test for convergence                                                    
c                                                                               
        if (qconv .and. qcgam) go to 999                                        
      endif                                                                     
c                                                                               
      call nrstep(aa,gm,rhs,res,ee,ir,zvclg1,alpha,beta,del,                    
     $ betao,delo,screwd,screwn,betamx,delmax,delfnc,betfnc,                    
     $ uzvec1,itermx,kdim,kmax,idelmx,iter,negdfc,negbfc,npconv,ier,            
     $ matrxe,ncmpe,betae,qcbeta,qcgam,qprb,qprc)                               
c                                                                               
      if (ier .gt. 0) then                                                      
c                                                                               
c       iteration has been stopped                                              
c                                                                               
        if (qcbeta .and. qcgam) then                                            
c                                                                               
c         have pseudo-convergence                                               
c                                                                               
          ier = 0                                                               
          if (qpra) then                                                        
            write (noutpl,2040)                                                 
 2040       format(' --- newton-raphson iteration pseudo-converged',            
     $      ' ---',/)                                                           
            do 283 kcol = 1,kdim                                                
            if (abs(del(kcol)).gt.toldl) then                                   
              write (noutpl,2042) kcol,uzvec1(kcol),del(kcol)                   
 2042         format(11x,'del (',i5,2x,a24,') = ',g12.5)                        
            endif                                                               
  283       continue                                                            
          endif                                                                 
          go to 999                                                             
        else                                                                    
c                                                                               
c         iteration has failed                                                  
c                                                                               
          write (noutpl,2030) iter                                              
 2030     format(' --- iteration has gone sour (iter= ',i3,') ---')             
          ier = 1                                                               
          go to 999                                                             
        endif                                                                   
      endif                                                                     
c                                                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     recompute xi, xisteq, shm, dshm.  recompute activity coefficients.        
c     then recompute the concentrations of dependent species.  then             
c     go back to recompute xi, etc.                                             
c                                                                               
      if (qprb) write (noutpl,2139)                                             
 2139 format(/3x,'--- post-newton-raphson update of activity',                  
     $ ' coefficients ---',/)                                                   
c                                                                               
c     get coefficients for xisteq, if needed                                    
c                                                                               
      call ngcadv(cdrs,glg,glgo,z,zsq2,azero,hydn,conc,concbs,                  
     $ cxistq,xi,xisteq,shm,shmlim,dshm,tempc,press,xio,xistqo,shmo,            
     $ uspec,jsflag,jsort,nsb,nsq,nsqmx1,nst,nhydr,nchlor,iacion,               
     $ iter,ier,ncmpe,qprb)                                                     
c                                                                               
      if (qprb) then                                                            
c                                                                               
c       note- the following betae call is purely in support of a                
c       debugging print.                                                        
c                                                                               
        call betae(qfalse,qfalse)                                               
        write (noutpl,2150)                                                     
        do 967 kcol = 1,kdim                                                    
        write (noutpl,2145) kcol,uzvec1(kcol),beta(kcol)                        
  967   continue                                                                
        write (noutpl,2135)                                                     
      endif                                                                     
c                                                                               
      go to 200                                                                 
c                                                                               
  999 continue                                                                  
      end                                                                       
c ngcadv   last revised 01/15/88 by tjw                                         
      subroutine ngcadv(cdrs,glg,glgo,z,zsq2,azero,hydn,conc,concbs,            
     $ cxistq,xi,xisteq,shm,shmlim,dshm,tempc,press,xio,xistqo,shmo,            
     $ uspec,jsflag,jsort,nsb,nsq,nsqmx1,nst,nhydr,nchlor,iacion,               
     $ iter,ier,ncmpe,qpr)                                                      
c                                                                               
c     this routine recalculates the ionic strength, etc., and the               
c     activity coefficients of aqueous species, and then recalculates           
c     the concentrations of dependent aqueous species.  it operates in          
c     direct support of newton-raphson iteration.                               
c                                                                               
c     input                                                                     
c       cdrs = array of coefficients for aqueous complex dissociation           
c              reactions                                                        
c       glgo = old activity coefficient array                                   
c       jsort = array of aqueous species indices, arranged in order of          
c               increasing concentration or mole number                         
c       nsb = number of strict master species                                   
c       nsq = number of master species                                          
c       nsqmx1 = first dimension of the cdrs array                              
c       nst = number of aqeuous species                                         
c       nhydr = ns index of the hydrogen ion                                    
c       nchlor = ns index of the chloride ion                                   
c       iacion = index of the species (na+ or cl-) used to define xisteq        
c       z = electrical charge array                                             
c       zsq2 = one half the charge squared array                                
c       azero = ion size array                                                  
c       hydn = hydration number array                                           
c       conc = concentration array                                              
c       concbs = total concentrations of master species array                   
c       cxistq = array of stoichiometric factors for calculating xisteq         
c       shmlim = temporary upper limit imposed on shm                           
c       tempc = temperature, deg celsius                                        
c       press = pressure, bars                                                  
c       xio = old value of xi                                                   
c       xistqo = old value of xisteq                                            
c       shmo = old value of shm                                                 
c       uspec = aqueous species name array                                      
c       jsflag = status flag for aqeuous species, species active if jsflag      
c                .le. 0                                                         
c       iter = newton-raphson iteration number                                  
c       ncmpe = external name, ncmpx for eq3nr, ncmpz for eq6                   
c       qpr = debugging print flag, causes prints if true                       
c                                                                               
c     output                                                                    
c       glg = activity coefficient array                                        
c       ier = error flag, okay if .le. 0                                        
c       xi = ionic strength                                                     
c       xisteq = equivalent stoichiometric ionic strength                       
c       shm = hydration theory 'sigma hm' parameter                             
c       dshm = hydration theory 'd' parameter                                   
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
      include "eqlgp.h"                                                  
      include "eqlpp.h"                                                  
      include "eqlun.h"                                                  
c                                                                               
      character*24 uspec(*)                                                     
c                                                                               
      dimension cdrs(nsqmx1,*),glg(*),glgo(*),z(*),zsq2(*),azero(*),            
     $ hydn(*),conc(*),concbs(*),cxistq(*),jsflag(*),jsort(*)                   
c                                                                               
      external ncmpe                                                            
c                                                                               
      data xlim/25./                                                            
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      call gxi(zsq2,conc,jsort,nst,xi)                                          
      xi = min(xi,xlim)                                                         
      if (iopg1.eq.0 .and. iacion.gt.0) then                                    
        call gxistq(cxistq,conc,jsort,nst,xisteq)                               
        xisteq = min(xisteq,xlim)                                               
      endif                                                                     
      if (qhydth) then                                                          
        call gshm(hydn,conc,jsort,nst,shm,dshm)                                 
        if (shm .gt. shmlim) then                                               
          if (iter .le. 4) then                                                 
            shm = shmlim                                                        
            dshm = 1.0 - (shm/om)                                               
          elseif ( (om - shm) .le. eps100 ) then                                
c                                                                               
c           shm exceeds the physical limit of about 55.51.                      
c                                                                               
            write (noutpl,343) shm,om                                           
  343       format(/' hydration theory has blown up-',                          
     $      /3x,'the calculated value of sigma (hm) is ',1pe12.5,               
     $      /3x,'this exceeds the physical limit of ',0pf10.5,                  
     $      /3x,'no physical solution exists.',/)                               
            ier = 700                                                           
            go to 999                                                           
          endif                                                                 
        endif                                                                   
      endif                                                                     
c                                                                               
      if (qpt4) call molint(conc,nsq,nst,cdrs,nsqmx1,nsb,3,concbs)              
      call gcoeff(conc,z,zsq2,azero,hydn,concbs,glg,xi,xisteq,                  
     $ dshm,tempc,press,jsflag,jsort,iacion,nsb,nst,nsq,nhydr,nchlor)           
c                                                                               
      if (qpr) then                                                             
        write (noutpl,2205)                                                     
 2205   format(/9x,'ns',6x,'name',10x,'glg',9x,'glgo',/)                        
        ns = 1                                                                  
        write (noutpl,2206) ns,uspec(ns),glg(ns),glgo(ns)                       
 2206   format(7x,i5,2x,a12,3x,1pe12.5,3x,1pe12.5)                              
        do 300 ns = 2,nst                                                       
        if (conc(ns) .gt. 0.)                                                   
     $  write (noutpl,2206) ns,uspec(ns),glg(ns),glgo(ns)                       
  300   continue                                                                
      endif                                                                     
c                                                                               
c     recalculate the concentrations of dependent species, etc.                 
c                                                                               
      call ncmpe                                                                
c                                                                               
  999 continue                                                                  
      end                                                                       
c nrstep   last revised 12/28/87 by tjw                                         
      subroutine nrstep(aa,gm,rhs,res,ee,ir,zvclg1,alpha,beta,del,              
     $ betao,delo,screwd,screwn,betamx,delmax,delfnc,betfnc,uzvec1,             
     $ itermx,kdim,kmax,idelmx,iter,negdfc,negbfc,npconv,ier,                   
     $ matrxe,ncmpe,betae,qcbeta,qcgam,qprb,qprc)                               
c                                                                               
c     this routine performs one newton-raphson iteration step.                  
c                                                                               
c     input                                                                     
c       res = floating point work space vector                                  
c       ee = floating point work space vector                                   
c       ir = integer work space vector                                          
c       screwd = under-relaxation control parameter.  it is used to             
c                reduce the magnitude of the del vector, if necessary           
c                so that the magnitude of the largest element of that           
c                vector does not exceed screwd.                                 
c       screwn = under-relaxation control parameter                             
c       itermx = maximum number of iterations                                   
c       kdim = dimension of aa                                                  
c       kmax = maximum dimension of aa                                          
c       matrxe = external that writes aa, matrix for eq3nr, matrxz              
c                for eq6                                                        
c       ncmpe = external that expands the system description so that            
c               residuals can be calculated, ncmpx for eq3nr, ncmpz             
c               for eq6.  mostly this involves calculating the                  
c               concentrations of dependent aqueous species.                    
c       betae = external that calculates the beta array, betas for              
c               eq3nr, betaz for eq6.                                           
c       qcbeta = true if the convergence criterion on betamx was met            
c       qcgam = true if the convergence criterion on activity                   
c               coefficients was met                                            
c       qprb = debugging print flag, if true, causes most details to            
c              be printed (does not cause the jacobian matrix to be             
c              printed)                                                         
c       qprc = debugging print flag, if true, causes the jacobian               
c              matrix to be printed                                             
c                                                                               
c     output                                                                    
c       aa = jacobian matrix                                                    
c       gm = copy of aa                                                         
c       rhs = right hand side vector                                            
c       alpha = residual function array                                         
c       idelmx = kcol index corresponding to delmax                             
c       delfnc = convergence function, defined by reference to delmax           
c       betfnc = convergence function, defined by reference to betamx           
c       uzvec1 = name array corresponding to zvclg1                             
c       ier = error flag, okay if .le. 0                                        
c             = 1  if divergence was detected                                   
c             = 2  if iteration terminated because of an ill-posed              
c                  matrix                                                       
c                                                                               
c     input/output                                                              
c       iter = the number of newton-raphson iterations                          
c       negdfc = the number of successive iterations that the                   
c                 convergence function delfnc has been zero or negative         
c       negbfc = the number of successive iterations that the                   
c                 convergence function betfnc has been zero or negative         
c       npconv = number of successive steps in which the convergence            
c                criteria on the residual norm has been satisfied               
c       zvclg1 = the 'log z' array, the array corrected by                      
c                newton-raphson iteration                                       
c       beta = normalized residual function array                               
c       del = correction array                                                  
c       betao = old beta array                                                  
c       delo = old delo array                                                   
c       betamx = norm (largest magnitude) of the beta array                     
c       delmax = norm (largest magntiude) of the del array                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
      include "eqlgp.h"                                                  
      include "eqlpp.h"                                                  
      include "eqlun.h"                                                  
c                                                                               
      character*(*) uzvec1(*)                                                   
c                                                                               
      dimension aa(kmax,*),gm(kmax,*),ee(*),res(*),zvclg1(*),rhs(*)             
      dimension alpha(*),beta(*),del(*),betao(*),delo(*)                        
c                                                                               
      external matrxe,ncmpe,betae                                               
c                                                                               
      data qfalse/.false./                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     ncut is the number of times the del array has been cut in                 
c     magntitude to satisfy  the under-relaxation algorithm that limits         
c     residual growth.                                                          
c                                                                               
      ncut = 0                                                                  
c                                                                               
c     check to see if iteration should be stopped because there is              
c     little probability of further improvement.  if any of the                 
c     following cause iteration to stop, the result is considered               
c     acceptable if it satisfies the convergence criteria that                  
c     apply to residual functions and activity coefficients..                   
c                                                                               
c        1. convergence criteria on residuals and activity coefficients         
c           are satisfied are four consecutive iterations.                      
c                                                                               
      if (qcbeta .and. qcgam) then                                              
        npconv = npconv + 1                                                     
        if (npconv .ge. 4) go to 210                                            
      else                                                                      
        npconv = 0                                                              
      endif                                                                     
c                                                                               
c        2. the improvement function delfnc is negative or zero for             
c           six consecutive iterations.                                         
c                                                                               
      if (iter .gt. 8) then                                                     
        if (delfnc .lt. 0.) then                                                
          negdfc = negdfc + 1                                                   
          if (negdfc .ge. 6) go to 210                                          
        else                                                                    
          negdfc = 0                                                            
        endif                                                                   
      endif                                                                     
c                                                                               
c        3. the improvement function betfnc is negative or zero for             
c           six consecutive iterations.                                         
c                                                                               
      if (iter .gt. 8) then                                                     
        if (betfnc .lt. 0.) then                                                
          negbfc = negbfc + 1                                                   
          if (negbfc .ge. 6) go to 210                                          
        else                                                                    
          negbfc = 0                                                            
        endif                                                                   
      endif                                                                     
c                                                                               
c        4. the maximum number of iterations have been done.                    
c                                                                               
      if (iter .ge. itermx) then                                                
        write (noutpl,2090)                                                     
 2090   format(' --- the maximum number of iterations have',                    
     $   ' been done ---')                                                      
        go to 210                                                               
      endif                                                                     
c                                                                               
c     do another newton-raphson iteration.                                      
c                                                                               
      iter = iter + 1                                                           
c                                                                               
c     calculate the jacobian matrix (matrxe = matrix for eq3nr,                 
c     matrxz for eq6)                                                           
c                                                                               
      call matrxe                                                               
c                                                                               
      do 235 krow = 1,kdim                                                      
      rhs(krow) = -alpha(krow)                                                  
  235 continue                                                                  
c                                                                               
      if (qprc) then                                                            
        write (noutpl,1210)                                                     
 1210   format(/5x,'--- the aa array, with rhs ---',/)                          
c                                                                               
        do 22 krow = 1,kdim                                                     
        write (noutpl,1216) krow,uzvec1(krow)                                   
 1216   format(1x,i3,2x,a24)                                                    
        write (noutpl,1212) (aa(krow,i),i=1,kdim),rhs(krow)                     
 1212   format((5(2x,g10.3),2x,g10.3))                                          
        write(noutpl,1214)                                                      
 1214   format(' ')                                                             
   22   continue                                                                
      endif                                                                     
c                                                                               
c     solve the jacobian system.                                                
c                                                                               
      call msolvr(aa,gm,rhs,del,res,ee,ir,kdim,kmax,ier,qfalse,                 
     $ qfalse)                                                                  
      if (ier .gt. 0) then                                                      
        ier = 2                                                                 
        go to 999                                                               
      endif                                                                     
c                                                                               
c     determine which correction terms are largest in magnitude.                
c     in the limit of the solution, the (unrelaxed) correction                  
c     terms bound the error in the variables being calculated.                  
c                                                                               
      ier = 0                                                                   
      delmax = 0.                                                               
      idelmx = 0                                                                
      do 257 kcol = 1,kdim                                                      
      adel = abs(del(kcol))                                                     
      if (adel .gt. delmax) then                                                
        delmax = adel                                                           
        idelmx = kcol                                                           
      endif                                                                     
  257 continue                                                                  
c                                                                               
c     apply under-relaxation according to various criteria.  this               
c     application precedes evaluation of new residuals.  another                
c     application follows such evaluation.                                      
c                                                                               
c       maintain bound on the largest magnitude of any correction               
c                                                                               
      adel = abs(del(idelmx))                                                   
      if (adel .gt. screwd) then                                                
        facurx = screwd/adel                                                    
        do 260 krow = 1,kdim                                                    
        del(krow) = del(krow)*facurx                                            
  260   continue                                                                
      endif                                                                     
c                                                                               
c       if the residual norm was large or the convergence function              
c       has been small, go slow.                                                
c                                                                               
      if (iter. le. 8) then                                                     
        if (betamx.gt.2. .or. betfnc.lt.0.20) then                              
          if (betamx .gt. screwn) then                                          
            do 270 kcol = 1,kdim                                                
            del(kcol) = del(kcol)*0.5                                           
  270       continue                                                            
          endif                                                                 
        endif                                                                   
      endif                                                                     
c                                                                               
c       under-relax if necessary to avoid oscillation                           
c                                                                               
      adum = del(idelmx)                                                        
      bdum = (2**ndamp)*delo(idelmx)                                            
      if ((adum*bdum).ge.0. .or.                                                
     $  abs((adum+bdum)/adum).gt.0.05) then                                     
        ndamp = 0                                                               
      else                                                                      
        ndamp = ndamp + 1                                                       
        facurx = 1./(2**ndamp)                                                  
        do 920 kcol = 1,kdim                                                    
        del(kcol) = facurx*del(kcol)                                            
  920   continue                                                                
      endif                                                                     
c                                                                               
c     apply correction terms.  the label below is a return point                
c     for post-residual evaluation under-relaxation.                            
c                                                                               
      ncut = -1                                                                 
  275 ncut = ncut + 1                                                           
      if (qprb) write (noutpl,2133) ncut                                        
 2133 format(/3x,'ncut= ',i3,/)                                                 
c                                                                               
      do 280 kcol = 1,kdim                                                      
      zvclg1(kcol) = zvclg1(kcol) + del(kcol)                                   
  280 continue                                                                  
c                                                                               
      if (qprb) then                                                            
        write (noutpl,2160)                                                     
 2160   format(/3x,'kcol',3x,'uzvec1',10x,'correction',3x,'zvclg1',/)           
        do 281 kcol = 1,kdim                                                    
        write (noutpl,2145) kcol,uzvec1(kcol),del(kcol),zvclg1(kcol)            
 2145   format(3x,i3,2x,a12,3x,1pe15.8,3x,0pf15.10)                             
  281   continue                                                                
      endif                                                                     
c                                                                               
c     expand the system and recalculate the newton-raphson residual             
c     functions.  it is important here to keep the activity coefficients        
c     constant,that is, not to update them until all under-relaxation           
c     has been completed.  ncmpe = ncmpx for eq3nr, ncmpz for eq6.              
c     betae = betas for eq3nr, betaz for eq6.                                   
c                                                                               
      call ncmpe                                                                
      call betae(qfalse,qfalse)                                                 
c                                                                               
      if (qprb) then                                                            
        write (noutpl,2150)                                                     
 2150   format(//7x,'kcol',3x,'uzvec1',10x,'beta',/)                            
        do 968 kcol = 1,kdim                                                    
        write (noutpl,2145) kcol,uzvec1(kcol),beta(kcol)                        
  968   continue                                                                
        write (noutpl,2135)                                                     
 2135   format(1x)                                                              
      endif                                                                     
c                                                                               
c       under-relax to control residual growth.  terminate                      
c       if residual tracking indicates that the iteration                       
c       is diverging.                                                           
c                                                                               
      if (iter .le. 12) then                                                    
        divfmx = 0.                                                             
        do 160 kcol = 1,kdim                                                    
        divfnc = abs(beta(kcol)) - betao(kcol)                                  
        if (divfmx .gt. divfnc) divfmx = divfnc                                 
  160   continue                                                                
        if (divfmx .gt. 0.10) then                                              
          ncut = ncut + 1                                                       
          if (ncut .le. 8) then                                                 
            if (qprb) write (noutpl,2180)                                       
 2180       format(' --- residual tracking requires',                           
     $      ' under-relaxation ---')                                            
            do 175 kcol = 1,kdim                                                
            zvclg1(kcol) = zvclg1(kcol) - del(kcol)                             
            del(kcol) = 0.5*del(kcol)                                           
  175       continue                                                            
            go to 275                                                           
          endif                                                                 
          if (iter .gt. 8) then                                                 
            write (noutpl,2020)                                                 
 2020       format(' --- residual tracking indicates diverging',                
     $      ' iteration',/' --- newton terminating ----')                       
            go to 210                                                           
          endif                                                                 
        endif                                                                   
      endif                                                                     
c                                                                               
      if (qprb) write (noutpl,2138)                                             
 2138 format(/3x,'a newton-raphson correction has been',                        
     $' completed',/)                                                           
      go to 999                                                                 
c                                                                               
c                                                                               
  210 ier = 1                                                                   
c                                                                               
  999 continue                                                                  
      end                                                                       
c omega4   last revised 10/27/87 by tjw                                         
c*omega4 f77 check, 07/08/87 by tjw                                             
      subroutine omega4(ns,omegns,zdum,ierr)                                    
c                                                                               
c     created 10feb87 rmm                                                       
c     get omega for hkf equations from common eqlhkf                            
c     created 10feb87 by rmm                                                    
c                                                                               
c     input   ns      index of species of interest                              
c                                                                               
c     output  omegns  omega value for ns-th species                             
c             ierr    0/1  no error/no omega found for nsth species             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqlhkf.h"                                                 
      include "eqlun.h"                                                  
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      qck = .false.                                                             
      omegns=0.0                                                                
      ierr=0                                                                    
c                                                                               
      do 100 i=1,ibjuse                                                         
      jj = indx1(i)                                                             
      if (ns .eq. jj) then                                                      
        omegns=omega(i)                                                         
        go to 200                                                               
      endif                                                                     
  100 continue                                                                  
c                                                                               
c              ns-th species not in hkf database                                
c              assign a value for omega based on charge                         
c                                                                               
      izdum = nint(zdum)                                                        
      if (qck) then                                                             
        write (noutpl,8000) ns,izdum                                            
 8000   format('for ns= ',i3,' izdum from omega4 ',i3)                          
      endif                                                                     
c                                                                               
      if (izdum .ge. 3) then                                                    
        omegns=4.0168e+05                                                       
      elseif (izdum .eq. 2) then                                                
        omegns=2.2289e+05                                                       
      elseif (izdum .eq. 1) then                                                
        omegns=.68044e+05                                                       
      elseif (izdum .eq. 0) then                                                
        omegns=0.5e+05                                                          
      elseif (izdum .eq. -1) then                                               
        omegns=.83014e+05                                                       
      elseif (izdum .le. -2) then                                               
        omegns=2.2137e+05                                                       
      else                                                                      
        ierr = 1                                                                
      endif                                                                     
c                                                                               
c     write (noutpl,1000) ns                                                    
c1000 format(' ** warning no omega found for species with index = ',i3)         
c                                                                               
  200 continue                                                                  
c ## debug                                                                      
      if (qck) write(noutpl,9000) ns,omegns                                     
 9000 format(' for species ',i3,' omega value is ',e15.5)                       
c                                                                               
      end                                                                       
c openin   last revised 07/08/87 by tjw                                         
c*openin f77 check, 07/08/87 by tjw                                             
      subroutine openin(ufiln,uform,ilu)                                        
c                                                                               
c     open an input type file.  the file must already exist.                    
c     an unused logical unit number is obtained.                                
c                                                                               
c     input   ufiln   ascii file name to open (error message format             
c                       assumes no more than twelve characters)                 
c             uform   file form, 'formatted' or 'unformatted'                   
c     output  ilu     logical unit number of opened file                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      character*(*) ufiln,uform                                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     check to make sure the file exists.                                       
c                                                                               
      inquire(file=ufiln,exist=qex,formatted=uformo)                            
      if (.not.qex) then                                                        
        write (noutpl,100) ufiln                                                
        write (nttyol,100) ufiln                                                
  100   format(' * error - file ',a12,' does not exist (eqlib/openin)')         
      stop                                                                      
      endif                                                                     
c                                                                               
c     check the file format                                                     
c                                                                               
      if (uform.eq.'formatted'.and.uformo.eq.'no') then                         
        write (noutpl,101) ufiln                                                
        write (nttyol,101) ufiln                                                
  101   format('* error - file ',a12,' should be formatted ',                   
     $  'but is not (eqlib/openin)')                                            
      stop                                                                      
      endif                                                                     
c                                                                               
      if (uform.eq.'unformatted'.and.uformo.eq.'yes') then                      
c       write (noutpl,102) ufiln                                                
c       write (nttyol,102) ufiln                                                
c 102   format('* error - file ',a12,' should be unformatted ',                 
c    $  'but is not (eqlib/openin)')                                            
c     stop                                                                      
      endif                                                                     
c                                                                               
c     get logical unit number                                                   
c                                                                               
      call getlu(ilu,nerr)                                                      
      if (nerr .ne. 0) then                                                     
        write (noutpl,105) ufiln                                                
        write (nttyol,105) ufiln                                                
  105   format(' * error - no l u available for',a12,' (eqlib/openin)')         
      stop                                                                      
      endif                                                                     
c                                                                               
      open(ilu,file=ufiln,form=uform,status='old',err=10)                       
      go to 999                                                                 
c                                                                               
   10 continue                                                                  
      write(noutpl,110) ufiln                                                   
      write(nttyol,110) ufiln                                                   
  110 format(' * error - can not open ',a12,' (eqlib/openin)')                  
      stop                                                                      
c                                                                               
  999 continue                                                                  
      end                                                                       
c openou   last revised 07/08/87 by tjw                                         
c*openou f77 check, 07/08/87 by tjw                                             
      subroutine openou(ufiln,uform,ilu)                                        
c                                                                               
c     open an output type file.  if the file of the same name                   
c     already exists, it is first destroyed.  an unused logical                 
c     unit number is obtained.                                                  
c                                                                               
c     input   ufiln   ascii file name to open (error message format             
c                       assumes no more than twelve characters)                 
c             uform   file form, 'formatted' or 'unformatted'                   
c     output  ilu     logical unit number of opened file                        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      character*(*) ufiln,uform                                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     see if a file of the same name already exists.  if so,                    
c     destroy it.  this provides a logical unit number.  if a                   
c     file of the same name does not exist, get a logical unit number.          
c                                                                               
      inquire(file=ufiln,exist=qex)                                             
      if (qex) then                                                             
        ustat='old'                                                             
        call getlu(ilu,nerr)                                                    
        if (nerr .ne. 0) then                                                   
          write (noutpl,100) ustat,ufiln                                        
          write (nttyol,100) ustat,ufiln                                        
  100     format(' * error - no l u available to open ',                        
     $    a3,' copy of ',a12,' (eqlib/openou)')                                 
          stop                                                                  
        endif                                                                   
        open(ilu,file=ufiln,status=ustat,err=10)                                
        close(ilu,status='delete',err=15)                                       
      else                                                                      
        ustat='new'                                                             
        call getlu(ilu,nerr)                                                    
        if (nerr .ne. 0) then                                                   
          write (noutpl,100) ustat,ufiln                                        
          write (nttyol,100) ustat,ufiln                                        
          stop                                                                  
        endif                                                                   
      endif                                                                     
c                                                                               
c     open the new file                                                         
c                                                                               
      open(ilu,file=ufiln,form=uform,status='new',err=10)                       
      go to 999                                                                 
c                                                                               
   10 write(noutpl,105) ustat,ufiln                                             
      write(nttyol,105) ustat,ufiln                                             
  105 format(' * error - can not open ',a3,' copy of ',a12,                     
     $ ' (eqlib/openou)')                                                       
      stop                                                                      
c                                                                               
   15 write (noutpl,110) ufiln                                                  
      write (nttyol,110) ufiln                                                  
  110 format(' * error - can not delete old copy of ',a12,                      
     $ ' (eqlib/openou)')                                                       
      stop                                                                      
c                                                                               
  999 continue                                                                  
      end                                                                       
c pjdd     last revised 07/08/87 by tjw                                         
c*pjdd f77 check, 07/08/87 by tjw                                               
      subroutine pjdd(x,pj,pj1,pj2,ier)                                         
c                                                                               
c     input x                                                                   
c     compute pj = j(x)                                                         
c             pj1 = j'(x)                                                       
c             pj2 = j''(x)                                                      
c     using pitzer's equations   pitzer(1975)                                   
c                                                                               
c     ier = 0/-1  ok/x out of range                                             
c     x must be .gt. 0. and .le. 900.                                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      data c1,c2,c3,c4 /4.581,0.7237,0.0120,0.528/                              
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      pj = 0.                                                                   
      pj1 = 0.                                                                  
      pj2 = 0.                                                                  
      ier = -1                                                                  
c                                                                               
      if (x .le. 0. .or. x .gt. 900.) go to 999                                 
      ier = 0                                                                   
      alx = log(x)                                                              
c                                                                               
c      get x**(-c2) and x**(c4) and e**(-c3 x**c4)                              
c                                                                               
      xc2 = exp(-c2 * alx)                                                      
      c3xc4 = c3*exp(c4 * alx)                                                  
      ec3xc4 = exp(-c3xc4)                                                      
c                                                                               
      pj = x / (4. + c1*xc2*ec3xc4)                                             
c                                                                               
c      compute ?2?                                                              
c                                                                               
      t = c4*c3xc4                                                              
      xc2p = exp((-c2 - 1.)*alx)                                                
      bk2 = c1*xc2p *(c2 + t)*ec3xc4                                            
c                                                                               
      pj1 = (pj / x)*(1. + pj*bk2)                                              
c                                                                               
      bk3 = (t/x)*((c4 / (c2 + t)) - (c2 + 1.) / t)                             
      pj2 = ((pj1 / pj) - 1. / x)*(2.*pj1 + pj*bk3)                             
c                                                                               
  999 continue                                                                  
      end                                                                       
c polx     last revised 11/16/87 by tjw                                         
c*polx f77 check, 07/08/87 by tjw                                               
      subroutine polx (x,y,n,c,ier)                                             
c                                                                               
c     fit an exact polynomial through n points, real*8 case                     
c                                                                               
c     input                                                                     
c      x     array of independent variable values                               
c      y     corresponding array of dependent variable values                   
c      n     number of values in  x,y                                           
c        note - n .le. 10                                                       
c                                                                               
c     output                                                                    
c      c     array of coefficients of a polynomial of degree n-1                
c            passing through the set of points  x,y                             
c      ier   0/1  ok/unable to solve matrix equation                            
c                                                                               
c     method is to form the matrix equation  ac = b                             
c      where                                                                    
c           a = ( 1  x(1)  x(1)**2 ... )    b = ( y(1) )                        
c               ( 1  x(2)  x(2)**2 ... )        ( y(2) )                        
c               ( ...                  )        ( ...  )                        
c               ( 1  x(n)  x(n)**2 ... )        ( y(n) )                        
c                                                                               
c      and solve for the coefficients c(i)                                      
c        y = c(1) + c(2)*x + c(3)*x**2 + ... + c(n)*x**n-1                      
c                                                                               
c     it is assumed that the x array contains distinct values                   
c      (no duplicates)                                                          
c                                                                               
c     this code needs the machine epsilon, eps, which is calculated             
c     by flpars.  this is usually initialized by a call to eqlib, which         
c     calls flpars.                                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqleps.h"                                                 
c                                                                               
      dimension x(*),y(*),c(*)                                                  
      dimension a(ipfpar,ipfpar),gm(ipfpar,ipfpar),rhs(ipfpar),                 
     $ ipvt(ipfpar),res(ipfpar),ee(ipfpar)                                      
c                                                                               
      data qfalse/.false./                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c      initialize                                                               
c                                                                               
      ier = 0                                                                   
      ipfmax = ipfpar                                                           
c                                                                               
c      generate matrix of eqn values                                            
c                                                                               
      do 30 j=1,n                                                               
      c(j) = 0.                                                                 
      rhs(j) = y(j)                                                             
c                                                                               
      ai = 1.                                                                   
      a(j,1) = 1.                                                               
c                                                                               
      do 20 i=2,n                                                               
      ai = ai * x(j)                                                            
      a(j,i) = ai                                                               
   20 continue                                                                  
c                                                                               
   30 continue                                                                  
c                                                                               
c     solve the matrix equation                                                 
c                                                                               
      call msolvr(a,gm,rhs,c,res,ee,ipvt,n,ipfmax,ier,qfalse,qfalse)            
c                                                                               
      end                                                                       
c prpheh   last revised 12/14/87 by tjw                                         
c*prpheh created in f77  12/12/87 by tjw                                        
      subroutine prpheh(ph,phnbs,phrat,phcl,eh,ehnbs,ehrat,pe,                  
     $ penbs,perat,iopg2,nchlor,noutpt,qnochb)                                  
c                                                                               
c     this routine prints the ph, redox potential, and pe                       
c     (electron activity function) on the operational ph scale used             
c     in the calculations (see iopg2), the modified nbs ph scale,               
c     and the rational ph scale.                                                
c                                                                               
c     input                                                                     
c       ph = ph on scale used in computation                                    
c       phnbs = ph on the modified nbs scale                                    
c       phrat = ph on the rational scale                                        
c       phcl = ph + pcl                                                         
c       eh = eh corresponding to the ph used in the computation                 
c       ehnbs = eh corresponding to the phnbs                                   
c       ehrat = eh corresponding to the phrat                                   
c       pe = pe function corresonding to eh                                     
c       penbs = pe function corresonding to ehnbs                               
c       perat = pe function corresonding to ehrat                               
c       iopg2 = activity coefficient option switch, defines the                 
c               operational ph scale used in the computation                    
c               = -1   internal scale defined for the activity                  
c                      coefficinet model defined by iopg1                       
c                = 0   the modified nbs ph scale (log gamma cl-                 
c                      defined by bates-guggenheim expression)                  
c                = 2   the rational ph scale (log gamma h+ defined              
c                      as zero)                                                 
c       nchlor = index of the chloride ion                                      
c       noutpt = unit number of the output file                                 
c       qnochb = logical flag, = .true. if no oxidation-reduction               
c                in the modeled system                                          
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*24 uphscl(3)                                                    
c                                                                               
      data (uphscl(i), i = 1,3)/'internal ph scale       ',                     
     $ 'modified nbs ph scale   ','rational ph scale       '/                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      i = iopg2 + 2                                                             
      write (noutpt,100) uphscl(i)                                              
  100 format(//3x,'single ion activities and activity coefficients',            
     $ ' are here defined',/3x,'with respect to the ',a24,/)                    
c                                                                               
      if (qnochb) then                                                          
        write (noutpt,102)                                                      
  102   format(/39x,'ph',/)                                                     
        if (iopg2 .eq. -1) then                                                 
          write (noutpt,104) uphscl(1),ph                                       
  104     format(5x,a24,3x,f11.4)                                               
        endif                                                                   
c                                                                               
        write (noutpt,104) uphscl(2),phnbs                                      
        write (noutpt,104) uphscl(3),phrat                                      
      else                                                                      
        write (noutpt,106)                                                      
  106   format(//39x,'ph',11x,'eh',11x,'pe',/)                                  
c                                                                               
        if (iopg2 .eq. -1) then                                                 
          write (noutpt,108) uphscl(1),ph,eh,pe                                 
  108     format(5x,a24,3x,f11.4,3x,f11.4,3x,1pe11.4)                           
        endif                                                                   
c                                                                               
        write (noutpt,108) uphscl(2),phnbs,ehnbs,penbs                          
        write (noutpt,108) uphscl(3),phrat,ehrat,perat                          
      endif                                                                     
c                                                                               
      if (nchlor .gt. 0) write (noutpt,110) phcl                                
  110 format(/3x,'phcl = ',f11.4)                                               
c                                                                               
      write (noutpt,112)                                                        
  112 format(1x)                                                                
c                                                                               
      end                                                                       
c prreac  last revised 12/11/87 by tjw                                          
c*preac f77 rewrite 11/23/87 by tjw                                             
      subroutine prreac(cdrx,uspec,unamsp,n,nsq,nsq1,nsqmx1,nf)                 
c                                                                               
c     this routine writes the n-th reaction in a set.                           
c                                                                               
c     input                                                                     
c       cdrx = the array of reaction coefficients for a given set               
c              cdrx = cdrs for aqueous species dissociation reactions           
c              cdrx = cdrm for mineral dissolution reactions                    
c              cdrx = cdrg for gas dissolution reactions                        
c       uspec = array of aqueous species names                                  
c       unamsp = name of species associated with the reaction                   
c       n = the index of the reaction                                           
c       nsqmx1 = maximum number of aqueous master species plus one;             
c                the leading dimension of cdrx                                  
c       nsq = number of aqueous master species                                  
c       nsq1 = number of aqueous master species plus one                        
c       nf = the unit number of the file to write on                            
c                                                                               
c     output                                                                    
c       none                                                                    
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*(*) uspec(*),unamsp                                             
c                                                                               
      dimension cdrx(nsqmx1,*)                                                  
c                                                                               
c--------------------------------------------------------------------           
c                                                                               
      write (nf,100)                                                            
  100 format(1x)                                                                
c                                                                               
c     print coefficient and name of the associated species                      
c                                                                               
      coeff = -cdrx(nsq1,n)                                                     
      write (nf,105) coeff,unamsp                                               
  105 format(21x,f7.3,2x,a24)                                                   
c                                                                               
c     print the coefficients and names of other reactants                       
c                                                                               
      do 5 ns = 1,nsq                                                           
      coeff = -cdrx(ns,n)                                                       
      if (coeff .gt. 0.) then                                                   
        write (nf,110) coeff,uspec(ns)                                          
  110   format(20x,'+',f7.3,2x,a24)                                             
      endif                                                                     
    5 continue                                                                  
c                                                                               
      write (nf,115)                                                            
  115 format(27x,'==')                                                          
c                                                                               
c     print the coefficients and names of the products                          
c                                                                               
      nj=0                                                                      
      do 10 ns=1,nsq                                                            
      coeff = cdrx(ns,n)                                                        
      if (coeff .gt. 0.) then                                                   
        if (nj .le. 0) then                                                     
          write (nf,105) coeff,uspec(ns)                                        
          nj=1                                                                  
        else                                                                    
          write (nf,110) coeff,uspec(ns)                                        
        endif                                                                   
      endif                                                                     
   10 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c ptztab   last revised 11/19/87 by tjw                                         
c*ptztab f77 check, 07/08/87 by tjw                                             
      subroutine ptztab(npair,ipairs,ntripl,itripl,nspec,uspec,                 
     $ jsflag,iprint)                                                           
c                                                                               
c  purpose:  tabulates pitzer coefficients for each species in                  
c           aqueous solution                                                    
c                                                                               
c  input arguments:                                                             
c      npair:    number of lamda pairs in database                              
c      ipairs:   array with indexes of lamda pairs in database                  
c      ntripl:   number of mu triplets in database                              
c      itripl:  array with indexes of mu triples in database                    
c      nspec:    total number of aqueous species in database                    
c      uspec:   names of all aqueous species in database                        
c      iprint:   print variable for type of tabulation                          
c                0 = only warnings                                              
c                1 = print warnings and number of pairs/triples                 
c                2 = print warnings and names of pairs/triples                  
c      jsflag:     0/1 = aqueous species in model/not in model                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "impliciti.h"                                              
c                                                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      character*24 uspec(*)                                                     
c                                                                               
      character*24 upair(2,nslpar),utripl(3,nmupar),ublank                      
c                                                                               
      dimension ipairs(2,*),itripl(3,*),jsflag(*)                               
c                                                                               
      data ublank /'                        '/                                  
      data uh2,uo2 /'H2(AQ)  ','O2(AQ)  '/                                      
c                                                                               
c----------------------------------------------------------------               
c                                                                               
      lamcnt = 0                                                                
      mucnt = 0                                                                 
c                                                                               
c  set largest dimension                                                        
c                                                                               
c   check validity of iprint                                                    
c                                                                               
      if ((iprint .gt. 2) .or. (iprint .lt. 0)) then                            
        write (noutpl,500)                                                      
        write (nttyol,500)                                                      
  500   format(' * info - initial choice for iopr9 was invalid ',/              
     $  '   therefore, iopr9 was reset to 0 (eqlib/ptztab)')                    
      endif                                                                     
c                                                                               
c  write out headings for tables                                                
c                                                                               
      if (iprint .ne. 0) then                                                   
      write (noutpl,1000)                                                       
 1000 format(/,                                                                 
     $' species       lamda pairs                      mu triplets',/)         
      endif                                                                     
c                                                                               
c  begin loop -- go through each aqueous species in model                       
c  start with number 2 -- water is no. 1                                        
c  blank out arrays                                                             
c                                                                               
      do 200 imodel = 2,nspec                                                   
      do 10 i = 1, npair                                                        
        upair(1,i) = ublank                                                     
        upair(2,i) = ublank                                                     
   10 continue                                                                  
c                                                                               
      do 12 i = 1, ntripl                                                       
        utripl(1,i) = ublank                                                    
        utripl(2,i) = ublank                                                    
        utripl(3,i) = ublank                                                    
   12 continue                                                                  
c                                                                               
      lamcnt = 0                                                                
      mucnt = 0                                                                 
      if (jsflag(imodel) .ne. 0) go to 200                                      
c                                                                               
c  check if imodel matches any component in lamda pair                          
c                                                                               
      do 25 j = 1, npair                                                        
        qdoit = .false.                                                         
        if (imodel .eq. ipairs(1,j)) then                                       
          qdoit = .true.                                                        
          isrcha = 2                                                            
        else if (imodel .eq. ipairs(2,j)) then                                  
          qdoit = .true.                                                        
          isrcha = 1                                                            
        endif                                                                   
c                                                                               
c  test if other component matches any of the aqueous species in model          
c                                                                               
        if (qdoit) then                                                         
          do 20 idum1 = 1,nspec                                                 
            if (jsflag(idum1) .ne. 0) go to 20                                  
            if (ipairs(isrcha,j) .eq. idum1) then                               
              lamcnt = lamcnt + 1                                               
              upair(1,lamcnt) = uspec(ipairs(1,j))                              
              upair(2,lamcnt) = uspec(ipairs(2,j))                              
            endif                                                               
   20       continue                                                            
        endif                                                                   
   25 continue                                                                  
c                                                                               
c  if no match with any lamda pair then skip check of triplets                  
c                                                                               
      if (lamcnt .eq. 0) go to 100                                              
c                                                                               
c  check for mu triplets                                                        
c                                                                               
      do 60 k = 1, ntripl                                                       
        qdoit = .false.                                                         
        if (imodel .eq. itripl(1,k)) then                                       
          qdoit = .true.                                                        
          isrcha = 2                                                            
          isrchb = 3                                                            
        else if (imodel .eq. itripl(2,k)) then                                  
          qdoit = .true.                                                        
          isrcha = 1                                                            
          isrchb = 3                                                            
        else if (imodel .eq. itripl(3,k)) then                                  
          qdoit = .true.                                                        
          isrcha = 1                                                            
          isrchb = 2                                                            
        endif                                                                   
c                                                                               
c  check if one other component matches                                         
c                                                                               
        if (qdoit) then                                                         
          do 50 idum1 = 1,nspec                                                 
          if (jsflag(idum1) .ne. 0) go to 50                                    
          if (itripl(isrcha,k) .eq. idum1) then                                 
c  check if last component is in model                                          
            do 40 idum2 = 1,nspec                                               
            if (jsflag(idum2) .ne. 0) go to 40                                  
            if (itripl(isrchb,k) .eq. idum2) then                               
              mucnt = mucnt + 1                                                 
              utripl(1,mucnt) = uspec(itripl(1,k))                              
              utripl(2,mucnt) = uspec(itripl(2,k))                              
              utripl(3,mucnt) = uspec(itripl(3,k))                              
              go to 60                                                          
            endif                                                               
   40       continue                                                            
          endif                                                                 
   50     continue                                                              
        endif                                                                   
   60   continue                                                                
c                                                                               
c  done with all checks                                                         
c                                                                               
  100 continue                                                                  
c                                                                               
c  write out tables                                                             
c                                                                               
      if ((lamcnt .eq. 0) .and. (mucnt .eq. 0)) then                            
c                                                                               
c       write out warnings                                                      
c                                                                               
        if ((uspec(imodel)(1:8).ne.uh2).and.(uspec(imodel)(1:8)                 
     $    .ne.uo2)) then                                                        
          write (noutpl,2000) uspec(imodel)                                     
          write (nttyol,2000) uspec(imodel)                                     
 2000     format('**warning** ',a12,' is in model but not represented',         
     $    ' in pitzer coeffiecients')                                           
        endif                                                                   
      else if (iprint .eq. 2) then                                              
c                                                                               
c       write maximum output - iprint = 2 - names of coefficients               
c                                                                               
        if (lamcnt .gt. mucnt) then                                             
          imncnt = lamcnt                                                       
        else                                                                    
          imncnt = mucnt                                                        
        endif                                                                   
        write (noutpl,2500) uspec(imodel)                                       
 2500   format(1x,a12)                                                          
        write (noutpl,3000) (upair(1,i),upair(2,i),                             
     $  utripl(1,i),utripl(2,i),utripl(3,i),i=1,imncnt)                         
 3000   format(15x,2a8,15x,3a8)                                                 
        if (lamcnt .gt. imncnt) then                                            
          write (noutpl,3002) (upair(1,i),upair(2,i),i=imncnt,lamcnt)           
 3002     format(15x,2a8)                                                       
        endif                                                                   
        if (mucnt .gt. imncnt) then                                             
          write (noutpl,3004) (utripl(1,i),utripl(2,i),utripl(3,i),             
     $    i=imncnt,mucnt)                                                       
 3004     format(15x,2a8,15x,3a8)                                               
        endif                                                                   
        write (noutpl,3500)                                                     
 3500   format('---------------------------------------------------',           
     $          '-----------------')                                            
      else if (iprint .eq. 1) then                                              
c                                                                               
c       write minimum requested output - iprint 2 - number of                   
c       coefficients                                                            
c                                                                               
        write (noutpl,4000) uspec(imodel),lamcnt,mucnt                          
 4000   format(x,a12,4x,i3,32x,i3)                                              
      endif                                                                     
c                                                                               
c  end loop for one aqueous species in the model                                
c                                                                               
  200 continue                                                                  
c                                                                               
      end                                                                       
c qsort    last revised 07/08/87 by tjw                                         
c*qsort f77 check, 07/08/87 by tjw                                              
      subroutine qsort(a,b,n,j,istack,jstack)                                   
c                                                                               
c     this routine copies the n elements of array a into array b                
c     and sorts the elements of b in order of increasing value.                 
c                                                                               
c     the sorting algorithm is the quicksort method of                          
c     c.a.r. hoare.                                                             
c                                                                               
c     note that the arrays b, j, istack, and jstack should have the             
c     same dimension as array a.  array j contains the original                 
c     indices of the elements of array a.  arrays istack and                    
c     jstack are stacks containing the left and right indices                   
c     of lists to be sorted.                                                    
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension a(*),b(*),j(*),istack(*),jstack(*)                              
c                                                                               
      nstack=1                                                                  
      istack(1)=1                                                               
      jstack(1)=n                                                               
      do 10 jpt=1,n                                                             
      j(jpt)=jpt                                                                
      b(jpt)=a(jpt)                                                             
   10 continue                                                                  
c                                                                               
c     if the stack of lists is empty the sort is finished.                      
c                                                                               
   30 if (nstack.le.0) go to 999                                                
c                                                                               
c     remove the last list from the stack.                                      
c                                                                               
      iend=istack(nstack)                                                       
      jend=jstack(nstack)                                                       
      nstack=nstack-1                                                           
      ipt=iend                                                                  
      jpt=jend                                                                  
c                                                                               
   40 if (ipt.ge.jpt) go to 70                                                  
c                                                                               
c     move the left pointer as far as possible.                                 
c                                                                               
   50 if (b(ipt).gt.b(jpt)) go to 55                                            
      ipt=ipt+1                                                                 
      if (ipt.ge.jpt) go to 70                                                  
      go to 50                                                                  
c                                                                               
c     have found an out of order element.  exchange elements.                   
c                                                                               
   55 bdum=b(ipt)                                                               
      b(ipt)=b(jpt)                                                             
      b(jpt)=bdum                                                               
      jdum=j(ipt)                                                               
      j(ipt)=j(jpt)                                                             
      j(jpt)=jdum                                                               
c                                                                               
c     move the right pointer as far as possible.                                
c                                                                               
   60 if (b(ipt).gt.b(jpt)) go to 65                                            
      jpt=jpt-1                                                                 
      if (ipt.ge.jpt) go to 70                                                  
      go to 60                                                                  
c                                                                               
c     have found an out of order element.  exchange elements.                   
c                                                                               
   65 bdum=b(ipt)                                                               
      b(ipt)=b(jpt)                                                             
      b(jpt)=bdum                                                               
      jdum=j(ipt)                                                               
      j(ipt)=j(jpt)                                                             
      j(jpt)=jdum                                                               
      go to 40                                                                  
c                                                                               
c     jpt is the dividing point.  split the list in two, and                    
c     sort each independently.  avoid sorting empty lists.                      
c                                                                               
   70 kdum=jpt-1                                                                
      if (kdum.gt.iend) then                                                    
        nstack=nstack+1                                                         
        istack(nstack)=iend                                                     
        jstack(nstack)=kdum                                                     
      endif                                                                     
      kdum=jpt+1                                                                
      if (kdum.ge.jend) go to 30                                                
      nstack=nstack+1                                                           
      istack(nstack)=kdum                                                       
      jstack(nstack)=jend                                                       
      go to 30                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c readr    last revised 07/08/87 by tjw                                         
c*readr f77 check, 07/08/87 by tjw                                              
      subroutine readr(nmdar,ubuf,nlines,uterm,navb,irfp)                       
c                                                                               
c     input   nmdar   unit number of direct access data file                    
c             nlines  upper bound, number of lines (records) to read            
c             uterm   a8 ascii terminator (in first 8 char of line)             
c             navb    d a file index to next available record                   
c                      note - this is at eof                                    
c             irfp    record number (zero is first record)                      
c                      irfp is incremented 1 for each line read                 
c     output  ubuf   output buffer, 80 character lines                          
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      character*80 ubuf                                                         
c                                                                               
      dimension ubuf(*)                                                         
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 10 k=1,nlines                                                          
      read (nmdar,1020,rec=irfp,err=20,iostat=ios) ubuf(k)               
 1020 format(a80)                                                               
      irfp = irfp+1                                                             
      if (ubuf(k)(1:8) .eq. uterm) go to 15                                     
      if (irfp .ge. navb) go to 15                                              
   10 continue                                                                  
c                                                                               
   15 continue                                                                  
      go to 999                                                                 
c                                                                               
   20 continue                                                                  
      write (noutpl,1010) irfp                                                  
      write (nttyol,1010) irfp                                                  
 1010 format(' * error - reading block ',i8,' of direct access file',           
     $ /5x,'iostat=',i5,' (eqlib/readr)')                                       
      stop                                                                      
c                                                                               
                                                                               
  999 continue                                                                  
      end                                                                       
c rscal    last revised 07/08/87 by tjw                                         
c*rscal f77 check, 07/08/87 by tjw                                              
      subroutine rscal (c, n, xmax, crs)                                        
c                                                                               
c     rescale a set of values, real*8 case                                      
c                                                                               
c     input                                                                     
c      array c  of n real*8 values                                              
c      xmax  real*8 rescale factor                                              
c                                                                               
c      output                                                                   
c       array crs  of n real*8 values                                           
c       crs(1) = c(1)                                                           
c       crs(2) = c(2) / xmax                                                    
c       crs(3) = c(3) / xmax**2                                                 
c       crs(4) = c(4) / xmax**3                                                 
c        ...                                                                    
c       crs(n) = c(n) / xmax**n-1                                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension c(*), crs(*)                                                    
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      px = 1.                                                                   
      crs(1) = c(1)                                                             
c                                                                               
      do 20 k=2,n                                                               
      px = px / xmax                                                            
      crs(k) = c(k) * px                                                        
   20 continue                                                                  
c                                                                               
      end                                                                       
c sasum    last revised 07/08/87 by tjw                                         
c*sasum f77 rewrite, 07/08/87 by tjw                                            
      real*8 function sasum(n,sx)                                               
c                                                                               
c***after lawson c. (jpl),hanson r. (sla),                                      
c                            kincaid d. (u texas), krogh f. (jpl)               
c***purpose                                                                     
c    sum of magnitudes of real*8 vector components                              
c***description                                                                 
c    description of parameters                                                  
c                                                                               
c     --input--                                                                 
c        n  number of elements in input vector(s)                               
c       sx  real*8 vector with n elements                                       
c                                                                               
c     --output--                                                                
c    sasum  real*8 result (zero if n.le.0)                                      
c                                                                               
c     returns sum of magnitudes of real*8 sx.                                   
c     sasum = sum from 1 to n of  abs(sx(i))                                    
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension sx(*)                                                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sasum = 0.                                                                
      if (n.le.0) go to 999                                                     
c                                                                               
c        clean-up loop so remaining vector length is a multiple of 6.           
c                                                                               
      nm = mod(n,6)                                                             
      if ( nm .gt. 0 ) then                                                     
        do 30 i = 1,nm                                                          
        sasum = sasum + abs(sx(i))                                              
   30   continue                                                                
        if ( n .lt. 6 ) go to 999                                               
      endif                                                                     
c                                                                               
      nmp1 = nm + 1                                                             
      do 50 i = nmp1,n,6                                                        
       sasum = sasum + abs(sx(i)) + abs(sx(i + 1)) + abs(sx(i + 2))             
     $ + abs(sx(i + 3)) + abs(sx(i + 4)) + abs(sx(i + 5))                       
   50 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c saxpy    last revised 07/08/87 by tjw                                         
c*saxpy f77 rewrite, 07/08/87 by tjw                                            
      subroutine saxpy(n,sa,sx,sy)                                              
c                                                                               
c***after lawson c. (jpl),hanson r. (sla),                                      
c                            kincaid d. (u texas), krogh f. (jpl)               
c***purpose                                                                     
c  computation y = a*x + y, real*8 case                                         
c***description                                                                 
c    description of parameters                                                  
c                                                                               
c     --input--                                                                 
c        n  number of elements in input vector(s)                               
c       sa  real*8 scalar multiplier                                            
c       sx  real*8 vector with n elements                                       
c       sy  real*8 vector with n elements                                       
c                                                                               
c     --output--                                                                
c       sy  real*8 result (unchanged if n.le.0)                                 
c                                                                               
c     overwrite real*8 sy with real*8 sa*sx +sy.                                
c      for i=1 to n, replace sy(i) with s(y) + sa*sx(i)                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension sx(*),sy(*)                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (n .le. 0 .or. sa .eq. 0.) go to 999                                   
c                                                                               
c        clean-up loop so remaining vector length is a multiple of 4.           
c                                                                               
      nm = mod(n,4)                                                             
      if( nm .gt. 0 ) then                                                      
        do 30 i = 1,nm                                                          
        sy(i) = sy(i) + sa*sx(i)                                                
   30   continue                                                                
        if ( n .lt. 4 ) go to 999                                               
      endif                                                                     
c                                                                               
      nmp1 = nm + 1                                                             
      do 50 i = nmp1,n,4                                                        
      sy(i) = sy(i) + sa*sx(i)                                                  
      sy(i + 1) = sy(i + 1) + sa*sx(i + 1)                                      
      sy(i + 2) = sy(i + 2) + sa*sx(i + 2)                                      
      sy(i + 3) = sy(i + 3) + sa*sx(i + 3)                                      
   50 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c scal     last revised 07/08/87 by tjw                                         
c*scal f77 rewrite, 07/08/87 by tjw                                             
      subroutine scal (x, n, xs,xmax,ier)                                       
c                                                                               
c     scale a set of real*8 values to (-1,1)                                    
c                                                                               
c     input                                                                     
c      array x of n real*8 values                                               
c                                                                               
c     output                                                                    
c      array xs of n real*8 values,                                             
c      xmax  the magnitude of largest x                                         
c                                                                               
c      where xs(i) = x(i) / xmax   1=1,...,n                                    
c                                                                               
c     notes   ier = 0/1  ok / all x's = 0                                       
c             if xmax .lt. 1., set xmax = 1. and                                
c             xs(i) = x(i)  i=1,...,n                                           
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension x(*),xs(*)                                                      
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      ier = 0                                                                   
c                                                                               
c     find xmax                                                                 
c                                                                               
      xmax = abs(x(1))                                                          
      if (n .gt. 1) go to 10                                                    
c                                                                               
c     only one value in x                                                       
c                                                                               
      if (xmax .le. 0.) then                                                    
        ier = 1                                                                 
        go to 999                                                               
      endif                                                                     
c                                                                               
      if (xmax .le. 1.) then                                                    
        xmax = 1.                                                               
        xs(1) = x(1)                                                            
        go to 999                                                               
      endif                                                                     
c                                                                               
      xs(1) = x(1) / xmax                                                       
      go to 999                                                                 
c                                                                               
   10 continue                                                                  
c                                                                               
      do 20 k=2,n                                                               
      xa = abs(x(k))                                                            
      if (xmax .lt. xa) xmax = xa                                               
   20 continue                                                                  
c                                                                               
      if (xmax .le. 0.) then                                                    
        ier = 1                                                                 
        go to 999                                                               
      endif                                                                     
c                                                                               
      if (xmax .le. 1.) then                                                    
c                                                                               
c       xmax .le. 1. here                                                       
c                                                                               
        xmax = 1.                                                               
        do 30 k=1,n                                                             
        xs(k) = x(k)                                                            
   30   continue                                                                
        go to 999                                                               
      endif                                                                     
c                                                                               
c     normal case                                                               
c                                                                               
      do 40 k=1,n                                                               
      xs(k) = x(k) / xmax                                                       
   40 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c sdot     last revised 07/08/87 by tjw                                         
c*sdot f77 rewrite, 07/08/87 by tjw                                             
      real*8 function sdot(n,sx,sy)                                             
c                                                                               
c***after lawson c. (jpl),hanson r. (sla),                                      
c                            kincaid d. (u texas), krogh f. (jpl)               
c***purpose                                                                     
c   inner product of vectors, real*8 case                                       
c***description                                                                 
c    description of parameters                                                  
c                                                                               
c     --input--                                                                 
c        n  number of elements in input vector(s)                               
c       sx  real*8 vector with n elements                                       
c       sy  real*8 vector with n elements                                       
c                                                                               
c     --output--                                                                
c     sdot  real*8 dot product (zero if n.le.0)                                 
c                                                                               
c     returns the dot product of real*8 sx and sy.                              
c     sdot = sum for i = 1 to n of sx(i) * sy(i)                                
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension sx(*),sy(*)                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      sdot = 0.                                                                 
      if (n.le.0) go to 999                                                     
c                                                                               
c        clean-up loop so remaining vector length is a multiple of 5.           
c                                                                               
      nm = mod(n,5)                                                             
      if ( nm .gt. 0 ) then                                                     
        do 30 i = 1,nm                                                          
        sdot = sdot + sx(i)*sy(i)                                               
   30   continue                                                                
        if ( n .lt. 5 ) go to 999                                               
      endif                                                                     
c                                                                               
      nmp1 = nm + 1                                                             
      do 50 i = nmp1,n,5                                                        
      sdot = sdot + sx(i)*sy(i) + sx(i + 1)*sy(i + 1) +                         
     $ sx(i + 2)*sy(i + 2) + sx(i + 3)*sy(i + 3) + sx(i + 4)*sy(i + 4)          
   50 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c sgeco    last revised 07/08/87 by tjw                                         
c*sgeco f77 rewrite, 07/08/87 by tjw                                            
      subroutine sgeco(a,ilda,n,ipvt,rcond,z)                                   
c                                                                               
c***after moler c.b.                                                            
c***purpose                                                                     
c  factors a real*8 matrix by gaussian elimination and estimates                
c  the condition number of the matrix.                                          
c***description                                                                 
c     sgeco factors a real matrix by gaussian elimination                       
c     and estimates the condition of the matrix.                                
c                                                                               
c     if  rcond  is not needed, sgefa is slightly faster.                       
c     to solve  a*x = b , follow sgeco by sgesl.                                
c     to compute  inverse(a)*c , follow sgeco by sgesl.                         
c                                                                               
c     on entry                                                                  
c                                                                               
c        a       real*8(ilda, n)                                                
c                the matrix to be factored.                                     
c                                                                               
c        ilda     integer                                                       
c                the leading dimension of the array  a .                        
c                                                                               
c        n       integer                                                        
c                the order of the matrix  a .                                   
c                                                                               
c     on return                                                                 
c                                                                               
c        a       an upper triangular matrix and the multipliers                 
c                which were used to obtain it.                                  
c                the factorization can be written  a = l*u  where               
c                l  is a product of permutation and unit lower                  
c                triangular matrices and  u  is upper triangular.               
c                                                                               
c        ipvt    integer(n)                                                     
c                an integer vector of pivot indices.                            
c                                                                               
c        rcond   real*8                                                         
c                an estimate of the reciprocal condition of  a .                
c                for the system  a*x = b , relative perturbations               
c                in  a  and  b  of size  epsilon  may cause                     
c                relative perturbations in  x  of size  epsilon/rcond .         
c                if  rcond  is so small that the logical expression             
c                           1.0 + rcond .eq. 1.0                                
c                is true, then  a  may be singular to working                   
c                precision.  in particular,  rcond  is zero  if                 
c                exact singularity is detected or the estimate                  
c                underflows.                                                    
c                                                                               
c                notes from 11jul86 changes:                                    
c                  there are two tests for definite singularity to              
c                  within machine precision.  if anorm is zero, this            
c                  condition is true and there is not much reason to            
c                  call sgefa.  rcond should be set to zero.  if info           
c                  is returned from sgefa with a non-zero value, this           
c                  also indicates that the same condition is true,              
c                  and the same action should be taken.                         
c                                                                               
c        z       real*8(n)                                                      
c                a work vector whose contents are usually unimportant.          
c                if  a  is close to a singular matrix, then  z  is              
c                an approximate null vector in the sense that                   
c                norm(a*z) = rcond*norm(a)*norm(z).                             
c                                                                               
c     note- eqlib or flpars must be called before this routine                  
c     in order to provide a value for smpos (28jul86)                           
c                                                                               
c***references                                                                  
c  dongarra j.j., bunch j.r., moler c.b., stewart g.w.,                         
c   *linpack users  guide*, siam, 1979.                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
c                                                                               
      dimension ipvt(*),a(ilda,*),z(*)                                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     set rcond to zero                                                         
c                                                                               
      rcond = 0.                                                                
c                                                                               
c     compute 1-norm of a                                                       
c                                                                               
      anorm = 0.                                                                
      do 10 j = 1, n                                                            
      anorm = max(anorm,sasum(n,a(1,j)))                                        
   10 continue                                                                  
c                                                                               
c     test anorm                                                                
c                                                                               
      if (anorm.le.smpos) go to 999                                             
c                                                                               
c     factor the matrix                                                         
c                                                                               
      call sgefa(a,ilda,n,ipvt,info)                                            
c                                                                               
c     test info                                                                 
c                                                                               
      if (info.ne.0) go to 999                                                  
c                                                                               
c     rcond = 1/(norm(a)*(estimate of norm(inverse(a)))) .                      
c     estimate = norm(z)/norm(y) where  a*z = y  and  trans(a)*y = e .          
c     trans(a)  is the transpose of a .  the components of  e  are              
c     chosen to cause maximum local growth in the elements of w  where          
c     trans(u)*w = e .  the vectors are frequently rescaled to avoid            
c     overflow.                                                                 
c                                                                               
c     solve trans(u)*w = e                                                      
c                                                                               
      ek = 1.                                                                   
      do 20 j = 1, n                                                            
      z(j) = 0.                                                                 
   20 continue                                                                  
c                                                                               
      do 100 k = 1, n                                                           
      if (z(k) .ne. 0.) ek = sign(ek,-z(k))                                     
      if (abs(ek-z(k)) .gt. abs(a(k,k))) then                                   
        s = abs(a(k,k))/abs(ek-z(k))                                            
        call sscal(n,s,z)                                                       
        ek = s*ek                                                               
      endif                                                                     
c                                                                               
      wk = ek - z(k)                                                            
      wkm = -ek - z(k)                                                          
      s = abs(wk)                                                               
      sm = abs(wkm)                                                             
      if (a(k,k) .ne. 0.) then                                                  
        wk = wk/a(k,k)                                                          
        wkm = wkm/a(k,k)                                                        
      else                                                                      
        wk = 1.                                                                 
        wkm = 1.                                                                
      endif                                                                     
c                                                                               
      kp1 = k + 1                                                               
      if (kp1 .le. n) then                                                      
        do 60 j = kp1, n                                                        
        sm = sm + abs(z(j)+wkm*a(k,j))                                          
        z(j) = z(j) + wk*a(k,j)                                                 
        s = s + abs(z(j))                                                       
   60   continue                                                                
        if (s .lt. sm) then                                                     
          t = wkm - wk                                                          
          wk = wkm                                                              
          do 70 j = kp1, n                                                      
          z(j) = z(j) + t*a(k,j)                                                
   70     continue                                                              
        endif                                                                   
      endif                                                                     
c                                                                               
      z(k) = wk                                                                 
  100 continue                                                                  
c                                                                               
      s = 1./sasum(n,z)                                                         
      call sscal(n,s,z)                                                         
c                                                                               
c     solve trans(l)*y = w                                                      
c                                                                               
      do 120 kb = 1, n                                                          
      k = n + 1 - kb                                                            
      if (k .lt. n) z(k) = z(k) + sdot(n-k,a(k+1,k),z(k+1))                     
      if (abs(z(k)) .gt. 1.) then                                               
        s = 1./abs(z(k))                                                        
        call sscal(n,s,z)                                                       
      endif                                                                     
      il = ipvt(k)                                                              
      t = z(il)                                                                 
      z(il) = z(k)                                                              
      z(k) = t                                                                  
  120 continue                                                                  
c                                                                               
      s = 1./sasum(n,z)                                                         
      call sscal(n,s,z)                                                         
c                                                                               
      ynorm = 1.                                                                
c                                                                               
c     solve l*v = y                                                             
c                                                                               
      do 140 k = 1, n                                                           
      il = ipvt(k)                                                              
      t = z(il)                                                                 
      z(il) = z(k)                                                              
      z(k) = t                                                                  
      if (k .lt. n) call saxpy(n-k,t,a(k+1,k),z(k+1))                           
      if (abs(z(k)) .gt. 1.) then                                               
        s = 1./abs(z(k))                                                        
        call sscal(n,s,z)                                                       
        ynorm = s*ynorm                                                         
      endif                                                                     
  140 continue                                                                  
c                                                                               
      s = 1./sasum(n,z)                                                         
      call sscal(n,s,z)                                                         
      ynorm = s*ynorm                                                           
c                                                                               
c     solve  u*z = v                                                            
c                                                                               
      do 160 kb = 1, n                                                          
      k = n + 1 - kb                                                            
      if (abs(z(k)) .gt. abs(a(k,k))) then                                      
        s = abs(a(k,k))/abs(z(k))                                               
        call sscal(n,s,z)                                                       
        ynorm = s*ynorm                                                         
      endif                                                                     
      if (a(k,k) .ne. 0.) z(k) = z(k)/a(k,k)                                    
      if (a(k,k) .eq. 0.) z(k) = 1.                                             
      t = -z(k)                                                                 
      call saxpy(k-1,t,a(1,k),z(1))                                             
  160 continue                                                                  
c                                                                               
c     make znorm = 1.0                                                          
c                                                                               
      s = 1./sasum(n,z)                                                         
      call sscal(n,s,z)                                                         
      ynorm = s*ynorm                                                           
c                                                                               
      rcond = ynorm/anorm                                                       
  999 continue                                                                  
c                                                                               
      end                                                                       
c sgefa    last revised 07/08/87 by tjw                                         
c*sgefa f77 check, 07/08/87 by tjw                                              
      subroutine sgefa(a,ilda,n,ipvt,info)                                      
c                                                                               
c***after moler c.b.                                                            
c***purpose                                                                     
c  factors a real*8 matrix by gaussian elimination.                             
c***description                                                                 
c     sgefa factors a real matrix by gaussian elimination.                      
c                                                                               
c     sgefa is usually called by sgeco, but it can be called                    
c     directly with a saving in time if  rcond  is not needed.                  
c     (time for sgeco) = (1 + 9/n)*(time for sgefa) .                           
c                                                                               
c     on entry                                                                  
c                                                                               
c        a       real*8(ilda, n)                                                
c                the matrix to be factored.                                     
c                                                                               
c        ilda     integer                                                       
c                the leading dimension of the array  a.                         
c                                                                               
c        n       integer                                                        
c                the order of the matrix  a.                                    
c                                                                               
c     on return                                                                 
c                                                                               
c        a       an upper triangular matrix and the multipliers                 
c                which were used to obtain it.                                  
c                the factorization can be written  a = l*u  where               
c                l  is a product of permutation and unit lower                  
c                triangular matrices and  u  is upper triangular.               
c                                                                               
c        ipvt    integer(n)                                                     
c                an integer vector of pivot indices.                            
c                                                                               
c        info    integer                                                        
c                = -1  singularity indicated by a would-be zero divide          
c                      (added 11jul86 by tjw).                                  
c                =  0  normal value.                                            
c                =  k  if  u(k,k) .eq. 0.0 .  this is not an error              
c                      condition for this routine, but it does                  
c                      indicate that sgesl or sgedi will divide by zero         
c                      if called.  use  rcond  in sgeco for a reliable          
c                      indication of singularity.                               
c                                                                               
c                                                                               
c     note- eqlib or flpars must be called before this routine                  
c     in order to provide a value for smpos (28jul86)                           
c                                                                               
c***references                                                                  
c  dongarra j.j., bunch j.r., moler c.b., stewart g.w.,                         
c   *linpack users  guide*, siam, 1979.                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
c                                                                               
      dimension ipvt(*),a(ilda,*)                                               
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     gaussian elimination with partial pivoting                                
c                                                                               
      info = 0                                                                  
      nm1 = n - 1                                                               
      if (nm1 .ge. 1) then                                                      
        do 60 k = 1, nm1                                                        
        kp1 = k + 1                                                             
c                                                                               
c       find il = pivot index                                                   
c                                                                               
        il = isamax(n-k+1,a(k,k)) + k - 1                                       
        ipvt(k) = il                                                            
c                                                                               
c       zero pivot implies this column already triangularized                   
c                                                                               
        if (a(il,k) .ne. 0.) then                                               
c                                                                               
c         interchange if necessary                                              
c                                                                               
          if (il .ne. k) then                                                   
            t = a(il,k)                                                         
            a(il,k) = a(k,k)                                                    
            a(k,k) = t                                                          
          endif                                                                 
c                                                                               
c         compute multipliers                                                   
c                                                                               
          div=a(k,k)                                                            
c                                                                               
c           test for potential zero divide.                                     
c                                                                               
          adiv=abs(div)                                                         
          if (adiv.le.smpos) then                                               
            info = -1                                                           
            go to 80                                                            
          endif                                                                 
          t = -1./div                                                           
          call sscal(n-k,t,a(k+1,k))                                            
c                                                                               
c         row elimination with column indexing                                  
c                                                                               
          do 30 j = kp1, n                                                      
          t = a(il,j)                                                           
          if (il .ne. k) then                                                   
            a(il,j) = a(k,j)                                                    
            a(k,j) = t                                                          
          endif                                                                 
          call saxpy(n-k,t,a(k+1,k),a(k+1,j))                                   
   30     continue                                                              
          go to 50                                                              
        endif                                                                   
        info = k                                                                
   50   continue                                                                
   60   continue                                                                
      endif                                                                     
      ipvt(n) = n                                                               
      if (a(n,n) .eq. 0.) info = n                                              
   80 continue                                                                  
c                                                                               
      end                                                                       
c sgesl    last revised 07/08/87 by tjw                                         
c*sgesl f77 check, 07/08/87 by tjw                                              
      subroutine sgesl(a,ilda,n,ipvt,b)                                         
c                                                                               
c***after  moler c.b. (unm)                                                     
c***purpose                                                                     
c  solves the real*8 system a*x=b                                               
c  using the factors of sgeco or sgefa                                          
c***description                                                                 
c     sgesl solves the real*8 system                                            
c     a * x = b                                                                 
c     using the factors computed by sgeco or sgefa.                             
c                                                                               
c     on entry                                                                  
c                                                                               
c        a       real*8(ilda, n)                                                
c                the output from sgeco or sgefa.                                
c                                                                               
c        ilda     integer                                                       
c                the leading dimension of the array  a .                        
c                                                                               
c        n       integer                                                        
c                the order of the matrix  a .                                   
c                                                                               
c        ipvt    integer(n)                                                     
c                the pivot vector from sgeco or sgefa.                          
c                                                                               
c        b       real*8(n)                                                      
c                the right hand side vector.                                    
c                                                                               
c                                                                               
c     on return                                                                 
c                                                                               
c        b       the solution vector  x .                                       
c                                                                               
c     error condition                                                           
c                                                                               
c        a division by zero will occur if the input factor contains a           
c        zero on the diagonal.  technically this indicates singularity          
c        but it is often caused by improper arguments or improper               
c        setting of ilda .  it will not occur if the routines are               
c        called correctly and if sgeco has set rcond .gt. 0.0                   
c        or sgefa has set info .eq. 0.                                          
c                                                                               
c     to compute  inverse(a) * c  where  c  is a matrix                         
c     with  p  columns                                                          
c           call sgeco(a,ilda,n,ipvt,rcond,z)                                   
c           if (rcond is too small) go to ...                                   
c           do 10 j = 1, p                                                      
c              call sgesl(a,ilda,n,ipvt,c(1,j),0)                               
c        10 continue                                                            
c                                                                               
c***references                                                                  
c  dongarra j.j., bunch j.r., moler c.b., stewart g.w.,                         
c   *linpack users  guide*, siam, 1979.                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension ipvt(*),a(ilda,*),b(*)                                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      nm1 = n - 1                                                               
c                                                                               
c     first solve  l*y = b                                                      
c                                                                               
      if (nm1 .ge. 1) then                                                      
        do 20 k = 1, nm1                                                        
        il = ipvt(k)                                                            
        t = b(il)                                                               
        if (il .ne. k) then                                                     
          b(il) = b(k)                                                          
          b(k) = t                                                              
        endif                                                                   
        call saxpy(n-k,t,a(k+1,k),b(k+1))                                       
   20   continue                                                                
      endif                                                                     
c                                                                               
c     now solve  u*x = y                                                        
c                                                                               
      do 40 kb = 1, n                                                           
      k = n + 1 - kb                                                            
      b(k) = b(k)/a(k,k)                                                        
      t = -b(k)                                                                 
      call saxpy(k-1,t,a(1,k),b(1))                                             
   40 continue                                                                  
c                                                                               
      end                                                                       
c shelv    last revised 07/08/87 by tjw                                         
c*shelv f77 rewrite, 07/08/87 by tjw                                            
      subroutine shelv(a,nel,nchars,kcol,kchars)                                
c                                                                               
c     shell sort                                                                
c                                                                               
c     shell sort of nel elements of length nchars each in array a.              
c     keys start in column kcol, of length kchars.                              
c     element length nchars must be .le. 80                                     
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "impliciti.h"                                              
c                                                                               
      character*(*) a(*)                                                        
      character*80 wa,key,wk                                                    
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      inc = 1                                                                   
      klstc = kcol+kchars-1                                                     
c                                                                               
c     generate first increment                                                  
c                                                                               
   10 inc = 3*inc+1                                                             
      if(inc .ge. nel) go to 15                                                 
      go to 10                                                                  
c                                                                               
   15 continue                                                                  
c                                                                               
c     back up one term of sequence                                              
c                                                                               
      inc = (inc-1)/3                                                           
c                                                                               
c     begin sort                                                                
c                                                                               
   20 inc = (inc-1)/3                                                           
c                                                                               
c     clamp                                                                     
c                                                                               
      if (inc .lt. 1) inc=1                                                     
      im = inc                                                                  
c                                                                               
c     set starting index                                                        
c                                                                               
      ja = im+1                                                                 
   30 ia = ja-im                                                                
c                                                                               
c     get working key and line image                                            
c                                                                               
      key = a(ja)(kcol:klstc)                                                   
      wa = a(ja)(1:nchars)                                                      
c                                                                               
c     get a key and test                                                        
c                                                                               
   40 wk = a(ia)(kcol:klstc)                                                    
      if (key .lt. wk) then                                                     
c                                                                               
c       shift a's                                                               
c                                                                               
        a(ia+im)(1:nchars) = a(ia)(1:nchars)                                    
        ia = ia-im                                                              
        if (ia .gt. 0) go to 40                                                 
      endif                                                                     
c                                                                               
c     insert wa                                                                 
c                                                                               
      a(ia+im)(1:nchars) = wa                                                   
      ja = ja+1                                                                 
      if (ja .le. nel) go to 30                                                 
      if (inc .gt. 1) go to 20                                                  
c                                                                               
      end                                                                       
c sortr8   last revised 07/08/87 by tjw                                         
c*sortr8 f77 rewrite, 07/08/87 by tjw                                           
      subroutine sortr8(a,nel)                                                  
c                                                                               
c     shell sort of floating point array                                        
c     24jun86  att  mod for accepting absolute value of a(i)                    
c     28mar86  mlc  adapted from shelv                                          
c                                                                               
c     shell sort of real*8 array a.                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension a(*)                                                            
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      inc = 1                                                                   
c                                                                               
c     generate first increment                                                  
c                                                                               
   10 inc = 3*inc+1                                                             
      if (inc .ge. nel) go to 15                                                
      go to 10                                                                  
c                                                                               
   15 continue                                                                  
c                                                                               
c     back up one term of sequence                                              
c                                                                               
      inc = (inc-1)/3                                                           
c                                                                               
c      begin sort                                                               
c                                                                               
   20 inc = (inc-1)/3                                                           
c                                                                               
c     clamp                                                                     
c                                                                               
      if (inc .lt. 1) inc=1                                                     
      im = inc                                                                  
c                                                                               
c     set starting index                                                        
c                                                                               
      ja = im+1                                                                 
   30 ia = ja-im                                                                
c                                                                               
c      get working key element                                                  
c                                                                               
      akey = abs(a(ja))                                                         
      wa = a(ja)                                                                
c                                                                               
c      get a key and test                                                       
c                                                                               
   40 wk = abs(a(ia))                                                           
      if (akey .lt. wk) then                                                    
c                                                                               
c       shift a's                                                               
c                                                                               
        a(ia+im) = a(ia)                                                        
        ia = ia-im                                                              
        if(ia .gt. 0) go to 40                                                  
      endif                                                                     
c                                                                               
c     insert wa                                                                 
c                                                                               
      a(ia+im) = wa                                                             
      ja = ja+1                                                                 
      if (ja .le. nel) go to 30                                                 
c                                                                               
      if (inc .gt. 1) go to 20                                                  
c                                                                               
      end                                                                       
c srchn    last revised 07/08/87 by tjw                                         
c*srchn f77 rewrite, 07/08/87 by tjw                                            
      subroutine srchn(ua,n,un,ix)                                              
c                                                                               
c     search array ua for occurrence of un                                      
c     temporary- compare only the first 12 characters                           
c       until the data file reads are changed to a24 format                     
c     return index ix of un in ua                                               
c     ix = -1 if not found                                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*24 ua(*),un                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      do 10 k=1,n                                                               
      if (ua(k)(1:12) .eq. un(1:12)) then                                       
c                                                                               
c       found it                                                                
c                                                                               
        ix = k                                                                  
        go to 999                                                               
c                                                                               
      endif                                                                     
   10 continue                                                                  
c                                                                               
c     not found                                                                 
c                                                                               
      ix = -1                                                                   
c                                                                               
  999 continue                                                                  
      end                                                                       
c srtsum   last revised 07/08/87 by tjw                                         
c*srtsum f77 rewrite, 07/08/87 by tjw                                           
      subroutine srtsum(d,n,sumd)                                               
c                                                                               
c     sort array and sum                                                        
c                                                                               
c     input   d      array of real*8 positive terms                             
c             n      number of terms in d                                       
c     output  sumd   sum of terms of d                                          
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension d(*)                                                            
c                                                                               
c----------------------------------------------------------------               
c                                                                               
      if (n .gt. 1) then                                                        
        call sortr8(d,n)                                                        
c                                                                               
        sumd = 0.                                                               
        do 10 k=1,n                                                             
        sumd = sumd + d(k)                                                      
   10   continue                                                                
c                                                                               
      elseif (n .eq. 1) then                                                    
        sumd = d(1)                                                             
      else                                                                      
        sumd = 0.                                                               
      endif                                                                     
c                                                                               
      end                                                                       
c sscal    last revised 07/08/87 by tjw                                         
c*sscal f77 rewrite, 07/08/87 by tjw                                            
      subroutine sscal(n,sa,sx)                                                 
c                                                                               
c***after lawson c. (jpl),hanson r. (sla),                                      
c                            kincaid d. (u texas), krogh f. (jpl)               
c***purpose                                                                     
c   vector scale x = a*x, real*8 case                                           
c***description                                                                 
c    description of parameters                                                  
c                                                                               
c     --input--                                                                 
c        n  number of elements in input vector(s)                               
c       sa  real*8 scale factor                                                 
c       sx  real*8 vector with n elements                                       
c                                                                               
c     --output--                                                                
c       sx  real*8 result (unchanged if n.le.0)                                 
c                                                                               
c     replace real*8 sx by real*8 sa*sx.                                        
c     for i=1 to n, replace sx(i) with sa * sx(i)                               
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension sx(*)                                                           
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (n.le.0) go to 999                                                     
c                                                                               
c     clean-up loop so remaining vector length is a multiple of 5.              
c                                                                               
      nm = mod(n,5)                                                             
      if ( nm .gt. 0 ) then                                                     
        do 30 i = 1,nm                                                          
        sx(i) = sa*sx(i)                                                        
   30   continue                                                                
        if ( n .lt. 5 ) go to 999                                               
      endif                                                                     
c                                                                               
      nmp1 = nm + 1                                                             
      do 50 i = nmp1,n,5                                                        
        sx(i) = sa*sx(i)                                                        
        sx(i + 1) = sa*sx(i + 1)                                                
        sx(i + 2) = sa*sx(i + 2)                                                
        sx(i + 3) = sa*sx(i + 3)                                                
        sx(i + 4) = sa*sx(i + 4)                                                
   50 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c stpp     last revised 07/08/87 by tjw                                         
c*stpp  f77 rewrite, 07/08/87 by tjw                                            
      subroutine stpp(p,dotp,ip,dpmax,dpmin,dotm,im,dmmax,dmmin,imax)           
c                                                                               
c     24jun86  mod for combining positive & neg column                          
c     13jun86  mod to real*8                                                    
c     25apr86  mod to do max, min work                                          
c     22apr86  mod to real*4 for tests                                          
c     21apr86  mlc                                                              
c                                                                               
c     store partial product                                                     
c                                                                               
c     input   p       real*8 term                                               
c             dotp    real*8 array of positive terms                            
c             ip      index into dotp (advanced 1 if p .gt. 0)                  
c             dpmax   set to max value in dotp                                  
c             dpmin   set to min value in dotp                                  
c             dotm    real*8 array of abs val of negative terms                 
c             im      index into dotm                                           
c             dmmax   set to max value in dotm                                  
c             dmmin   set to min value in dotm                                  
c             imax    maximum value for indices ip, im                          
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      dimension dotp(*),dotm(*)                                                 
c                                                                               
c------------------------------------------------------------------             
c                                                                               
      ip = ip+1                                                                 
      if (ip .gt. imax) then                                                    
        write (noutpl,1010) imax                                                
        write (nttyol,1010) imax                                                
 1010   format(' * error - buffer index overflow, index .gt. ',i5,              
     $  ' (eqlib/stpp)')                                                        
        stop                                                                    
      endif                                                                     
c                                                                               
      dotp(ip) = p                                                              
      if (abs(p) .gt. dpmax) dpmax = abs(p)                                     
      if (abs(p) .lt. dpmin) dpmin = abs(p)                                     
c                                                                               
      end                                                                       
c stripl   last revised 07/08/87 by tjw                                         
c*stripl f77 rewrite, 07/08/87 by tjw                                           
      subroutine stripl(nin,nout,ilen)                                          
c                                                                               
c     strip lines with * in col 1  write long lines                             
c                                                                               
c     25nov85  mlc  adapted from strips                                         
c                                                                               
c     read a file from unit number nin                                          
c     skip lines with * in column 1, write all other lines                      
c     to unit number nout                                                       
c     len is line length in characters                                          
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*80 uibuf                                                        
      character*5 ufmt                                                          
      character*1 uch,ustar,ublank                                              
c                                                                               
      data ufmt /'(a80)'/                                                       
      data ustar /'*'/                                                          
      data ublank /' '/                                                         
c                                                                               
c-----------------------------------------------------------------------        
c                                                                               
c     clamp line length and construct format                                    
c                                                                               
      illen = ilen                                                              
      if (illen .gt. 80) illen = 80                                             
      if (illen .lt. 0) illen = 1                                               
      write (ufmt(3:4),1005) illen                                              
 1005 format(i2)                                                                
c                                                                               
   10 read(nin,1010,end=999) uibuf                                              
 1010 format(a80)                                                               
c                                                                               
c     skip lines with * in col 1                                                
c                                                                               
      uch = uibuf(1:1)                                                          
      if (uch .eq. ustar) go to 10                                              
c                                                                               
      write (nout,ufmt) uibuf                                                   
      go to 10                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c supprs   last revised 12/08/87 by tjw                                         
c*supprs created in f77 12/08/87 by tjw                                         
      subroutine supprs(nxmod,jxmod,kxmod,uxmod,uspec,umin,ugas,                
     $ usolx,jsflag,jmflag,jgflag,jxflag,nst,nmt,ngt,nxt,noutpt,nttyo)          
c                                                                               
c     this routine suppresses species/reactions as directed by                  
c     what is on the input file.  uxmod is the name of the associated           
c     species and kxmod = -1.  routine alters handles the log k                 
c     alter function (kxmod = 0, 1, or 2).                                      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*(*) uxmod(*),uspec(*),umin(*),ugas(*),usolx(*)                  
      character*24 uaq,umn,ugs,uss,unamsp                                       
c                                                                               
      dimension jxmod(*),kxmod(*),jsflag(*),jmflag(*),jgflag(*),                
     $ jxflag(*)                                                                
c                                                                               
      data uaq /'aqueous species         '/                                     
      data umn /'minerals                '/                                     
      data ugs /'gases                   '/                                     
      data uss /'solid solutions         '/                                     
      data ufix /'fix     '/                                                    
c                                                                               
c-----------------------------------------------------------------              
c                                                                               
      if (nxmod .le. 0) go to 999                                               
c                                                                               
      do 1200 n = 1,nxmod                                                       
      if (kxmod(n) .ge. 0) go to 1200                                           
      unamsp = uxmod(n)                                                         
c                                                                               
c     aqueous species                                                           
c                                                                               
      if (jxmod(n) .eq. 0) then                                                 
        do 1005 ns=1,nst                                                        
        if (unamsp .eq. uspec(ns)) then                                         
          jsflag(ns)=2                                                          
          write (noutpt,1028) unamsp                                            
          write (nttyo,1028) unamsp                                             
 1028     format(' the species ',a24,' has been user-suppressed',/)             
          go to 1200                                                            
        endif                                                                   
 1005   continue                                                                
        write (noutpt,237) unamsp,uaq                                           
        write (nttyo,237) unamsp,uaq                                            
  237   format(' * note- suppress species ',a24,' was not among',               
     $  /7x,'the loaded ',a24,' (eqlib/supprs)',/)                              
        go to 1200                                                              
      endif                                                                     
c                                                                               
c     minerals                                                                  
c                                                                               
      if (jxmod(n) .eq. 1) then                                                 
        if (unamsp(1:8) .eq. ufix(1:8)) then                                    
          write (noutpt,1216) unamsp                                            
          write (nttyo,1216) unamsp                                             
 1216     format(' * note- ',a24,' is a fictive fugacity fixing',               
     $    /7x,'so it can not be suppressed (eqlib/supprs)',/)                   
          go to 1200                                                            
        endif                                                                   
        do 1035 nm = 1,nmt                                                      
        if (unamsp .eq. umin(nm)) then                                          
          jmflag(nm)=2                                                          
          write (noutpt,1028) unamsp                                            
          write (nttyo,1028) unamsp                                             
          go to 1200                                                            
        endif                                                                   
 1035   continue                                                                
        write (noutpt,237) unamsp,umn                                           
        write (nttyo,237) unamsp,umn                                            
        go to 1200                                                              
      endif                                                                     
c                                                                               
c     gases                                                                     
c                                                                               
      if (jxmod(n) .eq. 2) then                                                 
        do 1065 ng=1,ngt                                                        
        if (unamsp .eq. ugas(ng)) then                                          
          jgflag(ng)=2                                                          
          write (noutpt,1028) unamsp                                            
          write (nttyo,1028) unamsp                                             
          go to 1200                                                            
        endif                                                                   
 1065   continue                                                                
        write (noutpt,237) unamsp,ugs                                           
        write (nttyo,237) unamsp,ugs                                            
        go to 1200                                                              
      endif                                                                     
c                                                                               
c     solid solutions                                                           
c                                                                               
      if (jxmod(n) .eq. 3) then                                                 
        do 1095 nx = 1,nxt                                                      
        if (unamsp .eq. usolx(nx)) then                                         
          jxflag(nx)=4                                                          
          write (noutpt,1028) unamsp                                            
          write (nttyo,1028) unamsp                                             
          go to 1200                                                            
        endif                                                                   
 1095   continue                                                                
        write (noutpt,237) unamsp,uss                                           
        write (nttyo,237) unamsp,uss                                            
        go to 1200                                                              
      endif                                                                     
c                                                                               
 1200 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c swchlm   last revised 01/25/88 by rmm                                         
c*swchlm f77 check, ok 04/06/87 by tjw                                          
      subroutine swchlm(n1,n2,nst)                                              
c                                                                               
c     this routine is called by routine switch.                                 
c                                                                               
c     switch lambda and mu indices for consistency with basis-switching,        
c     then remake derived index arrays                                          
c                                                                               
c     input n1, n2   species indices to switch                                  
c      it is assumed n1 .ne. n2                                                 
c     in arrays nslmx and nmux,                                                 
c      if n1 occurs, replace it with n2                                         
c      if n2 occurs, replace it with n1                                         
c     and rebuild the derived index arrays in commons /eqlpmx/, /eqlpsl/        
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqlpmu.h"                                                 
      include "eqlpsa.h"                                                 
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     switch lambda indices                                                     
c                                                                               
      do 10 j=1,nslm                                                            
      call iswch(n1,n2,nslmx(1,j))                                              
      call iswch(n1,n2,nslmx(2,j))                                              
   10 continue                                                                  
c                                                                               
c     switch mu indices                                                         
c                                                                               
      do 20 j=1,nmu                                                             
      call iswch(n1,n2,nmux(1,j))                                               
      call iswch(n1,n2,nmux(2,j))                                               
      call iswch(n1,n2,nmux(3,j))                                               
   20 continue                                                                  
c                                                                               
c     build new sets of derived index arrays                                    
c                                                                               
      call bdslx(nst)                                                           
      call bdmlx(nst)                                                           
c                                                                               
      end                                                                       
c switch   last revised 12/11/87 by tjw                                         
c*f77 check, ok, 11/30/87 tjw                                                   
      subroutine switch(ns1,ns2,icode,ars,amn,ags,cess,cdrs,cdrm,               
     $ cdrg,cxistq,mwtss,z,zsq2,titr,azero,hydn,csp,uspec,jflag,                
     $ jsflag,ibasis,nsp,iopg1,iacion,iebal,nhydr,nchlor,nct,nsb,               
     $ nsq,nsq1,nst,nrst,nmt,ngt,nctmax,nsqmx1,narxmx,ntprmx,nerr,              
     $ noutpt,nttyo,qhydth,qpt4,qbassw)                                         
c                                                                               
c     this routine performs a basis-switch between to aqueous species.          
c     the local indices of the switched species are ns1 and ns2, where          
c     ns2 is greater in value than ns1.  if icode = 3, then this                
c     routine is being used by eq3nr.  this causes modification of              
c     the csp and nsp arrays.                                                   
c                                                                               
c     basis switching is subject to the following rules-                        
c                                                                               
c        1. a strict basis species can not be switched with another             
c           strict basis species                                                
c        2. an auxiliary basis species can not be switched with another         
c           auxiliary basis species.                                            
c        3. the second species, identified by the index ns2 =                   
c           ibasis(ns1), must have a reaction associated with it.               
c           hence it can not be in the strict basis.  its associated            
c           reaction must link it with the first species (ns1).                 
c           hence rules 1 and 2 above.                                          
c        4. the positions with regard to the strict and auxiliary bases         
c           noted above are those holding at the time of the call to            
c           this routine.                                                       
c        5. switching is not permitted if ars(1,1,nrs2) = 500. for the          
c           linking reaction (nrs2), unless the switch is a simple or           
c           discretionary switch between a strict basis species and             
c           an auxiliary basis species whose jflag is not 30.                   
c        6. the following species can not be switched out of the basis          
c           set-                                                                
c             h20, a species (na+ or cl-) is used to define the                 
c             equivalent stoichiometric ionic strength, or a species            
c             used to make electrical balancing adjustments.                    
c                                                                               
c     there are three possible switching cases-                                 
c                                                                               
c        1. a species in the auxiliary basis whose jflag value is               
c           not 30 (eliminated species) is switched into the strict             
c           basis.  this is a 'simple' switch.  the linking reaction            
c           must be re-written and the change in indexing must be               
c           reflected in the reaction coefficient arrays.  all                  
c           properties of the switched species are exchanged so as to           
c           follow the species to which they were originally assigned.          
c                                                                               
c        2. a species not in the basis, or an auxiliary basis species           
c           whose jflag value is 30, is switched into the strict                
c           basis.  the reaction coefficient arrays must be re-written          
c           so that reactions involving the original basis species are          
c           put in terms of the new one.  a total concentration value           
c           is multiplied by a stoichiometric factor to maintain                
c           consistency.                                                        
c                                                                               
c        3. a species not in the basis is switched into the auxiliary           
c           basis.  the reaction coefficient arrays must be treated as          
c           in 2 above.  the reaction linking the auxiliary basis species       
c           with its counterpart in the strict basis must be re-written         
c           with special instructions.  a total concentration value is          
c           multiplied by a stoichiometric factor to maintain consistency.      
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
c                                                                               
      character*(*) uspec(*)                                                    
      character*24 unamsp,udm                                                   
c                                                                               
      dimension ars(narxmx,ntprmx,*),amn(narxmx,ntprmx,*),                      
     $ ags(narxmx,ntprmx,*),cess(nctmax,*),cdrs(nsqmx1,*),                      
     $ cdrm(nsqmx1,*),cdrg(nsqmx1,*),cxistq(*),mwtss(*),z(*),zsq2(*),           
     $ titr(*),azero(*),hydn(*),csp(*),jflag(*),jsflag(*),ibasis(*),            
     $ nsp(*)                                                                   
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      call swtchk(cdrs,uspec,jflag,jsflag,ibasis,                               
     $ iacion,iebal,nsb,nsq,nsq1,nsqmx1,ns1,ns2,nerr,noutpt,nttyo)              
c                                                                               
      if (nerr .gt. 0) go to 999                                                
c                                                                               
c     get the index of the linking reacion                                      
c                                                                               
      nrs2 = ns2 - nsb                                                          
c                                                                               
      write (noutpt,1055) uspec(ns1),uspec(ns2)                                 
 1055 format(/6x,'switching ',a24,' from the basis for ',a24,/)                 
c                                                                               
c     print the existing linking reaction                                       
c                                                                               
      unamsp = uspec(ns2)                                                       
      call prreac(cdrs,uspec,unamsp,nrs2,nsq,nsq1,nsqmx1,noutpt)                
c                                                                               
      if (ns2.gt.nsq .or. jflag(ns2).eq.30) go to 700                           
c                                                                               
c---------------------------------------------------------------------          
c                                                                               
c     case 1.  this is a 'simple' switch.  a species in the auxiliary           
c     basis whose jflag value is not 30 is switched with a species in           
c     the strict basis.  all intrinsic and input properties are switched        
c     so as to remain assigned to the species with which they were              
c     originally associated.                                                    
c                                                                               
      if (cdrs(ns1,nrs2) .gt. 0.) then                                          
c                                                                               
c      check to see if the linking reaction is suppressed                       
c                                                                               
        do 402 j = 1,ntprmx                                                     
        if (ars(1,j,nrs2) .lt. 500.) then                                       
          do 400 i = 1,narxmx                                                   
          ars(i,j,nrs2) = -ars(i,j,nrs2)                                        
  400     continue                                                              
        endif                                                                   
  402   continue                                                                
c                                                                               
c       reverse the sign of the coefficients of the linking reaction            
c                                                                               
        do 305 nss = 1,nsq1                                                     
        cdrs(nss,nrs2) = -cdrs(nss,nrs2)                                        
  305   continue                                                                
      endif                                                                     
c                                                                               
c     exchange the positions of the two species in the linking reaction         
c                                                                               
      cx = cdrs(ns1,nrs2)                                                       
      cdrs(ns1,nrs2) = cdrs(nsq1,nrs2)                                          
      cdrs(nsq1,nrs2) = cx                                                      
c                                                                               
c     exchange places in the aqueous species name array                         
c                                                                               
      udm = uspec(ns1)                                                          
      uspec(ns1) = uspec(ns2)                                                   
      uspec(ns2) = udm                                                          
c                                                                               
c     print the new linking reaction                                            
c                                                                               
      unamsp = uspec(ns2)                                                       
      call prreac(cdrs,uspec,unamsp,nrs2,nsq,nsq1,nsqmx1,noutpt)                
c                                                                               
c     rewrite all other aqueous dissociation reactions                          
c                                                                               
      nn = nrs2 - 1                                                             
      do 310 nrs = 1,nn                                                         
      cx = cdrs(ns2,nrs)                                                        
      cdrs(ns2,nrs) = cdrs(ns1,nrs)                                             
      cdrs(ns1,nrs) = cx                                                        
  310 continue                                                                  
c                                                                               
      nn = nrs2 + 1                                                             
      do 312 nrs = nn,nrst                                                      
      cx = cdrs(ns2,nrs)                                                        
      cdrs(ns2,nrs) = cdrs(ns1,nrs)                                             
      cdrs(ns1,nrs) = cx                                                        
  312 continue                                                                  
c                                                                               
c     rewrite the mineral dissolution reactions                                 
c                                                                               
      do 410 nm = 1,nmt                                                         
      cx = cdrm(ns2,nm)                                                         
      cdrm(ns2,nm) = cdrm(ns1,nm)                                               
      cdrm(ns1,nm) = cx                                                         
  410 continue                                                                  
c                                                                               
c     rewrite the gas dissolution reactions                                     
c                                                                               
      do 420 ng = 1,ngt                                                         
      cx = cdrg(ns2,ng)                                                         
      cdrg(ns2,ng) = cdrg(ns1,ng)                                               
      cdrg(ns1,ng) = cx                                                         
  420 continue                                                                  
c                                                                               
c     exchange positions in the jflag array                                     
c                                                                               
      ii = jflag(ns1)                                                           
      jflag(ns1) = jflag(ns2)                                                   
      jflag(ns2) = ii                                                           
c                                                                               
      if (icode .eq. 3) then                                                    
c                                                                               
c       if called by eq3nr, exchange positions in the csp and nsp arrays        
c                                                                               
        cx = csp(ns1)                                                           
        csp(ns1) = csp(ns2)                                                     
        csp(ns2) = cx                                                           
c                                                                               
        ii = nsp(ns1)                                                           
        nsp(ns1) = nsp(ns2)                                                     
        nsp(ns2) = ii                                                           
      endif                                                                     
c                                                                               
      go to 330                                                                 
c                                                                               
c---------------------------------------------------------------------          
c                                                                               
c     cases 2 and 3.  switch a non-basis species into the expanded              
c     basis or switch an auxiliary basis species whose jflag value is           
c     30 into the strict basis.                                                 
c                                                                               
c        first re-write all reactions except that which links the two           
c        species being switched and, if ns1.gt.nsb, that                        
c        reaction which cross-links the ns1-th species with                     
c        a species in the strict basis.                                         
c                                                                               
  700 continue                                                                  
      do 720 nrs = 1,nrst                                                       
      if (nrs .eq. nrs2) go to 720                                              
      cx = cdrs(ns1,nrs)                                                        
      if (cx .ne. 0.) then                                                      
        stofac = -cx/cdrs(ns1,nrs2)                                             
        do 710 nss = 1,nsq                                                      
        xx = cdrs(nss,nrs) + stofac*cdrs(nss,nrs2)                              
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrs(nss,nrs) = xx                                                      
  710   continue                                                                
        xx = stofac*cdrs(nsq1,nrs2)                                             
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrs(ns1,nrs) = xx                                                      
        do 407 j = 1,ntprmx                                                     
        if (ars(1,j,nrs) .lt. 500.) then                                        
          do 405 i = 1,narxmx                                                   
          ars(i,j,nrs) = ars(i,j,nrs) + stofac*ars(i,j,nrs2)                    
  405     continue                                                              
        endif                                                                   
  407   continue                                                                
      endif                                                                     
      if (ns2 .le. nsq) then                                                    
        cx = cdrs(ns2,nrs)                                                      
        cdrs(ns1,nrs) = cdrs(ns1,nrs) + cx                                      
        cdrs(ns2,nrs) = 0.                                                      
      endif                                                                     
  720 continue                                                                  
c                                                                               
      do 724 nm = 1,nmt                                                         
      cx = cdrm(ns1,nm)                                                         
      if (cx .ne. 0.) then                                                      
        stofac = -cx/cdrs(ns1,nrs2)                                             
        do 722 nss = 1,nsq                                                      
        xx = cdrm(nss,nm) + stofac*cdrs(nss,nrs2)                               
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrm(nss,nm) = xx                                                       
  722   continue                                                                
        xx = stofac*cdrs(nsq1,nrs2)                                             
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrm(ns1,nm) = xx                                                       
        do 414 j = 1,ntprmx                                                     
        if (amn(1,j,nm) .lt. 500.) then                                         
          do 412 i = 1,narxmx                                                   
          amn(i,j,nm) = amn(i,j,nm) + stofac*ars(i,j,nrs2)                      
  412     continue                                                              
        endif                                                                   
  414   continue                                                                
      endif                                                                     
      if (ns2 .le. nsq) then                                                    
        cx = cdrm(ns2,nm)                                                       
        cdrm(ns1,nm) = cdrm(ns1,nm) + cx                                        
        cdrm(ns2,nm) = 0.                                                       
      endif                                                                     
  724 continue                                                                  
c                                                                               
      do 728 ng = 1,ngt                                                         
      cx = cdrg(ns1,ng)                                                         
      if (cx .ne. 0.) then                                                      
        stofac = -cx/cdrs(ns1,nrs2)                                             
        do 726 nss = 1,nsq                                                      
        xx = cdrg(nss,ng) + stofac*cdrs(nss,nrs2)                               
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrg(nss,ng) = xx                                                       
  726   continue                                                                
        xx = stofac*cdrs(nsq1,nrs2)                                             
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrg(ns1,ng) = xx                                                       
        do 417 j = 1,ntprmx                                                     
        if (ags(1,j,ng) .lt. 500.) then                                         
          do 415 i = 1,narxmx                                                   
          ags(i,j,ng) = ags(i,j,ng) + stofac*ars(i,j,nrs2)                      
  415     continue                                                              
        endif                                                                   
  417   continue                                                                
      endif                                                                     
      if (ns2 .le. nsq) then                                                    
        cx = cdrg(ns2,ng)                                                       
        cdrg(ns1,ng) = cdrg(ns1,ng) + cx                                        
        cdrg(ns2,ng) = 0.                                                       
      endif                                                                     
  728 continue                                                                  
c                                                                               
c     re-write the dissociation/destruction reaction for the                    
c     ns1-th species if it is in the auxiliary basis.                           
c                                                                               
      if (ns1 .gt. nsb) then                                                    
        nrs1 = ns1 - nsb                                                        
        stofac = -cdrs(nsq1,nrs1)/cdrs(ns1,nrs2)                                
        cdrs(ns1,nrs1) = cdrs(nsq1,nrs1)                                        
        do 730 nss = 1,nsq                                                      
        xx = cdrs(nss,nrs1) + stofac*cdrs(nss,nrs2)                             
        if (abs(xx) .le. eps100) xx = 0.                                        
        cdrs(nss,nrs1) = xx                                                     
  730   continue                                                                
        cdrs(ns1,nrs1) = 0.                                                     
        cdrs(nsq1,nrs1) = stofac*cdrs(nsq1,nrs2)                                
        do 424 j = 1,ntprmx                                                     
        if (ars(1,j,nrs) .lt. 500.) then                                        
          do 422 i = 1,narxmx                                                   
          ars(i,j,nrs1) = ars(i,j,nrs1) + stofac*ars(i,j,nrs2)                  
  422     continue                                                              
        endif                                                                   
  424   continue                                                                
      endif                                                                     
c                                                                               
c     re-write the reaction linking the switched species                        
c                                                                               
      do 750 nss = 1,nsq1                                                       
      cdrs(nss,nrs2) = -cdrs(nss,nrs2)                                          
  750 continue                                                                  
      cx = cdrs(ns1,nrs2)                                                       
      cdrs(ns1,nrs2) = cdrs(nsq1,nrs2)                                          
      cdrs(nsq1,nrs2) = cx                                                      
c                                                                               
      do 427 j = 1,ntprmx                                                       
      do 425 i = 1,narxmx                                                       
      ars(i,j,nrs2) = -ars(i,j,nrs2)                                            
  425 continue                                                                  
  427 continue                                                                  
c                                                                               
      udm = uspec(ns1)                                                          
      uspec(ns1) = uspec(ns2)                                                   
      uspec(ns2) = udm                                                          
c                                                                               
c     print the new linking reaction                                            
c                                                                               
      unamsp = uspec(ns2)                                                       
      call prreac(cdrs,uspec,unamsp,nrs2,nsq,nsq1,nsqmx1,noutpt)                
c                                                                               
      if (icode .eq. 3) then                                                    
c                                                                               
c       if called by eq3nr, recalculate the total concentration, if             
c       one is specified                                                        
c                                                                               
        if (jflag(ns1) .eq. 0)                                                  
     $  csp(ns1) = csp(ns1)*(-cdrs(ns1,nrs2)/cdrs(nsq1,nrs2))                   
      endif                                                                     
c                                                                               
c---------------------------------------------------------------------          
c                                                                               
c     exchange specific properties of the switched species.                     
c     (this is done for all cases.)                                             
c                                                                               
c       z = electrical charge                                                   
c       zsq2 = one half the charge squared                                      
c       titr = titration factor                                                 
c       cxistq = coefficient for calculation of xisteq                          
c       mwtss = molecular weight                                                
c       cess = elemental composition                                            
c       nhydr = index of hydrogen ion                                           
c       nchlor = index of chloride ion                                          
c       azero = ion size                                                        
c       hydn = hydration number                                                 
c                                                                               
  330 xx = z(ns1)                                                               
      z(ns1) = z(ns2)                                                           
      z(ns2) = xx                                                               
c                                                                               
      xx = zsq2(ns1)                                                            
      zsq2(ns1) = zsq2(ns2)                                                     
      zsq2(ns2) = xx                                                            
c                                                                               
      xx = titr(ns1)                                                            
      titr(ns1) = titr(ns2)                                                     
      titr(ns2) = xx                                                            
c                                                                               
      xx = cxistq(ns1)                                                          
      cxistq(ns1) = cxistq(ns2)                                                 
      cxistq(ns2) = xx                                                          
c                                                                               
      xx = mwtss(ns1)                                                           
      mwtss(ns1) = mwtss(ns2)                                                   
      mwtss(ns2) = xx                                                           
c                                                                               
      do 32 nc = 1,nct                                                          
      xx = cess(nc,ns1)                                                         
      cess(nc,ns1) = cess(nc,ns2)                                               
      cess(nc,ns2) = xx                                                         
   32 continue                                                                  
c                                                                               
      qq = nhydr .eq. ns2                                                       
      if (nhydr .eq. ns1) nhydr = ns2                                           
      if (qq) nhydr = ns1                                                       
c                                                                               
      qq = nchlor .eq. ns2                                                      
      if (nchlor .eq. ns1) nchlor = ns2                                         
      if (qq) nchlor = ns1                                                      
c                                                                               
c     switch ion sizes                                                          
c                                                                               
      xx = azero(ns1)                                                           
      azero(ns1) = azero(ns2)                                                   
      azero(ns2) = xx                                                           
c                                                                               
c     switch hydration numbers                                                  
c                                                                               
      if (qhydth) then                                                          
        xx = hydn(ns1)                                                          
        hydn(ns1) = hydn(ns2)                                                   
        hydn(ns2) = xx                                                          
      endif                                                                     
c                                                                               
c     if doing pitzer's equations, switch the lambda and mu indices and         
c     rebuild the derived index arrays                                          
c                                                                               
      if (iopg1.eq.1) call swchlm(ns1,ns2,nst)                                  
c                                                                               
      qbassw = .true.                                                           
c                                                                               
  999 continue                                                                  
      end                                                                       
c swtchk   last revised 12/11/87 by tjw                                         
c*swtchk, created in f77 11/30/87 tjw                                           
      subroutine swtchk(cdrs,uspec,jflag,jsflag,ibasis,                         
     $ iacion,iebal,nsb,nsq,nsq1,nsqmx1,ns1,ns2,nerr,noutpt,nttyo)              
c                                                                               
c     this routine checks a proposed basis switch for routine switch            
c     to see if doing the switch is ok or not.                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      character*(*) uspec(*)                                                    
      character*24 unamsp                                                       
c                                                                               
      dimension cdrs(nsqmx1,*),jflag(*),jsflag(*),ibasis(*)                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      nerr = 0                                                                  
      if (ns1 .eq. ns2) then                                                    
        write (noutpt,1000) uspec(ns1)                                          
        write (nttyo,1000) uspec(ns1)                                           
 1000   format(/' * basis switching error- '                                    
     $  /7x,a24,' can not be switched with itself (eqlib/swtchk)')              
        nerr = nerr + 1                                                         
      endif                                                                     
c                                                                               
c     ns2 must be greater than ns1.                                             
c                                                                               
      if (ns2 .lt. ns1) then                                                    
        write (noutpt,1005) uspec(ns1),uspec(ns2)                               
        write (nttyo,1005) uspec(ns1),uspec(ns2)                                
 1005   format(/' * basis switching error-',                                    
     $  /7x,a24,' can not be switched with ',a24,' (eqlib/swtchk)')             
        write (noutpt,1010) ns2,ns1,uspec(ns2),uspec(ns1)                       
        write (nttyo,1010) ns2,ns1,uspec(ns2),uspec(ns1)                        
 1010   format(7x,'ns2= ',i4,' is less than ns1= ',i4,                          
     $  /7x,'uspec(ns2)= ',a24,', uspec(ns1)= ',a24)                            
        nerr = nerr + 1                                                         
      endif                                                                     
c                                                                               
c     it is not permissible to switch a species more than once                  
c     (in leapfrog fashion).                                                    
c                                                                               
      do 100 nsg = 2,nsb                                                        
      if (ibasis(nsg).eq.ns2 .and. nsg.ne.ns1) then                             
        write (noutpt,1015) uspec(ns2),uspec(ns1),uspec(nsg)                    
        write (nttyo,1015) uspec(ns2),uspec(ns1),uspec(nsg)                     
 1015   format(/' * basis switching error-',                                    
     $  /7x,'attempt to switch ',a24,' with ',a24,                              
     $  /7x,'when it is also to be switched  with ',a24,                        
     $  ' (eqlib/swtchk)')                                                      
        nerr = nerr + 1                                                         
      endif                                                                     
  100 continue                                                                  
c                                                                               
c     h20 and the species (usually na+ or cl-) that defines the                 
c     equivalent stoichiometric ionic strength can not be switched,             
c     nor can a species that is constrained to satisfy electrical               
c     balance.                                                                  
c                                                                               
      if (ns1 .eq. 1) then                                                      
        write (noutpt,1020) uspec(ns1)                                          
        write (nttyo,1020) uspec(ns1)                                           
 1020  format(/' * basis switching error-',                                     
     $ /7x,a24,' can not be switched (eqlib/swtchk)')                           
        nerr = nerr + 1                                                         
      endif                                                                     
c                                                                               
      if (ns1 .eq. iacion) then                                                 
        write (noutpt,1020) uspec(ns1)                                          
        write (nttyo,1020) uspec(ns1)                                           
        write (noutpt,1025) uspec(ns1)                                          
        write (nttyo,1025) uspec(ns1)                                           
 1025  format(7x,'it defines the equiv. stoich. ionic strength')                
        nerr = nerr + 1                                                         
      endif                                                                     
c                                                                               
      if (ns1 .eq. iebal) then                                                  
        write (noutpt,1020) uspec(ns1)                                          
        write (nttyo,1020) uspec(ns1)                                           
        write (noutpt,1030) uspec(ns1)                                          
        write (nttyo,1030) uspec(ns1)                                           
 1030  format(7x,'it is adjusted for electrical balancing')                     
        nerr = nerr + 1                                                         
      endif                                                                     
c                                                                               
c     the second species can not be in the strict basis, because                
c     strict basis species do not have an associated reaction.                  
c                                                                               
      if (ns2 .le. nsb) then                                                    
        write (noutpt,1005) uspec(ns1),uspec(ns2)                               
        write (nttyo,1005) uspec(ns1),uspec(ns2)                                
        write (noutpt,1035) uspec(ns2)                                          
        write (nttyo,1035) uspec(ns2)                                           
 1035   format(7x,a24,' is in the strict basis')                                
        nerr = nerr + 1                                                         
      endif                                                                     
c                                                                               
      if (ns2.gt.nsq .or. jflag(ns2).eq.30) then                                
        if (jsflag(ns2) .gt. 0) then                                            
          write (noutpt,1005) uspec(ns1),uspec(ns2)                             
          write (nttyo,1005) uspec(ns1),uspec(ns2)                              
          write (noutpt,1040) uspec(ns2)                                        
          write (nttyo,1040) uspec(ns2)                                         
 1040     format(7x,a24,' is suppressed')                                       
          nerr = nerr + 1                                                       
        endif                                                                   
      endif                                                                     
c                                                                               
c     both species can not be in the auxiliary basis, because there             
c     then exists no linking reaction.                                          
c                                                                               
      qreac = .true.                                                            
      if (ns1.gt.nsb .and. ns2.le.nsq) then                                     
        write (noutpt,1005) uspec(ns1),uspec(ns2)                               
        write (nttyo,1005) uspec(ns1),uspec(ns2)                                
        write (noutpt,1045)                                                     
        write (nttyo,1045)                                                      
 1045  format(7x,'both species are in the auxiliary basis')                     
        nerr = nerr + 1                                                         
        qreac = .false.                                                         
      endif                                                                     
c                                                                               
c     get the index of the linking reacion                                      
c                                                                               
      if (qreac) then                                                           
        nrs2 = ns2 - nsb                                                        
c                                                                               
c       the switched species must be linked by the reaction formally            
c       associated with the second species.                                     
c                                                                               
        if (cdrs(ns1,nrs2) .eq. 0.) then                                        
          write (noutpt,1005) uspec(ns1),uspec(ns2)                             
          write (nttyo,1005) uspec(ns1),uspec(ns2)                              
          write (noutpt,1050) uspec(ns1)                                        
          write (nttyo,1050) uspec(ns1)                                         
 1050     format(7x,a24,' is not in the linking reaction')                      
          unamsp = uspec(ns2)                                                   
          call prreac(cdrs,uspec,unamsp,nrs2,nsq,nsq1,nsqmx1,noutpt)            
          nerr = nerr + 1                                                       
        endif                                                                   
      endif                                                                     
c                                                                               
      end                                                                       
c texp     last revised 11/18/87 by tjw                                         
c*texp   f77 check, ok 04/06/87 by tjw                                          
      real*8 function texp(x)                                                   
c                                                                               
c     function texp is the function 10**x                                       
c                                                                               
c        al10 is the natural logarithm of ten                                   
c        qtxppr is a logical switch that turns on a warning print               
c          when exponentiation is truncated.                                    
c                                                                               
c     xltxp is the lower truncation limit on the argument.  if x is             
c     not greater than xltxp, the result is set to zero.  xutxp is the          
c     upper trucatiion limit on the argument.  if x is not less than            
c     xutxp, the result is truncated to yutxp (yutxp = 10**xutxp).              
c     xutxp is used to avoid overflow when texp is called with a large          
c     argument.  this is critical.  xltxp, xutxp, and yutxp are                 
c     calculated by routine flpars.                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqltxp.h"                                                 
      include "eqlpp.h"                                                  
      include "eqlun.h"                                                  
c                                                                               
      if (x.le.xltxp) then                                                      
        texp=0.                                                                 
      elseif (x.gt.xutxp) then                                                  
        texp=yutxp                                                              
        if (qtxppr) write (noutpl,25) x,yutxp                                   
   25   format(' * info - exponential truncation, texp (',1pe12.5,              
     $  ') set to ',e12.5,' (eqlib/texp)')                                      
      else                                                                      
        y=al10*x                                                                
        texp=exp(y)                                                             
      endif                                                                     
      end                                                                       
c timdat   last revised 07/06/87 by tjw                                         
c*timdat f77 rewrite, 07/06/87 by tjw                                           
CDAS      subroutine timdat(udate,utime)                                            
c                                                                               
c     get current date, time in ascii (ridge/sun dependent)                     
c     see also gtime                                                            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
cdas      include "implicit.h"                                               
c                                                                               
cdas      character*24 utd                                                          
cdas      character*1 ublank                                                        
cdas      data ublank /' '/                                                         
c                                                                               
c---------------------------------------------------------------                
c                                                                               
c     fdate is a ridge/sun routine                                              
c                                                                               
CDAS      call fdate(utd)                                                           
c                                                                               
CDAS      udate(1:2) = utd(9:10)                                                    
CDAS      udate(3:5) = utd(5:7)                                                     
CDAS      udate(6:7) = utd(23:24)                                                   
CDAS      udate(8:8) = ublank                                                       
c                                                                               
CDAS      utime(1:5) = utd(12:16)                                                   
CDAS      utime(6:8) = ublank                                                       
c                                                                               
cDAS      end                                                                       
c tlg      last revised 07/08/87 by tjw                                         
c*tlg f77 check, ok 07/08/87 by tjw                                             
      real*8 function tlg(x)                                                    
c                                                                               
c     function tlg is the function log10, except that the log of zero           
c     is -999.                                                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqleps.h"                                                 
      include "eqlun.h"                                                  
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      if (x.lt.0.) then                                                         
        write (noutpl,20) x                                                     
        write (nttyol,20) x                                                     
   20   format(' * error - log(',e12.5,') can not be calculated',               
     $  ' (eqlib/tlg)')                                                         
        stop                                                                    
      elseif (x.le.smpos) then                                                  
        tlg=-999.                                                               
      else                                                                      
        tlg=log10(x)                                                            
      endif                                                                     
c                                                                               
      end                                                                       
c tmpcor   last revised 07/24/87 by rmm                                         
c*tmpcor f77 check, ok 07/08/87 by tjw                                          
      subroutine tmpcor(tempc)                                                  
c                                                                               
c     make temperature corrections to lambda and mu values                      
c                                                                               
c     using base values from commons /eqlpsb/ and /eqlpmb/,                     
c     compute operating values, and place them in                               
c     commons /eqlpsa/ and /eqlpmu/                                             
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
      include "eqlpar.h"                                                 
c                                                                               
      include "eqlpmu.h"                                                 
      include "eqlpmb.h"                                                 
      include "eqlpsa.h"                                                 
      include "eqlpsb.h"                                                 
c                                                                               
      data baset /25./                                                          
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
      delt = tempc - baset                                                      
      delt2=0.5*delt*delt                                                       
c                                                                               
      do 10 k=1,nslm                                                            
      pslm(1,k) = bslm(1,k) + dslm1(1,k)*delt + dslm2(1,k)*delt2                
      pslm(2,k) = bslm(2,k) + dslm1(2,k)*delt + dslm2(2,k)*delt2                
      pslm(3,k) = bslm(3,k) + dslm1(3,k)*delt + dslm2(3,k)*delt2                
   10 continue                                                                  
c                                                                               
      do 20 k=1,nmu                                                             
      pmu(k) = bmu(k) + dmu1(k)*delt + dmu2(k)*delt2                            
   20 continue                                                                  
c                                                                               
      end                                                                       
c undflw   last revised 07/08/87 by tjw                                         
c*undflw f77 check, ok 04/10/87 by tjw                                          
      subroutine undflw                                                         
c                                                                               
c     this routine provides a graceful way of handling underflows.              
c                                                                               
c     *** caution ****                                                          
c     This routine is specific to the RIDGE computers.                          
c     If the computer being used is not a RIDGE; then this routine              
c     must be modified.  This routine disables the trap used                    
c     in handling floating point underflows.                                    
c                                                                               
c     This routine does not seem to cause any harm on SUN computers.            
c                                                                               
c     Other computers may handle underflows automatically; making               
c     this routine unnecessary.                                                 
c                                                                               
c      integer signal                                                            
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     comment out the following line if not operating on a ridge                
c     or sun computer.                                                          
c                                                                               
C      i = signal(119,0,1)                                                      
c                                                                               
      end                                                                       
c writr    last revised 07/30/87 by rlh                                         
c*writr f77 check, ok 07/08/87 by tjw                                           
      subroutine writr(irfil,ubbuf,nlines,irfp)                                 
c                                                                               
c     14jan85  mlc                                                              
c                                                                               
c     direct access write of nlines from ubbuf to irfil                         
c                                                                               
c     input   irfil   unit number of direct access file                         
c             ubbuf   output buffer                                             
c             nlines  number of lines to write                                  
c             irfp    record number (one is first record)                       
c                      irfp is incremented one for each line written            
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
      include "eqlun.h"                                                  
c                                                                               
      character*80 ubbuf(*)                                                     
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     write (noutpl,1310) irfil,nlines,irfp                                     
c1310 format(' writr entered',/                                                 
c    $   '  irfil=',i3,'  nlines='i5,'  irfp=',i10)                             
c                                                                               
      do 10 k=1,nlines                                                          
      write (irfil,1010,rec=irfp,err=15) ubbuf(k)                               
 1010 format(a80)                                                               
      irfp = irfp+1                                                             
   10 continue                                                                  
      go to 999                                                                 
c                                                                               
   15 write (noutpl,1020) irfp                                                  
      write (nttyol,1020) irfp                                                  
 1020 format(' * error - trouble writing direct access block ',i8,              
     $ ' (eqlib/writr)')                                                        
      stop                                                                      
c                                                                               
  999 continue                                                                  
      end                                                                       
c wterm    last revised 11/25/87 by tjw                                         
c*wterm f77 check, ok 07/08/87 by tjw                                           
      subroutine wterm(apx,w,tempk,press,rconst,usolx,iopt4,jsol,nxt,           
     $ iktmax,iapxmx,noutpt,nttyo)                                              
c                                                                               
c   new version to combine eq3 and eq6 versions                                 
c   wlb 8/23/85                                                                 
c                                                                               
c     this routine computes the w(i,nx) array, which contains                   
c     the coefficients for the excess free energy function of                   
c     solid solutions.  if non-zero coefficients are                            
c     lacking, the solid solution reduces to an ideal solution                  
c     and the jsol value is changed to zero to reflect this.                    
c                                                                               
c   apx       array of non-ideal mixing parameters                              
c   iopt4     if iop4=0 no solid solutions are considered                       
c   jsol      array holding mixing model code                                   
c   usolx     name of solid solution                                            
c   w         array of non-ideal mixing parameters calculated from              
c             apx array                                                         
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      include "eqlun.h"                                                  
c                                                                               
      character*(*) usolx(*)                                                    
c                                                                               
      dimension apx(iapxmx,*),jsol(*),w(iktmax,*)                               
c                                                                               
      if (iopt4.le.0) go to 999                                                 
      do 30 nx=1,nxt                                                            
      sum=0.                                                                    
      inote=0                                                                   
      do 15 i=1,iapxmx                                                          
      sum=sum+apx(i,nx)*apx(i,nx)                                               
   15 continue                                                                  
c                                                                               
c     flag user if inconsistent ss model/data in data file                      
c                                                                               
      if (sum.eq.0 .and. jsol(nx).ne.0) inote=1                                 
      if (sum.ne.0 .and. jsol(nx).eq.0) inote=2                                 
      if (inote.eq.2) write (noutpt,1735)usolx(nx)                              
      if (inote.eq.1) write (noutpt,1736)usolx(nx)                              
1735  format(3x,'mineral ',a18,3x,'was assumed ideal - inconsistent',           
     *          ' with data in apx array - fix this')                           
1736  format(3x,'mineral ',a18,3x,'was assumed ideal because no data',          
     *          ' was found in apx array - fix this')                           
c                                                                               
c     reset jsol to 0 if no data present                                        
      if (sum.le.0) jsol(nx)=0                                                  
   20 continue                                                                  
c                                                                               
c     initialize w values to zero                                               
c                                                                               
      do 25 i=1,iktmax                                                          
      w(i,nx)=0.                                                                
   25 continue                                                                  
c                                                                               
   30 continue                                                                  
c                                                                               
      do 90 nx=1,nxt                                                            
      k=jsol(nx)                                                                
      if (k.eq.0) then                                                          
        go to 90                                                                
      elseif (k.eq.1) then                                                      
c                                                                               
c     binary solution, third-order maclaurin expansion                          
c     original pathi solid solution model.                                      
c                                                                               
        w(1,nx)=apx(1,nx)                                                       
        w(2,nx)=apx(2,nx)                                                       
        w(3,nx)=apx(3,nx)                                                       
        w(1,nx)=-w(2,nx)/2.-w(3,nx)/6.                                          
c                                                                               
      elseif (k.eq.2) then                                                      
c                                                                               
c     binary solution, parabolic maclaurin expansion                            
c                                                                               
        w(1,nx)=apx(1,nx)                                                       
c                                                                               
      elseif (k.eq.3) then                                                      
c                                                                               
c     binary solution, cubic maclaurin (p,t dependent)                          
c                                                                               
        w(1,nx)=apx(1,nx)+apx(2,nx)*tempk+apx(3,nx)*press                       
        w(2,nx)=apx(4,nx)+apx(5,nx)*tempk+apx(6,nx)*press                       
c                                                                               
      elseif (k.eq.4) then                                                      
c                                                                               
c     binary solution, guggenheim polynomial (t dependent)                      
c                                                                               
        w(1,nx)=apx(1,nx)+apx(2,nx)*tempk+apx(3,nx)*tempk*tempk                 
        w(2,nx)=apx(4,nx)+apx(5,nx)*tempk+apx(6,nx)*tempk*tempk                 
        w(3,nx)=apx(7,nx)+apx(8,nx)*tempk+apx(9,nx)*tempk*tempk                 
c                                                                               
      elseif (k.eq.5) then                                                      
c                                                                               
c     ternary regular solution (see prigogine and defay, p. 257)                
c     w(1,nx)=alpha12  w(2,nx)=alpha13  w(3,nx)=alpha23                         
c                                                                               
        w(1,nx)=apx(1,nx)                                                       
        w(2,nx)=apx(2,nx)                                                       
        w(3,nx)=apx(3,nx)                                                       
c                                                                               
      elseif (k.eq.6) then                                                      
c                                                                               
c   newton et al plagioclase model (gca vol 44 p. 933, 1980)                    
c   1 - albite; 2 - anorthite                                                   
        w(1,nx)=apx(1,nx)                                                       
        w(2,nx)=apx(2,nx)                                                       
      else                                                                      
        write (noutpt,100) jsol(nx),usolx(nx)                                   
100   format(3x,'jsol value ',i2,' for mineral ',a18,' is invalid')             
      end if                                                                    
c                                                                               
   90 continue                                                                  
c                                                                               
  999 continue                                                                  
      end                                                                       
c zsrt     last revised 10/27/87 by tjw                                         
c*zsrt f77 check, ok 07/08/87 by tjw                                            
      subroutine zsrt(z,nst,jsflag,izm,izoff)                                   
c                                                                               
c     input  z      charge array                                                
c            nst    number of species                                           
c            jsflag status array                                                
c                    if value .ge. 2, species is not present                    
c     output izm    max(izmax,-izmin)                                           
c            izoff  1-izmin  (the zero offset)                                  
c                                                                               
C  ************************************************                             
C                                                                               
C                                                                               
C             CHANGES TO ALLOW MACHINE                                          
C             GENERALITY WITH EQPTR71.F77                                       
C                                                                               
C                                                                               
c     include 'eqlibr136.inc'                                                   
C                                                                               
C                                                                               
C  ************************************************                             
      include "implicit.h"                                               
c                                                                               
      dimension z(*),jsflag(*)                                                  
c                                                                               
c----------------------------------------------------------------------         
c                                                                               
c     find min and max charge values and associated constants                   
c                                                                               
      zmax = -100.                                                              
      zmin = 100.                                                               
c                                                                               
      do 10 ns=1,nst                                                            
      if (jsflag(ns) .ge. 2) go to 10                                           
      zn = z(ns)                                                                
      if (zn .lt. zmin) zmin = zn                                               
      if (zn .gt. zmax) zmax = zn                                               
   10 continue                                                                  
c                                                                               
      izmin = nint(zmin)                                                        
      izmax = nint(zmax)                                                        
c                                                                               
c     zero offset                                                               
c                                                                               
      izoff = 1-izmin                                                           
c                                                                               
      izm = -izmin                                                              
      if (izmax .gt. izm) izm = izmax                                           
c                                                                               
      end                                                                       
C
C
CDAS      SUBROUTINE FDATE(UPASS)
CDAS      CHARACTER*24 UPASS
CDAS      CHARACTER*9 UDAT
CDAS      CHARACTER*8 UTIM
CDAS      CHARACTER*1 UZERO,UCOL
CDAS      DATA UZERO/'0'/,UCOL/':'/
CDAS      CALL DATE(UDAT)
CDAS      CALL TIME(UTIM)
CDAS      DO 10 I=1,24
CDAS   10 UPASS(I:I)=UZERO
CDAS      UPASS(9:10)=UDAT(1:2)
CDAS      UPASS(5:7)=UDAT(4:6)
CDAS      UPASS(23:24)=UDAT(8:9)
CDAS      UPASS(12:13)=UTIM(1:2)
CDAS      UPASS(15:16)=UTIM(4:5)
CDAS      UPASS(18:19)=UTIM(7:8)
CDAS      UPASS(14:14)=UCOL
CDAS      UPASS(17:17)=UCOL
CDAS      RETURN
CDAS      END