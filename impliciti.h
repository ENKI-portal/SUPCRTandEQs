c-----------------------------------------------------------------------        
c impliciti.h                                                                  
c   standard implicit statement for eq3/6 routines not requiring                
c   floating point variables                                                    
      implicit integer(a-p,r-t,v-z),logical(q),character*8(u)                   
