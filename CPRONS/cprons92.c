/* cprons92.f -- translated by f2c (version 20100827).
   You must link the resulting object file with libf2c:
	on Microsoft Windows system, link with libf2c.lib;
	on Linux or Unix systems, link with .../path/to/libf2c.a -lm
	or, if you install libf2c.a in a standard place, with -lf2c -lm
	-- in that order, at the end of the command line, as in
		cc *.o -lf2c -lm
	Source for libf2c is in /netlib/f2c/libf2c.zip, e.g.,

		http://www.netlib.org/f2c/libf2c.zip
*/

#include "f2c.h"

/* Common Block Declarations */

struct io_1_ {
    integer rterm, wterm, saf, daf;
};

#define io_1 (*(struct io_1_ *) &io_)

/* Initialized data */

struct {
    integer e_1[4];
    } io_ = { 5, 6, 43, 44 };


/* Table of constant values */

static integer c__1 = 1;
static integer c__90 = 90;
static integer c__2 = 2;
static integer c__9 = 9;
static integer c__0 = 0;
static integer c__3 = 3;

/* ** cprons92 - Read a sequential-access sprons.dat file and */
/* **            generate its direct-access dprons.dat equivalent. */
/* ** */
/* ******************************************************************** */
/* ** */
/* ** Author:     James W. Johnson */
/* **             Earth Sciences Department, L-219 */
/* **             Lawrence Livermore National Laboratory */
/* **             Livermore, CA 94550 */
/* **             johnson@s05.es.llnl.gov */
/* ** */
/* ** Abandoned:  8 November 1991 */
/* ** */
/* ******************************************************************** */
/* Main program */ int MAIN__(void)
{
    extern /* Subroutine */ int opener_(void), trnsfr_(void);

    opener_();
    trnsfr_();
    return 0;
} /* MAIN__ */

/* ******************************************************************** */
/* ** consts - Constants. */
/* Subroutine */ int consts_(void)
{
    return 0;
} /* consts_ */


/* ******************************************************************** */
/* ** opener - Open an existing sequential-access sprons.dat file (saf) */
/* **          and a new direct-access dprons.dat file (daf). */
/* Subroutine */ int opener_(void)
{
    /* Format strings */
    static char fmt_10[] = "(/,\002 specify name of INPUT sequential-acces"
	    "s\002,\002 THERMODYNAMIC DATABASE: \002,/)";
    static char fmt_20[] = "(a20)";
    static char fmt_30[] = "(/,\002 specify name of OUTPUT direct-acces"
	    "s\002,\002 THERMODYNAMIC DATABASE: \002,/)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void), s_rsfe(cilist *), do_fio(integer *
	    , char *, ftnlen), e_rsfe(void);

    /* Local variables */
    static char fname[20];
    extern logical openf_(integer *, integer *, char *, integer *, integer *, 
	    integer *, integer *, ftnlen);
    extern /* Subroutine */ int banner_(void);

    /* Fortran I/O blocks */
    static cilist io___1 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___2 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___4 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___5 = { 0, 0, 0, fmt_20, 0 };


    banner_();
L1:
    io___1.ciunit = io_1.wterm;
    s_wsfe(&io___1);
    e_wsfe();
    io___2.ciunit = io_1.rterm;
    s_rsfe(&io___2);
    do_fio(&c__1, fname, (ftnlen)20);
    e_rsfe();
    if (! openf_(&io_1.wterm, &io_1.saf, fname, &c__1, &c__1, &c__1, &c__90, (
	    ftnlen)20)) {
	goto L1;
    }
L2:
    io___4.ciunit = io_1.wterm;
    s_wsfe(&io___4);
    e_wsfe();
    io___5.ciunit = io_1.rterm;
    s_rsfe(&io___5);
    do_fio(&c__1, fname, (ftnlen)20);
    e_rsfe();
    if (! openf_(&io_1.wterm, &io_1.daf, fname, &c__2, &c__2, &c__1, &c__90, (
	    ftnlen)20)) {
	goto L2;
    }
    return 0;
} /* opener_ */

/* ****************************************************************** */
/* ** banner - Write program banner to the terminal screen. */
/* Subroutine */ int banner_(void)
{
    /* Format strings */
    static char fmt_10[] = "(/,5x,\002 Welcome to CPRONS92 Version 1.0\002,/"
	    ",5x,\002 Author:    James W. Johnson\002,/,5x,\002 Abandoned: 8 "
	    "November 1991\002,/)";

    /* Builtin functions */
    integer s_wsfe(cilist *), e_wsfe(void);

    /* Fortran I/O blocks */
    static cilist io___6 = { 0, 0, 0, fmt_10, 0 };


    io___6.ciunit = io_1.wterm;
    s_wsfe(&io___6);
    e_wsfe();
    return 0;
} /* banner_ */

/* ****************************************************************** */
/* ** trnsfr - Transfer data from saf to daf. */
/* Subroutine */ int trnsfr_(void)
{
    /* Builtin functions */
    integer s_wsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_wsle(void);

    /* Local variables */
    static integer ngas;
    extern /* Subroutine */ int tail_(integer *, integer *, integer *, 
	    integer *, integer *, integer *, integer *, integer *, integer *, 
	    integer *, integer *, integer *);
    static integer naqs, rec1a, rec1g, nmin1, nmin2, nmin3, nmin4, rec1m1, 
	    rec1m2, rec1m3, rec1m4;
    extern /* Subroutine */ int aqtran_(integer *, integer *), mgtran_(
	    integer *, integer *, integer *);

    /* Fortran I/O blocks */
    static cilist io___19 = { 0, 0, 0, 0, 0 };
    static cilist io___20 = { 0, 0, 0, 0, 0 };
    static cilist io___21 = { 0, 0, 0, 0, 0 };
    static cilist io___22 = { 0, 0, 0, 0, 0 };
    static cilist io___23 = { 0, 0, 0, 0, 0 };
    static cilist io___24 = { 0, 0, 0, 0, 0 };


    tail_(&nmin1, &nmin2, &nmin3, &nmin4, &ngas, &naqs, &rec1m1, &rec1m2, &
	    rec1m3, &rec1m4, &rec1g, &rec1a);
    io___19.ciunit = io_1.wterm;
    s_wsle(&io___19);
    do_lio(&c__9, &c__1, " starting min1 transfer ", (ftnlen)24);
    e_wsle();
    mgtran_(&c__0, &nmin1, &rec1m1);
    io___20.ciunit = io_1.wterm;
    s_wsle(&io___20);
    do_lio(&c__9, &c__1, " starting min2 transfer ", (ftnlen)24);
    e_wsle();
    mgtran_(&c__1, &nmin2, &rec1m2);
    io___21.ciunit = io_1.wterm;
    s_wsle(&io___21);
    do_lio(&c__9, &c__1, " starting min3 transfer ", (ftnlen)24);
    e_wsle();
    mgtran_(&c__2, &nmin3, &rec1m3);
    io___22.ciunit = io_1.wterm;
    s_wsle(&io___22);
    do_lio(&c__9, &c__1, " starting min4 transfer ", (ftnlen)24);
    e_wsle();
    mgtran_(&c__3, &nmin4, &rec1m4);
    io___23.ciunit = io_1.wterm;
    s_wsle(&io___23);
    do_lio(&c__9, &c__1, " starting gas transfer ", (ftnlen)23);
    e_wsle();
    mgtran_(&c__0, &ngas, &rec1g);
    io___24.ciunit = io_1.wterm;
    s_wsle(&io___24);
    do_lio(&c__9, &c__1, " starting aqueous species transfer ", (ftnlen)35);
    e_wsle();
    aqtran_(&naqs, &rec1a);
    return 0;
} /* trnsfr_ */

/* ***************************************************************** */
/* ** tail - Read nmin1..4, ngas, naqs, lskip from tail of saf; */
/* **        calculate corresponding rec1m1..4, rec1g, rec1a; */
/* **        transfer these bookkeeping parameters to head of daf; */
/* **        read-skip to top of min1 block of saf. */
/* Subroutine */ int tail_(integer *nmin1, integer *nmin2, integer *nmin3, 
	integer *nmin4, integer *ngas, integer *naqs, integer *rec1m1, 
	integer *rec1m2, integer *rec1m3, integer *rec1m4, integer *rec1g, 
	integer *rec1a)
{
    /* Format strings */
    static char fmt_32[] = "()";
    static char fmt_50[] = "()";
    static char fmt_60[] = "(6(1x,i4))";

    /* System generated locals */
    integer i__1;
    alist al__1, al__2;

    /* Builtin functions */
    integer f_rew(alist *), s_rsfe(cilist *), e_rsfe(void), f_back(alist *), 
	    s_rsle(cilist *), do_lio(integer *, integer *, char *, ftnlen), 
	    e_rsle(void), s_wdfe(cilist *), do_fio(integer *, char *, ftnlen),
	     e_wdfe(void);

    /* Local variables */
    static integer i__, l, lskip;

    /* Fortran I/O blocks */
    static cilist io___26 = { 0, 0, 1, fmt_32, 0 };
    static cilist io___27 = { 0, 0, 0, 0, 0 };
    static cilist io___28 = { 0, 0, 0, 0, 0 };
    static cilist io___29 = { 0, 0, 0, 0, 0 };
    static cilist io___30 = { 0, 0, 0, 0, 0 };
    static cilist io___31 = { 0, 0, 0, 0, 0 };
    static cilist io___32 = { 0, 0, 0, 0, 0 };
    static cilist io___33 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___34 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___35 = { 0, 0, 0, fmt_32, 0 };
    static cilist io___36 = { 0, 0, 0, 0, 0 };
    static cilist io___39 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___40 = { 0, 0, 0, fmt_60, 1 };
    static cilist io___41 = { 0, 0, 0, fmt_60, 2 };


/* **** NBACK = # lines to backspace before reading nmin[1..4],ngas,naqs. */
/* ****         NOTE!!!  On some installations, NBACK must be */
/* ****                  incremented to 11 */
/* ** go to EOF and backpeddle to read current species totals and lskip */
    al__1.aerr = 0;
    al__1.aunit = io_1.saf;
    f_rew(&al__1);
    for (i__ = 1; i__ <= 20000; ++i__) {
	io___26.ciunit = io_1.saf;
	i__1 = s_rsfe(&io___26);
	if (i__1 != 0) {
	    goto L3;
	}
	i__1 = e_rsfe();
	if (i__1 != 0) {
	    goto L3;
	}
/* L31: */
    }
L3:
    for (i__ = 1; i__ <= 11; ++i__) {
/* L33: */
	al__2.aerr = 0;
	al__2.aunit = io_1.saf;
	f_back(&al__2);
    }
    io___27.ciunit = io_1.saf;
    s_rsle(&io___27);
    do_lio(&c__3, &c__1, (char *)&(*nmin1), (ftnlen)sizeof(integer));
    e_rsle();
    io___28.ciunit = io_1.saf;
    s_rsle(&io___28);
    do_lio(&c__3, &c__1, (char *)&(*nmin2), (ftnlen)sizeof(integer));
    e_rsle();
    io___29.ciunit = io_1.saf;
    s_rsle(&io___29);
    do_lio(&c__3, &c__1, (char *)&(*nmin3), (ftnlen)sizeof(integer));
    e_rsle();
    io___30.ciunit = io_1.saf;
    s_rsle(&io___30);
    do_lio(&c__3, &c__1, (char *)&(*nmin4), (ftnlen)sizeof(integer));
    e_rsle();
    io___31.ciunit = io_1.saf;
    s_rsle(&io___31);
    do_lio(&c__3, &c__1, (char *)&(*ngas), (ftnlen)sizeof(integer));
    e_rsle();
    io___32.ciunit = io_1.saf;
    s_rsle(&io___32);
    do_lio(&c__3, &c__1, (char *)&(*naqs), (ftnlen)sizeof(integer));
    e_rsle();
    io___33.ciunit = io_1.saf;
    s_rsfe(&io___33);
    e_rsfe();
    io___34.ciunit = io_1.saf;
    s_rsfe(&io___34);
    e_rsfe();
    io___35.ciunit = io_1.saf;
    s_rsfe(&io___35);
    e_rsfe();
    io___36.ciunit = io_1.saf;
    s_rsle(&io___36);
    do_lio(&c__3, &c__1, (char *)&lskip, (ftnlen)sizeof(integer));
    e_rsle();
/* ** return to TOF and skip over comment lines, */
/* ** i.e. move forward to top of min1 block */
    al__1.aerr = 0;
    al__1.aunit = io_1.saf;
    f_rew(&al__1);
    i__1 = lskip;
    for (l = 1; l <= i__1; ++l) {
	io___39.ciunit = io_1.saf;
	s_rsfe(&io___39);
	e_rsfe();
/* L40: */
    }
/* ** calculate rec1m1..4, rec1g, rec1a; */
/* ** write these parameters to line 2 of daf. */
    *rec1m1 = 3;
    *rec1m2 = *rec1m1 + *nmin1 * 6;
    *rec1m3 = *rec1m2 + *nmin2 * 7;
    *rec1m4 = *rec1m3 + (*nmin3 << 3);
    *rec1g = *rec1m4 + *nmin4 * 9;
    *rec1a = *rec1g + *ngas * 6;
    io___40.ciunit = io_1.daf;
    s_wdfe(&io___40);
    do_fio(&c__1, (char *)&(*nmin1), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*nmin2), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*nmin3), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*nmin4), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*ngas), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*naqs), (ftnlen)sizeof(integer));
    e_wdfe();
    io___41.ciunit = io_1.daf;
    s_wdfe(&io___41);
    do_fio(&c__1, (char *)&(*rec1m1), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rec1m2), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rec1m3), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rec1m4), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rec1g), (ftnlen)sizeof(integer));
    do_fio(&c__1, (char *)&(*rec1a), (ftnlen)sizeof(integer));
    e_wdfe();
    return 0;
} /* tail_ */

/* ***************************************************************** */
/* ** mgtran - Read (from saf) data for current block of nmg mineral */
/* **          or gas species, each of which contains ntran phase */
/* **          transitions; write these data to daf, */
/* **          starting at REC=rec1. */
/* Subroutine */ int mgtran_(integer *ntran, integer *nmg, integer *rec1)
{
    /* Format strings */
    static char fmt_2[] = "()";
    static char fmt_10[] = "(1x,a20,a30)";
    static char fmt_20[] = "(1x,a15,5x,a40)";
    static char fmt_30[] = "(1x,a12,8x,a9)";
    static char fmt_40[] = "(4x,2(2x,f12.1),2(2x,f8.3))";
    static char fmt_50[] = "(4x,3(2x,f12.6),2x,f7.2,2x,f8.1,2(2x,f10.3))";
    static char fmt_60[] = "(4x,3(2x,f12.6))";
    static char fmt_70[] = "(8x,f7.2)";

    /* System generated locals */
    integer i__1, i__2;

    /* Builtin functions */
    integer s_rsfe(cilist *), e_rsfe(void), do_fio(integer *, char *, ftnlen),
	     s_wdfe(cilist *), e_wdfe(void);

    /* Local variables */
    static doublereal a[4], b[4], c__[4];
    static integer i__, j, r__;
    static doublereal gf, hf;
    static char ref[12];
    static doublereal htr[4], ttr[4], vtr[4];
    static char date[9], name__[20];
    static doublereal sref, vref, tmax;
    static char abrev[15], ecform[40], scform[30];
    static doublereal tslope[4];

    /* Fortran I/O blocks */
    static cilist io___43 = { 0, 0, 0, fmt_2, 0 };
    static cilist io___45 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___48 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___51 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___54 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___60 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___68 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___69 = { 0, 0, 0, fmt_70, 0 };
    static cilist io___71 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___72 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___73 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___74 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___75 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___76 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___77 = { 0, 0, 0, fmt_70, 0 };


/* **** skip global 3-line header for min1..4 or gas data block */
    for (i__ = 1; i__ <= 3; ++i__) {
	io___43.ciunit = io_1.saf;
	s_rsfe(&io___43);
	e_rsfe();
/* L1: */
    }
/* **** transfer data for all nmg individual species blocks */
    i__1 = *nmg;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* ****      set r = first record for current species block */
	r__ = *rec1 + (i__ - 1) * (*ntran + 6);
/* ****      read data from saf */
	io___45.ciunit = io_1.saf;
	s_rsfe(&io___45);
	do_fio(&c__1, name__, (ftnlen)20);
	do_fio(&c__1, scform, (ftnlen)30);
	e_rsfe();
	io___48.ciunit = io_1.saf;
	s_rsfe(&io___48);
	do_fio(&c__1, abrev, (ftnlen)15);
	do_fio(&c__1, ecform, (ftnlen)40);
	e_rsfe();
	io___51.ciunit = io_1.saf;
	s_rsfe(&io___51);
	do_fio(&c__1, ref, (ftnlen)12);
	do_fio(&c__1, date, (ftnlen)9);
	e_rsfe();
	io___54.ciunit = io_1.saf;
	s_rsfe(&io___54);
	do_fio(&c__1, (char *)&gf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&hf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&sref, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&vref, (ftnlen)sizeof(doublereal));
	e_rsfe();
	i__2 = *ntran;
	for (j = 1; j <= i__2; ++j) {
	    io___60.ciunit = io_1.saf;
	    s_rsfe(&io___60);
	    do_fio(&c__1, (char *)&a[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&b[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&c__[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&ttr[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&htr[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&vtr[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tslope[j - 1], (ftnlen)sizeof(doublereal));
	    e_rsfe();
/* L45: */
	}
	j = *ntran + 1;
	io___68.ciunit = io_1.saf;
	s_rsfe(&io___68);
	do_fio(&c__1, (char *)&a[j - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&b[j - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&c__[j - 1], (ftnlen)sizeof(doublereal));
	e_rsfe();
	io___69.ciunit = io_1.saf;
	s_rsfe(&io___69);
	do_fio(&c__1, (char *)&tmax, (ftnlen)sizeof(doublereal));
	e_rsfe();
/* ****      write data to daf ***** */
	io___71.ciunit = io_1.daf;
	io___71.cirec = r__;
	s_wdfe(&io___71);
	do_fio(&c__1, name__, (ftnlen)20);
	do_fio(&c__1, scform, (ftnlen)30);
	e_wdfe();
	io___72.ciunit = io_1.daf;
	io___72.cirec = r__ + 1;
	s_wdfe(&io___72);
	do_fio(&c__1, abrev, (ftnlen)15);
	do_fio(&c__1, ecform, (ftnlen)40);
	e_wdfe();
	io___73.ciunit = io_1.daf;
	io___73.cirec = r__ + 2;
	s_wdfe(&io___73);
	do_fio(&c__1, ref, (ftnlen)12);
	do_fio(&c__1, date, (ftnlen)9);
	e_wdfe();
	io___74.ciunit = io_1.daf;
	io___74.cirec = r__ + 3;
	s_wdfe(&io___74);
	do_fio(&c__1, (char *)&gf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&hf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&sref, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&vref, (ftnlen)sizeof(doublereal));
	e_wdfe();
	i__2 = *ntran;
	for (j = 1; j <= i__2; ++j) {
	    io___75.ciunit = io_1.daf;
	    io___75.cirec = r__ + 3 + j;
	    s_wdfe(&io___75);
	    do_fio(&c__1, (char *)&a[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&b[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&c__[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&ttr[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&htr[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&vtr[j - 1], (ftnlen)sizeof(doublereal));
	    do_fio(&c__1, (char *)&tslope[j - 1], (ftnlen)sizeof(doublereal));
	    e_wdfe();
/* L65: */
	}
	j = *ntran + 1;
	io___76.ciunit = io_1.daf;
	io___76.cirec = r__ + 4 + *ntran;
	s_wdfe(&io___76);
	do_fio(&c__1, (char *)&a[j - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&b[j - 1], (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&c__[j - 1], (ftnlen)sizeof(doublereal));
	e_wdfe();
	io___77.ciunit = io_1.daf;
	io___77.cirec = r__ + 5 + *ntran;
	s_wdfe(&io___77);
	do_fio(&c__1, (char *)&tmax, (ftnlen)sizeof(doublereal));
	e_wdfe();
/* L5: */
    }
    return 0;
} /* mgtran_ */

/* *********************************************************** */
/* ** aqtran - Read (from saf) data for aqueous species block; */
/* **          write these data to daf, starting at REC=rec1. */
/* Subroutine */ int aqtran_(integer *naqs, integer *rec1)
{
    /* Format strings */
    static char fmt_2[] = "()";
    static char fmt_10[] = "(1x,a20,a30)";
    static char fmt_20[] = "(1x,a15,5x,a40)";
    static char fmt_30[] = "(1x,a12,8x,a9)";
    static char fmt_40[] = "(4x,2(2x,f10.0),4x,f8.3)";
    static char fmt_50[] = "(4x,4(1x,f10.4,1x))";
    static char fmt_60[] = "(4x,3(2x,f8.4,2x),9x,f3.0)";

    /* System generated locals */
    integer i__1;

    /* Builtin functions */
    integer s_rsfe(cilist *), e_rsfe(void), do_fio(integer *, char *, ftnlen),
	     s_wdfe(cilist *), e_wdfe(void);

    /* Local variables */
    static integer i__, r__;
    static doublereal a1, a2, a3, a4, c1, c2, gf, hf;
    static char ref[12], date[9], name__[20];
    static doublereal sref, omega;
    static char abrev[15];
    static doublereal charge;
    static char ecform[40], scform[30];

    /* Fortran I/O blocks */
    static cilist io___79 = { 0, 0, 0, fmt_2, 0 };
    static cilist io___81 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___84 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___87 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___90 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___94 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___99 = { 0, 0, 0, fmt_60, 0 };
    static cilist io___104 = { 0, 0, 0, fmt_10, 0 };
    static cilist io___105 = { 0, 0, 0, fmt_20, 0 };
    static cilist io___106 = { 0, 0, 0, fmt_30, 0 };
    static cilist io___107 = { 0, 0, 0, fmt_40, 0 };
    static cilist io___108 = { 0, 0, 0, fmt_50, 0 };
    static cilist io___109 = { 0, 0, 0, fmt_60, 0 };


/* **** skip global 3-line header for aqueous species data block */
    for (i__ = 1; i__ <= 3; ++i__) {
	io___79.ciunit = io_1.saf;
	s_rsfe(&io___79);
	e_rsfe();
/* L1: */
    }
/* **** transfer data for all naqs individual aqueous species blocks */
    i__1 = *naqs;
    for (i__ = 1; i__ <= i__1; ++i__) {
/* ****      set r = first record for current species block */
	r__ = *rec1 + (i__ - 1) * 6;
/* ****      read data from saf */
	io___81.ciunit = io_1.saf;
	s_rsfe(&io___81);
	do_fio(&c__1, name__, (ftnlen)20);
	do_fio(&c__1, scform, (ftnlen)30);
	e_rsfe();
	io___84.ciunit = io_1.saf;
	s_rsfe(&io___84);
	do_fio(&c__1, abrev, (ftnlen)15);
	do_fio(&c__1, ecform, (ftnlen)40);
	e_rsfe();
	io___87.ciunit = io_1.saf;
	s_rsfe(&io___87);
	do_fio(&c__1, ref, (ftnlen)12);
	do_fio(&c__1, date, (ftnlen)9);
	e_rsfe();
	io___90.ciunit = io_1.saf;
	s_rsfe(&io___90);
	do_fio(&c__1, (char *)&gf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&hf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&sref, (ftnlen)sizeof(doublereal));
	e_rsfe();
	io___94.ciunit = io_1.saf;
	s_rsfe(&io___94);
	do_fio(&c__1, (char *)&a1, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&a2, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&a3, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&a4, (ftnlen)sizeof(doublereal));
	e_rsfe();
	io___99.ciunit = io_1.saf;
	s_rsfe(&io___99);
	do_fio(&c__1, (char *)&c1, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&c2, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&omega, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&charge, (ftnlen)sizeof(doublereal));
	e_rsfe();
/* ****      write data to daf */
	io___104.ciunit = io_1.daf;
	io___104.cirec = r__;
	s_wdfe(&io___104);
	do_fio(&c__1, name__, (ftnlen)20);
	do_fio(&c__1, scform, (ftnlen)30);
	e_wdfe();
	io___105.ciunit = io_1.daf;
	io___105.cirec = r__ + 1;
	s_wdfe(&io___105);
	do_fio(&c__1, abrev, (ftnlen)15);
	do_fio(&c__1, ecform, (ftnlen)40);
	e_wdfe();
	io___106.ciunit = io_1.daf;
	io___106.cirec = r__ + 2;
	s_wdfe(&io___106);
	do_fio(&c__1, ref, (ftnlen)12);
	do_fio(&c__1, date, (ftnlen)9);
	e_wdfe();
	io___107.ciunit = io_1.daf;
	io___107.cirec = r__ + 3;
	s_wdfe(&io___107);
	do_fio(&c__1, (char *)&gf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&hf, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&sref, (ftnlen)sizeof(doublereal));
	e_wdfe();
	io___108.ciunit = io_1.daf;
	io___108.cirec = r__ + 4;
	s_wdfe(&io___108);
	do_fio(&c__1, (char *)&a1, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&a2, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&a3, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&a4, (ftnlen)sizeof(doublereal));
	e_wdfe();
	io___109.ciunit = io_1.daf;
	io___109.cirec = r__ + 5;
	s_wdfe(&io___109);
	do_fio(&c__1, (char *)&c1, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&c2, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&omega, (ftnlen)sizeof(doublereal));
	do_fio(&c__1, (char *)&charge, (ftnlen)sizeof(doublereal));
	e_wdfe();
/* L5: */
    }
    return 0;
} /* aqtran_ */

/* ************************************************************** */
/* ** */
/* ** openf -  Returns .TRUE. and opens the file specified by */
/* **          fname, fstat, facces, fform, and frecl */
/* **          if this file exists and is accessible; otherwise, */
/* **          returns .FALSE. and prints an appropriate error */
/* **          message to the device specified by iterm. */
/* ** */
/* ** Author:     James W. Johnson */
/* ** */
/* ** Abandoned:  8 October 1990 */
/* ** */
/* ************************************************************** */
logical openf_(integer *iterm, integer *iunit, char *fname, integer *istat, 
	integer *iacces, integer *iform, integer *irecl, ftnlen fname_len)
{
    /* Initialized data */

    static char fform[11*2] = "FORMATTED  " "UNFORMATTED";
    static char facces[10*2] = "SEQUENTIAL" "DIRECT    ";
    static char fstat[3*2] = "OLD" "NEW";

    /* Format strings */
    static char fmt_20[] = "(/,\002 nonexistant file or invalid specificatio"
	    "ns\002,\002 ... try again\002,/)";

    /* System generated locals */
    integer i__1;
    logical ret_val;
    olist o__1;

    /* Builtin functions */
    integer f_open(olist *), s_wsfe(cilist *), e_wsfe(void);

    /* Fortran I/O blocks */
    static cilist io___113 = { 0, 0, 0, fmt_20, 0 };


    ret_val = FALSE_;
    if (*iacces < 1 || *iacces > 2 || *iform < 1 || *iform > 2 || *istat < 1 
	    || *istat > 2) {
	goto L10;
    }
    if (*iacces == 1) {
	o__1.oerr = 1;
	o__1.ounit = *iunit;
	o__1.ofnmlen = 20;
	o__1.ofnm = fname;
	o__1.orl = 0;
	o__1.osta = fstat + (*istat - 1) * 3;
	o__1.oacc = facces + (*iacces - 1) * 10;
	o__1.ofm = fform + (*iform - 1) * 11;
	o__1.oblnk = 0;
	i__1 = f_open(&o__1);
	if (i__1 != 0) {
	    goto L10;
	}
	ret_val = TRUE_;
	return ret_val;
    } else {
	o__1.oerr = 1;
	o__1.ounit = *iunit;
	o__1.ofnmlen = 20;
	o__1.ofnm = fname;
	o__1.orl = *irecl;
	o__1.osta = fstat + (*istat - 1) * 3;
	o__1.oacc = facces + (*iacces - 1) * 10;
	o__1.ofm = fform + (*iform - 1) * 11;
	o__1.oblnk = 0;
	i__1 = f_open(&o__1);
	if (i__1 != 0) {
	    goto L10;
	}
	ret_val = TRUE_;
	return ret_val;
    }
L10:
    io___113.ciunit = *iterm;
    s_wsfe(&io___113);
    e_wsfe();
    return ret_val;
} /* openf_ */

/* Main program alias */ int cprons_ () { MAIN__ (); return 0; }
